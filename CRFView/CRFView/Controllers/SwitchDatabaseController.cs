﻿using System.Web.Mvc;
using System.Configuration;

namespace CRFView.Controllers
{
    public class SwitchDatabaseController : Controller
    {
        [HttpPost]
        public string configuration(string catalog)
        {
            
            Session["Currdb"] = catalog;
            string Connstring;
            if (catalog == ConfigurationManager.AppSettings["ProductionDB"])
            {
                Connstring = ConfigurationManager.AppSettings["ProductionConString"];
            }
            else if (catalog == ConfigurationManager.AppSettings["TestDB"])
            {
                Connstring = ConfigurationManager.AppSettings["TestConString"];
            }
            else
            {
                Connstring = ConfigurationManager.AppSettings["ProductionConString"];
            }
            ConfigurationManager.AppSettings.Set("PORTAL", Connstring);
            
            string currpath =Request.UrlReferrer.PathAndQuery;
            return currpath;

        }
    }
}