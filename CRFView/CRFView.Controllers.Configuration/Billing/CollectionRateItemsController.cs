﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;


namespace CRFView.Controllers
{
    public class CollectionRateItemsController : Controller
    {
        ADCollectionRateItems objADCollectionRateItems = new ADCollectionRateItems();
        // GET: ADCollectionRateItems
        public ActionResult Index()
        {
            List<ADCollectionRateItems> lstCollectionRateItems = GetList();
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstCollectionRateItems);
        }
        //Show details on Edit
        public ActionResult CollectionRateItemsDetails(int id)
        {
            ADCollectionRateItems objCollectionRateItems = objADCollectionRateItems.ReadCollectionRateItems(id);
            List<CommonBindList> CollectionRateList = CommonBindList.GetCollectionRateList();
            ViewBag.CollectionRateList = CollectionRateList;
            return View(GetVirtualPath("CollectionRateItemsDetails"), objCollectionRateItems);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult CollectionRateItemsSave(ADCollectionRateItems updatedCollectionRateItems)
        {
            objADCollectionRateItems.SaveCollectionRateItems(updatedCollectionRateItems);
            List<ADCollectionRateItems> lstCollectionRateItems = GetList();
            return PartialView(GetVirtualPath("_CollectionRateItemsList"), lstCollectionRateItems);
        }
        //Show blank details on Add
        public ActionResult CollectionRateItemsAdd()
        {
            ADCollectionRateItems objCollectionRateItems = new ADCollectionRateItems();
            ModelState.Clear();
            objCollectionRateItems.CollectionRateItemId = 0;
            List<CommonBindList> CollectionRateList = CommonBindList.GetCollectionRateList();
            ViewBag.CollectionRateList = CollectionRateList;
            return View(GetVirtualPath("CollectionRateItemsDetails"), objCollectionRateItems);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value ,string Key)
        {
            List<ADCollectionRateItems> ListCollectionRateItemsFilter = new List<ADCollectionRateItems>();
            List<ADCollectionRateItems> ListCollectionRateItems= (List<ADCollectionRateItems>)Session["ListCollectionRateItems"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    ListCollectionRateItemsFilter = (from item in ListCollectionRateItems
                                                   where item.CommCode.ToString() == value
                                                   select item).ToList();
                }
               else if (Key== "2")
                {
                    ListCollectionRateItemsFilter = (from item in ListCollectionRateItems
                                                     where item.SecondRate.ToString() == value
                                                     select item).ToList();
                }
                else if (Key == "3")
                {
                    ListCollectionRateItemsFilter = (from item in ListCollectionRateItems
                                                     where item.SkipRate.ToString() == value
                                                     select item).ToList();
                }
                else if (Key == "4")
                {
                    ListCollectionRateItemsFilter = (from item in ListCollectionRateItems
                                                     where item.AsgBalRate.ToString() == value
                                                     select item).ToList();
                }
                else if (Key == "5")
                {
                    ListCollectionRateItemsFilter = (from item in ListCollectionRateItems
                                                     where item.AgeThreshold.ToString() == value
                                                     select item).ToList();
                }
                else if (Key == "6")
                {
                    ListCollectionRateItemsFilter = (from item in ListCollectionRateItems
                                                     where item.AgeRate.ToString() == value
                                                     select item).ToList();
                }
                else if (Key == "7")
                {
                    ListCollectionRateItemsFilter = (from item in ListCollectionRateItems
                                                     where item.AsgBalThreshold.ToString() == value
                                                     select item).ToList();
                }
                else if (Key == "8")
                {
                    ListCollectionRateItemsFilter = (from item in ListCollectionRateItems
                                                     where item.JudgmentRate.ToString() == value
                                                     select item).ToList();
                }
                else if (Key == "9")
                {
                    ListCollectionRateItemsFilter = (from item in ListCollectionRateItems
                                                     where item.IntlRate.ToString() == value
                                                     select item).ToList();
                }
                else if (Key == "10")
                {
                    ListCollectionRateItemsFilter = (from item in ListCollectionRateItems
                                                     where item.CommType.ToLower().Contains(value.ToLower())
                                                     select item).ToList();
                }
                else if (Key == "11")
                {
                    ListCollectionRateItemsFilter = (from item in ListCollectionRateItems
                                                     where item.BalFrom.ToString() == value
                                                     select item).ToList();
                }
                else if (Key == "12")
                {
                    ListCollectionRateItemsFilter = (from item in ListCollectionRateItems
                                                     where item.BalThru.ToString() == value
                                                     select item).ToList();
                }
                else if (Key == "13")
                {
                    ListCollectionRateItemsFilter = (from item in ListCollectionRateItems
                                                     where item.CommRate.ToString() == value
                                                     select item).ToList();
                }

            }
            return View(GetVirtualPath("_CollectionRateItemsList"), ListCollectionRateItemsFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "CommCode" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Second Rate" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "Skip Rate" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "AsgBal Rate" });
            filterlist.Add(new CommonBindList { Id = 5, Name = "AgeThreshold" });
            filterlist.Add(new CommonBindList { Id = 6, Name = "AgeRate" });
            filterlist.Add(new CommonBindList { Id = 7, Name = "AsgBalThreshold" });
            filterlist.Add(new CommonBindList { Id = 8, Name = "Judgment Rate" });
            filterlist.Add(new CommonBindList { Id = 9, Name = "Initial Rate" });
            filterlist.Add(new CommonBindList { Id = 10, Name = "CommType" });
            filterlist.Add(new CommonBindList { Id = 11, Name = "Balance From" });
            filterlist.Add(new CommonBindList { Id = 12, Name = "Balance Thru" });
            filterlist.Add(new CommonBindList { Id = 13, Name = "CommRate" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADCollectionRateItems> lstCollectionRateItems = GetList();
            return PartialView(GetVirtualPath("_CollectionRateItemsList"), lstCollectionRateItems);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Billing/CollectionRateItems/" + ViewName + ".cshtml";
            return path;
        }

        //getList
        public List<ADCollectionRateItems> GetList()
        {
            List<ADCollectionRateItems> lstCollectionRateItems = objADCollectionRateItems.ListCollectionRateItems();
            Session["ListCollectionRateItems"] = lstCollectionRateItems;
            return  lstCollectionRateItems;
        }
    }
}