﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class BillableEventTypeController : Controller
    {
        ADBillableEventType objADBillableEventType = new ADBillableEventType();
        // GET: ADBillableEventType
        public ActionResult Index()
        {
            List<ADBillableEventType> lstBillableEventType = GetList();
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstBillableEventType);
        }
        //Show details on Edit
        public ActionResult BillableEventTypeDetails(int id)
        {
            ADBillableEventType objBillableEventType = objADBillableEventType.ReadBillableEventType(id);
            List<CommonBindList> ManagerList = CommonBindList.GetInternalUsersList();
            ViewBag.ManagerList = ManagerList;
            return View(GetVirtualPath("BillableEventTypeDetails"), objBillableEventType);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult BillableEventTypeSave(ADBillableEventType updatedBillableEventType)
        {
            objADBillableEventType.SaveBillableEventType(updatedBillableEventType);
            List<ADBillableEventType> lstBillableEventType = GetList();
            return PartialView(GetVirtualPath("_BillableEventTypeList"), lstBillableEventType);
        }
        //Show blank details on Add
        public ActionResult BillableEventTypeAdd()
        {
            ADBillableEventType objBillableEventType = new ADBillableEventType();
            ModelState.Clear();
            objBillableEventType.Id = 0;
            List<CommonBindList> ManagerList = CommonBindList.GetInternalUsersList();
            ViewBag.ManagerList = ManagerList;
            return View(GetVirtualPath("BillableEventTypeDetails"), objBillableEventType);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value ,string Key)
        {
            List<ADBillableEventType> ListBillableEventTypeFilter = new List<ADBillableEventType>();
            List<ADBillableEventType> ListBillableEventType= (List<ADBillableEventType>)Session["ListBillableEventType"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    ListBillableEventTypeFilter = (from item in ListBillableEventType
                                                   where item.Id.ToString() == value
                                                   select item).ToList();
                }
               else if (Key== "2")
                {
                    ListBillableEventTypeFilter = (from item in ListBillableEventType
                                                   where item.BillableEventName.ToLower().Contains(value.ToLower())
                                                   select item).ToList();
                   
                }
                else if (Key == "3")
                {
                    ListBillableEventTypeFilter = (from item in ListBillableEventType
                                                   where item.BillableEventCode.ToLower().Contains(value.ToLower())
                                                   select item).ToList();
                }
                else if (Key == "4")
                {
                    ListBillableEventTypeFilter = (from item in ListBillableEventType
                                                   where item.DisplayOrder.ToString() == value
                                                   select item).ToList();
                }

            }
            return View(GetVirtualPath("_BillableEventTypeList"), ListBillableEventTypeFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "EventType ID" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "EventType Name" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "EventType Code" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "Display Order" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADBillableEventType> lstBillableEventType = GetList();
            return PartialView(GetVirtualPath("_BillableEventTypeList"), lstBillableEventType);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Billing/BillableEventType/" + ViewName + ".cshtml";
            return path;
        }

        //getList
        public List<ADBillableEventType> GetList()
        {
            List<ADBillableEventType> lstBillableEventType = objADBillableEventType.ListBillableEventType();
            Session["ListBillableEventType"] = lstBillableEventType;
            return  lstBillableEventType;
        }
    }
}