﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class EventPricingController : Controller
    {
        ADEventPricing objADEventPricing = new ADEventPricing();
        // GET: ADEventPricing
        public ActionResult Index()
        {
            List<ADEventPricing> lstEventPricing = GetList();
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstEventPricing);
        }
        //Show details on Edit
        public ActionResult EventPricingDetails(int id)
        {
            ADEventPricing objEventPricing = objADEventPricing.ReadEventPricing(id);
            List<CommonBindList> PriceCodeList = CommonBindList.GetPriceCodeList();
            ViewBag.PriceCodeList = PriceCodeList;
            return View(GetVirtualPath("EventPricingDetails"), objEventPricing);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult EventPricingSave(ADEventPricing updatedEventPricing)
        {
            objADEventPricing.SaveEventPricing(updatedEventPricing);
            List<ADEventPricing> lstEventPricing = GetList();
            return PartialView(GetVirtualPath("_EventPricingList"), lstEventPricing);
        }
        //Show blank details on Add
        public ActionResult EventPricingAdd()
        {
            ADEventPricing objEventPricing = new ADEventPricing();
            ModelState.Clear();
            objEventPricing.Id = 0;
            List<CommonBindList> PriceCodeList = CommonBindList.GetPriceCodeList();
            ViewBag.PriceCodeList = PriceCodeList;
            return View(GetVirtualPath("EventPricingDetails"), objEventPricing);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value ,string Key)
        {
            List<ADEventPricing> ListEventPricingFilter = new List<ADEventPricing>();
            List<ADEventPricing> ListEventPricing= (List<ADEventPricing>)Session["ListEventPricing"];
            if (value != "" || value != null)
            {
            if (Key== "1")
                {
                    ListEventPricingFilter = (from item in ListEventPricing
                                                   where item.PriceCodeName.ToLower().Contains(value.ToLower())
                                                   select item).ToList();
                }
                else if (Key == "2")
                {
                    ListEventPricingFilter = (from item in ListEventPricing
                                              where item.QtyFrom.ToString().ToLower().Contains(value.ToLower())
                                              select item).ToList();
                }
                else if (Key == "3")
                {
                    ListEventPricingFilter = (from item in ListEventPricing
                                             where item.QtyThru.ToString().ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
                else if (Key == "4")
                {
                    ListEventPricingFilter = (from item in ListEventPricing
                                             where item.Price.ToString().ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
                else if (Key == "5")
                {
                    ListEventPricingFilter = (from item in ListEventPricing
                                             where item.IsDefaultPrice.ToString().ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
            }
            return View(GetVirtualPath("_EventPricingList"), ListEventPricingFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Price Description" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "QtyFrom" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "QtyThru" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "Price" });
            filterlist.Add(new CommonBindList { Id = 5, Name = "IsDefaultPrice" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADEventPricing> lstEventPricing = GetList();
            return PartialView(GetVirtualPath("_EventPricingList"), lstEventPricing);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Billing/EventPricing/" + ViewName + ".cshtml";
            return path;
        }

        //getList
        public List<ADEventPricing> GetList()
        {
            List<ADEventPricing> lstEventPricing = objADEventPricing.ListEventPricing();
            Session["ListEventPricing"] = lstEventPricing;
            return  lstEventPricing;
        }
    }
}