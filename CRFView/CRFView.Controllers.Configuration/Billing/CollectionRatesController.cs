﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;


namespace CRFView.Controllers
{
    public class CollectionRatesController : Controller
    {
        ADCollectionRates objADCollectionRates = new ADCollectionRates();
        // GET: ADCollectionRates
        public ActionResult Index()
        {
            List<ADCollectionRates> lstCollectionRates = GetList();
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstCollectionRates);
        }
        //Show details on Edit
        public ActionResult CollectionRatesDetails(int id)
        {
            ADCollectionRates objCollectionRates = objADCollectionRates.ReadADCollectionRates(id);
            List<CommonBindList> ManagerList = CommonBindList.GetInternalUsersList();
            ViewBag.ManagerList = ManagerList;
            return View(GetVirtualPath("CollectionRatesDetails"), objCollectionRates);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult CollectionRatesSave(ADCollectionRates updatedCollectionRates)
        {
            objADCollectionRates.SaveCollectionRates(updatedCollectionRates);
            List<ADCollectionRates> lstCollectionRates = GetList();
            return PartialView(GetVirtualPath("_CollectionRatesList"), lstCollectionRates);
        }
        //Show blank details on Add
        public ActionResult CollectionRatesAdd()
        {
            ADCollectionRates objCollectionRates = new ADCollectionRates();
            ModelState.Clear();
            objCollectionRates.Id = 0;
            List<CommonBindList> ManagerList = CommonBindList.GetInternalUsersList();
            ViewBag.ManagerList = ManagerList;
            return View(GetVirtualPath("CollectionRatesDetails"), objCollectionRates);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value ,string Key)
        {
            List<ADCollectionRates> ListCollectionRatesFilter = new List<ADCollectionRates>();
            List<ADCollectionRates> ListCollectionRates= (List<ADCollectionRates>)Session["ListCollectionRates"];
            if (value != "" || value != null)
            {
 
            if (Key== "1")
                {
                    ListCollectionRatesFilter = (from item in ListCollectionRates
                                                   where item.CollectionRateName.ToLower().Contains(value.ToLower())
                                                   select item).ToList();
                   
                }
                else if (Key == "2")
                {
                    ListCollectionRatesFilter = (from item in ListCollectionRates
                                                   where item.CollectionRateCode.ToLower().Contains(value.ToLower())
                                                   select item).ToList();
                }
     

            }
            return View(GetVirtualPath("_CollectionRatesList"), ListCollectionRatesFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Collection Rate Name" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Collection Rate Code" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADCollectionRates> lstCollectionRates = GetList();
            return PartialView(GetVirtualPath("_CollectionRatesList"), lstCollectionRates);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Billing/CollectionRates/" + ViewName + ".cshtml";
            return path;
        }

        //getList
        public List<ADCollectionRates> GetList()
        {
            List<ADCollectionRates> lstCollectionRates = objADCollectionRates.ListCollectionRates();
            Session["ListCollectionRates"] = lstCollectionRates;
            return  lstCollectionRates;
        }
    }
}