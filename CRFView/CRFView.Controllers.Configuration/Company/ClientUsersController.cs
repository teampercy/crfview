﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class ClientUsersController : Controller
    {
        ADClientUser objADClientUser = new ADClientUser();

        // // GET: CLientUsers
        public ActionResult Index()
        {
            List<ADClientUser> ClientUsersList = objADClientUser.ClientUserGetList();
            Session["objClientUserList"] = ClientUsersList;
            SetFilterList();
            ViewBag.totalCount = ClientUsersList.Count;
            return PartialView(GetVirtualPath("Index"), ClientUsersList);
        }
        public ActionResult ClientUserAdd()
        { 
            objADClientUser = new ADClientUser();
            objADClientUser.ID = 0;
            objADClientUser.ClientList = CommonBindList.GetClientList();
            objADClientUser.ClientViewGroupList = CommonBindList.GetClientViewGroups();
            return View(GetVirtualPath("ClientUserDetails"), objADClientUser);

        }
        [HttpPost]
        public ActionResult Search(string value,string Key)
        {
            List<ADClientUser> ClientUserList = (List<ADClientUser>)Session["objClientUserList"];
            List<ADClientUser> ClientUserFilterList = new List<ADClientUser>();
            if (value != "" || value != null)
            {
                if(Key=="1")
                {
                    ClientUserFilterList = (from item in ClientUserList
                                            where item.ClientCode.ToLower().Contains(value.ToLower())
                                            select item).ToList();
                }
                else if (Key == "2")
                {
                    ClientUserFilterList = (from item in ClientUserList
                                            where item.UserName.ToLower().Contains(value.ToLower())
                                            select item).ToList();
                }
                else if (Key == "3")
                {
                    ClientUserFilterList = (from item in ClientUserList
                                            where item.Email.ToLower().Contains(value.ToLower())
                                            select item).ToList();
                }
           

            }
            return View(GetVirtualPath("_ClientUsersList"), ClientUserFilterList);
        }

        public ActionResult ClientUserDetails(int id)
        {         
            ADClientUser objClientUsers = new ADClientUser();
            objClientUsers = objADClientUser.ClientUserRead(id);
            objClientUsers.ClientList= CommonBindList.GetClientList();
            objClientUsers.ClientViewGroupList = CommonBindList.GetClientViewGroups();
            objClientUsers.UserSecurity = objADClientUser.UpdateUserSecurity(objClientUsers);
            objClientUsers.HasGroupAccess= objADClientUser.UpdateHasAccess(objClientUsers);
            return View(GetVirtualPath("ClientUserDetails"), objClientUsers);
        }

        [HttpPost]
        public ActionResult ClientUserSave(ADClientUser updatedUser)
        {
            #region UserFieldsUpdates
            updatedUser.InternalUser = 0;
            if(updatedUser.HasGroupAccess==false)
            {
                updatedUser.ClientViewGroupId = 0;
            }
            switch (updatedUser.UserSecurity)
            {
                case "Manager":
                    updatedUser.IsClientViewManager = 1;
                    updatedUser.IsClientViewAdmin = false;
                    break;
                case "Staff":
                    updatedUser.IsClientViewManager = 0;
                    updatedUser.IsClientViewAdmin = false;
                    break;
                case "Admin":
                    updatedUser.IsClientViewAdmin = true;
                    updatedUser.IsClientViewManager = 0;
                    break;
                default:
                    updatedUser.IsClientViewManager = 0;
                    updatedUser.IsClientViewAdmin = false;
                    break;
            }

            objADClientUser.SaveClientUser(updatedUser);
            List<ADClientUser> ClientUserList = objADClientUser.ClientUserGetList();
            Session["objClientUserList"] = ClientUserList;
            return PartialView(GetVirtualPath("_ClientUsersList"), ClientUserList);

        }
        [HttpPost]
        public ActionResult Refresh()
        {
            List <ADClientUser> ClientUserList= objADClientUser.ClientUserGetList();
            Session["objClientUserList"] = ClientUserList;
            return PartialView(GetVirtualPath("_ClientUsersList"), ClientUserList);
        }
       
        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Client Code" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "UserName" });
            filterlist.Add(new CommonBindList { Id = 5, Name = "Email" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Company/ClientUsers/" + ViewName + ".cshtml";
            return path;
        }
        #endregion
    }
}