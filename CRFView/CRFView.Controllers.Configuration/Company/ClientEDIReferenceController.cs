﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CRFView.Adapters;

namespace CRFView.Controllers
{
    public class ClientEDIReferenceController : Controller
    {
        ADClientEDIReference objClientEDIReference = new ADClientEDIReference();
   
       
        // GET: ClientEDIReference
        public ActionResult Index()
        {
            List<ADClientEDIReference> ClientEDIReferenceList = objClientEDIReference.ListClientEDIReference();
            Session["ListClientEDIReference"] = ClientEDIReferenceList;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"), ClientEDIReferenceList);
        }
        //Show details on Edit
        public ActionResult ClientEDIReferenceDetails(int id)
        {
             objClientEDIReference = objClientEDIReference.ReadClientEDIReference(id);
            objClientEDIReference.ClientList = CommonBindList.GetClientList();
            objClientEDIReference.ClientInterfaceList = CommonBindList.GetClientInterface();
            return View(GetVirtualPath("ClientEDIReferenceDetails"), objClientEDIReference);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult ClientEDIReferenceSave(ADClientEDIReference updatedClientEDIReference)
        {
            objClientEDIReference.SaveClientEDIReference(updatedClientEDIReference);
            List<ADClientEDIReference> ClientEDIReferenceList = objClientEDIReference.ListClientEDIReference();
            Session["ListClientEDIReference"] = ClientEDIReferenceList;
            return PartialView(GetVirtualPath("_ClientEDIReferenceList"), ClientEDIReferenceList);
        }
        //Show blank details on Add
        public ActionResult ClientEDIReferenceAdd()
        {
            objClientEDIReference = new ADClientEDIReference();
            objClientEDIReference.Id = 0;
            objClientEDIReference.ClientList = CommonBindList.GetClientList();
            objClientEDIReference.ClientInterfaceList = CommonBindList.GetClientInterface();
            return View(GetVirtualPath("ClientEDIReferenceDetails"), objClientEDIReference);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value ,string Key)
        {
           List<ADClientEDIReference> ClientEDIReferenceList = (List<ADClientEDIReference>)Session["ListClientEDIReference"];
            List<ADClientEDIReference> ClientEDIReferenceFilterList = ClientEDIReferenceList;
            if (value != "" || value != null)
            {
                switch (Key)
                {
                    case "1":
                        ClientEDIReferenceFilterList = (from item in ClientEDIReferenceList
                                                        where item.ClientCode.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    case "2":

                        ClientEDIReferenceFilterList = (from item in ClientEDIReferenceList
                                                        where item.InterfaceName.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    case "3":

                        ClientEDIReferenceFilterList = (from item in ClientEDIReferenceList
                                                        where item.InputField.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    case "4":

                        ClientEDIReferenceFilterList = (from item in ClientEDIReferenceList
                                                        where item.ClientName.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    default:
                        ClientEDIReferenceFilterList = null;
                        break;

                }
            }
            return View(GetVirtualPath("_ClientEDIReferenceList"), ClientEDIReferenceFilterList);


        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Client Code" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Interface Name" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "Input Field" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "Client Name" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }
        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADClientEDIReference> ClientEDIReferenceList = objClientEDIReference.ListClientEDIReference();
            Session["ListClientEDIReference"] = ClientEDIReferenceList;
            return PartialView(GetVirtualPath("_ClientEDIReferenceList"), ClientEDIReferenceList);
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Company/ClientEDIReference/" + ViewName + ".cshtml";
            return path;
        }
    }
}