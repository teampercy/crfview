﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class SystemKeyValuesController : Controller
    {
        ADSystemKeyValues objADSystemValues = new ADSystemKeyValues();
        // GET: SystemValues
        public ActionResult Index()
        {
            List<ADSystemKeyValues> lstSystemKeyValues =  objADSystemValues.ListSystemKeyValues();
            SetFilterList();
            Session["ListSystemKeyValues"] = lstSystemKeyValues;
            return PartialView(GetVirtualPath("Index"),lstSystemKeyValues);
        }
        //Show details on Edit
        public ActionResult SystemKeyValuesDetails(int id)
        {
            ADSystemKeyValues objSystemValues = objADSystemValues.ReadSystemKeyValues(id);
            return View(GetVirtualPath("SystemKeyValuesDetails"), objSystemValues);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult SystemKeyValuesSave(ADSystemKeyValues updatedSystemKeyValues)
        {
            objADSystemValues.SaveSystemKeyValues(updatedSystemKeyValues);
            List<ADSystemKeyValues> lstSystemKeyValues =  objADSystemValues.ListSystemKeyValues();
            Session["ListSystemKeyValues"] = lstSystemKeyValues;
            return PartialView(GetVirtualPath("_SystemKeyValuesList"), lstSystemKeyValues);
        }
        //Show blank details on Add
        public ActionResult SystemKeyValuesAdd()
        {
            ADSystemKeyValues objSystemKeyValues = new ADSystemKeyValues();
            ModelState.Clear();
            objSystemKeyValues.Id = 0;
            return View(GetVirtualPath("SystemKeyValuesDetails"), objSystemKeyValues);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {
            List<ADSystemKeyValues> ListSystemKeyValuesFilter = new List<ADSystemKeyValues>();
            List<ADSystemKeyValues> ListSystemKeyValues = (List<ADSystemKeyValues>)Session["ListSystemKeyValues"];
            if (value != "" || value != null)
            {
                if(Key=="1")
                {
                    ListSystemKeyValuesFilter = (from item in ListSystemKeyValues
                                                 where item.VALUEKEY.ToLower().Contains(value.ToLower())
                                                 select item).ToList();
                }
                else if (Key == "1")
                {
                    ListSystemKeyValuesFilter = (from item in ListSystemKeyValues
                                                 where item.KEYVALUE.ToLower().Contains(value.ToLower())
                                                 select item).ToList();
                }

            }
            return View(GetVirtualPath("_SystemKeyValuesList"), ListSystemKeyValuesFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "VALUEKEY" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "KEYVALUE" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADSystemKeyValues> lstSystemKeyValues = objADSystemValues.ListSystemKeyValues();
            Session["ListSystemKeyValues"] = lstSystemKeyValues;
            return PartialView(GetVirtualPath("_SystemKeyValuesList"), lstSystemKeyValues);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Company/SystemKeyValues/" + ViewName + ".cshtml";
            return path;
        }
    }
}