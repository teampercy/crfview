﻿
using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;


namespace CRFView_WEB.Controllers
{
    public class SchedulerTasksController : Controller
    {
        ADSchedulerTasks objADSchedulerTasks = new ADSchedulerTasks();

        // GET: SchedulerTasks
        public ActionResult Index()
        {
             List<ADSchedulerTasks> SchedulerTasksList = objADSchedulerTasks.ListSchedulerTasks();
            Session["ListSchedulerTasks"] = SchedulerTasksList;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"), SchedulerTasksList);
    
        }

        //Show details on Edit
        public ActionResult SchedulerTasksDetails(int id)
        {

            ADSchedulerTasks SchedulerTasksDetails = objADSchedulerTasks.ReadSchedulerTasks(id);
            SchedulerTasksDetails.ClientUsersList = CommonBindList.GetClientUsersList();
            SchedulerTasksDetails.InternalUsersList = CommonBindList.GetInternalUsersList();
            SchedulerTasksDetails.SelectedClientUsersList = CommonBindList.GetSelectedUsersList(SchedulerTasksDetails.clientuserlist,"Client");
            SchedulerTasksDetails.SelectedInternalUsersList = CommonBindList.GetSelectedUsersList(SchedulerTasksDetails.internaluserlist,"Internal");
            SchedulerTasksDetails.TaskNamesList = CommonBindList.GetTaskNamesList();
           return View(GetVirtualPath("SchedulerTasksDetails"), SchedulerTasksDetails);
        }

        //Save Updated or New Details
        [HttpPost]
        public ActionResult SchedulerTasksSave(ADSchedulerTasks updatedADSchedulerTasks)
        {
            objADSchedulerTasks.SaveSchedulerTasks(updatedADSchedulerTasks);
            List<ADSchedulerTasks> SchedulerTasksList = objADSchedulerTasks.ListSchedulerTasks();
            Session["ListSchedulerTasks"] = SchedulerTasksList;
            return PartialView(GetVirtualPath("_SchedulerTasksList"), SchedulerTasksList);
        }

        //Show blank details on Add
        public ActionResult SchedulerTasksAdd()
        {
            ADSchedulerTasks SchedulerTasksDetails = new ADSchedulerTasks();
            SchedulerTasksDetails.PKID = 0;
            SchedulerTasksDetails.ClientUsersList = CommonBindList.GetClientUsersList();
            SchedulerTasksDetails.InternalUsersList = CommonBindList.GetInternalUsersList();
            SchedulerTasksDetails.SelectedClientUsersList =  new List<CommonBindList>();
            SchedulerTasksDetails.SelectedInternalUsersList = new List<CommonBindList>();
            SchedulerTasksDetails.TaskNamesList = CommonBindList.GetTaskNamesList();
            return View(GetVirtualPath("SchedulerTasksDetails"), SchedulerTasksDetails);
        }

        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value , string Key)
        {
            List<ADSchedulerTasks> SchedulerTasksFilter = new List<ADSchedulerTasks>();
            List<ADSchedulerTasks> SchedulerTasksList = (List<ADSchedulerTasks>) Session["ListSchedulerTasks"];
            if (value != "" || value != null)
            {
                if(Key=="1")
                {
                    SchedulerTasksFilter = (from item in SchedulerTasksList
                                            where item.taskname.ToLower().Contains(value.ToLower())
                                                  select item).ToList();
                }
                else if (Key == "2")
                {
                    SchedulerTasksFilter = (from item in SchedulerTasksList
                                            where item.lastmessage.ToLower().Contains(value.ToLower())
                                            select item).ToList();
                }

            }
            return View(GetVirtualPath("_SchedulerTasksList"), SchedulerTasksFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Task Name" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Last Message" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADSchedulerTasks> SchedulerTasksList = objADSchedulerTasks.ListSchedulerTasks();
            Session["ListSchedulerTasks"] = SchedulerTasksList;
            return PartialView(GetVirtualPath("_SchedulerTasksList"), SchedulerTasksList);
        }

        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Company/SchedulerTasks/" + ViewName +".cshtml";
            return path;
        }
    }
}