﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CRFView.Adapters;

namespace CRFView.Controllers
{
    public class ReportFileNamesController : Controller
    {
        ADReportFileName objReportFileName = new ADReportFileName();
        // GET: ADReportFileName
        public ActionResult Index()
        {
            List<ADReportFileName> lstReportfileNames =  objReportFileName.ListReportFileNames();
            Session["ListReportfileNames"] = lstReportfileNames;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstReportfileNames);
        }
        public ActionResult ReportFileNameDetails(int id)
        {
            ADReportFileName objReportFilenames =  objReportFileName.ReadReportFileNames(id);
            return View(GetVirtualPath("ReportFileNameDetails"), objReportFilenames);
        }

        [HttpPost]
        public ActionResult ReportFileNameSave(ADReportFileName updatedReportFilenames)
        {
            objReportFileName.SaveReportFilenames(updatedReportFilenames);
            List<ADReportFileName> lstReportfileNames =  objReportFileName.ListReportFileNames();
            Session["ListReportfileNames"] = lstReportfileNames;
            return PartialView(GetVirtualPath("_ReportFileNamesList"), lstReportfileNames);

        }
        public ActionResult ReportFileNameAdd()
        {
            ADReportFileName objReportFileName = new ADReportFileName();
            ModelState.Clear();
            objReportFileName.ReportFileId = 0;
            return View(GetVirtualPath("ReportFileNameDetails"), objReportFileName);

        }

        [HttpPost]
        public ActionResult Search(string value,string Key)
        {
            List<ADReportFileName> ListReportfileNamesFilter = new List<ADReportFileName>();
            List<ADReportFileName> ListReportfileNames= (List<ADReportFileName>)Session["ListReportfileNames"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    ListReportfileNamesFilter = (from item in ListReportfileNames
                                                 where item.ReportFileName.ToLower().Contains(value.ToLower())
                                                 select item).ToList();
                }
            }
            return View(GetVirtualPath("_ReportFileNamesList"), ListReportfileNamesFilter);
        }
        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "ReportFile Name" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }
        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADReportFileName> lstReportfileNames =  objReportFileName.ListReportFileNames();
            Session["ListReportfileNames"] = lstReportfileNames;
            return PartialView(GetVirtualPath("_ReportFileNamesList"),lstReportfileNames);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Company/ReportFileNames/" + ViewName + ".cshtml";
            return path;
        }

    }
}