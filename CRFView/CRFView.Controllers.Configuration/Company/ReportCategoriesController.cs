﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class ReportCategoriesController : Controller
    {
        ADReportCategory objReportCategories = new ADReportCategory();
        // GET: ReportCategories
        public ActionResult Index()
        {
            List<ADReportCategory> lstReportCategories =  objReportCategories.ListReportCategories();
            Session["ListReportCategories"] = lstReportCategories;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstReportCategories);
            
        }
        //Show details on Edit
        public ActionResult ReportCategoryDetails(int id)
        {
            ADReportCategory objReportCatagories = objReportCategories.ReadReportCategories(id);
            return View(GetVirtualPath("ReportCategoryDetails"), objReportCatagories);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult ReportCategorySave(ADReportCategory updatedReportCategories)
        {
            objReportCategories.SaveReportCategories(updatedReportCategories);
            List<ADReportCategory> lstReportCategories =  objReportCategories.ListReportCategories();
            Session["ListReportCategories"] = lstReportCategories;
            return PartialView(GetVirtualPath("_ReportCategoriesList"), lstReportCategories);
        }
        //Show blank details on Add
        public ActionResult ReportCategoryAdd()
        {
            ADReportCategory objReportCatagories = new ADReportCategory();
            ModelState.Clear();
            objReportCatagories.Id = 0;
            return View(GetVirtualPath("ReportCategoryDetails"), objReportCatagories);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value,string Key)
        {
            List<ADReportCategory> ListReportCategoriesFilter = new List<ADReportCategory>();
            List<ADReportCategory> ListReportCategories;
            ListReportCategories = (List<ADReportCategory>)Session["ListReportCategories"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    ListReportCategoriesFilter = (from item in ListReportCategories
                                                  where item.ReportCategory.ToLower().Contains(value.ToLower())
                                                  select item).ToList();
                }
            }        
            return View(GetVirtualPath("_ReportCategoriesList"), ListReportCategoriesFilter);
        }
        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADReportCategory> lstReportCategories =  objReportCategories.ListReportCategories();
            Session["ListReportCategories"] = lstReportCategories;
            return PartialView(GetVirtualPath("_ReportCategoriesList"), lstReportCategories);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Report Category" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Company/ReportCategories/" + ViewName + ".cshtml";
            return path;
        }
    }
}