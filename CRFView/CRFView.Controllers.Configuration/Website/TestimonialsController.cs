﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class TestimonialsController : Controller
    {
        ADPortal_Testimonials objTestimonials = new ADPortal_Testimonials();

        // GET: Testimonials
        public ActionResult Index()
        {
            List<ADPortal_Testimonials> TestimonialsList = objTestimonials.ListTestimonials();
            Session["ListTestimonials"] = TestimonialsList;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"), TestimonialsList);

        }
        //Show details on Edit
        public ActionResult TestimonialsDetails(int id)
        {
            objTestimonials = new ADPortal_Testimonials();
            objTestimonials = objTestimonials.ReadTestimonials(id);

            return View(GetVirtualPath("TestimonialsDetails"), objTestimonials);
        }
        //Save Updated or New Details
        [HttpPost]
        [ValidateInput(false)]
        public bool TestimonialsSave(ADPortal_Testimonials updatedTestimonials)
        {
            return objTestimonials.SaveTestimonials(updatedTestimonials);
        }
        //Show blank details on Add
        public ActionResult TestimonialsAdd()
        {
            objTestimonials = new ADPortal_Testimonials();
            objTestimonials.PKID = 0;
            return View(GetVirtualPath("TestimonialsDetails"), objTestimonials);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {

            List<ADPortal_Testimonials> TestimonialsList = (List<ADPortal_Testimonials>)Session["ListTestimonials"];
            List<ADPortal_Testimonials> TestimonialsFilterList = null;
            if (value != "" || value != null)
            {
                switch (Key)
                {
                    case "1":
                        TestimonialsFilterList = (from item in TestimonialsList
                                          where item.testimonial.ToLower().Contains(value.ToLower())
                                          select item).ToList();
                        break;
                    case "2":
                        TestimonialsFilterList = (from item in TestimonialsList
                                          where item.testimonialby.ToLower().Contains(value.ToLower())
                                          select item).ToList();
                        break;
                    default:
                        TestimonialsFilterList = null;
                        break;
                }
            }
            return View(GetVirtualPath("_TestimonialsList"), TestimonialsFilterList);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Testimonial" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Testimonialby" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADPortal_Testimonials> ListTestimonials = objTestimonials.ListTestimonials();
            Session["ListTestimonials"] = ListTestimonials;
            return PartialView(GetVirtualPath("_TestimonialsList"), ListTestimonials);
        }

        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Website/Testimonials/" + ViewName + ".cshtml";
            return path;
        }
    }
}