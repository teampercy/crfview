﻿using CRFView.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class AttorneysController : Controller
    {
        ADAttorneys objADAttorneys = new ADAttorneys();
        // GET: ADAttorneys
        public ActionResult Index()
        {
            List<ADAttorneys> lstAttorneys = GetList();
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstAttorneys);
        }
        //Show details on Edit
        public ActionResult AttorneysDetails(int id)
        {
            ADAttorneys objAttorneys = objADAttorneys.ReadAttorney(id);
            BindDropdowns();
            objAttorneys.LawListValue = objAttorneys.LawListId.ToString() + "-" + objAttorneys.LawList;
            return View(GetVirtualPath("AttorneysDetails"), objAttorneys);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult AttorneysSave(ADAttorneys updatedAttorneys)
        {
            string[] LawListValue = updatedAttorneys.LawListValue.Split('-');
            updatedAttorneys.LawListId = Convert.ToInt32(LawListValue[0]);
            updatedAttorneys.LawList = LawListValue[1];
            objADAttorneys.SaveAttorney(updatedAttorneys);
            List<ADAttorneys> lstAttorneys = GetList();
            return PartialView(GetVirtualPath("_AttorneysList"), lstAttorneys);
        }
        //Show blank details on Add
        public ActionResult AttorneysAdd()
        {
            ADAttorneys objAttorneys = new ADAttorneys();
            ModelState.Clear();
            objAttorneys.PKID = 0;
            BindDropdowns();
            return View(GetVirtualPath("AttorneysDetails"), objAttorneys);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value ,string Key)
        {
            List<ADAttorneys> ListAttorneysFilter = new List<ADAttorneys>();
            List<ADAttorneys> ListAttorneys= (List<ADAttorneys>)Session["ListAttorneys"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    ListAttorneysFilter = (from item in ListAttorneys
                                             where item.AttyCode.ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
                else if (Key == "2")
                {
                    ListAttorneysFilter = (from item in ListAttorneys
                                             where item.LawList.ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
                else if (Key == "3")
                {
                    ListAttorneysFilter = (from item in ListAttorneys
                                             where item.AttorneyName.ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }

                else if (Key == "4")
                {
                    ListAttorneysFilter = (from item in ListAttorneys
                                             where item.FirmName.ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
            }
            return View(GetVirtualPath("_AttorneysList"), ListAttorneysFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "AttyCode" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "LawList" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "AttorneyName" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "FirmName" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADAttorneys> lstAttorneys = GetList();
            return PartialView(GetVirtualPath("_AttorneysList"), lstAttorneys);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Collections/Attorneys/" + ViewName + ".cshtml";
            return path;
        }

        //getList
        public List<ADAttorneys> GetList()
        {
            List<ADAttorneys> lstAttorneys = objADAttorneys.ListAttorney();
            Session["ListAttorneys"] = lstAttorneys;
            return  lstAttorneys;
        }

        //Bind dropdowns
        public void BindDropdowns()
        {
          ViewBag.LawList=CommonBindList.GetLawList();
        }
    }
}