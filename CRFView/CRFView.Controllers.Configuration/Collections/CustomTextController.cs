﻿using CRFView.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class CustomTextController : Controller
    {
        ADCustomText objADCustomText = new ADCustomText();
        // GET: ADCustomText
        public ActionResult Index()
        {
            List<ADCustomText> lstCustomText = GetList();
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstCustomText);
        }
        //Show details on Edit
        public ActionResult CustomTextDetails(int id)
        {
            ADCustomText objCustomText = objADCustomText.ReadCustomText(id);
          return View(GetVirtualPath("CustomTextDetails"), objCustomText);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult CustomTextSave(ADCustomText updatedCustomText)
        {
            objADCustomText.SaveCustomText(updatedCustomText);
            List<ADCustomText> lstCustomText = GetList();
            return PartialView(GetVirtualPath("_CustomTextList"), lstCustomText);
        }
        //Show blank details on Add
        public ActionResult CustomTextAdd()
        {
            ADCustomText objCustomText = new ADCustomText();
            ModelState.Clear();
            objCustomText.PKID = 0;
            return View(GetVirtualPath("CustomTextDetails"), objCustomText);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value ,string Key)
        {
            List<ADCustomText> ListCustomTextFilter = new List<ADCustomText>();
            List<ADCustomText> ListCustomText= (List<ADCustomText>)Session["ListCustomText"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    ListCustomTextFilter = (from item in ListCustomText
                                             where item.Description.ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
                else if (Key == "2")
                {
                    ListCustomTextFilter = (from item in ListCustomText
                                             where item.LetterType.ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
                else if (Key == "3")
                {
                    ListCustomTextFilter = (from item in ListCustomText
                                            where item.LetterCode.ToLower().Contains(value.ToLower())
                                            select item).ToList();
                }
                else if (Key == "4")
                {
                    ListCustomTextFilter = (from item in ListCustomText
                                            where item.CustomText.ToLower().Contains(value.ToLower())
                                            select item).ToList();
                }

            }
            return View(GetVirtualPath("_CustomTextList"), ListCustomTextFilter);
        }

        //Set FilterList for search
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Description" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Letter Type" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "Letter Code" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "Custom Text" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADCustomText> lstCustomText = GetList();
            return PartialView(GetVirtualPath("_CustomTextList"), lstCustomText);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Collections/CustomText/" + ViewName + ".cshtml";
            return path;
        }

        //getList
        public List<ADCustomText> GetList()
        {
            List<ADCustomText> lstCustomText = objADCustomText.ListCustomText();
            Session["ListCustomText"] = lstCustomText;
            return  lstCustomText;
        }

       
    }
}