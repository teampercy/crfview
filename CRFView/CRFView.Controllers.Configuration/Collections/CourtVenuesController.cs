﻿using CRFView.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class CourtVenuesController : Controller
    {
        ADCourtVenues objADCourtVenues = new ADCourtVenues();
        // GET: ADCourtVenues
        public ActionResult Index()
        {
            List<ADCourtVenues> lstCourtVenues = GetList();
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstCourtVenues);
        }
        //Show details on Edit
        public ActionResult CourtVenuesDetails(int id)
        {
            ADCourtVenues objCourtVenues = objADCourtVenues.ReadCourtVenues(id);
          return View(GetVirtualPath("CourtVenuesDetails"), objCourtVenues);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult CourtVenuesSave(ADCourtVenues updatedCourtVenues)
        {
            objADCourtVenues.SaveCourtVenues(updatedCourtVenues);
            List<ADCourtVenues> lstCourtVenues = GetList();
            return PartialView(GetVirtualPath("_CourtVenuesList"), lstCourtVenues);
        }
        //Show blank details on Add
        public ActionResult CourtVenuesAdd()
        {
            ADCourtVenues objCourtVenues = new ADCourtVenues();
            ModelState.Clear();
            objCourtVenues.PKID = 0;
            return View(GetVirtualPath("CourtVenuesDetails"), objCourtVenues);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value ,string Key)
        {
            List<ADCourtVenues> ListCourtVenuesFilter = new List<ADCourtVenues>();
            List<ADCourtVenues> ListCourtVenues= (List<ADCourtVenues>)Session["ListCourtVenues"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    ListCourtVenuesFilter = (from item in ListCourtVenues
                                             where item.CourtId.ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
                else if (Key == "2")
                {
                    ListCourtVenuesFilter = (from item in ListCourtVenues
                                             where item.Venue.ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
              
            }
            return View(GetVirtualPath("_CourtVenuesList"), ListCourtVenuesFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Court Code" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Venue" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADCourtVenues> lstCourtVenues = GetList();
            return PartialView(GetVirtualPath("_CourtVenuesList"), lstCourtVenues);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Collections/CourtVenues/" + ViewName + ".cshtml";
            return path;
        }

        //getList
        public List<ADCourtVenues> GetList()
        {
            List<ADCourtVenues> lstCourtVenues = objADCourtVenues.ListCourtVenues();
            Session["ListCourtVenues"] = lstCourtVenues;
            return  lstCourtVenues;
        }

       
    }
}