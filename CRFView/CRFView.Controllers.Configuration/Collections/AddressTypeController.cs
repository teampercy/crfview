﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class AddressTypeController : Controller
    {
        ADAddressType objADAddressType = new ADAddressType();
        // GET: ADAddressType
        public ActionResult Index()
        {
            List<ADAddressType> lstAddressType = GetList();
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstAddressType);
        }
        //Show details on Edit
        public ActionResult AddressTypeDetails(int id)
        {
            ADAddressType objAddressType = objADAddressType.ReadAddressType(id);
            return View(GetVirtualPath("AddressTypeDetails"), objAddressType);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult AddressTypeSave(ADAddressType updatedAddressType)
        {
            objADAddressType.SaveAddressType(updatedAddressType);
            List<ADAddressType> lstAddressType = GetList();
            return PartialView(GetVirtualPath("_AddressTypeList"), lstAddressType);
        }
        //Show blank details on Add
        public ActionResult AddressTypeAdd()
        {
            ADAddressType objAddressType = new ADAddressType();
            ModelState.Clear();
            objAddressType.Id = 0;
            return View(GetVirtualPath("AddressTypeDetails"), objAddressType);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value ,string Key)
        {
            List<ADAddressType> ListAddressTypeFilter = new List<ADAddressType>();
            List<ADAddressType> ListAddressType= (List<ADAddressType>)Session["ListAddressType"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    ListAddressTypeFilter = (from item in ListAddressType
                                                   where item.AddressType.ToLower().Contains(value.ToLower())
                                                   select item).ToList();
                }
            
            }
            return View(GetVirtualPath("_AddressTypeList"), ListAddressTypeFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Address Type" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADAddressType> lstAddressType = GetList();
            return PartialView(GetVirtualPath("_AddressTypeList"), lstAddressType);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Collections/AddressType/" + ViewName + ".cshtml";
            return path;
        }

        //getList
        public List<ADAddressType> GetList()
        {
            List<ADAddressType> lstAddressType = objADAddressType.ListAddressType();
            Session["ListAddressType"] = lstAddressType;
            return  lstAddressType;
        }
    }
}