﻿using CRFView.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class PriorityController : Controller
    {
        ADPriority objADPriority = new ADPriority();
        // GET: ADPriority
        public ActionResult Index()
        {
            List<ADPriority> lstPriority = GetList();
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstPriority);
        }
        //Show details on Edit
        public ActionResult PriorityDetails(int id)
        {
            ADPriority objPriority = objADPriority.ReadPriority(id);
          return View(GetVirtualPath("PriorityDetails"), objPriority);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult PrioritySave(ADPriority updatedPriority)
        {
            objADPriority.SavePriority(updatedPriority);
            List<ADPriority> lstPriority = GetList();
            return PartialView(GetVirtualPath("_PriorityList"), lstPriority);
        }
        //Show blank details on Add
        public ActionResult PriorityAdd()
        {
            ADPriority objPriority = new ADPriority();
            ModelState.Clear();
            objPriority.Id = 0;
            return View(GetVirtualPath("PriorityDetails"), objPriority);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value ,string Key)
        {
            List<ADPriority> ListPriorityFilter = new List<ADPriority>();
            List<ADPriority> ListPriority= (List<ADPriority>)Session["ListPriority"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    ListPriorityFilter = (from item in ListPriority
                                             where item.PriorityCode.ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
                else if (Key == "2")
                {
                    ListPriorityFilter = (from item in ListPriority
                                             where item.PriorityName.ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
              
            }
            return View(GetVirtualPath("_PriorityList"), ListPriorityFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "PriorityCode" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "PriorityName" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADPriority> lstPriority = GetList();
            return PartialView(GetVirtualPath("_PriorityList"), lstPriority);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Collections/Priority/" + ViewName + ".cshtml";
            return path;
        }

        //getList
        public List<ADPriority> GetList()
        {
            List<ADPriority> lstPriority = objADPriority.ListPriority();
            Session["ListPriority"] = lstPriority;
            return  lstPriority;
        }

       
    }
}