﻿using CRFView.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class StatusController : Controller
    {
        ADStatus objADStatus = new ADStatus();
        // GET: ADStatus
        public ActionResult Index()
        {
            List<ADStatus> lstStatus = GetList();
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstStatus);
        }
        //Show details on Edit
        public ActionResult StatusDetails(int id)
        {
            ADStatus objStatus = objADStatus.ReadStatus(id);
            BindDropdowns();
            return View(GetVirtualPath("StatusDetails"), objStatus);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult StatusSave(ADStatus updatedStatus)
        {
            objADStatus.SaveStatus(updatedStatus);
            List<ADStatus> lstStatus = GetList();
            return PartialView(GetVirtualPath("_StatusList"), lstStatus);
        }
        //Show blank details on Add
        public ActionResult StatusAdd()
        {
            ADStatus objStatus = new ADStatus();
            ModelState.Clear();
            BindDropdowns();
            objStatus.CollectionStatusId = 0;
            return View(GetVirtualPath("StatusDetails"), objStatus);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value ,string Key)
        {
            List<ADStatus> ListStatusFilter = new List<ADStatus>();
            List<ADStatus> ListStatus= (List<ADStatus>)Session["ListStatus"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    ListStatusFilter = (from item in ListStatus
                                             where item.CollectionStatus.ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
                else if (Key == "2")
                {
                    ListStatusFilter = (from item in ListStatus
                                             where item.CollectionStatusDescr.ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
                else if (Key == "3")
                {
                    ListStatusFilter = (from item in ListStatus
                                        where item.StatusGroupDescription.ToLower().Contains(value.ToLower())
                                        select item).ToList();
                }

            }
            return View(GetVirtualPath("_StatusList"), ListStatusFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Collection Status" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Description" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "StatusGroup" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADStatus> lstStatus = GetList();
            return PartialView(GetVirtualPath("_StatusList"), lstStatus);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Collections/Status/" + ViewName + ".cshtml";
            return path;
        }

        //getList
        public List<ADStatus> GetList()
        {
            List<ADStatus> lstStatus = objADStatus.ListStatus();
            Session["ListStatus"] = lstStatus;
            return  lstStatus;
        }
        //Bind Dropdowms
        public void BindDropdowns()
        {
            ViewBag.StatusGroupList = CommonBindList.GetCollectionStatusGroupList();
        }

    }
}