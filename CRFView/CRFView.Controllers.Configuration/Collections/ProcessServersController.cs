﻿using CRFView.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class ProcessServersController : Controller
    {
        ADProcessServers objADProcessServers = new ADProcessServers();
        // GET: ADProcessServers
        public ActionResult Index()
        {
            List<ADProcessServers> lstProcessServers = GetList();
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstProcessServers);
        }
        //Show details on Edit
        public ActionResult ProcessServersDetails(int id)
        {
            ADProcessServers objProcessServers = objADProcessServers.ReadProcessServers(id);
          return View(GetVirtualPath("ProcessServersDetails"), objProcessServers);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult ProcessServersSave(ADProcessServers updatedProcessServers)
        {
            objADProcessServers.SaveProcessServers(updatedProcessServers);
            List<ADProcessServers> lstProcessServers = GetList();
            return PartialView(GetVirtualPath("_ProcessServersList"), lstProcessServers);
        }
        //Show blank details on Add
        public ActionResult ProcessServersAdd()
        {
            ADProcessServers objProcessServers = new ADProcessServers();
            ModelState.Clear();
            objProcessServers.PKID = 0;
            return View(GetVirtualPath("ProcessServersDetails"), objProcessServers);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value ,string Key)
        {
            List<ADProcessServers> ListProcessServersFilter = new List<ADProcessServers>();
            List<ADProcessServers> ListProcessServers= (List<ADProcessServers>)Session["ListProcessServers"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    ListProcessServersFilter = (from item in ListProcessServers
                                             where item.ProcessServerNo.ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
                else if (Key == "2")
                {
                    ListProcessServersFilter = (from item in ListProcessServers
                                             where item.MarshalOffice.ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
              
            }
            return View(GetVirtualPath("_ProcessServersList"), ListProcessServersFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Process Server No" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Marshal Office" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADProcessServers> lstProcessServers = GetList();
            return PartialView(GetVirtualPath("_ProcessServersList"), lstProcessServers);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Collections/ProcessServers/" + ViewName + ".cshtml";
            return path;
        }

        //getList
        public List<ADProcessServers> GetList()
        {
            List<ADProcessServers> lstProcessServers = objADProcessServers.ListProcessServers();
            Session["ListProcessServers"] = lstProcessServers;
            return  lstProcessServers;
        }

       
    }
}