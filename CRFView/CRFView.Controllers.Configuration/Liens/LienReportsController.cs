﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class LienReportsController : Controller
    {
        ADLienReports objLienReports = new ADLienReports();

        // GET: LienReports
        public ActionResult Index()
        {
            List<ADLienReports> LienReportsList = objLienReports.ListLienReports();
            Session["ListLienReports"] = LienReportsList;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"), LienReportsList);

        }
        //Show details on Edit
        public ActionResult LienReportsDetails(int id)
        {
            objLienReports = new ADLienReports();
            BindDropdowns();
            objLienReports = objLienReports.ReadLienReports(id);
            return View(GetVirtualPath("LienReportsDetails"), objLienReports);
        }
        //Save Updated or New Details
        [HttpPost]
        public bool LienReportsSave(ADLienReports updatedLienReports)
        {
            return objLienReports.SaveLienReports(updatedLienReports);
        }
        //Show blank details on Add
        public ActionResult LienReportsAdd()
        {
            objLienReports = new ADLienReports();
            objLienReports.ReportId = 0;
            BindDropdowns();
            return View(GetVirtualPath("LienReportsDetails"), objLienReports);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {

            List<ADLienReports> LienReportsList = (List<ADLienReports>)Session["ListLienReports"];
            List<ADLienReports> LienReportsFilterList = null;
            if (value != "" || value != null)
            {
                switch (Key)
                {
                    case "1":
                        LienReportsFilterList = (from item in LienReportsList
                                                 where item.ReportName.ToLower().Contains(value.ToLower())
                                                 select item).ToList();
                        break;
                    case "2":
                        LienReportsFilterList = (from item in LienReportsList
                                                 where item.Description.ToLower().Contains(value.ToLower())
                                                 select item).ToList();
                        break;
                    case "3":
                        LienReportsFilterList = (from item in LienReportsList
                                                 where item.ReportCategory.ToLower().Contains(value.ToLower())
                                                 select item).ToList();
                        break;
                    case "4":
                        LienReportsFilterList = (from item in LienReportsList
                                                 where item.PrintFrom.ToLower().Contains(value.ToLower())
                                                 select item).ToList();
                        break;
                    case "5":
                        LienReportsFilterList = (from item in LienReportsList
                                                 where item.CustomSPName.ToLower().Contains(value.ToLower())
                                                 select item).ToList();
                        break;
                    case "6":
                        LienReportsFilterList = (from item in LienReportsList
                                                 where item.ReportFileName.ToLower().Contains(value.ToLower())
                                                 select item).ToList();
                        break;
                    default:
                        LienReportsFilterList = null;
                        break;
                }
            }
            return View(GetVirtualPath("_LienReportsList"), LienReportsFilterList);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Report Name" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Description" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "Report Category" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "Print From" });
            filterlist.Add(new CommonBindList { Id = 5, Name = "Custom SProc" });
            filterlist.Add(new CommonBindList { Id = 6, Name = "Report FileName" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }
        //Bind Dropdowns
        public void BindDropdowns()
        {
            ViewBag.ReportCategoriesList = CommonBindList.GetReportCategoryList();
            ViewBag.ReportFileNamesList = CommonBindList.GetLienReportFiles();
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADLienReports> ListLienReports = objLienReports.ListLienReports();
            Session["ListLienReports"] = ListLienReports;
            return PartialView(GetVirtualPath("_LienReportsList"), ListLienReports);
        }

        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Liens/LienReports/" + ViewName + ".cshtml";
            return path;
        }
    }
}