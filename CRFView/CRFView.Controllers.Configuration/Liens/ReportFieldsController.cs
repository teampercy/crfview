﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class ReportFieldsController : Controller
    {
        ADReportFields objReportFields = new ADReportFields();

        // GET: ReportFields
        public ActionResult Index()
        {
            List<ADReportFields> ReportFieldsList = objReportFields.ListReportFields();
            Session["ListReportFields"] = ReportFieldsList;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"), ReportFieldsList);

        }
        //Show details on Edit
        public ActionResult ReportFieldsDetails(int id)
        {
            objReportFields = new ADReportFields();
            BindDropdowns();
            objReportFields = objReportFields.ReadReportFields(id);
            return View(GetVirtualPath("ReportFieldsDetails"), objReportFields);
        }
        //Save Updated or New Details
        [HttpPost]
        public bool ReportFieldsSave(ADReportFields updatedReportFields)
        {
            return objReportFields.SaveReportFields(updatedReportFields);
        }
        //Show blank details on Add
        public ActionResult ReportFieldsAdd()
        {
            objReportFields = new ADReportFields();
            objReportFields.Id = 0;
            BindDropdowns();
            return View(GetVirtualPath("ReportFieldsDetails"), objReportFields);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {

            List<ADReportFields> ReportFieldsList = (List<ADReportFields>)Session["ListReportFields"];
            List<ADReportFields> ReportFieldsFilterList = null;
            if (value != "" || value != null)
            {
                switch (Key)
                {
                    case "1":
                        ReportFieldsFilterList = (from item in ReportFieldsList
                                              where item.ReportName.ToLower().Contains(value.ToLower())
                                              select item).ToList();
                        break;
                    case "2":
                        ReportFieldsFilterList = (from item in ReportFieldsList
                                              where item.FieldName.ToLower().Contains(value.ToLower())
                                              select item).ToList();
                        break;
                    default:
                        ReportFieldsFilterList = null;
                        break;
                }
            }
            return View(GetVirtualPath("_ReportFieldsList"), ReportFieldsFilterList);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Report Name" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Field Name" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADReportFields> ListReportFields = objReportFields.ListReportFields();
            Session["ListReportFields"] = ListReportFields;
            return PartialView(GetVirtualPath("_ReportFieldsList"), ListReportFields);
        }
        //Bind dropdowns
        public void BindDropdowns()
        {
            ViewBag.ReportList = CommonBindList.GetLienReportsList();
            ViewBag.FieldList = CommonBindList.GetReportFieldNamesList();
        }


        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Liens/ReportFields/" + ViewName + ".cshtml";
            return path;
        }
    }
}