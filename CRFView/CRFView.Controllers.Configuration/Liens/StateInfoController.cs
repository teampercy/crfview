﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class StateInfoController : Controller
    {
        ADStateInfo objStateInfo = new ADStateInfo();

        // GET: StateInfo
        public ActionResult Index()
        {
            List<ADStateInfo> StateInfoList = objStateInfo.ListStateInfo();
            Session["ListStateInfo"] = StateInfoList;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"), StateInfoList);
        }
        //Show details on Edit
        public ActionResult StateInfoDetails(int id)
        {
            objStateInfo = new ADStateInfo();
            objStateInfo = objStateInfo.ReadStateInfo(id);
            if (objStateInfo.FirstFurnish)
            {
                objStateInfo.Furnish = "FirstFurnish";
            }
            else if (objStateInfo.LastFurnish)
            {
                objStateInfo.Furnish = "LastFurnish";
            }
            else if (objStateInfo.NonStatutory)
            {
                objStateInfo.Furnish = "NonStatutory";
            }
            else if (objStateInfo.Monthly)
            {
                objStateInfo.Furnish = "Monthly";
            }
            else
            { }
            if (objStateInfo.LienSuitStartLF)
            {
                objStateInfo.LienSuit = "LienSuitStartLF";
            }
            else if (objStateInfo.LienSuitStartFileDate)
            {
                objStateInfo.LienSuit = "LienSuitStartFileDate";
            }
            else
            { }
            if (objStateInfo.BondSuitStartLF)
            {
                objStateInfo.BondSuit = "BondSuitStartLF";
            }
            else if (objStateInfo.BondSuitStartFileDate)
            {
                objStateInfo.BondSuit = "BondSuitStartFileDate";
            }
            else
            { }
            ViewBag.JobDeskRulesList = CommonBindList.GetJobDeskRulesList();

            return View(GetVirtualPath("StateInfoDetails"), objStateInfo);
        }
        //Save Updated or New Details
        [HttpPost]
        public bool StateInfoSave(ADStateInfo updatedStateInfo)
        {
            updatedStateInfo.FirstFurnish = false;
            updatedStateInfo.LastFurnish = false;
            updatedStateInfo.NonStatutory = false;
            updatedStateInfo.Monthly = false;
            updatedStateInfo.LienSuitStartLF = false;
            updatedStateInfo.LienSuitStartFileDate = false;
            updatedStateInfo.BondSuitStartLF = false;
            updatedStateInfo.BondSuitStartFileDate = false;

            if (updatedStateInfo.Furnish == "FirstFurnish")
            {
                updatedStateInfo.FirstFurnish = true;
            }
            else if (updatedStateInfo.Furnish == "LastFurnish")
            {
                updatedStateInfo.LastFurnish = true;
            }
            else if (updatedStateInfo.Furnish == "NonStatutory")
            {
                updatedStateInfo.NonStatutory = true;
            }
            else if (updatedStateInfo.Furnish == "Monthly")
            {
                updatedStateInfo.Monthly = true;
            }
            else { }
            if (updatedStateInfo.LienSuit == "LienSuitStartLF")
            {
                updatedStateInfo.LienSuitStartLF = true;
            }
            else if (updatedStateInfo.LienSuit == "LienSuitStartFileDate")
            {
                updatedStateInfo.LienSuitStartFileDate = true;
            }
            else { }
            if (updatedStateInfo.BondSuit == "BondSuitStartLF")
            {
                updatedStateInfo.BondSuitStartLF = true;
            }
            else if (updatedStateInfo.BondSuit == "BondSuitStartFileDate")
            {
                updatedStateInfo.BondSuitStartFileDate = true;
            }
            else { }
            return objStateInfo.SaveStateInfo(updatedStateInfo);
        }
        //Show blank details on Add
        public ActionResult StateInfoAdd()
        {
            objStateInfo = new ADStateInfo();
            objStateInfo.Id = 0;
            ViewBag.JobDeskRulesList = CommonBindList.GetJobDeskRulesList();
            return View(GetVirtualPath("StateInfoDetails"), objStateInfo);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {
            List<ADStateInfo> StateInfoList = (List<ADStateInfo>)Session["ListStateInfo"];
            List<ADStateInfo> StateInfoFilterList = null;
            if (value != "" || value != null)
            {
                switch (Key)
                {
                    case "1":
                        StateInfoFilterList = (from item in StateInfoList
                                               where item.StateName.ToLower().Contains(value.ToLower())
                                               select item).ToList();
                        break;
                    case "2":
                        StateInfoFilterList = (from item in StateInfoList
                                               where item.StateInitials.ToLower().Contains(value.ToLower())
                                               select item).ToList();
                        break;
                    case "3":
                        StateInfoFilterList = (from item in StateInfoList
                                               where item.PrelimReq.ToLower().Contains(value.ToLower())
                                               select item).ToList();
                        break;
                    case "4":
                        StateInfoFilterList = (from item in StateInfoList
                                               where item.PrelimTime.ToLower().Contains(value.ToLower())
                                               select item).ToList();
                        break;
                    case "5":
                        StateInfoFilterList = (from item in StateInfoList
                                               where item.PrelimInfo.ToLower().Contains(value.ToLower())
                                               select item).ToList();
                        break;
                    case "6":
                        StateInfoFilterList = (from item in StateInfoList
                                               where item.MechTime.ToLower().Contains(value.ToLower())
                                               select item).ToList();
                        break;
                    case "7":
                        StateInfoFilterList = (from item in StateInfoList
                                               where item.MechPeriod.ToLower().Contains(value.ToLower())
                                               select item).ToList();
                        break;
                    case "8":
                        StateInfoFilterList = (from item in StateInfoList
                                               where item.MechServe.ToLower().Contains(value.ToLower())
                                               select item).ToList();
                        break;
                    case "9":
                        StateInfoFilterList = (from item in StateInfoList
                                               where item.MechInfo.ToLower().Contains(value.ToLower())
                                               select item).ToList();
                        break;
                    case "10":
                        StateInfoFilterList = (from item in StateInfoList
                                               where item.StopNotice.ToLower().Contains(value.ToLower())
                                               select item).ToList();
                        break;
                    case "11":
                        StateInfoFilterList = (from item in StateInfoList
                                               where item.StopNoticeInfo.ToLower().Contains(value.ToLower())
                                               select item).ToList();
                        break;
                    case "12":
                        StateInfoFilterList = (from item in StateInfoList
                                               where item.MiscInfo.ToLower().Contains(value.ToLower())
                                               select item).ToList();
                        break;
                    default:
                        StateInfoFilterList = null;
                        break;
                }
            }
            return View(GetVirtualPath("_StateInfoList"), StateInfoFilterList);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "StateName" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "StateInitials" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "PrelimReq" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "PrelimTime" });
            filterlist.Add(new CommonBindList { Id = 5, Name = "PrelimInfo" });
            filterlist.Add(new CommonBindList { Id = 6, Name = "MechTime" });
            filterlist.Add(new CommonBindList { Id = 7, Name = "MechPeriod" });
            filterlist.Add(new CommonBindList { Id = 8, Name = "MechServe" });
            filterlist.Add(new CommonBindList { Id = 9, Name = "MechInfo" });
            filterlist.Add(new CommonBindList { Id = 10, Name = "StopNotice" });
            filterlist.Add(new CommonBindList { Id = 11, Name = "StopNoticeInfo" });
            filterlist.Add(new CommonBindList { Id = 12, Name = "MiscInfo" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        
        public ActionResult Refresh()
        {
            List<ADStateInfo> ListStateInfo = objStateInfo.ListStateInfo();
            Session["ListStateInfo"] = ListStateInfo;
            return PartialView(GetVirtualPath("_StateInfoList"), ListStateInfo);
        }
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Liens/StateInfo/" + ViewName + ".cshtml";
            return path;
        }

        // StateNotesHistory functions
        public ActionResult GetNotesList()
        {
            ADStateInfoHistory objStateInfoHistory = new ADStateInfoHistory();
            List<ADStateInfoHistory> StateInfoHistory = objStateInfoHistory.ListStateInfoHistory();
            return PartialView(GetVirtualPath("_Notes"), StateInfoHistory);
        }
        public JsonResult  NotesDetails(int id)
        {
            ADStateInfoHistory objStateInfoHistory = new ADStateInfoHistory();
            objStateInfoHistory = objStateInfoHistory.ReadStateInfoHistory(id);
            return Json(objStateInfoHistory, JsonRequestBehavior.AllowGet);
        }
        [HttpPost]
        public bool NotesSave(string Note)
        {
            ADStateInfoHistory objStateInfoHistory=new ADStateInfoHistory();
            objStateInfoHistory.Note = Note;
            return objStateInfoHistory.SaveStateInfoHistory(objStateInfoHistory);
        }
        public ActionResult NotesRefresh()
        {
            ADStateInfoHistory objStateInfoHistory = new ADStateInfoHistory();
            List<ADStateInfoHistory> StateInfoHistory = objStateInfoHistory.ListStateInfoHistory();
            return PartialView(GetVirtualPath("_NotesList"), StateInfoHistory);
        }
    }
}