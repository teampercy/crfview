﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class JobDeskRulesController : Controller
    {
        ADJobDeskRules objJobDeskRules = new ADJobDeskRules();

        // GET: JobDeskRules
        public ActionResult Index()
        {
            List<ADJobDeskRules> JobDeskRulesList = objJobDeskRules.ListJobDeskRules();
            Session["ListJobDeskRules"] = JobDeskRulesList;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"), JobDeskRulesList);

        }
        //Show details on Edit
        public ActionResult JobDeskRulesDetails(int id)
        {
            objJobDeskRules = new ADJobDeskRules();
            objJobDeskRules = objJobDeskRules.ReadJobDeskRules(id);
            ViewBag.JobDeskList = CommonBindList.GetJobDeskList();
            return View(GetVirtualPath("JobDeskRulesDetails"), objJobDeskRules);
        }
        //Save Updated or New Details
        [HttpPost]
        public bool JobDeskRulesSave(ADJobDeskRules updatedJobDeskRules)
        {
            return objJobDeskRules.SaveJobDeskRules(updatedJobDeskRules);
        }
        //Show blank details on Add
        public ActionResult JobDeskRulesAdd()
        {
            objJobDeskRules = new ADJobDeskRules();
            objJobDeskRules.Id = 0;
            ViewBag.JobDeskList = CommonBindList.GetJobDeskList();
            return View(GetVirtualPath("JobDeskRulesDetails"), objJobDeskRules);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {

            List<ADJobDeskRules> JobDeskRulesList = (List<ADJobDeskRules>)Session["ListJobDeskRules"];
            List<ADJobDeskRules> JobDeskRulesFilterList = null;
            if (value != "" || value != null)
            {
                switch (Key)
                {
                    case "1":
                        JobDeskRulesFilterList = (from item in JobDeskRulesList
                                                        where item.Description.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    case "2":

                        JobDeskRulesFilterList = (from item in JobDeskRulesList
                                                        where item.FromBalance.ToString().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    case "3":

                        JobDeskRulesFilterList = (from item in JobDeskRulesList
                                                        where item.ThruBalance.ToString().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    case "4":

                        JobDeskRulesFilterList = (from item in JobDeskRulesList
                                                        where item.DeskNum.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    default:
                        JobDeskRulesFilterList = null;
                        break;

                }

            }
            return View(GetVirtualPath("_JobDeskRulesList"), JobDeskRulesFilterList);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Description" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "From Balance" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "Thru Balance" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "Desk Num" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

    
        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADJobDeskRules> ListJobDeskRules = objJobDeskRules.ListJobDeskRules();
            Session["ListJobDeskRules"] = ListJobDeskRules;
            return PartialView(GetVirtualPath("_JobDeskRulesList"), ListJobDeskRules);
        }

        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Liens/JobDeskRules/" + ViewName + ".cshtml";
            return path;
        }
    }
}