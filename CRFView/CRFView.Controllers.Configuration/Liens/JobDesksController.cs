﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class JobDesksController : Controller
    {
        ADJobDesks objJobDesks = new ADJobDesks();

        // GET: JobDesks
        public ActionResult Index()
        {
            List<ADJobDesks> JobDesksList = objJobDesks.ListJobDesks();
            Session["ListJobDesks"] = JobDesksList;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"), JobDesksList);

        }
        //Show details on Edit
        public ActionResult JobDesksDetails(int id)
        {
            objJobDesks = new ADJobDesks();
            objJobDesks = objJobDesks.ReadJobDesks(id);
            ViewBag.JobDeskGroupList = CommonBindList.GetJobDeskGroupList();
            return View(GetVirtualPath("JobDesksDetails"), objJobDesks);
        }
        //Save Updated or New Details
        [HttpPost]
        public bool JobDesksSave(ADJobDesks updatedJobDesks)
        {
            return objJobDesks.SaveJobDesks(updatedJobDesks);
        }
        //Show blank details on Add
        public ActionResult JobDesksAdd()
        {
            objJobDesks = new ADJobDesks();
            objJobDesks.DeskId = 0;
            ViewBag.JobDeskGroupList = CommonBindList.GetJobDeskGroupList();
            return View(GetVirtualPath("JobDesksDetails"), objJobDesks);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {

            List<ADJobDesks> JobDesksList = (List<ADJobDesks>)Session["ListJobDesks"];
            List<ADJobDesks> JobDesksFilterList = null;
            if (value != "" || value != null)
            {
                switch (Key)
                {
                    case "1":
                        JobDesksFilterList = (from item in JobDesksList
                                                        where item.DeskNum.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    case "2":

                        JobDesksFilterList = (from item in JobDesksList
                                                        where item.DeskName.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    case "3":

                        JobDesksFilterList = (from item in JobDesksList
                                                        where item.Title.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    case "4":

                        JobDesksFilterList = (from item in JobDesksList
                                                        where item.PhoneNum.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    case "5":

                        JobDesksFilterList = (from item in JobDesksList
                                              where item.FaxNum.ToLower().Contains(value.ToLower())
                                              select item).ToList();
                        break;
                    case "6":

                        JobDesksFilterList = (from item in JobDesksList
                                              where item.Email.ToLower().Contains(value.ToLower())
                                              select item).ToList();
                        break;
                    default:
                        JobDesksFilterList = null;
                        break;

                }

            }
            return View(GetVirtualPath("_JobDesksList"), JobDesksFilterList);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Desk Number" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Desk Name" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "Title" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "Phone Number" });
            filterlist.Add(new CommonBindList { Id = 5, Name = "Fax Number" });
            filterlist.Add(new CommonBindList { Id = 6, Name = "Email" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

    
        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADJobDesks> ListJobDesks = objJobDesks.ListJobDesks();
            Session["ListJobDesks"] = ListJobDesks;
            return PartialView(GetVirtualPath("_JobDesksList"), ListJobDesks);
        }

        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Liens/JobDesks/" + ViewName + ".cshtml";
            return path;
        }
    }
}