﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class PostageRatesController : Controller
    {
        ADPostageRates objPostageRates = new ADPostageRates();

        // GET: PostageRates
        public ActionResult Index()
        {
            List<ADPostageRates> PostageRatesList = objPostageRates.ListPostageRates();
            Session["ListPostageRates"] = PostageRatesList;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"), PostageRatesList);

        }
        //Show details on Edit
        public ActionResult PostageRatesDetails(int id)
        {
            objPostageRates = new ADPostageRates();
            objPostageRates = objPostageRates.ReadPostageRates(id);
            return View(GetVirtualPath("PostageRatesDetails"), objPostageRates);
        }
        //Save Updated or New Details
        [HttpPost]
        public bool PostageRatesSave(ADPostageRates updatedPostageRates)
        {
            return objPostageRates.SavePostageRates(updatedPostageRates);
        }
        //Show blank details on Add
        public ActionResult PostageRatesAdd()
        {
            objPostageRates = new ADPostageRates();
            objPostageRates.Id = 0;
            return View(GetVirtualPath("PostageRatesDetails"), objPostageRates);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {

            List<ADPostageRates> PostageRatesList = (List<ADPostageRates>)Session["ListPostageRates"];
            List<ADPostageRates> PostageRatesFilterList = null;
            if (value != "" || value != null)
            {
                switch (Key)
                {
                    case "1":
                        PostageRatesFilterList = (from item in PostageRatesList
                                              where item.PostageType.ToLower().Contains(value.ToLower())
                                              select item).ToList();
                        break;
                    case "2":
                        PostageRatesFilterList = (from item in PostageRatesList
                                              where item.PostageRate.ToString().Contains(value.ToLower())
                                              select item).ToList();
                        break;
                    default:
                        PostageRatesFilterList = null;
                        break;
                }
            }
            return View(GetVirtualPath("_PostageRatesList"), PostageRatesFilterList);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Postage Type" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Postage Rate" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADPostageRates> ListPostageRates = objPostageRates.ListPostageRates();
            Session["ListPostageRates"] = ListPostageRates;
            return PartialView(GetVirtualPath("_PostageRatesList"), ListPostageRates);
        }

        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Liens/PostageRates/" + ViewName + ".cshtml";
            return path;
        }
    }
}