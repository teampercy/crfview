﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class StateFormFieldsController : Controller
    {
        ADStateFormFields objStateFormFields = new ADStateFormFields();

        // GET: StateFormFields
        public ActionResult Index()
        {
            List<ADStateFormFields> StateFormFieldsList = objStateFormFields.ListStateFormFields();
            Session["ListStateFormFields"] = StateFormFieldsList;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"), StateFormFieldsList);

        }
        //Show details on Edit
        public ActionResult StateFormFieldsDetails(int id)
        {
            objStateFormFields = new ADStateFormFields();
            objStateFormFields = objStateFormFields.ReadStateFormFields(id);
            return View(GetVirtualPath("StateFormFieldsDetails"), objStateFormFields);
        }
        //Save Updated or New Details
        [HttpPost]
        public bool StateFormFieldsSave(ADStateFormFields updatedStateFormFields)
        {
            return objStateFormFields.SaveStateFormFields(updatedStateFormFields);
        }
        //Show blank details on Add
        public ActionResult StateFormFieldsAdd()
        {
            objStateFormFields = new ADStateFormFields();
            objStateFormFields.StateFormFieldID = 0;
            return View(GetVirtualPath("StateFormFieldsDetails"), objStateFormFields);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {

            List<ADStateFormFields> StateFormFieldsList = (List<ADStateFormFields>)Session["ListStateFormFields"];
            List<ADStateFormFields> StateFormFieldsFilterList = null;
            if (value != "" || value != null)
            {
                switch (Key)
                {
                    case "1":
                        StateFormFieldsFilterList = (from item in StateFormFieldsList
                                              where item.FieldName.ToLower().Contains(value.ToLower())
                                              select item).ToList();
                        break;
                    case "2":
                        StateFormFieldsFilterList = (from item in StateFormFieldsList
                                              where item.FriendlyName.ToLower().Contains(value.ToLower())
                                              select item).ToList();
                        break;
                    case "3":
                        StateFormFieldsFilterList = (from item in StateFormFieldsList
                                                     where item.DataType.ToLower().Contains(value.ToLower())
                                                     select item).ToList();
                        break;
                    default:
                        StateFormFieldsFilterList = null;
                        break;
                }
            }
            return View(GetVirtualPath("_StateFormFieldsList"), StateFormFieldsFilterList);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Field Name" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Friendly Name" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "DataType" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADStateFormFields> ListStateFormFields = objStateFormFields.ListStateFormFields();
            Session["ListStateFormFields"] = ListStateFormFields;
            return PartialView(GetVirtualPath("_StateFormFieldsList"), ListStateFormFields);
        }

        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Liens/StateFormFields/" + ViewName + ".cshtml";
            return path;
        }
    }
}