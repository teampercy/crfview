﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class JobDeskGroupController : Controller
    {
        ADJobDeskGroup objJobDeskGroup = new ADJobDeskGroup();

        // GET: JobDeskGroup
        public ActionResult Index()
        {
            List<ADJobDeskGroup> JobDeskGroupList = objJobDeskGroup.ListJobDeskGroup();
            Session["ListJobDeskGroup"] = JobDeskGroupList;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"), JobDeskGroupList);

        }
        //Show details on Edit
        public ActionResult JobDeskGroupDetails(int id)
        {
            objJobDeskGroup = new ADJobDeskGroup();
            objJobDeskGroup = objJobDeskGroup.ReadJobDeskGroup(id);
            ViewBag.JobDeskGroupList = CommonBindList.GetJobDeskGroupList();
            return View(GetVirtualPath("JobDeskGroupDetails"), objJobDeskGroup);
        }
        //Save Updated or New Details
        [HttpPost]
        public bool JobDeskGroupSave(ADJobDeskGroup updatedJobDeskGroup)
        {
            return objJobDeskGroup.SaveJobDeskGroup(updatedJobDeskGroup);
        }
        //Show blank details on Add
        public ActionResult JobDeskGroupAdd()
        {
            objJobDeskGroup = new ADJobDeskGroup();
            objJobDeskGroup.Id = 0;
            ViewBag.JobDeskGroupList = CommonBindList.GetJobDeskGroupList();
            return View(GetVirtualPath("JobDeskGroupDetails"), objJobDeskGroup);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {

            List<ADJobDeskGroup> JobDeskGroupList = (List<ADJobDeskGroup>)Session["ListJobDeskGroup"];
            List<ADJobDeskGroup> JobDeskGroupFilterList = null;
            if (value != "" || value != null)
            {
                        JobDeskGroupFilterList = (from item in JobDeskGroupList
                                                        where item.DeskGroupName.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
            }
            return View(GetVirtualPath("_JobDeskGroupList"), JobDeskGroupFilterList);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "DeskGroup Name" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

    
        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADJobDeskGroup> ListJobDeskGroup = objJobDeskGroup.ListJobDeskGroup();
            Session["ListJobDeskGroup"] = ListJobDeskGroup;
            return PartialView(GetVirtualPath("_JobDeskGroupList"), ListJobDeskGroup);
        }

        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Liens/JobDeskGroup/" + ViewName + ".cshtml";
            return path;
        }
    }
}