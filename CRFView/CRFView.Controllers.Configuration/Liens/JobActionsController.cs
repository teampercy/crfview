﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CRFView.Adapters;

namespace CRFView.Controllers
{
    public class JobActionsController : Controller
    {
        ADJobActions objJobActions= new ADJobActions();
        List<ADJobActions> ResultListJobActions= new List<ADJobActions>();
        // GET: JobViewDesks
        public ActionResult Index()
        {
            ResultListJobActions= GetList();
            return PartialView(GetVirtualPath("Index"), ResultListJobActions);
        }
        //Show details on Edit
        public ActionResult JobActionsDetails(int id)
        {
            ADJobActions JobActionsinfo = objJobActions.ReadJobActions(id);
            BindDropdowns();
            if(JobActionsinfo.IsVerifier)
            {
                JobActionsinfo.ActionType = "IsVerifier";
            }
            else if (JobActionsinfo.IsLienVerifier)
            {
                JobActionsinfo.ActionType = "IsLienVerifier";
            }
            else if (JobActionsinfo.IsDataEntry)
            {
                JobActionsinfo.ActionType = "IsDataEntry";
            }
            else if (JobActionsinfo.IsClient)
            {
                JobActionsinfo.ActionType = "IsClient";
            }
            else if (JobActionsinfo.IsSystem)
            {
                JobActionsinfo.ActionType = "IsSystem";
            }
            else if (JobActionsinfo.IsJobView)
            {
                JobActionsinfo.ActionType = "IsJobView";
            }
            else { }
            return View(GetVirtualPath("JobActionsDetails"), JobActionsinfo);
        }
     
        //Save Updated or New Details
        [HttpPost]
        public bool JobActionsSave(ADJobActions updatedJobActions)
        {
            return objJobActions.SaveJobActions(updatedJobActions);
        }
        //Show blank details on Add
        public ActionResult JobActionsAdd()
        {
            ADJobActions objJobActions= new ADJobActions();
            ModelState.Clear();
            objJobActions.Id = 0;
            BindDropdowns();
            return View(GetVirtualPath("JobActionsDetails"), objJobActions);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {
            List<ADJobActions> FilterResult = new List<ADJobActions>();
            List<ADJobActions> ListJobActions = (List<ADJobActions>)Session["ListJobActions"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    FilterResult = (from item in ListJobActions
                                    where item.Id.ToString().ToLower().Contains(value.ToLower())
                                    select item).ToList();
                }
                else if (Key == "2")
                {
                    FilterResult = (from item in ListJobActions
                                    where item.Description.ToLower().Contains(value.ToLower())
                                    select item).ToList();
                }
                else if (Key == "3")
                {
                    FilterResult = (from item in ListJobActions
                                    where item.DeskNum.ToLower().Contains(value.ToLower())
                                    select item).ToList();
                }
                else if (Key == "4")
                {
                    FilterResult = (from item in ListJobActions
                                    where item.JobStatus.ToLower().Contains(value.ToLower())
                                    select item).ToList();
                }
                else if (Key == "5")
                {
                    FilterResult = (from item in ListJobActions
                                    where item.NextCall.ToString().ToLower().Contains(value.ToLower())
                                    select item).ToList();
                }
                else if (Key == "6")
                {
                    FilterResult = (from item in ListJobActions
                                    where item.JobViewDesk.ToLower().Contains(value.ToLower())
                                    select item).ToList();
                }
                else if (Key == "7")
                {
                    FilterResult = (from item in ListJobActions
                                    where item.JobviewStatus.ToLower().Contains(value.ToLower())
                                    select item).ToList();
                }
            }

            return View(GetVirtualPath("_JobActionsList"), FilterResult);
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            ResultListJobActions= GetList();
            return PartialView(GetVirtualPath("_JobActionsList"), ResultListJobActions);
        }
       
        // Get List from database 
        public List<ADJobActions> GetList()
        {
            List<ADJobActions> lstJobActions= objJobActions.ListJobActions();
            Session["ListJobActions"] = lstJobActions;
            SetFilterList();
            return lstJobActions;
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Id" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Action" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "New Desk" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "New Status" });
            filterlist.Add(new CommonBindList { Id = 5, Name = "NextCall" });
            filterlist.Add(new CommonBindList { Id = 6, Name = "JobViewDesk" });
            filterlist.Add(new CommonBindList { Id = 7, Name = "JobViewStatus" });
          
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }
        //Bind dropdowns
        public void BindDropdowns()
        {
            ViewBag.JobDeskList = CommonBindList.GetJobDeskList();
            ViewBag.JobStatusList = CommonBindList.GetJobStatusList();
            ViewBag.JobViewDesksList = CommonBindList.GetJobViewDesksList();
            ViewBag.JobViewStatusList = CommonBindList.GetJobViewStatusList();
            ViewBag.JobActionCategoriesList = CommonBindList.GetJobActionCategoriesList();
            ViewBag.BillableEventTypeList = CommonBindList.GetBillableEventTypeList();
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Liens/JobActions/" + ViewName + ".cshtml";
            return path;
        }
    }
}