﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class ClientSignerController : Controller
    {
        ADClientSigner objClientSigner = new ADClientSigner();

        // GET: ClientSigner
        public ActionResult Index()
        {
            List<ADClientSigner> ClientSignerList = objClientSigner.ListClientSigner();
            Session["ListClientSigner"] = ClientSignerList;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"), ClientSignerList);

        }
        //Show details on Edit
        public ActionResult ClientSignerDetails(int id)
        {
            objClientSigner = new ADClientSigner();
            objClientSigner = objClientSigner.ReadClientSigner(id);
            ViewBag.ClientList = CommonBindList.GetClientList();
            return View(GetVirtualPath("ClientSignerDetails"), objClientSigner);
        }
        //Save Updated or New Details
        [HttpPost]
        public bool ClientSignerSave(ADClientSigner updatedClientSigner)
        {
            return objClientSigner.SaveClientSigner(updatedClientSigner);
        }
        //Show blank details on Add
        public ActionResult ClientSignerAdd()
        {
            objClientSigner = new ADClientSigner();
            objClientSigner.Id = 0;
            ViewBag.ClientList = CommonBindList.GetClientList();
            return View(GetVirtualPath("ClientSignerDetails"), objClientSigner);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {

            List<ADClientSigner> ClientSignerList = (List<ADClientSigner>)Session["ListClientSigner"];
            List<ADClientSigner> ClientSignerFilterList = null;
            if (value != "" || value != null)
            {
                switch (Key)
                {
                    case "1":
                        ClientSignerFilterList = (from item in ClientSignerList
                                                        where item.ClientCode.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    case "2":

                        ClientSignerFilterList = (from item in ClientSignerList
                                                        where item.Signer.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    case "3":

                        ClientSignerFilterList = (from item in ClientSignerList
                                                        where item.SignerTitle.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
                        break;

                    default:
                        ClientSignerFilterList = null;
                        break;

                }

            }
            return View(GetVirtualPath("_ClientSignerList"), ClientSignerFilterList);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Client Code" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Signer" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "Signer Title" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

    
        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADClientSigner> ListClientSigner = objClientSigner.ListClientSigner();
            Session["ListClientSigner"] = ListClientSigner;
            return PartialView(GetVirtualPath("_ClientSignerList"), ListClientSigner);
        }

        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Liens/ClientSigner/" + ViewName + ".cshtml";
            return path;
        }
    }
}