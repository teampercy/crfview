﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class StateFormsController : Controller
    {
        ADStateForms objStateForms = new ADStateForms();

        // GET: StateForms
        public ActionResult Index()
        {
            List<ADStateForms> StateFormsList = objStateForms.ListStateForms();
            Session["ListStateForms"] = StateFormsList;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"), StateFormsList);

        }
        //Show details on Edit
        public ActionResult StateFormsDetails(int id)
        {
            objStateForms = new ADStateForms();
            objStateForms = objStateForms.ReadStateForms(id);
            if(objStateForms.IsPublic)
            {
                objStateForms.JobType = "Public";
            }
            else if(objStateForms.IsPrivate)
            {
                objStateForms.JobType = "Private";
            }
            else if (objStateForms.IsResidential)
            {
                objStateForms.JobType = "Residential";
            }
            else if (objStateForms.IsFederal)
            {
                objStateForms.JobType = "Federal";
            }
            else if (objStateForms.IsRental)
            {
                objStateForms.JobType = "Rental";
            }
           else
            { }
            return View(GetVirtualPath("StateFormsDetails"), objStateForms);
        }
        //Save Updated or New Details
        [HttpPost]
        public bool StateFormsSave(ADStateForms updatedStateForms)
        {
            if (updatedStateForms.JobType== "Public")
            {
                updatedStateForms.IsPublic = true;
                updatedStateForms.IsPrivate = false;
                updatedStateForms.IsResidential = false;
                updatedStateForms.IsFederal = false;
                updatedStateForms.IsRental = false;
            }
            else if (updatedStateForms.JobType == "Private")
            {
                updatedStateForms.IsPublic = false;
                updatedStateForms.IsPrivate = true;
                updatedStateForms.IsResidential = false;
                updatedStateForms.IsFederal = false;
                updatedStateForms.IsRental = false;
            }
            else if (updatedStateForms.JobType == "Residential")
            {
                updatedStateForms.IsPublic = false;
                updatedStateForms.IsPrivate = false;
                updatedStateForms.IsResidential = true;
                updatedStateForms.IsFederal = false;
                updatedStateForms.IsRental = false;
            }
            else if (updatedStateForms.JobType == "Federal")
            {
                updatedStateForms.IsPublic = false;
                updatedStateForms.IsPrivate = false;
                updatedStateForms.IsResidential = false;
                updatedStateForms.IsFederal = true;
                updatedStateForms.IsRental = false;
            }
            else if (updatedStateForms.JobType == "Rental")
            {
                updatedStateForms.IsPublic = false;
                updatedStateForms.IsPrivate = false;
                updatedStateForms.IsResidential = false;
                updatedStateForms.IsFederal = false;
                updatedStateForms.IsRental = true;
            }
            else
            {
                updatedStateForms.IsPublic = false;
                updatedStateForms.IsPrivate = false;
                updatedStateForms.IsResidential = false;
                updatedStateForms.IsFederal = false;
                updatedStateForms.IsRental = false;
            }
            return objStateForms.SaveStateForms(updatedStateForms);
        }
        //Show blank details on Add
        public ActionResult StateFormsAdd()
        {
            objStateForms = new ADStateForms();
            objStateForms.Id = 0;
            return View(GetVirtualPath("StateFormsDetails"), objStateForms);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {

            List<ADStateForms> StateFormsList = (List<ADStateForms>)Session["ListStateForms"];
            List<ADStateForms> StateFormsFilterList = null;
            if (value != "" || value != null)
            {
                switch (Key)
                {
                    case "1":
                        StateFormsFilterList = (from item in StateFormsList
                                              where item.StateCode.ToLower().Contains(value.ToLower())
                                              select item).ToList();
                        break;
                    case "2":
                        StateFormsFilterList = (from item in StateFormsList
                                              where item.FormType.ToLower().Contains(value.ToLower())
                                              select item).ToList();
                        break;
                    case "3":
                        StateFormsFilterList = (from item in StateFormsList
                                                where item.FormCode.ToLower().Contains(value.ToLower())
                                                select item).ToList();
                        break;
                    case "4":
                        StateFormsFilterList = (from item in StateFormsList
                                                where item.Description.ToLower().Contains(value.ToLower())
                                                select item).ToList();
                        break;
                    case "5":
                        StateFormsFilterList = (from item in StateFormsList
                                                where item.IsPublic.ToString().ToLower().Contains(value.ToLower())
                                                select item).ToList();
                        break;
                    case "6":
                        StateFormsFilterList = (from item in StateFormsList
                                                where item.IsPrivate.ToString().ToLower().Contains(value.ToLower())
                                                select item).ToList();
                        break;
                    case "7":
                        StateFormsFilterList = (from item in StateFormsList
                                                where item.IsResidential.ToString().ToLower().Contains(value.ToLower())
                                                select item).ToList();
                        break;
                    case "8":
                        StateFormsFilterList = (from item in StateFormsList
                                                where item.IsFederal.ToString().ToLower().Contains(value.ToLower())
                                                select item).ToList();
                        break;
                    case "9":
                        StateFormsFilterList = (from item in StateFormsList
                                                where item.IsRental.ToString().ToLower().Contains(value.ToLower())
                                                select item).ToList();
                        break;
                    case "10":
                        StateFormsFilterList = (from item in StateFormsList
                                                where item.IsNonStat.ToString().ToLower().Contains(value.ToLower())
                                                select item).ToList();
                        break;
                    case "11":
                        StateFormsFilterList = (from item in StateFormsList
                                                where item.IsTX60Day.ToString().ToLower().Contains(value.ToLower())
                                                select item).ToList();
                        break;
                    case "12":
                        StateFormsFilterList = (from item in StateFormsList
                                                where item.IsTX90Day.ToString().ToLower().Contains(value.ToLower())
                                                select item).ToList();
                        break;
                    case "13":
                        StateFormsFilterList = (from item in StateFormsList
                                                where item.IsWaiverUpdated.ToString().ToLower().Contains(value.ToLower())
                                                select item).ToList();
                        break;
                    default:
                        StateFormsFilterList = null;
                        break;
                }
            }
            return View(GetVirtualPath("_StateFormsList"), StateFormsFilterList);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "StateCode" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "FormType" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "FormCode" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "Description" });
            filterlist.Add(new CommonBindList { Id = 5, Name = "Public" });
            filterlist.Add(new CommonBindList { Id = 6, Name = "Private" });
            filterlist.Add(new CommonBindList { Id = 7, Name = "Residential" });
            filterlist.Add(new CommonBindList { Id = 8, Name = "Federal" });
            filterlist.Add(new CommonBindList { Id = 9, Name = "Rental" });
            filterlist.Add(new CommonBindList { Id = 10, Name = "NonStat" });
            filterlist.Add(new CommonBindList { Id = 11, Name = "TX60" });
            filterlist.Add(new CommonBindList { Id = 12, Name = "TX90" });
            filterlist.Add(new CommonBindList { Id = 13, Name = "WaiverUpdated" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADStateForms> ListStateForms = objStateForms.ListStateForms();
            Session["ListStateForms"] = ListStateForms;
            return PartialView(GetVirtualPath("_StateFormsList"), ListStateForms);
        }
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Liens/StateForms/" + ViewName + ".cshtml";
            return path;
        }
    }
}