﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class MailDeliveredController : Controller
    {
        ADMailDelivered objMailDelivered = new ADMailDelivered();

        // GET: MailDelivered
        public ActionResult Index()
        {
            List<ADMailDelivered> MailDeliveredList = objMailDelivered.ListMailDelivered();
            Session["ListMailDelivered"] = MailDeliveredList;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"), MailDeliveredList);

        }
        //Show details on Edit
        public ActionResult MailDeliveredDetails(int id)
        {
            objMailDelivered = new ADMailDelivered();
            BindDropdowns();
            objMailDelivered = objMailDelivered.ReadMailDelivered(id);
            return View(GetVirtualPath("MailDeliveredDetails"), objMailDelivered);
        }
        //Save Updated or New Details
        [HttpPost]
        public bool MailDeliveredSave(ADMailDelivered updatedMailDelivered)
        {
            return objMailDelivered.SaveMailDelivered(updatedMailDelivered);
        }
        //Show blank details on Add
        public ActionResult MailDeliveredAdd()
        {
            objMailDelivered = new ADMailDelivered();
            objMailDelivered.Id = 0;
            BindDropdowns();
            return View(GetVirtualPath("MailDeliveredDetails"), objMailDelivered);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {

            List<ADMailDelivered> MailDeliveredList = (List<ADMailDelivered>)Session["ListMailDelivered"];
            List<ADMailDelivered> MailDeliveredFilterList = null;
            if (value != "" || value != null)
            {
                switch (Key)
                {
                    case "1":
                        MailDeliveredFilterList = (from item in MailDeliveredList
                                                        where item.Description.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    case "2":

                        MailDeliveredFilterList = (from item in MailDeliveredList
                                                        where item.LegalPartyType.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    case "3":

                        MailDeliveredFilterList = (from item in MailDeliveredList
                                                        where item.ResultDescription.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    case "4":

                        MailDeliveredFilterList = (from item in MailDeliveredList
                                                        where item.ActionDescription.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    default:
                        MailDeliveredFilterList = null;
                        break;

                }

            }
            return View(GetVirtualPath("_MailDeliveredList"), MailDeliveredFilterList);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Description" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "LegalParty Type" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "Result" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "Action Description" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

    
        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADMailDelivered> ListMailDelivered = objMailDelivered.ListMailDelivered();
            Session["ListMailDelivered"] = ListMailDelivered;
            return PartialView(GetVirtualPath("_MailDeliveredList"), ListMailDelivered);
        }

        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Liens/MailDelivered/" + ViewName + ".cshtml";
            return path;
        }

        public void BindDropdowns()
        {
            ViewBag.MailDeliveredJobActionList = CommonBindList.GetMailDeliveredJobActionList();
            ViewBag.MailDeliveredResultList = CommonBindList.GetMailDeliveredResultList();
        }
    }
}