Namespace ClientView.Lien
    Public Class NewJob
        Inherits CRFDB.TABLES.BatchJob
        Dim ObjectState As New ObjectState
        Dim _clientinfo As CRM.ClientView.ClientInfoForUser
        Public CurrentUser As Users.CurrentUser
        Public Sub New(ByVal auser As Users.CurrentUser)
            MyBase.New()
            Me.CurrentUser = auser
            _clientinfo = New CRM.ClientView.ClientInfoForUser(Me.CurrentUser.Id)
        End Sub
        Public ReadOnly Property ClientInfo() As CRM.ClientView.ClientInfoForUser
            Get
                Return _clientinfo
            End Get
        End Property
        Public Function SelectById(ByVal keyvalue As String) As Boolean
            Me.ObjectState.IsValid = DBO.Read(keyvalue, Me)
            Me.ObjectState.LastErrorMessage = DBO.LastError
            Return Me.ObjectState.IsValid
        End Function
        Public Function Insert() As Boolean
            Me.ObjectState.IsValid = DBO.Create(Me)
            Me.ObjectState.LastErrorMessage = DBO.LastError
            Return Me.ObjectState.IsValid
        End Function
        Public Function Delete() As Boolean
            Me.ObjectState.IsValid = DBO.Delete(Me)
            Me.ObjectState.LastErrorMessage = DBO.LastError
            Return Me.ObjectState.IsValid
        End Function
        Public Function Update() As Boolean
            Me.ObjectState.IsValid = DBO.Update(Me)
            Me.ObjectState.LastErrorMessage = DBO.LastError
            Return Me.ObjectState.IsValid
        End Function
        Public Function GetClientCustomerById(ByVal acustid As String) As CRFDB.TABLES.ClientCustomer
            Dim mycust As New CRFDB.TABLES.ClientCustomer
            DBO.Read(acustid, mycust)
            Return mycust
        End Function
        Public Function GetClientCustomersByName(ByVal aclientid As String, ByVal afilter As String) As DAL.COMMON.TableView
            Dim mysproc As New CRFDB.SPROCS.uspbo_Jobs_GetCustomerList
            mysproc.ClientId = aclientid
            mysproc.Name = afilter
            Return DBO.GetTableView(mysproc)
        End Function
        Public Function GetClienListforUser() As DAL.COMMON.TableView
            Dim mysproc As New CRFDB.SPROCS.uspbo_Clientview_GetClientListForUser
            mysproc.UserId = Me.CurrentUser.Id
            Return DBO.GetTableView(mysproc)
        End Function

        Public Function GetClientCustomersByRef(ByVal aclientid As String, ByVal afilter As String) As DAL.COMMON.TableView
            Dim mysproc As New CRFDB.SPROCS.uspbo_ClientView_GetCustomerRefList
            mysproc.ClientId = aclientid
            mysproc.Reference = afilter
            Return DBO.GetTableView(mysproc)
        End Function
        Public Function GetClientGCSByName(ByVal aclientid As String, ByVal afilter As String) As DAL.COMMON.TableView
            Dim mysproc As New CRFDB.SPROCS.uspbo_Jobs_GetGeneralContractorList
            mysproc.ClientId = aclientid
            mysproc.Name = afilter
            Return DBO.GetTableView(mysproc)
        End Function
        Public Function GetClientGCById(ByVal agenid As String) As CRFDB.TABLES.ClientGeneralContractor
            Dim mygc As New CRFDB.TABLES.ClientGeneralContractor
            DBO.Read(agenid, mygc)
            Return mygc
        End Function
        Public Function GetLegalPartiesForBatchJob(ByVal abatchjobid As String) As DAL.COMMON.TableView
            Dim myquery As New DAL.COMMON.Query(New CRFDB.TABLES.BatchJobLegalParties)
            Dim mytable As New CRFDB.TABLES.BatchJobLegalParties
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, CRFDB.TABLES.BatchJobLegalParties.ColumnNames.BatchJobId, DAL.COMMON.Conditions._IsEqual, abatchjobid)
            Return DBO.GetTableView(myquery.BuildQuery)
        End Function
    End Class
End Namespace
