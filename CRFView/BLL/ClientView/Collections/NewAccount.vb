Namespace ClientView.Collection
    Public Class NewAccount
        Inherits CRFDB.TABLES.BatchDebtAccount
        Dim ObjectState As New ObjectState
        Dim _clientid As Integer
        Dim _contractid As Integer
        Public CurrentUser As Users.CurrentUser
        Dim _clientinfo As CRM.ClientView.ClientInfoForUser
        Public Sub New(ByVal auser As Users.CurrentUser)
            MyBase.New()
            Me.CurrentUser = auser
            _clientinfo = New CRM.ClientView.ClientInfoForUser(Me.CurrentUser.Id)

        End Sub
        Public ReadOnly Property ClientInfo() As CRM.ClientView.ClientInfoForUser
            Get
                Return _clientinfo
            End Get
        End Property
        Public Function Insert() As Boolean
            Me.ObjectState.IsValid = DBO.Create(Me)
            Me.ObjectState.LastErrorMessage = DBO.LastError
            Return Me.ObjectState.IsValid
        End Function
        Public Function Delete() As Boolean
            Me.ObjectState.IsValid = DBO.Delete(Me)
            Me.ObjectState.LastErrorMessage = DBO.LastError
            Return Me.ObjectState.IsValid
        End Function
        Public Function Update() As Boolean
            Me.ObjectState.IsValid = DBO.Update(Me)
            Me.ObjectState.LastErrorMessage = DBO.LastError
            Return Me.ObjectState.IsValid
        End Function
        Public Function SelectById(ByVal keyvalue As String) As Boolean
            Me.ObjectState.IsValid = DBO.Read(keyvalue, Me)
            Me.ObjectState.LastErrorMessage = DBO.LastError
            Return Me.ObjectState.IsValid

        End Function
      End Class
End Namespace
