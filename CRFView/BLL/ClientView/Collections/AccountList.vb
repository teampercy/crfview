Namespace ClientView.Collection
    Public Class AccountList
        Inherits Common.DoListPage
        Public SprocParams As New CRFDB.SPROCS.uspbo_ClientView_GetAccountLists
        Dim _hasservice As String = True
        Dim _clientid As Integer
        Dim _contractid As Integer
        Dim _clientinfo As CRM.ClientView.ClientInfoForUser
        Public Sub New(ByVal auser As Users.CurrentUser)
            MyBase.New()
            Me.CurrentUser = auser
            _clientinfo = New CRM.ClientView.ClientInfoForUser(Me.CurrentUser.Id)

        End Sub
        Public ReadOnly Property ClientInfo() As CRM.ClientView.ClientInfoForUser
            Get
                Return _clientinfo
            End Get
        End Property
        Public Overrides Function GetView() As DAL.COMMON.TableView
            Dim myds As DataSet = DBO.GetDataSet(SprocParams)
            Return New DAL.COMMON.TableView(myds.Tables(0), "")

        End Function
    End Class
End Namespace
