Imports CRF.BLL
Namespace Accounting
    Public Class Liens
        Public Shared Function GetClientList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Dim sql As String = "Select ClientId As ClientId,  ClientCode, ClientCode + '-' + ClientName As ClientName from Client Where IsLienClient = 1 and IsBranch = 0"
            Return DBO.GetTableView(sql)

        End Function
        Public Shared Function GetParentClientList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView

            Dim sql As String = "Select ClientId As ClientId,  ClientCode, ClientCode + '-' + ClientName As ClientName from Client Where IsLienClient = 1 and IsBranch = 0 and ClientId = ParentClientId"
            Return DBO.GetTableView(sql)

        End Function
        Public Shared Function CreateInvoice(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.uspbo_Accounting_CreateLienInvoice) As Boolean
            DBO.ExecNonQuery(asproc)
            Return True

        End Function
        Public Shared Function GetInvoices(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.uspbo_Accounting_GetClientFinDocs, ByVal printmode As Common.PrintMode) As DAL.COMMON.TableView
            Return DBO.GetTableView(asproc)

        End Function
        Public Shared Function GetRecaps(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.uspbo_Accounting_GetInvoiceRecaps) As DataSet
            Return DBO.GetDataSet(asproc)

        End Function


        Public Shared Function PrintInvoice(ByVal auser As Users.CurrentUser, ByVal aclientfindocid As Integer, ByVal printmode As Common.PrintMode, ByVal afilename As String) As String

            Dim myreport As New BLL.Common.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\Acc-Liens.xml"
            myreport.ReportName = "Liens Invoice"
            myreport.FilePrefix = afilename & "-I"
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.PrintMode = printmode

            Dim myquery As New DAL.COMMON.Query(New CRFDB.VIEWS.vwClientFinDocDetail)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, CRFDB.VIEWS.vwClientFinDocDetail.ColumnNames.ClientFinDocId, DAL.COMMON.Conditions._IsEqual, aclientfindocid)
            myreport.DataView = DBO.GetTableView(myquery.BuildQuery)

            myreport.RenderReport()
            Return myreport.ReportFileName

        End Function
        Public Shared Function PrintInvoiceStatement(ByVal auser As Users.CurrentUser, ByVal aclientfindocid As Integer, ByVal printmode As Common.PrintMode, ByVal afilename As String) As String

            Dim myreport As New BLL.Common.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\Acc-Liens.xml"
            myreport.ReportName = "Liens Statement"
            myreport.FilePrefix = afilename & "-S"
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.PrintMode = printmode

            Dim myquery As New DAL.COMMON.Query(New CRFDB.VIEWS.vwClientFinDocStatement)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, CRFDB.VIEWS.vwClientFinDocDetail.ColumnNames.ClientFinDocId, DAL.COMMON.Conditions._IsEqual, aclientfindocid)
            myreport.DataView = DBO.GetTableView(myquery.BuildQuery)

            myreport.RenderReport()

            Return myreport.ReportFileName

        End Function
        Public Shared Function PrintInvoiceStatementByBranch(ByVal auser As Users.CurrentUser, ByVal aclientfindocid As Integer, ByVal printmode As Common.PrintMode, ByVal afilename As String) As String

            Dim myreport As New BLL.Common.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\Acc-Liens.xml"
            myreport.ReportName = "Liens Statement By Branch"
            myreport.FilePrefix = afilename & "-S"
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.PrintMode = printmode

            Dim myquery As New DAL.COMMON.Query(New CRFDB.VIEWS.vwClientFinDocStatement)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, CRFDB.VIEWS.vwClientFinDocDetail.ColumnNames.ClientFinDocId, DAL.COMMON.Conditions._IsEqual, aclientfindocid)
            myreport.DataView = DBO.GetTableView(myquery.BuildQuery)

            myreport.RenderReport()

            Return myreport.ReportFileName

        End Function
        Public Shared Function PrintInvoiceClientSummary(ByVal auser As Users.CurrentUser, ByVal aclientfindocid As Integer, ByVal printmode As Common.PrintMode, ByVal afilename As String) As String

            Dim myreport As New BLL.Common.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\Acc-Liens.xml"
            myreport.ReportName = "Client Summary"
            myreport.FilePrefix = afilename & "-S"
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.PrintMode = printmode

            Dim myquery As New DAL.COMMON.Query(New CRFDB.VIEWS.vwInvoiceDetailSummary)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, CRFDB.VIEWS.vwInvoiceDetailSummary.ColumnNames.Id, DAL.COMMON.Conditions._IsEqual, aclientfindocid)
            myreport.DataView = DBO.GetTableView(myquery.BuildQuery)

            myreport.RenderReport()

            Return myreport.ReportFileName

        End Function
        Public Shared Function PrintInvoiceBranchSummary(ByVal auser As Users.CurrentUser, ByVal aclientfindocid As Integer, ByVal printmode As Common.PrintMode, ByVal afilename As String) As String

            Dim myreport As New BLL.Common.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\Acc-Liens.xml"
            myreport.ReportName = "Branch Summary"
            myreport.FilePrefix = afilename & "-S"
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.PrintMode = printmode

            Dim myquery As New DAL.COMMON.Query(New CRFDB.VIEWS.vwInvoiceDetailSummary)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, CRFDB.VIEWS.vwInvoiceDetailSummary.ColumnNames.Id, DAL.COMMON.Conditions._IsEqual, aclientfindocid)
            myreport.DataView = DBO.GetTableView(myquery.BuildQuery)

            myreport.RenderReport()

            Return myreport.ReportFileName

        End Function

        Public Shared Function PrintInvoiceBranchDetail(ByVal auser As Users.CurrentUser, ByVal aclientfindocid As Integer, ByVal printmode As Common.PrintMode, ByVal afilename As String) As String

            Dim myreport As New BLL.Common.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\Acc-Liens.xml"
            myreport.ReportName = "Branch Detail Summary"
            myreport.FilePrefix = afilename & "-S"
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.PrintMode = printmode

            Dim myquery As New DAL.COMMON.Query(New CRFDB.VIEWS.vwInvoiceDetailSummary)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, CRFDB.VIEWS.vwInvoiceDetailSummary.ColumnNames.Id, DAL.COMMON.Conditions._IsEqual, aclientfindocid)
            myreport.DataView = DBO.GetTableView(myquery.BuildQuery)

            myreport.RenderReport()

            Return myreport.ReportFileName

        End Function
        Public Shared Function ExportInvoiceBranchSummary(ByVal auser As Users.CurrentUser, ByVal aclientfindocid As Integer, ByVal afilename As String) As String

            Dim myquery As New DAL.COMMON.Query(New CRFDB.VIEWS.vwInvoiceBranchSummary)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, CRFDB.VIEWS.vwInvoiceBranchSummary.ColumnNames.ClientFinDocId, DAL.COMMON.Conditions._IsEqual, aclientfindocid)
            Dim mysql As String = myquery.BuildQuery
            Dim myview As DAL.COMMON.TableView = DBO.GetTableView(mysql)

            If myview.Count < 1 Then
                Return True
            End If

            Dim myfilepath As String = Common.FileOps.GetFileName(auser, afilename & "-S-BranchSummary", ".CSV", True)
            myview.ExportColumns.Add(vwInvoiceBranchSummary.ColumnNames.BranchNumber)
            myview.ExportColumns.Add(vwInvoiceBranchSummary.ColumnNames.Price)
            myview.ExportToCSV(myfilepath)

            Return myfilepath

        End Function
        Public Shared Function ExportInvoiceBranchDetail(ByVal auser As Users.CurrentUser, ByVal aclientfindocid As Integer, ByVal afilename As String) As String
            Dim myquery As New DAL.COMMON.Query(New CRFDB.VIEWS.vwInvoiceBranchDetailSummary)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, CRFDB.VIEWS.vwInvoiceBranchDetailSummary.ColumnNames.ClientFinDocId, DAL.COMMON.Conditions._IsEqual, aclientfindocid)
            Dim mysql As String = myquery.BuildQuery
            Dim myview As DAL.COMMON.TableView = DBO.GetTableView(mysql)

            If myview.Count < 1 Then
                Return True
            End If

            Dim myfilepath As String = Common.FileOps.GetFileName(auser, afilename & "-S-BranchDetailSummary", ".CSV", True)
            myview.ExportColumns.Add(vwInvoiceBranchDetailSummary.ColumnNames.BranchNumber)
            myview.ExportColumns.Add(vwInvoiceBranchDetailSummary.ColumnNames.Description)
            myview.ExportColumns.Add(vwInvoiceBranchDetailSummary.ColumnNames.Qty)
            myview.ExportColumns.Add(vwInvoiceBranchDetailSummary.ColumnNames.Price)
            myview.ExportToCSV(myfilepath)

            Return myfilepath


        End Function
        Public Shared Function PrintSmanCommission(ByVal auser As Users.CurrentUser, ByVal aview As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode) As Boolean
            Dim myreport As New BLL.Common.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\Acc-Liens.xml"
            myreport.ReportName = "Sman Commission"
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.DataView = aview
            myreport.PrintMode = printmode
            myreport.RenderReport()
            Return True

        End Function
        Public Shared Function PrintInvoiceRecap(ByVal auser As Users.CurrentUser, ByVal aview As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode) As Boolean
            Dim myreport As New BLL.Common.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\Acc-Liens.xml"
            myreport.ReportName = "Invoice Recap"
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.DataView = aview
            myreport.PrintMode = printmode
            myreport.RenderReport()
            Return True

        End Function
        Public Shared Function PrintInvoiceSummary(ByVal auser As Users.CurrentUser, ByVal aview As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode) As Boolean
            Dim myreport As New BLL.Common.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\Acc-Liens.xml"
            myreport.ReportName = "Invoice Summary"
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.DataView = aview
            myreport.PrintMode = printmode
            myreport.RenderReport()
            Return True

        End Function
        Public Shared Function PrintInvoiceSummaryByClient(ByVal auser As Users.CurrentUser, ByVal aview As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode) As Boolean
            Dim myreport As New BLL.Common.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\Acc-Liens.xml"
            myreport.ReportName = "Invoice Summary By Client"
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.DataView = aview
            myreport.PrintMode = printmode
            myreport.RenderReport()
            Return True

        End Function
        Public Shared Function ExportStatement(ByVal auser As Users.CurrentUser, ByVal aclientfindocid As Integer, ByVal afilename As String) As String

            Dim myquery As New DAL.COMMON.Query(New CRFDB.VIEWS.vwClientFinDocStatement)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, CRFDB.VIEWS.vwClientFinDocDetail.ColumnNames.ClientFinDocId, DAL.COMMON.Conditions._IsEqual, aclientfindocid)
            Dim mysql As String = myquery.BuildQuery
            Dim myview As DAL.COMMON.TableView = DBO.GetTableView(mysql)

            Return ExportSelection(auser, myview, afilename)
         
        End Function


        Public Shared Function ExportSelection(ByVal auser As Users.CurrentUser, ByRef aview As DAL.COMMON.TableView, ByVal afilename As String) As String

            If aview.Count < 1 Then
                Return True
            End If

            Dim myfilepath As String = Common.FileOps.GetFileName(auser, afilename & "-S-LiensStatement", ".CSV", True)
            aview.ExportColumns.Add(vwClientFinDocStatement.ColumnNames.ClientNumber)
            aview.ExportColumns.Add(vwClientFinDocStatement.ColumnNames.ClientFinDocId)
            aview.ExportColumns.Add(vwClientFinDocStatement.ColumnNames.TrxPeriod)
            aview.ExportColumns.Add(vwClientFinDocStatement.ColumnNames.BranchNumber)
            aview.ExportColumns.Add(vwClientFinDocStatement.ColumnNames.OurFileNo)
            aview.ExportColumns.Add(vwClientFinDocStatement.ColumnNames.CustomerNo, "CustRefNum")
            aview.ExportColumns.Add(vwClientFinDocStatement.ColumnNames.ClientRefNo, "JobNum")
            aview.ExportColumns.Add(vwClientFinDocStatement.ColumnNames.JobName)
            aview.ExportColumns.Add(vwClientFinDocStatement.ColumnNames.JobAddress)
            aview.ExportColumns.Add(vwClientFinDocStatement.ColumnNames.Description)
            aview.ExportColumns.Add(vwClientFinDocStatement.ColumnNames.QTY)
            aview.ExportColumns.Add(vwClientFinDocStatement.ColumnNames.Price)
            aview.ExportColumns.Add(vwClientFinDocStatement.ColumnNames.SubmittedBy)
            aview.ExportToCSV(myfilepath)

            Return myfilepath

        End Function

        Public Shared Function ExportInvoiceSummary(ByVal auser As Users.CurrentUser, ByRef aview As DAL.COMMON.TableView, ByVal afilename As String) As String

            If aview.Count < 1 Then
                Return True
            End If

            Dim myfilepath As String = Common.FileOps.GetFileName(auser, afilename, ".CSV", True)
            aview.ExportColumns.Add(vwInvoiceSummary.ColumnNames.Description)
            aview.ExportColumns.Add(vwInvoiceSummary.ColumnNames.Qty)
            aview.ExportColumns.Add(vwInvoiceSummary.ColumnNames.Price)
            aview.ExportColumns.Add(vwInvoiceSummary.ColumnNames.TrxPeriod)
            aview.ExportColumns.Add(vwInvoiceSummary.ColumnNames.IsCommissionable)

            aview.ExportToCSV(myfilepath)

            Return myfilepath

        End Function
        Public Shared Function ExportLienRevenue(ByVal auser As Users.CurrentUser, ByRef aview As DAL.COMMON.TableView, ByVal afilename As String) As String

            If aview.Count < 1 Then
                Return True
            End If

            Dim myfilepath As String = Common.FileOps.GetFileName(auser, afilename, ".CSV", True)
            aview.ExportColumns.Add("ClientCode")
            aview.ExportColumns.Add("ClientName")
            aview.ExportColumns.Add("ParentClientCode")
            aview.ExportColumns.Add("EventtypeId")
            aview.ExportColumns.Add("EventDesc")
            aview.ExportColumns.Add("Qty")
            aview.ExportColumns.Add("Amount")
            aview.ExportColumns.Add("SalesNum")
            aview.ExportColumns.Add("SalesName")
            aview.ExportColumns.Add("SetupDate")
            aview.ExportColumns.Add("FromDate")
            aview.ExportColumns.Add("ThruDate")
            aview.ExportColumns.Add("IsCommissionable")


            aview.ExportToCSV(myfilepath)

            Return myfilepath

        End Function
        'Public Shared Function RecreateInvoice(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.uspbo_Accounting_CreateLienInvoiceClient) As Boolean
        '    DBO.ExecNonQuery(asproc)
        '    Return True

        'End Function
        Public Shared Function VoidInvoice(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.uspbo_Accounting_VoidClientFinDoc) As Boolean
            DBO.ExecNonQuery(asproc)
            Return True

        End Function

    End Class
End Namespace