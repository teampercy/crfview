Namespace Accounting
    Public Class Collections
        Public Shared Function GetClientList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Dim sql As String = "Select ClientId As ClientId,  ClientCode, ClientCode + '-' + ClientName As ClientName from Client Where IsCollectionClient = 1 and IsBranch = 0"
            Return DBO.GetTableView(sql)

        End Function
        Public Shared Function GetParentClientList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView

            Dim sql As String = "Select ClientId As ClientId,  ClientCode, ClientCode + '-' + ClientName As ClientName from Client Where IsCollectionClient = 1 and IsBranch = 0 and ClientId = ParentClientId"
            Return DBO.GetTableView(sql)

        End Function

        Public Shared Function CreateInvoice(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.uspbo_Accounting_CreateLienInvoice) As Boolean
            DBO.ExecNonQuery(asproc)
            Return True

        End Function
        Public Shared Function GetInvoices(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.uspbo_Accounting_GetLienFinDocs, ByVal printmode As Common.PrintMode) As DataSet
            Return DBO.GetDataSet(asproc)
        End Function
        Public Shared Function GetRecaps(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.uspbo_Accounting_GetInvoiceRecaps) As DataSet
            Return DBO.GetDataSet(asproc)

        End Function
        Public Shared Function PrintInvoice(ByVal auser As Users.CurrentUser, ByVal aclientfindocid As Integer, ByVal printmode As Common.PrintMode) As Boolean

            Dim myreport As New BLL.Common.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\Acc-Liens.xml"
            myreport.ReportName = "Liens Invoice"
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.PrintMode = printmode


            Dim myquery As New DAL.COMMON.Query(New CRFDB.VIEWS.vwClientFinDocDetail)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, CRFDB.VIEWS.vwClientFinDocDetail.ColumnNames.ClientFinDocId, DAL.COMMON.Conditions._IsEqual, aclientfindocid)
            myreport.DataSource = DBO.GetDataSet(myquery.BuildQuery)

            myreport.RenderReport()
            Return True

        End Function
        Public Shared Function PrintInvoiceStatement(ByVal auser As Users.CurrentUser, ByVal aclientfindocid As Integer, ByVal printmode As Common.PrintMode) As Boolean

            Dim myreport As New BLL.Common.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\Acc-Liens.xml"
            myreport.ReportName = "Liens Statement"
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.PrintMode = printmode

            Dim myquery As New DAL.COMMON.Query(New CRFDB.VIEWS.vwClientFinDocStatement)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, CRFDB.VIEWS.vwClientFinDocDetail.ColumnNames.ClientFinDocId, DAL.COMMON.Conditions._IsEqual, aclientfindocid)
            myreport.DataSource = DBO.GetDataSet(myquery.BuildQuery)

            myreport.RenderReport()

            Return True

        End Function
        Public Shared Function PrintInvoiceStatementByBranch(ByVal auser As Users.CurrentUser, ByVal aclientfindocid As Integer, ByVal printmode As Common.PrintMode) As Boolean

            Dim myreport As New BLL.Common.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\Acc-Liens.xml"
            myreport.ReportName = "Liens Statement By Branch"
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.PrintMode = printmode

            Dim myquery As New DAL.COMMON.Query(New CRFDB.VIEWS.vwClientFinDocStatement)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, CRFDB.VIEWS.vwClientFinDocDetail.ColumnNames.ClientFinDocId, DAL.COMMON.Conditions._IsEqual, aclientfindocid)
            myreport.DataSource = DBO.GetDataSet(myquery.BuildQuery)

            myreport.RenderReport()

            Return True

        End Function
        Public Shared Function PrintSalesmanCommission(ByVal auser As Users.CurrentUser, ByVal aview As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode) As Boolean
            Dim myreport As New BLL.Common.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\Acc-Liens.xml"
            myreport.ReportName = "Sman Commission"
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.DataView = aview
            myreport.PrintMode = printmode
            myreport.RenderReport()
            Return True

        End Function
        Public Shared Function PrintInvoiceRecap(ByVal auser As Users.CurrentUser, ByVal aview As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode) As Boolean
            Dim myreport As New BLL.Common.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\Acc-Liens.xml"
            myreport.ReportName = "Invoice Recap"
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.DataView = aview
            myreport.PrintMode = printmode
            myreport.RenderReport()
            Return True

        End Function
        Public Shared Function PrintInvoiceSummary(ByVal auser As Users.CurrentUser, ByVal aview As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode) As Boolean
            Dim myreport As New BLL.Common.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\Acc-Liens.xml"
            myreport.ReportName = "Invoice Summary"
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.DataView = aview
            myreport.PrintMode = printmode
            myreport.RenderReport()
            Return True

        End Function
        Public Shared Function PrintInvoiceSummaryByClient(ByVal auser As Users.CurrentUser, ByVal aview As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode) As Boolean
            Dim myreport As New BLL.Common.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\Acc-Liens.xml"
            myreport.ReportName = "Invoice Summary By Client"
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.DataView = aview
            myreport.PrintMode = printmode
            myreport.RenderReport()
            Return True

        End Function
        Public Shared Function ExportStatement(ByVal auser As Users.CurrentUser, ByVal aview As DAL.COMMON.TableView, ByVal afilename As String) As Boolean
            Dim myreport As New BLL.Common.StandardReport
            aview.ExportToCSV(afilename)
            Return True

        End Function
        'Public Shared Function RecreateInvoice(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.uspbo_Accounting_CreateLienInvoiceClient) As Boolean
        '    DBO.ExecNonQuery(asproc)
        '    Return True

        'End Function
        Public Shared Function VoidInvoice(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.uspbo_Accounting_VoidClientFinDoc) As Boolean
            DBO.ExecNonQuery(asproc)
            Return True

        End Function

    End Class
End Namespace