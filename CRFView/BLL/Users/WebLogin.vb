Namespace Users
    Public Class WebLogin
        Inherits CRFDB.TABLES.Portal_Users
        Public SprocParams As New CRFDB.SPROCS.Portal_UserLogin_ClientView
        Dim myuserinfo As DataSet
        Public Sub New()
            MyBase.New()
        End Sub
        Public Function Execute() As Boolean
            myuserinfo = DBO.GetDataSet(SprocParams)
            If myuserinfo.Tables(0).Rows.Count = 0 Then
                Return False
            End If
            DAL.COMMON.Mapper.GetRowValues(myuserinfo.Tables(0), Me)
            Return True
        End Function
    End Class
End Namespace
