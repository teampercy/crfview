Namespace Users
    Public Class SignOff
        Inherits CRFDB.TABLES.Portal_Users
        Public SprocParams As New CRFDB.SPROCS.Portal_LogoffUser
        Dim myuserinfo As DataSet
        Public Sub New()
            MyBase.New()
        End Sub
        Public Function Execute() As Boolean
            DBO.ExecNonQuery(SprocParams)
            Return True
        End Function
    End Class
End Namespace
