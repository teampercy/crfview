Imports System.Web
Namespace Users
    Public Interface IUser
        Property Id() As String
        Property MachineId() As String
        Property IPAddress() As String
        Property UserName() As String
        Property Email() As String
        Property Roles() As String
        Property PasswordHint() As String
        Property DateJoined() As String
        Property LastAccess() As String
        Property PasswordExpires() As String
    End Interface
    Public Class SessionCache
        Inherits DictionaryBase
        Default Public Property Blubber(ByVal key As String) As Object
            Get

                If Not (System.Web.HttpContext.Current.Cache(System.Web.HttpContext.Current.Session.SessionID + "~" + key) Is Nothing) Then
                    Return (System.Web.HttpContext.Current.Cache(System.Web.HttpContext.Current.Session.SessionID + "~" + key))
                Else
                    If Not (System.Web.HttpContext.Current.Session(key) Is Nothing) Then
                        System.Web.HttpContext.Current.Cache(System.Web.HttpContext.Current.Session.SessionID + "~" + key) = System.Web.HttpContext.Current.Session(key)
                        Add(System.Web.HttpContext.Current.Session.SessionID + "~" + key, System.Web.HttpContext.Current.Cache(System.Web.HttpContext.Current.Session.SessionID + "~" + key))
                        Return (System.Web.HttpContext.Current.Session(key))
                    End If
                End If

                Return (Nothing)
            End Get
            Set(ByVal Value As Object)
                System.Web.HttpContext.Current.Cache(System.Web.HttpContext.Current.Session.SessionID + "~" + key) = Value
                System.Web.HttpContext.Current.Session(key) = Value
                Add(System.Web.HttpContext.Current.Session.SessionID + "~" + key, Value)
            End Set
        End Property
        Public Sub ClearSession()
            Dim sSessionID As String = System.Web.HttpContext.Current.Session.SessionID
            Dim pArray As ArrayList = New ArrayList(Keys)
            Dim i As Integer = 0
            While i < pArray.Count
                If pArray(i).ToString.IndexOf(sSessionID + "~") = 0 Then
                    System.Web.HttpContext.Current.Cache.Remove(pArray(i).ToString)
                End If
                System.Math.Min(System.Threading.Interlocked.Increment(i), i - 1)
            End While
        End Sub
        Protected Sub Add(ByVal key As String, ByVal oValue As Object)
            Me.Dictionary.Add(key, oValue)
        End Sub
        Public Function Contains(ByVal key As String) As Boolean
            Return Me.Dictionary.Contains(key)
        End Function
        Public Sub Remove(ByVal key As String)
            If Me.Dictionary.Contains(key) Then
                Me.Dictionary.Remove(key)
                System.Web.HttpContext.Current.Cache.Remove(key)
            End If
        End Sub
        Protected ReadOnly Property Keys() As ICollection
            Get
                Return Me.Dictionary.Keys
            End Get
        End Property
    End Class
    Public Class CookieManager
        Public CookieName As String = "WebPortalUser"
        Public CookieTimeoutInMonths As Integer = 48
        Public Sub New()
        End Sub
        Public Sub New(ByVal NewCookieName As String)
            Me.CookieName = NewCookieName
        End Sub
        Public Sub WriteCookie(ByVal Value As String, ByVal NonPersistent As Boolean)
            Dim Cookie As HttpCookie = New HttpCookie(CookieName, Value)
            If Not NonPersistent Then
                Cookie.Expires = DateTime.Now.AddMonths(CookieTimeoutInMonths)
            End If
            HttpContext.Current.Response.Cookies.Add(Cookie)
        End Sub
        Public Sub WriteCookie(ByVal Value As String)
            WriteCookie(Value, False)
        End Sub
        Public Sub Remove()
            Dim Cookie As HttpCookie = HttpContext.Current.Request.Cookies(CookieName)
            If Not (Cookie Is Nothing) Then
                Cookie.Expires = DateTime.Now.AddHours(-2)
                HttpContext.Current.Response.Cookies.Add(Cookie)
            End If
        End Sub
        Public Function GetId() As String
            Dim UserId As String
            Dim Cookie As HttpCookie = HttpContext.Current.Request.Cookies(CookieName)
            If Cookie Is Nothing Then
                UserId = Nothing
            Else
                UserId = CType(Cookie.Value, String)
            End If
            If UserId Is Nothing Then
                UserId = Me.GenerateId
                WriteCookie(UserId)
            End If
            Return UserId
        End Function
        Protected Overridable Function GenerateId() As String
            Return Guid.NewGuid.ToString.GetHashCode.ToString("x")
        End Function
        Public Function CookieExist() As Boolean
            Dim Cookie As HttpCookie = HttpContext.Current.Request.Cookies(CookieName)
            If Cookie Is Nothing Then
                Return False
            End If
            Return True
        End Function
    End Class
End Namespace
