Namespace Clients
    Public Class ClientInfo
        Dim myds As DataSet
        Dim myclient As DAL.COMMON.TableView
        Dim mycollinfo As DAL.COMMON.TableView
        Dim mylieninfo As DAL.COMMON.TableView
        Dim mycontracts As DAL.COMMON.TableView
        Dim myusers As DAL.COMMON.TableView
        Dim mybranches As DAL.COMMON.TableView
        Dim myopenissues As DAL.COMMON.TableView
        Dim myclosedissues As DAL.COMMON.TableView
        Dim mycontacts As DAL.COMMON.TableView
        Dim mylogo As DAL.COMMON.TableView
        Dim mysigner As DAL.COMMON.TableView
        Dim myrpts As DAL.COMMON.TableView
        Dim myLienContractHist As DAL.COMMON.TableView
        Dim myCollContractHist As DAL.COMMON.TableView
       

        Public Sub New(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer)
            Dim mysproc As New SPROCS.uspbo_CRM_GetClientInfo
            mysproc.ClientId = aitemid
            myds = DBO.GetDataSet(mysproc)
            myclient = New DAL.COMMON.TableView(myds.Tables(0), "")
            mycollinfo = New DAL.COMMON.TableView(myds.Tables(1), "")
            mylieninfo = New DAL.COMMON.TableView(myds.Tables(2), "")
            mycontracts = New DAL.COMMON.TableView(myds.Tables(3), "")
            mybranches = New DAL.COMMON.TableView(myds.Tables(4), "")
            myopenissues = New DAL.COMMON.TableView(myds.Tables(5), "")
            myclosedissues = New DAL.COMMON.TableView(myds.Tables(6), "")
            myusers = New DAL.COMMON.TableView(myds.Tables(8), "")
            mycontacts = New DAL.COMMON.TableView(myds.Tables(9), "")
            mylogo = New DAL.COMMON.TableView(myds.Tables(10), "")
            mysigner = New DAL.COMMON.TableView(myds.Tables(11), "")
            myrpts = New DAL.COMMON.TableView(myds.Tables(12), "")
            myLienContractHist = New DAL.COMMON.TableView(myds.Tables(13), "")
            myCollContractHist = New DAL.COMMON.TableView(myds.Tables(14), "")
           
        End Sub
        Public Function ClientId() As Integer
            Return myclient.RowItem(TABLES.Client.ColumnNames.ClientId)
        End Function
        Public Function ClientCode() As String
            Return myclient.RowItem(TABLES.Client.ColumnNames.ClientCode)
        End Function
        Public Function ParentClientId() As Integer
            Return myclient.RowItem(TABLES.Client.ColumnNames.ParentClientId)
        End Function
        Public Function ClientName() As String
            Return myclient.RowItem(TABLES.Client.ColumnNames.ClientName)
        End Function
        Public Function CollectionInfo() As DAL.COMMON.TableView
            Return mycollinfo
        End Function
        Public Function LienInfo() As DAL.COMMON.TableView
            Return mylieninfo
        End Function
        Public Function Contracts() As DAL.COMMON.TableView
            Return mycontracts
        End Function
        Public Function Branches() As DAL.COMMON.TableView
            Return mybranches
        End Function
        Public Function OpenIssues() As DAL.COMMON.TableView
            Return myopenissues
        End Function
        Public Function ClosedIssues() As DAL.COMMON.TableView
            Return myclosedissues
        End Function
        Public Function Users() As DAL.COMMON.TableView
            Return myusers
        End Function
        Public Function Contacts() As DAL.COMMON.TableView
            Return mycontacts
        End Function
        Public Function Logo() As DAL.COMMON.TableView
            Return mylogo
        End Function
        Public Function Signers() As DAL.COMMON.TableView
            Return mysigner
        End Function
        Public Function ScheduledReports() As DAL.COMMON.TableView
            Return myrpts
        End Function
        Public Function LienContractHistory() As DAL.COMMON.TableView
            Return myLienContractHist
        End Function
        Public Function CollContractHistory() As DAL.COMMON.TableView
            Return myCollContractHist
        End Function
    End Class

End Namespace
