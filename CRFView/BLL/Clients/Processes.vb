Namespace Clients
    Public Class Processes
        Public Shared Function GetClientList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Dim sql As String = "Select ClientId As ClientId,  ClientCode, ClientCode + '-' + ClientName As ClientName from Client Where IsCollectionClient = 1 and IsBranch = 0"
            Return DBO.GetTableView(sql)
        End Function
        Public Shared Function GetClientInfo(ByVal auser As Users.CurrentUser, ByVal aclientId As Integer) As Clients.ClientInfo
            Return New Clients.ClientInfo(auser, aclientId)
        End Function
        Public Shared Function CreateNewClient(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.uspbo_CRM_CreateClient) As DAL.COMMON.TableView
            Dim MYCLIENTID As Integer = DBO.ExecScalar(asproc)
            Dim MYVIEW As New VIEWS.vwClientList
            Dim MYQUERY As New DAL.COMMON.Query(MYVIEW)
            MYQUERY.AddCondition(DAL.COMMON.LogicalOperators._And, VIEWS.vwClientList.ColumnNames.ClientId, DAL.COMMON.Conditions._IsEqual, MYCLIENTID)
            Return DBO.GetTableView(MYQUERY.BuildQuery)
        End Function
        Public Shared Function GetEmailList(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.uspbo_CRM_GetEmailList) As DAL.COMMON.TableView
            Return DBO.GetTableView(asproc)
        End Function
        Public Shared Function PostEmailList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Dim mysproc As New SPROCS.uspbo_CRM_BatchEmailBlast
            Return DBO.GetTableView(mysproc)
        End Function

        Public Shared Function CreateNewRapidPayClient(ByVal auser As Users.CurrentUser, ByVal RapidPayId As Integer) As Boolean
            Dim mysproc As New CRFDB.SPROCS.uspbo_CRM_RapidPayCreateClient
            mysproc.RapidPayClientId = RapidPayId
            DBO.ExecNonQuery(mysproc)
            Return True
        End Function
        Public Shared Function CreateRapidPayOrderRequest(ByVal auser As Users.CurrentUser, ByVal ClientId As Integer, ByVal Units As Integer, ByVal UnitPrice As Decimal) As Boolean
            Dim mysproc As New CRFDB.SPROCS.uspbo_CRM_RapidPayAdditionalOrderRequest
            mysproc.RapidPayClientId = ClientId
            mysproc.JobQty = Units
            mysproc.UnitPrice = UnitPrice
            DBO.ExecNonQuery(mysproc)
            Return True
        End Function
        Public Shared Function GetRapidPayPendingList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Dim mysproc As New CRFDB.SPROCS.uspbo_CRM_RapidPayGetPendingList
            Return DBO.GetTableView(mysproc)
        End Function
        Public Shared Function GetPricingForContact(ByVal auser As Users.CurrentUser, ByVal acontractId As Integer) As DAL.COMMON.TableView
            Dim mycontract As New TABLES.ClientContract
            DBO.Read(acontractId, mycontract)
            Dim myview As New VIEWS.vwContractPricing
            Dim myquery As New HDS.DAL.COMMON.Query(myview)
            Dim mysql As String
            myquery.AddCondition(HDS.DAL.COMMON.LogicalOperators._And, CRFDB.VIEWS.vwContractPricing.ColumnNames.ContractId, HDS.DAL.COMMON.Conditions._IsEqual, acontractId)
            myview.IsAllInclusivePrice = mycontract.IsAllInclusivePrice
            mysql = myquery.BuildQuery
            Return DBO.GetTableView(myquery.BuildQuery)

        End Function
        Public Shared Function GetRatesForBillableEvent(ByVal auser As Users.CurrentUser, ByVal aeventId As Integer, ByVal IsAllInclusive As Boolean) As DAL.COMMON.TableView
            Dim myview As New VIEWS.vwPriceCode
            Dim myquery As New HDS.DAL.COMMON.Query(myview)
            Dim mysql As String
            myquery.AddCondition(HDS.DAL.COMMON.LogicalOperators._And, CRFDB.VIEWS.vwPriceCode.ColumnNames.BillableEventId, HDS.DAL.COMMON.Conditions._IsEqual, aeventId)

            'If IsAllInclusive = True Then
            myquery.AddCondition(HDS.DAL.COMMON.LogicalOperators._And, CRFDB.VIEWS.vwPriceCode.ColumnNames.IsAllInclusivePrice, HDS.DAL.COMMON.Conditions._IsEqual, IsAllInclusive)
            'End If
            mysql = myquery.BuildQuery
            Return DBO.GetTableView(myquery.BuildQuery)

        End Function

    End Class
End Namespace
