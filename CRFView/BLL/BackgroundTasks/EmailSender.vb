Imports System.Web
Imports System.Threading
Imports System.Web.Mail
Namespace BackgroundTasks
    Public Class QuoteofTheDay
        Dim _message As System.Net.Mail.MailMessage
        Dim _smtpserver As String
        Public Sub New()
        End Sub
        Public Sub StartTask(ByVal message As System.Net.Mail.MailMessage, ByVal server As String)
            _message = message
            _smtpserver = server
            Dim threadStart As ThreadStart = Nothing
            Dim thread As Thread = Nothing
            Try
                threadStart = AddressOf _DoTheWork
                thread = New Thread(threadStart)
                '    thread.Name = "SendAsyncEmail " + _message.To.Item
                thread.Start()
                thread = Nothing
            Catch generatedExceptionVariable0 As Exception
            Finally
                thread = Nothing
                threadStart = Nothing
            End Try
        End Sub
        Private Sub _DoTheWork()
            Try

                Dim MYOBJ As New System.Net.Mail.SmtpClient
                MYOBJ.Host = _smtpserver
                MYOBJ.Send(_message)

            Catch generatedExceptionVariable0 As Exception
            Finally
                _message = Nothing
            End Try
        End Sub
    End Class
End Namespace
