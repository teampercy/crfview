Namespace Collections
    Public Class Reports
        Public Shared Function AccountsClosed(ByRef auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal MYview As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode, ByVal aspreadsheet As Boolean) As String

            If MYview.Count < 1 Then
                Return Nothing
            End If

            Dim myreportpath As String = auser.SettingsFolder & "Reports\CollelctionReports.xml"
            Dim myreportname As String = "CloseNotice"
            If printmode <> Common.PrintMode.NoReport Then
                Common.Reporting.StandardReport(myreportpath, myreportname, auser, akeyvals, MYview, printmode)
            End If

            If aspreadsheet = True Then
                Dim myspreadsheetpath As String = Common.FileOps.GetFileName(auser, myreportname)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.FileNumber)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.ServiceCode)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.AccountNo)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.DebtorName)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.ContactName)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.AddressLine1)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.AddressLine2)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.City)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.State)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.PostalCode)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.PhoneNo)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.Fax)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.Email)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.ReferalDate)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.LastServiceDate)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.LastPaymentDate)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.TotAsgAmt)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.TotPmtAmt)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.TotAdjAmt)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.TotBalAmt)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.CollectionStatus)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.CloseDate)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.CloseCode)
                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    akeyvals.Add("SPREADSHEET", myspreadsheetpath)
                Else
                    akeyvals.Add("SPREADSHEET", Nothing)
                End If

            End If

            Return Nothing

        End Function
        Public Shared Function AccountsPayment(ByRef auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal MYview As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode, ByVal aspreadsheet As Boolean) As String
            If MYview.Count < 1 Then
                Return Nothing
            End If
            Dim myreportpath As String = auser.SettingsFolder & "Reports\AgencyReports.xml"
            Dim myreportname As String = "PaymentNotice"
            If printmode <> Common.PrintMode.NoReport Then
                Common.Reporting.StandardReport(myreportpath, myreportname, auser, akeyvals, MYview, printmode)
            End If


            If aspreadsheet = True Then
                Dim myspreadsheetpath As String = Common.FileOps.GetFileName(auser, "AccountPayments")
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.ClientCode)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.FileNumber)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.AccountNo)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.DebtorName)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.TrxDate)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.LedgerType)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.Rate)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.RcvByAgencyAmt)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.FeeByAgencyAmt)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.RcvByClientAmt)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.FeeByClientAmt)
                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    akeyvals.Add("SPREADSHEET", myspreadsheetpath)
                Else
                    akeyvals.Add("SPREADSHEET", Nothing)
                End If

            End If
            Return Nothing

        End Function
        Public Shared Function AccountsMaster(ByRef auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal MYview As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode, ByVal aspreadsheet As Boolean) As String
            If MYview.Count < 1 Then
                Return Nothing
            End If

            Dim myreportpath As String = auser.SettingsFolder & "Reports\CollelctionReports.xml"
            Dim myreportname As String = "InventoryNotice"
            If printmode <> Common.PrintMode.NoReport Then
                Common.Reporting.StandardReport(myreportpath, myreportname, auser, akeyvals, MYview, printmode)
            End If

            If aspreadsheet = True Then
                Dim myspreadsheetpath As String = Common.FileOps.GetFileName(auser, myreportname)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.FileNumber)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.ServiceCode)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.AccountNo)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.DebtorName)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.ContactName)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.AddressLine1)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.AddressLine2)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.City)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.State)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.PostalCode)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.PhoneNo)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.Fax)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.Email)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.ReferalDate)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.LastServiceDate)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.LastPaymentDate)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.TotAsgAmt)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.TotPmtAmt)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.TotAdjAmt)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.TotBalAmt)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.CollectionStatus)
                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    akeyvals.Add("SPREADSHEET", myspreadsheetpath)
                Else
                    akeyvals.Add("SPREADSHEET", Nothing)
                End If

            End If

            Return Nothing

        End Function
        Public Shared Function BatchList(ByVal auser As Users.CurrentUser, ByVal MYview As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode) As String
            If MYview.Count < 1 Then
                Return Nothing
            End If

            If printmode = Common.PrintMode.PrintToPDF Then
                Dim myreport As New BLL.Common.StandardReport
                myreport.ReportDefPath = auser.SettingsFolder & "Reports\CollectViewReports.xml"
                myreport.OutputFolder = auser.OutputFolder
                myreport.UserName = auser.UserName
                myreport.ReportName = "BatchReport"
                myreport.TableIndex = 1
                myreport.DataView = MYview
                myreport.PrintMode = printmode
                myreport.Sort = ""
                myreport.RenderReport()
                If myreport.ReportFileName.Length < 1 Then
                    Return Nothing
                Else
                    Return myreport.ReportFileName
                End If

            End If
            If printmode.PrinttoExcel Then
                Dim myspreadsheetpath As String = Common.FileOps.GetFileName(auser, "BatchList")
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.ServiceCode)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.AccountNo)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.DebtorName)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.ContactName)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.AddressLine1)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.AddressLine2)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.City)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.State)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.PostalCode)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.PhoneNo)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.Fax)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.Email)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.ReferalDate)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.LastServiceDate)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.LastPaymentDate)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.TotAsgAmt)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.AccountNote)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.BatchId)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.BatchDebtAccountId)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.SubmittedByUserId)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.ClientAgentCode)

                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    Return myspreadsheetpath
                Else
                    Return Nothing
                End If

            End If

            Return Nothing

        End Function
    End Class
End Namespace
