Namespace Collections
    Public Class AccountInfo
        Dim myds As DataSet
        Dim CurrentUser As Users.CurrentUser
        Dim _accountview As DAL.COMMON.TableView
        Dim _client As DAL.COMMON.TableView
        Dim _ledgers As DAL.COMMON.TableView
        Dim _historys As DAL.COMMON.TableView
        Dim _addresses As DAL.COMMON.TableView
        Dim _accounts As DAL.COMMON.TableView
        Dim _invoice As DAL.COMMON.TableView
        Dim _invoicedetail As DAL.COMMON.TableView
        Dim _contract As DAL.COMMON.TableView
        Public Sub New(ByVal auser As CRF.BLL.Users.CurrentUser, ByVal keyvalue As String)
            MyBase.New()
            Me.CurrentUser = auser
            Dim mysproc As New CRFDB.SPROCS.uspbo_Accounts_GetAccountInfo
            mysproc.AccountId = keyvalue
            myds = DBO.GetDataSet(mysproc)
            _accountview = New DAL.COMMON.TableView(myds.Tables(0))
            _ledgers = New DAL.COMMON.TableView(myds.Tables(1))
            _historys = New DAL.COMMON.TableView(myds.Tables(2))
            _addresses = New DAL.COMMON.TableView(myds.Tables(3))
            _client = New DAL.COMMON.TableView(myds.Tables(4))
            _contract = New DAL.COMMON.TableView(myds.Tables(5))
            _accounts = New DAL.COMMON.TableView(myds.Tables(6))
            '_invoice = New DAL.COMMON.TableView(myds.Tables(7))
            '_invoicedetail = New DAL.COMMON.TableView(myds.Tables(8))

        End Sub
        Public Function AccountInfo() As DAL.COMMON.TableView
            Return _accountview
        End Function
        Public Function ClientInfo() As DAL.COMMON.TableView
            Return _client
        End Function
        Public Function Contract() As DAL.COMMON.TableView
            Return _contract
        End Function
        Public Function Historys() As DAL.COMMON.TableView
            Return _historys
        End Function
        Public Function Ledgers() As DAL.COMMON.TableView
            Return _ledgers
        End Function
        Public Function Addresses() As DAL.COMMON.TableView
            Return _addresses
        End Function
        Public Function Accounts() As DAL.COMMON.TableView
            Return _accounts
        End Function
        'Public Function Invoices() As DAL.COMMON.TableView
        '    Return _invoice
        'End Function
        'Public Function InvoiceDetails() As DAL.COMMON.TableView
        '    Return _invoicedetail
        'End Function

    End Class
End Namespace
