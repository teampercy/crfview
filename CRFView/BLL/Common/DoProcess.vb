Namespace Common
    Public Class DoProcess
        Implements IProcessor
        Dim _username As String = ""
        Dim _userid As String = ""
        Dim _outputfolder As String = ""
        Dim _settingsfolder As String = ""
        Public Sub New()

        End Sub
        Public Overridable Function Execute() As Boolean Implements IProcessor.Execute
            Return False
        End Function

        Public Property OutputFolder() As String Implements IProcessor.OutputFolder
            Get
                Return _outputfolder
            End Get
            Set(ByVal Value As String)
                _outputfolder = Value
            End Set
        End Property

        Public Property SettingsFolder() As String Implements IProcessor.SettingsFolder
            Get
                Return _settingsfolder
            End Get
            Set(ByVal Value As String)
                _settingsfolder = Value
            End Set
        End Property

        Public Property UserId() As String Implements IProcessor.UserId
            Get
                Return _userid
            End Get
            Set(ByVal Value As String)
                _userid = Value
            End Set
        End Property

        Public Property UserName() As String Implements IProcessor.UserName
            Get
                Return _username
            End Get
            Set(ByVal Value As String)
                _username = Value
            End Set
        End Property
    End Class

End Namespace
