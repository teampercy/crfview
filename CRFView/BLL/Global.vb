Imports System.Configuration.ConfigurationManager
Module Globals
    Public DBO As DAL.Providers.IDAL = DAL.Providers.DataAccessFactory.DataAccess("PORTAL", AppSettings.Get("SQLCLIENT"))
    Public MYC1RPT As New HDS.Reporting.C1Reports.C1ReportProvider
    Public MYDTRPT As New HDS.Reporting.DataReports.DataReport
    Public Sub SetC1RptLogo()
        Dim MYAGENCY As New TABLES.Agency
        DBO.Read(1, MYAGENCY)
        Dim mylogo As New HDS.WINLIB.Controls.ImageViewer
        mylogo.LoadImage(MYAGENCY.AgencyLogo)
        MYC1RPT.ReportLogo = mylogo.PictureBox1.Image
        MYC1RPT.ReportFooter = MYAGENCY.AgencyRptFooter

    End Sub
End Module
