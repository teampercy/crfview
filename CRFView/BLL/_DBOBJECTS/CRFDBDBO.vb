Imports Microsoft.VisualBasic
Imports System.Collections
Imports System.Threading
Imports System.Reflection
Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Namespace CRFDB
#Region "Core Functions"
    Public Class Provider
        Private Shared current As System.Web.HttpContext = System.Web.HttpContext.Current
        Private Shared _isInitialized As Boolean = False
        Private Shared _DBO As DAL.Providers.IDAL
        Public Shared ReadOnly Property DBO() As DAL.Providers.IDAL
            Get
                Return HDS.DAL.Providers.DataAccessFactory.DataAccess("PORTAL", AppSettings.Get("SQLCLIENT"))
            End Get
        End Property
        Private Shared Sub Initialize()
            If Not _isInitialized Then
                _isInitialized = True
            End If
        End Sub
        Public Shared Function GetDataTable(ByVal ObjectName As String, ByVal maxrows As Integer, ByVal filter As String, ByVal sort As String) As System.Data.DataTable
            Dim MYSQL As String = DBO.GetSQL(ObjectName, maxrows, filter, sort)
            Return DBO.GetDataSet(MYSQL).Tables(0)
        End Function
        Public Shared Function FillEntity(ByVal dr As DataRow, ByRef entity As Object) As Object
            DBO.FillEntity(dr, entity)
            Return entity
        End Function
        Public Shared Function GetItem(ByVal aobjectname As String, ByRef aentity As Object, ByVal akeyname As String, ByVal avalue As String)
            Dim MYSQL As String = DBO.GetSQL(aobjectname, 1, akeyname & "='" & avalue & "'", "")
            Dim myds As DataSet = DBO.GetDataSet(MYSQL)
            If myds.Tables(0).Rows.Count > 0 Then
                DBO.FillEntity(myds.Tables(0).Rows.Item(0), aentity)
            End If
            Return aentity

        End Function
        Public Shared Function GetItem(ByVal aobjectname As String, ByRef aentity As Object, ByVal filter As String)
            Dim MYSQL As String = DBO.GetSQL(aobjectname, 1, filter, "")
            Dim myds As DataSet = DBO.GetDataSet(MYSQL)
            If myds.Tables(0).Rows.Count > 0 Then
                DBO.FillEntity(myds.Tables(0).Rows.Item(0), aentity)
            End If
            Return aentity

        End Function
    End Class
#End Region


End Namespace
