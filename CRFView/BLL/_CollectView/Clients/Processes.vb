Imports HDS
Imports HDS.CVPORTAL
Imports HDS.CVPORTAL.BLL
Imports HDS.CVPORTAL.BLL.CVDB.VIEWS
Imports HDS.CVPORTAL.BLL.CVDB.TABLES
Imports HDS.Reporting
Imports HDS.Reporting.COMMON
Namespace Collectview.Clients
    Public Class Processes
        Public Shared Function GetClientInfo(ByVal auser As Users.CurrentUser, ByVal aitemid As String) As ClientInfo
            Return New ClientInfo(auser, aitemid)
        End Function
        Public Shared Function PrintClosedReport(ByVal auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal asproc As CVDB.SPROCS.cview_Reports_GetClosedAccountList, ByVal printmode As HDS.Reporting.COMMON.PrintMode, ByVal spreadsheet As Boolean) As Integer
            Dim MYVIEW As DAL.COMMON.TableView = DBO.GetTableView(asproc)
            akeyvals.Add("RECORDCOUNT", MYVIEW.Count)
            If MYVIEW.Count > 0 Then
                Collections.Reports.AccountsClosed(auser, akeyvals, MYVIEW, printmode, spreadsheet)
            End If
            Return MYVIEW.Count

        End Function
        Public Shared Function PrintMasterReport(ByVal auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal asproc As CVDB.SPROCS.cview_Reports_GetAccountList, ByVal printmode As HDS.Reporting.COMMON.PrintMode, ByVal spreadsheet As Boolean) As Integer
            Dim MYVIEW As DAL.COMMON.TableView = DBO.GetTableView(asproc)
            akeyvals.Add("RECORDCOUNT", MYVIEW.Count)
            If MYVIEW.Count > 0 Then
                Collections.Reports.AccountsMaster(auser, akeyvals, MYVIEW, printmode, spreadsheet)
            End If
            Return MYVIEW.Count

        End Function
        Public Shared Function PrintPaymentReport(ByVal auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal asproc As CVDB.SPROCS.cview_Reports_GetPaymentList, ByVal printmode As HDS.Reporting.COMMON.PrintMode, ByVal spreadsheet As Boolean) As Integer
            Dim MYVIEW As DAL.COMMON.TableView = DBO.GetTableView(asproc)
            akeyvals.Add("RECORDCOUNT", MYVIEW.Count)
            If MYVIEW.Count > 0 Then
                Collections.Reports.AccountsPayment(auser, akeyvals, MYVIEW, printmode, spreadsheet)
            End If
            Return MYVIEW.Count

        End Function
    End Class
End Namespace
