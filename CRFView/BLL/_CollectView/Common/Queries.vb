Imports System.Web
Public Class Queries
    Public Shared Function GetView(ByVal aentity As DAL.Providers.ITable, Optional ByVal Cached As Boolean = False)
        Dim myview As DAL.COMMON.TableView = Nothing
        If Cached = False Or IsNothing(HttpContext.Current.Cache(aentity.TableName)) = True Then
            Dim myquery As New DAL.COMMON.Query(aentity)
            myview = DBO.GetTableView(myquery.SelectAll)
            If Cached = True Then
                HttpContext.Current.Cache.Insert(aentity.TableName, myview)
            End If
        Else
            myview = HttpContext.Current.Cache.Item(aentity.TableName)
        End If
        Return myview
    End Function
    Public Shared Function GetInternalUsers() As DAL.COMMON.TableView
        Dim mytable As New CVDB.TABLES.Portal_Users
        Dim myquery As New DAL.COMMON.Query(mytable)
        myquery.AddCondition(DAL.COMMON.LogicalOperators._And, TABLES.Portal_Users.ColumnNames.InternalUser, DAL.COMMON.Conditions._IsEqual, 1)
        Dim sql As String = myquery.BuildQuery
        Return DBO.GetTableView(sql)

    End Function
    Public Shared Function GetServiceCodes() As DAL.COMMON.TableView
        Dim mytable As New CVDB.VIEWS.vwClientServiceCode
        Dim myquery As New DAL.COMMON.Query(mytable)
        Dim sql As String = myquery.SelectAll
        Return DBO.GetTableView(sql)

    End Function
    Public Shared Function GetServiceTeams() As DAL.COMMON.TableView
        Dim mytable As New CVDB.TABLES.Team
        Dim myquery As New DAL.COMMON.Query(mytable)
        myquery.AddCondition(DAL.COMMON.LogicalOperators._And, TABLES.Team.ColumnNames.IsServiceTeam, DAL.COMMON.Conditions._IsEqual, 1)
        Dim sql As String = myquery.BuildQuery
        Return DBO.GetTableView(sql)

    End Function
    Public Shared Function GetClientType() As DAL.COMMON.TableView
        Dim mytable As New CVDB.TABLES.ClientType
        Dim myquery As New DAL.COMMON.Query(mytable)
        Dim sql As String = myquery.SelectAll
        Return DBO.GetTableView(sql)

    End Function
    Public Shared Function GetContractType() As DAL.COMMON.TableView
        Dim mytable As New CVDB.TABLES.ContractType
        Dim myquery As New DAL.COMMON.Query(mytable)
        Dim sql As String = myquery.SelectAll
        Return DBO.GetTableView(sql)

    End Function
    Public Shared Function GetFinDocType() As DAL.COMMON.TableView
        Dim mytable As New CVDB.TABLES.FinDocType
        Dim myquery As New DAL.COMMON.Query(mytable)
        Dim sql As String = myquery.SelectAll
        Return DBO.GetTableView(sql)

    End Function
    Public Shared Function GetServiceType() As DAL.COMMON.TableView
        Dim mytable As New CVDB.TABLES.Product
        Dim myquery As New DAL.COMMON.Query(mytable)
        Dim sql As String = myquery.SelectAll
        Return DBO.GetTableView(sql)

    End Function
    Public Shared Function GetSalesTeams() As DAL.COMMON.TableView
        Dim mytable As New CVDB.TABLES.Team
        Dim myquery As New DAL.COMMON.Query(mytable)
        myquery.AddCondition(DAL.COMMON.LogicalOperators._And, TABLES.Team.ColumnNames.IsSalesTeam, DAL.COMMON.Conditions._IsEqual, 1)
        Dim sql As String = myquery.BuildQuery
        Return DBO.GetTableView(sql)

    End Function
    Public Shared Function ClientViewGroups() As DAL.COMMON.TableView
        Dim mystate As New CVDB.TABLES.ClientViewGroup
        Dim myquery As New DAL.COMMON.Query(mystate)
        Return DBO.GetTableView(myquery.SelectAll)
    End Function
    Public Shared Function GetMainClients() As DataView
        Dim mysproc As New SPROCS.cview_Clients_GetMainClients
        Return DBO.GetTableView(mysproc)
    End Function
    Public Shared Function GetAllClients() As DataView
        Dim sql As String = "Select ClientId As ClientId, ClientCode, ClientCode + '-' + ClientName As ClientName from Client Where IsBranch = 0 ORDER BY CLIENTNAME"
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetCollectionStatusGroupList() As DAL.COMMON.TableView
        Dim mystate As New CVDB.TABLES.CollectionStatusGroup
        Dim myquery As New DAL.COMMON.Query(mystate)
        Return DBO.GetTableView(myquery.SelectAll)
    End Function

    Public Shared Function GetCollectionStatusList() As DAL.COMMON.TableView
        Dim sql As String = "Select vw.*, CollectionStatus + '-' + CollectionStatusDescr As Title from CollectionStatus as vw ORDER BY TITLE"
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetLocationList() As DAL.COMMON.TableView
        Dim sql As String = "Select vw.*, Location + '-' + LocationDescr As Title from Location as vw ORDER BY TITLE"
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetOwnerList() As DAL.COMMON.TableView
        Dim sql As String = "Select vw.*, LoginCode + '-' + UserName As Title from Portal_users as vw Where InternalUser = 1 ORDER BY TITLE"
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetLetterList() As DAL.COMMON.TableView
        Dim sql As String = "Select vw.*, LetterCode + '-' + LetterDescr As Title from Letter as vw "
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetLocationUsers(ByVal alocationid As Integer) As DAL.COMMON.TableView
        Dim sql As String = "Select vw.* from vwLocationUsers vw where LocationId = " & alocationid
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetUserLocations(ByVal auserid As Integer) As DAL.COMMON.TableView
        Dim sql As String = "Select vw.* from vwLocationUsers vw where UserId = " & auserid
        Return DBO.GetTableView(sql)
    End Function

  End Class
