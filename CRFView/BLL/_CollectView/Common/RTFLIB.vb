Public Class RTFLIB
    Private Sub Replace(ByVal artf As Object, ByVal atag As String, ByVal avalue As String)
        Try
            artf = Strings.Replace(artf, atag, avalue)
        Catch
        End Try

    End Sub
    Private Sub ReplaceDate(ByVal artf As Object, ByVal atag As String, ByVal avalue As Object)
        Try
            If IsDate(avalue) Then
                Dim mydate As String = Date.Parse(avalue).ToShortDateString
                artf = Strings.Replace(artf, atag, " " & mydate)
            Else
                artf = Strings.Replace(artf, atag, " ")
            End If

        Catch ex As Exception
            artf = Strings.Replace(artf, atag, " ")
        End Try

    End Sub
    Private Sub ReplaceAmt(ByVal artf As Object, ByVal atag As String, ByVal avalue As Object)
        Try
            Dim myamt As String = Strings.FormatCurrency(avalue)
            artf = Strings.Replace(artf, atag, myamt)
        Catch
            artf = Strings.Replace(artf, atag, " " & Strings.FormatCurrency(0))
        End Try

    End Sub
End Class
