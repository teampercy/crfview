Namespace Collectview.Collections
    Public Interface LedgerTrx
        ReadOnly Property Distribution() As Distribution
        ReadOnly Property Rate() As Decimal
        ReadOnly Property Receipt() As TABLES.DebtAccountLedger
        Function Distribute() As Boolean
        Function Save() As Boolean
    End Interface
    Public Class Distribution
        Public AccountId As Integer = 0
        Public Amt As Decimal = 0
        Public DstAmt As Decimal = 0
        Public LedgerType As String = ""
        Public LedgerTypeId As Integer = 0
        Public Rate As Decimal = 0
        Public FeeAmt As Decimal = 0
        Public AsgAmt As Decimal = 0
        Public AdjAmt As Decimal = 0
        Public RcvAmt As Decimal = 0
        Public BalAmt As Decimal = 0
        Public IntAmt As Decimal = 0
        Public CostAmt As Decimal = 0
        Public DueAmt As Decimal = 0
        Public dstAsgAmt As Decimal = 0
        Public dstAdjAmt As Decimal = 0
        Public dstRcvAmt As Decimal = 0
        Public dstBalAmt As Decimal = 0
        Public dstIntAmt As Decimal = 0
        Public dstCostAmt As Decimal = 0
        Public dstDueAmt As Decimal = 0
        Public newAsgAmt As Decimal = 0
        Public newAdjAmt As Decimal = 0
        Public newRcvAmt As Decimal = 0
        Public newBalAmt As Decimal = 0
        Public newIntAmt As Decimal = 0
        Public newCostAmt As Decimal = 0
        Public newDueAmt As Decimal = 0
    End Class
    Public Class LedgerTrxBase
        Protected m_accountid As Integer
        Protected m_dist As New Distribution
        Protected m_client As New TABLES.Client
        Protected m_contract As New TABLES.ClientContract
        Protected m_account As New TABLES.DebtAccount
        Protected m_ledger As New TABLES.DebtAccountLedger
        Protected m_ds As DataSet
        Dim MYVIEW0 As DAL.COMMON.TableView
        Dim MYVIEW1 As DAL.COMMON.TableView
        Dim MYVIEW2 As DAL.COMMON.TableView
        Dim MYVIEW3 As DAL.COMMON.TableView
        Dim MYVIEW4 As DAL.COMMON.TableView
        Dim MYVIEW5 As DAL.COMMON.TableView
        Dim MYVIEW6 As DAL.COMMON.TableView
        Dim MYVIEW7 As DAL.COMMON.TableView

        Public Sub New(ByVal ACCOUNTID As Integer)
            m_accountid = ACCOUNTID
            Dim MYSPROC As New SPROCS.cview_Accounts_GetAccountInfoforLedgerTrx
            MYSPROC.AccountId = ACCOUNTID
            m_ds = DBO.GetDataSet(MYSPROC)
            MYVIEW0 = New DAL.COMMON.TableView(m_ds.Tables(0))
            MYVIEW1 = New DAL.COMMON.TableView(m_ds.Tables(1))
            MYVIEW2 = New DAL.COMMON.TableView(m_ds.Tables(2))
            MYVIEW3 = New DAL.COMMON.TableView(m_ds.Tables(3))
            MYVIEW4 = New DAL.COMMON.TableView(m_ds.Tables(4))
            MYVIEW5 = New DAL.COMMON.TableView(m_ds.Tables(5))
            MYVIEW0.FillEntity(m_account)
            MYVIEW2.FillEntity(m_client)
            MYVIEW3.FillEntity(m_contract)

            m_dist.dstAsgAmt = 0
            m_dist.dstIntAmt = 0
            m_dist.dstRcvAmt = 0
            m_dist.dstAdjAmt = 0
            m_dist.dstBalAmt = 0
            m_dist.dstCostAmt = 0
            m_dist.dstDueAmt = 0

            TotalDistributions()


        End Sub
        Public ReadOnly Property AccountId() As String
            Get
                Return m_accountid
            End Get
        End Property

        Public ReadOnly Property Rate() As String
            Get
                Return m_ds.Tables(4).Rows(0).Item(0)
            End Get
        End Property
        Public ReadOnly Property Distributions() As Distribution
            Get
                Return m_dist
            End Get
        End Property
        Public Function Distribute(ByVal amount As Decimal, ByVal aledgertype As String)
            Dim m_ledgertype As New TABLES.LedgerType
            MYVIEW5.FillByKeyValue("LEDGERTYPE", aledgertype, m_ledgertype)

            m_dist.AsgAmt = m_account.TotAsgAmt
            m_dist.IntAmt = m_account.TotIntAmt
            m_dist.RcvAmt = m_account.TotRcvAmt
            m_dist.AdjAmt = m_account.TotAdjAmt
            m_dist.BalAmt = m_account.TotBalAmt
            m_dist.CostAmt = m_account.TotCostAmt

            m_dist.DueAmt = m_dist.BalAmt + m_dist.IntAmt + m_dist.CostAmt
            m_dist.Amt = amount
            m_dist.DstAmt = m_dist.Amt
            Dim s As String = m_ledgertype.ApplyToCols, ss() As String, s1 As String
            ss = Split(s, ",")
            For Each s1 In ss
                If m_dist.DstAmt > 0 Then
                    Select Case s1
                        Case "2"
                            m_dist.dstRcvAmt = GetAmt(m_ledgertype.NoAutoDistribution, m_dist.BalAmt, m_dist.DstAmt)

                        Case "4"
                            m_dist.dstIntAmt = GetAmt(m_ledgertype.NoAutoDistribution, m_dist.IntAmt, m_dist.DstAmt)

                        Case "5"
                            m_dist.dstCostAmt = GetAmt(m_ledgertype.NoAutoDistribution, m_dist.CostAmt, m_dist.DstAmt)

                    End Select

                End If

            Next
            m_dist.dstRcvAmt = m_dist.dstRcvAmt + (m_dist.DstAmt * -1)
        End Function
        Public Sub TotalDistributions()

            ' move results
            m_dist.AsgAmt = m_account.TotAsgAmt
            m_dist.IntAmt = m_account.TotIntAmt
            m_dist.RcvAmt = m_account.TotRcvAmt
            m_dist.AdjAmt = m_account.TotAdjAmt
            m_dist.BalAmt = m_account.TotBalAmt
            m_dist.CostAmt = m_account.TotCostAmt

            m_dist.newAsgAmt = m_dist.AsgAmt + m_dist.dstAsgAmt
            m_dist.newAdjAmt = m_dist.AdjAmt + m_dist.dstAdjAmt
            m_dist.newRcvAmt = m_dist.RcvAmt + m_dist.dstRcvAmt
            m_dist.newBalAmt = m_dist.BalAmt + m_dist.dstAdjAmt + m_dist.newRcvAmt
            m_dist.newIntAmt = m_dist.IntAmt + m_dist.dstIntAmt
            m_dist.newCostAmt = m_dist.CostAmt + m_dist.dstCostAmt

            m_dist.newDueAmt = m_dist.newAsgAmt + m_dist.newRcvAmt + m_dist.newAdjAmt + m_dist.newIntAmt + m_dist.newCostAmt
            m_dist.dstDueAmt = m_dist.dstAsgAmt + m_dist.dstRcvAmt + m_dist.dstAdjAmt + m_dist.dstIntAmt + m_dist.dstCostAmt


        End Sub

        Private Function GetAmt(ByRef NoAutoDistribution As Object, ByRef amt1 As Object, ByRef amt2 As Object) As Object
            Dim x As Decimal
            If amt1 < 0 Then Return 0
            x = IIf(amt1 <= amt2, amt1, amt2)
            amt2 = amt2 - x
            Return x * -1

        End Function
    End Class

    Public Class PaymentToClient
        Inherits LedgerTrxBase
        Public Sub New(ByVal aacountid As Integer, ByVal ledgerid As Integer)
            MyBase.New(aacountid)

        End Sub
        Public Sub Save()

        End Sub
    End Class
    Public Class PaymentToAgency
        Inherits LedgerTrxBase
        Public Sub New(ByVal aacountid As Integer, ByVal ledgerid As Integer)
            MyBase.New(aacountid)


        End Sub

    End Class
    Public Class LedgerEntry
        Inherits LedgerTrxBase
        Public Sub New(ByVal aacountid As Integer, ByVal ledgerid As Integer)
            MyBase.New(aacountid)


        End Sub

    End Class


    'Public MustInherit Class LedgerTrxBase
    '    Implements LedgerTrx

    '    Protected m_account As Object
    '    Protected m_receipt As TABLES.DebtAccountLedger
    '    Protected m_contract As TABLES.ClientContract
    '    Protected m_ledgertype As TABLES.LedgerType
    '    Protected m_dist As Distribution
    '    Protected m_autodistribute As Boolean
    '    Public Sub New()
    '        m_dist = New Distribution
    '        m_ledgertype = New TABLES.LedgerType

    '    End Sub

    '    Public Function Distribute() As Boolean Implements LedgerTrx.Distribute


    '        m_dist.Rate = m_receipt.Rate
    '        m_dist.AsgAmt = m_account.TotAsgAmt
    '        m_dist.IntAmt = m_account.TotIntAmt
    '        m_dist.RcvAmt = m_account.TotRcvAmt
    '        m_dist.AdjAmt = m_account.TotAdjAmt
    '        m_dist.BalAmt = m_account.TotBalAmt
    '        m_dist.CostAmt = m_account.TotCostAmt

    '        m_dist.DueAmt = m_dist.BalAmt + m_dist.IntAmt + m_dist.CostAmt

    '        m_dist.dstAsgAmt = 0
    '        m_dist.dstIntAmt = 0
    '        m_dist.dstRcvAmt = 0
    '        m_dist.dstAdjAmt = 0
    '        m_dist.dstBalAmt = 0
    '        m_dist.dstCostAmt = 0
    '        m_dist.dstDueAmt = 0

    '        m_dist.Amt = m_receipt.TrxAmt
    '        m_dist.DstAmt = m_dist.Amt
    '        ' distribution logic
    '        'If m_ledgertype.LoadByKeyValue("LedgerType", m_receipt.LedgerType) = True Then
    '        '    m_receipt.LedgerTypeId = m_ledgertype.LedgerTypeId
    '        'Else
    '        '    Return False
    '        'End If

    '        If m_autodistribute = True Then
    '            m_receipt.LedgerTypeId = m_ledgertype.LedgerTypeId
    '            Dim s As String = m_ledgertype.ApplyToCols, ss() As String, s1 As String
    '            ss = Split(s, ",")
    '            For Each s1 In ss
    '                If m_dist.DstAmt > 0 Then
    '                    Select Case s1
    '                        Case "2"
    '                            m_dist.dstRcvAmt = GetAmt(m_ledgertype.NoAutoDistribution, m_dist.BalAmt, m_dist.DstAmt)

    '                        Case "4"
    '                            m_dist.dstIntAmt = GetAmt(m_ledgertype.NoAutoDistribution, m_dist.IntAmt, m_dist.DstAmt)

    '                        Case "5"
    '                            m_dist.dstCostAmt = GetAmt(m_ledgertype.NoAutoDistribution, m_dist.CostAmt, m_dist.DstAmt)

    '                    End Select

    '                End If

    '            Next
    '            m_dist.dstRcvAmt = m_dist.dstRcvAmt + (m_dist.DstAmt * -1)
    '        Else
    '            m_dist.dstAsgAmt = m_receipt.AsgAmt
    '            m_dist.dstIntAmt = m_receipt.IntAmt
    '            m_dist.dstRcvAmt = m_receipt.RcvAmt
    '            m_dist.dstAdjAmt = m_receipt.AdjAmt
    '            m_dist.dstCostAmt = m_receipt.CostAmt
    '        End If
    '        TotalDistributions()
    '        'IF  @CalcTotRcvOnPaidClnt = 1AND @IsRcvdByClient = 1 SET @BILLABLEAMT = @TOTAMT
    '        '	IF  @CalcTotRcvOnPaiduS = 1 AND @IsRcvdByClient = 0 SET @BILLABLEAMT = @TOTAMT
    '        '	if  @NoCommission =1 SET @BILLABLEAMT = 0

    '        m_receipt.BillableAmt = 0
    '        If m_contract.IsTrustAccount = True Then

    '            m_receipt.BillableAmt = m_dist.dstRcvAmt * -1
    '            If m_contract.IsInterestShared = True Then
    '                m_receipt.BillableAmt = (m_dist.dstRcvAmt + m_dist.dstIntAmt) * -1
    '            End If

    '            If m_contract.CalcTotRcvOnPaidUs = True And m_ledgertype.ToAgency = True Then
    '                m_receipt.BillableAmt = m_receipt.TrxAmt
    '            End If

    '            If m_contract.CalcTotRcvOnPaidClnt = True And m_ledgertype.ToClient = True Then
    '                m_receipt.BillableAmt = m_receipt.TrxAmt
    '            End If

    '        End If

    '        If m_receipt.Rate <> 0 And m_dist.dstRcvAmt <> 0 Then
    '            m_dist.FeeAmt = (m_receipt.BillableAmt) * m_receipt.Rate / 100
    '        End If

    '        m_receipt.ExtendedAmt = m_dist.FeeAmt
    '        m_receipt.AsgAmt = m_dist.dstAsgAmt
    '        m_receipt.AdjAmt = m_dist.dstAdjAmt
    '        m_receipt.RcvAmt = m_dist.dstRcvAmt
    '        m_receipt.IntAmt = m_dist.dstIntAmt
    '        m_receipt.CostAmt = m_dist.dstCostAmt

    '        m_receipt.LedgerTypeId = m_ledgertype.LedgerTypeId
    '        m_receipt.OwedAmt = Me.Distributions.BalAmt

    '        Return True

    '    End Function
    '    Protected Sub TotalDistributions()

    '        ' move results
    '        m_dist.AsgAmt = m_account.TotAsgAmt
    '        m_dist.IntAmt = m_account.TotIntAmt
    '        m_dist.RcvAmt = m_account.TotRcvAmt
    '        m_dist.AdjAmt = m_account.TotAdjAmt
    '        m_dist.BalAmt = m_account.TotBalAmt
    '        m_dist.CostAmt = m_account.TotCostAmt

    '        m_dist.DueAmt = m_dist.BalAmt + m_dist.IntAmt + m_dist.CostAmt
    '        m_dist.newAsgAmt = m_dist.AsgAmt + m_dist.dstAsgAmt
    '        m_dist.newAdjAmt = m_dist.AdjAmt + m_dist.dstAdjAmt
    '        m_dist.newRcvAmt = m_dist.RcvAmt + m_dist.dstRcvAmt
    '        m_dist.newBalAmt = m_dist.BalAmt + m_dist.dstAdjAmt + m_dist.newRcvAmt
    '        m_dist.newIntAmt = m_dist.IntAmt + m_dist.dstIntAmt
    '        m_dist.newCostAmt = m_dist.CostAmt + m_dist.dstCostAmt
    '        m_dist.newDueAmt = m_dist.newAsgAmt + m_dist.newRcvAmt + m_dist.newAdjAmt + m_dist.newIntAmt + m_dist.newCostAmt
    '        m_dist.dstDueAmt = m_dist.dstAsgAmt + m_dist.dstRcvAmt + m_dist.dstAdjAmt + m_dist.dstIntAmt + m_dist.dstCostAmt


    '    End Sub

    '    Public ReadOnly Property Distributions() As LedgerTrxBase.Distribution Implements LedgerTrx.Distributions
    '        Get
    '            Return m_dist
    '        End Get
    '    End Property
    '    Public ReadOnly Property Rate() As Decimal Implements LedgerTrx.Rate
    '        Get

    '        End Get
    '    End Property

    '    Public Function Save() As Boolean Implements LedgerTrx.Save
    '        'm_receipt.ReceiptDate = m_receipt.TrxDate
    '        'm_dbobj.SaveRow(m_receipt.Row)

    '        'm_dbobj.Exec()
    '        'm_dbobj.Dispose()
    '        Return True

    '    End Function
    '    Public ReadOnly Property Receipt() As TABLES.DebtAccountLedger Implements LedgerTrx.Receipt
    '        Get
    '            Return m_receipt
    '        End Get
    '    End Property

    '    Public ReadOnly Property TrxTable() As System.Data.DataTable Implements LedgerTrx.TrxTable
    '        Get
    '            '    Return m_receipt.Table
    '            Return Nothing
    '        End Get
    '    End Property
    '    Private Function GetAmt(ByRef NoAutoDistribution As Object, ByRef amt1 As Object, ByRef amt2 As Object) As Object
    '        Dim x As Decimal
    '        If amt1 < 0 Then Return 0
    '        x = IIf(amt1 <= amt2, amt1, amt2)
    '        amt2 = amt2 - x
    '        Return x * -1

    '    End Function
    '    Public Function GeRate() As Decimal
    '        Dim dt As DataTable, sql As String

    '        sql = "select DBO.fnGetContingencyRate(" & m_account.DebtAccountId & "," & m_account.TotAsgAmt & ") As Rate "
    '        dt = m_receipt.PassThruQuery(sql)
    '        If IsDBNull(dt.Rows(0).Item("Rate")) Then
    '            Return 0
    '        Else
    '            Return dt.Rows(0).Item("Rate")
    '        End If

    '    End Function
    '    Public Class Distribution
    '        Public AccountId As Integer = 0
    '        Public Amt As Decimal = 0
    '        Public DstAmt As Decimal = 0
    '        Public LedgerType As String = ""
    '        Public LedgerTypeId As Integer = 0
    '        Public Rate As Decimal = 0
    '        Public FeeAmt As Decimal = 0
    '        Public AsgAmt As Decimal = 0
    '        Public AdjAmt As Decimal = 0
    '        Public RcvAmt As Decimal = 0
    '        Public BalAmt As Decimal = 0
    '        Public IntAmt As Decimal = 0
    '        Public CostAmt As Decimal = 0
    '        Public DueAmt As Decimal = 0
    '        Public dstAsgAmt As Decimal = 0
    '        Public dstAdjAmt As Decimal = 0
    '        Public dstRcvAmt As Decimal = 0
    '        Public dstBalAmt As Decimal = 0
    '        Public dstIntAmt As Decimal = 0
    '        Public dstCostAmt As Decimal = 0
    '        Public dstDueAmt As Decimal = 0
    '        Public newAsgAmt As Decimal = 0
    '        Public newAdjAmt As Decimal = 0
    '        Public newRcvAmt As Decimal = 0
    '        Public newBalAmt As Decimal = 0
    '        Public newIntAmt As Decimal = 0
    '        Public newCostAmt As Decimal = 0
    '        Public newDueAmt As Decimal = 0
    '    End Class

    'End Class
    'Public Class PaymentToClient
    '    Inherits LedgerTrxBase
    '    Public Sub New(ByVal aAccount As CollectView.Views.vwDebtAccountList)
    '        MyBase.New()
    '        m_account = aAccount
    '        m_contract = New TABLES.ClientContract
    '        m_contract.Load(m_account.ContractId)

    '        m_receipt = New TABLES.DebtAccountLedger

    '        m_receipt.Create()
    '        m_receipt.DebtAccountId = m_account.DebtAccountId
    '        m_receipt.LedgerType = "PMTC"
    '        m_receipt.TrxDate = Today
    '        m_receipt.RemitDate = Today
    '        m_autodistribute = True
    '        m_receipt.Rate = Me.GeRate
    '        m_receipt.HoldDays = 0
    '        m_receipt.IsOnHold = 0

    '        m_dbobj = New CollectView.SPROCS.uspnq_Accounts_PaymentToClient

    '    End Sub

    'End Class
    'Public Class PaymentToAgency
    '    Inherits LedgerTrxBase
    '    Public Sub New(ByVal aAccount As VIEWS.vwDebtAccountList)
    '        MyBase.New()
    '        m_account = aAccount
    '        'm_contract = New Tables.ClientContract
    '        'm_contract.Load(m_account.ContractId)

    '        'm_receipt = New Tables.DebtAccountLedger

    '        'm_receipt.Create()
    '        'm_receipt.DebtAccountId = m_account.DebtAccountId
    '        'm_receipt.LedgerType = "PMTA"
    '        'm_receipt.TrxDate = Today
    '        'm_receipt.RemitDate = Today
    '        'm_autodistribute = True
    '        'm_receipt.Rate = Me.GeRate
    '        'm_receipt.HoldDays = 0
    '        'm_receipt.IsOnHold = 0
    '        'If m_contract.HoldDaysOnPmts > 0 Then
    '        '    m_receipt.RemitDate = DateAdd(DateInterval.Day, CDbl(m_contract.HoldDaysOnPmts), Today)
    '        'End If

    '        'm_dbobj = New CollectView.SPROCS.uspnq_Accounts_PaymentToAgency

    '    End Sub
    'End Class
    'Public Class LedgerEntry
    '    Inherits LedgerTrxBase
    '    Public Sub New(ByVal aAccount As VIEWS.vwDebtAccountList, Optional ByVal aledgerid As Integer = 0)
    '        MyBase.New()

    '        m_account = aAccount
    '        m_contract = New TABLES.ClientContract
    '        m_contract.Load(m_account.ContractId)

    '        m_receipt = New TABLES.DebtAccountLedger
    '        If aledgerid = 0 Then
    '            m_receipt.Create()
    '            m_receipt.DebtAccountId = m_account.DebtAccountId
    '            m_receipt.TrxDate = Today
    '            m_receipt.RemitDate = Today
    '            m_receipt.Rate = Me.GeRate
    '            m_autodistribute = False
    '        Else
    '            m_receipt.Load(aledgerid)
    '            m_autodistribute = False
    '        End If

    '        Me.TotalDistributions()
    '        m_dbobj = New CollectView.SPROCS.uspnq_Accounts_LedgerEntry

    '    End Sub


    'End Class
    'Public Class CreditCardPayment
    '    Inherits LedgerTrxBase
    '    Public Sub New(ByVal aAccount As VIEWS.vwDebtAccountList)
    '        MyBase.New()
    '        'm_account = aAccount
    '        'm_contract = New TABLES.ClientContract
    '        'm_contract.Load(m_account.ContractId)

    '        'm_receipt = New TABLES.DebtAccountLedger

    '        'm_receipt.Create()
    '        'm_receipt.DebtAccountId = m_account.DebtAccountId
    '        'm_receipt.RemitDate = Today
    '        'm_autodistribute = True
    '        'm_receipt.Rate = Me.GeRate

    '        'm_dbobj = New CollectView.SPROCS.uspnq_Accounts_PaymentToClient

    '    End Sub

    'End Class
    'Public Class LedgerReversalEntry
    '    'Dim m_obj As CRUDS.DebtAccountLedger
    '    'Dim m_account As DebtManagement.Account
    '    'Dim m_dist As New Ledger.Distribution
    '    'Dim m_old As CV.Database.RowMaps.Ledgers
    '    'Dim m_receipt As CV.DataBase.RowMaps.Receipt
    '    'Dim m_receiptinfo As CV.Database.RowMaps.Receipt
    '    'Dim m_IsNSF As Boolean
    '    'Public Sub New(ByVal aAccount As COLLECTVIEW.Views.vwDebtAccountList, ByVal aDebtAccountLedgerid As Integer, ByVal aIsNSF As Boolean)
    '    '    m_account = aAccount
    '    '    m_IsNSF = aIsNSF

    '    '    m_obj = New CRUDS.DebtAccountLedger
    '    '    m_obj.Load(aDebtAccountLedgerid)
    '    '    '   m_old = New CV.Database.RowMaps.Ledgers(m_obj.Table)

    '    '    m_dist.LedgerType = m_old.LedgerType
    '    '    m_dist.LedgerTypeId = m_old.LedgerTypeId

    '    '    m_dist.AsgAmt = m_account.AccountInfo.TotAsgAmt
    '    '    m_dist.RcvAmt = m_account.AccountInfo.TotRcvAmt
    '    '    m_dist.AdjAmt = m_account.AccountInfo.TotAdjAmt
    '    '    m_dist.IntAmt = m_account.AccountInfo.TotIntAmt
    '    '    m_dist.BalAmt = m_account.AccountInfo.TotBalAmt
    '    '    m_dist.CostAmt = m_account.AccountInfo.TotCostAmt
    '    '    m_dist.DueAmt = m_account.AccountInfo.TotalDue

    '    '    m_dist.dstAsgAmt = m_old.AsgAmt * -1
    '    '    m_dist.dstAdjAmt = m_old.AdjAmt * -1
    '    '    m_dist.dstRcvAmt = m_old.RcvAmt * -1
    '    '    m_dist.dstIntAmt = m_old.IntAmt * -1
    '    '    m_dist.dstCostAmt = m_old.CostAmt * -1
    '    '    m_dist.Rate = m_old.Rate
    '    '    m_dist.FeeAmt = m_old.ExtendedAmt * -1

    '    '    m_dist.newAsgAmt = m_dist.AsgAmt + m_dist.dstAsgAmt
    '    '    m_dist.newAdjAmt = m_dist.AdjAmt + m_dist.dstAdjAmt
    '    '    m_dist.newRcvAmt = m_dist.RcvAmt + m_dist.dstRcvAmt
    '    '    m_dist.newBalAmt = m_dist.BalAmt + m_dist.dstAdjAmt + m_dist.newRcvAmt
    '    '    m_dist.newIntAmt = m_dist.IntAmt + m_dist.dstIntAmt
    '    '    m_dist.newCostAmt = m_dist.CostAmt + m_dist.dstCostAmt
    '    '    m_dist.newDueAmt = m_dist.newAsgAmt + m_dist.newRcvAmt + m_dist.newAdjAmt + m_dist.newIntAmt + m_dist.newCostAmt
    '    '    m_dist.dstDueAmt = m_dist.dstAsgAmt + m_dist.dstRcvAmt + m_dist.dstAdjAmt + m_dist.dstIntAmt + m_dist.dstCostAmt

    '    '    m_dist.FeeAmt = m_old.ExtendedAmt * -1



    '    'End Sub
    '    'Public ReadOnly Property Distribution() As Ledger.Distribution
    '    '    Get
    '    '        Return m_dist
    '    '    End Get
    '    'End Property
    '    'Public Sub DELETE()
    '    '    m_obj.Delete()

    '    'End Sub
    '    'Public Sub Save()

    '    '    Dim m_receipt As New Tables.DebtAccountReceipt

    '    '    m_receipt.Create()

    '    '    m_receipt.DebtAccountId = m_account.AccountInfo.DebtAccountId
    '    '    '    m_receipt.DebtId = m_account.AccountInfo.DebtId
    '    '    m_receipt.CollectorId = m_account.AccountInfo.OwnerId
    '    '    m_receipt.RemitDate = Now
    '    '    m_receipt.TrxDate = Now
    '    '    m_receipt.LedgerType = m_old.LedgerType
    '    '    m_receipt.LedgerTypeId = m_old.LedgerTypeId

    '    '    m_receipt.TrxAmt = m_old.TrxAmt * -1
    '    '    m_receipt.AsgAmt = m_old.AsgAmt * -1
    '    '    m_receipt.AdjAmt = m_old.AdjAmt * -1
    '    '    m_receipt.RcvAmt = m_old.RcvAmt * -1
    '    '    m_receipt.IntAmt = m_old.IntAmt * -1
    '    '    m_receipt.CostAmt = m_old.CostAmt * -1

    '    '    m_receipt.Rate = m_old.Rate
    '    '    m_receipt.BillableAmt = m_old.BillableAmt * -1
    '    '    m_receipt.ExtendedAmt = m_old.ExtendedAmt * -1
    '    '    '   m_receipt.IsNSF = m_IsNSF
    '    '    '  m_receipt.IsOnHold = False

    '    '    'Dim m_obj As New SPROCS.DebtAccount.LedgerEntry
    '    '    'm_obj.SaveRow(m_receipt.Row)
    '    '    'm_obj.Exec()
    '    '    'm_obj.Dispose()

    '    'End Sub


End Namespace
