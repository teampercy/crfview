Imports System.Data
Namespace Collectview.Collections
    Public Class AccountInfo
        Dim CurrentUser As Users.CurrentUser
        Dim myds As DataSet
        Public Sub New(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer)
            Dim mysproc As New SPROCS.cview_Accounts_GetAccountInfo
            mysproc.AccountId = aitemid
            myds = DBO.GetDataSet(mysproc)
        End Sub
        Public Function AccountView() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(0), "")
        End Function
        Public Function Ledger() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(1), "")
        End Function
        Public Function History() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(2), "")
        End Function
        Public Function Documents() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(3), "")
        End Function
        Public Function Address() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(6), "")
        End Function
        Public Function Client() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(4), "")
        End Function
        Public Function Contract() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(5), "")
        End Function
    End Class
End Namespace
