Imports HDS
Imports HDS.Reporting
Imports HDS.Reporting.COMMON
Namespace Collectview.Accounting
    Public Class Processes
        Public Shared Function PrintTrustAuditLists(ByRef auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal asproc As SPROCS.cview_Accounting_GetAuditList, ByVal printmode As HDS.Reporting.COMMON.PrintMode, ByVal spreadsheet As Boolean) As Integer
            With asproc
            End With
            Dim MYVIEW As DAL.COMMON.TableView = DBO.GetTableView(asproc)
            akeyvals.Add("RECORDCOUNT", MYVIEW.Count)
            If MYVIEW.Count > 0 Then
                Accounting.Reports.TrustAuditLists(auser, akeyvals, MYVIEW, printmode, spreadsheet)
            End If
            Return MYVIEW.Count

        End Function
        Public Shared Function CreateInvoice(ByRef auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal asproc As SPROCS.cview_Accounting_CreateInvoice) As Integer
            Return DBO.ExecScalar(asproc)

        End Function
        Public Shared Function VoidInvoice(ByRef auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal asproc As SPROCS.cview_Accounting_VoidInvoice) As Integer
            Return DBO.ExecScalar(asproc)

        End Function
        Public Shared Function PrintTrustRecaps(ByRef auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal asproc As SPROCS.cview_Accounting_GetFinDocs, ByVal printmode As HDS.Reporting.COMMON.PrintMode, ByVal spreadsheet As Boolean) As Integer
            With asproc
            End With
            Dim Myds As DataSet = DBO.GetDataSet(asproc)

            Dim myview As New DAL.COMMON.TableView(Myds.Tables(0))
            akeyvals.Add("RECORDCOUNT", myview.Count)
            If myview.Count > 0 Then
                Accounting.Reports.TrustAuditLists(auser, akeyvals, myview, printmode, spreadsheet)
            End If
            Return myview.Count

        End Function
        Public Shared Function PrintInvoices(ByRef auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal asproc As SPROCS.cview_Accounting_GetFinDocs, ByVal printmode As HDS.Reporting.COMMON.PrintMode, ByVal spreadsheet As Boolean) As Integer
            With asproc
            End With
            Dim Myds As DataSet = DBO.GetDataSet(asproc)

            Dim myview As New DAL.COMMON.TableView(Myds.Tables(1))
            akeyvals.Add("RECORDCOUNT", myview.Count)
            If myview.Count > 0 Then
                Accounting.Reports.TrustInvoice(auser, akeyvals, myview, printmode, spreadsheet)
            End If

            Dim myview1 As New DAL.COMMON.TableView(Myds.Tables(2))
            akeyvals.Add("RECORDCOUNT", myview1.Count)
            If myview1.Count > 0 Then
                Accounting.Reports.TrustStatement(auser, akeyvals, myview, printmode, spreadsheet)
            End If

            Return myview.Count

        End Function
    End Class
End Namespace
