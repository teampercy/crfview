Public Class Queries
    Public Shared Function GetView(ByVal aentity As DAL.Providers.ITable, Optional ByVal Cached As Boolean = False)
        Dim myview As DAL.COMMON.TableView = Nothing
        If Cached = False Or IsNothing(HttpContext.Current.Cache(aentity.TableName)) = True Then
            Dim myquery As New DAL.COMMON.Query(aentity)
            myview = DBO.GetTableView(myquery.SelectAll)
            If Cached = True Then
                HttpContext.Current.Cache.Insert(aentity.TableName, myview)
            End If
        Else
            myview = HttpContext.Current.Cache.Item(aentity.TableName)
        End If
        Return myview
    End Function
    Public Shared Function GetViewForConfigTable(ByRef atable As DAL.Providers.ITable) As DAL.COMMON.TableView
        Dim myquery As New DAL.COMMON.Query(atable)
        Dim sql As String = myquery.SelectAll
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetLienUsers() As DAL.COMMON.TableView
        Dim mytable As New CRFDB.TABLES.Portal_Users
        Dim myquery As New DAL.COMMON.Query(mytable)
        myquery.AddCondition(DAL.COMMON.LogicalOperators._And, Portal_Users.ColumnNames.InternalUser, DAL.COMMON.Conditions._IsEqual, 1)
        Dim sql As String = myquery.BuildQuery
        Return DBO.GetTableView(sql)

    End Function
    Public Shared Function GetInternalUsers() As DAL.COMMON.TableView

        Dim mytable As New CRFDB.TABLES.Portal_Users
        Dim myquery As New DAL.COMMON.Query(mytable)
        myquery.AddCondition(DAL.COMMON.LogicalOperators._And, Portal_Users.ColumnNames.InternalUser, DAL.COMMON.Conditions._IsEqual, 1)
        Dim sql As String = myquery.BuildQuery
        Return DBO.GetTableView(sql)

    End Function
    Public Shared Function GetServiceTeams() As DAL.COMMON.TableView
        Dim mytable As New CRFDB.TABLES.Team
        Dim myquery As New DAL.COMMON.Query(mytable)
        myquery.AddCondition(DAL.COMMON.LogicalOperators._And, TABLES.Team.ColumnNames.IsServiceTeam, DAL.COMMON.Conditions._IsEqual, 1)
        Dim sql As String = myquery.BuildQuery
        Return DBO.GetTableView(sql)

    End Function
    Public Shared Function GetClientType() As DAL.COMMON.TableView
        Dim mytable As New CRFDB.TABLES.ClientType
        Dim myquery As New DAL.COMMON.Query(mytable)
        Dim sql As String = myquery.SelectAll
        Return DBO.GetTableView(sql)

    End Function
    Public Shared Function GetSalesTeams() As DAL.COMMON.TableView
        Dim mytable As New CRFDB.TABLES.Team
        Dim myquery As New DAL.COMMON.Query(mytable)
        myquery.AddCondition(DAL.COMMON.LogicalOperators._And, TABLES.Team.ColumnNames.IsSalesTeam, DAL.COMMON.Conditions._IsEqual, 1)
        Dim sql As String = myquery.BuildQuery
        Return DBO.GetTableView(sql)

    End Function
    Public Shared Function StateInfo() As DAL.COMMON.TableView
        Dim mystate As New CRFDB.TABLES.StateInfo
        Dim myquery As New DAL.COMMON.Query(mystate)
        Return DBO.GetTableView(myquery.SelectAll)

    End Function
    Public Shared Function StateForms() As DAL.COMMON.TableView
        Dim mystate As New CRFDB.TABLES.StateForms
        Dim myquery As New DAL.COMMON.Query(mystate)
        Return DBO.GetTableView(myquery.SelectAll)

    End Function
    Public Shared Function StateFormFields() As DAL.COMMON.TableView
        Dim mystate As New CRFDB.TABLES.StateFormFields
        Dim myquery As New DAL.COMMON.Query(mystate)
        Return DBO.GetTableView(myquery.SelectAll)

    End Function

    Public Shared Function StateFormNotices() As DAL.COMMON.TableView
        Dim mystate As New CRFDB.TABLES.StateFormNotices
        Dim myquery As New DAL.COMMON.Query(mystate)
        Return DBO.GetTableView(myquery.SelectAll)
    End Function
    Public Shared Function JobStatus() As DAL.COMMON.TableView
        Dim mystate As New CRFDB.TABLES.JobStatus
        Dim myquery As New DAL.COMMON.Query(mystate)
        Return DBO.GetTableView(myquery.SelectAll)
    End Function
    Public Shared Function JobStatusGroup() As DAL.COMMON.TableView
        Dim mystate As New CRFDB.TABLES.JobStatusGroup
        Dim myquery As New DAL.COMMON.Query(mystate)
        Return DBO.GetTableView(myquery.SelectAll)
    End Function
    Public Shared Function JobDesks() As DAL.COMMON.TableView
        Dim mystate As New CRFDB.TABLES.JobDesks
        Dim myquery As New DAL.COMMON.Query(mystate)
        Return DBO.GetTableView(myquery.SelectAll)
    End Function
    Public Shared Function CollectionStatusGroup() As DAL.COMMON.TableView
        Dim mystate As New CRFDB.TABLES.CollectionStatusGroup
        Dim myquery As New DAL.COMMON.Query(mystate)
        Return DBO.GetTableView(myquery.SelectAll)
    End Function
    Public Shared Function ClientViewGroups() As DAL.COMMON.TableView
        Dim mystate As New CRFDB.TABLES.ClientViewGroup
        Dim myquery As New DAL.COMMON.Query(mystate)
        Return DBO.GetTableView(myquery.SelectAll)
    End Function
    Public Shared Function GetMainClients() As DataView
        Dim mysproc As New SPROCS.uspbo_CRM_GetMainClients
        Return DBO.GetTableView(mysproc)
    End Function
    Public Shared Function GetAllClients() As DataView
        Dim sql As String = "Select ClientId As ClientId, ClientCode, ClientCode + '-' + ClientName As ClientName from Client Where IsBranch = 0"
        Return DBO.GetTableView(sql)
    End Function

    Public Shared Function GetServiceCodes() As DataView
        Dim sql As String = "Select * from vwClientCollectionServiceCode"
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetLiensActiveClients() As DataView
        Dim sql As String = "Select ClientId, ClientCode,  ClientCode + '-' + ClientName As ClientName from vwClientLienClients"
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetLienClients() As DataView
        Dim sql As String = "Select ClientId As ClientId,  ClientCode, ClientCode + '-' + ClientName As ClientName from Client Where IsLienClient = 1 and IsBranch = 0 and IsInactive = 0"
        Return DBO.GetTableView(sql)
    End Function

    Public Shared Function GetCollectionClients() As DataView
        Dim sql As String = "Select ClientId As ClientId,  ClientCode, ClientCode + '-' + ClientName As ClientName from Client Where IsCollectionClient = 1 and IsBranch = 0 and IsInactive = 0"
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetLienReportFiles() As DAL.COMMON.TableView
        Dim sql As String = "Select ReportFileId,  ReportFilename from ReportFilenames Where ReportType = 'L'"
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetCollectionReportFiles() As DAL.COMMON.TableView
        Dim sql As String = "Select ReportFileId,  ReportFilename from ReportFilenames Where ReportType = 'C'"
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetCollectionStatusList() As DAL.COMMON.TableView
        Dim sql As String = "Select vw.*, CollectionStatus + '-' + CollectionStatusDescr As Title from CollectionStatus as vw ORDER BY TITLE"
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetLocationList() As DAL.COMMON.TableView
        Dim sql As String = "Select vw.*, Location + '-' + LocationDescr As Title from Location as vw ORDER BY TITLE"
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetOwnerList() As DAL.COMMON.TableView
        Dim sql As String = "Select vw.*, LoginCode + '-' + UserName As Title from Portal_users as vw Where InternalUser = 1 ORDER BY TITLE"
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetClientUsers() As DAL.COMMON.TableView
        Dim sql As String = "Select * from vwclientuser as vw  order by friendlyname"
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetClientUserList() As DAL.COMMON.TableView
        Dim sql As String = "Select client.clientcode, * from portal_users join client on portal_users.clientid = client.clientid where internaluser = 0"
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetLienInterfaces(ByVal auser As Users.CurrentUser) As HDS.DAL.COMMON.TableView
        Dim sql As String = "Select * from clientinterface VW where INTERFACETYPE = 'L' "
        Return DBO.GetTableView(sql)

    End Function
    Public Shared Function GetJobViewInterfaces(ByVal auser As Users.CurrentUser) As HDS.DAL.COMMON.TableView
        Dim sql As String = "Select * from clientinterface VW where INTERFACETYPE = 'J' "
        Return DBO.GetTableView(sql)

    End Function
    Public Shared Function GetCollectionInterfaces(ByVal auser As Users.CurrentUser) As HDS.DAL.COMMON.TableView
        Dim sql As String = "Select * from clientinterface VW where INTERFACETYPE = 'C' "
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetClientContractList(ByVal ClientId As Integer) As HDS.DAL.COMMON.TableView
        Dim sql As String = "Select * from ClientContract Where BillingClientId = '" & ClientId & "'"
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetClientInterfaceList() As HDS.DAL.COMMON.TableView
        Dim sql As String = "Select * from ClientInterface"
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetSelectedClientList(ByVal ClientcodeList As String) As HDS.DAL.COMMON.TableView
        Dim sql As String = "Select concat(Clientcode,'-',ClientName) as Clientname,Clientcode from client where Clientcode in(select value from fn_Split('" & ClientcodeList & "',',')) and isBranch=0"
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetTaskList() As HDS.DAL.COMMON.TableView
        Dim sql As String = "Select * from Tasks_TaskDef"
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetCollectionRateList() As HDS.DAL.COMMON.TableView
        Dim sql As String = "Select * From CollectionRate"
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetLawList() As HDS.DAL.COMMON.TableView
        Dim sql As String = "Select * from LawListInfo"
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetCollectionActionTypes() As HDS.DAL.COMMON.TableView
        Dim sql As String = "Select * From CollectionActionCategories"
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetPriorityList() As HDS.DAL.COMMON.TableView
        Dim sql As String = "select *,PriorityCode + '-' + PriorityName as FriendlyName from PriorityTypes"
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetReportCatagories() As HDS.DAL.COMMON.TableView
        Dim sql As String = "select Id,ReportCategory from ReportCatagories"
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetJobActionsList() As HDS.DAL.COMMON.TableView
        Dim sql As String = "Select Id,Description,NextCall,IsAll,IsClient,IsVerifier,IsLienVerifier,IsDataEntry,NotifyClient,IsChangeStatus,IsChangeDesk,NewStatusId,NewDeskId,EventTypeId,IsBillableEvent,IsDropReview,IsNotation,HistoryNote,IsSystem,CategoryType,PublicJobBox,FederalJobBox,CustRecBox,CustFaxRecBox,GCRecBox,GCFaxRecBox,AccessorBox,ClientRecBox,OnlineRecBox,CustChangeBox,GCChangeBox,OpenItem1Box,OpenItem2Box,OPenItem3Box,CustAutoFaxBox,GCAutoFaxBox,JobChangeBox,LenderChangeBox,OwnerChangeBox,CancelLienRequestBox,CancelLienReleaseBox,CancelBondClaimBox,CancelBondReleaseBox,CancelStopNoticeBox,CancelStopReleaseBox,CancelNTOBox,CancelNOIBox,CancelLienExtensionBox,PromptActionQty,ValidateForNoticeSent,DesigneeChangeBox,IsTXJobAction,DeskChangeBox,IsMailReturnAction,NOCBox,IsReviewed,IsMailDeliveredAction,IsJobView,IsChangeJVStatus,NewJobViewStatusId,IsChangeJVDesk,NewJobViewDeskId,NCInfoBox from JobActions Where 1 = 1  And IsDataEntry = '1'"
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetCollectionUsers() As DAL.COMMON.TableView
        Dim sql As String = "Select vw.* from Portal_Users vw (nolock) where InternalUser = 1 And ModuleList Like '%COLL%' "
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetLocationUsers(ByVal alocationid As Integer) As DAL.COMMON.TableView
        Dim sql As String = "Select vw.* from vwLocationUsers vw (nolock) where LocationId = " & alocationid
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function GetLatestLocationId() As String
        Dim sql As String = "SELECT TOP 1 LocationId FROM Location ORDER BY LocationId DESC"
        Return DBO.ExecScalar(sql).ToString()
    End Function
    Public Shared Function GetSelectedUsersList(ByVal UserType As String, ByVal UserIdList As String) As HDS.DAL.COMMON.TableView
        Dim sql As String = ""
        If UserType = "Client" Then
            sql = "Select * from Portal_users where ID in(select value from fn_Split('" & UserIdList & "',',')) and Internaluser=0"
        End If
        If UserType = "Internal" Then
            sql = "Select * from Portal_users where ID in(select value from fn_Split('" & UserIdList & "',',')) and Internaluser=1"
        End If
        Return DBO.GetTableView(sql)
    End Function
    Public Shared Function JobDeskList() As DAL.COMMON.TableView
        Dim mysql As String = "select *,DESKNUM + '-' + DeskName as FriendlyName from jobdesks"
        Return DBO.GetTableView(mysql)

    End Function
    Public Shared Function JobStatusList() As DAL.COMMON.TableView
        Dim mysql As String = "select *,JOBSTATUS + '-' + JOBSTATUSDESCR as FriendlyName from jobstatus"
        Return DBO.GetTableView(mysql)

    End Function
    Public Shared Function JobViewDeskList() As DAL.COMMON.TableView
        Dim mysql As String = "select *,DESKNUM + '-' + DeskName as FriendlyName from jobViewdesks"
        Return DBO.GetTableView(mysql)

    End Function
    Public Shared Function JobViewStatusList() As DAL.COMMON.TableView
        Dim mysql As String = "select *,JOBVIEWSTATUS + '-' + Description as FriendlyName from jobViewstatus"
        Return DBO.GetTableView(mysql)

    End Function
    Public Shared Function JobActionCategoriesList() As DAL.COMMON.TableView
        Dim mysql As String = "Select * From JobActionCategories"
        Return DBO.GetTableView(mysql)
    End Function

    Public Shared Function JobDeskGroupList() As DAL.COMMON.TableView
        Dim mysql As String = "select * from JobDeskGroup"
        Return DBO.GetTableView(mysql)
    End Function
    Public Shared Function MailReturnJobActionList() As DAL.COMMON.TableView
        Dim mysql As String = "Select * from JobActions Where IsMailReturnAction = 1"
        Return DBO.GetTableView(mysql)
    End Function
    Public Shared Function MailReturnReasonList() As DAL.COMMON.TableView
        Dim mysql As String = "Select * from MailReturnReason"
        Return DBO.GetTableView(mysql)
    End Function
    Public Shared Function MailReturnResearchList() As DAL.COMMON.TableView
        Dim mysql As String = "Select * from MailReturnResearch"
        Return DBO.GetTableView(mysql)
    End Function
    Public Shared Function MailDeliveredResultList() As DAL.COMMON.TableView
        Dim mysql As String = "Select * from MailDeliveredResults"
        Return DBO.GetTableView(mysql)
    End Function
    Public Shared Function MailDeliveredJobActionList() As DAL.COMMON.TableView
        Dim mysql As String = "Select * from JobActions Where IsMailDeliveredAction = 1"
        Return DBO.GetTableView(mysql)
    End Function
    Public Shared Function LienReportsList() As DAL.COMMON.TableView
        Dim mysql As String = "Select * from LienReports"
        Return DBO.GetTableView(mysql)
    End Function
    Public Shared Function ReportFieldNamesList() As DAL.COMMON.TableView
        Dim mysql As String = "Select * from ReportFieldNames where ReportType='L'"
        Return DBO.GetTableView(mysql)
    End Function
End Class
