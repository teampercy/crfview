Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Web.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Xml
Imports System.Xml.Schema
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections.Specialized
Imports System.Configuration.Provider
Imports System.Web.Hosting
Imports System.Web.Management
Imports System.Security.Permissions
Imports System.Text
Imports System.Security.Cryptography
Namespace Providers
    Friend Class SQLCollectionsDataEntryProvider
        Inherits ProviderBase
        Implements ICollectionsDataEntryProvider
        Public Function GetAccountsSubmittedthruWeb(ByVal auser As Users.CurrentUser, ByVal MyView As HDS.DAL.COMMON.TableView, ByVal printmode As Common.PrintMode) As String Implements ICollectionsDataEntryProvider.GetAccountsSubmittedthruWeb

            If MyView.Count < 1 Then
                Return Nothing
            End If

            If printmode = Common.PrintMode.PrintToPDF Then
                Dim myreport As New BLL.Common.StandardReport
                myreport.ReportDefPath = auser.SettingsFolder & "Reports\WebReportsCollections.xml"
                myreport.OutputFolder = auser.OutputFolder
                myreport.UserName = auser.UserName
                myreport.ReportName = "AccountsSubmitted"
                myreport.TableIndex = 1
                myreport.DataView = MyView
                myreport.PrintMode = printmode
                myreport.Sort = ""
                myreport.RenderReport()
                If myreport.ReportFileName.Length < 1 Then
                    Return Nothing
                Else
                    Return myreport.ReportFileName
                End If

            End If

            If printmode.PrinttoExcel Then
                Dim myspreadsheetpath As String = Common.FileOps.GetFileName(auser, "AccounsSubmitted")
                MyView.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.ServiceCode)
                MyView.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.AccountNo)
                MyView.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.DebtorName)
                MyView.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.ContactName)
                MyView.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.AddressLine1)
                MyView.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.AddressLine2)
                MyView.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.City)
                MyView.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.State)
                MyView.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.PostalCode)
                MyView.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.PhoneNo)
                MyView.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.Fax)
                MyView.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.Email)
                MyView.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.ReferalDate)
                MyView.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.LastServiceDate)
                MyView.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.LastPaymentDate)
                MyView.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.TotAsgAmt)
                MyView.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.AccountNote)
                MyView.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.BatchId)
                MyView.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.BatchDebtAccountId)
                MyView.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.SubmittedByUserId)
                MyView.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.ClientAgentCode)

                If MyView.ExportToCSV(myspreadsheetpath) = True Then
                    Return myspreadsheetpath
                Else
                    Return Nothing
                End If

            End If

            Return Nothing

        End Function
        Public Function GetMyOpenBatchList(ByVal auser As Users.CurrentUser) As HDS.DAL.COMMON.TableView Implements ICollectionsDataEntryProvider.GetMyOpenBatchList
            Dim sql As String = "Select BatchId As BatchId, BatchStatus, BatchDate from Batch Where SubmittedById = '" & auser.Id & "' and BatchType = 'COLL' And BatchStatus = 'OPEN'"
            Return DBO.Provider.GetTableView(sql)
        End Function
        Public Function PostWebPlacements(ByVal auser As Users.CurrentUser) As System.Data.DataSet Implements ICollectionsDataEntryProvider.PostWebPlacements
            Dim mysproc As New BLL.CRFDB.SPROCS.uspbo_ClientView_BatchNewAccounts
            Return DBO.Provider.GetDataSet(mysproc)
        End Function
        Public Function GetAccountInBatches(ByVal auser As Users.CurrentUser, ByVal asproc As CRFDB.SPROCS.cview_Reports_GetAccountsInBatches) As HDS.DAL.COMMON.TableView Implements ICollectionsDataEntryProvider.GetAccountInBatches
            Return DBO.Provider.GetTableView(asproc)
        End Function
        Public Function GetCollectionClients(ByVal auser As Users.CurrentUser) As HDS.DAL.COMMON.TableView Implements ICollectionsDataEntryProvider.GetCollectionClients
            Return Queries.GetCollectionClients
        End Function
        Public Function GetNewAccountInfo(ByVal auser As Users.CurrentUser, ByVal anewaccountid As String) As System.Data.DataSet Implements ICollectionsDataEntryProvider.GetNewAccountInfo
            Dim MYSPROC As New CRFDB.SPROCS.uspbo_CollDataEntry_GetNewAccountInfo
            MYSPROC.BatchDebtAccountId = anewaccountid
            Return DBO.Provider.GetDataSet(MYSPROC)
        End Function
        Public Function GetNewAccountList(ByVal auser As Users.CurrentUser, ByVal asproc As CRFDB.SPROCS.cview_Reports_GetAccountsInBatches) As HDS.DAL.COMMON.TableView Implements ICollectionsDataEntryProvider.GetNewAccountList
            Return DBO.Provider.GetTableView(asproc)
        End Function
        Public Function ImportEDI(ByVal auser As Users.CurrentUser, ByVal afilepath As String, ByVal acilentid As String, ByVal ainterfaceid As String) As Boolean Implements ICollectionsDataEntryProvider.ImportEDI

        End Function
        Public Function PostBatches(ByVal auser As Users.CurrentUser) As Boolean Implements ICollectionsDataEntryProvider.PostBatches
            Dim MYSPROC As New CRFDB.SPROCS.uspbo_Processing_PostCollBatches
            Return DBO.Provider.ExecScalar(MYSPROC)

        End Function

        Public Function PrintBatchAccountInfo(ByVal auser As Users.CurrentUser, ByVal aview As HDS.DAL.COMMON.TableView, ByVal aprintmode As Common.PrintMode) As String Implements ICollectionsDataEntryProvider.PrintBatchAccountInfo
            Dim MYSPROC As New CRFDB.SPROCS.uspbo_Processing_PostCollBatches
            Return DBO.Provider.ExecScalar(MYSPROC)
        End Function
        Public Function PrintBatchList(ByVal auser As Users.CurrentUser, ByVal aprintmode As Common.PrintMode, ByVal aview As HDS.DAL.COMMON.TableView) As String Implements ICollectionsDataEntryProvider.PrintBatchList
            Return Collections.Reports.BatchList(auser, aview, aprintmode)
        End Function
        Public Function GetUserList(ByVal auser As Users.CurrentUser) As HDS.DAL.COMMON.TableView Implements ICollectionsDataEntryProvider.GetUserList
            Return Queries.GetInternalUsers
        End Function
    End Class
End Namespace
