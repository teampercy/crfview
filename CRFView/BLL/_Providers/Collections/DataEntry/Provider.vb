Imports Microsoft.VisualBasic

Imports System
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Web.Configuration
Namespace Providers
    Public Class CollectionsDataEntry
        Private Shared _isInitialized As Boolean = False
        Private Shared _provider As ICollectionsDataEntryProvider
        ' <summary>
        ' Returns provider
        ' </summary>
        Public Shared ReadOnly Property Provider() As ICollectionsDataEntryProvider
            Get
                Initialize()
                Return _provider

            End Get
        End Property
        Private Shared Sub Initialize()
            If Not _isInitialized Then
                _provider = SingletonProvider(Of Providers.SQLCollectionsDataEntryProvider).Instance
                _isInitialized = True
            End If
        End Sub
        Public Shared Function GetMyOpenBatchList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Return Provider.GetMyOpenBatchList(auser)
        End Function
        Public Shared Function PostBatches(ByVal auser As Users.CurrentUser) As Boolean
            Return Provider.PostBatches(auser)
        End Function
        Public Shared Function PrintBatchList(ByVal auser As Users.CurrentUser, ByVal aprintmode As Common.PrintMode, ByVal aview As DAL.COMMON.TableView) As String
            Return Collections.Reports.BatchList(auser, aview, aprintmode)
        End Function
        Public Shared Function GetAccountsSubmittedthruWeb(ByVal auser As Users.CurrentUser, ByVal MyView As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode) As String
            Return Provider.GetAccountsSubmittedthruWeb(auser, MyView, printmode)
        End Function
        Public Shared Function PostWebPlacements(ByVal auser As Users.CurrentUser) As DataSet
            Return Provider.PostWebPlacements(auser)
        End Function
        Public Shared Function GetNewAccountInfo(ByVal auser As Users.CurrentUser, ByVal anewaccountid As String) As DataSet
            Return Provider.GetNewAccountInfo(auser, anewaccountid)
        End Function
        Public Shared Function GetNewAccountList(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.cview_Reports_GetAccountsInBatches) As DAL.COMMON.TableView
            Return Provider.GetNewAccountList(auser, asproc)
        End Function
        Public Shared Function GetCollectionClients(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Return Provider.GetCollectionClients(auser)
        End Function
        Public Shared Function GetAccountInBatches(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.cview_Reports_GetAccountsInBatches) As DAL.COMMON.TableView
            Return Provider.GetAccountInBatches(auser, asproc)
        End Function
        Public Shared Function GetUserList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Return Provider.GetUserList(auser)
        End Function

    End Class
End Namespace
