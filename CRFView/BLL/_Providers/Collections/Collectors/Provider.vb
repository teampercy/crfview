Imports Microsoft.VisualBasic

Imports System
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Web.Configuration
Namespace Providers
    Public Class CollectionsCollectors
        Private Shared _isInitialized As Boolean = False
        Private Shared _provider As ICollectionsCollectorsProvider
        ' <summary>
        ' Returns provider
        ' </summary>
        Public Shared ReadOnly Property Provider() As ICollectionsCollectorsProvider
            Get
                Initialize()
                Return _provider

            End Get
        End Property
        Private Shared Sub Initialize()
            If Not _isInitialized Then
                _provider = SingletonProvider(Of Providers.SQLCollectionsCollectorsProvider).Instance
                _isInitialized = True
            End If
        End Sub
        Public Shared Function GetAccountInfo(ByVal auser As Users.CurrentUser, ByVal anewaccountid As String) As DataSet
            Return Provider.GetAccountInfo(auser, anewaccountid)
        End Function
        Public Shared Function GetAccountList(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Accounts_GetAccountList) As DAL.COMMON.TableView
            Return Provider.GetAccountList(auser, asproc)
        End Function
        Public Shared Function PostLedgerEntry(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Accounts_LedgerEntry) As Boolean
            Return Provider.PostLedgerEntry(auser, asproc)
        End Function
        Public Shared Function PostLedgerReversal(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Accounts_LedgerReversal) As Boolean
            Return Provider.PostLedgerReversal(auser, asproc)
        End Function
        Public Shared Function PostPaymenttoAgency(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Accounts_PaymentToAgency) As Boolean
            Return Provider.PostPaymenttoAgency(auser, asproc)
        End Function
        Public Shared Function PostPaymenttoClient(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Accounts_PaymentToClient) As Boolean
            Return Provider.PostPaymenttoClient(auser, asproc)
        End Function
        Public Shared Function ReassignService(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Accounts_ReassignService) As Boolean
            Return Provider.ReassignService(auser, asproc)
        End Function
        Public Shared Function PostAccountUpdate(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Accounts_Update) As Boolean
            Return Provider.PostAccountUpdate(auser, asproc)
        End Function
        Public Shared Function GetInventoryList(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Reports_GetAccountList) As DataSet
            Return Provider.GetInventoryList(auser, asproc)
        End Function
        Public Shared Function GetClosedList(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Reports_GetClosedAccountList) As DataSet
            Return Provider.GetClosedList(auser, asproc)
        End Function
        Public Shared Function GetPaymentList(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Reports_GetPaymentList) As DataSet
            Return Provider.GetPaymentList(auser, asproc)
        End Function
        Public Shared Function PrintMasterReport(ByVal auser As Users.CurrentUser, ByVal aprintmode As Common.PrintMode, ByVal aview As DAL.COMMON.TableView) As String
            Return Provider.PrintMasterReport(auser, aprintmode, aview)
        End Function
        Public Shared Function PrintClosedReport(ByVal auser As Users.CurrentUser, ByVal aprintmode As Common.PrintMode, ByVal aview As DAL.COMMON.TableView) As String
            Return Provider.PrintClosedReport(auser, aprintmode, aview)
        End Function
        Public Shared Function PrintPaymentReport(ByVal auser As Users.CurrentUser, ByVal aprintmode As Common.PrintMode, ByVal aview As DAL.COMMON.TableView) As String
            Return Provider.PrintPaymentReport(auser, aprintmode, aview)
        End Function
        Public Shared Function GetUserList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Return Provider.GetUserList(auser)
        End Function
        Public Shared Function GetLocationList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Return Provider.GetLocations(auser)
        End Function
        Public Shared Function GetCollectionStatusList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Return Provider.GetColectionStatus(auser)
        End Function
        Public Shared Function GetClientList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Return Provider.GetCollectionClients(auser)
        End Function

        Public Shared Function PrintCollectionLetter(ByVal auser As Users.CurrentUser, ByVal akeyid As Integer, ByVal areportdef As String, ByVal areportname As String, ByVal aprintmode As HDS.Reporting.COMMON.PrintMode, Optional ByVal areportprefix As String = "", Optional ByVal asubtitle1 As String = "", Optional ByVal asubtitle2 As String = "", Optional ByVal asubreportname As String = "") As String
            Return Provider.PrintCollectionLetter(auser, akeyid, areportdef, areportname, aprintmode, areportprefix, asubtitle1, asubtitle2, asubreportname)
        End Function
    End Class
End Namespace
