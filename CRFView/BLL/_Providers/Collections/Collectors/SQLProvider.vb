Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Web.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Xml
Imports System.Xml.Schema
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections.Specialized
Imports System.Configuration.Provider
Imports System.Web.Hosting
Imports System.Web.Management
Imports System.Security.Permissions
Imports System.Text
Imports System.Security.Cryptography
Namespace Providers
    Friend Class SQLCollectionsCollectorsProvider
        Inherits ProviderBase
        Implements ICollectionsCollectorsProvider

        Public Function GetAccountInfo(ByVal auser As Users.CurrentUser, ByVal anewaccountid As String) As System.Data.DataSet Implements ICollectionsCollectorsProvider.GetAccountInfo
            Dim mysproc As New BLL.CRFDB.SPROCS.uspbo_Accounts_GetAccountInfo
            mysproc.AccountId = anewaccountid
            Return Providers.DBO.Provider.GetDataSet(mysproc)
        End Function
        Public Function GetAccountList(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Accounts_GetAccountList) As DAL.COMMON.TableView Implements ICollectionsCollectorsProvider.GetAccountList
            Return Providers.DBO.Provider.GetTableView(asproc)
        End Function
        Public Function PostAccountUpdate(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Accounts_Update) As Boolean Implements ICollectionsCollectorsProvider.PostAccountUpdate
            Return DBO.Provider.ExecScalar(asproc)
        End Function
        Public Function PostLedgerEntry(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Accounts_LedgerEntry) As Boolean Implements ICollectionsCollectorsProvider.PostLedgerEntry
            Return DBO.Provider.ExecScalar(asproc)
        End Function
        Public Function PostLedgerReversal(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Accounts_LedgerReversal) As Boolean Implements ICollectionsCollectorsProvider.PostLedgerReversal
            Return DBO.Provider.ExecScalar(asproc)
        End Function
        Public Function PostPaymenttoAgency(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Accounts_PaymentToAgency) As Boolean Implements ICollectionsCollectorsProvider.PostPaymenttoAgency
            Return DBO.Provider.ExecScalar(asproc)
        End Function
        Public Function PostPaymenttoClient(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Accounts_PaymentToClient) As Boolean Implements ICollectionsCollectorsProvider.PostPaymenttoClient
            Return DBO.Provider.ExecScalar(asproc)
        End Function
        Public Function GetInventoryList(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Reports_GetAccountList) As System.Data.DataSet Implements ICollectionsCollectorsProvider.GetInventoryList
            '    Return Collections.Reports.AccountsMaster(auser, aprintmode, aview)
        End Function
        Public Function GetPaymentList(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Reports_GetPaymentList) As System.Data.DataSet Implements ICollectionsCollectorsProvider.GetPaymentList
            '  Return Collections.Reports.AccountsPayment(auser, aprintmode, aview)
        End Function
        Public Function PrintClosedReport(ByVal auser As Users.CurrentUser, ByVal aprintmode As Common.PrintMode, ByVal aview As DAL.COMMON.TableView) As String Implements ICollectionsCollectorsProvider.PrintClosedReport
            '    Return Collections.Reports.AccountsClosed(auser, aprintmode, aview)
        End Function
        Public Function PrintMasterReport(ByVal auser As Users.CurrentUser, ByVal aprintmode As Common.PrintMode, ByVal aview As DAL.COMMON.TableView) As String Implements ICollectionsCollectorsProvider.PrintMasterReport
            '    Return Collections.Reports.AccountsMaster(auser, aprintmode, aview)
        End Function
        Public Function PrintPaymentReport(ByVal auser As Users.CurrentUser, ByVal aprintmode As Common.PrintMode, ByVal aview As DAL.COMMON.TableView) As String Implements ICollectionsCollectorsProvider.PrintPaymentReport
            '   Return Collections.Reports.AccountsPayment(auser, aprintmode, aview)
        End Function
        Public Function ReassignService(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Accounts_ReassignService) As Boolean Implements ICollectionsCollectorsProvider.ReassignService
            Return DBO.Provider.ExecScalar(asproc)
        End Function
        Public Function GetClosedList(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Reports_GetClosedAccountList) As System.Data.DataSet Implements ICollectionsCollectorsProvider.GetClosedList
            Return Providers.DBO.Provider.GetDataSet(asproc)
        End Function
        Public Function GetColectionStatus(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView Implements ICollectionsCollectorsProvider.GetColectionStatus
            Return Queries.GetCollectionStatusList
        End Function
        Public Function GetCollectionClients(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView Implements ICollectionsCollectorsProvider.GetCollectionClients
            Return Queries.GetCollectionClients
        End Function
        Public Function GetLocations(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView Implements ICollectionsCollectorsProvider.GetLocations
            Return Queries.GetLocationList
        End Function
        Public Function GetUserList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView Implements ICollectionsCollectorsProvider.GetUserList
            Return Queries.GetOwnerList
        End Function

        Public Function PrintCollectionLetter(ByVal auser As Users.CurrentUser, ByVal akeyid As Integer, ByVal areportdef As String, ByVal areportname As String, ByVal aprintmode As HDS.Reporting.COMMON.PrintMode, Optional ByVal areportprefix As String = "", Optional ByVal asubtitle1 As String = "", Optional ByVal asubtitle2 As String = "", Optional ByVal asubreportname As String = "") As String Implements ICollectionsCollectorsProvider.PrintCollectionLetter
            Dim mysproc As New SPROCS.cview_Processing_GetLetterInfo
            mysproc.DebtAccountId = akeyid
            ' mysproc.DebtAccountId = "11773"
            Dim MYVIEW As DAL.COMMON.TableView = DBO.Provider.GetTableView(mysproc)
            Dim objcollectionletter As New CollectViewC1Letter
            Dim ReportPath As String = objcollectionletter.RenderLetter(auser, MYVIEW, areportdef, areportname, aprintmode, areportprefix, asubtitle1, asubtitle2, asubreportname)

            Return ReportPath

        End Function

    End Class
End Namespace
