﻿Imports C1
Imports C1.C1Report
Imports HDS.Reporting
Public Class CollectViewC1Letter
    Inherits HDS.Reporting.C1Reports.Common.StandardReport
    Public myview As DAL.COMMON.TableView
    Public CollectorSign As System.Drawing.Image
    Public Sub New()
        Dim MYAGENCY As New BLL.CRFDB.TABLES.Agency
        DBO.Read(1, MYAGENCY)
        Dim mylogo As New HDS.WINLIB.Controls.ImageViewer
        mylogo.LoadImage(MYAGENCY.AgencyLogo)
        Me.ReportLogo = mylogo.PictureBox1.Image
        Me.ReportFooter = MYAGENCY.AgencyRptFooter

        'mylogo.LoadImage(MYAGENCY.AgencyClientLogo)
        'Me.ReportLogo1 = mylogo.PictureBox1.Image

    End Sub

    Public Function RenderLetter(ByVal auser As BLL.Users.CurrentUser, ByVal aview As HDS.DAL.COMMON.TableView, ByVal areportdef As String, ByVal areportname As String, ByVal aprintmode As HDS.Reporting.COMMON.PrintMode, Optional ByVal areportprefix As String = "", Optional ByVal asubtitle1 As String = "", Optional ByVal asubtitle2 As String = "", Optional ByVal asubreportname As String = "") As String
        Try

            myview = aview
            Dim mylogo As New HDS.WINLIB.Controls.ImageViewer
            mylogo.LoadImage(myview.RowItem("CollectorSign"))
            CollectorSign = mylogo.PictureBox1.Image

            'mylogo.LoadImage(myview.RowItem("AgencyLogo"))
            'Me.ReportLogo = mylogo.PictureBox1.Image

            Dim myuser As BLL.Users.CurrentUser = auser
            Dim myreportpath As String = myuser.SettingsFolder & "\REPORTS\" & areportdef

            Dim myoutputfolder As String = myuser.OutputFolder
            Me.ReportDefPath = myuser.SettingsFolder & "\REPORTS"
            Me.ReportDef = areportdef

            Me.UserName = myuser.UserName
            Me.OutputFolder = myoutputfolder
            Me.ReportDefPath = myreportpath
            Me.ReportName = areportname
            Me.FilePrefix = areportprefix

            Me.DataView = aview
            Me.SetSubReportRecordset(Me.C1R, asubreportname, aview)
            'Me.PrintMode = Reporting.COMMON.PrintMode.PrintToPDF
            Me.PrintMode = aprintmode

            If ReportExists(ReportDef, areportname) = False Then
                'Dim RptError As String = areportname & ":C1 Report Not Found"
                Return "Error"
            End If

            Me.RenderReport()


        Catch ex As Exception
            Return "Error"
        End Try
        Return Me.ReportFileName
    End Function
    Private Function C1ReportExists(ByVal reportdef, ByVal reportname) As Boolean
        Dim rpts() As String = C1R.GetReportList(reportdef)
        Dim rpt As String
        For Each rpt In rpts
            'If Trim(rpt).ToUpper = Trim(reportname).ToUpper Then
            If String.Compare(Trim(rpt), Trim(reportname), True) Then
                Return True
            End If
        Next
        Return False
    End Function
    Public Shared Sub View(ByVal AFILENAME As String)
        KillAcrobatReader()

        If System.IO.File.Exists(AFILENAME) = False Then Exit Sub

        Dim psInfo As New System.Diagnostics.ProcessStartInfo(AFILENAME)
        psInfo.WindowStyle = System.Diagnostics.ProcessWindowStyle.Maximized
        psInfo.Verb = ""

        psInfo.WindowStyle = ProcessWindowStyle.Maximized
        Dim myProcess As Process = System.Diagnostics.Process.Start(psInfo)

    End Sub
    Private Shared Sub KillAcrobatReader()
        Dim PR() As Process = Process.GetProcesses
        Dim P As Process
        For Each P In PR
            If UCase(P.ProcessName) = UCase("AcroRD32") Then
                P.Kill()
            End If
        Next
    End Sub
    Public Shadows Sub StartReport(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim C1R As C1Report = sender
        Dim fld As Field
        For Each fld In C1R.Fields
            Try
                If UCase(fld.Name) = "CRFBW" Then
                    fld.Value = Me.ReportLogo
                    fld.RTF = True
                End If
                If UCase(fld.Name) = "COLLECTORSIGN" Then
                    fld.Picture = Me.CollectorSign
                    fld.Visible = True
                End If
            Catch
                fld.Visible = False
            End Try

        Next fld

    End Sub
    Public Shadows Sub StartSection(ByVal sender As Object, ByVal e As C1.C1Report.ReportEventArgs)
        Dim C1R As C1.C1Report.C1Report = sender
        Dim fld As Field
        For Each fld In C1R.Fields
            Try
                If UCase(fld.Name) = "CRFBW" Then
                    fld.Picture = Me.ReportLogo
                    fld.RTF = True
                End If
                If UCase(fld.Name) = "AGENCYLOGO" Then
                    fld.Picture = Me.ReportLogo
                    fld.RTF = True
                End If
                If UCase(fld.Name) = "COLLECTORSIGN" Then
                    fld.Picture = Me.CollectorSign
                    fld.PictureScale = PictureScaleEnum.Scale
                    fld.Visible = True
                    fld.RTF = True
                End If
            Catch
                fld.Visible = False
            End Try

        Next fld


        Try

        Catch EX As Exception

            ' Common.ErrorHandler.Logger(CurrentUser, "StartSection", EX)
            Throw New ApplicationException(EX.Message)

        End Try


    End Sub
    Public Function MergeLetter(ByVal myrtfletter As String, ByVal mydata As HDS.DAL.COMMON.TableView) As String
        ' If IsNothing(mydoc.LetterImage) Then Return Nothing
        Dim m_rtf As New System.Windows.Forms.RichTextBox

        m_rtf.Rtf = RTFLIB.GetDoc(myrtfletter)
        Dim mymergefields As ArrayList = GetMergeFields(m_rtf.Rtf)
        Dim myfield As String
        For Each myfield In mymergefields
            Dim myvalue As String = ""
            If mydata.Table.Columns.Contains(myfield) Then
                myvalue = mydata.RowItem(myfield)
            End If
            Select Case myfield
                Case "CURRENTDATE"
                    m_rtf.Rtf = Replace(m_rtf.Rtf, "[" & myfield & "]", Today)

                Case "TOTASGAMT", "TOTINTAMT", "TOTBALAMT", "COLLECTIONFEEAMT", "TOTALDUE"
                    m_rtf.Rtf = ReplaceAmt(m_rtf.Rtf, "[" & myfield & "]", myvalue)

                Case Else
                    m_rtf.Rtf = Replace(m_rtf.Rtf, "[" & myfield & "]", myvalue)

            End Select
        Next
        Return m_rtf.Rtf

    End Function
    Public Shared Function GetMergeFields(ByVal artf As String) As ArrayList
        Dim myarray As New ArrayList
        myarray.Add("CURRENTDATE")
        myarray.Add("ACCOUNTNO")
        myarray.Add("DEBTORNAME")
        myarray.Add("CONTACTNAME")
        myarray.Add("ADDRESSLINE1")
        myarray.Add("TOTASGAMT")
        myarray.Add("TOTINTAMT")
        myarray.Add("TOTBALAMT")
        myarray.Add("COLLECTIONFEEAMT")
        myarray.Add("TOTALDUE")

        Return myarray

    End Function
    Public Shared Function Replace(ByRef artf As String, ByVal atag As String, ByVal avalue As String) As String
        Dim S As String = artf
        Try
            S = Strings.Replace(artf, atag, avalue)
        Catch
        End Try
        Return S

    End Function
    Public Shared Sub ReplaceDate(ByVal artf As Object, ByVal atag As String, ByVal avalue As Object)
        Try
            If IsDate(avalue) Then
                Dim mydate As String = Date.Parse(avalue).ToShortDateString
                artf = Strings.Replace(artf, atag, " " & mydate)
            Else
                artf = Strings.Replace(artf, atag, " ")
            End If

        Catch ex As Exception
            artf = Strings.Replace(artf, atag, " ")
        End Try

    End Sub
    Public Shared Function ReplaceAmt(ByVal artf As Object, ByVal atag As String, ByVal avalue As Object) As String
        Try
            Dim myamt As String = Strings.FormatCurrency(avalue)
            artf = Strings.Replace(artf, atag, myamt)
        Catch
            artf = Strings.Replace(artf, atag, " " & Strings.FormatCurrency(0))
        End Try
        Return artf
    End Function

    Private Sub CollectViewC1Letter_Progress(ByVal message As String, ByVal cancel As Boolean) Handles Me.Progress

    End Sub


End Class