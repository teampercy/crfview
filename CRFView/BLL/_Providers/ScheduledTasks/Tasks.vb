Imports Microsoft.VisualBasic
Imports System.Collections
Imports System.Threading
Imports System.Reflection
Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports System.Net.Mail
Namespace Tasks
#Region "Core Functions"
    Public Class Provider
        Private Shared current As System.Web.HttpContext = System.Web.HttpContext.Current
        Private Shared _isInitialized As Boolean = False
        Private Shared schedulerThread As System.Threading.Thread = Nothing
        Private Shared scheduler As Scheduler
        Private Shared _DBO As DAL.Providers.IDAL
        Public Shared ReadOnly Property DAL() As DAL.Providers.IDAL
            Get
                Return HDS.DAL.Providers.DataAccessFactory.DataAccess("PORTAL", AppSettings.Get("SQLCLIENT"))
            End Get
        End Property
        Private Shared Sub Initialize()
            If Not _isInitialized Then
                _isInitialized = True
                scheduler = New Scheduler()
                Dim schedulerThreadstart As New System.Threading.ThreadStart(AddressOf scheduler.Start)
                schedulerThread = New System.Threading.Thread(schedulerThreadstart)
                schedulerThread.Start()
            End If
        End Sub
        Public Shared Function Execute() As Boolean
            Initialize()
            Return True
        End Function
        Public Shared Function StopIt() As Boolean
            Initialize()
            scheduler.StopIt()
        End Function
        Public Shared Function GetTasks(ByVal maxrows As Integer, ByVal filter As String, ByVal sort As String) As System.Data.DataTable
            Dim myitem As New VIEWS.vwTasksList
            Dim MYSQL As String = DAL.GetSQL("vwTasksList", maxrows, filter, sort)

            Return DAL.GetDataSet(MYSQL).Tables(0)
        End Function
        Public Shared Function GetTaskDefs(ByVal maxrows As Integer, ByVal filter As String, ByVal sort As String) As System.Data.DataTable
            Dim myitem As New Tasks_TaskDef
            Dim MYSQL As String = DAL.GetSQL(myitem.TableObjectName, maxrows, filter, sort)
            Return DAL.GetDataSet(MYSQL).Tables(0)
        End Function
        Public Shared Function GetAttachments(ByVal maxrows As Integer, ByVal filter As String, ByVal sort As String) As System.Data.DataTable
            Dim myitem As New Tasks_HistoryAttachment
            Dim MYSQL As String = DAL.GetSQL(myitem.TableObjectName, maxrows, filter, sort)
            Return DAL.GetDataSet(MYSQL).Tables(0)
        End Function

        Public Shared Function SendEmailToClient(ByVal historyid As Integer) As Integer
            Dim msg As New MailMessage

            Dim M_SETTINGS As New Tasks_Settings
            DAL.Read(1, M_SETTINGS)

            Dim M_HISTORY As New TABLES.Tasks_History
            DAL.Read(historyid, M_HISTORY)

            Dim M_TASK As New Tasks_Task
            DAL.Read(M_HISTORY.taskid, M_TASK)

            msg.From = New Net.Mail.MailAddress(M_SETTINGS.adminemail, M_SETTINGS.adminname)
            msg.Subject = "Scheduled Report(" & M_TASK.subject & ")"
            msg.IsBodyHtml = False
            msg.Body = M_TASK.description
            msg.Body += vbCrLf
            msg.Body += "This Report was Prepared on:" & Now.ToString
            msg.Body += vbCrLf & vbCrLf
            msg.Body += "Click on the Link(s) below to retrieve your report"
            msg.Body += vbCrLf & vbCrLf
            'For Each s In attachs

            '    'Dim myattach As New CRF.BLL.CRFDB.TABLES.Tasks_HistoryAttachment
            '    'myattach.historyid = Me.m_history.PKID
            '    'myattach.url = System.IO.Path.GetFileName(s)
            '    'CRF.BLL.Providers.DBO.Provider.Create(myattach)

            '    msg.Body += M_SETTINGS.reportserverurl & System.IO.Path.GetFileName(s) & vbCrLf

            'Next
            Dim S As String = ""
            Dim myds As DataSet

            myds = DAL.GetDataSet("Select * from Tasks_HistoryAttachment Where HistoryId ='" & historyid & "'")
            Dim m_histattach As New TABLES.Tasks_HistoryAttachment
            For Each dr As DataRow In myds.Tables(0).Rows
                DAL.FillEntity(dr, m_histattach)
                Dim m_filename As String = M_SETTINGS.reportserverurl & "?historyid=" & m_histattach.historyid & "-" & m_histattach.PKID
                m_filename = "HTTP://CFS4FILING.COM/DOWNLOAD.AXD" & "?ItemId=" & m_histattach.PKID & "&HistoryId=" & m_histattach.historyid
                msg.Body += M_SETTINGS.reportserverurl & m_filename & vbCrLf
            Next

            myds = DAL.GetDataSet("Select * from Tasks_HistoryRecipient Where HistoryId ='" & historyid & "'")

            Dim mymailer As New Emails.EmailSender
            For Each dr As DataRow In myds.Tables(0).Rows
                Dim m_histRECIP As New TABLES.Tasks_HistoryRecipient
                Dim m_subscriber As New TABLES.Tasks_Subscribers

                DAL.FillEntity(dr, m_histRECIP)
                DAL.Read(m_histRECIP.subscriberid, m_subscriber)

                With M_SETTINGS
                    msg.To.Clear()

                    Dim myemail As String = m_subscriber.email
                    'myemail = "cogiguy@gmail.com"
                    'msg.To.Add(myemail)

                    msg.To.Add("CRFIT@crfsonline.com")
                    '   mymailer.StartTask(msg, .smtpserver, .smtpport, .smtplogin, .smtppassword, .smtpenablessl)

                End With

            Next
            

           
            Try

                With M_SETTINGS

                    Dim _smtp As New SmtpClient(.smtpserver)
                    If IsNothing(.smtplogin) = False And Len(.smtplogin) > 5 Then
                        _smtp.Port = .smtpport
                        _smtp.Credentials = New System.Net.NetworkCredential(.smtplogin, .smtppassword)
                        _smtp.EnableSsl = .smtpenablessl
                    End If
                    _smtp.Send(msg)
                    Return True
                End With


            Catch ex As Exception

                S = "Mail To" & msg.To.ToString & vbCrLf
                S += msg.Body
                Common.LOGGER.WriteToErrorLog(ex.Message & vbCrLf & S, "", "Email Error On")
                Return False

            End Try

            Return True

        End Function

    End Class
    Public Enum FrequencyTypes
        OneTime
        Minutes
        Daily
        Weekly
        Monthly
    End Enum
    Public Interface ISchedulerJob
        Property IsValid() As Boolean
        Property ErrorMessage() As String
        Function Execute(ByRef atask As TABLES.Tasks_Task) As Boolean
        Function DoTask() As Boolean
    End Interface
    Public Class Scheduler
        Dim _eoj As Boolean = False
        Public Sub New()
            _eoj = False
        End Sub
        Public Shared ReadOnly Property DAL() As HDS.DAL.Providers.IDAL
            Get
                Return HDS.DAL.Providers.DataAccessFactory.DataAccess("PORTAL", AppSettings.Get("SQLCLIENT"))
            End Get
        End Property
        Public Sub Start()
            Do Until _eoj = True
                Dim mysql As String = "Select * from tasks_task where IsWeb=1 and  IsDisabled = 0 and NextRunDate <= '" & Today.ToString & "' "
                Dim dt As DataTable = DAL.GetDataSet(mysql).Tables(0)
                Dim dr As DataRow
                Dim mytask As New TABLES.Tasks_Task
                Dim mytaskdef As New TABLES.Tasks_TaskDef
                Dim myworker As ISchedulerJob

                For Each dr In dt.Rows
                    Try

                        DAL.FillEntity(dr, mytask)
                        DAL.Read(mytask.taskdefid, mytaskdef)

                        myworker = LoadModule(mytaskdef.type, mytaskdef.assemblyfile)
                        If IsNothing(myworker) = False Then
                            myworker.Execute(mytask)
                        Else
                            'mytask.isdisabled = False
                            'mytask.lastmessage = "Invalid Type"
                            'Tasks.Provider.DAL.Update(mytask)
                            Common.LOGGER.WriteToErrorLog(mytask.lastmessage, "", "SCHEDULER:" & mytaskdef.taskname)
                        End If


                    Catch ex As Exception

                        mytask.isdisabled = False
                        mytask.lastmessage = ex.Message
                        DAL.Update(mytask)
                        Common.LOGGER.WriteToErrorLog(ex.Message, "", "SCHEDULER:" & mytaskdef.taskname)

                    End Try

                Next

                Thread.Sleep(30000)

            Loop

        End Sub
        Private Shared Function TaskIsValid(ByRef atask As TABLES.Tasks_Task) As Boolean
        End Function
        Private Shared Function TaskExecute(ByRef atask As TABLES.Tasks_Task) As Boolean
            If atask.isdisabled = False Then Return False

        End Function
        Public Sub StopIt()
            _eoj = True
        End Sub
        Private Shared Function LoadModule(ByVal atype As String, ByVal assemblyfile As String) As Tasks.SchedulerJob
            If IsNothing(atype) Then Return Nothing
            If atype.Length < 1 Then Return Nothing

            Dim args() As Object = Nothing
            Dim returnObj As Tasks.SchedulerJob = Nothing
            Dim s As String = Assembly.GetExecutingAssembly.FullName
            Dim Type As Type
            If assemblyfile.Length < 1 Then
                Type = Assembly.GetExecutingAssembly().GetType(atype)
            Else
                Dim asm As System.Reflection.Assembly = Assembly.LoadFrom(assemblyfile)
                If IsNothing(asm) = False Then
                    Type = asm.GetType(atype)
                End If
            End If

            If Type IsNot Nothing Then
                returnObj = DirectCast(Activator.CreateInstance(Type, args), Tasks.ISchedulerJob)

            End If
            Return returnObj

        End Function
    End Class
    Public Class SchedulerJob
        Implements ISchedulerJob
        Dim _isdisabled As Boolean = True
        Dim _taskname As String = ""
        Dim _errormessage As String = ""
        Dim _frequency As FrequencyTypes = FrequencyTypes.Daily
        Dim _nextrundate As Date = Now()
        Dim _lastrundate As Date = Now()
        Dim _interval As Integer = 1
        Dim _fromhour As Integer = 0
        Dim _thruhour As Integer = 24
        Dim _task As New TABLES.Tasks_Task
        Dim _taskdef As New TABLES.Tasks_TaskDef
        Dim _taskhistory As New TABLES.Tasks_History
        Dim _taskattach As New TABLES.Tasks_HistoryAttachment
        Dim _tasksettings As New TABLES.Tasks_Settings

        Dim _emailto As New List(Of System.Net.Mail.MailAddress)

        Public Sub New()
            Tasks.Provider.DAL.Read(1, _tasksettings)

        End Sub
       
        Public Function Execute(ByRef atask As TABLES.Tasks_Task) As Boolean Implements ISchedulerJob.Execute
            Try
                _task = atask
               
                'If Me.isdisabled = True Then
                '    Return False
                'End If
                _taskhistory.taskid = _task.PKID
                _taskhistory.startdate = Now
                Tasks.Provider.DAL.Create(_taskhistory)

                If DoTask() = True Then
                    Schedule()
                Else
                    Me.isdisabled = False
                    UpdateTask()
                End If

            Catch ex As Exception

                _task.isdisabled = True
                _task.lastmessage = ex.Message
                Tasks.Provider.DAL.Update(_task)

            End Try


        End Function
        Friend Overridable Function DoTask() As Boolean Implements ISchedulerJob.DoTask
            Return False

        End Function
        Public ReadOnly Property Task() As TABLES.Tasks_Task
            Get
                Return _task
            End Get
        End Property
        Public ReadOnly Property Settings() As TABLES.Tasks_Settings
            Get
                Return _tasksettings
            End Get
        End Property

        Public ReadOnly Property EmailTo() As List(Of System.Net.Mail.MailAddress)
            Get
                Return _emailto
            End Get
        End Property
        Public Sub SendEmailToUs(ByVal amessage As String, ByVal attachs As List(Of System.Net.Mail.Attachment))
            Dim mysettings As New TABLES.Tasks_Settings
            DBO.Read(1, mysettings)

            Dim msg As New MailMessage
            msg.From = New MailAddress(mysettings.adminemail, mysettings.adminname)
            msg.To.Add(mysettings.adminemail)
            If Me.Task.ccemail.Length > 10 Then
                msg.To.Add(Me.Task.ccemail)
            End If

            msg.IsBodyHtml = False
            msg.Subject = Me.Task.subject
            msg.Body = Me.Task.description & vbCrLf & amessage

            If IsNothing(attachs) = False Then
                For Each item As Attachment In attachs
                    msg.Attachments.Add(item)
                Next
            End If

            With mysettings
                Dim _smtp As New SmtpClient(.smtpserver)
                If IsNothing(.smtplogin) = False And Len(.smtplogin) > 5 Then
                    _smtp.Port = .smtpport
                    _smtp.Credentials = New System.Net.NetworkCredential(.smtplogin, .smtppassword)
                    _smtp.EnableSsl = .smtpenablessl
                End If
                _smtp.Send(msg)
            End With

         
        End Sub
        Public Sub SendEmailToUser(ByVal amessage As String, ByVal attachs As List(Of System.Net.Mail.Attachment))
            Dim mysettings As New TABLES.Tasks_Settings
            DBO.Read(1, mysettings)

            Dim msg As New MailMessage
            msg.From = New MailAddress(mysettings.adminemail, mysettings.adminname)

            If IsNothing(EmailTo) = False Then
                For Each item As System.Net.Mail.MailAddress In EmailTo
                    msg.To.Add(item)
                Next
                If Me.Task.ccemail.Length > 10 Then
                    msg.To.Add(Me.Task.ccemail)
                End If
            End If

            msg.IsBodyHtml = False
            msg.Subject = Me.Task.subject
            msg.Body = Me.Task.description & vbCrLf & amessage
            If IsNothing(attachs) = False Then
                For Each item As Attachment In attachs
                    msg.Attachments.Add(item)
                Next
            End If


            With mysettings
                Dim _smtp As New SmtpClient(.smtpserver)
                If IsNothing(.smtplogin) = False And Len(.smtplogin) > 5 Then
                    _smtp.Port = .smtpport
                    _smtp.Credentials = New System.Net.NetworkCredential(.smtplogin, .smtppassword)
                    _smtp.EnableSsl = .smtpenablessl
                End If
                _smtp.Send(msg)
            End With

        End Sub
        Public ReadOnly Property TaskDef() As TABLES.Tasks_TaskDef
            Get
                Return _taskdef
            End Get
        End Property

        Public Property IsDisabled() As Boolean Implements ISchedulerJob.IsValid
            Get
                If _task.isdisabled = True Then
                    Return True
                End If

                If _task.nextrundate > Now Then
                    Return False
                End If

                Dim hh As Integer = DatePart(DateInterval.Hour, Now)
                If hh >= _task.runfromhour And hh < _task.runuptohour Then
                    Return False
                Else
                    Return True
                End If

            End Get

            Set(ByVal value As Boolean)
                _isdisabled = value
            End Set

        End Property
        Private Sub Schedule()

            _task.lastrundate = Now

            Select Case _task.dateinterval

                Case FrequencyTypes.Minutes
                    _task.nextrundate = DateAdd(DateInterval.Minute, _task.number, Now)

                Case FrequencyTypes.Daily
                    _task.nextrundate = DateAdd(DateInterval.Day, _task.number, Today)

                Case FrequencyTypes.Monthly
                    _task.nextrundate = DateAdd(DateInterval.Day, _task.number, Today)

                Case FrequencyTypes.Weekly
                    _task.nextrundate = DateAdd(DateInterval.Day, _task.number, Today)

                Case Else
                    _isdisabled = True

            End Select

            UpdateTask()


        End Sub
        Public Function CreateAttachment(ByVal clientid As String, ByVal afilepath As String)
            _taskattach = New TABLES.Tasks_HistoryAttachment
            _taskattach.datecreated = Now()
            _taskattach.historyid = _taskhistory.PKID
            _taskattach.clientid = clientid
            _taskattach.image = PDFLIB.GetStream(afilepath)
            _taskattach.url = System.IO.Path.GetFileName(afilepath)
            Tasks.Provider.DAL.Create(_taskattach)
          
        End Function

        Private Sub UpdateTask()
            With _task
                .lastrundate = Now
                .lastmessage = Me.ErrorMessage
                .isdisabled = _isdisabled
            End With
            Tasks.Provider.DAL.Update(_task)

            _taskhistory.taskid = _task.PKID
            _taskhistory.enddate = Now()
            _taskhistory.message = Me.ErrorMessage
            Tasks.Provider.DAL.Update(_taskhistory)

           
        End Sub
        Public Property ErrorMessage() As String Implements ISchedulerJob.ErrorMessage
            Get
                Return _errormessage
            End Get
            Set(ByVal value As String)
                _errormessage = value
            End Set
        End Property
        Friend Shared Function FixEmail(ByVal emailaddr As String) As String
            If IsDBNull(emailaddr) = True Then
                Return String.Empty
            End If
            Dim s As String = Strings.Replace(emailaddr.Trim, vbCrLf, "")
            Return s

        End Function
        Public Function RenderC1Report(ByVal myview As DataView, ByVal areportdef As String, ByVal areportname As String, Optional ByVal pdf As Boolean = True, Optional ByVal areportprefix As String = "", Optional ByVal asubtitle1 As String = "", Optional ByVal asubtitle2 As String = "") As String
            Dim MYC1RPT As New HDS.Reporting.C1Reports.C1ReportProvider
            Dim myreportpath As String = Settings.settingsfolder & areportdef
            Dim myoutputfolder As String = Settings.outputfolder

            Dim MYAGENCY As New CRFDB.TABLES.Agency
            DBO.Read(1, MYAGENCY)
            Dim mylogo As New HDS.WINLIB.Controls.ImageViewer
            mylogo.LoadImage(MYAGENCY.AgencyLogo)
            MYC1RPT.ReportLogo = mylogo.PictureBox1.Image
            MYC1RPT.ReportFooter = MYAGENCY.AgencyRptFooter
            Dim mymode As HDS.Reporting.COMMON.PrintMode = HDS.Reporting.COMMON.PrintMode.PrintToPDF
            Dim sreport As String = MYC1RPT.Render(areportprefix, myoutputfolder, myreportpath, areportname, myview, HDS.Reporting.COMMON.PrintMode.PrintToPDF, "", asubtitle1, asubtitle2)
            If System.IO.File.Exists(sreport) Then
                Return sreport
            End If
            Return Nothing


        End Function
    End Class
    Public Class TaskHistory
        Dim _isdisabled As Boolean = True
        Dim _taskname As String = ""
        Dim _errormessage As String = ""
        Dim _frequency As FrequencyTypes = FrequencyTypes.Daily
        Dim _nextrundate As Date = Now()
        Dim _lastrundate As Date = Now()
        Dim _interval As Integer = 1
        Dim _fromhour As Integer = 0
        Dim _thruhour As Integer = 24
        Dim _task As New TABLES.Tasks_Task
        Dim _taskdef As New TABLES.Tasks_TaskDef
        Dim _taskhistory As New TABLES.Tasks_History
        Dim _taskattach As New TABLES.Tasks_HistoryAttachment
        Dim _tasksettings As New TABLES.Tasks_Settings

        Dim _emailto As New List(Of System.Net.Mail.MailAddress)

        Public Sub New()
            Tasks.Provider.DAL.Read(1, _tasksettings)

        End Sub

        Public Function Create(ByRef ataskhistory As TABLES.Tasks_History) As Boolean
            Try
                _taskhistory = ataskhistory
                Tasks.Provider.DAL.Read(_taskhistory.taskid, _task)
                _taskhistory.taskalias = _task.prefix
                Tasks.Provider.DAL.Create(_taskhistory)

            Catch ex As Exception

                _task.isdisabled = True
                _task.lastmessage = ex.Message
                Tasks.Provider.DAL.Update(_task)

            End Try


        End Function
        Public Sub CreateAttachment(ByVal afilepath As String)
            If System.IO.File.Exists(afilepath) = False Then Exit Sub

            _taskattach = New TABLES.Tasks_HistoryAttachment
            _taskattach.datecreated = Now()
            _taskattach.historyid = _taskhistory.PKID
            _taskattach.image = PDFLIB.GetStream(afilepath)
            _taskattach.url = System.IO.Path.GetFileName(afilepath)

            Tasks.Provider.DAL.Create(_taskattach)

        End Sub
        Public Sub CreateSubscriber(ByVal aemail As String, ByVal aname As String)
           
            Dim myitem As New TABLES.Tasks_HistoryRecipient
            Dim mysub As New TABLES.Tasks_Subscribers
            CRFDB.Provider.GetItem(mysub.TableObjectName, mysub, "email", aemail)
            With mysub
                .email = aemail
                .username = aname
                If .PKID > 0 Then
                    CRFDB.Provider.DBO.Update(mysub)
                Else
                    CRFDB.Provider.DBO.Create(mysub)
                End If
            End With

            With myitem
                .historyid = _taskhistory.PKID
                .subscriberid = mysub.PKID
            End With

            Tasks.Provider.DAL.Create(myitem)

        End Sub

        Public Sub Close()

            With _task
                .lastrundate = Now
                .lastmessage = Me.ErrorMessage
                .isdisabled = _isdisabled

            End With

            Tasks.Provider.DAL.Update(_task)

            _taskhistory.taskid = _task.PKID
            _taskhistory.enddate = Now()
            _taskhistory.message = Me.ErrorMessage
            _taskhistory.status = 1

            Tasks.Provider.DAL.Update(_taskhistory)


        End Sub

        Public ReadOnly Property Task() As TABLES.Tasks_Task
            Get
                Return _task
            End Get
        End Property
        Public ReadOnly Property TaskHistory() As TABLES.Tasks_History
            Get
                Return _taskhistory
            End Get
        End Property
        Public ReadOnly Property TaskDef() As TABLES.Tasks_TaskDef
            Get
                Return _taskdef
            End Get
        End Property
        Public ReadOnly Property Settings() As TABLES.Tasks_Settings
            Get
                Return _tasksettings
            End Get
        End Property
        Public ReadOnly Property EmailTo() As List(Of System.Net.Mail.MailAddress)
            Get
                Return _emailto
            End Get
        End Property
        Private Sub SendEmailToUs(ByVal amessage As String, ByVal attachs As List(Of System.Net.Mail.Attachment))
            Dim mysettings As New TABLES.Tasks_Settings
            DBO.Read(1, mysettings)

            Dim msg As New MailMessage
            msg.From = New MailAddress(mysettings.adminemail, mysettings.adminname)
            msg.To.Add(mysettings.adminemail)
            If Me.Task.ccemail.Length > 10 Then
                msg.To.Add(Me.Task.ccemail)
            End If

            msg.IsBodyHtml = False
            msg.Subject = Me.Task.subject
            msg.Body = Me.Task.description & vbCrLf & amessage

            If IsNothing(attachs) = False Then
                For Each item As Attachment In attachs
                    msg.Attachments.Add(item)
                Next
            End If

            With mysettings
                Dim _smtp As New SmtpClient(.smtpserver)
                If IsNothing(.smtplogin) = False And Len(.smtplogin) > 5 Then
                    _smtp.Port = .smtpport
                    _smtp.Credentials = New System.Net.NetworkCredential(.smtplogin, .smtppassword)
                    _smtp.EnableSsl = .smtpenablessl
                End If
                _smtp.Send(msg)
            End With


        End Sub
        Private Sub SendEmailToUser(ByVal amessage As String, ByVal attachs As List(Of System.Net.Mail.Attachment))
            Dim mysettings As New TABLES.Tasks_Settings
            DBO.Read(1, mysettings)

            Dim msg As New MailMessage
            msg.From = New MailAddress(mysettings.adminemail, mysettings.adminname)

            If IsNothing(EmailTo) = False Then
                For Each item As System.Net.Mail.MailAddress In EmailTo
                    msg.To.Add(item)
                Next
                If Me.Task.ccemail.Length > 10 Then
                    msg.To.Add(Me.Task.ccemail)
                End If
            End If

            msg.IsBodyHtml = False
            msg.Subject = Me.Task.subject
            msg.Body = Me.Task.description & vbCrLf & amessage
            If IsNothing(attachs) = False Then
                For Each item As Attachment In attachs
                    msg.Attachments.Add(item)
                Next
            End If


            With mysettings
                Dim _smtp As New SmtpClient(.smtpserver)
                If IsNothing(.smtplogin) = False And Len(.smtplogin) > 5 Then
                    _smtp.Port = .smtpport
                    _smtp.Credentials = New System.Net.NetworkCredential(.smtplogin, .smtppassword)
                    _smtp.EnableSsl = .smtpenablessl
                End If
                _smtp.Send(msg)
            End With

        End Sub
       
        Public Property ErrorMessage() As String
            Get
                Return _errormessage
            End Get
            Set(ByVal value As String)
                _errormessage = value
            End Set
        End Property
        Friend Shared Function FixEmail(ByVal emailaddr As String) As String
            If IsDBNull(emailaddr) = True Then
                Return String.Empty
            End If
            Dim s As String = Strings.Replace(emailaddr.Trim, vbCrLf, "")
            Return s

        End Function
    End Class
#End Region
   

End Namespace
