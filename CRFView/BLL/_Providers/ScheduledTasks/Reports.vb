Imports Microsoft.VisualBasic
Imports System.Collections
Imports System.Threading
Imports System.Reflection
Imports System.Data
Imports System.Configuration.ConfigurationManager
Imports CRF.BLL.CRFDB
Imports CRF.BLL.CRFDB.TABLES
Imports System.Net.Mail
Namespace Reports
#Region "Core Functions"
    Public Class Provider
        Private Shared current As System.Web.HttpContext = System.Web.HttpContext.Current
        Private Shared _isInitialized As Boolean = False
        Private Shared schedulerThread As System.Threading.Thread = Nothing
        Private Shared scheduler As Scheduler
        Private Shared _DBO As DAL.Providers.IDAL
        Public Shared ReadOnly Property DAL() As DAL.Providers.IDAL
            Get
                Return HDS.DAL.Providers.DataAccessFactory.DataAccess("PORTAL", AppSettings.Get("SQLCLIENT"))
            End Get
        End Property
        Private Shared Sub Initialize()
            If Not _isInitialized Then
                _isInitialized = True
                scheduler = New Scheduler()
                Dim schedulerThreadstart As New System.Threading.ThreadStart(AddressOf scheduler.Start)
                schedulerThread = New System.Threading.Thread(schedulerThreadstart)
                schedulerThread.Start()
            End If
        End Sub
        Public Shared Function Execute() As Boolean
            Initialize()

            Return True
        End Function
        Public Shared Function StopIt() As Boolean
            Initialize()
            scheduler.StopIt()
        End Function
  

    End Class
    Public Enum FrequencyTypes
        OneTime
        Minutes
        Daily
        Weekly
        Monthly
    End Enum
    Public Interface ISchedulerJob
        Property IsValid() As Boolean
        Property ErrorMessage() As String
        Function Execute(ByRef atask As TABLES.Report) As Boolean
        Function DoTask() As Boolean
        Function DoRequest(ByVal xmlstring As String) As String
    End Interface
    Public Class Scheduler
        Dim _eoj As Boolean = False
        Public Sub New()
            _eoj = False
        End Sub
        Public Shared ReadOnly Property DAL() As HDS.DAL.Providers.IDAL
            Get
                Return HDS.DAL.Providers.DataAccessFactory.DataAccess("PORTAL", AppSettings.Get("SQLCLIENT"))
            End Get
        End Property
        Public Sub Start()
            Do Until _eoj = True
                Dim mysql As String = "Select * from Report where IsDisabled = 0 and NextRunDate <= '" & Today.ToString & "' "
                Dim dt As DataTable = DAL.GetDataSet(mysql).Tables(0)
                Dim dr As DataRow
                Dim mytask As New TABLES.Report
                Dim myworker As ISchedulerJob

                For Each dr In dt.Rows
                    Try

                        DAL.FillEntity(dr, mytask)
                        myworker = LoadModule(mytask.type, mytask.assemblyfile)
                        If IsNothing(myworker) = False Then
                            myworker.Execute(mytask)
                        Else
                            'mytask.isdisabled = False
                            'mytask.lastmessage = "Invalid Type"
                            'Tasks.Provider.DAL.Update(mytask)
                            Common.LOGGER.WriteToErrorLog(mytask.lastmessage, "", "SCHEDULER:" & mytask.taskname)
                        End If


                    Catch ex As Exception

                        mytask.isdisabled = False
                        mytask.lastmessage = ex.Message
                        DAL.Update(mytask)
                        Common.LOGGER.WriteToErrorLog(ex.Message, "", "SCHEDULER:" & mytask.taskname)

                    End Try

                Next

                Thread.Sleep(30000)

            Loop

        End Sub
        Private Shared Function TaskIsValid(ByRef atask As TABLES.Report) As Boolean
        End Function
        Private Shared Function TaskExecute(ByRef atask As TABLES.Report) As Boolean
            If atask.isdisabled = False Then Return False

        End Function
        Public Sub StopIt()
            _eoj = True
        End Sub
        Public Shared Function LoadModule(ByVal atype As String, ByVal assemblyfile As String) As Object
            If IsNothing(atype) Then Return Nothing
            If atype.Length < 1 Then Return Nothing
            Dim asm As System.Reflection.Assembly
            Dim args() As Object = Nothing
            Dim returnObj As Object = Nothing
            Dim s As String = Assembly.GetExecutingAssembly.FullName
            Dim Type As Type
            If assemblyfile.Length < 1 Then
                Type = Assembly.GetExecutingAssembly().GetType(atype)
            Else
                asm = Assembly.LoadFrom(assemblyfile)
                If IsNothing(asm) = False Then
                    Type = asm.GetType(atype)
                    If Type IsNot Nothing Then
                        returnObj = asm.CreateInstance(atype)
                        '   returnObj = DirectCast(Activator.CreateInstance(Type, args), Tasks.SchedulerJob)
                        Return returnObj
                    End If
                End If
            End If


            Return returnObj

        End Function
    End Class
    Public Class SchedulerJob
        Implements ISchedulerJob
        Dim _isdisabled As Boolean = True
        Dim _taskname As String = ""
        Dim _errormessage As String = ""
        Dim _frequency As FrequencyTypes = FrequencyTypes.Daily
        Dim _nextrundate As Date = Now()
        Dim _lastrundate As Date = Now()
        Dim _interval As Integer = 1
        Dim _fromhour As Integer = 0
        Dim _thruhour As Integer = 24
        Dim _task As New TABLES.Report
        Dim _taskhistory As New TABLES.Report_History
        Dim _taskattach As New TABLES.Report_HistoryAttachment
        Dim _tasksettings As New TABLES.Tasks_Settings

        Dim _emailto As New List(Of System.Net.Mail.MailAddress)

        Public Sub New()
            ProviderBase.DAL.Read(1, _tasksettings)

        End Sub
        Public Function Execute(ByRef atask As TABLES.Report) As Boolean Implements ISchedulerJob.Execute
            Try
                _task = atask

                'If Me.isdisabled = True Then
                '    Return False
                'End If
                _taskhistory.reportid = _task.PKID
                _taskhistory.startdate = Now
                Tasks.Provider.DAL.Create(_taskhistory)

                If DoTask() = True Then
                    Schedule()
                Else
                    Me.IsDisabled = False
                    UpdateTask()
                End If

            Catch ex As Exception

                _task.isdisabled = True
                _task.lastmessage = ex.Message
                Tasks.Provider.DAL.Update(_task)

            End Try


        End Function
        Friend Overridable Function DoTask() As Boolean Implements ISchedulerJob.DoTask
            Return False

        End Function
        Public Overridable Function DoRequest(ByVal xmlstring As String) As String Implements ISchedulerJob.DoRequest
            Return Nothing

        End Function
        Public ReadOnly Property Report() As TABLES.Report
            Get
                Return _task
            End Get
        End Property
        Public ReadOnly Property Settings() As TABLES.Tasks_Settings
            Get
                Return _tasksettings
            End Get
        End Property

        Public ReadOnly Property EmailTo() As List(Of System.Net.Mail.MailAddress)
            Get
                Return _emailto
            End Get
        End Property
        Public Sub SendEmailToUs(ByVal amessage As String, ByVal attachs As List(Of System.Net.Mail.Attachment))
            'Dim mysettings As New TABLES.Tasks_Settings
            'CRF.CLIENTVIEW.BLL.Providers.WebSite.DAL.Read(1, mysettings)

            'Dim msg As New MailMessage
            'msg.From = New MailAddress(mysettings.adminemail, mysettings.adminname)
            'msg.To.Add(mysettings.adminemail)
            'If Me.Task.ccemail.Length > 10 Then
            '    msg.To.Add(Me.Task.ccemail)
            'End If

            'msg.IsBodyHtml = False
            'msg.Subject = Me.Task.subject
            'msg.Body = Me.Task.description & vbCrLf & amessage

            'If IsNothing(attachs) = False Then
            '    For Each item As Attachment In attachs
            '        msg.Attachments.Add(item)
            '    Next
            'End If

            'With mysettings
            '    Dim _smtp As New SmtpClient(.smtpserver)
            '    If IsNothing(.smtplogin) = False And Len(.smtplogin) > 5 Then
            '        _smtp.Port = .smtpport
            '        _smtp.Credentials = New System.Net.NetworkCredential(.smtplogin, .smtppassword)
            '        _smtp.EnableSsl = .smtpenablessl
            '    End If
            '    _smtp.Send(msg)
            'End With


        End Sub
        Public Sub SendEmailToUser(ByVal amessage As String, ByVal attachs As List(Of System.Net.Mail.Attachment))
            'Dim mysettings As New TABLES.Tasks_Settings
            'CRF.CLIENTVIEW.BLL.Providers.WebSite.DAL.Read(1, mysettings)

            'Dim msg As New MailMessage
            'msg.From = New MailAddress(mysettings.adminemail, mysettings.adminname)

            'If IsNothing(EmailTo) = False Then
            '    For Each item As System.Net.Mail.MailAddress In EmailTo
            '        msg.To.Add(item)
            '    Next
            '    If Me.Task.ccemail.Length > 10 Then
            '        msg.To.Add(Me.Task.ccemail)
            '    End If
            'End If

            'msg.IsBodyHtml = False
            'msg.Subject = Me.Task.subject
            'msg.Body = Me.Task.description & vbCrLf & amessage
            'If IsNothing(attachs) = False Then
            '    For Each item As Attachment In attachs
            '        msg.Attachments.Add(item)
            '    Next
            'End If


            'With mysettings
            '    Dim _smtp As New SmtpClient(.smtpserver)
            '    If IsNothing(.smtplogin) = False And Len(.smtplogin) > 5 Then
            '        _smtp.Port = .smtpport
            '        _smtp.Credentials = New System.Net.NetworkCredential(.smtplogin, .smtppassword)
            '        _smtp.EnableSsl = .smtpenablessl
            '    End If
            '    _smtp.Send(msg)
            'End With

        End Sub
        'Public ReadOnly Property TaskDef() As TABLES.Report_TaskDef
        '    Get
        '        Return _taskdef
        '    End Get
        'End Property

        Public Property IsDisabled() As Boolean Implements ISchedulerJob.IsValid
            Get
                If _task.isdisabled = True Then
                    Return True
                End If

                If _task.nextrundate > Now Then
                    Return False
                End If

                Dim hh As Integer = DatePart(DateInterval.Hour, Now)
                If hh >= _task.runfromhour And hh < _task.runuptohour Then
                    Return False
                Else
                    Return True
                End If

            End Get

            Set(ByVal value As Boolean)
                _isdisabled = value
            End Set

        End Property
        Private Sub Schedule()

            _task.lastrundate = Now

            Select Case _task.frequency

                Case FrequencyTypes.Minutes
                    _task.nextrundate = DateAdd(DateInterval.Minute, _task.number, Now)

                Case FrequencyTypes.Daily
                    _task.nextrundate = DateAdd(DateInterval.Day, _task.number, Today)

                Case FrequencyTypes.Monthly
                    _task.nextrundate = DateAdd(DateInterval.Day, _task.number, Today)

                Case FrequencyTypes.Weekly
                    _task.nextrundate = DateAdd(DateInterval.Day, _task.number, Today)

                Case Else
                    _isdisabled = True

            End Select

            UpdateTask()


        End Sub
        Public Sub CreateAttachment(ByVal ahistoryid As String, ByVal adescription As String, ByVal afilepath As String)
            Dim _taskattach As New TABLES.Report_HistoryAttachment

            _taskattach.datecreated = Now()
            _taskattach.description = adescription
            _taskattach.historyid = ahistoryid
            _taskattach.image = PDFLIB.GetStream(afilepath)
            _taskattach.url = System.IO.Path.GetFileName(afilepath)
            ProviderBase.DAL.Create(_taskattach)

        End Sub
        Public Sub CreateRecipient(ByVal ahistoryid As String, ByVal aemails As String)
            Dim ss As String() = Strings.Split(aemails, ",")
            For Each s As String In ss
                Dim myitem As New TABLES.Report_HistoryRecipient
                With myitem
                    .historyid = ahistoryid
                    .email = FixEmail(s)
                End With
                ProviderBase.DAL.Create(myitem)
            Next

        End Sub
        Private Sub UpdateTask()
            With _task
                .lastrundate = Now
                .lastmessage = Me.ErrorMessage
                .isdisabled = _isdisabled
            End With
            Tasks.Provider.DAL.Update(_task)

            _taskhistory.reportid = _task.PKID
            _taskhistory.enddate = Now()
            _taskhistory.message = Me.ErrorMessage
            Tasks.Provider.DAL.Update(_taskhistory)


        End Sub
        Public Property ErrorMessage() As String Implements ISchedulerJob.ErrorMessage
            Get
                Return _errormessage
            End Get
            Set(ByVal value As String)
                _errormessage = value
            End Set
        End Property
        Friend Shared Function FixEmail(ByVal emailaddr As String) As String
            If IsDBNull(emailaddr) = True Then
                Return String.Empty
            End If
            Dim s As String = Strings.Replace(emailaddr.Trim, vbCrLf, "")
            Return s

        End Function
        Public Shared Function RenderC1Report(ByVal aview As DataView, ByVal areportdef As String, ByVal areportname As String, ByVal aoutputfolder As String, Optional ByVal pdf As Boolean = True, Optional ByVal areportprefix As String = "", Optional ByVal asubtitle1 As String = "", Optional ByVal asubtitle2 As String = "") As String

            Dim MYC1RPT As New HDS.Reporting.C1Reports.C1ReportProvider
            Dim myreportpath As String = areportdef
            Dim myoutputfolder As String = aoutputfolder
            Dim MYAGENCY As New CRFDB.TABLES.Agency
            CRFDB.Provider.DBO.Read(1, MYAGENCY)
            Dim mylogo As New HDS.WINLIB.Controls.ImageViewer
            mylogo.LoadImage(MYAGENCY.AgencyLogo)
            MYC1RPT.ReportLogo = mylogo.PictureBox1.Image
            MYC1RPT.ReportFooter = MYAGENCY.AgencyRptFooter
            Dim mymode As HDS.Reporting.COMMON.PrintMode = HDS.Reporting.COMMON.PrintMode.PrintToPDF
            Dim sreport As String = MYC1RPT.Render(areportprefix, myoutputfolder, myreportpath, areportname, aview, HDS.Reporting.COMMON.PrintMode.PrintToPDF, areportprefix, asubtitle1, asubtitle2)
            If System.IO.File.Exists(sreport) Then
                Return aoutputfolder & System.IO.Path.GetFileName(sreport)
            End If

            Return Nothing


        End Function
      
    End Class
    Public Class ClientReport
        Inherits ProviderBase
        Dim _report As New Report
        Dim _history As New Report_History
        Dim _keyvalues As New Dictionary(Of String, String)
        Dim _attachments As New Dictionary(Of String, String)
        Dim _recipients As New Dictionary(Of String, String)
        Public Sub New()

        End Sub
        
        Public Sub New(ByVal areportname As String, ByVal Batchid As String, ByVal ClientId As Integer, ByVal clientcode As String)
            Dim mysql As String = "select * from report where taskname = '[REPORTNAME]' "
            mysql = Strings.Replace(mysql, "[REPORTNAME]", areportname)
            Dim mydt As DataTable = DAL.GetDataSet(mysql).Tables(0)
            Dim MYID As String = mydt.Rows(0).Item("PKID")

            DAL.Read(MYID, _report)


            Dim myreport As New TABLES.Report
            _history = New TABLES.Report_History
            _history.batchid = Batchid
            _history.reportid = _report.PKID
            _history.taskname = areportname
            _history.startdate = Now
            _history.clientid = ClientId
            _history.clientcode = clientcode
            _history.status = 0
            ProviderBase.DAL.Create(_history)


        End Sub
        'Public ReadOnly Property KeyValues() As Dictionary(Of String, String)
        '    Get
        '        Return _keyvalues
        '    End Get
        'End Property
        'Public ReadOnly Property Attachments() As Dictionary(Of String, String)
        '    Get
        '        Return _attachments
        '    End Get
        'End Property
        'Public ReadOnly Property Recipients() As Dictionary(Of String, String)
        '    Get
        '        Return _recipients
        '    End Get
        'End Property
        Public ReadOnly Property History() As TABLES.Report_History
            Get
                Return _history
            End Get
        End Property
        Public Sub CreateAttachment(ByVal adescription As String, ByVal afilepath As String)
            Dim _taskattach As New TABLES.Report_HistoryAttachment
            _taskattach.datecreated = Now()
            _taskattach.description = adescription
            _taskattach.historyid = _history.PKID
            _taskattach.image = PDFLIB.GetStream(afilepath)
            _taskattach.url = System.IO.Path.GetFileName(afilepath)
            ProviderBase.DAL.Create(_taskattach)

        End Sub
        Public Sub CreateRecipient(ByVal aemail As String, ByVal ausername As String)
            Dim ss As String() = Strings.Split(aemail, ",")
            For Each s As String In ss
                Dim myitem As New TABLES.Report_HistoryRecipient
                With myitem
                    .historyid = _history.PKID
                    .email = FixEmail(s)
                    .username = ausername
                End With
                ProviderBase.DAL.Create(myitem)
            Next

        End Sub
        Public Sub UpdateStatus(ByVal altkeyid As Integer, ByVal totrecords As Integer, ByVal lastrecord As Integer)
            _history.altkeyid = altkeyid
            _history.totitems = totrecords
            _history.lastitem = lastrecord
            _history.enddate = Now()

            ProviderBase.DAL.Update(_history)

        End Sub
        Public Sub CloseHistory(ByVal status As Integer, ByVal message As String)
            _history.enddate = Now
            _history.status = status
            _history.message = message
            _history.keyvalues = ""
            'For Each KEYV As KeyValuePair(Of String, String) In Me.KeyValues
            '    _history.keyvalues += KEYV.Key & vbTab & KEYV.Value & vbCrLf
            'Next
            ProviderBase.DAL.Update(_history)

        End Sub
        Friend Shared Function FixEmail(ByVal emailaddr As String) As String
            If IsDBNull(emailaddr) = True Then
                Return String.Empty
            End If
            Dim s As String = Strings.Replace(emailaddr.Trim, vbCrLf, "")
            Return s

        End Function
    End Class
#End Region


End Namespace
