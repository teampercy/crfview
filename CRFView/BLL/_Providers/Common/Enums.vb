Namespace COMMON
    Public Enum PrintMode
        Preview = 1
        PrintDirect = 2
        PrintDialog = 3
        PrintToPDF = 4
        PrinttoExcel = 5
        PrinttoHTML = 6
        PrinttoRTF = 7
        NoReport = 8
        SaveToPDFExcel = 9
    End Enum
End Namespace
