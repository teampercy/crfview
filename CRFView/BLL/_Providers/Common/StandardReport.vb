Imports System
Imports System.IO
Imports System.Data.Common
Imports C1.C1Report
Imports Microsoft.VisualBasic
Namespace Common
    Public Class StandardReport
        Implements IReportProvider, IDisposable
        Public Event Progress(ByVal message As String, ByVal cancel As Boolean)

        Dim m_connectionstring As String
        Dim m_printmode As PrintMode = PrintMode.PrintToPDF
        Dim m_reportdef As String
        Dim m_reportname As String
        Dim m_username As String
        Dim m_reportpath As String
        Dim m_reportdefpath As String

        Dim m_exportfolder As String
        Dim m_fileprefix As String
        Dim m_filter As String
        Dim m_sort As String
        Dim m_subtitle1 As String
        Dim m_subtitle2 As String
        Dim m_reportfooter As Object
        Dim m_settingsfolder As String

        Dim m_reportlogo As System.Drawing.Image
        Dim m_reportlogo1 As System.Drawing.Image
        Dim m_exportflds As Collection
        Dim m_reportfolder As String
        Protected m_reportfilename As String
        Protected m_exportfilename As String
        Protected m_ds As New DataSet
        Protected m_dt As New DataTable
        Protected m_dv As New DataView

        Protected m_tableIndex As Integer = 0
        Protected WithEvents m_c1r As C1.C1Report.C1Report
        Dim myagency As New TABLES.Agency
        Public Sub New()
            m_tableIndex = 0
            m_c1r = New C1Report
            DBO.Read(1, MYAGENCY)
            Dim mylogo As New HDS.WINLIB.Controls.ImageViewer
            mylogo.LoadImage(myagency.AgencyLogo)
            Me.ReportLogo = mylogo.PictureBox1.Image
            Me.ReportFooter = myagency.AgencyRptFooter

            mylogo.LoadImage(myagency.AgencyClientLogo)
            Me.ReportLogo1 = mylogo.PictureBox1.Image
        End Sub
        Public Sub New(ByRef ac1r As C1.C1Report.C1Report, ByVal areportdef As String, ByVal areportfolder As String)

            m_reportfolder = System.IO.Path.GetDirectoryName(areportdef)
            m_reportdef = areportdef
            m_reportfolder = areportfolder
            If System.IO.Directory.Exists(m_reportfolder) = False Then
                System.IO.Directory.CreateDirectory(m_reportfolder)
            End If
            m_printmode = PrintMode.PrintToPDF
            m_tableIndex = 0
            m_c1r = ac1r

        End Sub
        Public Sub New(ByVal areportdef As String, ByVal areportfolder As String)

            m_reportfolder = System.IO.Path.GetDirectoryName(areportdef)
            m_reportdef = areportdef
            m_reportfolder = areportfolder
            If System.IO.Directory.Exists(m_reportfolder) = False Then
                System.IO.Directory.CreateDirectory(m_reportfolder)
            End If
            m_printmode = PrintMode.PrintToPDF
            m_tableIndex = 0
            m_c1r = New C1Report

        End Sub
        Public Property RenderMode() As Common.PrintMode
            Get
                Return Me.PrintMode
            End Get
            Set(ByVal Value As Common.PrintMode)
                Me.PrintMode = Value
            End Set
        End Property
        Public Property OutputFolder() As String
            Get
                Return m_reportfolder
            End Get
            Set(ByVal Value As String)
                m_reportfolder = Value
            End Set
        End Property
        Public Property SettingsFolder() As String
            Get
                Return m_settingsfolder
            End Get
            Set(ByVal Value As String)
                m_settingsfolder = Value
            End Set
        End Property
        Public Property UserName() As String
            Get
                Return m_username
            End Get
            Set(ByVal Value As String)
                m_username = Value
            End Set
        End Property
        Public Property ReportPath() As String
            Get
                Return m_reportpath
            End Get
            Set(ByVal Value As String)
                m_reportpath = Value
            End Set
        End Property
        Public Property ReportDefPath() As String
            Get
                Return m_reportdef
            End Get
            Set(ByVal Value As String)
                m_reportdef = Value
            End Set
        End Property
        Public ReadOnly Property IsValid() As Boolean
            Get
                If System.IO.File.Exists(Me.ReportDef) Then
                    If ReportExists(Me.ReportDef, Me.ReportName) Then
                        Return True
                    Else
                        Return False
                    End If
                End If
                Return False

            End Get
        End Property
        Public ReadOnly Property ReportFolder()
            Get
                m_reportfolder = Me.OutputFolder
                Return Me.OutputFolder
            End Get
        End Property
        Public Property ConnectionString() As String Implements IReportProvider.ConnectionString
            Get
                Return m_connectionstring
            End Get
            Set(ByVal Value As String)
                m_connectionstring = Value
            End Set
        End Property
        Public Property ExportFields() As Collection
            Get
                Return m_exportflds
            End Get
            Set(ByVal Value As Collection)
                m_exportflds = Value
            End Set
        End Property
        Public Property DataView() As DataView
            Get
                Return m_dv
            End Get
            Set(ByVal Value As DataView)
                m_dv = Value
            End Set
        End Property
        Public Property DataTable() As DataTable
            Get
                Return m_dt
            End Get
            Set(ByVal Value As DataTable)
                m_dt = Value
            End Set
        End Property
        Public Property DataSource() As Object Implements IReportProvider.DataSource
            Get
                Return m_ds
            End Get
            Set(ByVal Value As Object)
                m_ds = Value
            End Set
        End Property
        Public Property Filter() As String Implements IReportProvider.Filter
            Get
                Return m_filter
            End Get
            Set(ByVal Value As String)
                m_filter = Value
            End Set
        End Property
        Public Property Sort() As String Implements IReportProvider.Sort
            Get
                Return m_sort
            End Get
            Set(ByVal Value As String)
                m_sort = Value
            End Set
        End Property

        Public Property FilePrefix() As String Implements IReportProvider.FilePrefix
            Get
                Return m_fileprefix
            End Get
            Set(ByVal Value As String)
                m_fileprefix = Value
            End Set
        End Property
        Public Property ReportFileName() As String Implements IReportProvider.ReportFileName
            Get
                Return m_reportfilename
            End Get
            Set(ByVal Value As String)
                m_reportfilename = Value
            End Set
        End Property
        Public Property SubTitle1() As String Implements IReportProvider.SubTitle1
            Get
                Return m_subtitle1
            End Get
            Set(ByVal Value As String)
                m_subtitle1 = Value
            End Set
        End Property
        Public Property SubTitle2() As String Implements IReportProvider.SubTitle2
            Get
                Return m_subtitle2
            End Get
            Set(ByVal Value As String)
                m_subtitle2 = Value
            End Set
        End Property
        Public Property ExportFileName() As String
            Get
                Return m_exportfilename
            End Get
            Set(ByVal Value As String)
                m_exportfilename = Value
            End Set
        End Property
        Public Property TableIndex() As Integer
            Get
                Return m_tableIndex
            End Get
            Set(ByVal Value As Integer)
                m_tableIndex = Value
            End Set
        End Property
        Public Property ExportFolder() As String Implements IReportProvider.ExportFolder
            Get
                Return m_exportfolder
            End Get
            Set(ByVal Value As String)
                m_exportfolder = Value
            End Set
        End Property
        Public Property PrintMode() As PrintMode Implements IReportProvider.PrintMode
            Get
                Return m_printmode
            End Get
            Set(ByVal Value As PrintMode)
                m_printmode = Value
            End Set
        End Property
        Public Property ReportDef() As String Implements IReportProvider.ReportDef
            Get
                Return m_reportdef
            End Get
            Set(ByVal Value As String)
                m_reportdef = m_reportfolder & "\" & Value
            End Set
        End Property
        Public Property ReportName() As String Implements IReportProvider.ReportName
            Get
                Return m_reportname
            End Get
            Set(ByVal Value As String)
                m_reportname = Value
            End Set
        End Property
        Public Sub SetRecordset(ByVal c1r As C1Report, ByVal arecordset As Object)
            c1r.DataSource.Recordset = arecordset
        End Sub
        Public Sub SetSubReportRecordset(ByVal c1r As C1Report, ByVal areportname As String, ByVal arecordset As Object)
            Dim fld As Field
            For Each fld In c1r.Fields
                If Not (fld.Subreport Is Nothing) Then
                    If fld.Subreport.ReportName = areportname Then
                        fld.Subreport.DataSource.Recordset = arecordset
                    End If
                End If
            Next fld
        End Sub
        'Protected Function GetFileName(ByVal aextension As String) As String
        '    Dim lio As System.IO.Path, lsfilename As String

        '    Dim lsfolder As String = Me.ExportFolder & "\"
        '    If System.IO.Directory.Exists(lsfolder) = False Then
        '        lsfolder = lio.GetTempPath
        '    End If

        '    Return lsfolder & TrimSpace(Me.ReportName & "-" & Format(Now(), "yyyyMMddhhmmss") & aextension)

        'End Function
        'Private Function TrimSpace(ByRef strInput As String) As String
        '    ' This procedure trims extra space from any part of
        '    ' a string.
        '    Dim str As String = strInput.Trim
        '    Return Join(str.Split(" "), "")

        'End Function
        Public ReadOnly Property C1R() As C1.C1Report.C1Report Implements IReportProvider.C1R
            Get
                Return m_c1r
            End Get
        End Property
        Public Sub StartReport(ByVal sender As Object, ByVal e As System.EventArgs) Handles m_c1r.StartReport
            Dim C1R As C1Report = sender

            If IsNothing(m_dv) Then
                m_dv = New DataView(Me.m_ds.Tables(Me.TableIndex))
            End If

            If Len(m_filter) > 0 Then
                m_dv.RowFilter = m_filter
            End If

            If Len(m_sort) > 0 Then
                m_dv.Sort = m_sort
            End If

            C1R.DataSource.Recordset = m_dv
            Dim fld As Field
            For Each fld In C1R.Fields
                Try
                    If UCase(fld.Name) = "SUBTITLE1" Then
                        fld.Text = Me.SubTitle1
                    End If
                    If UCase(fld.Name) = "SUBTITLE2" Then
                        fld.Text = Me.SubTitle2
                    End If
                    If UCase(fld.Name) = "REPORTFOOTER" Then
                        fld.Value = Me.ReportFooter
                        fld.RTF = True
                    End If
                    If UCase(fld.Name) = "LOGO" Then
                        fld.Picture = Me.ReportLogo
                        fld.Visible = True
                    End If
                    If UCase(fld.Name) = "LOGO1" Then
                        fld.Picture = Me.ReportLogo1
                        fld.Visible = True
                    End If
                Catch
                    fld.Visible = False
                End Try

            Next fld
        End Sub
        Public Sub Dispose() Implements System.IDisposable.Dispose
            m_c1r.Dispose()

        End Sub
        Public Sub RenderReport(Optional ByVal IsPrintDirect As Boolean = False)
            Me.C1R.Clear()

            If System.IO.File.Exists(Me.ReportDef) Then

                If ReportExists(Me.ReportDef, Me.ReportName) Then
                    Try
                        C1R.Load(Me.ReportDef, Me.ReportName)
                        If Me.PrintMode = PrintMode.Preview Then
                            C1R.Render()
                        ElseIf Me.PrintMode = PrintMode.PrintDirect Then
                            C1R.Render()
                            C1R.Document.Print()

                            Me.ReportFileName = ""
                        Else

                            Me.ReportFileName = GetFileName(Me.FilePrefix)
                            Select Case Me.PrintMode
                                Case PrintMode.PrinttoExcel
                                    C1R.RenderToFile(Me.ReportFileName, FileFormatEnum.Excel)
                                Case PrintMode.PrintToPDF
                                    C1R.RenderToFile(Me.ReportFileName, FileFormatEnum.PDF)
                                Case PrintMode.PrinttoRTF
                                    C1R.RenderToFile(Me.ReportFileName, FileFormatEnum.RTF)
                                Case PrintMode.PrinttoHTML
                                    C1R.RenderToFile(Me.ReportFileName, FileFormatEnum.HTML)
                            End Select
                            If IsPrintDirect = True Then
                                C1R.Render()
                                C1R.Document.Print()
                            End If
                        End If
                    Catch ex As ApplicationException

                        Throw New Exception("Report Error" & vbCrLf & ex.Message)
                    End Try
                Else
                    Throw New Exception("Report does not exist" & vbCrLf & Me.ReportName)
                End If


            Else
                Throw New Exception("Report Definition File does not exist" & vbCrLf & Me.ReportDef)
            End If

        End Sub
        Private Function GetFileName(ByVal aprefix As String, Optional ByVal aext As String = ".PDF") As String
            Dim mext As String = ".PDF"
            Select Case Me.PrintMode
                Case PrintMode.PrinttoExcel
                    mext = ".XLS"
                Case PrintMode.PrintToPDF
                    mext = ".PDF"
                Case PrintMode.PrinttoRTF
                    mext = ".DOC"
                Case PrintMode.PrinttoHTML
                    mext = ".HTML"
            End Select

            If System.IO.Directory.Exists(Me.ReportFolder) = False Then
                System.IO.Directory.CreateDirectory(Me.ReportFolder)
            End If
            Dim S As String
            If Len(aprefix) > 0 Then
                S = aprefix & "-" & Me.ReportName
            Else
                S = Me.ReportName
            End If
            S = Strings.Replace(S, "\", "")
            S = Strings.Replace(S, "/", "")

            Return Me.ReportFolder & TrimSpace(S & "-" & Format(Now(), "yyyyMMddhhmmss") & mext)

        End Function
        Public Function TrimSpace(ByRef strInput As String) As String
            ' This procedure trims extra space from any part of
            ' a string.
            Dim str As String = strInput.Trim
            Return Join(str.Split(" "), "")

        End Function
        Private Function ReportExists(ByVal reportdef, ByVal reportname) As Boolean
            Dim rpts() As String = C1R.GetReportList(reportdef)
            Dim rpt As String
            For Each rpt In rpts
                'If Trim(rpt).ToUpper = Trim(reportname).ToUpper Then
                If String.Compare(Trim(rpt), Trim(reportname), True) Then
                    Return True
                End If
            Next
            Return False
        End Function
        Private Function HandleDbNull(ByVal adt As DataTable, ByVal ColumnName As String, ByVal VALUE As Object) As Object
            Try
                If DBNull.Value.Equals(VALUE) Then
                    Select Case UCase(adt.Columns(ColumnName).DataType.Name)
                        Case "DECIMAL", "MONEY"
                            Return 0
                        Case "BOOLEAN"
                            Return 0
                        Case "FLOAT", "INT32", "DECIMAL", "INT"
                            Return 0
                        Case "DATE", "DATETIME"
                            Return ""
                        Case Else
                            Return ""
                    End Select
                Else
                    Select Case UCase(adt.Columns(ColumnName).DataType.Name)
                        Case "DATE", "DATETIME"
                            Dim str As String = VALUE
                            Return str
                        Case Else
                            Return VALUE

                    End Select
                End If
            Catch ex As Exception
                Return VALUE
            End Try

        End Function
        Public Property ReportFooter() As Object Implements IReportProvider.ReportFooter
            Get
                Return m_reportfooter
            End Get
            Set(ByVal Value As Object)
                m_reportfooter = Value
            End Set
        End Property

        Public Property ReportLogo() As System.Drawing.Image Implements IReportProvider.ReportLogo
            Get
                Return m_reportlogo
            End Get
            Set(ByVal Value As System.Drawing.Image)
                m_reportlogo = Value
            End Set
        End Property

        Public Property ReportLogo1() As System.Drawing.Image Implements IReportProvider.ReportLogo1
            Get
                Return m_reportlogo1
            End Get
            Set(ByVal Value As System.Drawing.Image)
                m_reportlogo1 = Value
            End Set
        End Property

        Public Overridable Sub StartSection(ByVal sender As Object, ByVal e As C1.C1Report.ReportEventArgs) Handles m_c1r.StartSection
        End Sub
        Public Overridable Sub PrintSection(ByVal sender As Object, ByVal e As C1.C1Report.ReportEventArgs) Handles m_c1r.PrintSection
        End Sub
      
    End Class
End Namespace
