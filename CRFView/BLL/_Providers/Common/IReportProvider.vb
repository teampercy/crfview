Namespace Common
    Public Interface IReportProvider
        Property ConnectionString() As String
        ReadOnly Property C1R() As C1.C1Report.C1Report
        Property DataSource() As Object
        Property Filter() As String
        Property Sort() As String
        Property ReportDef() As String
        Property SubTitle1() As String
        Property SubTitle2() As String
        Property ReportFooter() As Object
        Property ReportLogo() As System.Drawing.Image
        Property ReportLogo1() As System.Drawing.Image
        Property ReportName() As String
        Property PrintMode() As PrintMode
        Property ExportFolder() As String
        Property FilePrefix() As String
        Property ReportFileName() As String
    End Interface
  
End Namespace
