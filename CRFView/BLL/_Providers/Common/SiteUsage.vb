Imports System
Imports System.Windows.Forms
Imports C1
Imports C1.C1Report
Imports C1.Win.C1Chart
Imports System.Drawing
Imports System.Drawing.Imaging
Imports ZedGraph

Namespace Common.Reports
    Public Class SiteUsage
        Inherits Common.StandardReport
        Dim m_rtf As RichTextBox
        Dim m_filename As String
        Dim m_formfolder As String
        Dim _reportpath As String
        Dim _settingsfolder As String
        Dim _reportdefpath As String
        Dim _username As String
        Dim srtf As String
        Public CurrentUser As Users.CurrentUser
        Dim myds As DataSet
        Dim mysitestats As HDS.DAL.COMMON.TableView
        Public Sub New(ByVal auser As Users.CurrentUser)
            MyBase.New()
            Me.CurrentUser = auser
            Me.OutputFolder = auser.OutputFolder
            Me.SettingsFolder = auser.SettingsFolder
            Me.UserName = auser.UserName
            Me.ReportDefPath = Me.SettingsFolder & "Reports\SiteStats.xml"
            Me.ReportName = "SITESTATS"

        End Sub
        Public Function ExecuteC1Report(ByVal ads As DataSet) As String
            myds = ads
            mysitestats = New HDS.DAL.COMMON.TableView(myds.Tables(0))
            If mysitestats.Count = 1 Then
                Me.DataView = mysitestats
                With Me
                    If .FilePrefix.Length < 1 Then
                        .FilePrefix = ClientView.Lien.Report.GetReportPrefix(Me.CurrentUser) & "-" & Me.ReportName
                    End If
                    .TableIndex = 0
                    .RenderReport()
                    .ReportPath = .ReportFileName
                    Return .ReportFileName

                End With
            Else
                Return Nothing
            End If


        End Function
        Public Overrides Sub StartSection(ByVal sender As Object, ByVal e As C1.C1Report.ReportEventArgs)


            Dim C1R As C1.C1Report.C1Report = sender
            Try
                If HasField("UsageByHour", e) = True Then
                    Dim ZIMAGE As Field = C1R.Fields("UsageByHour")
                    ZIMAGE.Picture = GetChart(New HDS.DAL.COMMON.TableView(myds.Tables(1)))
                    ZIMAGE.PictureScale = PictureScaleEnum.Stretch
                End If

            Catch ex As Exception
                Dim s As String = String.Empty

            End Try

        End Sub
        Private Function HasField(ByVal aname As String, ByVal e As C1.C1Report.ReportEventArgs) As Boolean
            Try
                Dim Zfield As Field = C1R.Fields(aname)
                If UCase(Zfield.Name) = UCase(aname) And e.Section = Zfield.Section Then
                    Return True
                Else
                    Return False
                End If
            Catch
                Return False
            End Try

        End Function
        Friend Function GetChart(ByVal data As HDS.DAL.COMMON.TableView)
            Dim zgc As New ZedGraphControl
            zgc.Dock = DockStyle.Fill
            zgc.Height = 500
            zgc.Width = 500

            data.MoveFirst()
            Dim y(24) As Double
            Do Until data.EOF
                Dim i As Integer = data.RowItem("LogHour")
                Dim u As Integer = data.RowItem("Users")
                y(i) = u
                data.MoveNext()
            Loop

            Dim myPane As GraphPane = zgc.GraphPane

            ' Set the titles and axis labels
            myPane.Title.Text = "Usage By Hour"
            myPane.XAxis.Title.Text = "Hour"
            myPane.YAxis.Title.Text = "Users"

            ' Make up some random data points
            Dim labels(24) As String
            labels(0) = "00"
            labels(1) = "01"
            labels(2) = "02"
            labels(3) = "03"
            labels(4) = "04"
            labels(5) = "04"
            labels(6) = "06"
            labels(7) = "07"
            labels(8) = "08"
            labels(9) = "09"
            labels(10) = "10"
            labels(11) = "11"
            labels(12) = "12"
            labels(13) = "13"
            labels(14) = "14"
            labels(15) = "15"
            labels(16) = "16"
            labels(17) = "17"
            labels(18) = "18"
            labels(19) = "19"
            labels(20) = "20"
            labels(21) = "21"
            labels(22) = "22"
            labels(23) = "23"


            ' Generate a red bar with "Curve 1" in the legend
            Dim myBar As BarItem = myPane.AddBar("", Nothing, y, Color.RosyBrown)
            myBar.Bar.Fill = New Fill(Color.Red, Color.White, Color.RosyBrown)


            '' Generate a black line with "Curve 4" in the legend
            Dim myCurve As LineItem = myPane.AddCurve("", _
               Nothing, y, Color.Black, SymbolType.Circle)
            myCurve.Line.Fill = New Fill(Color.White, Color.LightSkyBlue, -45.0F)

            ' Fix up the curve attributes a little
            myCurve.Symbol.Size = 8.0F
            myCurve.Symbol.Fill = New Fill(Color.White)
            myCurve.Line.Width = 2.0F

            ' Draw the X tics between the labels instead of at the labels
            myPane.XAxis.MajorTic.IsBetweenLabels = True

            ' Set the XAxis labels
            myPane.XAxis.Scale.TextLabels = labels
            ' Set the XAxis to Text type
            myPane.XAxis.Type = AxisType.Text

            ' Fill the axis area with a gradient
            myPane.Chart.Fill = New Fill(Color.White, _
               Color.FromArgb(255, 255, 166), 90.0F)
            ' Fill the pane area with a solid color
            myPane.Fill = New Fill(Color.FromArgb(250, 250, 255))

            ' Calculate the Axis Scale Ranges
            zgc.AxisChange()
            Dim myimg As Bitmap = zgc.GetImage
            Return myimg

        End Function
        Public Function CreateBarGraph(ByVal atitle As String, ByVal xtitle As String, ByVal ytitle As String) As Bitmap
            Dim zgc As New ZedGraphControl
            zgc.Dock = DockStyle.Fill
            zgc.Height = 500
            zgc.Width = 500

            Dim myPane As GraphPane = zgc.GraphPane
            ' Set the title and axis labels
            myPane.Title.Text = atitle
            myPane.XAxis.Title.Text = xtitle
            myPane.YAxis.Title.Text = ytitle

            Dim list As New PointPairList()
            Dim list2 As New PointPairList()
            Dim list3 As New PointPairList()
            Dim rand As New Random()

            ' Generate random data for three curves
            Dim i As Integer = 0
            While i < 24
                Dim x As Double = DirectCast(i, Integer)
                Dim y As Double = rand.NextDouble() * 1000
                Dim y2 As Double = rand.NextDouble() * 1000
                Dim y3 As Double = rand.NextDouble() * 1000
                list.Add(x, y)
                list2.Add(x, y2)
                list3.Add(x, y3)
                System.Math.Max(System.Threading.Interlocked.Increment(i), i - 1)
            End While

            ' create the curves
            Dim myCurve As BarItem = myPane.AddBar("curve 1", list, Color.Blue)
            Dim myCurve2 As BarItem = myPane.AddBar("curve 2", list2, Color.Red)
            Dim myCurve3 As BarItem = myPane.AddBar("curve 3", list3, Color.Green)

            ' Fill the axis background with a color gradient
            myPane.Chart.Fill = New Fill(Color.White, Color.FromArgb(255, 255, 166), 45.0F)

            zgc.AxisChange()

            ' expand the range of the Y axis slightly to accommodate the labels
            myPane.YAxis.Scale.Max += myPane.YAxis.Scale.MajorStep

            ' Create TextObj's to provide labels for each bar
            BarItem.CreateBarLabels(myPane, False, "f0")
            Dim myimg As Bitmap = zgc.GetImage
            Return myimg

        End Function
    End Class

End Namespace
