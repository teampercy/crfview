Imports System.IO
Imports System.Windows
Imports System.Collections.Specialized
Namespace Common
    Public Class FileOps
        Public Shared Function AddBackslash(ByVal aPath As String, Optional ByVal ach As Char = "\"c) _
                    As String
            If Not aPath.EndsWith(ach) Then
                AddBackslash = aPath & ach
            Else
                AddBackslash = aPath
            End If
        End Function
        Public Shared Function DirectoryClean(ByVal aSourcePath As String, ByVal adate As Date) As Boolean

            Dim SourceDir As DirectoryInfo = New DirectoryInfo(aSourcePath)
           
            If SourceDir.Exists Then
                ' copy all the files of the current directory
                Dim ChildFile As FileInfo
                For Each ChildFile In SourceDir.GetFiles()
                    If ChildFile.LastWriteTime < adate Then
                        System.IO.File.Delete(ChildFile.FullName)
                    End If
                Next
                Dim SubDir As DirectoryInfo
                For Each SubDir In SourceDir.GetDirectories()
                    DirectoryClean(SubDir.FullName, adate)
                Next
            End If

            Return True

        End Function
        Public Shared Function DirectoryCopy(ByVal aSourcePath As String, ByVal aDestPath As String, _
         ByVal SearchPattern As String, Optional ByVal Overwrite As Boolean = True, Optional ByVal adate As Date = Nothing)
            'Dim SourceDir As DirectoryInfo = New DirectoryInfo(System.IO.Path.GetDirectoryName(aSourcePath))
            'Dim DestDir As DirectoryInfo = New DirectoryInfo(System.IO.Path.GetDirectoryName(aDestPath))
            Dim SourceDir As DirectoryInfo = New DirectoryInfo(aSourcePath)
            Dim DestDir As DirectoryInfo = New DirectoryInfo(aDestPath)

            ' the source directory must exist, otherwise throw an exception
            If SourceDir.Exists Then
                ' if destination SubDir's parent SubDir does not exist throw an exception
                If Not DestDir.Parent.Exists Then
                    Throw New DirectoryNotFoundException _
                        ("Destination directory does not exist: " + DestDir.Parent.FullName)
                End If

                If Not DestDir.Exists = False Then
                    DestDir.Create()
                End If

                ' copy all the files of the current directory
                Dim ChildFile As FileInfo
                For Each ChildFile In SourceDir.GetFiles()
                    If ChildFile.LastWriteTime < adate Then
                        If Overwrite Then
                            Dim src As String, dst As String
                            src = ChildFile.FullName
                            dst = Path.Combine(DestDir.FullName, ChildFile.Name)
                            ChildFile.Attributes = FileAttributes.Normal
                            ChildFile.CopyTo(Path.Combine(DestDir.FullName, ChildFile.Name), True)

                        Else
                            ' if Overwrite = false, copy the file only if it does not exist
                            ' this is done to avoid an IOException if a file already exists
                            ' this way the other files can be copied anyway...
                            If Not File.Exists(Path.Combine(DestDir.FullName, ChildFile.Name)) Then
                                ChildFile.CopyTo(Path.Combine(DestDir.FullName, ChildFile.Name), _
                                    False)
                            End If
                        End If
                    End If

                Next

                ' copy all the sub-directories by recursively calling this same routine
                Dim SubDir As DirectoryInfo
                For Each SubDir In SourceDir.GetDirectories()
                    Dim s As String = Path.Combine(DestDir.FullName, SubDir.Name)
                    If s <> aDestPath Then
                        If System.IO.Directory.Exists(s) = False Then
                            System.IO.Directory.CreateDirectory(s)
                        End If
                        DirectoryCopy(SubDir.FullName, Path.Combine(DestDir.FullName, _
                            SubDir.Name), SearchPattern, Overwrite, adate)
                    End If
                   
                Next
            Else
                Throw New DirectoryNotFoundException("Source directory does not exist: " + _
                    SourceDir.FullName)
            End If
            Return True

        End Function
        Public Shared Function FileCopy(ByVal aSourcePath As String, ByVal aDestPath As String, Optional ByVal Overwrite As Boolean = True)
            Dim SourceFile As FileInfo = New FileInfo(aSourcePath)
            Dim DestFile As FileInfo = New FileInfo(aDestPath)
            If DestFile.Exists And Overwrite = False Then
                Throw New ApplicationException("Destination File Exists")
            End If

            ' the source directory must exist, otherwise throw an exception
            If SourceFile.Exists Then
                SourceFile.CopyTo(DestFile.FullName)
            Else
                Throw New ApplicationException("Source File Does Not Exist")
            End If

            Return True

        End Function

        Public Shared Sub Writefile(ByVal filename As String, ByRef buffer As String)
            Dim sdir As String = System.IO.Path.GetDirectoryName(filename)
            If System.IO.Directory.Exists(sdir) = False Then
                System.IO.Directory.CreateDirectory(sdir)
            End If

            If System.IO.File.Exists(filename) Then
                System.IO.File.Delete(filename)
            End If
            Dim myWriter As New System.IO.StreamWriter(filename)
            myWriter.Write(buffer)
            myWriter.Close()

        End Sub
        Public Shared Function ReadFfile(ByVal filename As String) As String
            Dim myreader As New System.IO.StreamReader(filename)
            Dim s As String = myreader.ReadToEnd
            myreader.Close()
            Return s

        End Function
        Private Shared mserror As String = ""
        Public Shared ReadOnly Property ErrorDescr() As String
            Get
                Return mserror
            End Get
        End Property
        '/ <summary> Write a string to a file, returning true if successful </summary>
        '/ <param name="fileName">Qualified filename</param>
        '/ <param name="data">String data to write</param>
        Public Overloads Shared Function WriteToFile(ByVal fileName As String, ByVal data As String, Optional ByVal CreateFile As Boolean = True) As Boolean
            Dim success As Boolean = True
            Try
                Try
                    If CreateFile = True Then
                        Dim w As System.IO.StreamWriter = System.IO.File.CreateText(fileName)
                        w.Write(data)
                        w.Close()
                    Else
                        Dim w As System.IO.StreamWriter = System.IO.File.AppendText(fileName)
                        w.Write(data)
                        w.Close()
                    End If

                Catch e As Exception
                    mserror = "Cannot write to file: " + fileName + ControlChars.Cr + ControlChars.Lf + e.Message
                    success = False
                Finally
                End Try
            Catch e As Exception
                mserror = "Cannot create file: " + fileName + ControlChars.Cr + ControlChars.Lf + e.Message
                success = False
            End Try
            Return success
        End Function 'WriteToFile
        '/ <summary>
        '/ Write a string to a file, returning true if successful.
        '/ </summary>
        '/ <param name="fileName">Qualified filename</param>
        '/ <param name="data">String collection to write</param>
        Public Overloads Shared Function WriteToFile(ByVal fileName As String, ByVal data As StringCollection) As Boolean
            Dim success As Boolean = True

            If System.IO.File.Exists(fileName) = False Then
                Dim ofd As New System.Windows.Forms.SaveFileDialog

                If ofd.ShowDialog <> System.Windows.Forms.DialogResult.Cancel Then
                    fileName = ofd.FileName
                Else
                    mserror = ("Write Cancelled")
                    Return False
                End If
            End If

            Try
                Dim w As System.IO.StreamWriter = System.IO.File.CreateText(fileName)
                Try
                    Dim s As String
                    For Each s In data
                        w.WriteLine(s)
                    Next s
                Catch e As Exception
                    mserror = "Cannot write to file: " + fileName + ControlChars.Cr + ControlChars.Lf + e.Message
                    success = False
                Finally
                    w.Close()
                End Try
            Catch e As Exception
                mserror = "Cannot create file: " + fileName + ControlChars.Cr + ControlChars.Lf + e.Message
                success = False
            End Try
            Return success
        End Function 'WriteToFile
        '/ <summary>
        '/ Reads the contents of a file into a string, returning true if successful.
        '/ </summary>
        '/ <param name="fileName">Qualified filename</param>
        '/ <param name="data">Output data</param>
        Public Overloads Shared Function ReadFromFile(ByVal initialdir As String, ByVal fileName As String, ByRef data As String) As Boolean
            Dim success As Boolean = True
            If System.IO.File.Exists(fileName) = False Then
                Dim ofd As New System.Windows.Forms.OpenFileDialog
                ofd.InitialDirectory = initialdir
                If ofd.ShowDialog <> System.Windows.Forms.DialogResult.Cancel Then
                    fileName = ofd.FileName
                Else
                    mserror = ("Load Cancelled")
                    Return False
                End If
            End If

            data = ""
            Try
                Dim r As System.IO.StreamReader = System.IO.File.OpenText(fileName)
                Try
                    data = r.ReadToEnd()
                Catch e As Exception
                    mserror = "Cannot read from file: " + fileName + ControlChars.Cr + ControlChars.Lf + e.Message
                    success = False
                Finally
                    r.Close()
                End Try
            Catch e As Exception
                mserror = "Cannot open file: " + fileName + ControlChars.Cr + ControlChars.Lf + e.Message
                success = False
            End Try
            Return success
        End Function 'ReadFromFile
        '/ <summary>
        '/ Reads the contents of a file into a string, returning true if successful.
        '/ </summary>
        '/ <param name="fileName">Qualified filename</param>
        '/ <param name="data">Output data</param>
        Public Overloads Shared Function ReadFromFile(ByVal fileName As String, ByRef data As StringCollection) As Boolean
            Dim success As Boolean = True
            If System.IO.File.Exists(fileName) = False Then
                Dim ofd As New System.Windows.Forms.OpenFileDialog
                If ofd.ShowDialog <> System.Windows.Forms.DialogResult.Cancel Then
                    fileName = ofd.FileName
                Else
                    mserror = ("Load Cancelled")
                    Return False
                End If
            End If

            data = New StringCollection
            Try
                Dim r As System.IO.StreamReader = System.IO.File.OpenText(fileName)
                Try
                    Dim s As String
                    Do
                        s = r.ReadLine()
                        If Not (s Is Nothing) Then
                            data.Add(s)
                        End If
                    Loop While Not (s Is Nothing)
                Catch e As Exception
                    mserror = "Cannot read from file: " + fileName + ControlChars.Cr + ControlChars.Lf + e.Message
                    success = False
                Finally
                    r.Close()
                End Try
            Catch e As Exception
                mserror = "Cannot open file: " + fileName + ControlChars.Cr + ControlChars.Lf + e.Message
                success = False
            End Try
            Return success
        End Function 'ReadFromFile
        Public Shared Function GetFileName(ByRef auser As Users.CurrentUser, ByVal aprefix As String, Optional ByVal aext As String = ".CSV", Optional ByVal suppressusername As Boolean = False) As String
            If System.IO.Directory.Exists(auser.OutputFolder) = False Then
                System.IO.Directory.CreateDirectory(auser.OutputFolder)
            End If
            Dim S As String
            If suppressusername = True Then
                S = aprefix
            Else
                S = auser.UserName & "-" & aprefix
            End If
            Return auser.OutputFolder & TrimSpace(S & "-" & Format(Now(), "yyyyMMddHHmmss") & aext)

        End Function
        Public Shared Function TrimSpace(ByRef strInput As String) As String
            ' This procedure trims extra space from any part of
            ' a string.
            Dim str As String = strInput.Trim
            Return Join(str.Split(" "), "")

        End Function
    End Class
End Namespace



