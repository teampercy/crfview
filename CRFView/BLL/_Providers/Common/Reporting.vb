Imports System
Imports System.Net
Imports System.Net.Mail
Imports System.Net.Mime
Imports System.Threading
Imports System.ComponentModel
Imports System.Text.RegularExpressions
Imports System.Text
Namespace Common
    Public Class Reporting
        Public Shared Function ClientPerformance(ByVal auser As Users.CurrentUser, ByVal afromdate As Date, ByVal athrudate As String, ByVal aprintmode As Common.PrintMode, Optional ByVal areportprefix As String = "") As String
            Dim myreport As New CRF.BLL.Common.Reports.ClientPerformance(auser)
            myreport.PrintMode = aprintmode
            myreport.FilePrefix = areportprefix
            myreport.SubTitle1 += " For " & Strings.FormatDateTime(DateAdd(DateInterval.Day, -1, Today), DateFormat.ShortDate)

            Return myreport.ExecuteC1Report()

        End Function

        Public Shared Function WebSiteStats(ByVal auser As Users.CurrentUser, ByVal afromdate As Date, ByVal athrudate As String, ByVal aprintmode As Common.PrintMode, Optional ByVal areportprefix As String = "") As String
            Dim mysproc As New CRF.BLL.CRFDB.SPROCS.uspbo_ClientView_GetSiteStats
            mysproc.FromDate = afromdate
            mysproc.ThruDate = athrudate
            Dim myreport As New CRF.BLL.Common.Reports.SiteUsage(auser)
            myreport.PrintMode = aprintmode
            myreport.FilePrefix = areportprefix
            myreport.SubTitle1 = " From " & Strings.FormatDateTime(mysproc.FromDate, DateFormat.ShortDate)
            myreport.SubTitle1 += " Thru " & Strings.FormatDateTime(DateAdd(DateInterval.Day, -1, mysproc.ThruDate), DateFormat.ShortDate)

            Return myreport.ExecuteC1Report(CRF.BLL.Providers.DBO.Provider.GetDataSet(mysproc))

        End Function
        'Public Shared Function SendFax(ByVal filenames As ArrayList) As Integer
        '    Dim objFaxServer As New FAXCOMLib.FaxServer
        '    Dim DOC As FAXCOMLib.FaxDoc
        '    With objFaxServer
        '        .Connect(System.Environment.MachineName)
        '    End With

        '    Dim s As String
        '    For Each s In filenames
        '        Dim faxes As String() = Strings.Split(s, "~")
        '        DOC = objFaxServer.CreateDocument(faxes(0))
        '        DOC.RecipientName = faxes(1)
        '        DOC.FaxNumber = faxes(2)
        '        SendFax = DOC.Send()
        '    Next

        '    objFaxServer.Disconnect()

        'End Function
        Public Shared Sub SendEmail(ByVal strTo As String, ByVal strFrom As String, ByVal strSubject As String, ByVal strMessage As String, ByVal filenames As ArrayList, Optional ByVal SmtpHost As String = "localhost", Optional ByVal SmtpUser As String = "", Optional ByVal SmtpPassword As String = "")

            Dim MailMsg As New MailMessage
            MailMsg.From = New MailAddress(strFrom.Trim)
            Dim msgtos As String() = Strings.Split(strTo, ";")
            Dim msgto As String
            For Each msgto In msgtos
                MailMsg.To.Add(New MailAddress(msgto.Trim))
            Next
            MailMsg.BodyEncoding = Encoding.Default
            MailMsg.Subject = strSubject.Trim()
            MailMsg.Body = strMessage.Trim() & vbCrLf
            MailMsg.Priority = MailPriority.High
            MailMsg.IsBodyHtml = True

            Dim filename As String = ""
            For Each filename In filenames
                Dim MsgAttach As New Attachment(filename)
                MailMsg.Attachments.Add(MsgAttach)
            Next

            Dim SmtpMail As New SmtpClient
            SmtpMail.Host = SmtpHost
            If SmtpUser.Length > 1 Then
                SmtpMail.Credentials = New NetworkCredential(SmtpUser, SmtpPassword)
            End If

            SmtpMail.Send(MailMsg)

        End Sub
        Public Shared Function StandardReport(ByVal areportdefpath As String, ByVal areportname As String, ByRef auser As Users.CurrentUser, ByRef akeyvals As HDS.DAL.COMMON.KeyValues, ByVal MYview As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode) As Boolean
            Try
                Dim myreport As New BLL.Common.StandardReport
                myreport.ReportDefPath = areportdefpath
                myreport.ReportName = areportname
                myreport.OutputFolder = auser.OutputFolder
                myreport.UserName = auser.UserName
                myreport.TableIndex = 1
                myreport.DataView = MYview
                myreport.PrintMode = printmode
                myreport.Sort = ""
                myreport.RenderReport()
                If myreport.ReportFileName.Length < 1 Then
                    akeyvals.Add("REPORTFILENAME", Nothing)
                Else
                    akeyvals.Add("REPORTFILENAME", myreport.ReportFileName)
                End If
                Return True
            Catch ex As Exception
                akeyvals.Add("REPORTERROR", ex.InnerException.Message)
                Return False
            End Try

        End Function
        Public Shared Function PrintDataReport(ByVal auser As Users.CurrentUser, ByVal areportdef As String, ByVal areportname As String, ByVal adatasource As Object, ByVal printmode As Common.PrintMode, Optional ByVal areportprefix As String = "", Optional ByVal asubtitle1 As String = "", Optional ByVal asubtitle2 As String = "") As String
            Dim myreportname As String = areportname

            Dim myreport As New HDS.Reporting.DataReports.DataReport
            With myreport
                .ConfigFolder = auser.SettingsFolder & "Reports\"
                .ReportLayout = areportdef
                .OutputFolder = auser.OutputFolder
                .ReportName = myreportname
                .KeyValues.SetValue("SUBTITLE1", asubtitle1)
                .KeyValues.SetValue("SUBTITLE2", asubtitle2)
                .FilePrefix = GetReportPrefix(auser, areportprefix)
                .ReportFormat = HDS.Reporting.ReportFormats.PDF
                .ExportFormat = HDS.Reporting.ExportFormats.NONE
                Select Case printmode

                    Case Common.PrintMode.SaveToPDFExcel
                        .ReportFormat = HDS.Reporting.ReportFormats.PDF
                        .ExportFormat = HDS.Reporting.ExportFormats.CSV
                        .DisposeMode = HDS.Reporting.DisposeModes.SAVE

                    Case Common.PrintMode.PrintDirect
                        .DisposeMode = HDS.Reporting.DisposeModes.PRINT

                    Case Else
                        .DisposeMode = HDS.Reporting.DisposeModes.SAVE
                End Select

                If .Render(adatasource) = True Then
                    Return .ReportPath
                Else
                    Return Nothing
                End If

            End With

            Return Nothing

        End Function
        Public Shared Function PrintDataReport(ByVal auser As Users.CurrentUser, ByVal areportdef As String, ByVal areportname As String, ByVal adatasource As DataTable, ByVal printmode As Common.PrintMode, Optional ByVal areportprefix As String = "", Optional ByVal asubtitle1 As String = "", Optional ByVal asubtitle2 As String = "") As String
            Dim myreportname As String = areportname

            Dim myreport As New HDS.Reporting.DataReports.DataReport
            With myreport
                .ConfigFolder = auser.SettingsFolder & "Reports\"
                .ReportLayout = areportdef
                .OutputFolder = auser.OutputFolder
                .ReportName = myreportname
                .KeyValues.SetValue("SUBTITLE1", asubtitle1)
                .KeyValues.SetValue("SUBTITLE2", asubtitle2)
                .FilePrefix = GetReportPrefix(auser, areportprefix)
                .ReportFormat = HDS.Reporting.ReportFormats.PDF
                .ExportFormat = HDS.Reporting.ExportFormats.NONE
                Select Case printmode

                    Case Common.PrintMode.SaveToPDFExcel
                        .ReportFormat = HDS.Reporting.ReportFormats.PDF
                        .ExportFormat = HDS.Reporting.ExportFormats.CSV
                        .DisposeMode = HDS.Reporting.DisposeModes.SAVE

                    Case Common.PrintMode.PrintDirect
                        .DisposeMode = HDS.Reporting.DisposeModes.PRINT

                    Case Else
                        .DisposeMode = HDS.Reporting.DisposeModes.SAVE
                End Select

                If .GenerateReport(adatasource) = True Then
                    Return .ReportPath
                Else
                    Return Nothing
                End If

            End With

            Return Nothing

        End Function
        Public Shared Function PrintC1Report(ByVal auser As Users.CurrentUser, ByVal areportdef As String, ByVal areportname As String, ByVal MYview As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode, Optional ByVal areportprefix As String = "", Optional ByVal asubtitle1 As String = "", Optional ByVal asubtitle2 As String = "") As String
            Dim myreportname As String = areportname

            If MYview.Count < 1 Then
                Return Nothing
            End If

            Dim myreport As New BLL.Common.StandardReport
            myreport.OutputFolder = auser.OutputFolder & "\"
            myreport.UserName = auser.UserName
            myreport.ReportDefPath = auser.SettingsFolder & "\Reports\" & areportdef
            myreport.ReportName = myreportname
            myreport.TableIndex = 1
            myreport.DataView = MYview
            myreport.PrintMode = printmode
            myreport.SubTitle1 = asubtitle1
            myreport.SubTitle2 = asubtitle2
            myreport.Sort = ""
            myreport.FilePrefix = GetReportPrefix(auser, areportprefix)

            myreport.RenderReport()
            If myreport.ReportFileName.Length < 1 Then
                Return Nothing
            Else
                Return myreport.ReportFileName
            End If

            Return Nothing

        End Function
        Public Shared Function PrintC1Report(ByVal auser As Users.CurrentUser, ByVal areportdef As String, ByVal areportname As String, ByVal MYview As DataView, ByVal printmode As COMMON.PrintMode, Optional ByVal areportprefix As String = "", Optional ByVal asubtitle1 As String = "", Optional ByVal asubtitle2 As String = "") As String
            Dim myreportname As String = areportname

            If MYview.Count < 1 Then
                Return Nothing
            End If

            Dim myreport As New BLL.COMMON.StandardReport
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\" & areportdef
            myreport.ReportName = myreportname
            myreport.TableIndex = 1
            myreport.DataView = MYview
            myreport.PrintMode = printmode
            myreport.SubTitle1 = asubtitle1
            myreport.SubTitle2 = asubtitle2
            myreport.Sort = ""
            myreport.FilePrefix = GetReportPrefix(auser, areportprefix)

            myreport.RenderReport()
            If myreport.ReportFileName.Length < 1 Then
                Return Nothing
            Else
                Return myreport.ReportFileName
            End If

            Return Nothing

        End Function
        Public Shared Function ExportCSV(ByVal auser As Users.CurrentUser, ByVal areportname As String, ByVal MYview As DAL.COMMON.TableView, Optional ByVal areportprefix As String = "", Optional ByVal acols As String = "") As String

            Dim myspreadsheetpath As String = Common.FileOps.GetFileName(auser, areportname)
            If acols.Length > 1 Then
                Dim scols As String() = Strings.Split(acols, ",")
                Dim scol As String = ""
                For Each scol In scols
                    If InStr(scol, "~") > 0 Then
                        Dim keyval As String() = Strings.Split(scol, "~")
                        MYview.ExportColumns.Add(keyval(0), keyval(1))
                    Else
                        MYview.ExportColumns.Add(scol)
                    End If
                Next
            End If

            If MYview.ExportToCSV(myspreadsheetpath) = True Then
                Return myspreadsheetpath
            Else
                Return Nothing
            End If
            Return Nothing

        End Function
        Public Shared Function GetReportPrefix(ByVal auser As Users.CurrentUser, Optional ByVal aprefix As String = "") As String
            Dim myprefix As String = aprefix
            If myprefix.Length > 1 Then
                myprefix += ""
            Else
                myprefix += auser.UserName
                myprefix += "-" & auser.Id
            End If
            Return myprefix
        End Function
        Public Shared Function GetFileName(ByVal auser As Users.CurrentUser, ByVal areportname As String, Optional ByVal aprefix As String = "") As String
            Dim myprefix As String = aprefix
            myprefix += "-" & auser.UserName
            myprefix += "-" & auser.Id
            myprefix += "-" & areportname
            If System.IO.Directory.Exists(auser.OutputFolder) = False Then
                System.IO.Directory.CreateDirectory(auser.OutputFolder)
            End If
            Return auser.OutputFolder & Common.FileOps.TrimSpace(myprefix & "-" & Format(Now(), "yyyyMMddhhmmss") & ".CSV")
        End Function
       
    End Class

End Namespace
