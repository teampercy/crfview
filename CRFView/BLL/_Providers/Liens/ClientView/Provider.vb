Imports Microsoft.VisualBasic

Imports System
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Web.Configuration
Namespace Providers
    Public Class LiensClientView
        Private Shared _isInitialized As Boolean = False
        Private Shared _provider As ILiensClientViewProvider
        ' <summary>
        ' Returns provider
        ' </summary>
        Public Shared ReadOnly Property Provider() As ILiensClientViewProvider
            Get
                Initialize()
                Return _provider
            End Get
        End Property
        Private Shared Sub Initialize()
            If Not _isInitialized Then
                _provider = SingletonProvider(Of Providers.SQLLiensClientViewProvider).Instance
                _isInitialized = True
            End If
        End Sub
        Public Shared Function GetOtherNoticesForState(ByVal auser As Users.CurrentUser, ByVal astateinitials As String) As DAL.COMMON.TableView
            Return Provider.GetOtherNoticeForms(auser, astateinitials)
        End Function
        Public Shared Function GetWaiverFormsForState(ByVal auser As Users.CurrentUser, ByVal astateinitials As String) As DAL.COMMON.TableView
            Return Provider.GetWaiverFormsForState(auser, astateinitials)
        End Function
        Public Shared Function GetSignersForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String) As DAL.COMMON.TableView
            Return Provider.GetSignersForClient(auser, aclientid)
        End Function
        Public Shared Function GetJobInfo(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As CRF.BLL.CRFDB.VIEWS.vwJobInfo
            Return Provider.GetJobInfo(auser, aitemid)
        End Function
        Public Shared Function GetClientViewGroups(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Return Provider.GetClientViewGroups(auser)
        End Function
        Public Shared Function GetClientListforUser(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Return Provider.GetClientforUser(auser)
        End Function
        Public Shared Function GetClientSignersforUser(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Return Provider.GetClientSignersforUser(auser)
        End Function
        Public Shared Function PrintNoJobWaiver(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String
            Return Provider.PrintNoJobWaiver(auser, aitemid)
        End Function
        Public Shared Function PrintJobWaiver(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String
            Return Provider.PrintJobWaiver(auser, aitemid)
        End Function
        Public Shared Function PrintPrelimNotice(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String
            Return Provider.PrintPrelimNotice(auser, aitemid)
        End Function
        Public Shared Function PrintOtherNotice(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String
            Return Provider.PrintOtherNotice(auser, aitemid)
        End Function
        Public Shared Function PrintJobsSubmitted(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.uspbo_ClientView_GetJobInventory, ByVal amode As Common.PrintMode) As String
            Return Provider.PrintJobsSubmitted(auser, asproc, amode)
        End Function
        Public Shared Function PrintOpenItems(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.uspbo_ClientView_GetJobInventory, ByVal amode As Common.PrintMode) As String
            Return Provider.PrintOpenItems(auser, asproc, amode)
        End Function
        Public Shared Function PrintNoticeSent(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.uspbo_ClientView_GetJobInventory, ByVal amode As Common.PrintMode) As String
            Return Provider.PrintNoticeSent(auser, asproc, amode)
        End Function
        Public Shared Function PrintTexasPending(ByVal auser As Users.CurrentUser, ByVal asproc As CRFDB.SPROCS.uspbo_ClientView_GetTexasRequests, ByVal amode As Common.PrintMode) As String
            Return Provider.PrintTexasPending(auser, asproc, amode)
        End Function
        Public Shared Function PrintJobNotes(ByVal auser As Users.CurrentUser, ByVal aitemid As String) As String
            Return Provider.PrintJobNotes(auser, aitemid)
        End Function
        Public Shared Function PrintJobAck(ByVal auser As Users.CurrentUser, ByVal aitemid As String) As String
            Return Provider.PrintJobAck(auser, aitemid)
        End Function
        Public Shared Function PrintJobsSubmitted(ByVal auser As Users.CurrentUser) As String
            Return Provider.PrintJobsSubmitted(auser)
        End Function
        Public Shared Function PrintTexasRequestAck(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String
            Return Provider.PrintTexasRequestAck(auser, aitemid)
        End Function

        Public Shared Function GetCustomersByNameForUser(ByVal auser As Users.CurrentUser, ByVal afilter As String) As DAL.COMMON.TableView
            Return Provider.GetCustomersByNameForUser(auser, afilter)

        End Function
        Public Shared Function GetCustomersByRefForUser(ByVal auser As Users.CurrentUser, ByVal afilter As String) As DAL.COMMON.TableView
            Return Provider.GetCustomersByRefForUser(auser, afilter)

        End Function
        Public Shared Function GetGCByNameForUser(ByVal auser As Users.CurrentUser, ByVal afilter As String) As DAL.COMMON.TableView
            Return Provider.GetGCByNameForUser(auser, afilter)

        End Function
        Public Shared Function GetCustomersByNameForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal afilter As String) As DAL.COMMON.TableView
            Return Provider.GetCustomersByNameForClient(auser, aclientid, afilter)
        End Function
        Public Shared Function GetCustomersByRefForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal afilter As String) As DAL.COMMON.TableView
            Return Provider.GetCustomersByRefForClient(auser, aclientid, afilter)
        End Function
        Public Shared Function GetGCByNameForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal afilter As String) As DAL.COMMON.TableView
            Return Provider.GetGCByNameForClient(auser, aclientid, afilter)
        End Function
        Public Shared Function GetCityStateforZip(ByVal auser As Users.CurrentUser, ByVal azip As String) As DAL.COMMON.TableView
            Return Provider.GetCityStateforZip(auser, azip)
        End Function
        Public Shared Function GetUserInfo(ByVal auser As Users.CurrentUser) As ClientView.ClientUserInfo
            Return Provider.GetUserInfo(auser)
        End Function
        Public Shared Function PrintTexasStatements(ByVal auser As Users.CurrentUser, ByVal asproc As CRFDB.SPROCS.uspbo_ClientView_GetTexasStatements)
            Return Provider.PrintTexasStatements(auser, asproc)
        End Function

    End Class
End Namespace
