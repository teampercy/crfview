Imports Microsoft.VisualBasic

Imports System
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Web.Configuration
Namespace Providers
    Public Class LiensDataEntry
        Private Shared _isInitialized As Boolean = False
        Private Shared _provider As ILiensDataEntryProvider
        ' <summary>
        ' Returns provider
        ' </summary>
        Public Shared ReadOnly Property Provider() As ILiensDataEntryProvider
            Get
                Initialize()
                Return _provider

            End Get
        End Property
        Private Shared Sub Initialize()
            If Not _isInitialized Then
                _provider = SingletonProvider(Of Providers.SQLLiensDataEntryProvider).Instance
                _isInitialized = True
            End If
        End Sub
        Public Shared Function GetMyOpenBatchList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Return Provider.GetMyOpenBatchList(auser)
        End Function
        Public Shared Function PostBatches(ByVal auser As Users.CurrentUser) As Boolean
            Return Provider.PostBatches(auser)
        End Function
        Public Shared Function PrintBatchList(ByVal auser As Users.CurrentUser, ByVal aprintmode As Common.PrintMode, ByVal aview As DAL.COMMON.TableView) As String
            Return Provider.PrintBatchList(auser, aprintmode, aview)
        End Function
        Public Shared Function PrintBatchJobInfo(ByVal auser As Users.CurrentUser, ByVal aview As DAL.COMMON.TableView, ByVal aprintmode As Common.PrintMode) As String
            Return Provider.PrintBatchList(auser, aprintmode, aview)
        End Function
        Public Shared Function PrintBatchOtherLegals(ByVal auser As Users.CurrentUser, ByVal aview As DAL.COMMON.TableView, ByVal aprintmode As Common.PrintMode) As String
            Return Provider.PrintBatchOtherLegals(auser, aview, aprintmode)
        End Function
        Public Shared Function GetWebJobsSubmitted(ByVal auser As Users.CurrentUser, ByVal aView As DAL.COMMON.TableView, ByVal aprintmode As COMMON.PrintMode, Optional ByVal filename As String = "WebJobsSubmitted") As String
            Return Provider.GetWebJobsSubmitted(auser, aView, aprintmode, filename)
        End Function
        Public Shared Function GetJobsInBatches(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_LiensDataEntry_GetJobsInBatches) As DAL.COMMON.TableView
            Return Provider.GetJobsInBatches(auser, asproc)
        End Function
        Public Shared Function GetNewJobInfo(ByVal auser As Users.CurrentUser, ByVal anewjobid As String) As DataSet
            Return Provider.GetNewJobInfo(auser, anewjobid)
        End Function
        Public Shared Function GetCustomersForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String) As DAL.COMMON.TableView
            Return Provider.GetCustomersForClient(auser, aclientid)
        End Function
        Public Shared Function GetGContractorsForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String) As DAL.COMMON.TableView
            Return Provider.GetGContractorsForClient(auser, aclientid)
        End Function
        Public Shared Function GetStateInfo(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Return Provider.GetStateInfo(auser)
        End Function
        Public Shared Function GetLienClients(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Return Provider.GetLienClients(auser)
        End Function
        Public Shared Function GetNewJobEntryActions(ByVal auser As Users.CurrentUser, ByVal IsTXJob As Boolean) As DAL.COMMON.TableView
            Return Provider.GetNewJobEntryActions(auser, IsTXJob)
        End Function
        Public Shared Function PostWebPlacements(ByVal auser As Users.CurrentUser) As DataSet
            Return Provider.PostWebPlacements(auser)
        End Function
        Public Shared Function GetUserList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Return Provider.GetUserList(auser)
        End Function
        Public Shared Function GetLienInterfaces(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Return Provider.GetLienInterfaces(auser)
        End Function
        Public Shared Function GetCustomersByNameForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal afilter As String) As DataTable
            Return Provider.GetCustomersByNameForClient(auser, aclientid, afilter)

        End Function
        Public Shared Function GetCustomersByRefForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal afilter As String) As DataTable
            Return Provider.GetCustomersByRefForClient(auser, aclientid, afilter)

        End Function
        Public Shared Function GetGCByNameForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal afilter As String) As DataTable
            Return Provider.GetGCByNameForClient(auser, aclientid, afilter)

        End Function
        Public Shared Function GetCityStateforZip(ByVal auser As Users.CurrentUser, ByVal azip As String) As DAL.COMMON.TableView
            Return Provider.GetCityStateforZip(auser, azip)
        End Function
        Public Shared Function GetStateInfoforState(ByVal auser As Users.CurrentUser, ByVal astateinitials As String) As TABLES.StateInfo
            Return Provider.GetStateInfoforState(auser, astateinitials)
        End Function
        Public Shared Function GetLienInfoforClient(ByVal auser As Users.CurrentUser, ByVal aclientid As Integer) As CRF.BLL.CRFDB.TABLES.ClientLienInfo
            Return Provider.GetLienInfoforClient(auser, aclientid)
        End Function
        Public Shared Function GetClientInfoforClient(ByVal auser As Users.CurrentUser, ByVal aclientid As Integer) As CRF.BLL.CRFDB.TABLES.Client
            Return Provider.GetClientInfoforClient(auser, aclientid)
        End Function
        Public Shared Function GetReleasedBatches(ByVal auser As Users.CurrentUser) As DataSet
            Return Provider.GetReleasedBatches(auser)
        End Function
    End Class
End Namespace
