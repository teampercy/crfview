Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Web.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Xml
Imports System.Xml.Schema
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections.Specialized
Imports System.Configuration.Provider
Imports System.Web.Hosting
Imports System.Web.Management
Imports System.Security.Permissions
Imports System.Text
Imports System.Security.Cryptography
Namespace Providers
    Friend Class SQLLiensDataEntryProvider
        Inherits ProviderBase
        Implements ILiensDataEntryProvider

        Public Function GetMyOpenBatchList(ByVal auser As Users.CurrentUser) As HDS.DAL.COMMON.TableView Implements ILiensDataEntryProvider.GetMyOpenBatchList
            Dim sql As String = "Select BatchId As BatchId, BatchStatus, BatchDate from Batch Where SubmittedById = '" & auser.Id & "' and BatchType IN ('LIEN', 'LIENCV' ) And BatchStatus = 'OPEN'"
            Return DBO.Provider.GetTableView(sql)
        End Function
        Public Function GetWebJobsSubmitted(ByVal auser As Users.CurrentUser, ByVal MyView As HDS.DAL.COMMON.TableView, ByVal printmode As COMMON.PrintMode, Optional ByVal filename As String = "WebJobsSubmitted") As String Implements ILiensDataEntryProvider.GetWebJobsSubmitted
            If MyView.Count < 1 Then
                Return Nothing
            End If
            Dim myspreadsheetpath As String = COMMON.FileOps.GetFileName(auser, filename, ".CSV", True)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.ClientId)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.ClientCode)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.BranchNum)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.JobNum)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.JobName)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.APNNum)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.JobAdd1)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.JobAdd2)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.JobCity)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.JobState)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.JobZip)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.JobCounty)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.EstBalance)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.CustId)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.CustJobNum)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.CustName)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.CustContactName)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.CustRefNum)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.CustAdd1)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.CustAdd2)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.CustCity)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.CustState)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.CustZip)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.CustPhone1)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.CustPhone2)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.CustFax)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.GenId)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.GCName)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.GCRefNum)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.GCAdd1)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.GCAdd2)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.GCCity)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.GCState)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.GCZip)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.GCPhone1)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.GCPhone2)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.GCFax)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.OwnrName)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.OwnrAdd1)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.OwnrAdd2)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.OwnrCity)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.OwnrState)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.OwnrZip)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.OwnrPhone1)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.OwnrPhone2)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.OwnrFax)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.LenderName)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.LenderAdd1)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.LenderAdd2)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.LenderCity)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.LenderState)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.LenderZip)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.LenderPhone1)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.LenderPhone2)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.LenderFax)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.BondNum)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.PONum)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.FolioNum)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.DesigneeName)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.DesigneeAdd1)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.DesigneeAdd2)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.DesigneeCity)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.DesigneeState)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.DesigneeZip)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.DesigneePhone1)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.DesigneePhone2)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.DesigneeFax)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.StartDate)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.EndDate)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.EquipRate)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.EquipRental)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.GreenCard)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.JointCk)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.PrelimBox)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.VerifyJob)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.TitleVerifiedBox)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.PrelimASIS)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.RushOrder)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.RushOrderML)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.MechanicLien)
            '   MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.MechLienRush)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.PrivateJob)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.PublicJob)
            '   MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.ReleaseRush)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.ResidentialBox)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.RevDate)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.StopNoticeBox)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.SpecialInstruction)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.BatchId)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.SubmittedOn)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.SubmittedByUserId)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.Id)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.AZLotAllocate)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.Note)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.BackUpBox)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.LienRelease)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.StopNoticeBox)
            MyView.ExportColumns.Add(vwBatchJobExport.ColumnNames.PaymentBond)

            MyView.ExportToCSV(myspreadsheetpath)

            Return True

        End Function
        Public Function PrintBatchJobInfo(ByVal auser As Users.CurrentUser, ByVal aview As HDS.DAL.COMMON.TableView, ByVal aprintmode As COMMON.PrintMode) As String Implements ILiensDataEntryProvider.PrintBatchJobInfo
            Dim myreport As New BLL.COMMON.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\LienReports.xml"
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.ReportName = "BatchJobInfo"
            myreport.TableIndex = 1
            myreport.DataView = aview
            myreport.PrintMode = COMMON.PrintMode.PrintToPDF
            myreport.Sort = ""
            myreport.RenderReport()
            Return myreport.ReportFileName
        End Function
        Public Function PrintBatchList(ByVal auser As Users.CurrentUser, ByVal aprintmode As COMMON.PrintMode, ByVal aview As HDS.DAL.COMMON.TableView) As String Implements ILiensDataEntryProvider.PrintBatchList
            Dim myreport As New BLL.COMMON.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\LienReports.xml"
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.ReportName = "BatchJobList"
            myreport.TableIndex = 1
            myreport.DataView = aview
            myreport.PrintMode = COMMON.PrintMode.PrintToPDF
            myreport.Sort = ""
            myreport.RenderReport()
            Return myreport.ReportFileName
        End Function
        Public Function PrintBatchOtherLegals(ByVal auser As Users.CurrentUser, ByVal aview As HDS.DAL.COMMON.TableView, ByVal aprintmode As COMMON.PrintMode) As String Implements ILiensDataEntryProvider.PrintBatchOtherLegals
            If aview.Count < 1 Then
                Return Nothing
            End If

            Dim myreport As New BLL.COMMON.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\LienReports.xml"
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.ReportName = "BatchJobOtherLegals"
            myreport.TableIndex = 1
            myreport.DataView = aview
            myreport.PrintMode = COMMON.PrintMode.PrintToPDF
            myreport.Sort = ""
            myreport.RenderReport()
            Return myreport.ReportFileName

        End Function
        Public Function PostBatches(ByVal auser As Users.CurrentUser) As Boolean Implements ILiensDataEntryProvider.PostBatches
            Dim mysproc As New BLL.CRFDB.SPROCS.uspbo_LiensDataEntry_PostBatches
            Return DBO.Provider.ExecScalar(mysproc)
        End Function
        Public Function GetJobsInBatches(ByVal auser As Users.CurrentUser, ByVal asproc As CRFDB.SPROCS.uspbo_LiensDataEntry_GetJobsInBatches) As HDS.DAL.COMMON.TableView Implements ILiensDataEntryProvider.GetJobsInBatches
            Return DBO.Provider.GetTableView(asproc)
        End Function
 
        Public Function GetNewJobInfo(ByVal auser As Users.CurrentUser, ByVal anewjobid As String) As System.Data.DataSet Implements ILiensDataEntryProvider.GetNewJobInfo
            Dim asproc As New BLL.CRFDB.SPROCS.uspbo_LiensDataEntry_GetNewJobInfo
            asproc.BatchJobId = anewjobid

            Return DBO.Provider.GetDataSet(asproc)
        End Function
        Public Function GetCustomersForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String) As HDS.DAL.COMMON.TableView Implements ILiensDataEntryProvider.GetCustomersForClient
            Dim mysproc As New BLL.CRFDB.SPROCS.uspbo_Jobs_GetCustomerList
            mysproc.ClientId = aclientid
            Return DBO.Provider.GetTableView(mysproc)
        End Function
        Public Function GetGContractorsForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String) As HDS.DAL.COMMON.TableView Implements ILiensDataEntryProvider.GetGContractorsForClient
            Dim mysproc As New BLL.CRFDB.SPROCS.uspbo_Jobs_GetGeneralContractorList
            mysproc.ClientId = aclientid
            Return DBO.Provider.GetTableView(mysproc)
        End Function
        Public Function GetLienClients(ByVal auser As Users.CurrentUser) As HDS.DAL.COMMON.TableView Implements ILiensDataEntryProvider.GetLienClients
            Return Queries.GetLienClients
        End Function
        Public Function GetStateInfo(ByVal auser As Users.CurrentUser) As HDS.DAL.COMMON.TableView Implements ILiensDataEntryProvider.GetStateInfo
            Return Queries.StateInfo
        End Function
        Public Function GetNewJobEntryActions(ByVal auser As Users.CurrentUser, ByVal IsTXJob As Boolean) As HDS.DAL.COMMON.TableView Implements ILiensDataEntryProvider.GetNewJobEntryActions
            Dim myQuery2 As New DAL.COMMON.Query(New CRFDB.TABLES.JobActions)
            myQuery2.AddCondition(DAL.COMMON.LogicalOperators._And, JobActions.ColumnNames.IsDataEntry, DAL.COMMON.Conditions._IsEqual, 1)

            If IsTXJob = True Then
                myQuery2.AddCondition(DAL.COMMON.LogicalOperators._And, JobActions.ColumnNames.IsTXJobAction, DAL.COMMON.Conditions._IsEqual, 1)
            Else
                myQuery2.AddCondition(DAL.COMMON.LogicalOperators._And, JobActions.ColumnNames.IsTXJobAction, DAL.COMMON.Conditions._IsEqual, 0)
            End If
            Dim sql As String = myQuery2.BuildQuery
            Return DBO.Provider.GetTableView(sql)
        End Function
        Public Function PostWebPlacements(ByVal auser As Users.CurrentUser) As DataSet Implements ILiensDataEntryProvider.PostWebPlacements
            Dim mysproc As New BLL.CRFDB.SPROCS.uspbo_ClientView_BatchNewJobs
            Return DBO.Provider.GetDataSet(mysproc)
        End Function
        Public Function GetUserList(ByVal auser As Users.CurrentUser) As HDS.DAL.COMMON.TableView Implements ILiensDataEntryProvider.GetUserList
            Return Queries.GetInternalUsers
        End Function
        Public Function GetLienInterfaces(ByVal auser As Users.CurrentUser) As HDS.DAL.COMMON.TableView Implements ILiensDataEntryProvider.GetLienInterfaces
            Dim myquery As New DAL.COMMON.Query(New CRFDB.TABLES.ClientInterface)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, ClientInterface.ColumnNames.InterfaceType, DAL.COMMON.Conditions._IsEqual, "L")
            Dim MYVIEW As HDS.DAL.COMMON.TableView = Providers.DBO.Provider.GetTableView(myquery.SelectAll)
            MYVIEW.Sort = ClientInterface.ColumnNames.InterfaceName
            Return MYVIEW

        End Function

        Public Function GetCustomersByNameForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal afilter As String) As DataTable Implements ILiensDataEntryProvider.GetCustomersByNameForClient
            Dim mysproc As New CRFDB.SPROCS.uspbo_LiensDataEntry_GetCustomerByNameforClient
            mysproc.ClientId = aclientid
            mysproc.Filter = afilter
            Return Providers.DBO.Provider.GetDataSet(mysproc).Tables(0)

        End Function

        Public Function GetCustomersByRefForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal afilter As String) As DataTable Implements ILiensDataEntryProvider.GetCustomersByRefForClient
            Dim mysproc As New CRFDB.SPROCS.uspbo_LiensDataEntry_GetCustomerByRefforClient
            mysproc.ClientId = aclientid
            mysproc.Filter = afilter
            Return Providers.DBO.Provider.GetDataSet(mysproc).Tables(0)

        End Function

        Public Function GetGCByNameForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal afilter As String) As DataTable Implements ILiensDataEntryProvider.GetGCByNameForClient
            Dim mysproc As New CRFDB.SPROCS.uspbo_LiensDataEntry_GetGGByNameforClient
            mysproc.ClientId = aclientid
            mysproc.Filter = afilter
            Return Providers.DBO.Provider.GetDataSet(mysproc).Tables(0)

        End Function

        Public Function GetCityStateforZip(ByVal auser As Users.CurrentUser, ByVal azip As String) As HDS.DAL.COMMON.TableView Implements ILiensDataEntryProvider.GetCityStateforZip
            Dim MYtable As New CRF.BLL.CRFDB.TABLES.PostalCode
            Dim myquery As New HDS.DAL.COMMON.Query(MYtable)
            myquery.AddCondition(HDS.DAL.COMMON.LogicalOperators._And, CRF.BLL.CRFDB.TABLES.PostalCode.ColumnNames.PostalCode, HDS.DAL.COMMON.Conditions._IsEqual, azip)
            Return Providers.DBO.Provider.GetTableView(myquery.BuildQuery)

        End Function
        Public Function GetStateInfoforState(ByVal auser As Users.CurrentUser, ByVal astateinitial As String) As TABLES.StateInfo Implements ILiensDataEntryProvider.GetStateInfoforState
            Dim MYtable As New CRF.BLL.CRFDB.TABLES.StateInfo
            Dim myquery As New HDS.DAL.COMMON.Query(MYtable)
            myquery.AddCondition(HDS.DAL.COMMON.LogicalOperators._And, CRF.BLL.CRFDB.TABLES.StateInfo.ColumnNames.StateInitials, HDS.DAL.COMMON.Conditions._IsEqual, astateinitial)
            Providers.DBO.Provider.GetTableView(myquery.BuildQuery).FillEntity(MYtable)
            Return MYtable

        End Function
        Public Function GetLienInfoforClient(ByVal auser As Users.CurrentUser, ByVal aclientid As Integer) As CRFDB.TABLES.ClientLienInfo Implements ILiensDataEntryProvider.GetLienInfoforClient
            Dim MYtable As New CRF.BLL.CRFDB.TABLES.ClientLienInfo
            Dim myquery As New HDS.DAL.COMMON.Query(MYtable)
            myquery.AddCondition(HDS.DAL.COMMON.LogicalOperators._And, CRF.BLL.CRFDB.TABLES.ClientLienInfo.ColumnNames.ClientId, HDS.DAL.COMMON.Conditions._IsEqual, aclientid)
            Dim MYVIEW As HDS.DAL.COMMON.TableView = Providers.DBO.Provider.GetTableView(myquery.BuildQuery)
            MYVIEW.FillEntity(MYtable)
            Return MYtable

        End Function
        Public Function GetClientInfoforClient(ByVal auser As Users.CurrentUser, ByVal aclientid As Integer) As CRFDB.TABLES.Client Implements ILiensDataEntryProvider.GetClientInfoforClient
            Dim MYtable As New CRF.BLL.CRFDB.TABLES.Client
            Dim myquery As New HDS.DAL.COMMON.Query(MYtable)
            myquery.AddCondition(HDS.DAL.COMMON.LogicalOperators._And, CRF.BLL.CRFDB.TABLES.Client.ColumnNames.ClientId, HDS.DAL.COMMON.Conditions._IsEqual, aclientid)
            Dim MYVIEW As HDS.DAL.COMMON.TableView = Providers.DBO.Provider.GetTableView(myquery.BuildQuery)
            MYVIEW.FillEntity(MYtable)
            Return MYtable

        End Function

        Public Function GetReleasedBatches(ByVal auser As Users.CurrentUser) As System.Data.DataSet Implements ILiensDataEntryProvider.GetReleasedBatches
            Dim mysproc As New BLL.CRFDB.SPROCS.uspbo_LiensDayEnd_ExportReleasedBatches
            Return DBO.Provider.GetDataSet(mysproc)

        End Function
    End Class
End Namespace
