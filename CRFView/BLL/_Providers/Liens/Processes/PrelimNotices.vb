Imports CRF.BLL.Providers
Imports System.Windows.Forms
Imports HDS.DAL.COMMON
Namespace Liens
    Public Class PrelimNotices
        Public Shared Function PrintNotices(ByVal auser As CRF.BLL.Users.CurrentUser, ByVal aview As HDS.DAL.COMMON.TableView, ByVal afromdate As Date, ByVal athrudate As Date, ByVal generatecrt As Boolean, ByVal combinepdf As Boolean, ByRef e As HDS.DAL.COMMON.KeyValues, ByRef amode As Common.PrintMode) As Boolean

            Common.FileOps.DirectoryClean(auser.OutputFolder, Now())

            aview.MoveFirst()
            Dim err As New HDS.DAL.COMMON.KeyValues
            Do Until aview.EOF
                Dim mynoticeid As String = aview.RowItem(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.JobNoticeHistoryId)
                err = New HDS.DAL.COMMON.KeyValues
                PrintNotice(auser, mynoticeid, False, True, err, CRF.BLL.Common.PrintMode.PrintToPDF)
                aview.MoveNext()
            Loop

            aview.MoveFirst()

            auser.OutputFolder = auser.OutputFolder & "NOTICEHISTORY\"
            PrelimNotice.Reports.MailingLog(auser, aview, Common.PrintMode.PrintToPDF)
            PrelimNotice.Reports.MailingLog(auser, aview, Common.PrintMode.PrinttoExcel)

        End Function
        Public Shared Function PrintNotice(ByVal auser As Users.CurrentUser, ByVal anoticeid As String, ByVal generatecrt As Boolean, ByVal combinepdf As Boolean, ByRef e As HDS.DAL.COMMON.KeyValues, ByRef amode As Common.PrintMode) As Boolean

            Dim mysproc As New SPROCS.uspbo_PrelimNotices_GetNoticeInfo
            mysproc.JobNoticeHistoryId = anoticeid
            Dim myds As DataSet = DBO.GetDataSet(mysproc)
            Dim myview As New DAL.COMMON.TableView(myds.Tables(0))
            Dim mycover As New DAL.COMMON.TableView(myds.Tables(0))

            Dim mydate As String = Format(myview.RowItem(VIEWS.vwJobNoticeRequestInfo.ColumnNames.DatePrinted), "yyyyMMdd")
            Dim mydir1 As String = auser.OutputFolder & "NOTICEHISTORY\"
            If System.IO.Directory.Exists(mydir1) = False Then
                System.IO.Directory.CreateDirectory(mydir1)
            End If
            Dim mydir3 As String = auser.OutputFolder & "INDIVIDUAL\"
            If System.IO.Directory.Exists(mydir3) = False Then
                System.IO.Directory.CreateDirectory(mydir3)
            End If

            Dim myforms As New DAL.COMMON.TableView(myds.Tables(1))
            Dim myform As New TABLES.JobNoticeRequestForms
            Dim myfiles As New ArrayList
            Dim myreport As New BLL.Common.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\PrelimNotice.xml"
            myreport.OutputFolder = mydir3
            myreport.UserName = auser.UserName
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\PrelimNotice.xml"
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName

            Dim mynotice As New Liens.Reports.JobPrelimNotice(auser)
            mynotice.ReportDefPath = auser.SettingsFolder & "Reports\PrelimNotice.xml"
            mynotice.OutputFolder = mydir3
            mynotice.UserName = auser.UserName
            mynotice.PrintMode = Common.PrintMode.PrintToPDF

            myforms.MoveFirst()
            myforms.Sort = "SeqNo"
            Dim myfilename As String = ""
            Do Until myforms.EOF
                myfilename = myview.RowItem(VIEWS.vwJobNoticeRequestInfo.ColumnNames.ClientCode)
                myfilename += "-" & myview.RowItem(VIEWS.vwJobNoticeRequestInfo.ColumnNames.JobId)
                myfilename += "-" & myview.RowItem(VIEWS.vwJobNoticeRequestInfo.ColumnNames.JobNoticeRequestId)
                myfilename += "-" & myview.RowItem(VIEWS.vwJobNoticeRequestInfo.ColumnNames.JobNoticeHistoryId)
                myfilename += "-" & myforms.RowItem(TABLES.JobNoticeRequestForms.ColumnNames.SeqNo)
                myfilename += "-" & myview.RowItem(VIEWS.vwJobNoticeRequestInfo.ColumnNames.FormCode)
                myforms.FillEntity(myform)
                myreport.ReportName = myform.ReportName
                myreport.DataView = myview
                myreport.PrintMode = Common.PrintMode.PrintToPDF
                myreport.Sort = ""
                myreport.FilePrefix = myfilename
                If myreport.IsValid = True Then
                    Select Case myform.SeqNo
                        Case 1
                            myreport.OutputFolder = mydir3
                            myreport.DataView = mycover
                            myreport.RenderReport(False)
                            myfiles.Add(myreport.ReportFileName)

                        Case 2
                            mynotice.OutputFolder = mydir3
                            mynotice.ExecuteC1Report(myview, myfilename)
                            myfiles.Add(mynotice.ReportFileName)

                        Case 4
                            Dim myview3 As New DAL.COMMON.TableView(myds.Tables(2))
                            myreport.DataView = myview3
                            myreport.RenderReport(False)
                            myfiles.Add(myreport.ReportFileName)

                        Case 5
                            If myds.Tables(4).Rows.Count = 1 Then
                                Dim myuser As New CRF.BLL.Users.CurrentUser
                                myuser.UserName = auser.UserName
                                myuser.OutputFolder = mydir3
                                myuser.SettingsFolder = auser.SettingsFolder
                                Dim myfile As String = COMMON.Reporting.PrintDataReport(myuser, "TexasStatements.DRL", "TexasStatement", myds.Tables(4), COMMON.PrintMode.PrintToPDF, auser.UserName, "", "")
                                myfiles.Add(myfile)
                            End If
                          
                        Case Else
                            myreport.OutputFolder = mydir3
                            myreport.RenderReport(False)
                            myfiles.Add(myreport.ReportFileName)
                    End Select
                End If
                myforms.MoveNext()
            Loop

            myfilename = ""
            myfilename += myview.RowItem(VIEWS.vwJobNoticeRequestInfo.ColumnNames.JobId).ToString
            myfilename += "-" & myview.RowItem(VIEWS.vwJobNoticeRequestInfo.ColumnNames.JobNoticeRequestId).ToString
            myfilename += "-" & myview.RowItem(VIEWS.vwJobNoticeRequestInfo.ColumnNames.JobNoticeHistoryId).ToString
            myfilename += ".pdf"
            Dim mytable As New TABLES.JobNoticeHistory
            DBO.Read(anoticeid, mytable)
            If Combine(myfiles) = True Then
                System.IO.File.Copy(Application.StartupPath & "\TEMP\ONE.PDF", mydir1 & myfilename)
                mytable.NoticeImage = myfilename
                DBO.Update(mytable)
            Else
                mytable.NoticeImage = ""
                DBO.Update(mytable)
            End If
            Return True

        End Function
        Public Shared Function GetNextCERT(ByRef mycertnum As String, ByVal e As KeyValues) As Boolean
            ' CHECK IF THIS NOTICE ALREADY HAS A CERT
            Dim myusscodes As New TABLES.USSCodes
            Dim li As New HDS.DAL.COMMON.KeyValue
            DBO.Read(1, myusscodes)
            If myusscodes.Id < 1 Then
                e.Add(New KeyValue("ERROR", "USSCODES TABLE EMPTY"))
                Return False
            End If

            Dim strServiceTypeCode As String
            Dim strPackageID As String
            Dim strCustomerID As String
            Dim lngMaxPackageID As Long
            Dim vntCode(1, 19)
            Dim x As Integer
            Dim strCode As String
            Dim y As Integer, z As Integer
            Dim strTmp As String
            'Set the first row of the Array
            For x = 0 To 19
                vntCode(0, x) = -(x + 1) + 21
            Next

            strServiceTypeCode = Strings.Right("000000000" + myusscodes.ServiceTypeCode, 2)
            strCustomerID = Strings.Right("000000000" + myusscodes.CustomerID, 9)
            strPackageID = Strings.Right("000000000" + myusscodes.PackageID, 8)
            lngMaxPackageID = myusscodes.MaxPackageID

            If CLng(strPackageID) > lngMaxPackageID Then
                e.Add(New KeyValue("ERROR", "MAXIMUM NUMBER OF CERT CODES REACHED"))
                Return False
            End If

            strCode = strServiceTypeCode & strCustomerID & strPackageID

            'Make sure that we have 19 Digits
            If Len(strCode) <> 19 Then
                e.Add(New KeyValue("ERROR", "NOT ENOUGH DIGITS FOR THE SERVICE TYPE CODE"))
                Return False
            End If

            'Add the Code to the Array
            For x = 0 To 19
                vntCode(1, x) = Mid(strCode, x + 1, 1)
            Next

            'Now we'll add up the even codes
            For x = 0 To 19
                Select Case x
                    Case 0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20
                        y = y + vntCode(1, x)
                End Select
            Next

            y = y * 3

            'Now we'll add up the odd codes
            For x = 0 To 19
                Select Case x
                    Case 1, 3, 5, 7, 9, 11, 13, 15, 17
                        z = z + vntCode(1, x)
                End Select
            Next

            z = z + y

            'Get the right most number
            z = Strings.Right(z, 1)

            'The Check digit is the number that added to z = 10
            If z = 0 Then
                z = 0
            Else
                z = 10 - z
            End If

            strTmp = strCode & z

            mycertnum = Strings.Left(strTmp, 4) & " " & Mid(strTmp, 5, 4) & " " & Mid(strTmp, 9, 4) & " " & Mid(strTmp, 13, 4) & " " & Mid(strTmp, 17, 4)
            myusscodes.PackageID = myusscodes.PackageID + 1
            DBO.Update(myusscodes)
            Return True

        End Function
        Public Shared Function Combine(ByVal afiles As ArrayList) As Boolean

            Try

                If System.IO.Directory.Exists(Application.StartupPath & "\TEMP\") Then
                    System.IO.Directory.Delete(Application.StartupPath & "\TEMP\", True)
                End If

                System.IO.Directory.CreateDirectory(Application.StartupPath & "\TEMP\")

                Dim FILE As String
                For Each FILE In afiles
                    Dim OUTFILE As String = Application.StartupPath & "\TEMP\" & System.IO.Path.GetFileName(FILE)
                    If System.IO.File.Exists(FILE) = True Then
                        System.IO.File.Copy(FILE, OUTFILE)
                    End If
                Next

                Dim S As String = Application.StartupPath & "\PDFTK  " & Application.StartupPath & "\TEMP\*.PDF CAT OUTPUT " & Application.StartupPath & "\TEMP\ONE.PDF"

                Shell(S, AppWinStyle.Hide, True)
                Application.DoEvents()


            Catch EX As Exception
                MsgBox(EX.Message, MsgBoxStyle.Critical)
                Return False
            Finally
            End Try
            Return True

        End Function
    End Class

End Namespace
