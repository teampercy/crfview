Namespace ClientView
    Public Class ClientUserInfo
        Dim _client As DAL.COMMON.TableView
        Dim _collinfo As DAL.COMMON.TableView
        Dim _lieninfo As DAL.COMMON.TableView
        Dim _mainclient As DAL.COMMON.TableView
        Dim _contracts As DAL.COMMON.TableView
        Dim _portaluser As DAL.COMMON.TableView
        Public Sub New(ByVal auserid)
            MyBase.New()
            Dim mysproc As New CRF.BLL.CRFDB.SPROCS.uspbo_ClientView_GetUserInfo
            Dim myds As DataSet
            mysproc.UserId = auserid
            myds = DBO.GetDataSet(mysproc)
            _client = New DAL.COMMON.TableView(myds.Tables(0), "")
            _collinfo = New DAL.COMMON.TableView(myds.Tables(1), "")
            _lieninfo = New DAL.COMMON.TableView(myds.Tables(2), "")
            _mainclient = New DAL.COMMON.TableView(myds.Tables(4), "")
            _contracts = New DAL.COMMON.TableView(myds.Tables(3), "")
            _portaluser = New DAL.COMMON.TableView(myds.Tables(5), "")

        End Sub
        Public ReadOnly Property Client() As TABLES.Client
            Get
                Dim myobj As New TABLES.Client
                Return _client.FillEntity(myobj)
            End Get
        End Property
        Public ReadOnly Property CollectionInfo() As TABLES.ClientCollectionInfo
            Get
                Dim myobj As New TABLES.ClientCollectionInfo
                Return _collinfo.FillEntity(myobj)
            End Get
        End Property
        Public ReadOnly Property LienInfo() As TABLES.ClientLienInfo
            Get
                Dim myobj As New TABLES.ClientLienInfo
                Return _lieninfo.FillEntity(myobj)
            End Get
        End Property
        Public ReadOnly Property MainClientInfo() As TABLES.Client
            Get
                Dim myobj As New TABLES.Client
                Return _mainclient.FillEntity(myobj)
            End Get
        End Property
        Public ReadOnly Property ContractInfo() As TABLES.ClientContract
            Get
                Dim myobj As New TABLES.ClientContract
                Return _contracts.FillEntity(myobj)
            End Get
        End Property
        Public ReadOnly Property UserInfo() As TABLES.Portal_Users
            Get
                Dim myobj As New TABLES.Portal_Users
                Return _portaluser.FillEntity(myobj)
            End Get
        End Property

        Public Function HasCollectionService() As Boolean
            _contracts.RowFilter = ""
            If _contracts.Count = 0 Then
                Return False
            End If
            _contracts.RowFilter = " ContractTypeId = 1 And IsClientViewCollection = 1 "
            If _contracts.Count < 1 Then
                Return False
            End If
            Return True

        End Function
        Public Function CollectionContractId() As TABLES.ClientContract
            _contracts.RowFilter = ""
            If _contracts.Count = 0 Then
                Return Nothing
            End If
            _contracts.RowFilter = " ContractTypeId = 1 And IsClientViewCollection = 1 "
            If _contracts.Count < 1 Then
                Return Nothing
            End If
            Dim myobj As New TABLES.ClientContract
            Return _contracts.FillEntity(myobj)

        End Function
        Public Function HasLienService() As Boolean
            _contracts.RowFilter = ""
            If _contracts.Count = 0 Then
                Return 0
            End If
            _contracts.RowFilter = " ContractTypeId = 2 And IsClientViewLiens = 1 "
            If _contracts.Count < 1 Then
                Return False
            End If
            Return True

        End Function
        Public Function LienContractId() As TABLES.ClientContract
            _contracts.RowFilter = ""
            If _contracts.Count = 0 Then
                Return Nothing
            End If
            _contracts.RowFilter = " ContractTypeId = 2 And IsClientViewLiens = 1 "
            If _contracts.Count < 1 Then
                Return Nothing
            End If

            Dim myobj As New TABLES.ClientContract
            Return _contracts.FillEntity(myobj)

        End Function
    End Class
End Namespace
