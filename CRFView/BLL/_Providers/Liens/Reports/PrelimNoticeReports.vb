Namespace PrelimNotice
    Public Class Reports
        Public Shared Function MailingLog(ByVal auser As Users.CurrentUser, ByVal MYview As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode) As String
            Dim myreportname As String = "MailingLog"

            If MYview.Count < 1 Then
                Return Nothing
            End If

            If printmode = Common.PrintMode.PrintToPDF Then
                Return PrintC1Report(auser, MYview, printmode, myreportname)
            End If

            If printmode.PrinttoExcel Then
                Dim myspreadsheetpath As String = GetFileName(auser, myreportname)
                MYview.ExportColumns.Add(VIEWS.vwJobNoticeRequestHistory.ColumnNames.DatePrinted)
                MYview.ExportColumns.Add(VIEWS.vwJobNoticeRequestHistory.ColumnNames.PrintMajorGroup)
                MYview.ExportColumns.Add(VIEWS.vwJobNoticeRequestHistory.ColumnNames.PrintGroup)
                MYview.ExportColumns.Add(VIEWS.vwJobNoticeRequestHistory.ColumnNames.Greencard)

                MYview.ExportColumns.Add(VIEWS.vwJobNoticeRequestHistory.ColumnNames.JobId)
                MYview.ExportColumns.Add(VIEWS.vwJobNoticeRequestHistory.ColumnNames.ClientCode)
                MYview.ExportColumns.Add(VIEWS.vwJobNoticeRequestHistory.ColumnNames.JobName)
                MYview.ExportColumns.Add(VIEWS.vwJobNoticeRequestHistory.ColumnNames.CertNum)
                MYview.ExportColumns.Add(VIEWS.vwJobNoticeRequestHistory.ColumnNames.CoverName)
                MYview.ExportColumns.Add(VIEWS.vwJobNoticeRequestHistory.ColumnNames.PostageRate)
                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    Return myspreadsheetpath
                Else
                    Return Nothing
                End If

            End If

            Return Nothing

        End Function
        Public Shared Function PrintC1Report(ByVal auser As Users.CurrentUser, ByVal MYview As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode, ByVal areportname As String) As String
            Dim myreportname As String = areportname

            If MYview.Count < 1 Then
                Return Nothing
            End If

            If printmode = Common.PrintMode.PrintToPDF Then
                Dim myreport As New BLL.Common.StandardReport
                myreport.ReportDefPath = auser.SettingsFolder & "Reports\PrelimNotice.xml"
                myreport.OutputFolder = auser.OutputFolder
                myreport.UserName = auser.UserName
                myreport.ReportName = myreportname
                myreport.TableIndex = 1
                myreport.DataView = MYview
                myreport.PrintMode = printmode
                myreport.Sort = ""
                myreport.FilePrefix = GetReportPrefix(auser)

                myreport.RenderReport()
                If myreport.ReportFileName.Length < 1 Then
                    Return Nothing
                Else
                    Return myreport.ReportFileName
                End If
            End If

            Return Nothing

        End Function
        Public Shared Function GetReportPrefix(ByVal auser As Users.CurrentUser) As String
            Dim myuser As New ClientView.ClientUserInfo(auser.Id)
            Dim myprefix As String = myuser.Client.ClientCode
            myprefix += "-" & myuser.UserInfo.LoginCode
            myprefix += "-" & myuser.UserInfo.ID
            Return myprefix
        End Function
        Public Shared Function GetFileName(ByVal auser As Users.CurrentUser, ByVal areportname As String) As String
            Dim myuser As New ClientView.ClientUserInfo(auser.Id)
            Dim myprefix As String = myuser.Client.ClientCode
            myprefix += "-" & myuser.UserInfo.LoginCode
            myprefix += "-" & myuser.UserInfo.ID
            myprefix += "-" & areportname
            If System.IO.Directory.Exists(auser.OutputFolder) = False Then
                System.IO.Directory.CreateDirectory(auser.OutputFolder)
            End If
            Return auser.OutputFolder & Common.FileOps.TrimSpace(myprefix & "-" & Format(Now(), "yyyyMMddhhmmss") & ".CSV")
        End Function

    End Class
End Namespace
