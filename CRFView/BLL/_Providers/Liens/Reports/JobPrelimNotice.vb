Imports System
Imports System.Windows.Forms
Imports C1
Imports C1.C1Report
Imports CRF.BLL.clientview
Namespace Liens.Reports
    Public Class JobPrelimNotice
        Inherits Common.StandardReport
        Dim m_rtf As RichTextBox
        Dim m_filename As String
        Dim m_formfolder As String
        Dim _reportpath As String
        Dim _settingsfolder As String
        Dim _reportdefpath As String
        Dim _username As String
        Dim srtf As String
        Public CurrentUser As Users.CurrentUser
        Dim myds As DataSet
        Dim mynoticeinfo As HDS.DAL.COMMON.TableView
        Dim myform As HDS.DAL.COMMON.TableView

        Public Sub New(ByVal auser As Users.CurrentUser)
            MyBase.New()
            Me.CurrentUser = auser
            Me.OutputFolder = auser.OutputFolder
            Me.SettingsFolder = auser.SettingsFolder
            Me.UserName = auser.UserName
            Me.ReportDefPath = Me.SettingsFolder & "Reports\PrelimNotice.xml"

        End Sub
        Public Function ExecuteC1Report(ByVal aitemid As String) As String
            Dim mysproc As New uspbo_Jobs_GetNoticeInfo
            mysproc.Id = aitemid
            myds = DBO.GetDataSet(mysproc)
            mynoticeinfo = New HDS.DAL.COMMON.TableView(myds.Tables(0))
            If mynoticeinfo.Count = 1 Then
                Me.ReportName = mynoticeinfo.RowItem("FormCode")
                Me.DataView = mynoticeinfo
                With Me
                    If IsNothing(.FilePrefix) Or Len(.FilePrefix) < 2 Then
                        .FilePrefix += ClientView.Lien.Report.GetReportPrefix(Me.CurrentUser) & "-" & mynoticeinfo.RowItem("JobId") & "-" & Me.ReportName
                    Else
                        .FilePrefix += "-" & mynoticeinfo.RowItem("JobId") & "-" & Me.ReportName
                    End If
                    .TableIndex = 0
                    .RenderReport()
                    .ReportPath = .ReportFileName
                    Return .ReportFileName

                End With
            Else
                Return Nothing
            End If


        End Function
        Public Function ExecuteC1Report(ByVal mynoticeinfo As HDS.DAL.COMMON.TableView, ByVal myfileprefix As String) As String
            If mynoticeinfo.Count = 1 Then
                Me.ReportName = mynoticeinfo.RowItem("FormCode")
                Me.DataView = mynoticeinfo
                With Me
                    .FilePrefix = myfileprefix
                    .TableIndex = 0
                    .RenderReport()
                    .ReportPath = .ReportFileName
                    Return .ReportFileName

                End With
            Else
                Return Nothing
            End If


        End Function
        Public Overrides Sub StartSection(ByVal sender As Object, ByVal e As C1.C1Report.ReportEventArgs)

            mynoticeinfo = Me.DataView
            Dim myclient As New TABLES.Client
            Dim mybranchsigner As New ClientBranchSigner

            Dim C1R As C1.C1Report.C1Report = sender

            If mynoticeinfo.RowItem("UseBranchSignature") = True Then
                Dim mysql As String = "Select Top 1 * from client where parentclientid = " & mynoticeinfo.RowItem("ClientId") & " And BranchNumber = '" & mynoticeinfo.RowItem("BranchNumber") & "'"
                DBO.GetItem(mysql, myclient)
                '
                If myclient.ClientId > 1 Then
                    mysql = "Select Top 1 * from clientbranchsigner where clientid = " & myclient.ClientId
                    DBO.GetItem(mysql, mybranchsigner)
                End If
            End If

            Try
                If HasField("Signature1", e) = True Then
                    Dim ZIMAGE As Field = C1R.Fields("Signature1")

                    If mynoticeinfo.RowItem("UseBranchSignature") = True Then

                        If myclient.ClientId > 0 Then
                            Dim s As String = "C:\TEMP\" & myclient.ClientId & ".jpg"
                            If System.IO.File.Exists(s) = False Then
                                CRF.BLL.PDFLIB.WriteStream(mybranchsigner.Signature, s)
                            End If

                            If System.IO.File.Exists(s) Then
                                Dim myimage As New HDS.WINLIB.Controls.ImageViewer
                                myimage.PictureBox1.Load(s)
                                ZIMAGE.Picture = myimage.PictureBox1.Image()
                            End If
                        Else
                            ZIMAGE.Picture = GetSignature1()
                        End If
                    Else
                        ZIMAGE.Picture = GetSignature1()
                    End If
                End If

                If HasField("BranchContact", e) = True Then
                    Dim ZTEXT As Field = C1R.Fields("BranchContact")
                    If mybranchsigner.Signer = "" Then
                        ZTEXT.Text = "Laura Pavey"
                    Else
                        ZTEXT.Text = mybranchsigner.Signer
                    End If
                End If

            Catch ex As Exception
                Dim s As String = String.Empty

            End Try

            Try
                If HasField("Signature2", e) = True Then
                    Dim ZIMAGE As Field = C1R.Fields("Signature2")

                    If mynoticeinfo.RowItem("UseBranchSignature") = True Then

                        If myclient.ClientId > 0 Then
                            Dim s As String = "C:\TEMP\" & myclient.ClientId & ".jpg"
                            If System.IO.File.Exists(s) = False Then
                                CRF.BLL.PDFLIB.WriteStream(mybranchsigner.Signature, s)
                            End If

                            If System.IO.File.Exists(s) Then
                                Dim myimage As New HDS.WINLIB.Controls.ImageViewer
                                myimage.PictureBox1.Load(s)
                                ZIMAGE.Picture = myimage.PictureBox1.Image()
                            End If
                        Else
                            ZIMAGE.Picture = GetSignature2()
                        End If
                    Else
                        ZIMAGE.Picture = GetSignature2()
                    End If
                End If

                If HasField("BranchContact2", e) = True Then
                    Dim ZTEXT As Field = C1R.Fields("BranchContact2")
                    If mybranchsigner.Signer = "" Then
                        ZTEXT.Text = "Laura Pavey"
                    Else
                        ZTEXT.Text = mybranchsigner.Signer
                    End If
                End If

            Catch ex As Exception
                Dim s As String = String.Empty

            End Try

        End Sub
        Private Function HasField(ByVal aname As String, ByVal e As C1.C1Report.ReportEventArgs) As Boolean
            Try
                Dim Zfield As Field = C1R.Fields(aname)
                If UCase(Zfield.Name) = UCase(aname) And e.Section = Zfield.Section Then
                    Return True
                Else
                    Return False
                End If
            Catch
                Return False
            End Try

        End Function
        Public Function GetSignature1() As Object
            Dim s As String = Me.SettingsFolder & "Reports\scan1.jpg"
            If System.IO.File.Exists(s) Then
                Dim myimage As New HDS.WINLIB.Controls.ImageViewer
                myimage.PictureBox1.Load(s)
                Return myimage.PictureBox1.Image()
            Else
                Return Nothing
            End If


        End Function
        Public Function GetSignature2() As Object
            Dim s As String = Me.SettingsFolder & "Reports\scan3.jpg"
            If System.IO.File.Exists(s) Then
                Dim myimage As New HDS.WINLIB.Controls.ImageViewer
                myimage.PictureBox1.Load(s)
                Return myimage.PictureBox1.Image()
            Else
                Return Nothing
            End If
        End Function

    End Class

End Namespace
