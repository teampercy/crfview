Namespace ClientView.Lien
    Public Class Report
        Public Shared Function NewJobAck(ByVal auser As Users.CurrentUser, ByVal MYview As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode) As String
            Dim myreportname As String = "NewJobAck"

            If MYview.Count < 1 Then
                Return Nothing
            End If

            If printmode = Common.PrintMode.PrintToPDF Then
                Dim myreport As New BLL.Common.StandardReport
                myreport.ReportDefPath = auser.SettingsFolder & "Reports\WebReportsLiens.xml"
                myreport.OutputFolder = auser.OutputFolder
                myreport.UserName = auser.UserName
                myreport.ReportName = "NewJobAck"
                myreport.TableIndex = 1
                myreport.DataView = MYview
                myreport.PrintMode = printmode
                myreport.Sort = ""
                myreport.FilePrefix = GetReportPrefix(auser)

                myreport.RenderReport()
                If myreport.ReportFileName.Length < 1 Then
                    Return Nothing
                Else
                    Return myreport.ReportFileName
                End If
            End If

            If printmode.PrinttoExcel Then
                Dim myspreadsheetpath As String = GetFileName(auser, myreportname)

                MYview.ExportColumns.Add(vwJobsList.ColumnNames.JobId)
                MYview.ExportColumns.Add(vwJobsList.ColumnNames.ClientCode)
                MYview.ExportColumns.Add(vwJobsList.ColumnNames.JobNum)
                MYview.ExportColumns.Add(vwJobsList.ColumnNames.JobName)
                MYview.ExportColumns.Add(vwJobsList.ColumnNames.APNNum)
                MYview.ExportColumns.Add(vwJobsList.ColumnNames.JobAdd1)
                MYview.ExportColumns.Add(vwJobsList.ColumnNames.JobAdd2)
                MYview.ExportColumns.Add(vwJobsList.ColumnNames.JobCity)
                MYview.ExportColumns.Add(vwJobsList.ColumnNames.JobState)
                MYview.ExportColumns.Add(vwJobsList.ColumnNames.JobZip)
                MYview.ExportColumns.Add(vwJobsList.ColumnNames.JobCounty)
                MYview.ExportColumns.Add(vwJobsList.ColumnNames.EstBalance)

                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    Return myspreadsheetpath
                Else
                    Return Nothing
                End If

            End If

            Return Nothing

        End Function
        Public Shared Function GetJobNotes(ByVal auser As Users.CurrentUser, ByVal aitemid As String, ByVal printmode As Common.PrintMode) As String
            Dim mytable As New CRFDB.VIEWS.vwJobNotes
            Dim myquery As New DAL.COMMON.Query(mytable)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, vwJobNotes.ColumnNames.JobId, DAL.COMMON.Conditions._IsEqual, aitemid)
            Dim sql As String = myquery.BuildQuery
            Dim myreport As New BLL.Common.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\WebReportsLiens.xml"
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.ReportName = "JobNotes"
            myreport.TableIndex = 1
            myreport.DataView = DBO.GetTableView(sql)
            If myreport.DataView.Count < 1 Then
                Return Nothing
            End If

            myreport.PrintMode = printmode
            myreport.Sort = ""
            myreport.FilePrefix = GetReportPrefix(auser)
            myreport.RenderReport()
            If myreport.ReportFileName.Length < 1 Then
                Return Nothing
            Else
                Return myreport.ReportFileName
            End If
        End Function

        Public Shared Function OpenItem(ByVal auser As Users.CurrentUser, ByVal MYview As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode) As String
            Dim myreportname As String = "OpenItem"

            If MYview.Count < 1 Then
                Return Nothing
            End If

            If printmode = Common.PrintMode.PrintToPDF Then
                Dim myreport As New BLL.Common.StandardReport
                myreport.ReportDefPath = auser.SettingsFolder & "Reports\WebReportsLiens.xml"
                myreport.OutputFolder = auser.OutputFolder
                myreport.UserName = auser.UserName
                myreport.ReportName = "OpenItem"
                myreport.TableIndex = 1
                myreport.DataView = MYview
                myreport.PrintMode = printmode
                myreport.Sort = ""
                myreport.FilePrefix = GetReportPrefix(auser)
                myreport.RenderReport()
                If myreport.ReportFileName.Length < 1 Then
                    Return Nothing
                Else
                    Return myreport.ReportFileName
                End If
            End If

            If printmode.PrinttoExcel Then
                Dim myspreadsheetpath As String = GetFileName(auser, myreportname)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.ClientCode)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobId)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobNum)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobName)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobAdd1)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobCity)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobState)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobZip)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobCounty)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.APNNum)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.EstBalance)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.ClientCustomer)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.CustRefNum)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.GCName)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.OwnerName)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.BranchNum)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.StatusCode)

                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    Return myspreadsheetpath
                Else
                    Return Nothing
                End If

            End If

            Return Nothing

        End Function
        Public Shared Function TexasNoticeSent(ByVal auser As Users.CurrentUser, ByVal MYview As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode) As String
            Dim myreportname As String = "TexasReport"

            If MYview.Count < 1 Then
                Return Nothing
            End If

            If printmode = Common.PrintMode.PrintToPDF Then
                Dim myreport As New BLL.Common.StandardReport
                myreport.ReportDefPath = auser.SettingsFolder & "Reports\WebReportsLiens.xml"
                myreport.OutputFolder = auser.OutputFolder
                myreport.UserName = auser.UserName
                myreport.ReportName = "TexasReport"
                myreport.TableIndex = 1
                myreport.DataView = MYview
                myreport.PrintMode = printmode
                myreport.Sort = ""
                myreport.RenderReport()
                myreport.FilePrefix = GetReportPrefix(auser)

                If myreport.ReportFileName.Length < 1 Then
                    Return Nothing
                Else
                    Return myreport.ReportFileName
                End If
            End If

            If printmode.PrinttoExcel Then
                Dim myspreadsheetpath As String = GetFileName(auser, myreportname)
                MYview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.ClientCode)
                MYview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.BranchNumber)
                MYview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.JobId)
                MYview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.JobNum)
                MYview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.JobName)
                MYview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.JobState)
                MYview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.APNNum)
                MYview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.EstBalance)
                MYview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.CustName)
                MYview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.CustRefNum)
                MYview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.PublicJob)
                MYview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.DateCreated)
                MYview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.IsTX60Day)
                MYview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.IsTX90Day)
                MYview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.AmountOwed)
                MYview.ExportColumns.Add(vwJobTexasRequests.ColumnNames.MonthDebtIncurred)

                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    Return myspreadsheetpath
                Else
                    Return Nothing
                End If

            End If

            Return Nothing

        End Function
        Public Shared Function NoticeSent(ByVal auser As Users.CurrentUser, ByVal MYview As DAL.COMMON.TableView, ByVal asubtitle1 As String, ByVal printmode As Common.PrintMode) As String
            Dim myreportname As String = "NoticeSent"
            If MYview.Count < 1 Then
                Return Nothing
            End If

            If printmode = Common.PrintMode.PrintToPDF Then
                Dim myreport As New BLL.Common.StandardReport
                myreport.ReportDefPath = auser.SettingsFolder & "Reports\WebReportsLiens.xml"
                myreport.OutputFolder = auser.OutputFolder
                myreport.UserName = auser.UserName
                myreport.ReportName = "NoticeSent"
                myreport.TableIndex = 1
                myreport.DataView = MYview
                myreport.PrintMode = printmode
                myreport.Sort = ""
                myreport.SubTitle1 = asubtitle1
                myreport.FilePrefix = GetReportPrefix(auser)

                myreport.RenderReport()
                If myreport.ReportFileName.Length < 1 Then
                    Return Nothing
                Else
                    Return myreport.ReportFileName
                End If
            End If

            If printmode.PrinttoExcel Then
                Dim myspreadsheetpath As String = GetFileName(auser, myreportname)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.ClientCode)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobId)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobNum)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobName)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobAdd1)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobCity)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobState)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobZip)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobCounty)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.APNNum)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.EstBalance)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.ClientCustomer)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.CustRefNum)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.GCName)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.OwnerName)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.BranchNum)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.StatusCode)

                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    Return myspreadsheetpath
                Else
                    Return Nothing
                End If

            End If

            Return Nothing

        End Function
        Public Shared Function JobsPlaced(ByVal auser As Users.CurrentUser, ByVal MYview As DAL.COMMON.TableView, ByVal asubtitle1 As String, ByVal printmode As Common.PrintMode) As String
            Dim myreportname As String = "JobsPlaced"

            If MYview.Count < 1 Then
                Return Nothing
            End If

            If printmode = Common.PrintMode.PrintToPDF Then
                Dim myreport As New BLL.Common.StandardReport
                myreport.ReportDefPath = auser.SettingsFolder & "Reports\WebReportsLiens.xml"
                myreport.OutputFolder = auser.OutputFolder
                myreport.UserName = auser.UserName
                myreport.ReportName = "JobsPlaced"
                myreport.TableIndex = 1
                myreport.DataView = MYview
                myreport.PrintMode = printmode
                myreport.Sort = ""
                myreport.SubTitle1 = asubtitle1
                myreport.FilePrefix = GetReportPrefix(auser)

                myreport.RenderReport()
                If myreport.ReportFileName.Length < 1 Then
                    Return Nothing
                Else
                    Return myreport.ReportFileName
                End If
            End If

            If printmode.PrinttoExcel Then
                Dim myspreadsheetpath As String = GetFileName(auser, myreportname)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.ClientCode)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobId)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobNum)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobName)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobAdd1)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobCity)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobState)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobZip)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobCounty)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.APNNum)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.EstBalance)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.ClientCustomer)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.CustRefNum)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.GCName)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.OwnerName)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.BranchNum)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.DateAssigned)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.StatusCode)

                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    Return myspreadsheetpath
                Else
                    Return Nothing
                End If

            End If

            Return Nothing

        End Function
        Public Shared Function WebPlacements(ByVal auser As Users.CurrentUser, ByVal MYview As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode) As String
            Dim myreportname As String = "JobsPlaced"

            If MYview.Count < 1 Then
                Return Nothing
            End If

            If printmode = Common.PrintMode.PrintToPDF Then
                Dim myreport As New BLL.Common.StandardReport
                myreport.ReportDefPath = auser.SettingsFolder & "Reports\WebReportsLiens.xml"
                myreport.OutputFolder = auser.OutputFolder
                myreport.UserName = auser.UserName
                myreport.ReportName = "JobsPlaced"
                myreport.TableIndex = 1
                myreport.DataView = MYview
                myreport.PrintMode = printmode
                myreport.Sort = ""
                myreport.FilePrefix = GetReportPrefix(auser)

                myreport.RenderReport()
                If myreport.ReportFileName.Length < 1 Then
                    Return Nothing
                Else
                    Return myreport.ReportFileName
                End If
            End If

            If printmode.PrinttoExcel Then
                Dim myspreadsheetpath As String = GetFileName(auser, myreportname)
                MYview.ExportColumns.Add(vwBatchJob.ColumnNames.ClientCode)
                MYview.ExportColumns.Add(vwBatchJob.ColumnNames.JobNum)
                MYview.ExportColumns.Add(vwBatchJob.ColumnNames.JobName)
                MYview.ExportColumns.Add(vwBatchJob.ColumnNames.APNNum)
                MYview.ExportColumns.Add(vwBatchJob.ColumnNames.JobAdd1)
                MYview.ExportColumns.Add(vwBatchJob.ColumnNames.JobAdd2)
                MYview.ExportColumns.Add(vwBatchJob.ColumnNames.JobCity)
                MYview.ExportColumns.Add(vwBatchJob.ColumnNames.JobState)
                MYview.ExportColumns.Add(vwBatchJob.ColumnNames.JobZip)
                MYview.ExportColumns.Add(vwBatchJob.ColumnNames.JobCounty)
                MYview.ExportColumns.Add(vwBatchJob.ColumnNames.EstBalance)

                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    Return myspreadsheetpath
                Else
                    Return Nothing
                End If

            End If

            Return Nothing

        End Function
        Public Shared Function GetReportPrefix(ByVal auser As Users.CurrentUser) As String
            Dim myuser As New ClientView.ClientUserInfo(auser.Id)
            Dim myprefix As String = myuser.Client.ClientCode
            myprefix += "-" & myuser.UserInfo.LoginCode
            myprefix += "-" & myuser.UserInfo.ID
            Return myprefix
        End Function
        Public Shared Function GetFileName(ByVal auser As Users.CurrentUser, ByVal areportname As String) As String
            Dim myuser As New ClientView.ClientUserInfo(auser.Id)
            Dim myprefix As String = myuser.Client.ClientCode
            myprefix += "-" & myuser.UserInfo.LoginCode
            myprefix += "-" & myuser.UserInfo.ID
            myprefix += "-" & areportname
            If System.IO.Directory.Exists(auser.OutputFolder) = False Then
                System.IO.Directory.CreateDirectory(auser.OutputFolder)
            End If
            Return auser.OutputFolder & Common.FileOps.TrimSpace(myprefix & "-" & Format(Now(), "yyyyMMddhhmmss") & ".CSV")
        End Function
       
    End Class
End Namespace
