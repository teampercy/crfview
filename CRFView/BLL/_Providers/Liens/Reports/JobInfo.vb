Imports CRF.BLL
Imports System.Data
Imports CRF.BLL.Common
Namespace Liens
    Public Class JobInfo
        Public ObjectState As New CRF.BLL.ObjectState
        Dim SprocParams As New CRFDB.SPROCS.uspbo_Jobs_GetJobInfo
        Dim myds As DataSet
        Dim CurrentUser As Users.CurrentUser
        Dim _job As DAL.COMMON.TableView
        Dim _notes As DAL.COMMON.TableView
        Dim _noticehistory As DAL.COMMON.TableView
        Dim _legalparties As DAL.COMMON.TableView
        Dim _customer As DAL.COMMON.TableView
        Dim _generalcontract As DAL.COMMON.TableView
        Dim _client As DAL.COMMON.TableView
        Dim _clientlieninfo As DAL.COMMON.TableView
        Dim _waiverhistory As DAL.COMMON.TableView
        Dim _jobalerts As DAL.COMMON.TableView
        Dim _othernotices As DAL.COMMON.TableView
        Dim _texasrequests As DAL.COMMON.TableView
        Public Sub New(ByVal auser As CRF.BLL.Users.CurrentUser, ByVal keyvalue As String)
            MyBase.New()
            Me.CurrentUser = auser
            SprocParams.JobId = keyvalue
            myds = DBO.GetDataSet(SprocParams)
            _job = New DAL.COMMON.TableView(myds.Tables(0))
            _notes = New DAL.COMMON.TableView(myds.Tables(1))
            _noticehistory = New DAL.COMMON.TableView(myds.Tables(2))
            _legalparties = New DAL.COMMON.TableView(myds.Tables(3))
            _customer = New DAL.COMMON.TableView(myds.Tables(4))
            _generalcontract = New DAL.COMMON.TableView(myds.Tables(5))
            _client = New DAL.COMMON.TableView(myds.Tables(6))
            _clientlieninfo = New DAL.COMMON.TableView(myds.Tables(7))
            _waiverhistory = New DAL.COMMON.TableView(myds.Tables(8))
            _jobalerts = New DAL.COMMON.TableView(myds.Tables(9))
            _texasrequests = New DAL.COMMON.TableView(myds.Tables(10))
            _othernotices = New DAL.COMMON.TableView(myds.Tables(11))

        End Sub
        Public Function Job() As DAL.COMMON.TableView
            Return _job
        End Function
        Public Function Notes() As DAL.COMMON.TableView
            Return _notes
        End Function
        Public Function NoticeHistory() As DAL.COMMON.TableView
            Return _noticehistory
        End Function
        Public Function LegalParties() As DAL.COMMON.TableView
            Return _legalparties
        End Function
        Public Function Customer() As DAL.COMMON.TableView
            Return _customer
        End Function
        Public Function GeneralContractor() As DAL.COMMON.TableView
            Return _generalcontract
        End Function
        Public Function Client() As DAL.COMMON.TableView
            Return _client
        End Function
        Public Function ClientLienInfo() As DAL.COMMON.TableView
            Return _clientlieninfo
        End Function
        Public Function WaiverHistory() As DAL.COMMON.TableView
            Return _waiverhistory
        End Function
        Public Function JobAlerts() As DAL.COMMON.TableView
            Return _jobalerts
        End Function
        Public Function JobTexasRequests() As DAL.COMMON.TableView
            Return _texasrequests
        End Function
        Public Function OtherNoticeLog() As DAL.COMMON.TableView
            Return _othernotices
        End Function
     
    End Class
End Namespace
