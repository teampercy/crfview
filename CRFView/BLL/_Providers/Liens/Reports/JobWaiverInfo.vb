Namespace ClientView.Lien
    Public Class JobWaiverInfo
        Dim myvw0 As DAL.COMMON.TableView
        Dim myvw1 As DAL.COMMON.TableView
        Dim myvw2 As DAL.COMMON.TableView
        Dim myvw3 As DAL.COMMON.TableView
        Dim myvw4 As DAL.COMMON.TableView
        Dim myvw5 As DAL.COMMON.TableView
        Dim myvw6 As DAL.COMMON.TableView
        Dim myvw7 As DAL.COMMON.TableView
        Dim myvw8 As DAL.COMMON.TableView
        Dim myvw9 As DAL.COMMON.TableView
        Dim myvw10 As DAL.COMMON.TableView

        Dim mytable0 As New CRF.BLL.CRFDB.VIEWS.vwJobInfo
        Dim mytable1 As New CRF.BLL.CRFDB.TABLES.JobWaiverLog
        Dim mytable2 As New CRF.BLL.CRFDB.TABLES.ClientCustomer
        Dim mytable3 As New CRF.BLL.CRFDB.TABLES.ClientGeneralContractor
        Dim mytable4 As New CRF.BLL.CRFDB.TABLES.JobLegalParties
        Dim mytable5 As New CRF.BLL.CRFDB.TABLES.JobLegalParties
        Dim mytable6 As New CRF.BLL.CRFDB.TABLES.Client
        Dim mytable7 As New CRF.BLL.CRFDB.TABLES.ClientLienInfo
        Dim mytable8 As New CRF.BLL.CRFDB.TABLES.ClientImage
        Dim mytable9 As New CRF.BLL.CRFDB.TABLES.StateForms
        Dim mytable10 As New CRF.BLL.CRFDB.TABLES.ClientSigner

        Dim MYDS As DataSet
        Public Sub New(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer)
            Dim mysproc As New SPROCS.uspbo_ClientView_GetWaiverInfo
            mysproc.JobWaiverLogId = aitemid
            myds = DBO.GetDataSet(mysproc)
            myvw0 = New DAL.COMMON.TableView(MYDS.Tables(0), "")
            myvw1 = New DAL.COMMON.TableView(MYDS.Tables(1), "")
            myvw2 = New DAL.COMMON.TableView(MYDS.Tables(2), "")
            myvw3 = New DAL.COMMON.TableView(MYDS.Tables(3), "")
            myvw4 = New DAL.COMMON.TableView(MYDS.Tables(4), "")
            myvw5 = New DAL.COMMON.TableView(MYDS.Tables(5), "")
            myvw6 = New DAL.COMMON.TableView(MYDS.Tables(6), "")
            myvw7 = New DAL.COMMON.TableView(MYDS.Tables(7), "")
            myvw8 = New DAL.COMMON.TableView(MYDS.Tables(8), "")
            myvw9 = New DAL.COMMON.TableView(MYDS.Tables(9), "")
            myvw10 = New DAL.COMMON.TableView(MYDS.Tables(10), "")
            
            myvw1.FillEntity(mytable1)
            myvw2.FillEntity(mytable2)
            myvw3.FillEntity(mytable3)
            myvw4.FillEntity(mytable4)
            myvw5.FillEntity(mytable5)
            myvw6.FillEntity(mytable6)
            myvw7.FillEntity(mytable7)
            myvw8.FillEntity(mytable8)
            myvw9.FillEntity(mytable9)
            myvw0.FillEntity(mytable0)
            myvw10.FillEntity(mytable10)

        End Sub
        Public Function vwJobInfo() As VIEWS.vwJobInfo
            Return mytable0
        End Function
        Public Function JobWaiverLog() As TABLES.JobWaiverLog
            Return mytable1
        End Function
        Public Function ClientCustomer() As ClientCustomer
            Return mytable2
        End Function
        Public Function GeneralContractor() As TABLES.ClientGeneralContractor
            Return mytable3
        End Function
        Public Function JobLegalParties_Owner() As TABLES.JobLegalParties
            Return mytable4
        End Function
        Public Function JobLegalParties_Lender() As TABLES.JobLegalParties
            Return mytable5
        End Function
        Public Function Client() As Client
            Return mytable6
        End Function
        Public Function ClientLienInfo() As ClientLienInfo
            Return mytable7
        End Function
        Public Function ClientImage() As TABLES.ClientImage
            Return mytable8
        End Function
        Public Function StateForm() As TABLES.StateForms
            Return mytable9
        End Function
        Public Function ClientSigner() As TABLES.ClientSigner
            Return mytable10
        End Function
    End Class
End Namespace
