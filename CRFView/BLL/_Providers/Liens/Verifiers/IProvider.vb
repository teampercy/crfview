Namespace Providers
    Public Interface ILiensVerifiersProvider
        Function GetJobList(ByVal auser As Users.CurrentUser, ByVal asproc As CRFDB.SPROCS.uspbo_Jobs_GetJobList) As DAL.COMMON.TableView
        Function GetJobInfo(ByVal auser As Users.CurrentUser, ByVal ajobid As String) As DataSet
        Function GetLienClients(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
        Function GetLienUsers(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
        Function GetReportList(ByVal auser As Users.CurrentUser, ByVal areportcategory As String) As DAL.COMMON.TableView
        Function GetStatusList(ByVal auser As Users.CurrentUser, ByVal bystatusgroup As Boolean, ByVal astatusgroupid As Integer) As DAL.COMMON.TableView
        Function GetCountyRecorderByNum(ByVal auser As Users.CurrentUser, ByVal arecordernum As String) As TABLES.CountyRecorder
        Function GetWebNotes(ByVal auser As Users.CurrentUser, ByVal asproc As CRFDB.SPROCS.uspbo_Jobs_GetWebNoteList) As DAL.COMMON.TableView
        Function GetOtherNoticeForms(ByVal auser As Users.CurrentUser, ByVal astateinitials As String) As DAL.COMMON.TableView
        Function GetWaiverFormsForState(ByVal auser As Users.CurrentUser, ByVal astateinitials As String) As DAL.COMMON.TableView
        Function PrintLienReport(ByVal auser As Users.CurrentUser, ByVal aprintmode As Common.PrintMode, ByVal aview As DAL.COMMON.TableView, ByVal areportfilename As String, ByVal areportname As String) As String
        Function UpdateJobStatus(ByVal auser As Users.CurrentUser, ByVal aentity As TABLES.Job, ByVal asproc As SPROCS.uspbo_Jobs_UpdateJobStatus) As Boolean
        Function PrintJobWaiver(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String
        Function PrintPrelimNotice(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String
        Function PrintOtherNotice(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String

    End Interface
End Namespace
