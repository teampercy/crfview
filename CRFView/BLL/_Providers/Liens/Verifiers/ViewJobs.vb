Namespace Liens
    Public Class ViewJobs
        Public Shared Function GetJobList(ByVal auser As Users.CurrentUser) As DAL.Common.TableView
            Dim sql As String = "Select ClientId As ClientId,  ClientCode, ClientCode + '-' + ClientName As ClientName from Client Where IsCollectionClient = 1 and IsBranch = 0"
            Return DBO.GetTableView(sql)
        End Function
        Public Shared Function GetJobInfo(ByVal auser As Users.CurrentUser, ByVal aitemId As Integer) As Liens.JobInfo
            Return New Liens.JobInfo(auser, aitemId)
        End Function
        Public Shared Function GetCountyRecorderByNum(ByVal auser As Users.CurrentUser, ByVal arecordernum As String) As TABLES.CountyRecorder
            Dim myview As New TABLES.CountyRecorder
            Dim myquery As New HDS.DAL.COMMON.Query(myview)
            Dim mysql As String
            myquery.AddCondition(HDS.DAL.COMMON.LogicalOperators._And, CRFDB.TABLES.CountyRecorder.ColumnNames.CountyRecorderNum, HDS.DAL.COMMON.Conditions._IsEqual, arecordernum)
            mysql = myquery.BuildQuery
            Dim myvw As DAL.COMMON.TableView = DBO.GetTableView(myquery.BuildQuery)
            If myvw.Count > 0 Then
                myvw.FillEntity(myview)
            End If
           
            Return myview

        End Function
        Public Shared Function UpdateJobStatus(ByVal auser As Users.CurrentUser, ByVal aentity As TABLES.Job, ByVal asproc As SPROCS.uspbo_Jobs_UpdateJobStatus) As Object
            Return DBO.ExecScalar(asproc)
        End Function

    End Class
End Namespace
