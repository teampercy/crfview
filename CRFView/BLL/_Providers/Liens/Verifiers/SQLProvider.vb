Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Web.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Xml
Imports System.Xml.Schema
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections.Specialized
Imports System.Configuration.Provider
Imports System.Web.Hosting
Imports System.Web.Management
Imports System.Security.Permissions
Imports System.Text
Imports System.Security.Cryptography
Namespace Providers
    Friend Class SQLLiensVerifiersProvider
        Inherits ProviderBase
        Implements ILiensVerifiersProvider

        Public Function GetJobInfo(ByVal auser As Users.CurrentUser, ByVal ajobid As String) As DataSet Implements ILiensVerifiersProvider.GetJobInfo
            Dim mysproc As New CRF.BLL.CRFDB.SPROCS.uspbo_Jobs_GetJobInfo
            mysproc.JobId = ajobid
            Return DBO.Provider.GetDataSet(mysproc)
        End Function
        Public Function GetJobList(ByVal auser As Users.CurrentUser, ByVal asproc As CRF.BLL.CRFDB.SPROCS.uspbo_Jobs_GetJobList) As HDS.DAL.COMMON.TableView Implements ILiensVerifiersProvider.GetJobList
            Return DBO.Provider.GetTableView(asproc)
        End Function
        Public Function UpdateJobStatus(ByVal auser As Users.CurrentUser, ByVal aentity As CRFDB.TABLES.Job, ByVal asproc As CRFDB.SPROCS.uspbo_Jobs_UpdateJobStatus) As Boolean Implements ILiensVerifiersProvider.UpdateJobStatus
            Return DBO.Provider.ExecScalar(asproc)
        End Function
        Public Function GetLienClients(ByVal auser As Users.CurrentUser) As HDS.DAL.COMMON.TableView Implements ILiensVerifiersProvider.GetLienClients
            Return Queries.GetLienClients
        End Function
        Public Function GetLienUsers(ByVal auser As Users.CurrentUser) As HDS.DAL.COMMON.TableView Implements ILiensVerifiersProvider.GetLienUsers
            Return Queries.GetLienUsers
        End Function
        Public Function GetWebNotes(ByVal auser As Users.CurrentUser, ByVal asproc As CRFDB.SPROCS.uspbo_Jobs_GetWebNoteList) As HDS.DAL.COMMON.TableView Implements ILiensVerifiersProvider.GetWebNotes
            Return DBO.Provider.GetTableView(asproc)
        End Function
        Public Function GetCountyRecorderByNum(ByVal auser As Users.CurrentUser, ByVal arecordernum As String) As CRFDB.TABLES.CountyRecorder Implements ILiensVerifiersProvider.GetCountyRecorderByNum
            Dim myview As New TABLES.CountyRecorder
            Dim myquery As New HDS.DAL.COMMON.Query(myview)
            Dim mysql As String
            myquery.AddCondition(HDS.DAL.COMMON.LogicalOperators._And, CRFDB.TABLES.CountyRecorder.ColumnNames.CountyRecorderNum, HDS.DAL.COMMON.Conditions._IsEqual, arecordernum)
            mysql = myquery.BuildQuery
            Dim myvw As DAL.COMMON.TableView = DBO.Provider.GetTableView(myquery.BuildQuery)
            If myvw.Count > 0 Then
                myvw.FillEntity(myview)
            End If
            Return myview
        End Function
        Public Function GetReportList(ByVal auser As Users.CurrentUser, ByVal areportcategory As String) As HDS.DAL.COMMON.TableView Implements ILiensVerifiersProvider.GetReportList
            Dim sql As String = "Select ReportName, Description,  PrintFrom, ReportCategory, ReportFilename, Reporttype from vwLienReportsList Where ReportCategory = '" & areportcategory.ToString & "' And PrintFrom in ('B', 'R') Order By Description"
            Return DBO.Provider.GetTableView(sql)
        End Function
        Public Function GetStatusList(ByVal auser As Users.CurrentUser, ByVal bystatusgroup As Boolean, ByVal astatusgroupid As Integer) As HDS.DAL.COMMON.TableView Implements ILiensVerifiersProvider.GetStatusList
            Dim sql As String
            If bystatusgroup = True Then
                sql = "Select JobStatus, JobStatusDescr, StatusGroup from JobStatus"
            Else
                sql = "Select JobStatus, JobStatusDescr, StatusGroup from JobStatus Where StatusGroup = " & astatusgroupid.ToString
            End If
            Return DBO.Provider.GetTableView(sql)

        End Function
        Public Function PrintLienReport(ByVal auser As Users.CurrentUser, ByVal aprintmode As Common.PrintMode, ByVal aview As HDS.DAL.COMMON.TableView, ByVal areportfilename As String, ByVal areportname As String) As String Implements ILiensVerifiersProvider.PrintLienReport
            Dim myreport As New BLL.Common.StandardReport

            myreport.ReportDefPath = auser.SettingsFolder & "Reports\" & areportfilename
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.ReportName = areportname
            myreport.TableIndex = 1
            myreport.DataView = aview
            myreport.PrintMode = aprintmode
            myreport.Sort = ""
            myreport.RenderReport()
            Return myreport.ReportFileName

        End Function
        Public Function GetOtherNoticeForms(ByVal auser As Users.CurrentUser, ByVal astateinitials As String) As HDS.DAL.COMMON.TableView Implements ILiensVerifiersProvider.GetOtherNoticeForms

        End Function

        Public Function GetWaiverFormsForState(ByVal auser As Users.CurrentUser, ByVal astateinitials As String) As HDS.DAL.COMMON.TableView Implements ILiensVerifiersProvider.GetWaiverFormsForState

        End Function

        Public Function PrintJobWaiver(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String Implements ILiensVerifiersProvider.PrintJobWaiver

        End Function
        Public Function PrintPrelimNotice(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String Implements ILiensVerifiersProvider.PrintPrelimNotice

        End Function
        Public Function PrintOtherNotice(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String Implements ILiensVerifiersProvider.PrintOtherNotice
            Dim mysproc As New SPROCS.uspbo_Jobs_GetOtherNoticeInternalInfo
            mysproc.Id = aitemid
            Dim myreport As New Liens.Reports.JobOtherNotice(auser, "Reports\LienNotices.xml")
            myreport.ExecuteC1Report(BLL.DBO.GetDataSet(mysproc))
            Return myreport.ReportPath

        End Function
    End Class

End Namespace
