Imports Microsoft.VisualBasic

Imports System
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Web.Configuration
Namespace Providers
    Public Class LiensVerifiers
        Private Shared _isInitialized As Boolean = False
        Private Shared _provider As ILiensVerifiersProvider
        ' <summary>
        ' Returns provider
        ' </summary>
        Public Shared ReadOnly Property Provider() As ILiensVerifiersProvider
            Get
                Initialize()
                Return _provider

            End Get
        End Property
        Private Shared Sub Initialize()
            If Not _isInitialized Then
                _provider = SingletonProvider(Of Providers.SQLLiensVerifiersProvider).Instance
                _isInitialized = True
            End If
        End Sub
        Public Shared Function GetJobList(ByVal auser As Users.CurrentUser, ByVal asproc As CRFDB.SPROCS.uspbo_Jobs_GetJobList) As DAL.COMMON.TableView
            Return Provider.GetJobList(auser, asproc)
        End Function
        Public Shared Function GetJobInfo(ByVal auser As Users.CurrentUser, ByVal ajobid As String) As DataSet
            Return Provider.GetJobInfo(auser, ajobid)
        End Function
        Public Shared Function GetLienClients(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Return Provider.GetLienClients(auser)
        End Function
        Public Shared Function GetLienUsers(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Return Provider.GetLienUsers(auser)
        End Function
        Public Shared Function GetWebNotes(ByVal auser As Users.CurrentUser, ByVal asproc As CRFDB.SPROCS.uspbo_Jobs_GetWebNoteList) As DAL.COMMON.TableView
            Return Provider.GetWebNotes(auser, asproc)
        End Function
        Public Shared Function GetReportList(ByVal auser As Users.CurrentUser, ByVal areportcategory As String) As DAL.COMMON.TableView
            Return Provider.GetReportList(auser, areportcategory)
        End Function
        Public Shared Function GetStatusList(ByVal auser As Users.CurrentUser, ByVal bystatusgroup As Boolean, ByVal astatusgroupid As Integer) As DAL.COMMON.TableView
            Return Provider.GetStatusList(auser, bystatusgroup, astatusgroupid)
        End Function
        Public Shared Function GetCountyRecorderByNum(ByVal auser As Users.CurrentUser, ByVal arecordernum As String) As TABLES.CountyRecorder
            Return Provider.GetCountyRecorderByNum(auser, arecordernum)
        End Function
        Public Shared Function GetOtherNoticesForState(ByVal auser As Users.CurrentUser, ByVal astateinitials As String) As DAL.COMMON.TableView
            Return Provider.GetOtherNoticeForms(auser, astateinitials)
        End Function
        Public Shared Function GetWaiverFormsForState(ByVal auser As Users.CurrentUser, ByVal astateinitials As String) As DAL.COMMON.TableView
            Return Provider.GetWaiverFormsForState(auser, astateinitials)
        End Function
        Public Shared Function PrintLienReport(ByVal auser As Users.CurrentUser, ByVal aprintmode As Common.PrintMode, ByVal aview As DAL.COMMON.TableView, ByVal areportfilename As String, ByVal areportname As String) As String
            Return Provider.PrintLienReport(auser, aprintmode, aview, areportfilename, areportname)
        End Function
        Public Shared Function UpdateJobStatus(ByVal auser As Users.CurrentUser, ByVal aentity As TABLES.Job, ByVal asproc As SPROCS.uspbo_Jobs_UpdateJobStatus) As Boolean
            Return Provider.UpdateJobStatus(auser, aentity, asproc)
        End Function
        Public Shared Function PrintJobWaiver(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String
            Return Provider.PrintJobWaiver(auser, aitemid)
        End Function
        Public Shared Function PrintPrelimNotice(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String
            Return Provider.PrintPrelimNotice(auser, aitemid)
        End Function
        Public Shared Function PrintOtherNotice(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String
            Return Provider.PrintOtherNotice(auser, aitemid)
        End Function

    End Class
End Namespace
