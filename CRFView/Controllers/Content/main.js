﻿//Function for datatable 
function makeDatatable(tblId, options1) {
    var options = {
        "pageLength": 10,
        "stateSave": true,
        "stateDuration": 180,
        "processing": true,        
        "columnDefs": [{
            "targets": 'no-sort',
            "orderable": false,
        }],
        "order": [[0, "asc"]],
        "fixedHeader": true,
        "searching": false,
        "lengthMenu": [10, 20, 30, 40, 50, 100],
        "language": {
            "paginate": {
                next: '<i class="icon wb-chevron-right-mini">',
                previous: '<i class="icon wb-chevron-left-mini">'
            }
        },
        responsive: true,
        "dom": "<'row'<'col-sm-12 col-xs-12'tr>>" +
              "<'row'<'col-sm-5 col-xs-5'il><'col-sm-7 col-xs-5'p>>"
    };
    if (tblId == "tblBillableEventType")
    {
        options1 = {
            "pageLength": 20,
            "order": [[1, "asc"]]
        };
    }
    if (tblId == "tblCollectionRateItems" || tblId=="tblCollectionRates") {
        options1 = {
            "pageLength": 100,
            "order": [[0, "asc"]]
        };
    }
    if (tblId == "tblLedgerTypes" || tblId == "tblEventPricing" || tblId=="tblNoticeRates")
    {
        options1 = {
            "order": [[0, "asc"]]
        };
    }

    //if (tblId == "tblLedger" || tblId == "tblCreditReport")
    //{
    //    options1 = {
    //        "scrollX": true
    //    };
    //}
    
    var optionExtra = $.extend({}, options, options1);

    $("#" + tblId).DataTable(optionExtra);
}

//clear state of datatable
function ClearDatatableState(tblId)
{
    var table =  $("#"+tblId).DataTable();
    table.state.clear();
    table.destroy();
}

//Convert timestamp to date
function convertDate(data) {
    var ts = data.substring(6, 19);
    ts = Number(ts);
    var date = new Date(ts);
    var month = date.getMonth() + 1;
    return (month.toString().length > 1 ? month : "0" + month) + "/" + date.getDate() + "/" + date.getFullYear();
}

//open selected menu
function openSelectedMenu(menuid) {
    debugger;
    $('li:has(a[id="'+menuid+'"])').addClass('active');
    $('li:has(ul:has(li:has(a[id="'+menuid+'"])))').addClass('active open');
}

//Clear Search Textbox
function ClearSearch()
{
    $('#txtSearch').val('');
    Search();
}
//loading image show/hide
function showLoadingImage() {
    $("#loadingImageDiv").show();
}
function hideLoadingImage() {
    $("#loadingImageDiv").hide();
}

//check isnumber
function isNumber(evt) {
    evt = (evt) ? evt : window.event;
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode > 31 && (charCode < 48 || charCode > 57) && charCode!=46) {
        return false;
    }
    return true;
}

//validate and format phone number
function validatePhone(phoneno)
{
    debugger;
    var phone = phoneno.replace(/[^0-9]/g, '');
   
    if (phone.length > 0 && phone.length != 10) {
        return false;
    }
    else
    {
            return true; 
    }
}

function formatPhone(phonefield)
{
    var phone = phonefield.value.replace(/[^0-9]/g, '');
    $(phonefield).val(phone.replace(/^(\d{3})(\d{3})(\d{4})+$/, "$1-$2-$3"));
}

function formatFax(faxfield) {
    var fax = faxfield.value.replace(/[^0-9]/g, '');
    $(faxfield).val(fax.replace(/^(\d{3})(\d{3})(\d{4})+$/, "$1-$2-$3"));
}

function formatCurrency(currencyfield) {
    debugger;
    var currency = currencyfield.value;
    currency = currency.replace("$", "");
    currency = currency.replace(",", "");
    const formatter = new Intl.NumberFormat('en-US', {
        style: 'currency',
        currency: 'USD',
    minimumFractionDigits: 2,
    maximumFractionDigits: 2
    })
    $(currencyfield).val(formatter.format(currency));
}

function formatPercentage(percentagefield) {
    debugger;
    var percentage = percentagefield.value;
    percentage = percentage.replace("%", "");
    percentage = percentage.concat("%")
    $(percentagefield).val(percentage);
}

function formatSSN(ssnfield) {
    var ssn = ssnfield.value.replace(/[^0-9]/g, '');
    $(ssnfield).val(ssn.replace(/^(\d{3})(\d{2})(\d{4})+$/, "$1-$2-$3"));
}

//Read Image
function readImage(input, imageid, hiddenid) {
    //Read image and set the base64 value in hidden field.
    debugger;
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#' + imageid).attr('src', e.target.result);
            $('#' + hiddenid).val(e.target.result.replace(/^data:image\/(bitmap|jpeg|gif);base64,/, ""));
        }
        reader.readAsDataURL(input.files[0]);
    }
}

// Convert uploaded file to base64
function readFile(input, imageid, hiddenid) {
    //Read file and set the base64 value in hidden field.
    debugger;
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#' + imageid).attr('src', e.target.result);
            $('#' + hiddenid).val(e.target.result.replace(/^data:image\/(bitmap|jpeg|gif);base64,/, ""));
        }
        reader.readAsDataURL(input.files[0]);
    }
}


function readFile(input, imageid, hiddenid) {
    //Read image and set the base64 value in hidden field.
    debugger;
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            $('#' + imageid).attr('src', e.target.result);
            $('#' + hiddenid).val(e.target.result.replace(/^data:image\/(bitmap|jpeg|gif);base64,/, ""));
        }
        reader.readAsDataURL(input.files[0]);
    }
}



//convert Json date to date
function ConvertJsonToDate(date)
{
    var dateString = date.substr(6);
    var currentTime = new Date(parseInt(dateString ));
    var month = currentTime.getMonth() + 1;
    var day = currentTime.getDate();
    var year = currentTime.getFullYear();
    var formatedate = month + "/" + day + "/" + year;
    return formatedate;
}