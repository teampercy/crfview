﻿using System;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Configuration;
using CRFView.Adapters;

using CRFView.Controllers.Collections.Collectors.WorkCard;

namespace CRFView.Controllers
{
    public class LoginController : Controller
    {
        // GET: /<login>/
        public ActionResult login()
        {
            SetDefaultConnString();
            GetAssemblyinfo();
            return View();
        }
        //post Login
        [HttpPost]
        public ActionResult login(Credentials updatedCredentials)
        {
            Credentials objCredentials = new Credentials();
            CurrentUser objCurrentUser = new CurrentUser();
            GetAssemblyinfo();
           
            if (ModelState.IsValid)
            {
                objCredentials = updatedCredentials;
                if (objCredentials.GetLoginDetails())
                {
                    ADInternalUser objUserLogin = (ADInternalUser)Session["UserLogin"];
                    objCurrentUser.Id = objUserLogin.ID;
                    objCurrentUser.UserName = objUserLogin.UserName;
                    objCurrentUser.Email = objUserLogin.Email;
                    objCurrentUser.SettingsFolder = Server.MapPath(ConfigurationManager.AppSettings.Get("SITEPATH"));
                    objCurrentUser.OutputFolder = Server.MapPath(ConfigurationManager.AppSettings.Get("OUTPUTPATH"));
                    Session["Modules"] = objUserLogin.ModuleList;
                    Session["MyMenu"] = null;
                    Session["IsClientViewManager"] = objUserLogin.IsClientViewManager;
                    Session["clientId"] = objUserLogin.ClientId;
                    Session["LoggedInUser"] = objCurrentUser;
                    Session["UserName"]= objUserLogin.UserName;
                    Session["LoginCode"] = objUserLogin.LoginCode;
                    //Session["IsLogin"] = true;
                    return RedirectToAction("Index", "Home");
                    //return obj.Index();
                }
                else
                {
                    //Session["IsLogin"] = false;
                    ModelState.AddModelError(string.Empty, "User name or Password is Invalid");
                }
            }

            return View();
        }
 
        public void GetAssemblyinfo()
        {
            string Verdetails = "";
            //display assembly details
            System.Reflection.Assembly assembly = System.Reflection.Assembly.GetExecutingAssembly();
            System.IO.FileInfo fileInfo = new System.IO.FileInfo(assembly.Location);
            DateTime lastModified = fileInfo.LastWriteTime;
            Verdetails += "VERSION = " + lastModified;
            //display server and catalog details
            string[] currentcons = ConfigurationManager.AppSettings.GetValues("PORTAL");
            string connectString = currentcons[0];
            SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectString);
            string DBdetails = "Server = " + builder.DataSource.ToUpper() + " - Initial Catalog = " + builder.InitialCatalog.ToUpper();
            Session["CurrentCatalog"] = builder.InitialCatalog.ToUpper();
            ViewBag.Versiondetails = Verdetails;
            ViewBag.Assemblydetails = DBdetails;


        }
        //set Production Connection string as default
        public void SetDefaultConnString()
        {
            string Connstring = ConfigurationManager.AppSettings["TestConString"];
            ConfigurationManager.AppSettings.Set("PORTAL", Connstring);
            Session["Currdb"] = ConfigurationManager.AppSettings["TestDB"];
           
        }
        public ActionResult LogOff()
        {
            Session["MyMenu"] = null;
           // Session["IsLogin"] = false;
            return RedirectToAction("Login", "login");
        }
       
    }
}