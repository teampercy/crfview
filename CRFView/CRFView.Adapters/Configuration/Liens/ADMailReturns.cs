﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using CRF.BLL.CRFDB.VIEWS;

namespace CRFView.Adapters
{
    public class ADMailReturns
    {
        #region Properties
        public int Id { get; set; }
        public string Description { get; set; }
        public string LegalPartyType { get; set; }
        public int ResearchTypeId { get; set; }
        public int ReasonTypeId { get; set; }
        public int ActionId { get; set; }
        //additional property for view
        public string ResearchType { get; set; }
        public string ReasonType { get; set; }
        public string ActionDescription { get; set; }
        #endregion

        #region CRUD Operations
        //List of MailReturns
        public List<ADMailReturns> ListMailReturns()
        {
            vwMailReturns EntityView = new vwMailReturns();
            TableView EntityList;
            try
            {
                Query query = new Query(EntityView);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADMailReturns> CIList = (from DataRow dr in dt.Rows
                                           select new ADMailReturns()
                                           {
                                               Id = Convert.ToInt32(dr["Id"]),
                                               Description = Convert.ToString(dr["Description"]),
                                               LegalPartyType = Convert.ToString(dr["LegalPartyType"]),
                                               ResearchType = Convert.ToString(dr["ResearchType"]),
                                               ReasonType = Convert.ToString(dr["ReasonType"]),
                                               ActionDescription = Convert.ToString(dr["ActionDescription"])
                                           }).ToList();
                return CIList;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<ADMailReturns>());
            }
        }

        //Read Details from database
        public ADMailReturns ReadMailReturns(int id)
        {
            MailReturns myentity = new MailReturns();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (MailReturns)myentityItable;
            AutoMapper.Mapper.CreateMap<MailReturns, ADMailReturns>();
            return (AutoMapper.Mapper.Map<ADMailReturns>(myentity));
        }

        ////Save(Create or Update) MailReturns Details
        public bool SaveMailReturns(ADMailReturns updatedMailReturns)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADMailReturns, MailReturns>();
                ITable tblMailReturns;
                if (updatedMailReturns.Id != 0)
                {
                    ADMailReturns loadMailReturns = ReadMailReturns(updatedMailReturns.Id);
                    loadMailReturns.Description = updatedMailReturns.Description;
                    loadMailReturns.LegalPartyType = updatedMailReturns.LegalPartyType;
                    loadMailReturns.ResearchTypeId = updatedMailReturns.ResearchTypeId;
                    loadMailReturns.ReasonTypeId = updatedMailReturns.ReasonTypeId;
                    loadMailReturns.ActionId = updatedMailReturns.ActionId;
                    MailReturns objMailReturns = AutoMapper.Mapper.Map<MailReturns>(loadMailReturns);
                    tblMailReturns = (ITable)objMailReturns;
                    DBO.Provider.Update(ref tblMailReturns);
                    return true;
                }
                else
                {
                    MailReturns objMailReturns = AutoMapper.Mapper.Map<MailReturns>(updatedMailReturns);
                    tblMailReturns = (ITable)objMailReturns;
                    DBO.Provider.Create(ref tblMailReturns);
                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }
        #endregion
    }
}