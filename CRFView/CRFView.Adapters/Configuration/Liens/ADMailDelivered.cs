﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using CRF.BLL.CRFDB.VIEWS;

namespace CRFView.Adapters
{
    public class ADMailDelivered
    {
        #region Properties
        public int Id { get; set; }
        public string Description { get; set; }
        public string LegalPartyType { get; set; }
        public int ResultId { get; set; }
        public int ActionId { get; set; }
        //additional property for view
        public string ResultDescription { get; set; }
        public string ActionDescription { get; set; }
        #endregion

        #region CRUD Operations
        //List of MailDelivered
        public List<ADMailDelivered> ListMailDelivered()
        {
            vwMailDelivered EntityView = new vwMailDelivered();
            TableView EntityList;
            try
            {
                Query query = new Query(EntityView);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADMailDelivered> CIList = (from DataRow dr in dt.Rows
                                                select new ADMailDelivered()
                                                {
                                                    Id = Convert.ToInt32(dr["Id"]),
                                                    Description = Convert.ToString(dr["Description"]),
                                                    LegalPartyType = Convert.ToString(dr["LegalPartyType"]),
                                                    ResultDescription = Convert.ToString(dr["ResultDescription"]),
                                                    ActionDescription = Convert.ToString(dr["ActionDescription"])
                                                }).ToList();
                return CIList;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<ADMailDelivered>());
            }
        }

        //Read Details from database
        public ADMailDelivered ReadMailDelivered(int id)
        {
            MailDelivered myentity = new MailDelivered();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (MailDelivered)myentityItable;
            AutoMapper.Mapper.CreateMap<MailDelivered, ADMailDelivered>();
            return (AutoMapper.Mapper.Map<ADMailDelivered>(myentity));
        }

        ////Save(Create or Update) MailDelivered Details
        public bool SaveMailDelivered(ADMailDelivered updatedMailDelivered)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADMailDelivered, MailDelivered>();
                ITable tblMailDelivered;
                if (updatedMailDelivered.Id != 0)
                {
                    ADMailDelivered loadMailDelivered = ReadMailDelivered(updatedMailDelivered.Id);
                    loadMailDelivered.Description = updatedMailDelivered.Description;
                    loadMailDelivered.LegalPartyType = updatedMailDelivered.LegalPartyType;
                    loadMailDelivered.ResultId = updatedMailDelivered.ResultId;
                    loadMailDelivered.ActionId = updatedMailDelivered.ActionId;
                    MailDelivered objMailDelivered = AutoMapper.Mapper.Map<MailDelivered>(loadMailDelivered);
                    tblMailDelivered = (ITable)objMailDelivered;
                    DBO.Provider.Update(ref tblMailDelivered);
                    return true;
                }
                else
                {
                    MailDelivered objMailDelivered = AutoMapper.Mapper.Map<MailDelivered>(updatedMailDelivered);
                    tblMailDelivered = (ITable)objMailDelivered;
                    DBO.Provider.Create(ref tblMailDelivered);
                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }
        #endregion
    }
}