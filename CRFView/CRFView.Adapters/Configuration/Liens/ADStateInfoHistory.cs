﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.Web;

namespace CRFView.Adapters
{
    public class ADStateInfoHistory
    {
        #region Properties
        public int Id { get; set; }
        public int StateId { get; set; }
        public DateTime DateCreated { get; set; }
        public string Note { get; set; }
        public string UserName { get; set; }

        #endregion

        #region CRUD Operations
        //List of StateInfoHistory
        public List<ADStateInfoHistory> ListStateInfoHistory()
        {
            StateInfoHistory EntityView = new StateInfoHistory();
            TableView EntityList;
            try
            {
                Query query = new Query(EntityView);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADStateInfoHistory> CIList = (from DataRow dr in dt.Rows
                                           select new ADStateInfoHistory()
                                           {
                                               Id = Convert.ToInt32(dr["Id"]),
                                               DateCreated = dr["DateCreated"]!=DBNull.Value?Convert.ToDateTime(dr["DateCreated"]):DateTime.MinValue,
                                               Note = Convert.ToString(dr["Note"]),
                                               UserName = Convert.ToString(dr["UserName"])
                                           }).ToList();
                return CIList;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<ADStateInfoHistory>());
            }
        }

        //Read Details from database
        public ADStateInfoHistory ReadStateInfoHistory(int id)
        {
            StateInfoHistory myentity = new StateInfoHistory();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (StateInfoHistory)myentityItable;
            AutoMapper.Mapper.CreateMap<StateInfoHistory, ADStateInfoHistory>();
            return (AutoMapper.Mapper.Map<ADStateInfoHistory>(myentity));
        }

        ////Save(Create or Update) StateInfoHistory Details
        public bool SaveStateInfoHistory(ADStateInfoHistory updatedStateInfoHistory)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADStateInfoHistory, StateInfoHistory>();
                ITable tblStateInfoHistory;
                updatedStateInfoHistory.UserName = (string)HttpContext.Current.Session["UserName"];
                    StateInfoHistory objStateInfoHistory = AutoMapper.Mapper.Map<StateInfoHistory>(updatedStateInfoHistory);
                    objStateInfoHistory.DateCreated = DateTime.Now;
                    tblStateInfoHistory = (ITable)objStateInfoHistory;
                    DBO.Provider.Create(ref tblStateInfoHistory);
                    return true;
              
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }
        #endregion
    }
}