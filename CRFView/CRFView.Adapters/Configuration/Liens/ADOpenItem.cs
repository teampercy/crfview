﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADOpenItem
    {
        #region Properties
        public int Id { get; set; }
        public string OpenItemType { get; set; }
        public string OpenItemMsg { get; set; }

        #endregion

        #region CRUD Operations
        //List of OpenItem
        public List<ADOpenItem> ListOpenItem()
        {
            OpenItem EntityView = new OpenItem();
            TableView EntityList;
            try
            {
                Query query = new Query(EntityView);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADOpenItem> CIList = (from DataRow dr in dt.Rows
                                           select new ADOpenItem()
                                           {
                                               Id = Convert.ToInt32(dr["Id"]),
                                               OpenItemType = Convert.ToString(dr["OpenItemType"]),
                                               OpenItemMsg = Convert.ToString(dr["OpenItemMsg"])
                                           }).ToList();
                return CIList;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<ADOpenItem>());
            }
        }

        //Read Details from database
        public ADOpenItem ReadOpenItem(int id)
        {
            OpenItem myentity = new OpenItem();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (OpenItem)myentityItable;
            AutoMapper.Mapper.CreateMap<OpenItem, ADOpenItem>();
            return (AutoMapper.Mapper.Map<ADOpenItem>(myentity));
        }

        ////Save(Create or Update) OpenItem Details
        public bool SaveOpenItem(ADOpenItem updatedOpenItem)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADOpenItem, OpenItem>();
                ITable tblOpenItem;
                if (updatedOpenItem.Id != 0)
                {
                    ADOpenItem loadOpenItem = ReadOpenItem(updatedOpenItem.Id);
                    loadOpenItem.OpenItemType = updatedOpenItem.OpenItemType;
                    loadOpenItem.OpenItemMsg = updatedOpenItem.OpenItemMsg;
                    OpenItem objOpenItem = AutoMapper.Mapper.Map<OpenItem>(loadOpenItem);
                    tblOpenItem = (ITable)objOpenItem;
                    DBO.Provider.Update(ref tblOpenItem);
                    return true;
                }
                else
                {
                    OpenItem objOpenItem = AutoMapper.Mapper.Map<OpenItem>(updatedOpenItem);
                    tblOpenItem = (ITable)objOpenItem;
                    DBO.Provider.Create(ref tblOpenItem);
                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }
        #endregion
    }
}