﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using CRF.BLL.CRFDB.VIEWS;

namespace CRFView.Adapters
{
    public class ADJobStatus
    {
        #region Properties
        public int Id { get; set; }
        public string JobStatus { get; set; }
        public string JobStatusDescr { get; set; }
        public int StatusGroup { get; set; }
        public bool IsClosed { get; set; }
        public bool IsLienAlert { get; set; }
      public bool IsReviewStatus { get; set; }
        public bool IsNoticeRequestStatus { get; set; }
        public string LongDescription { get; set; }
        // addition Property for view
        public string StatusGroupDescription { get; set; }
        #endregion

        #region CRUD Operations
        //List of JobStatus
        public List<ADJobStatus> ListJobStatus()
        {
            vwJobStatusList EntityView = new vwJobStatusList();
            TableView EntityList;
            try
            {
                Query query = new Query(EntityView);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADJobStatus> CIList = (from DataRow dr in dt.Rows
                                           select new ADJobStatus()
                                           {
                                               Id = Convert.ToInt32(dr["Id"]),
                                               JobStatus = Convert.ToString(dr["JobStatus"]),
                                               JobStatusDescr = Convert.ToString(dr["JobStatusDescr"]),
                                               StatusGroupDescription = Convert.ToString(dr["StatusGroupDescription"]),
                                               IsClosed = dr["IsClosed"] !=DBNull.Value?Convert.ToBoolean(dr["IsClosed"]):false,
                                               IsLienAlert = dr["IsLienAlert"] != DBNull.Value ? Convert.ToBoolean(dr["IsLienAlert"]) : false,
                                               IsNoticeRequestStatus = dr["IsNoticeRequestStatus"] != DBNull.Value ? Convert.ToBoolean(dr["IsNoticeRequestStatus"]) : false,
                                              // IsReviewStatus = dr["IsReviewStatus"] != DBNull.Value ? Convert.ToBoolean(dr["IsReviewStatus"]) : false,
                                               LongDescription = Convert.ToString(dr["LongDescription"])
                                           }).ToList();
                return CIList;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<ADJobStatus>());
            }
        }

        //Read Details from database
        public ADJobStatus ReadJobStatus(int id)
        {
            JobStatus myentity = new JobStatus();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (JobStatus)myentityItable;
            AutoMapper.Mapper.CreateMap<JobStatus, ADJobStatus>();
            return (AutoMapper.Mapper.Map<ADJobStatus>(myentity));
        }

        ////Save(Create or Update) JobStatus Details
        public bool SaveJobStatus(ADJobStatus updatedJobStatus)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADJobStatus, JobStatus>();
                ITable tblJobStatus;
                if (updatedJobStatus.Id != 0)
                {
                    ADJobStatus loadJobStatus = ReadJobStatus(updatedJobStatus.Id);
                    loadJobStatus.JobStatus = updatedJobStatus.JobStatus;
                    loadJobStatus.JobStatusDescr = updatedJobStatus.JobStatusDescr;
                    loadJobStatus.StatusGroup = updatedJobStatus.StatusGroup;
                    loadJobStatus.IsClosed = updatedJobStatus.IsClosed;
                    loadJobStatus.IsLienAlert = updatedJobStatus.IsLienAlert;
                    loadJobStatus.IsNoticeRequestStatus = updatedJobStatus.IsNoticeRequestStatus;
                    loadJobStatus.IsReviewStatus = updatedJobStatus.IsReviewStatus;
                    loadJobStatus.LongDescription = updatedJobStatus.LongDescription;
                    JobStatus objJobStatus = AutoMapper.Mapper.Map<JobStatus>(loadJobStatus);
                    tblJobStatus = (ITable)objJobStatus;
                    DBO.Provider.Update(ref tblJobStatus);
                    return true;
                }
                else
                {
                    JobStatus objJobStatus = AutoMapper.Mapper.Map<JobStatus>(updatedJobStatus);
                    tblJobStatus = (ITable)objJobStatus;
                    DBO.Provider.Create(ref tblJobStatus);
                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }
        #endregion
    }
}