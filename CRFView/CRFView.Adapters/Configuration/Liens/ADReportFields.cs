﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using CRF.BLL.CRFDB.VIEWS;

namespace CRFView.Adapters
{
    public class ADReportFields
    {
        #region Properties
        public int Id { get; set; }
        public int ReportId { get; set; }
        public int FieldId { get; set; }
        //other properties for view
        public string ReportName { get; set; }
        public string FieldName { get; set; }
        #endregion

        #region CRUD Operations
        //List of ReportFields
        public List<ADReportFields> ListReportFields()
        {
            vwLienReportFields EntityView = new vwLienReportFields();
            TableView EntityList;
            try
            {
                Query query = new Query(EntityView);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADReportFields> CIList = (from DataRow dr in dt.Rows
                                           select new ADReportFields()
                                           {
                                               Id = Convert.ToInt32(dr["Id"]),
                                               ReportName = Convert.ToString(dr["Description"]),
                                               FieldName = Convert.ToString(dr["FieldName"])
                                           }).ToList();
                return CIList;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<ADReportFields>());
            }
        }

        //Read Details from database
        public ADReportFields ReadReportFields(int id)
        {
            ReportFields myentity = new ReportFields();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (ReportFields)myentityItable;
            AutoMapper.Mapper.CreateMap<ReportFields, ADReportFields>();
            return (AutoMapper.Mapper.Map<ADReportFields>(myentity));
        }

        ////Save(Create or Update) ReportFields Details
        public bool SaveReportFields(ADReportFields updatedReportFields)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADReportFields, ReportFields>();
                ITable tblReportFields;
                if (updatedReportFields.Id != 0)
                {
                    ADReportFields loadReportFields = ReadReportFields(updatedReportFields.Id);
                    loadReportFields.ReportId = updatedReportFields.ReportId;
                    loadReportFields.FieldId = updatedReportFields.FieldId;
                    ReportFields objReportFields = AutoMapper.Mapper.Map<ReportFields>(loadReportFields);
                    tblReportFields = (ITable)objReportFields;
                    DBO.Provider.Update(ref tblReportFields);
                    return true;
                }
                else
                {
                    ReportFields objReportFields = AutoMapper.Mapper.Map<ReportFields>(updatedReportFields);
                    tblReportFields = (ITable)objReportFields;
                    DBO.Provider.Create(ref tblReportFields);
                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }
        #endregion
    }
}