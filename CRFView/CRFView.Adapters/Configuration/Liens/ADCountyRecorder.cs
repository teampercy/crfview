﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;

namespace CRFView.Adapters
{
    public class ADCountyRecorder
    {
        #region Properties
        public int CountyRecorderID { get; set; }
        public string CountyRecorderNum { get; set; }
        public string RecorderName { get; set; }
        public string RecorderName2 { get; set; }
        public string RecorderAdd { get; set; }
        public string RecorderCity { get; set; }
        public string RecorderState { get; set; }
        public string RecorderZip { get; set; }
        public string RecorderPhone { get; set; }
        public string RecorderFax { get; set; }
        public string RecorderWeb { get; set; }
        public string RecorderEmail { get; set; }
        [DisplayFormat(ApplyFormatInEditMode = true, DataFormatString = "{0:C}")]
        public decimal RecorderLienFee { get; set; }
        public decimal RecorderPageFee { get; set; }
        public bool RecorderCourier { get; set; }
        public bool RecorderOvernight { get; set; }
        public string RecorderInfo { get; set; }
        public decimal RecorderLienReleaseFee { get; set; }
        public decimal RecorderNoticeFee { get; set; }
        public decimal RecorderCopyFee { get; set; }
        public string RecorderCounty { get; set; }
        public string APNFormat { get; set; }
        #endregion

        #region CRUD Operations

        CountyRecorder myentity;
        //List of CountyRecorder
        public List<ADCountyRecorder> ListCountyRecorder()
        {
          
            myentity = new CountyRecorder();
            TableView EntityList;
            try
            {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADCountyRecorder> List = (from DataRow dr in dt.Rows
                                             select new ADCountyRecorder()
                                             {
                                                 CountyRecorderID = Convert.ToInt16(dr["CountyRecorderID"]),
                                                 CountyRecorderNum = Convert.ToString(dr["CountyRecorderNum"]),
                                                 RecorderName = Convert.ToString(dr["RecorderName"]),
                                                 RecorderCounty = Convert.ToString(dr["RecorderCounty"]),
                                                 RecorderPhone = Convert.ToString(dr["RecorderPhone"]),
                                                 RecorderInfo = Convert.ToString(dr["RecorderInfo"])
                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<ADCountyRecorder>());
            }
        }

        //Read Details from database
        public ADCountyRecorder ReadCountyRecorder(int id)
        {
            CountyRecorder myentity = new CountyRecorder();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (CountyRecorder)myentityItable;
            AutoMapper.Mapper.CreateMap<CountyRecorder, ADCountyRecorder>();
            return (AutoMapper.Mapper.Map<ADCountyRecorder>(myentity));
        }

        //Save(Create or Update) CountyRecorder Details
        public bool SaveCountyRecorder(ADCountyRecorder updatedCountyRecorder)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADCountyRecorder, CountyRecorder>();
                ITable tblCountyRecorder;
                if (updatedCountyRecorder.CountyRecorderID != 0)
                {
                    ADCountyRecorder loadCountyRecorder = ReadCountyRecorder(updatedCountyRecorder.CountyRecorderID);
                    loadCountyRecorder.CountyRecorderNum = updatedCountyRecorder.CountyRecorderNum;
                    loadCountyRecorder.RecorderName = updatedCountyRecorder.RecorderName;
                    loadCountyRecorder.RecorderName2 = updatedCountyRecorder.RecorderName2;
                    loadCountyRecorder.RecorderAdd = updatedCountyRecorder.RecorderAdd;
                    loadCountyRecorder.RecorderCity = updatedCountyRecorder.RecorderCity;
                    loadCountyRecorder.RecorderState = updatedCountyRecorder.RecorderState;
                    loadCountyRecorder.RecorderZip = updatedCountyRecorder.RecorderZip;
                    loadCountyRecorder.RecorderCounty = updatedCountyRecorder.RecorderCounty;
                    loadCountyRecorder.RecorderPhone = updatedCountyRecorder.RecorderPhone;
                    loadCountyRecorder.RecorderFax = updatedCountyRecorder.RecorderFax;
                    loadCountyRecorder.RecorderWeb = updatedCountyRecorder.RecorderWeb;
                    loadCountyRecorder.RecorderEmail = updatedCountyRecorder.RecorderEmail;
                    loadCountyRecorder.RecorderLienFee = updatedCountyRecorder.RecorderLienFee;
                    loadCountyRecorder.APNFormat = updatedCountyRecorder.APNFormat;
                    loadCountyRecorder.RecorderLienReleaseFee = updatedCountyRecorder.RecorderLienReleaseFee;
                    loadCountyRecorder.RecorderPageFee = updatedCountyRecorder.RecorderPageFee;
                    loadCountyRecorder.RecorderNoticeFee = updatedCountyRecorder.RecorderNoticeFee;
                    loadCountyRecorder.RecorderCopyFee = updatedCountyRecorder.RecorderCopyFee;
                    loadCountyRecorder.RecorderInfo = updatedCountyRecorder.RecorderInfo;
                    loadCountyRecorder.RecorderCourier = updatedCountyRecorder.RecorderCourier;
                    loadCountyRecorder.RecorderOvernight = updatedCountyRecorder.RecorderOvernight;
                    CountyRecorder objCountyRecorder = AutoMapper.Mapper.Map<CountyRecorder>(loadCountyRecorder);
                     tblCountyRecorder = objCountyRecorder;
                    DBO.Provider.Update(ref tblCountyRecorder);
                }
                else
                {
                    CountyRecorder objCountyRecorder = AutoMapper.Mapper.Map<CountyRecorder>(updatedCountyRecorder);
                    tblCountyRecorder = objCountyRecorder;
                    DBO.Provider.Create(ref tblCountyRecorder);
                }
            }
            catch(Exception ex)
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}