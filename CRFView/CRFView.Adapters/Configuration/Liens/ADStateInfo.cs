﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using CRF.BLL.CRFDB.VIEWS;

namespace CRFView.Adapters
{
    public class ADStateInfo
    {
        #region Properties
        public int Id { get; set; }
        public string StateName { get; set; }
        public string StateInitials { get; set; }
        public string PrelimReq { get; set; }
        public string PrelimTime { get; set; }
        public string PrelimInfo { get; set; }
        public string MechTime { get; set; }
        public string MechPeriod { get; set; }
        public string MechServe { get; set; }
        public string MechInfo { get; set; }
        public string StopNotice { get; set; }
        public string StopNoticeInfo { get; set; }
        public string MiscInfo { get; set; }
        public bool OwnerServBox { get; set; }
        public bool GCServBox { get; set; }
        public bool LenderServBox { get; set; }
        public bool CustServBox { get; set; }
        public bool CourtServeBox { get; set; }
        public bool EstBalanceBox { get; set; }
        public bool NOIStateBox { get; set; }
        public bool ResidentialRulesBox { get; set; }
        public bool WebLien { get; set; }
        public bool WebNTO { get; set; }
        public bool NOCBox { get; set; }
        public string NTODays { get; set; }
        public string LienDays { get; set; }
        public string BondDays { get; set; }
        public string SNDays { get; set; }
        public bool  JobCountyReq { get; set; }
        public string NTODaysRes { get; set; }
        public bool  DesigneeServBox { get; set; }
        public string DefaultDesk { get; set; }
        public int  DeskRuleId { get; set; }
        public bool  NoGCNTOWithSameCust { get; set; }
        public string ForeclosureDays { get; set; }
        public decimal  InterestRate { get; set; }
        public string StateAlert { get; set; }
        public bool  FullPriceLien { get; set; }
        public bool  MLAgent { get; set; }
        public bool  IsBillableLegalDesc { get; set; }
        public bool  GCOnlyState { get; set; }
        public string NOCLienDays { get; set; }
        public string NOCBondDays { get; set; }
        public string NOCSNDays { get; set; }
        public string NOILienDays { get; set; }
        public string PublicLienDays { get; set; }
        public string PublicNOCLienDays { get; set; }
        public string PublicBondDays { get; set; }
        public string PublicNOCBondDays { get; set; }
        public string PublicSNDays { get; set; }
        public string PublicNOCSNDays { get; set; }
        public string ResLienDays { get; set; }
        public string ResNOCLienDays { get; set; }
        public string ResBondDays { get; set; }
        public string ResNOCBondDays { get; set; }
        public string ResSNDays { get; set; }
        public string ResNOCSNDays { get; set; }
        public string ResForeclosureDays { get; set; }
        public string PublicForeclosureDays { get; set; }
        public string LienAlert { get; set; }
        public DateTime  LastUpdatedOn { get; set; }
        public bool  NoticeOfCompBox { get; set; }
        public bool  GCSendReg { get; set; }
        public DateTime  NTOUpdateDate { get; set; }
        public DateTime  LienUpdateDate { get; set; }
        public DateTime  BondUpdateDate { get; set; }
        public DateTime  StopNoticeUpdateDat { get; set; }
        public DateTime  NOIUpdateDate { get; set; }
        public bool  MaterialValidation { get; set; }
        public bool  FirstFurnish { get; set; }
        public bool  LastFurnish { get; set; }
        public bool  NonStatutory { get; set; }
        public bool  LateNotice { get; set; }
        public bool  LienSuitStartLF { get; set; }
        public bool  LienSuitStartFileDate { get; set; }
        public bool  BondSuitStartLF { get; set; }
        public bool  BondSuitStartFileDate { get; set; }
        public string BondSuitDays { get; set; }
        public string PublicBondSuitDays { get; set; }
        public string ResBondSuitDays { get; set; }
        public bool  Monthly { get; set; }
        public string PublicNTODays { get; set; }
        public bool  RentalException { get; set; }
        public string PublicNOIDays { get; set; }
        public string ResNOIDays { get; set; }
        public bool  ReturnReceiptPrivate { get; set; }
        public bool  ReturnReceiptPublic { get; set; }
        //Properties for View
        public string Furnish { get; set; }
        public string LienSuit { get; set; }
        public string BondSuit { get; set; }
        #endregion

        #region CRUD Operations
        //List of StateInfo
        public List<ADStateInfo> ListStateInfo()
        {
            StateInfo EntityView = new StateInfo();
            TableView EntityList;
            try
            {
                Query query = new Query(EntityView);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADStateInfo> CIList = (from DataRow dr in dt.Rows
                                           select new ADStateInfo()
                                           {
                                               Id = Convert.ToInt32(dr["Id"]),
                                               StateName = Convert.ToString(dr["StateName"]),
                                               StateInitials = Convert.ToString(dr["StateInitials"]),
                                               PrelimReq = Convert.ToString(dr["PrelimReq"]),
                                               PrelimTime = Convert.ToString(dr["PrelimTime"]),
                                               PrelimInfo = Convert.ToString(dr["PrelimInfo"]),
                                               MechTime = Convert.ToString(dr["MechTime"]),
                                               MechPeriod = Convert.ToString(dr["MechPeriod"]),
                                               MechServe = Convert.ToString(dr["MechServe"]),
                                               MechInfo = Convert.ToString(dr["MechInfo"]),
                                               StopNotice = Convert.ToString(dr["StopNotice"]),
                                               StopNoticeInfo = Convert.ToString(dr["StopNoticeInfo"]),
                                               MiscInfo= Convert.ToString(dr["MiscInfo"]),
                                               OwnerServBox = dr["OwnerServBox"] != DBNull.Value?Convert.ToBoolean(dr["OwnerServBox"]):false,
                                               GCServBox = dr["GCServBox"] != DBNull.Value ? Convert.ToBoolean(dr["GCServBox"]) : false,
                                               LenderServBox = dr["LenderServBox"] != DBNull.Value ? Convert.ToBoolean(dr["LenderServBox"]) : false,
                                               CustServBox = dr["CustServBox"] != DBNull.Value ? Convert.ToBoolean(dr["CustServBox"]) : false,
                                               CourtServeBox = dr["CourtServeBox"] != DBNull.Value ? Convert.ToBoolean(dr["CourtServeBox"]) : false,
                                               EstBalanceBox = dr["EstBalanceBox"] != DBNull.Value ? Convert.ToBoolean(dr["EstBalanceBox"]) : false,
                                               NOIStateBox = dr["NOIStateBox"] != DBNull.Value ? Convert.ToBoolean(dr["NOIStateBox"]) : false,
                                               ResidentialRulesBox = dr["ResidentialRulesBox"] != DBNull.Value ? Convert.ToBoolean(dr["ResidentialRulesBox"]) : false,
                                               WebLien = dr["WebLien"] != DBNull.Value ? Convert.ToBoolean(dr["WebLien"]) : false,
                                               WebNTO = dr["WebNTO"] != DBNull.Value ? Convert.ToBoolean(dr["WebNTO"]) : false,
                                               NOCBox = dr["NOCBox"] != DBNull.Value ? Convert.ToBoolean(dr["NOCBox"]) : false,
                                               NTODays = Convert.ToString(dr["NTODays"]),
                                               LienDays = Convert.ToString(dr["LienDays"]),
                                               BondDays = Convert.ToString(dr["BondDays"]),
                                               SNDays = Convert.ToString(dr["SNDays"]),
                                               JobCountyReq = dr["JobCountyReq"] != DBNull.Value ? Convert.ToBoolean(dr["JobCountyReq"]) : false,
                                               NTODaysRes = Convert.ToString(dr["NTODaysRes"]),
                                               DesigneeServBox = dr["DesigneeServBox"] != DBNull.Value ? Convert.ToBoolean(dr["DesigneeServBox"]) : false,
                                               DefaultDesk = Convert.ToString(dr["DefaultDesk"]),
                                               DeskRuleId = dr["DeskRuleId"] != DBNull.Value ? Convert.ToInt32(dr["DeskRuleId"]) : 0,
                                               NoGCNTOWithSameCust = dr["NoGCNTOWithSameCust"] != DBNull.Value ? Convert.ToBoolean(dr["NoGCNTOWithSameCust"]) : false,
                                               ForeclosureDays = Convert.ToString(dr["ForeclosureDays"]),
                                               InterestRate = dr["InterestRate"] != DBNull.Value ? Convert.ToDecimal(dr["InterestRate"]) : 0,
                                               StateAlert = Convert.ToString(dr["StateAlert"]),
                                               FullPriceLien = dr["FullPriceLien"] != DBNull.Value ? Convert.ToBoolean(dr["FullPriceLien"]) : false,
                                               MLAgent = dr["MLAgent"] != DBNull.Value ? Convert.ToBoolean(dr["MLAgent"]) : false,
                                               IsBillableLegalDesc = dr["IsBillableLegalDesc"] != DBNull.Value ? Convert.ToBoolean(dr["IsBillableLegalDesc"]) : false,
                                               GCOnlyState = dr["GCOnlyState"] != DBNull.Value ? Convert.ToBoolean(dr["GCOnlyState"]) : false,
                                               NOCLienDays = Convert.ToString(dr["NOCLienDays"]),
                                               NOCBondDays = Convert.ToString(dr["NOCBondDays"]),
                                               NOCSNDays = Convert.ToString(dr["NOCSNDays"]),
                                               NOILienDays = Convert.ToString(dr["NOILienDays"]),
                                               PublicLienDays = Convert.ToString(dr["PublicLienDays"]),
                                               PublicNOCLienDays = Convert.ToString(dr["PublicNOCLienDays"]),
                                               PublicBondDays = Convert.ToString(dr["PublicBondDays"]),
                                               PublicNOCBondDays = Convert.ToString(dr["PublicNOCBondDays"]),
                                               PublicSNDays = Convert.ToString(dr["PublicSNDays"]),
                                               PublicNOCSNDays = Convert.ToString(dr["PublicNOCSNDays"]),
                                               ResLienDays = Convert.ToString(dr["ResLienDays"]),
                                               ResNOCLienDays = Convert.ToString(dr["ResNOCLienDays"]),
                                               ResBondDays = Convert.ToString(dr["ResBondDays"]),
                                               ResNOCBondDays = Convert.ToString(dr["ResNOCBondDays"]),
                                               ResSNDays = Convert.ToString(dr["ResSNDays"]),
                                               ResNOCSNDays = Convert.ToString(dr["ResNOCSNDays"]),
                                               ResForeclosureDays = Convert.ToString(dr["ResForeclosureDays"]),
                                               PublicForeclosureDays = Convert.ToString(dr["PublicForeclosureDays"]),
                                               LienAlert = Convert.ToString(dr["LienAlert"]),
                                               LastUpdatedOn = dr["LastUpdatedOn"] != DBNull.Value ? Convert.ToDateTime(dr["LastUpdatedOn"]) : DateTime.MinValue,
                                               NoticeOfCompBox = dr["NoticeOfCompBox"] != DBNull.Value ? Convert.ToBoolean(dr["NoticeOfCompBox"]) : false,
                                               GCSendReg = dr["GCSendReg"] != DBNull.Value ? Convert.ToBoolean(dr["GCSendReg"]) : false,
                                               NTOUpdateDate = dr["NTOUpdateDate"] != DBNull.Value ? Convert.ToDateTime(dr["NTOUpdateDate"]) : DateTime.MinValue,
                                               LienUpdateDate = dr["LienUpdateDate"] != DBNull.Value ? Convert.ToDateTime(dr["LienUpdateDate"]) : DateTime.MinValue,
                                               BondUpdateDate = dr["BondUpdateDate"] != DBNull.Value ? Convert.ToDateTime(dr["BondUpdateDate"]) : DateTime.MinValue,
                                               StopNoticeUpdateDat = dr["StopNoticeUpdateDat"] != DBNull.Value ? Convert.ToDateTime(dr["StopNoticeUpdateDat"]) : DateTime.MinValue,
                                               NOIUpdateDate = dr["NOIUpdateDate"] != DBNull.Value ? Convert.ToDateTime(dr["NOIUpdateDate"]) : DateTime.MinValue,
                                               MaterialValidation = dr["MaterialValidation"] != DBNull.Value ? Convert.ToBoolean(dr["MaterialValidation"]) : false,
                                               FirstFurnish = dr["FirstFurnish"] != DBNull.Value ? Convert.ToBoolean(dr["FirstFurnish"]) : false,
                                               LastFurnish = dr["LastFurnish"] != DBNull.Value ? Convert.ToBoolean(dr["LastFurnish"]) : false,
                                               NonStatutory = dr["NonStatutory"] != DBNull.Value ? Convert.ToBoolean(dr["NonStatutory"]) : false,
                                               LateNotice = dr["LateNotice"] != DBNull.Value ? Convert.ToBoolean(dr["LateNotice"]) : false,
                                               LienSuitStartLF = dr["LienSuitStartLF"] != DBNull.Value ? Convert.ToBoolean(dr["LienSuitStartLF"]) : false,
                                               LienSuitStartFileDate = dr["LienSuitStartFileDate"] != DBNull.Value ? Convert.ToBoolean(dr["LienSuitStartFileDate"]) : false,
                                               BondSuitStartLF = dr["BondSuitStartLF"] != DBNull.Value ? Convert.ToBoolean(dr["BondSuitStartLF"]) : false,
                                               BondSuitStartFileDate = dr["BondSuitStartFileDate"] != DBNull.Value ? Convert.ToBoolean(dr["BondSuitStartFileDate"]) : false,
                                               BondSuitDays = Convert.ToString(dr["BondSuitDays"]),
                                               PublicBondSuitDays = Convert.ToString(dr["PublicBondSuitDays"]),
                                               ResBondSuitDays = Convert.ToString(dr["ResBondSuitDays"]),
                                               Monthly = dr["Monthly"] != DBNull.Value ? Convert.ToBoolean(dr["Monthly"]) : false,
                                               PublicNTODays = Convert.ToString(dr["PublicNTODays"]),
                                               RentalException = dr["RentalException"] != DBNull.Value ? Convert.ToBoolean(dr["RentalException"]) : false,
                                               PublicNOIDays = Convert.ToString(dr["PublicNOIDays"]),
                                               ResNOIDays = Convert.ToString(dr["ResNOIDays"]),
                                               ReturnReceiptPrivate = dr["ReturnReceiptPrivate"] != DBNull.Value ? Convert.ToBoolean(dr["ReturnReceiptPrivate"]) : false,
                                               ReturnReceiptPublic = dr["ReturnReceiptPublic"] != DBNull.Value ? Convert.ToBoolean(dr["ReturnReceiptPublic"]) : false
                                                 }).ToList();
                return CIList;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<ADStateInfo>());
            }
        }

        //Read Details from database
        public ADStateInfo ReadStateInfo(int id)
        {
            StateInfo myentity = new StateInfo();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (StateInfo)myentityItable;
            AutoMapper.Mapper.CreateMap<StateInfo, ADStateInfo>();
            return (AutoMapper.Mapper.Map<ADStateInfo>(myentity));
        }

        ////Save(Create or Update) StateInfo Details
        public bool SaveStateInfo(ADStateInfo updatedStateInfo)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADStateInfo, StateInfo>();
                ITable tblStateInfo;
                if (updatedStateInfo.Id != 0)
                {             
                    StateInfo objStateInfo = AutoMapper.Mapper.Map<StateInfo>(updatedStateInfo);
                    tblStateInfo = (ITable)objStateInfo;
                    DBO.Provider.Update(ref tblStateInfo);
                    return true;
                }
                else
                {
                    StateInfo objStateInfo = AutoMapper.Mapper.Map<StateInfo>(updatedStateInfo);
                    tblStateInfo = (ITable)objStateInfo;
                    DBO.Provider.Create(ref tblStateInfo);
                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }
        #endregion
    }
}