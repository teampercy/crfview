﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using CRF.BLL.CRFDB.VIEWS;

namespace CRFView.Adapters
{
    public class ADClientSigner
    {
        #region Properties
        public int Id { get; set; }
        public int ClientId { get; set; }
        public string Signer { get; set; }
        public string SignerTitle { get; set; }
        public bool IsDupped { get; set; }
        public byte[] Signature { get; set; }
        public string SignerEmail { get; set; }
        //additional property for view
        public string ClientCode { get; set; }
        #endregion

        #region CRUD Operations
        //List of ClientSigner
        public List<ADClientSigner> ListClientSigner()
        {
            vwClientSignerList EntityView = new vwClientSignerList();
            TableView EntityList;
            try
            {

                Query query = new Query(EntityView);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADClientSigner> CIList = (from DataRow dr in dt.Rows
                                           select new ADClientSigner()
                                           {
                                               Id = Convert.ToInt32(dr["Id"]),
                                               ClientCode = Convert.ToString(dr["ClientCode"]),
                                               Signer = Convert.ToString(dr["Signer"]),
                                               SignerTitle = Convert.ToString(dr["SignerTitle"])
                                              }).ToList();

                return CIList;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<ADClientSigner>());
            }
        }

        //Read Details from database
        public ADClientSigner ReadClientSigner(int id)
        {
            ClientSigner myentity = new ClientSigner();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (ClientSigner)myentityItable;
            AutoMapper.Mapper.CreateMap<ClientSigner, ADClientSigner>();
            return (AutoMapper.Mapper.Map<ADClientSigner>(myentity));
        }

        ////Save(Create or Update) ClientSigner Details
        public bool SaveClientSigner(ADClientSigner updatedClientSigner)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADClientSigner, ClientSigner>();
                ITable tblClientSigner;
                if (updatedClientSigner.Id != 0)
                {
                    ADClientSigner loadClientSigner = ReadClientSigner(updatedClientSigner.Id);
                    loadClientSigner.ClientId = updatedClientSigner.ClientId;
                    loadClientSigner.Signer = updatedClientSigner.Signer;
                    loadClientSigner.SignerTitle = updatedClientSigner.SignerTitle;
                    ClientSigner objClientSigner = AutoMapper.Mapper.Map<ClientSigner>(loadClientSigner);
                    tblClientSigner = (ITable)objClientSigner;
                    DBO.Provider.Update(ref tblClientSigner);
                    return true;
                }
                else
                {
                    ClientSigner objClientSigner = AutoMapper.Mapper.Map<ClientSigner>(updatedClientSigner);
                    tblClientSigner = (ITable)objClientSigner;
                    DBO.Provider.Create(ref tblClientSigner);
                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }
        #endregion


    }
}