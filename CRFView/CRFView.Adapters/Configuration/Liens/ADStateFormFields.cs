﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADStateFormFields
    {
        #region Properties
        public int StateFormFieldID { get; set; }
        public string FieldName { get; set; }
        public string FriendlyName { get; set; }
        public string DataType { get; set; }

        #endregion

        #region CRUD Operations
        //List of StateFormFields
        public List<ADStateFormFields> ListStateFormFields()
        {
            StateFormFields EntityView = new StateFormFields();
            TableView EntityList;
            try
            {
                Query query = new Query(EntityView);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADStateFormFields> CIList = (from DataRow dr in dt.Rows
                                           select new ADStateFormFields()
                                           {
                                               StateFormFieldID = Convert.ToInt32(dr["StateFormFieldID"]),
                                               FieldName = Convert.ToString(dr["FieldName"]),
                                               FriendlyName = Convert.ToString(dr["FriendlyName"]),
                                               DataType = Convert.ToString(dr["DataType"])
                                           }).ToList();
                return CIList;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<ADStateFormFields>());
            }
        }

        //Read Details from database
        public ADStateFormFields ReadStateFormFields(int id)
        {
            StateFormFields myentity = new StateFormFields();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (StateFormFields)myentityItable;
            AutoMapper.Mapper.CreateMap<StateFormFields, ADStateFormFields>();
            return (AutoMapper.Mapper.Map<ADStateFormFields>(myentity));
        }

        ////Save(Create or Update) StateFormFields Details
        public bool SaveStateFormFields(ADStateFormFields updatedStateFormFields)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADStateFormFields, StateFormFields>();
                ITable tblStateFormFields;
                if (updatedStateFormFields.StateFormFieldID != 0)
                {
                    ADStateFormFields loadStateFormFields = ReadStateFormFields(updatedStateFormFields.StateFormFieldID);
                    loadStateFormFields.FieldName = updatedStateFormFields.FieldName;
                    loadStateFormFields.FriendlyName = updatedStateFormFields.FriendlyName;
                    loadStateFormFields.DataType = updatedStateFormFields.DataType;
                    StateFormFields objStateFormFields = AutoMapper.Mapper.Map<StateFormFields>(loadStateFormFields);
                    tblStateFormFields = (ITable)objStateFormFields;
                    DBO.Provider.Update(ref tblStateFormFields);
                    return true;
                }
                else
                {
                    StateFormFields objStateFormFields = AutoMapper.Mapper.Map<StateFormFields>(updatedStateFormFields);
                    tblStateFormFields = (ITable)objStateFormFields;
                    DBO.Provider.Create(ref tblStateFormFields);
                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }
        #endregion
    }
}