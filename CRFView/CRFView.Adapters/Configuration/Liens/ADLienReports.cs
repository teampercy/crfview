﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using CRF.BLL.CRFDB.VIEWS;

namespace CRFView.Adapters
{
    public class ADLienReports
    {
        #region Properties
        public int ReportId { get; set; }
        public string ReportName { get; set; }
        public string Description { get; set; }
        public int ReportFileId { get; set; }
        public int ReportCategoryId { get; set; }
        public string PrintFrom { get; set; }
        public string CustomSPName { get; set; }
        //addition properties for view
        public string ReportCategory { get; set; }
        public string ReportFileName { get; set; }
        #endregion

        #region CRUD Operations
        //List of LienReports
        public List<ADLienReports> ListLienReports()
        {
            vwLienReportsList EntityView = new vwLienReportsList();
            TableView EntityList;
            try
            {
                Query query = new Query(EntityView);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADLienReports> CIList = (from DataRow dr in dt.Rows
                                           select new ADLienReports()
                                           {
                                               ReportId = Convert.ToInt32(dr["ReportId"]),
                                               ReportName = Convert.ToString(dr["ReportName"]),
                                               Description = Convert.ToString(dr["Description"]),
                                               ReportCategory = Convert.ToString(dr["ReportCategory"]),
                                               PrintFrom = Convert.ToString(dr["PrintFrom"]),
                                               CustomSPName = Convert.ToString(dr["CustomSPName"]),
                                               ReportFileName = Convert.ToString(dr["ReportFileName"])
                                           }).ToList();
                return CIList;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<ADLienReports>());
            }
        }

        //Read Details from database
        public ADLienReports ReadLienReports(int id)
        {
            LienReports myentity = new LienReports();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (LienReports)myentityItable;
            AutoMapper.Mapper.CreateMap<LienReports, ADLienReports>();
            return (AutoMapper.Mapper.Map<ADLienReports>(myentity));
        }

        ////Save(Create or Update) LienReports Details
        public bool SaveLienReports(ADLienReports updatedLienReports)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADLienReports, LienReports>();
                ITable tblLienReports;
                if (updatedLienReports.ReportId != 0)
                {
                    ADLienReports loadLienReports = ReadLienReports(updatedLienReports.ReportId);
                    loadLienReports.ReportName = updatedLienReports.ReportName;
                    loadLienReports.Description = updatedLienReports.Description;
                    loadLienReports.ReportCategoryId = updatedLienReports.ReportCategoryId;
                    loadLienReports.PrintFrom = updatedLienReports.PrintFrom;
                    loadLienReports.ReportFileId = updatedLienReports.ReportFileId;
                    loadLienReports.CustomSPName = updatedLienReports.CustomSPName;
                    LienReports objLienReports = AutoMapper.Mapper.Map<LienReports>(loadLienReports);
                    tblLienReports = (ITable)objLienReports;
                    DBO.Provider.Update(ref tblLienReports);
                    return true;
                }
                else
                {
                    LienReports objLienReports = AutoMapper.Mapper.Map<LienReports>(updatedLienReports);
                    tblLienReports = (ITable)objLienReports;
                    DBO.Provider.Create(ref tblLienReports);
                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }
        #endregion
    }
}