﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADPostageRates
    {
        #region Properties
        public int Id { get; set; }
        public string PostageType { get; set; }
        public decimal PostageRate { get; set; }

        #endregion

        #region CRUD Operations
        //List of PostageRates
        public List<ADPostageRates> ListPostageRates()
        {
            PostageRates EntityView = new PostageRates();
            TableView EntityList;
            try
            {
                Query query = new Query(EntityView);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADPostageRates> CIList = (from DataRow dr in dt.Rows
                                           select new ADPostageRates()
                                           {
                                               Id = Convert.ToInt32(dr["Id"]),
                                               PostageType = Convert.ToString(dr["PostageType"]),
                                               PostageRate = dr["PostageRate"]!=DBNull.Value?Convert.ToDecimal(dr["PostageRate"]):0
                                           }).ToList();
                return CIList;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<ADPostageRates>());
            }
        }

        //Read Details from database
        public ADPostageRates ReadPostageRates(int id)
        {
            PostageRates myentity = new PostageRates();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (PostageRates)myentityItable;
            AutoMapper.Mapper.CreateMap<PostageRates, ADPostageRates>();
            return (AutoMapper.Mapper.Map<ADPostageRates>(myentity));
        }

        ////Save(Create or Update) PostageRates Details
        public bool SavePostageRates(ADPostageRates updatedPostageRates)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADPostageRates, PostageRates>();
                ITable tblPostageRates;
                if (updatedPostageRates.Id != 0)
                {
                    ADPostageRates loadPostageRates = ReadPostageRates(updatedPostageRates.Id);
                    loadPostageRates.PostageType = updatedPostageRates.PostageType;
                    loadPostageRates.PostageRate = updatedPostageRates.PostageRate;
                    PostageRates objPostageRates = AutoMapper.Mapper.Map<PostageRates>(loadPostageRates);
                    tblPostageRates = (ITable)objPostageRates;
                    DBO.Provider.Update(ref tblPostageRates);
                    return true;
                }
                else
                {
                    PostageRates objPostageRates = AutoMapper.Mapper.Map<PostageRates>(updatedPostageRates);
                    tblPostageRates = (ITable)objPostageRates;
                    DBO.Provider.Create(ref tblPostageRates);
                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }
        #endregion
    }
}