﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADStateFormNotices
    {
        #region Properties
        public int StateFormNoticeId { get; set; }
        public string StateCode { get; set; }
        public string ReportName { get; set; }
        public string FriendlyName { get; set; }
        public string FormType { get; set; }
        public bool ClientView { get; set; }
        public bool BalanceDueAmt { get; set; }
        public bool ContractAmt { get; set; }
        public bool PaymentAmt { get; set; }
        public bool FinanceChargeAmt { get; set; }
        public bool ExtrasToContractAmt { get; set; }
        public bool AgreedChangeOrdersAmt { get; set; }
        public bool PendingChangeOrdersAmt { get; set; }
        public bool DisputedClaimsAmt { get; set; }
        public bool FileDate { get; set; }
        public bool SuiteFileDate { get; set; }
        public bool ContractDate { get; set; }
        public bool NOISendDate { get; set; }
        public bool WorkPerformedMMYY { get; set; }
        public bool StructureType { get; set; }
        public bool AmendmentsToContractAmt { get; set; }
        public bool AgreedUponCreditsAmt { get; set; }
        public bool ContractPaymentsAmt { get; set; }
        public bool AmendmentPaymentsAmt { get; set; }
        public bool BlockNumber { get; set; }
        public bool LotNumber { get; set; }
        public bool DueDate { get; set; }
        public bool DescribePublicContract { get; set; }
        public bool DaysCreditIsExtended { get; set; }
        public bool NoticeofContractDate { get; set; }
        public bool BookNum { get; set; }
        public bool RecorderNum { get; set; }
        public bool EndDate { get; set; }
        public bool PageNum { get; set; }
        public bool JobCounty { get; set; }
        public bool DateNoticeSent { get; set; }
        public bool BondNumber { get; set; }
        public bool RegisteredAgent { get; set; }
        public bool CRFView { get; set; }
        public bool AltClaimantZip { get; set; }
        public bool AltClaimantState { get; set; }
        public bool AltClaimantCity { get; set; }
        public bool AltClaimantAdd2 { get; set; }
        public bool AltClaimantAdd1 { get; set; }
        public bool AltClaimantContact { get; set; }
        public bool APNNum { get; set; }
        public bool AlternateSigner { get; set; }
        
        #endregion

        #region CRUD Operations
        //List of StateFormNotices
        public List<ADStateFormNotices> ListStateFormNotices()
        {
            StateFormNotices EntityView = new StateFormNotices();
            TableView EntityList;
            try
            {
                Query query = new Query(EntityView);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADStateFormNotices> CIList = (from DataRow dr in dt.Rows
                                           select new ADStateFormNotices()
                                           {
                                               StateFormNoticeId = Convert.ToInt32(dr["StateFormNoticeId"]),
                                               StateCode = Convert.ToString(dr["StateCode"]),
                                               ReportName = Convert.ToString(dr["ReportName"]),
                                               FriendlyName = Convert.ToString(dr["FriendlyName"]),
                                               FormType = Convert.ToString(dr["FormType"])
                                           }).ToList();
                return CIList;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<ADStateFormNotices>());
            }
        }

        //Read Details from database
        public ADStateFormNotices ReadStateFormNotices(int id)
        {
            StateFormNotices myentity = new StateFormNotices();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (StateFormNotices)myentityItable;
            AutoMapper.Mapper.CreateMap<StateFormNotices, ADStateFormNotices>();
            return (AutoMapper.Mapper.Map<ADStateFormNotices>(myentity));
        }

        ////Save(Create or Update) StateFormNotices Details
        public bool SaveStateFormNotices(ADStateFormNotices updatedStateFormNotices)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADStateFormNotices, StateFormNotices>();
                ITable tblStateFormNotices;
                if (updatedStateFormNotices.StateFormNoticeId != 0)
                {
                    ADStateFormNotices loadStateFormNotices = ReadStateFormNotices(updatedStateFormNotices.StateFormNoticeId);
                    loadStateFormNotices.StateCode = updatedStateFormNotices.StateCode;
                    loadStateFormNotices.ReportName = updatedStateFormNotices.ReportName;
                    loadStateFormNotices.FormType = updatedStateFormNotices.FormType;
                    loadStateFormNotices.ClientView = updatedStateFormNotices.ClientView;
                    loadStateFormNotices.AlternateSigner = updatedStateFormNotices.AlternateSigner;
                    loadStateFormNotices.BalanceDueAmt = updatedStateFormNotices.BalanceDueAmt;
                    loadStateFormNotices.ContractAmt = updatedStateFormNotices.ContractAmt;
                    loadStateFormNotices.PaymentAmt = updatedStateFormNotices.PaymentAmt;
                    loadStateFormNotices.FinanceChargeAmt = updatedStateFormNotices.FinanceChargeAmt;
                    loadStateFormNotices.ExtrasToContractAmt = updatedStateFormNotices.ExtrasToContractAmt;
                    loadStateFormNotices.AgreedChangeOrdersAmt = updatedStateFormNotices.AgreedChangeOrdersAmt;
                    loadStateFormNotices.PendingChangeOrdersAmt = updatedStateFormNotices.PendingChangeOrdersAmt;
                    loadStateFormNotices.BookNum = updatedStateFormNotices.BookNum;
                    loadStateFormNotices.FileDate = updatedStateFormNotices.FileDate;
                    loadStateFormNotices.SuiteFileDate = updatedStateFormNotices.SuiteFileDate;
                    loadStateFormNotices.ContractDate = updatedStateFormNotices.ContractDate;
                    loadStateFormNotices.NOISendDate = updatedStateFormNotices.NOISendDate;
                    loadStateFormNotices.WorkPerformedMMYY = updatedStateFormNotices.WorkPerformedMMYY;
                    loadStateFormNotices.StructureType = updatedStateFormNotices.StructureType;
                    loadStateFormNotices.AmendmentsToContractAmt = updatedStateFormNotices.AmendmentsToContractAmt;
                    loadStateFormNotices.AgreedUponCreditsAmt = updatedStateFormNotices.AgreedUponCreditsAmt;
                    loadStateFormNotices.ContractPaymentsAmt = updatedStateFormNotices.ContractPaymentsAmt;
                    loadStateFormNotices.RecorderNum = updatedStateFormNotices.RecorderNum;
                    loadStateFormNotices.AmendmentPaymentsAmt = updatedStateFormNotices.AmendmentPaymentsAmt;
                    loadStateFormNotices.BlockNumber = updatedStateFormNotices.BlockNumber;
                    loadStateFormNotices.LotNumber = updatedStateFormNotices.LotNumber;
                    loadStateFormNotices.DueDate = updatedStateFormNotices.DueDate;
                    loadStateFormNotices.DescribePublicContract = updatedStateFormNotices.DescribePublicContract;
                    loadStateFormNotices.DaysCreditIsExtended = updatedStateFormNotices.DaysCreditIsExtended;
                    loadStateFormNotices.NoticeofContractDate = updatedStateFormNotices.NoticeofContractDate;
                    loadStateFormNotices.JobCounty = updatedStateFormNotices.JobCounty;
                    loadStateFormNotices.DateNoticeSent = updatedStateFormNotices.DateNoticeSent;
                    loadStateFormNotices.EndDate = updatedStateFormNotices.EndDate;
                    loadStateFormNotices.PageNum = updatedStateFormNotices.PageNum;
                    loadStateFormNotices.BondNumber = updatedStateFormNotices.BondNumber;
                    loadStateFormNotices.RegisteredAgent = updatedStateFormNotices.RegisteredAgent;
                    loadStateFormNotices.AltClaimantContact = updatedStateFormNotices.AltClaimantContact;
                    loadStateFormNotices.AltClaimantAdd1 = updatedStateFormNotices.AltClaimantAdd1;
                    loadStateFormNotices.AltClaimantAdd2 = updatedStateFormNotices.AltClaimantAdd2;
                    loadStateFormNotices.AltClaimantCity = updatedStateFormNotices.AltClaimantCity;
                    loadStateFormNotices.AltClaimantState = updatedStateFormNotices.AltClaimantState;
                    loadStateFormNotices.AltClaimantZip = updatedStateFormNotices.AltClaimantZip;
                    loadStateFormNotices.CRFView = updatedStateFormNotices.CRFView;
                    loadStateFormNotices.APNNum = updatedStateFormNotices.APNNum;
                    StateFormNotices objStateFormNotices = AutoMapper.Mapper.Map<StateFormNotices>(loadStateFormNotices);
                    tblStateFormNotices = (ITable)objStateFormNotices;
                    DBO.Provider.Update(ref tblStateFormNotices);
                    return true;
                }
                else
                {
                    StateFormNotices objStateFormNotices = AutoMapper.Mapper.Map<StateFormNotices>(updatedStateFormNotices);
                    tblStateFormNotices = (ITable)objStateFormNotices;
                    DBO.Provider.Create(ref tblStateFormNotices);
                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }
        #endregion
    }
}