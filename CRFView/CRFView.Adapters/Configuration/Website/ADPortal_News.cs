﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADPortal_News
    {
        #region Properties
        public int PKID { get; set; }
        public string title { get; set; }
        public string shortdescription { get; set; }
        public string description { get; set; }
        public string imageurl { get; set; }
        public string imagealttext { get; set; }
        public int imageid { get; set; }
        public bool isvisible { get; set; }
        public bool isemployeeprofile { get; set; }
        public bool isclientview { get; set; }
        public bool ismainsite { get; set; }
        public DateTime datecreated { get; set; }
        public bool ishomepage { get; set; }
        //additional property for view
        public byte[] image { get; set; }
        public string uniqueid { get; set; }
        #endregion

        #region CRUD Operations
        //List of Portal_News
        public List<ADPortal_News> ListNews()
        {
            Portal_News EntityView = new Portal_News();
            TableView EntityList;
            try
            {
                Query query = new Query(EntityView);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADPortal_News> CIList = (from DataRow dr in dt.Rows
                                           select new ADPortal_News()
                                           {
                                               PKID = Convert.ToInt32(dr["PKID"]),
                                               title = Convert.ToString(dr["title"]),
                                               shortdescription = Convert.ToString(dr["shortdescription"])
                                           }).ToList();
                return CIList;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<ADPortal_News>());
            }
        }

        //Read Details from database
        public ADPortal_News ReadNews(int id)
        {
            Portal_News myentity = new Portal_News();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (Portal_News)myentityItable;
            AutoMapper.Mapper.CreateMap<Portal_News, ADPortal_News>();
            ADPortal_News objADPortal_News = AutoMapper.Mapper.Map<ADPortal_News>(myentity);
            ADPortal_Image objADPortal_Image = new ADPortal_Image();
            objADPortal_Image = objADPortal_Image.ReadImage(objADPortal_News.imageid);
            objADPortal_News.image = objADPortal_Image.Image;
            objADPortal_News.uniqueid = objADPortal_Image.UniqueId;
            return (objADPortal_News);
        }

        ////Save(Create or Update) Portal_News Details
        
        public bool SaveNews(ADPortal_News updatedNews)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADPortal_News, Portal_News>();
                ITable tblNews;
                Portal_News objNews;

                ADPortal_Image objPortal_Image = new ADPortal_Image();
                objPortal_Image.PKID = updatedNews.imageid;
                objPortal_Image.Image = updatedNews.image;
                objPortal_Image.UniqueId = DBO.GetUniqueId();
                updatedNews.imageid= objPortal_Image.SaveImage(objPortal_Image);

                if (updatedNews.PKID != 0)
                {
                    ADPortal_News loadNews = ReadNews(updatedNews.PKID);
                    loadNews.title = updatedNews.title;
                    loadNews.shortdescription = updatedNews.shortdescription;
                    loadNews.description = updatedNews.description;
                    loadNews.datecreated = updatedNews.datecreated;
                    loadNews.isvisible = updatedNews.isvisible;
                    loadNews.isclientview = updatedNews.isclientview;
                    loadNews.ismainsite = updatedNews.ismainsite;
                    loadNews.isemployeeprofile = updatedNews.isemployeeprofile;
                    loadNews.ishomepage = updatedNews.ishomepage;
                    loadNews.isemployeeprofile = updatedNews.isemployeeprofile;
                    loadNews.imageid = updatedNews.imageid;
                   objNews = AutoMapper.Mapper.Map<Portal_News>(loadNews);
                    tblNews = (ITable)objNews;
                    DBO.Provider.Update(ref tblNews);
                    
                }
                else
                {
                   objNews = AutoMapper.Mapper.Map<Portal_News>(updatedNews);
                    tblNews = (ITable)objNews;
                    DBO.Provider.Create(ref tblNews);
                }
                    
                           
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }
        #endregion
    }
}