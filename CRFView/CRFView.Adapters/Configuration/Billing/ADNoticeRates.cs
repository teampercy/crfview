﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADNoticeRates
    {
        #region Properties
        public int Id { get; set; }
        public string PriceCode1 { get; set; }
        public string PriceCodeName { get; set; }
        public bool IsAllInclusivePrice { get; set; }
        public decimal PrelimPercentage { get; set; }
        public bool UsePrelimVolume { get; set; }
        public int BillableEventId { get; set; }
        public bool IsFreeFormPricing { get; set; }
        #endregion

        #region CRUD Operations
        //List of PriceCode
        public List<ADNoticeRates> ListNoticeRates()
        {
            PriceCode myentity = new PriceCode();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADNoticeRates> CTList =  (from DataRow dr in dt.Rows
                          select new ADNoticeRates()
                          {
                              Id = Convert.ToInt32(dr["Id"]),
                              PriceCodeName = Convert.ToString(dr["PriceCodeName"]),
                              UsePrelimVolume = ((dr["UsePrelimVolume"] != DBNull.Value ? Convert.ToBoolean(dr["UsePrelimVolume"]) : false)),
                              IsAllInclusivePrice = ((dr["IsAllInclusivePrice"] != DBNull.Value ? Convert.ToBoolean(dr["IsAllInclusivePrice"]) : false)),
                              IsFreeFormPricing = ((dr["IsFreeFormPricing"] != DBNull.Value ? Convert.ToBoolean(dr["IsFreeFormPricing"]) : false)),
                              PrelimPercentage = ((dr["PrelimPercentage"] != DBNull.Value ? Convert.ToDecimal(dr["PrelimPercentage"]) : 0))
                          }).ToList();

                return CTList;
            }
            catch(Exception ex)
            {
                return (new List<ADNoticeRates>());
            }
        }

        //Read Details from database
        public ADNoticeRates ReadNoticeRates(int id)
        {
            PriceCode myentity = new PriceCode();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (PriceCode)myentityItable;
            AutoMapper.Mapper.CreateMap<PriceCode, ADNoticeRates>();
            return (AutoMapper.Mapper.Map<ADNoticeRates>(myentity));
        }

        //Save(Create or Update) PriceCode Details
        public void SaveNoticeRates(ADNoticeRates updatedNoticeRates)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADNoticeRates, PriceCode>();
                ITable tblNoticeRates;
                if (updatedNoticeRates.Id != 0)
                {
                    ADNoticeRates loadNoticeRates = ReadNoticeRates(updatedNoticeRates.Id);
                    loadNoticeRates.PriceCodeName = updatedNoticeRates.PriceCodeName;
                    loadNoticeRates.BillableEventId = updatedNoticeRates.BillableEventId;
                    loadNoticeRates.IsAllInclusivePrice = updatedNoticeRates.IsAllInclusivePrice;
                    loadNoticeRates.UsePrelimVolume = updatedNoticeRates.UsePrelimVolume;
                    PriceCode objNoticeRates = AutoMapper.Mapper.Map<PriceCode>(loadNoticeRates);
                    tblNoticeRates = objNoticeRates;
                    DBO.Provider.Update(ref tblNoticeRates);
                }
                else
                {
                    PriceCode objNoticeRates = AutoMapper.Mapper.Map<PriceCode>(updatedNoticeRates);
                    tblNoticeRates = objNoticeRates;
                    DBO.Provider.Create(ref tblNoticeRates);

                }
            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion
    }
}