﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADCollectionRateItems
    {
        #region Properties
        public int CollectionRateItemId { get; set; }
        public int CollectionRateId { get; set; }
        public int CommCode { get; set; }
        public decimal SecondRate { get; set; }
        public decimal SkipRate { get; set; }
        public decimal AgeRate { get; set; }
        public int AgeThreshold { get; set; }
        public decimal AsgBalRate { get; set; }
        public decimal AsgBalThreshold { get; set; }
        public string CommType { get; set; }
        public decimal BalFrom { get; set; }
        public decimal BalThru { get; set; }
        public decimal CommRate { get; set; }
        public decimal JudgmentRate { get; set; }
        public decimal IntlRate { get; set; }
        #endregion

        #region CRUD Operations
        //List of CollectionRateItem
        public List<ADCollectionRateItems> ListCollectionRateItems()
        {
            CollectionRateItem myentity = new CollectionRateItem();
            TableView EntityList;
            try
            {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADCollectionRateItems> CRIList = (from DataRow dr in dt.Rows
                                                      select new ADCollectionRateItems()
                                                      {
                                                          CollectionRateItemId = Convert.ToInt32(dr["CollectionRateItemId"]),
                                                          CommCode = Convert.ToInt32(dr["CommCode"]),
                                                          SecondRate = ((dr["SecondRate"] != DBNull.Value ? Convert.ToDecimal(dr["SecondRate"]) : 0)),
                                                          SkipRate = ((dr["SkipRate"] != DBNull.Value ? Convert.ToDecimal(dr["SkipRate"]) : 0)),
                                                          AsgBalRate = ((dr["AsgBalRate"] != DBNull.Value ? Convert.ToDecimal(dr["AsgBalRate"]) : 0)),
                                                          AgeThreshold = Convert.ToInt32(dr["AgeThreshold"]),
                                                          AgeRate = ((dr["AgeRate"] != DBNull.Value ? Convert.ToDecimal(dr["AgeRate"]) : 0)),
                                                          AsgBalThreshold = ((dr["AsgBalThreshold"] != DBNull.Value ? Convert.ToDecimal(dr["AsgBalThreshold"]) : 0)),
                                                          JudgmentRate = ((dr["JudgmentRate"] != DBNull.Value ? Convert.ToDecimal(dr["JudgmentRate"]) : 0)),
                                                          IntlRate = ((dr["IntlRate"] != DBNull.Value ? Convert.ToDecimal(dr["IntlRate"]) : 0)),
                                                          CommType = Convert.ToString(dr["CommType"]),
                                                          BalFrom = ((dr["BalFrom"] != DBNull.Value ? Convert.ToDecimal(dr["BalFrom"]) : 0)),
                                                          BalThru = ((dr["BalThru"] != DBNull.Value ? Convert.ToDecimal(dr["BalThru"]) : 0)),
                                                          CommRate = ((dr["CommRate"] != DBNull.Value ? Convert.ToDecimal(dr["CommRate"]) : 0))
                                                      }).ToList();

                return CRIList;
            }
            catch (Exception ex)
            {
                return (new List<ADCollectionRateItems>());
            }
        }

        //Read Details from database
        public ADCollectionRateItems ReadCollectionRateItems(int id)
        {
            CollectionRateItem myentity = new CollectionRateItem();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (CollectionRateItem)myentityItable;
            AutoMapper.Mapper.CreateMap<CollectionRateItem, ADCollectionRateItems>();
            return (AutoMapper.Mapper.Map<ADCollectionRateItems>(myentity));
        }

        //Save(Create or Update) CollectionRateItem Details
        public void SaveCollectionRateItems(ADCollectionRateItems updatedCollectionRateItem)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADCollectionRateItems, CollectionRateItem>();
                ITable tblCollectionRateItem;
                if (updatedCollectionRateItem.CollectionRateItemId != 0)
                {
                    ADCollectionRateItems loadADCollectionRateItems = ReadCollectionRateItems(updatedCollectionRateItem.CollectionRateItemId);
                    loadADCollectionRateItems.CommCode = updatedCollectionRateItem.CommCode;
                    loadADCollectionRateItems.CollectionRateId = updatedCollectionRateItem.CollectionRateId;
                    loadADCollectionRateItems.SecondRate = updatedCollectionRateItem.SecondRate;
                    loadADCollectionRateItems.SkipRate = updatedCollectionRateItem.SkipRate;
                    loadADCollectionRateItems.AsgBalRate = updatedCollectionRateItem.AsgBalRate;
                    loadADCollectionRateItems.AsgBalThreshold = updatedCollectionRateItem.AsgBalThreshold;
                    loadADCollectionRateItems.AgeRate = updatedCollectionRateItem.AgeRate;
                    loadADCollectionRateItems.AgeThreshold = updatedCollectionRateItem.AgeThreshold;
                    loadADCollectionRateItems.JudgmentRate = updatedCollectionRateItem.JudgmentRate;
                    loadADCollectionRateItems.IntlRate = updatedCollectionRateItem.IntlRate;
                    loadADCollectionRateItems.CommType = updatedCollectionRateItem.CommType;
                    loadADCollectionRateItems.BalFrom = updatedCollectionRateItem.BalFrom;
                    loadADCollectionRateItems.BalThru = updatedCollectionRateItem.BalThru;
                    loadADCollectionRateItems.CommRate = updatedCollectionRateItem.CommRate;
                    CollectionRateItem objCollectionRateItem = AutoMapper.Mapper.Map <CollectionRateItem>(loadADCollectionRateItems);
                    tblCollectionRateItem = objCollectionRateItem;
                    DBO.Provider.Update(ref tblCollectionRateItem);
                }
                else
                {
                    CollectionRateItem objCollectionRateItem = AutoMapper.Mapper.Map<CollectionRateItem>(updatedCollectionRateItem);
                    tblCollectionRateItem = objCollectionRateItem;
                    DBO.Provider.Create(ref tblCollectionRateItem);

                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
    }
}