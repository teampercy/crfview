﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADCollectionRates
    {
        #region Properties
        public int Id { get; set; }
        public string CollectionRateCode { get; set; }
        public string CollectionRateName { get; set; }
        #endregion

        #region CRUD Operations
        //List of CollectionRate
        public List<ADCollectionRates> ListCollectionRates()
        {
            CollectionRate myentity = new CollectionRate();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADCollectionRates> CTList =  (from DataRow dr in dt.Rows
                          select new ADCollectionRates()
                          {
                              Id = Convert.ToInt32(dr["Id"]),
                              CollectionRateCode = Convert.ToString(dr["CollectionRateCode"]),
                              CollectionRateName = Convert.ToString(dr["CollectionRateName"])
                          }).ToList();

                return CTList;
            }
            catch(Exception ex)
            {
                return (new List<ADCollectionRates>());
            }
        }

        //Read Details from database
        public ADCollectionRates ReadADCollectionRates(int id)
        {
            CollectionRate myentity = new CollectionRate();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (CollectionRate)myentityItable;
            AutoMapper.Mapper.CreateMap<CollectionRate, ADCollectionRates>();
            return (AutoMapper.Mapper.Map<ADCollectionRates>(myentity));
        }

        //Save(Create or Update) CollectionRate Details
        public void SaveCollectionRates(ADCollectionRates updatedCollectionRate)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADCollectionRates, CollectionRate>();
                ITable tblCollectionRate;
                if (updatedCollectionRate.Id != 0)
                {
                    ADCollectionRates loadADCollectionRates = ReadADCollectionRates(updatedCollectionRate.Id);
                    loadADCollectionRates.CollectionRateName = updatedCollectionRate.CollectionRateName;
                    loadADCollectionRates.CollectionRateCode = updatedCollectionRate.CollectionRateCode; 
                    CollectionRate objCollectionRate = AutoMapper.Mapper.Map<CollectionRate>(loadADCollectionRates);
                    tblCollectionRate = objCollectionRate;
                    DBO.Provider.Update(ref tblCollectionRate);
                }
                else
                {
                    CollectionRate objCollectionRate = AutoMapper.Mapper.Map<CollectionRate>(updatedCollectionRate);
                    tblCollectionRate = objCollectionRate;
                    DBO.Provider.Create(ref tblCollectionRate);

                }
            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion
    }
}