﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADPriority
    {
        #region Properties
        public int Id { get; set; }
        public string PriorityName { get; set; }
        public string PriorityCode { get; set; }

        #endregion

        #region CRUD Operations
        //List of PriorityTypes
        public List<ADPriority> ListPriority()
        {
            PriorityTypes myentity = new PriorityTypes();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADPriority> CTList =  (from DataRow dr in dt.Rows
                          select new ADPriority()
                          {
                              Id = Convert.ToInt32(dr["Id"]),
                              PriorityName = Convert.ToString(dr["PriorityName"]),
                              PriorityCode = Convert.ToString(dr["PriorityCode"])
                          }).ToList();

                return CTList;
            }
            catch(Exception ex)
            {
                return (new List<ADPriority>());
            }
        }

        //Read Details from database
        public ADPriority ReadPriority(int id)
        {
            PriorityTypes myentity = new PriorityTypes();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (PriorityTypes)myentityItable;
            AutoMapper.Mapper.CreateMap<PriorityTypes, ADPriority>();
            return (AutoMapper.Mapper.Map<ADPriority>(myentity));
        }

        //Save(Create or Update) PriorityTypes Details
        public void SavePriority(ADPriority updatedPriorityTypes)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADPriority, PriorityTypes>();
                ITable tblPriorityTypes;
       
                if (updatedPriorityTypes.Id != 0)
                {
                    ADPriority loadPriority = ReadPriority(updatedPriorityTypes.Id);
                    loadPriority.PriorityName = updatedPriorityTypes.PriorityName;
                    loadPriority.PriorityCode = updatedPriorityTypes.PriorityCode;
                   
                    PriorityTypes objPriorityTypes = AutoMapper.Mapper.Map<PriorityTypes>(loadPriority);
                    tblPriorityTypes = objPriorityTypes;
                    DBO.Provider.Update(ref tblPriorityTypes);
                }
                else
                {
                    PriorityTypes objPriorityTypes = AutoMapper.Mapper.Map<PriorityTypes>(updatedPriorityTypes);
                    tblPriorityTypes = objPriorityTypes;
                    DBO.Provider.Create(ref tblPriorityTypes);

                }
            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion
    }
}