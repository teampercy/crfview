﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADAddressType
    {
        #region Properties
        public int Id { get; set; }
        public string AddressType { get; set; }
        #endregion

        #region CRUD Operations
        //List of AddressTypes
        public List<ADAddressType> ListAddressType()
        {
            AddressTypes myentity = new AddressTypes();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADAddressType> CTList =  (from DataRow dr in dt.Rows
                          select new ADAddressType()
                          {
                              Id = Convert.ToInt32(dr["Id"]),
                              AddressType = Convert.ToString(dr["AddressType"])
                             
                          }).ToList();

                return CTList;
            }
            catch(Exception ex)
            {
                return (new List<ADAddressType>());
            }
        }

        //Read Details from database
        public ADAddressType ReadAddressType(int id)
        {
            AddressTypes myentity = new AddressTypes();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (AddressTypes)myentityItable;
            AutoMapper.Mapper.CreateMap<AddressTypes, ADAddressType>();
            return (AutoMapper.Mapper.Map<ADAddressType>(myentity));
        }

        //Save(Create or Update) AddressTypes Details
        public void SaveAddressType(ADAddressType updatedAddressType)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADAddressType, AddressTypes>();
                ITable tblAddressType;
                if (updatedAddressType.Id != 0)
                {
                    ADAddressType loadAddressType = ReadAddressType(updatedAddressType.Id);
                    loadAddressType.AddressType = updatedAddressType.AddressType;
                     AddressTypes objAddressType = AutoMapper.Mapper.Map<AddressTypes>(loadAddressType);
                    tblAddressType = objAddressType;
                    DBO.Provider.Update(ref tblAddressType);
                }
                else
                {
                    AddressTypes objAddressType = AutoMapper.Mapper.Map<AddressTypes>(updatedAddressType);
                    tblAddressType = objAddressType;
                    DBO.Provider.Create(ref tblAddressType);

                }
            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion
    }
}