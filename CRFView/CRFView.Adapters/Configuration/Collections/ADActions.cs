﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using CRF.BLL.CRFDB.VIEWS;

namespace CRFView.Adapters
{
    public class ADActions
    {
        #region Properties
        public int Id { get; set; }
        public string Description { get; set; }
        public int NextCall { get; set; }
        public bool IsCollection { get; set; }
        public bool IsForwarding { get; set; }
        public bool IsChangeStatus { get; set; }
        public bool IsChangeDesk { get; set; }
        public int NewStatusId { get; set; }
        public int NewDeskId { get; set; }
        public bool IsAutoNotate { get; set; }
        public string HistoryNote { get; set; }
        public bool IsDropReviewDate { get; set; }
        public bool IsChangePriority { get; set; }
        public int NewPriorityId { get; set; }
        public int NextStatusDays { get; set; }
        public bool IsSystem { get; set; }
        public bool IsSmallClaims { get; set; }
        public bool IsDropLetterSeries { get; set; }
        public string CategoryType { get; set; }
        public bool AccountChangeBox { get; set; }
        public bool RevDateChangeBox { get; set; }
        public string ActionCode { get; set; }
        public bool IsChangeToDesk { get; set; }
        public int NewToDeskId { get; set; }
        public bool DeskChangeBox { get; set; }

        //Other properties
        public string Location { get; set; }
        public string CollectionStatus { get; set; }
        public string NewToDesk { get; set; }
        public string PriorityCode { get; set; }
        public string ActionType { get; set; }
        #endregion

        #region CRUD Operations
        //List of CollectionActions
        public List<ADActions> ListActions()
        {
            vwCollectionActionsList myentity = new vwCollectionActionsList();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADActions> CTList =  (from DataRow dr in dt.Rows
                          select new ADActions()
                          {
                              Id = Convert.ToInt32(dr["Id"]),
                              Description = Convert.ToString(dr["Description"]),
                              ActionCode = Convert.ToString(dr["ActionCode"]),
                              IsSystem = ((dr["IsSystem"] != DBNull.Value ? Convert.ToBoolean(dr["IsSystem"]) : false)),
                              IsCollection = ((dr["IsCollection"] != DBNull.Value ? Convert.ToBoolean(dr["IsCollection"]) : false)),
                              IsForwarding = ((dr["IsForwarding"] != DBNull.Value ? Convert.ToBoolean(dr["IsForwarding"]) : false)),
                              IsSmallClaims = ((dr["IsSmallClaims"] != DBNull.Value ? Convert.ToBoolean(dr["IsSmallClaims"]) : false)),
                              Location = Convert.ToString(dr["Location"]),
                              NewToDesk = Convert.ToString(dr["NewToDesk"]),
                              CollectionStatus = Convert.ToString(dr["CollectionStatus"]),
                              NextCall = Convert.ToInt32(dr["NextCall"]),
                              PriorityCode = Convert.ToString(dr["PriorityCode"]),
                              IsDropReviewDate = ((dr["IsDropReviewDate"] != DBNull.Value ? Convert.ToBoolean(dr["IsDropReviewDate"]) : false)),
                              AccountChangeBox = ((dr["AccountChangeBox"] != DBNull.Value ? Convert.ToBoolean(dr["AccountChangeBox"]) : false)),
                              RevDateChangeBox = ((dr["RevDateChangeBox"] != DBNull.Value ? Convert.ToBoolean(dr["RevDateChangeBox"]) : false)),
                              DeskChangeBox = ((dr["DeskChangeBox"] != DBNull.Value ? Convert.ToBoolean(dr["DeskChangeBox"]) : false)),
                              IsAutoNotate = ((dr["IsAutoNotate"] != DBNull.Value ? Convert.ToBoolean(dr["IsAutoNotate"]) : false))
                          }).ToList();

                return CTList;
            }
            catch(Exception ex)
            {
                return (new List<ADActions>());
            }
        }

        //Read Details from database
        public ADActions ReadActions(int id)
        {
            CollectionActions myentity = new CollectionActions();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (CollectionActions)myentityItable;
            AutoMapper.Mapper.CreateMap<CollectionActions, ADActions>();
            return (AutoMapper.Mapper.Map<ADActions>(myentity));
        }

        //Save(Create or Update) CollectionActions Details
        public void SaveActions(ADActions updatedActions)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADActions, CollectionActions>();
                ITable tblActions;
                if (updatedActions.IsCollection == false)
                {
                    updatedActions.CategoryType = "";
                }
                if (updatedActions.IsChangeDesk == false)
                {
                    updatedActions.NewDeskId = 0;
                }
                if (updatedActions.IsChangeToDesk == false)
                {
                    updatedActions.NewToDeskId = 0;
                }
                if (updatedActions.IsChangeStatus == false)
                {
                    updatedActions.NewStatusId = 0;
                }
                if (updatedActions.IsChangePriority == false)
                {
                    updatedActions.NewPriorityId = 0;
                }

                if (updatedActions.Id != 0)
                {
                    ADActions objADActions = ReadActions(updatedActions.Id);
                    objADActions.Description = updatedActions.Description;
                    objADActions.ActionCode = updatedActions.ActionCode;

                    objADActions.IsSmallClaims = updatedActions.IsSmallClaims;
                    objADActions.IsForwarding = updatedActions.IsForwarding;
                    objADActions.IsCollection = updatedActions.IsCollection;
                    objADActions.IsSystem = updatedActions.IsSystem;
                    objADActions.CategoryType = updatedActions.CategoryType;

                    objADActions.IsChangeDesk = updatedActions.IsChangeDesk;
                    objADActions.NewDeskId = updatedActions.NewDeskId;
                    objADActions.IsChangeToDesk = updatedActions.IsChangeToDesk;
                    objADActions.NewToDeskId = updatedActions.NewToDeskId;
                    objADActions.IsChangeStatus = updatedActions.IsChangeStatus;
                    objADActions.NewStatusId = updatedActions.NewStatusId;
                    objADActions.IsChangePriority = updatedActions.IsChangePriority;
                    objADActions.NewPriorityId = updatedActions.NewPriorityId;

                    objADActions.NextCall = updatedActions.NextCall;
                    objADActions.IsDropReviewDate = updatedActions.IsDropReviewDate;
                    objADActions.IsDropLetterSeries = updatedActions.IsDropLetterSeries;
                    objADActions.AccountChangeBox = updatedActions.AccountChangeBox;
                    objADActions.RevDateChangeBox = updatedActions.RevDateChangeBox;
                    objADActions.DeskChangeBox = updatedActions.DeskChangeBox;
                    objADActions.IsAutoNotate = updatedActions.IsAutoNotate;
                    objADActions.HistoryNote = updatedActions.HistoryNote;
                  
                    CollectionActions objActions = AutoMapper.Mapper.Map<CollectionActions>(objADActions);
                    tblActions = objActions;
                    DBO.Provider.Update(ref tblActions);
                }
                else
                {
                    CollectionActions objActions = AutoMapper.Mapper.Map<CollectionActions>(updatedActions);
                    tblActions = objActions;
                    DBO.Provider.Create(ref tblActions);

                }
            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion
    }
}