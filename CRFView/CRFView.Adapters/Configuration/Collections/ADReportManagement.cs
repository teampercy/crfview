﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using CRF.BLL.CRFDB.VIEWS;

namespace CRFView.Adapters
{
    public class ADReportManagement
    {
        #region Properties
        public int ReportId { get; set; }
        public string ReportName { get; set; }
        public string Description { get; set; }
        public int ReportFileId { get; set; }
        public int ReportCategoryId { get; set; }
        public string PrintFrom { get; set; }
        public string CustomSPName { get; set; }

        //additional property for View
        public string ReportFileName { get; set; }
        public string ReportCategory { get; set; }
        #endregion

        #region CRUD Operations
        //List of CollectionReports
        public List<ADReportManagement> ListReport()
        {
            vwCollectionReportsList myentity = new vwCollectionReportsList();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADReportManagement> RMList =  (from DataRow dr in dt.Rows
                          select new ADReportManagement()
                          {
                              ReportId = Convert.ToInt32(dr["ReportId"]),
                              ReportName = Convert.ToString(dr["ReportName"]),
                              Description = Convert.ToString(dr["Description"]),
                              ReportCategory = Convert.ToString(dr["ReportCategory"]),
                              PrintFrom = Convert.ToString(dr["PrintFrom"]),
                              CustomSPName = Convert.ToString(dr["CustomSPName"]),
                              ReportFileName = Convert.ToString(dr["ReportFileName"])
                          }).ToList();

                return RMList;
            }
            catch(Exception ex)
            {
                return (new List<ADReportManagement>());
            }
        }

        //Read Details from database
        public ADReportManagement ReadReport(int id)
        {
            CollectionReports myentity = new CollectionReports();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (CollectionReports)myentityItable;
            AutoMapper.Mapper.CreateMap<CollectionReports, ADReportManagement>();
            return (AutoMapper.Mapper.Map<ADReportManagement>(myentity));
        }

        //Save(Create or Update) CollectionReports Details
        public void SaveReport(ADReportManagement updatedReport)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADReportManagement, CollectionReports>();
                ITable tblReport;
       
                if (updatedReport.ReportId != 0)
                {
                    ADReportManagement loadReport = ReadReport(updatedReport.ReportId);
                    loadReport.ReportName = updatedReport.ReportName;
                    loadReport.Description = updatedReport.Description;
                    loadReport.PrintFrom = updatedReport.PrintFrom;
                    loadReport.CustomSPName = updatedReport.CustomSPName;
                    loadReport.ReportFileId = updatedReport.ReportFileId;
                    loadReport.ReportCategoryId = updatedReport.ReportCategoryId;
                    CollectionReports objReport = AutoMapper.Mapper.Map<CollectionReports>(loadReport);
                    tblReport = objReport;
                    DBO.Provider.Update(ref tblReport);
                }
                else
                {
                    CollectionReports objReport = AutoMapper.Mapper.Map<CollectionReports>(updatedReport);
                    tblReport = objReport;
                    DBO.Provider.Create(ref tblReport);

                }
            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion
    }
}