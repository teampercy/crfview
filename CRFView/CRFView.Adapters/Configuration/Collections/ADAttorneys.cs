﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADAttorneys
    {
        #region Properties
        public int PKID { get; set; }
        public string AttyCode { get; set; }
        public string FirmName { get; set; }
        public string AttorneyName { get; set; }
        public string AttyAs { get; set; }
        public string AttyAddress { get; set; }
        public string AttyCity { get; set; }
        public string AttyState { get; set; }
        public string AttyZip { get; set; }
        public string AttyPhoneNo { get; set; }
        public string AttyFax { get; set; }
        public string LawList { get; set; }
        public string AttyNote { get; set; }
        public string EMail { get; set; }
        public string TaxID { get; set; }
        public int LawListId { get; set; }
        //additional Property for View
        public string LawListValue { get; set; }
        #endregion

        #region CRUD Operations
        //List of AttorneyInfo
        public List<ADAttorneys> ListAttorney()
        {
            AttorneyInfo myentity = new AttorneyInfo();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADAttorneys> CTList =  (from DataRow dr in dt.Rows
                          select new ADAttorneys()
                          {
                              PKID = Convert.ToInt32(dr["PKID"]),
                              AttyCode = Convert.ToString(dr["AttyCode"]),
                              LawList = Convert.ToString(dr["LawList"]),
                              AttorneyName = Convert.ToString(dr["AttorneyName"]),
                              FirmName = Convert.ToString(dr["FirmName"])
                          }).ToList();

                return CTList;
            }
            catch(Exception ex)
            {
                return (new List<ADAttorneys>());
            }
        }

        //Read Details from database
        public ADAttorneys ReadAttorney(int id)
        {
            AttorneyInfo myentity = new AttorneyInfo();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (AttorneyInfo)myentityItable;
            AutoMapper.Mapper.CreateMap<AttorneyInfo, ADAttorneys>();
            return (AutoMapper.Mapper.Map<ADAttorneys>(myentity));
        }

        //Save(Create or Update) AttorneyInfo Details
        public void SaveAttorney(ADAttorneys updatedAttorney)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADAttorneys, AttorneyInfo>();
                ITable tblAttorney;
       
                if (updatedAttorney.PKID != 0)
                {
                    ADAttorneys loadAttorney = ReadAttorney(updatedAttorney.PKID);
                    loadAttorney.AttyCode = updatedAttorney.AttyCode;
                    loadAttorney.LawList = updatedAttorney.LawList;
                    loadAttorney.AttorneyName = updatedAttorney.AttorneyName;
                    loadAttorney.FirmName = updatedAttorney.FirmName;
                    loadAttorney.AttyAs = updatedAttorney.AttyAs;
                    loadAttorney.AttyAddress = updatedAttorney.AttyAddress;
                    loadAttorney.AttyCity = updatedAttorney.AttyCity;
                    loadAttorney.AttyState = updatedAttorney.AttyState;
                    loadAttorney.AttyZip = updatedAttorney.AttyZip;
                    loadAttorney.AttyPhoneNo = updatedAttorney.AttyPhoneNo;
                    loadAttorney.AttyFax = updatedAttorney.AttyFax;
                    loadAttorney.EMail = updatedAttorney.EMail;
                    loadAttorney.TaxID = updatedAttorney.TaxID;
                    loadAttorney.AttyNote = updatedAttorney.AttyNote;
                    loadAttorney.LawListId = updatedAttorney.LawListId;
                    AttorneyInfo objAttorney = AutoMapper.Mapper.Map<AttorneyInfo>(loadAttorney);
                    tblAttorney = objAttorney;
                    DBO.Provider.Update(ref tblAttorney);
                }
                else
                {
                    AttorneyInfo objAttorney = AutoMapper.Mapper.Map<AttorneyInfo>(updatedAttorney);
                    tblAttorney = objAttorney;
                    DBO.Provider.Create(ref tblAttorney);

                }
            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion
    }
}