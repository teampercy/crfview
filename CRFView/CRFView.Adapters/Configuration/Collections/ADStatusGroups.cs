﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADStatusGroups
    {
        #region Properties
        public int Id { get; set; }
        public string StatusGroupDescription { get; set; }
        public bool IncludeInCompletion { get; set; }
        public bool IsActive { get; set; }
        public bool IsPaid { get; set; }
        public bool IsFowarding { get; set; }
        public bool IsSmallClaims { get; set; }
        public bool IsClosed { get; set; }
        #endregion

        #region CRUD Operations
        //List of CollectionStatusGroup
        public List<ADStatusGroups> ListStatusGroups()
        {
            CollectionStatusGroup myentity = new CollectionStatusGroup();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADStatusGroups> CTList =  (from DataRow dr in dt.Rows
                          select new ADStatusGroups()
                          {
                              Id = Convert.ToInt32(dr["Id"]),
                              StatusGroupDescription = Convert.ToString(dr["StatusGroupDescription"])
                          }).ToList();

                return CTList;
            }
            catch(Exception ex)
            {
                return (new List<ADStatusGroups>());
            }
        }

        //Read Details from database
        public ADStatusGroups ReadStatusGroups(int id)
        {
            CollectionStatusGroup myentity = new CollectionStatusGroup();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (CollectionStatusGroup)myentityItable;
            AutoMapper.Mapper.CreateMap<CollectionStatusGroup, ADStatusGroups>();
            return (AutoMapper.Mapper.Map<ADStatusGroups>(myentity));
        }

        //Save(Create or Update) CollectionStatusGroup Details
        public void SaveStatusGroups(ADStatusGroups updatedCollectionStatusGroup)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADStatusGroups, CollectionStatusGroup>();
                ITable tblCollectionStatusGroup;
       
                if (updatedCollectionStatusGroup.Id != 0)
                {
                    ADStatusGroups loadStatusGroups = ReadStatusGroups(updatedCollectionStatusGroup.Id);
                    loadStatusGroups.StatusGroupDescription = updatedCollectionStatusGroup.StatusGroupDescription;
                    CollectionStatusGroup objCollectionStatusGroup = AutoMapper.Mapper.Map<CollectionStatusGroup>(loadStatusGroups);
                    tblCollectionStatusGroup = objCollectionStatusGroup;
                    DBO.Provider.Update(ref tblCollectionStatusGroup);
                }
                else
                {
                    CollectionStatusGroup objCollectionStatusGroup = AutoMapper.Mapper.Map<CollectionStatusGroup>(updatedCollectionStatusGroup);
                    tblCollectionStatusGroup = objCollectionStatusGroup;
                    DBO.Provider.Create(ref tblCollectionStatusGroup);

                }
            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion
    }
}