﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADLawLists
    {
        #region Properties
        public int PKID { get; set; }
        public string LawList { get; set; }
        public string LawAdd { get; set; }
        public string LawCity { get; set; }
        public string LawSt { get; set; }
        public string LawZip { get; set; }
        public string LawListCode { get; set; }

        #endregion

        #region CRUD Operations
        //List of LawListInfo
        public List<ADLawLists> ListLawLists()
        {
            LawListInfo myentity = new LawListInfo();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADLawLists> CTList =  (from DataRow dr in dt.Rows
                          select new ADLawLists()
                          {
                              PKID = Convert.ToInt32(dr["PKID"]),
                              LawListCode = Convert.ToString(dr["LawListCode"]),
                              LawList = Convert.ToString(dr["LawList"])
                          }).ToList();

                return CTList;
            }
            catch(Exception ex)
            {
                return (new List<ADLawLists>());
            }
        }

        //Read Details from database
        public ADLawLists ReadLawLists(int id)
        {
            LawListInfo myentity = new LawListInfo();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (LawListInfo)myentityItable;
            AutoMapper.Mapper.CreateMap<LawListInfo, ADLawLists>();
            return (AutoMapper.Mapper.Map<ADLawLists>(myentity));
        }

        //Save(Create or Update) LawListInfo Details
        public void SaveLawLists(ADLawLists updatedLawLists)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADLawLists, LawListInfo>();
                ITable tblLawLists;
       
                if (updatedLawLists.PKID != 0)
                {
                    ADLawLists loadLawLists = ReadLawLists(updatedLawLists.PKID);
                    loadLawLists.LawListCode = updatedLawLists.LawListCode;
                    loadLawLists.LawList = updatedLawLists.LawList;
                    loadLawLists.LawAdd = updatedLawLists.LawAdd;
                    loadLawLists.LawCity = updatedLawLists.LawCity;
                    loadLawLists.LawSt = updatedLawLists.LawSt;
                    loadLawLists.LawZip = updatedLawLists.LawZip;
                    LawListInfo objLawLists = AutoMapper.Mapper.Map<LawListInfo>(loadLawLists);
                    tblLawLists = objLawLists;
                    DBO.Provider.Update(ref tblLawLists);
                }
                else
                {
                    LawListInfo objLawLists = AutoMapper.Mapper.Map<LawListInfo>(updatedLawLists);
                    tblLawLists = objLawLists;
                    DBO.Provider.Create(ref tblLawLists);

                }
            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion
    }
}