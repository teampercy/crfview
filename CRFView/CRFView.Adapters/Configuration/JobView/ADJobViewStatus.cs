﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using CRF.BLL.CRFDB.VIEWS;

namespace CRFView.Adapters
{
    public class ADJobViewStatus
    {
        #region Properties
        public int Id { get; set; }
        public string JobviewStatus { get; set; }
        public string Description { get; set; }
        public int StatusGroup { get; set; }
        public bool IsManualStatus { get; set; }
        public bool IsNoticeRequestStatus { get; set; }
        public bool IsClosed { get; set; }
        //Additional properties for view
        public string StatusGroupDescription { get; set; }
        #endregion

        #region CRUD Operations

        JobViewStatus myentity;
        //List of JobViewStatus
        public List<ADJobViewStatus> ListJobViewStatus()
        {

            vwJobViewStatusList EntityView = new vwJobViewStatusList();
            TableView EntityList;
            try
            {
                Query query = new Query(EntityView);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADJobViewStatus> List = (from DataRow dr in dt.Rows
                                             select new ADJobViewStatus()
                                             {
                                                 Id = Convert.ToInt16(dr["Id"]),
                                                 JobviewStatus = Convert.ToString(dr["JobviewStatus"]),
                                                 Description = Convert.ToString(dr["Description"]),
                                                 StatusGroupDescription = Convert.ToString(dr["StatusGroupDescription"]),
                                                 IsManualStatus = dr["IsManualStatus"] != DBNull.Value ? Convert.ToBoolean(dr["IsManualStatus"]):false,
                                                 IsNoticeRequestStatus = dr["IsNoticeRequestStatus"] != DBNull.Value ? Convert.ToBoolean(dr["IsNoticeRequestStatus"]):false,
                                                 IsClosed = dr["IsClosed"] != DBNull.Value ? Convert.ToBoolean(dr["IsClosed"]):false
                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                return (new List<ADJobViewStatus>());
            }
        }

        //Read Details from database
        public ADJobViewStatus ReadJobViewStatus(int id)
        {
            JobViewStatus myentity = new JobViewStatus();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (JobViewStatus)myentityItable;
            AutoMapper.Mapper.CreateMap<JobViewStatus, ADJobViewStatus>();
            return (AutoMapper.Mapper.Map<ADJobViewStatus>(myentity));
        }

        //Save(Create or Update) JobViewStatus Details
        public bool SaveJobViewStatus(ADJobViewStatus updatedJobViewStatus)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADJobViewStatus, JobViewStatus>();
                ITable tblJobViewStatus;
                if (updatedJobViewStatus.Id != 0)
                {
                    ADJobViewStatus loadJobViewStatus = ReadJobViewStatus(updatedJobViewStatus.Id);
                    loadJobViewStatus.JobviewStatus = updatedJobViewStatus.JobviewStatus;
                    loadJobViewStatus.Description = updatedJobViewStatus.Description;
                    loadJobViewStatus.StatusGroup = updatedJobViewStatus.StatusGroup;
                    loadJobViewStatus.IsManualStatus = updatedJobViewStatus.IsManualStatus;
                    loadJobViewStatus.IsNoticeRequestStatus = updatedJobViewStatus.IsNoticeRequestStatus;
                    loadJobViewStatus.IsClosed = updatedJobViewStatus.IsClosed;
                    JobViewStatus objJobViewStatus= AutoMapper.Mapper.Map<JobViewStatus>(loadJobViewStatus);
                     tblJobViewStatus= objJobViewStatus;
                    DBO.Provider.Update(ref tblJobViewStatus);
                }
                else
                {
                    JobViewStatus objJobViewStatus= AutoMapper.Mapper.Map<JobViewStatus>(updatedJobViewStatus);
                    tblJobViewStatus= objJobViewStatus;
                    DBO.Provider.Create(ref tblJobViewStatus);
                }
            }
            catch(Exception ex)
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}