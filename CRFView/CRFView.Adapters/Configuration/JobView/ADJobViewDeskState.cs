﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADJobViewDeskState
    {
        #region Properties
        public int Id { get; set; }
        public int JobViewDeskId { get; set; }
        public int JobStateId { get; set; }
        public string JobState { get; set; }
        public string JobViewDeskNum { get; set; }
        #endregion

        #region CRUD Operations

        JobViewDeskStateInfo myentity;
        //List of JobViewDeskStateInfo
        public List<ADJobViewDeskState> ListJobViewDeskState()
        {
          
            myentity = new JobViewDeskStateInfo();
            TableView EntityList;
            try
            {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADJobViewDeskState> CAList = (from DataRow dr in dt.Rows
                                             select new ADJobViewDeskState()
                                             {
                                                 Id = Convert.ToInt16(dr["Id"]),
                                                 JobState = Convert.ToString(dr["JobState"]),
                                                 JobViewDeskNum = Convert.ToString(dr["JobViewDeskNum"])
                                             }).ToList();

                return CAList;
            }
            catch (Exception ex)
            {
                return (new List<ADJobViewDeskState>());
            }
        }

        //Read Details from database
        public ADJobViewDeskState ReadJobViewDeskState(int id)
        {
            JobViewDeskStateInfo myentity = new JobViewDeskStateInfo();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (JobViewDeskStateInfo)myentityItable;
            AutoMapper.Mapper.CreateMap<JobViewDeskStateInfo, ADJobViewDeskState>();
            return (AutoMapper.Mapper.Map<ADJobViewDeskState>(myentity));
        }

        //Save(Create or Update) JobViewDeskStateInfo Details
        public bool SaveJobViewDeskState(ADJobViewDeskState updatedJobViewDeskState)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADJobViewDeskState, JobViewDeskStateInfo>();
                ITable tblJobViewDeskState;
                if (updatedJobViewDeskState.Id != 0)
                {
                    ADJobViewDeskState loadJobViewDeskState = ReadJobViewDeskState(updatedJobViewDeskState.Id);
                    loadJobViewDeskState.JobViewDeskId = updatedJobViewDeskState.JobViewDeskId;
                    loadJobViewDeskState.JobStateId = updatedJobViewDeskState.JobStateId;
                    loadJobViewDeskState.JobState = updatedJobViewDeskState.JobState;
                    loadJobViewDeskState.JobViewDeskNum = updatedJobViewDeskState.JobViewDeskNum;
                    JobViewDeskStateInfo objJobViewDeskState= AutoMapper.Mapper.Map<JobViewDeskStateInfo>(loadJobViewDeskState);
                     tblJobViewDeskState= objJobViewDeskState;
                    DBO.Provider.Update(ref tblJobViewDeskState);
                }
                else
                {
                    JobViewDeskStateInfo objJobViewDeskState= AutoMapper.Mapper.Map<JobViewDeskStateInfo>(updatedJobViewDeskState);
                    tblJobViewDeskState= objJobViewDeskState;
                    DBO.Provider.Create(ref tblJobViewDeskState);
                }
            }
            catch(Exception ex)
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}