﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADContractType
    {
        #region Properties
        public int Id { get; set; }
        public string ContractTypeCode { get; set; }
        public string ContractTypeName { get; set; }
        public bool IsLienContract { get; set; }
        #endregion

        #region CRUD Operations
        //List of ContractType
        public List<ADContractType> ListContractType()
        {
            ContractType myentity = new ContractType();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADContractType> CTList = (from DataRow dr in dt.Rows
                          select new ADContractType()
                          {
                              Id = Convert.ToInt16(dr["Id"]),
                              ContractTypeCode = Convert.ToString(dr["ContractTypeCode"]),
                              ContractTypeName = Convert.ToString(dr["ContractTypeName"])

                          }).ToList();

                return CTList;
            }
            catch(Exception ex)
            {
                return (new List<ADContractType>());
            }
        }

        //Read Details from database
        public ADContractType ReadContractType(int id)
        {
            ContractType myentity = new ContractType();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (ContractType)myentityItable;
            AutoMapper.Mapper.CreateMap<ContractType, ADContractType>();
            return (AutoMapper.Mapper.Map<ADContractType>(myentity));
        }

        //Save(Create or Update) ContractType Details
        public void SaveContractType(ADContractType updatedContractType)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADContractType, ContractType>();
                ITable tblContractType;
                if (updatedContractType.Id != 0)
                {
                    ADContractType loadContractType = ReadContractType(updatedContractType.Id);
                    loadContractType.ContractTypeName = updatedContractType.ContractTypeName;
                    loadContractType.ContractTypeCode = updatedContractType.ContractTypeCode;
                    ContractType objContractType = AutoMapper.Mapper.Map<ContractType>(loadContractType);
                    tblContractType = objContractType;
                    DBO.Provider.Update(ref tblContractType);
                }
                else
                {
                    ContractType objContractType = AutoMapper.Mapper.Map<ContractType>(updatedContractType);
                    tblContractType = objContractType;
                    DBO.Provider.Create(ref tblContractType);

                }
            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion
    }
}