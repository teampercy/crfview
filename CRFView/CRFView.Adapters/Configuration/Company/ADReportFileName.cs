﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADReportFileName
    {
        #region Properties
        public int ReportFileId { get; set; }
        public string ReportFileName { get; set; }
        public string ReportType { get; set; }
        #endregion

        #region CRUD Operations
        //List of ReportFileNames
        public List<ADReportFileName> ListReportFileNames()
        {
            ReportFilenames myentity = new ReportFilenames();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADReportFileName> RFList =  (from DataRow dr in dt.Rows
                          select new ADReportFileName()
                          {
                              ReportFileId = Convert.ToInt16(dr["ReportFileId"]),
                              ReportFileName = Convert.ToString(dr["ReportFileName"]),
                              ReportType = Convert.ToString(dr["ReportType"])

                          }).ToList();

                return RFList;
            }
            catch(Exception ex)
            {
                return (new List<ADReportFileName>());
            }
        }

        //Read Details from database
        public ADReportFileName ReadReportFileNames(int id)
        {
            ReportFilenames myentity = new ReportFilenames();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (ReportFilenames)myentityItable;
            AutoMapper.Mapper.CreateMap<ReportFilenames, ADReportFileName>();
            return (AutoMapper.Mapper.Map<ADReportFileName>(myentity));
        }

        //Save(Create or Update) ReportFilenames Details
        public void SaveReportFilenames(ADReportFileName updatedReportFilename)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADReportFileName, ReportFilenames>();
                ITable tblReportFileName;
                if (updatedReportFilename.ReportFileId != 0)
                {
                    ADReportFileName loadReportFilename = ReadReportFileNames(updatedReportFilename.ReportFileId);
                    loadReportFilename.ReportFileName = updatedReportFilename.ReportFileName;
                    loadReportFilename.ReportType = updatedReportFilename.ReportType;
                    ReportFilenames objReportFileName = AutoMapper.Mapper.Map<ReportFilenames>(loadReportFilename);
                    tblReportFileName = objReportFileName;
                    DBO.Provider.Update(ref tblReportFileName);
                }
                else
                {
                    ReportFilenames objReportFileName = AutoMapper.Mapper.Map<ReportFilenames>(updatedReportFilename);
                    tblReportFileName = objReportFileName;
                    DBO.Provider.Create(ref tblReportFileName);

                }
            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion
    }
}