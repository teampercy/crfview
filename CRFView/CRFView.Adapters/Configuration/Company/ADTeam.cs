﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.Web.Mvc;

namespace CRFView.Adapters
{
    public class ADTeam
    {
        #region Properties
        public int Id { get; set; }
        public string TeamName { get; set; }
        public int TeamType { get; set; }
        public int ManagerId { get; set; }
        public string TeamCode { get; set; }
        public bool IsSalesTeam { get; set; }
        public bool IsServiceTeam { get; set; }
        public string UserList { get; set; }
        /////////////////////////////////////
        public string AllInternalUsersListPlaceholder { get; set; }
        public IEnumerable<SelectListItem> AllInternalUsersList { get; set; }
        public IEnumerable<SelectListItem> SelectedInternalUsersList { get; set; }
        ////////////////////////////////////
        #endregion

        #region CRUD Operations
        //List of Team
        public List<ADTeam> ListTeam()
        {
            Team myentity = new Team();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADTeam> CTList = (from DataRow dr in dt.Rows
                          select new ADTeam()
                          {
                              Id = Convert.ToInt32(dr["Id"]),
                              TeamCode = Convert.ToString(dr["TeamCode"]),
                              TeamName = Convert.ToString(dr["TeamName"])

                          }).ToList();

                return CTList;
            }
            catch(Exception ex)
            {
                return (new List<ADTeam>());
            }
        }

        //Read Details from database
        public ADTeam ReadTeam(int id)
        {
            Team myentity = new Team();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (Team)myentityItable;
            AutoMapper.Mapper.CreateMap<Team, ADTeam>();
            return (AutoMapper.Mapper.Map<ADTeam>(myentity));
        }

        //Save(Create or Update) Team Details
        public void SaveTeam(ADTeam updatedTeam)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADTeam, Team>();
                ITable tblTeam;
                if (updatedTeam.Id != 0)
                {
                    ADTeam loadTeam = ReadTeam(updatedTeam.Id);
                    loadTeam.TeamName = updatedTeam.TeamName;
                    loadTeam.TeamCode = updatedTeam.TeamCode;
                    loadTeam.IsSalesTeam = updatedTeam.IsSalesTeam;
                    loadTeam.IsServiceTeam = updatedTeam.IsServiceTeam;
                    loadTeam.ManagerId = updatedTeam.ManagerId;
                    //////////////////////////////// New Change
                    loadTeam.UserList = updatedTeam.UserList;
                    ////////////////////////////////
                    Team objTeam = AutoMapper.Mapper.Map<Team>(loadTeam);
                    tblTeam = objTeam;
                    DBO.Provider.Update(ref tblTeam);
                }
                else
                {
                    Team objTeam = AutoMapper.Mapper.Map<Team>(updatedTeam);
                    tblTeam = objTeam;
                    DBO.Provider.Create(ref tblTeam);

                }
            }
            catch(Exception ex)
            {
      
            }
        }

        // Delete the team
        public static bool DeletetTeam(string Id)
        {
            try
            {
                ITable Iteam;
                Team objTeam = new Team();
                objTeam.Id = Convert.ToInt32(Id);
                Iteam = objTeam;
                DBO.Provider.Delete(ref Iteam);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }

        }
        #endregion
    }
}