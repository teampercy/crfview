﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADServiceType
    {
        #region properties
        public int ID { get; set; }
        public string ProdCode { get; set; }
        public string ProdDesc { get; set; }
        public string ProdName { get; set; }
        public int Active { get; set; }
        public int PackID { get; set; }
        public int ContractTypeId { get; set; }
        #endregion

        #region CRUD Operations
        //List of Product
        public List<ADServiceType> ListProduct()
        {
            Product myentity = new Product();
            TableView EntityList;
            try
            {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADServiceType> CTList = (from DataRow dr in dt.Rows
                                              select new ADServiceType()
                                              {
                                                  ID = Convert.ToInt16(dr["ID"]),
                                                  ProdCode = Convert.ToString(dr["ProdCode"]),
                                                  ProdName = Convert.ToString(dr["ProdName"])
                                              }).ToList();

                return CTList;
            }
            catch (Exception ex)
            {
                return (new List<ADServiceType>());
            }
        }

        //Read Details from database
        public ADServiceType ReadProduct(int id)
        {
            Product myentity = new Product();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (Product)myentityItable;
            AutoMapper.Mapper.CreateMap<Product, ADServiceType>();
            return (AutoMapper.Mapper.Map<ADServiceType>(myentity));
        }

        //Save(Create or Update) Product Details
        public void SaveProduct(ADServiceType updatedProduct)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADServiceType, Product>();
                ITable tblProduct;
                if (updatedProduct.ID != 0)
                {
                    ADServiceType loadProduct = ReadProduct(updatedProduct.ID);
                    loadProduct.ProdName = updatedProduct.ProdName;
                    loadProduct.ProdCode = updatedProduct.ProdCode;
                    Product objProduct= AutoMapper.Mapper.Map<Product>(loadProduct);
                    tblProduct = objProduct;
                    DBO.Provider.Update(ref tblProduct);
                }
                else
                {
                    Product objProduct = AutoMapper.Mapper.Map<Product>(updatedProduct);
                    tblProduct = objProduct;
                    DBO.Provider.Create(ref tblProduct);

                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
    }
}