﻿using System;
using System.Collections.Generic;
using System.Linq;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using CRF.BLL.CRFDB.TABLES;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADInternalUser
    {
        #region Properties
        public int ID { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string LoginCode { get; set; }
        public string LoginPassword { get; set; }
        public DateTime ExpireDate { get; set; }
        public DateTime ResetDate { get; set; }
        public string PasswordHint1 { get; set; }
        public string PasswordHint2 { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
        public string PhoneNo { get; set; }
        public string Fax { get; set; }
        public string Note { get; set; }
        public int InternalUser { get; set; }
        public int ClientId { get; set; }
        public int ManagerId { get; set; }
        public byte[] SignatureLogo { get; set; }
        public string Alias1 { get; set; }
        public string Alias2 { get; set; }
        public string RoleList { get; set; }
        public int IsClientViewManager { get; set; }
        public string GroupList { get; set; }
        public string IPRestrictList { get; set; }
        public string ModuleList { get; set; }
        public int ClientViewGroupId { get; set; }
        public int LastClientId { get; set; }
        public string LastBranchNum { get; set; }
        public string LastJobState { get; set; }
        public string SignatureLine1 { get; set; }
        public string SignatureLine2 { get; set; }
        public string SignatureLine3 { get; set; }
        public string SignatureLine4 { get; set; }
        public DateTime LastLoginDate { get; set; }
        public string url { get; set; }
        public string nickname { get; set; }
        public string theme { get; set; }
        public string address { get; set; }
        public string cellno { get; set; }
        public string faxno { get; set; }
        public bool isnotauthenticated { get; set; }
        public bool isinactive { get; set; }
        public string LastMachineId { get; set; }
        public int LocationId { get; set; }
        public DateTime DOB { get; set; }
        public bool IsClientViewAdmin { get; set; }
        public string ClientCode { get; set; }
        public List<CommonBindList> ClientList { get; set; }
        public List<CommonBindList> ClientViewGroupList { get; set; }
        public string UserSecurity { get; set; }
        public Boolean HasGroupAccess { get; set; }
        #endregion

        #region CRUD Opertaions Internal Users
        //call methods from DAL
        Portal_Users myentity = new Portal_Users();
        TableView EntityList;

        // List of internal Users
        public List<ADInternalUser> InternalUserGetList()
        {
           
            Query query = new Query(myentity);
            query.AddCondition(LogicalOperators._And, Portal_Users.ColumnNames.InternalUser, Conditions._IsEqual, "1");
            string sql = query.BuildQuery();
            EntityList = DBO.Provider.GetTableView(sql);
            DataTable dt = EntityList.ToTable();
            List<ADInternalUser> IUList = (from DataRow dr in dt.Rows
                      select new ADInternalUser()
                      {
                          ID = Convert.ToInt16(dr["id"]),
                          FName = Convert.ToString(dr["FName"]),
                          LName= Convert.ToString(dr["LName"]),
                          UserName = Convert.ToString(dr["UserName"]),
                          Email = Convert.ToString(dr["Email"]),
                          RoleList = Convert.ToString(dr["Rolelist"])
                      }).ToList();
           
            return IUList;
          
        }
        //Read from database
        public ADInternalUser InternalUserRead(int id)
        {
            Portal_Users myentity = new Portal_Users();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (Portal_Users)myentityItable;
            AutoMapper.Mapper.CreateMap<Portal_Users, ADInternalUser>();
            return (AutoMapper.Mapper.Map<ADInternalUser>(myentity));
        }

        //Save(Create or Update) InternalUsers Details
        public void InternalUserSave(ADInternalUser updatedUser)
        {
            AutoMapper.Mapper.CreateMap<ADInternalUser, Portal_Users>();
            if (updatedUser.ID != 0)
            {
              
                ADInternalUser ReadInternalUsers = InternalUserRead(updatedUser.ID);
                ReadInternalUsers.UserName = updatedUser.UserName;
                ReadInternalUsers.InternalUser = updatedUser.InternalUser;
                ReadInternalUsers.ClientId = updatedUser.ClientId;
                ReadInternalUsers.RoleList = updatedUser.RoleList;
                ReadInternalUsers.LoginCode = updatedUser.LoginCode;
                ReadInternalUsers.LoginPassword = updatedUser.LoginPassword;
                ReadInternalUsers.PhoneNo = updatedUser.PhoneNo;
                ReadInternalUsers.Email = updatedUser.Email;
                ReadInternalUsers.SignatureLine1 = updatedUser.SignatureLine1;
                ReadInternalUsers.SignatureLine2 = updatedUser.SignatureLine2;
                ReadInternalUsers.SignatureLine3 = updatedUser.SignatureLine3;
                ReadInternalUsers.SignatureLine4 = updatedUser.SignatureLine4;
                ReadInternalUsers.ModuleList = updatedUser.ModuleList;

                Portal_Users objInternalUser = AutoMapper.Mapper.Map<Portal_Users>(ReadInternalUsers);
                ITable tblPortal_Users = objInternalUser;
                DBO.Provider.Update(ref tblPortal_Users);
            }
            else
            {
                Portal_Users objInternalUser = AutoMapper.Mapper.Map<Portal_Users>(updatedUser);
                ITable tblPortal_Users = objInternalUser;
                DBO.Provider.Update(ref tblPortal_Users);

            }
        }


        //public TableView CollectionDeskList()
        //{
        //    string mysql = "select *,Location + '-' + LocationDescr as FriendlyName from Location";

        //    return DBO.Provider.GetTableView(mysql);

        //}
        #endregion




    }

}