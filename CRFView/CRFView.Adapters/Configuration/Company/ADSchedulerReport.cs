﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADSchedulerReport
    {
        #region Properties
        public int PKID { get; set; }
        public string taskname { get; set; }
        public string taskalias { get; set; }
        public string description { get; set; }
        public string sqltext { get; set; }
        public string exportcols { get; set; }
        public string reportdef { get; set; }
        public string reportname { get; set; }
        public string note { get; set; }
        public bool IsPDF { get; set; }
        public bool IsCSV { get; set; }
        public string type { get; set; }
        public string assemblyfile { get; set; }
        public bool IsCollections { get; set; }
        public bool IsLiens { get; set; }
        public bool IsManagement { get; set; }
        #endregion


        #region CRUD Operations
        //List of Tasks_TaskDef
        public List<ADSchedulerReport> ListSchedulerReport()
        {
            Tasks_TaskDef myentity = new Tasks_TaskDef();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADSchedulerReport> TDList = new List<ADSchedulerReport>();
                TDList = (from DataRow dr in dt.Rows
                          select new ADSchedulerReport()
                          {
                              PKID = Convert.ToInt16(dr["PKID"]),
                              taskname = Convert.ToString(dr["taskname"]),
                              taskalias = Convert.ToString(dr["taskalias"]),
                              description = Convert.ToString(dr["description"])

                          }).ToList();

                return TDList;
            }
            catch(Exception ex)
            {
                return (new List<ADSchedulerReport>());
            }
        }

        //Read Details from database
        public ADSchedulerReport ReadSchedulerReport(int id)
        {
            Tasks_TaskDef myentity = new Tasks_TaskDef();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (Tasks_TaskDef)myentityItable;
            AutoMapper.Mapper.CreateMap<Tasks_TaskDef, ADSchedulerReport>();
            return (AutoMapper.Mapper.Map<ADSchedulerReport>(myentity));
        }

        //Save(Create or Update) Tasks_TaskDef Details
        public void SaveSchedulerReport(ADSchedulerReport updatedTasks_TaskDef)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADSchedulerReport, Tasks_TaskDef>();
                ITable tblTasks_TaskDef;
                if (updatedTasks_TaskDef.PKID != 0)
                {
                    ADSchedulerReport loadTasks_TaskDef = ReadSchedulerReport(updatedTasks_TaskDef.PKID);
                    loadTasks_TaskDef.taskname = updatedTasks_TaskDef.taskname;
                    loadTasks_TaskDef.taskalias = updatedTasks_TaskDef.taskalias;
                    loadTasks_TaskDef.type = updatedTasks_TaskDef.type;
                    loadTasks_TaskDef.assemblyfile = updatedTasks_TaskDef.assemblyfile;
                    loadTasks_TaskDef.IsCollections = updatedTasks_TaskDef.IsCollections;
                    loadTasks_TaskDef.IsLiens = updatedTasks_TaskDef.IsLiens;
                    loadTasks_TaskDef.IsManagement = updatedTasks_TaskDef.IsManagement;
                    Tasks_TaskDef objTasks_TaskDef = AutoMapper.Mapper.Map<Tasks_TaskDef>(loadTasks_TaskDef);
                    tblTasks_TaskDef = objTasks_TaskDef;
                    DBO.Provider.Update(ref tblTasks_TaskDef);
                }
                else
                {
                    Tasks_TaskDef objTasks_TaskDef = AutoMapper.Mapper.Map<Tasks_TaskDef>(updatedTasks_TaskDef);
                    tblTasks_TaskDef = objTasks_TaskDef;
                    DBO.Provider.Create(ref tblTasks_TaskDef);

                }
            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion
    }
}