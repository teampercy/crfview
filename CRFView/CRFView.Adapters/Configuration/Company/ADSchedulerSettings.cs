﻿using System;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.Providers;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADSchedulerSettings
    {
        #region Properties
        public int PKID { get; set; }
        public string sitename { get; set; }
        public string homepageurl { get; set; }
        public string smtpserver { get; set; }
        public string smtpport { get; set; }
        public bool smtpenablessl { get; set; }
        public string smtplogin { get; set; }
        public string smtppassword { get; set; }
        public string errorfolder { get; set; }
        public string outputfolder { get; set; }
        public string settingsfolder { get; set; }
        public string adminname { get; set; }
        public string adminemail { get; set; }
        public string reportserverurl { get; set; }
        #endregion

        #region CRUD Operations

        //Read Details from database
        public ADSchedulerSettings ReadSchedulerSettings(int id)
        {
            Tasks_Settings myentity = new Tasks_Settings();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (Tasks_Settings)myentityItable;
            AutoMapper.Mapper.CreateMap<Tasks_Settings, ADSchedulerSettings>();
            return (AutoMapper.Mapper.Map<ADSchedulerSettings>(myentity));
        }

        //Save(Create or Update) Tasks_Settings Details
        public void SaveSchedulerSettings(ADSchedulerSettings updatedSchedulerSettings)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADSchedulerSettings, Tasks_Settings>();
                Tasks_Settings objtaskSettings = AutoMapper.Mapper.Map<Tasks_Settings>(updatedSchedulerSettings);
                ITable tbltaskSettings = objtaskSettings;
                DBO.Provider.Update(ref tbltaskSettings);
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
    }
}