﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADClientTypes
    {
        #region Properties
        public int ClientTypeId { get; set; }
        public string ClientTypeName { get; set; }
        public string ClientTypeCode { get; set; }
        #endregion
        #region CRUD Operations
        //List of ClientType
        public List<ADClientTypes> ListClientType()
        {
            ClientType myentity = new ClientType();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADClientTypes> CTList = (from DataRow dr in dt.Rows
                          select new ADClientTypes()
                          {
                              ClientTypeId = Convert.ToInt16(dr["ClientTypeId"]),
                              ClientTypeCode = Convert.ToString(dr["ClientTypeCode"]),
                              ClientTypeName = Convert.ToString(dr["ClientTypeName"])

                          }).ToList();

                return CTList;
            }
            catch(Exception ex)
            {
                return (new List<ADClientTypes>());
            }
        }

        //Read Details from database
        public ADClientTypes ReadClientType(int id)
        {
            ClientType myentity = new ClientType();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (ClientType)myentityItable;
            AutoMapper.Mapper.CreateMap<ClientType, ADClientTypes>();
            return (AutoMapper.Mapper.Map<ADClientTypes>(myentity));
        }

        //Save(Create or Update) ClientType Details
        public void SaveClientType(ADClientTypes updatedClientType)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADClientTypes, ClientType>();
                ITable tblClientType;
                if (updatedClientType.ClientTypeId != 0)
                {
                    ADClientTypes loadClientType = ReadClientType(updatedClientType.ClientTypeId);
                    loadClientType.ClientTypeName = updatedClientType.ClientTypeName;
                    loadClientType.ClientTypeCode = updatedClientType.ClientTypeCode;
                    ClientType objClientAssociation = AutoMapper.Mapper.Map<ClientType>(loadClientType);
                    tblClientType = (ITable)objClientAssociation;
                    DBO.Provider.Update(ref tblClientType);
                }
                else
                {
                    ClientType objClientAssociation = AutoMapper.Mapper.Map<ClientType>(updatedClientType);
                    tblClientType = (ITable)objClientAssociation;
                    DBO.Provider.Create(ref tblClientType);

                }
            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion
    }
}