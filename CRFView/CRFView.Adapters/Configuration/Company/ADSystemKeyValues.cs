﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADSystemKeyValues
    {
        #region Properties
        public int Id { get; set; }
        public string VALUEKEY { get; set; }
        public string KEYVALUE { get; set; }
        #endregion

        #region CRUD Operations
        //List of SystemKeyValues
        public List<ADSystemKeyValues> ListSystemKeyValues()
        {
            SystemKeyValues myentity = new SystemKeyValues();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADSystemKeyValues> RFList =  (from DataRow dr in dt.Rows
                          select new ADSystemKeyValues()
                          {
                              Id = Convert.ToInt16(dr["Id"]),
                              VALUEKEY = Convert.ToString(dr["VALUEKEY"]),
                              KEYVALUE = Convert.ToString(dr["KEYVALUE"])

                          }).ToList();

                return RFList;
            }
            catch(Exception ex)
            {
                return (new List<ADSystemKeyValues>());
            }
        }

        //Read Details from database
        public ADSystemKeyValues ReadSystemKeyValues(int id)
        {
            SystemKeyValues myentity = new SystemKeyValues();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (SystemKeyValues)myentityItable;
            AutoMapper.Mapper.CreateMap<SystemKeyValues, ADSystemKeyValues>();
            return (AutoMapper.Mapper.Map<ADSystemKeyValues>(myentity));

        }

        //Save(Create or Update) SystemKeyValues Details
        public void SaveSystemKeyValues(ADSystemKeyValues updatedSystemKeyValues)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADSystemKeyValues, SystemKeyValues>();
                ITable tblSystemKeyValues;
                if (updatedSystemKeyValues.Id != 0)
                {
                    ADSystemKeyValues loadSystemKeyValues = ReadSystemKeyValues(updatedSystemKeyValues.Id);
                    loadSystemKeyValues.VALUEKEY = updatedSystemKeyValues.VALUEKEY;
                    loadSystemKeyValues.KEYVALUE = updatedSystemKeyValues.KEYVALUE;
                    SystemKeyValues objSystemKeyValues = AutoMapper.Mapper.Map<SystemKeyValues>(loadSystemKeyValues);
                    tblSystemKeyValues = (ITable)objSystemKeyValues;
                    DBO.Provider.Update(ref tblSystemKeyValues);
                }
                else
                {
                    SystemKeyValues objSystemKeyValues = AutoMapper.Mapper.Map<SystemKeyValues>(updatedSystemKeyValues);
                    tblSystemKeyValues = (ITable)objSystemKeyValues;
                    DBO.Provider.Create(ref tblSystemKeyValues);

                }
            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion
    }
}