﻿using System;
using System.Collections.Generic;
using System.Linq;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using CRF.BLL.CRFDB.TABLES;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    
    public class ADClientUser 
    {
        #region Properties
        public int ID { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string LoginCode { get; set; }
        public string LoginPassword { get; set; }
        public DateTime ExpireDate { get; set; }
        public DateTime ResetDate { get; set; }
        public string PasswordHint1 { get; set; }
        public string PasswordHint2 { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
        public string PhoneNo { get; set; }
        public string Fax { get; set; }
        public string Note { get; set; }
        public int InternalUser { get; set; }
        public int ClientId { get; set; }
        public int ManagerId { get; set; }
        public byte[] SignatureLogo { get; set; }
        public string Alias1 { get; set; }
        public string Alias2 { get; set; }
        public string RoleList { get; set; }
        public int IsClientViewManager { get; set; }
        public string GroupList { get; set; }
        public string IPRestrictList { get; set; }
        public string ModuleList { get; set; }
        public int ClientViewGroupId { get; set; }
        public int LastClientId { get; set; }
        public string LastBranchNum { get; set; }
        public string LastJobState { get; set; }
        public string SignatureLine1 { get; set; }
        public string SignatureLine2 { get; set; }
        public string SignatureLine3 { get; set; }
        public string SignatureLine4 { get; set; }
        public DateTime LastLoginDate { get; set; }
        public string url { get; set; }
        public string nickname { get; set; }
        public string theme { get; set; }
        public string address { get; set; }
        public string cellno { get; set; }
        public string faxno { get; set; }
        public bool isnotauthenticated { get; set; }
        public bool isinactive { get; set; }
        public string LastMachineId { get; set; }
        public int LocationId { get; set; }
        public DateTime DOB { get; set; }
        public bool IsClientViewAdmin { get; set; }
        public string ClientCode { get; set; }
        public List<CommonBindList> ClientList { get; set; }
        public List<CommonBindList> ClientViewGroupList { get; set; }
        public string UserSecurity { get; set; }
        public Boolean HasGroupAccess { get; set; }
        #endregion

        #region CRUD Operations
        Portal_Users myentity = new Portal_Users();
        TableView EntityList;
        //ClientUser functions
        public List<ADClientUser> ClientUserGetList()
        {
            try
            {
                EntityList = CRF.BLL.Queries.GetClientUserList();
                DataTable dt = EntityList.ToTable();
                List<ADClientUser> IUList =  (from DataRow dr in dt.Rows
                          select new ADClientUser()
                          {
                              ID = Convert.ToInt16(dr["ID"]),
                              ClientCode = Convert.ToString(dr["ClientCode"]),
                              FName = Convert.ToString(dr["FName"]),
                              LName = Convert.ToString(dr["LName"]),
                              UserName = Convert.ToString(dr["UserName"]),
                              Email = Convert.ToString(dr["Email"]),
                              LastLoginDate = ((dr["LastLoginDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastLoginDate"]) : DateTime.MinValue)),
                              isinactive = Convert.ToBoolean(dr["isinactive"])
                          }).ToList();
                
                return IUList;
            }
            catch (Exception ex)
            {
                return (new List<ADClientUser>());
            }
        }

        //Read from database
        public ADClientUser ClientUserRead(int id)
        {
            Portal_Users myentity = new Portal_Users();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (Portal_Users)myentityItable;
            AutoMapper.Mapper.CreateMap<Portal_Users, ADClientUser>();
            return (AutoMapper.Mapper.Map<ADClientUser>(myentity));
           
        }

        public void SaveClientUser(ADClientUser updatedUser)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADClientUser, Portal_Users>();
                if (updatedUser.ID != 0)
                {
                    ADClientUser ReadClientUsers = ClientUserRead(updatedUser.ID);
                    ReadClientUsers.UserName = updatedUser.UserName;
                    ReadClientUsers.InternalUser = updatedUser.InternalUser;
                    ReadClientUsers.isinactive = updatedUser.isinactive;
                    ReadClientUsers.ClientId = updatedUser.ClientId;
                    ReadClientUsers.LoginCode = updatedUser.LoginCode;
                    ReadClientUsers.LoginPassword = updatedUser.LoginPassword;
                    ReadClientUsers.ClientViewGroupId = updatedUser.ClientViewGroupId;
                    ReadClientUsers.Email = updatedUser.Email;
                    ReadClientUsers.IsClientViewAdmin = updatedUser.IsClientViewAdmin;
                    ReadClientUsers.IsClientViewManager = updatedUser.IsClientViewManager;
                    ReadClientUsers.PhoneNo = updatedUser.PhoneNo;
                    Portal_Users objClientUser = AutoMapper.Mapper.Map<Portal_Users>(ReadClientUsers);
                    ITable tblPortal_Users = objClientUser;
                    DBO.Provider.Update(ref tblPortal_Users);
                }
                else
                {
                    Portal_Users objClientUser = AutoMapper.Mapper.Map<Portal_Users>(updatedUser);
                    ITable tblPortal_Users = objClientUser;
                    DBO.Provider.Create(ref tblPortal_Users);

                }
            }
            catch (Exception ex)
            {

            }
        }

        public static bool DeletetClient(string ID)
        {
            try
            {
                ITable IUser;
                Portal_Users objUser = new Portal_Users();
                objUser.ID = Convert.ToInt32(ID);
                IUser = objUser;
                DBO.Provider.Delete(ref IUser);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        #endregion

        #region additional functions
        public string UpdateUserSecurity(ADClientUser obj)
        {
            if (obj.IsClientViewAdmin == true)
                return "Admin";
            else if (obj.IsClientViewAdmin == false && (obj.IsClientViewManager == 1 || obj.IsClientViewManager == -1))
                return "Manager";
            else if (obj.IsClientViewAdmin == false && (obj.IsClientViewManager == 0))
                return "Staff";
            else
                return "Staff";

        }
        public Boolean UpdateHasAccess(ADClientUser obj)
        {
            if (obj.ClientViewGroupId == 0)
            {
                return false;
            }
            else
            {
                return true;
            }
        }
        #endregion
    }
}