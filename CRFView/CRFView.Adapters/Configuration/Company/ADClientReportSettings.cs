﻿using System;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.Providers;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADClientReportSettings
    {
        #region Properties
        public int PKID { get; set; }
        public string sitename { get; set; }
        public string homepageurl { get; set; }
        public string smtpserver { get; set; }
        public string smtpport { get; set; }
        public bool smtpenablessl { get; set; }
        public string smtplogin { get; set; }
        public string smtppassword { get; set; }
        public string errorfolder { get; set; }
        public string outputfolder { get; set; }
        public string settingsfolder { get; set; }
        public string adminname { get; set; }
        public string adminemail { get; set; }
        public string reportserverurl { get; set; }
        public string emailtemplate { get; set; }
        #endregion

        #region CRUD Operations

        //Read Details from database
        public ADClientReportSettings ReadReportSettings(int id)
        {
            Report_Settings myentity = new Report_Settings();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (Report_Settings)myentityItable;
            AutoMapper.Mapper.CreateMap<Report_Settings, ADClientReportSettings>();
            return (AutoMapper.Mapper.Map<ADClientReportSettings>(myentity));
        }

        //Save(Create or Update) Report_Settings Details
        public void SaveReportSettings(ADClientReportSettings updatedReportSettings)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADClientReportSettings, Report_Settings>();
                Report_Settings objReportSettings = AutoMapper.Mapper.Map<Report_Settings>(updatedReportSettings);
                ITable tblReportSettings = objReportSettings;
                DBO.Provider.Update(ref tblReportSettings);
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
    }
}