﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADClientViewGroup
    {
        #region Properties
        public int Id { get; set; }
        public string ClientViewGroupName { get; set; }
        public string ClientCodeList { get; set; }
        #endregion

        #region CRUD Opertions
        //List of ClientViewGroup
        public List<ADClientViewGroup> ListClientViewGroup()
        {
            ClientViewGroup myentity = new ClientViewGroup();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADClientViewGroup> CVList =  (from DataRow dr in dt.Rows
                          select new ADClientViewGroup()
                          {
                              Id= Convert.ToInt16(dr["Id"]),
                              ClientViewGroupName = Convert.ToString(dr["ClientViewGroupName"]),
                              ClientCodeList = Convert.ToString(dr["ClientCodeList"])
                          }).ToList();

                return CVList;
            }
            catch(Exception ex)
            {
                return (new List<ADClientViewGroup>());
            }
        }

        //Read Details from database
        public ADClientViewGroup ReadClientViewGroup(int id)
        {
            ClientViewGroup myentity = new ClientViewGroup();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (ClientViewGroup)myentityItable;
            AutoMapper.Mapper.CreateMap<ClientViewGroup, ADClientViewGroup>();
            return (AutoMapper.Mapper.Map<ADClientViewGroup>(myentity));
        }

        //Save(Create or Update) ClientViewGroup Details
        public void SaveClientViewGroup(ADClientViewGroup updatedClientViewGroup)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADClientViewGroup, ClientViewGroup>();
                ITable tblClientViewGroup;
                if (updatedClientViewGroup.Id != 0)
                {
                    ADClientViewGroup loadClientViewGroup = ReadClientViewGroup(updatedClientViewGroup.Id);
                    loadClientViewGroup.ClientViewGroupName = updatedClientViewGroup.ClientViewGroupName;
                    loadClientViewGroup.ClientCodeList = updatedClientViewGroup.ClientCodeList;
                    ClientViewGroup objClientViewgroup = AutoMapper.Mapper.Map<ClientViewGroup>(loadClientViewGroup);
                    tblClientViewGroup = objClientViewgroup;
                    DBO.Provider.Update(ref tblClientViewGroup);
                }
                else
                {
                    ClientViewGroup objClientViewgroup = AutoMapper.Mapper.Map<ClientViewGroup>(updatedClientViewGroup);
                    tblClientViewGroup = objClientViewgroup;
                    DBO.Provider.Create(ref tblClientViewGroup);

                }
            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion

       
    }
}