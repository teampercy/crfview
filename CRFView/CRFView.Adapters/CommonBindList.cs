﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using System.Data;
using HDS.DAL.COMMON;
using CRF.BLL.Providers;
using System.Web.Mvc;

namespace CRFView.Adapters
{
    public class CommonBindList
    {
        #region Properties

        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }

        #endregion

        #region Functions

        public static List<CommonBindList> GetClientList()
        {
            try
            {
                List<CommonBindList> IUList = new List<CommonBindList>();
                DataView ClientDataView = CRF.BLL.Queries.GetAllClients();
                DataTable dt = ClientDataView.ToTable();

                IUList = (from DataRow dr in dt.Rows
                          select new CommonBindList()
                          {
                              Id = Convert.ToInt16(dr["ClientId"]),
                              Name = Convert.ToString(dr["ClientName"]),
                              Code = Convert.ToString(dr["Clientcode"])
                          }).ToList();

                return IUList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetClientContract(int ClientId)
        {
            try
            {
                TableView tblClientContract = CRF.BLL.Queries.GetClientContractList(ClientId);
                DataTable dt = tblClientContract.ToTable();
                List<CommonBindList> ListClientContract = new List<CommonBindList>();
                if (tblClientContract.Count > 0)
                {
                    ListClientContract = (from DataRow dr in dt.Rows
                                          select new CommonBindList()
                                          {
                                              Id = Convert.ToInt32(dr["Id"]),
                                              Name = Convert.ToString(dr["Title"])
                                          }).ToList();
                }
                return ListClientContract;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetClientInterface()
        {

            TableView tblClientInterface = CRF.BLL.Queries.GetClientInterfaceList();
            DataTable dt = tblClientInterface.ToTable();
            List<CommonBindList> ListClientInterface = new List<CommonBindList>();
            if (tblClientInterface.Count > 0)
            {
                ListClientInterface = (from DataRow dr in dt.Rows
                                       select new CommonBindList()
                                       {
                                           Id = Convert.ToInt32(dr["Id"]),
                                           Name = Convert.ToString(dr["InterfaceName"])

                                       }).ToList();
            }
            return ListClientInterface;
        }

        public static List<CommonBindList> GetClientViewGroups()
        {
            try
            {
                List<CommonBindList> IUList = new List<CommonBindList>();
                TableView ClientViewGroupTV = CRF.BLL.Queries.ClientViewGroups();
                DataTable dt = ClientViewGroupTV.ToTable();

                IUList = (from DataRow dr in dt.Rows
                          select new CommonBindList()
                          {
                              Id = Convert.ToInt16(dr["Id"]),
                              Name = Convert.ToString(dr["ClientViewGroupName"])
                          }).ToList();

                return IUList;
            }

            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }

        }

        public static List<CommonBindList> GetSelectedClientList(string ClientCodeList)
        {
            TableView EntityList = CRF.BLL.Queries.GetSelectedClientList(ClientCodeList);
            DataTable dt = EntityList.ToTable();
            List<CommonBindList> CList = new List<CommonBindList>();
            CList = (from DataRow dr in dt.Rows
                     select new CommonBindList()
                     {
                         Code = Convert.ToString(dr["Clientcode"]),
                         Name = Convert.ToString(dr["Clientname"])

                     }).ToList();

            return CList;

        }

        public static List<CommonBindList> GetTaskNamesList()
        {
            try
            {

                TableView tblTask = CRF.BLL.Queries.GetTaskList();
                DataTable dt = tblTask.ToTable();
                List<CommonBindList> Listtasks = new List<CommonBindList>();
                if (tblTask.Count > 0)
                {
                    Listtasks = (from DataRow dr in dt.Rows
                                 select new CommonBindList()
                                 {
                                     Id = Convert.ToInt32(dr["PKID"]),
                                     Name = Convert.ToString(dr["taskname"])
                                 }).ToList();
                }
                return Listtasks;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetInternalUsersList()
        {
            try
            {
                List<CommonBindList> InternalUserList = new List<CommonBindList>();
                DataView InternalUserDataView = CRF.BLL.Queries.GetInternalUsers();
                DataTable dt = InternalUserDataView.ToTable();

                InternalUserList = (from DataRow dr in dt.Rows
                                    select new CommonBindList()
                                    {
                                        Id = Convert.ToInt16(dr["ID"]),
                                        Name = Convert.ToString(dr["UserName"])
                                    }).ToList();

                return InternalUserList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetClientUsersList()
        {
            try
            {
                List<CommonBindList> ClientUsersList = new List<CommonBindList>();
                DataView ClientUsersDataView = CRF.BLL.Queries.GetClientUsers();
                DataTable dt = ClientUsersDataView.ToTable();

                ClientUsersList = (from DataRow dr in dt.Rows
                                   select new CommonBindList()
                                   {
                                       Id = Convert.ToInt16(dr["userid"]),
                                       Name = Convert.ToString(dr["friendlyname"])

                                   }).ToList();

                return ClientUsersList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetSelectedUsersList(string UserIdList, string UserType)
        {

            TableView EntityList = CRF.BLL.Queries.GetSelectedUsersList(UserType, UserIdList);
            DataTable dt = EntityList.ToTable();
            List<CommonBindList> UserList = (from DataRow dr in dt.Rows
                        select new CommonBindList()
                        {
                            Id = Convert.ToInt32(dr["ID"]),
                            Name = Convert.ToString(dr["UserName"])

                        }).ToList();

            return UserList;

        }

        public static List<CommonBindList> GetJobActionsList()
        {
            try
            {
                TableView tblJobActions = CRF.BLL.Queries.GetJobActionsList();
                DataTable dt = tblJobActions.ToTable();
                List<CommonBindList> JobActionsList = (from DataRow dr in dt.Rows
                                                       select new CommonBindList()
                                                       {
                                                           Id = Convert.ToInt16(dr["Id"]),
                                                           Name = Convert.ToString(dr["Description"])

                                                       }).ToList();

                return JobActionsList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetCollectionRateList()
        {
            try
            {
                TableView tblCollectinRates = CRF.BLL.Queries.GetCollectionRateList();
                DataTable dt = tblCollectinRates.ToTable();
                List<CommonBindList> CollectinRatesList = (from DataRow dr in dt.Rows
                                                       select new CommonBindList()
                                                       {
                                                           Id = Convert.ToInt16(dr["Id"]),
                                                           Name = Convert.ToString(dr["CollectionRateCode"])

                                                       }).ToList();

                return CollectinRatesList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetBillableEventTypeList()
        {
            try
            {
                BillableEventType objBillableEventType = new BillableEventType();
                TableView EntityList;
                Query query = new Query(objBillableEventType);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<CommonBindList> BillableEventTypeList = (from DataRow dr in dt.Rows
                                                           select new CommonBindList()
                                                           {
                                                               Id = Convert.ToInt16(dr["Id"]),
                                                               Name = Convert.ToString(dr["BillableEventName"])

                                                           }).ToList();

                return BillableEventTypeList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetPriceCodeList()
        {
            try
            {
                PriceCode objPriceCode = new PriceCode();
                TableView EntityList;
                Query query = new Query(objPriceCode);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<CommonBindList> PriceCodeList = (from DataRow dr in dt.Rows
                                                              select new CommonBindList()
                                                              {
                                                                  Id = Convert.ToInt16(dr["Id"]),
                                                                  Name = Convert.ToString(dr["PriceCodeName"])

                                                              }).ToList();

                return PriceCodeList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetLocationList()
        {
            try
            {
                TableView tblLocations = CRF.BLL.Queries.GetLocationList();
                DataTable dt = tblLocations.ToTable();
                List<CommonBindList> LocationList = (from DataRow dr in dt.Rows
                                                           select new CommonBindList()
                                                           {
                                                               Id = Convert.ToInt16(dr["LocationId"]),
                                                               Name = Convert.ToString(dr["Title"])

                                                           }).ToList();

                return LocationList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetCollectionStatusList()
        {
            try
            {
                TableView tblCollectionStatus = CRF.BLL.Queries.GetCollectionStatusList();
                DataTable dt = tblCollectionStatus.ToTable();
                List<CommonBindList> CollectionStatusList = (from DataRow dr in dt.Rows
                                                     select new CommonBindList()
                                                     {
                                                         Id = Convert.ToInt16(dr["CollectionStatusId"]),
                                                         Name = Convert.ToString(dr["Title"])

                                                     }).ToList();

                return CollectionStatusList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetPriorityList()
        {
            try
            {
                TableView tblPriority = CRF.BLL.Queries.GetPriorityList();
                DataTable dt = tblPriority.ToTable();
                List<CommonBindList> PriorityList = (from DataRow dr in dt.Rows
                                                             select new CommonBindList()
                                                             {
                                                                 Id = Convert.ToInt16(dr["Id"]),
                                                                 Name = Convert.ToString(dr["FriendlyName"])

                                                             }).ToList();

                return PriorityList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetCollectionActionTypes()
        {
            try
            {
                TableView tblCollectionActionTypes = CRF.BLL.Queries.GetCollectionActionTypes();
                DataTable dt = tblCollectionActionTypes.ToTable();
                List<CommonBindList> CollectionActionTypesList = (from DataRow dr in dt.Rows
                                                     select new CommonBindList()
                                                     {
                                                         Id = Convert.ToInt16(dr["Id"]),
                                                         Name = Convert.ToString(dr["Description"]),
                                                         Code = Convert.ToString(dr["TypeCode"])

                                                     }).ToList();

                return CollectionActionTypesList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetLawList()
        {
            try
            {
                TableView tblLawList = CRF.BLL.Queries.GetLawList();
                DataTable dt = tblLawList.ToTable();
                List<CommonBindList> LawList = (from DataRow dr in dt.Rows
                                                                  select new CommonBindList()
                                                                  {
                                                                      Name = Convert.ToString(dr["LawList"]),
                                                                      Code = Convert.ToString(dr["PKID"])+"-"+Convert.ToString(dr["LawListCode"])

                                                                  }).ToList();

                return LawList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetCollectionStatusGroupList()
        {
            try
            {
                TableView tblStatusGroupList = CRF.BLL.Queries.CollectionStatusGroup();
                DataTable dt = tblStatusGroupList.ToTable();
                List<CommonBindList> StatusGroupList = (from DataRow dr in dt.Rows
                                                select new CommonBindList()
                                                {
                                                    Id = Convert.ToInt32(dr["Id"]),
                                                    Name = Convert.ToString(dr["StatusGroupDescription"]) 

                                                }).ToList();

                return StatusGroupList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetReportFilesList()
        {
            try
            {
                TableView tblReportFilesList = CRF.BLL.Queries.GetCollectionReportFiles();
                DataTable dt = tblReportFilesList.ToTable();
                List<CommonBindList> ReportFilesList = (from DataRow dr in dt.Rows
                                                        select new CommonBindList()
                                                        {
                                                            Id = Convert.ToInt32(dr["ReportFileId"]),
                                                            Name = Convert.ToString(dr["ReportFileName"])

                                                        }).ToList();

                return ReportFilesList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetReportCategoryList()
        {
            try
            {
                TableView tblReportCategoryList = CRF.BLL.Queries.GetReportCatagories();
                DataTable dt = tblReportCategoryList.ToTable();
                List<CommonBindList> ReportCategoryList = (from DataRow dr in dt.Rows
                                                        select new CommonBindList()
                                                        {
                                                            Id = Convert.ToInt32(dr["Id"]),
                                                            Name = Convert.ToString(dr["ReportCategory"])

                                                        }).ToList();

                return ReportCategoryList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetCollectionUsers()
        {
            try
            {
                TableView tblCollectionUsersList = CRF.BLL.Queries.GetCollectionUsers();
                DataTable dt = tblCollectionUsersList.ToTable();
                List<CommonBindList> CollectionUsersList = (from DataRow dr in dt.Rows
                                                           select new CommonBindList()
                                                           {
                                                               Code = Convert.ToString(dr["Id"]),
                                                               Name = Convert.ToString(dr["UserName"])

                                                           }).ToList();

                return CollectionUsersList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetLocationUsers(int aLocationId)
        {
            try
            {
                TableView tblLocationUsersList = CRF.BLL.Queries.GetLocationUsers(aLocationId);
                DataTable dt = tblLocationUsersList.ToTable();
                List<CommonBindList> LocationUsersList = (from DataRow dr in dt.Rows
                                                            select new CommonBindList()
                                                            {
                                                                Code = Convert.ToString(dr["UserId"]),
                                                                Name = Convert.ToString(dr["UserName"])
                                                            }).ToList();

                return LocationUsersList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetJobViewDesksList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.JobViewDeskList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                                            select new CommonBindList()
                                                            {
                                                                Id = Convert.ToInt32(dr["DeskId"]),
                                                                Name = Convert.ToString(dr["DeskNum"]),
                                                                Code = Convert.ToString(dr["FriendlyName"])

                                                            }).ToList();
                return List;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetJobStateList()
        {
            try
            {
                StateInfo mytable = new StateInfo();
                Query query = new Query(mytable);
                TableView tblList = DBO.Provider.GetTableView(query.SelectAll());
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["StateInitials"])

                                             }).ToList();
                return List;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetStatusGroupList()
        {
            try
            {
                JobStatusGroup mytable = new JobStatusGroup();
                Query query = new Query(mytable);
                TableView tblList = DBO.Provider.GetTableView(query.SelectAll());
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["StatusGroupDescription"])

                                             }).ToList();
                return List;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetJobDeskList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.JobDeskList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["DeskId"]),
                                                 Name = Convert.ToString(dr["FriendlyName"]),
                                                                Code = Convert.ToString(dr["DeskNum"])
                                                            }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }
        public static List<CommonBindList> GetJobStatusList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.JobStatusList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                                            select new CommonBindList()
                                                            {
                                                                Id = Convert.ToInt32(dr["Id"]),
                                                                Name = Convert.ToString(dr["FriendlyName"])

                                                            }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }
      
        public static List<CommonBindList> GetJobViewStatusList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.JobViewStatusList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                                            select new CommonBindList()
                                                            {
                                                                Id = Convert.ToInt32(dr["Id"]),
                                                                Name = Convert.ToString(dr["FriendlyName"])

                                                            }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }
        public static List<CommonBindList> GetJobActionCategoriesList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.JobActionCategoriesList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Code = Convert.ToString(dr["TypeCode"]),
                                                 Name = Convert.ToString(dr["Description"])

                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }
        public static List<CommonBindList> GetJobDeskGroupList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.JobDeskGroupList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["DeskGroupName"])

                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }
        public static List<CommonBindList> GetMailReturnJobActionList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.MailReturnJobActionList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["Description"])

                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }
        public static List<CommonBindList> GetMailReturnReasonList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.MailReturnReasonList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["ReasonType"])

                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }
        public static List<CommonBindList> GetMailReturnResearchList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.MailReturnResearchList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["ResearchType"])

                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }
        public static List<CommonBindList> GetMailDeliveredResultList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.MailDeliveredResultList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["ResultDescription"])

                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }
        public static List<CommonBindList> GetMailDeliveredJobActionList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.MailDeliveredJobActionList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["Description"])

                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }
        public static List<CommonBindList> GetLienReportsList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.LienReportsList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["ReportId"]),
                                                 Name = Convert.ToString(dr["Description"])

                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }
        public static List<CommonBindList> GetReportFieldNamesList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.ReportFieldNamesList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["FieldId"]),
                                                 Name = Convert.ToString(dr["FieldName"])

                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }

        }
        public static List<CommonBindList> GetLienReportFiles()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.GetLienReportFiles();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["ReportFileId"]),
                                                 Name = Convert.ToString(dr["ReportFilename"])

                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }
        public static List<CommonBindList> GetJobDeskRulesList()
        {
            try
            {
                JobDeskRules mytable = new JobDeskRules();
                Query query = new Query(mytable);
                TableView tblList = DBO.Provider.GetTableView(query.SelectAll());
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["Description"])

                                             }).ToList();
                return List;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static IEnumerable<SelectListItem> GetMainClientList()
        {
            try
            {
                TableView tblList =(TableView) CRF.BLL.Queries.GetMainClients();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["Descr"])
                                             }).ToList();

               return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetSalesTeamList()
        {
            try
            {
                TableView tblList = (TableView)CRF.BLL.Queries.GetSalesTeams();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["TeamName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetServiceTeamList()
        {
            try
            {
                TableView tblList = (TableView)CRF.BLL.Queries.GetServiceTeams();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["TeamName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetClientTypeList()
        {
            try
            {
                TableView tblList = (TableView)CRF.BLL.Queries.GetClientType();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["ClientTypeId"]),
                                                 Name = Convert.ToString(dr["ClientTypeName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetContactTypeList()
        {
            try
            {
                HDS.DAL.Providers.ITable dt_ITable;
                ContractType dt_CT = new ContractType();
                dt_ITable = dt_CT;
                TableView tblList = (TableView)CRF.BLL.Queries.GetViewForConfigTable(ref dt_ITable);
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["ContractTypeName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetProductTypeList()
        {
            try
            {
                HDS.DAL.Providers.ITable dt_ITable;
                Product dt_Pt = new Product();
                dt_ITable = dt_Pt;
                TableView tblList = (TableView)CRF.BLL.Queries.GetViewForConfigTable(ref dt_ITable);
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["ID"]),
                                                 Name = Convert.ToString(dr["ProdDesc"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetAssociationList()
        {
            try
            {
                HDS.DAL.Providers.ITable dt_ITable;
                ClientAssociation dt_CA = new ClientAssociation();
                dt_ITable = dt_CA;
                TableView tblList = (TableView)CRF.BLL.Queries.GetViewForConfigTable(ref dt_ITable);
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["AssociationName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetFinDocTypesList()
        {
            try
            {
                HDS.DAL.Providers.ITable dt_ITable;
                FinDocType dt_FDT = new FinDocType();
                dt_ITable = dt_FDT;
                TableView tblList = (TableView)CRF.BLL.Queries.GetViewForConfigTable(ref dt_ITable);
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["FinDocTypeId"]),
                                                 Name = Convert.ToString(dr["FinDocTypeName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetFinDocCycleList()
        {
            try
            {
                HDS.DAL.Providers.ITable dt_ITable;
                FinDocCycle dt_FDC = new FinDocCycle();
                dt_ITable = dt_FDC;
                TableView tblList = (TableView)CRF.BLL.Queries.GetViewForConfigTable(ref dt_ITable);
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["CycleDescription"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetContactPlanList()
        {
            try
            {
                var ContactPlanView = DBO.Provider.GetTableView("Select ContactPlanId, ContactPlanCode + '-' + ContactPlan as FriendlyName From ContactPlan Where IsTenDayContactPlan = 0");
                DataTable dt = ContactPlanView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["ContactPlanId"]),
                                                 Name = Convert.ToString(dr["FriendlyName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
                
            }
            catch(Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetTenDatContactPlan()
        {
            try
            {
                var TenDayContactPlanView = DBO.Provider.GetTableView("Select ContactPlanId, ContactPlanCode + '-' + ContactPlan as FriendlyName From ContactPlan Where IsTenDayContactPlan = 1");
                DataTable dt = TenDayContactPlanView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["ContactPlanId"]),
                                                 Name = Convert.ToString(dr["FriendlyName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");

            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetContractListForClient(int ClientId)
        {
            try
            {
                string sql = "Select * from ClientContract Where BillingClientId= "+ ClientId;
                var ContactView = DBO.Provider.GetTableView(sql);
                DataTable dt = ContactView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["Title"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch(Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }


        public static IEnumerable<SelectListItem> GetCommissionRates()
        {
            try
            {
                string sql = "Select id, substring(VW.CollectionRateCode,1,3) + '-' + VW.CollectionRateName AS FriendlyName From CollectionRate VW";
                var ContactView = DBO.Provider.GetTableView(sql);
                DataTable dt = ContactView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["id"]),
                                                 Name = Convert.ToString(dr["FriendlyName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetFinDocCollection()
        {
            try
            {
                string sql = "Select FinDocTypeId, FinDocTypeName From FinDocType Where IsAvailableInColl = 1";
                var FinDocView = DBO.Provider.GetTableView(sql);
                DataTable dt = FinDocView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["FinDocTypeId"]),
                                                 Name = Convert.ToString(dr["FinDocTypeName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch(Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetViewGroups()
        {
            try
            {
                List<CommonBindList> IUList = new List<CommonBindList>();
                TableView ClientViewGroupTV = CRF.BLL.Queries.ClientViewGroups();
                DataTable dt = ClientViewGroupTV.ToTable();

                IUList = (from DataRow dr in dt.Rows
                          select new CommonBindList()
                          {
                              Id = Convert.ToInt16(dr["Id"]),
                              Name = Convert.ToString(dr["ClientViewGroupName"])
                          }).ToList();
                return new SelectList(IUList,"Id","Name");
            }

            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetReportTypes()
        {
            try
            {
                List<CommonBindList> ReportTypeList = new List<CommonBindList>();
                TableView ReportView = DBO.Provider.GetTableView("Select * from Report Where Isdisabled = 0");
                DataTable dt = ReportView.ToTable();

                ReportTypeList = (from DataRow dr in dt.Rows
                          select new CommonBindList()
                          {
                              Id = Convert.ToInt16(dr["PKId"]),
                              Name = Convert.ToString(dr["TaskName"])
                          }).ToList();
                return new SelectList(ReportTypeList, "Id", "Name");
            }

            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetInternalUsersOpenIssues()
        {
            try
            {
                List<CommonBindList> InternalUserList = new List<CommonBindList>();
                TableView UserView = CRF.BLL.Queries.GetInternalUsers(); 
                DataTable dt = UserView.ToTable();

                InternalUserList = (from DataRow dr in dt.Rows
                                  select new CommonBindList()
                                  {
                                      Id = Convert.ToInt16(dr["ID"]),
                                      Name = Convert.ToString(dr["UserName"])
                                  }).ToList();
                return new SelectList(InternalUserList, "Id", "Name");
            }

            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetInternalUsersSelectList()
        {
            try
            {
                List<CommonBindList> InternalUserList = new List<CommonBindList>();
                DataView InternalUserDataView = CRF.BLL.Queries.GetInternalUsers();
                DataTable dt = InternalUserDataView.ToTable();

                InternalUserList = (from DataRow dr in dt.Rows
                                    select new CommonBindList()
                                    {
                                        Id = Convert.ToInt16(dr["ID"]),
                                        Name = Convert.ToString(dr["UserName"])
                                    }).ToList();
                return new SelectList(InternalUserList, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetCollectionServiceCodes()
        {
            try
            {
                List<CommonBindList> ReportTypeList = new List<CommonBindList>();
                TableView ReportView = DBO.Provider.GetTableView("Select * from vwClientCollectionServiceCode");
                DataTable dt = ReportView.ToTable();

                ReportTypeList = (from DataRow dr in dt.Rows
                                  select new CommonBindList()
                                  {
                                      Id = Convert.ToInt16(dr["ClientId"]),
                                      Name = Convert.ToString(dr["FriendlyName"])
                                  }).ToList();
                return new SelectList(ReportTypeList, "Id", "Name");
            }
            catch(Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetStates()
        {
            try
            {
                List<CommonBindList> StateList = new List<CommonBindList>();
                StateInfo mytable = new StateInfo();
                Query query = new Query(mytable);
                TableView tblList = DBO.Provider.GetTableView(query.SelectAll());
                DataTable dt = tblList.ToTable();

                StateList = (from DataRow dr in dt.Rows
                                  select new CommonBindList()
                                  {
                                      Id = Convert.ToInt16(dr["Id"]),
                                      Name = Convert.ToString(dr["StateInitials"])
                                  }).ToList();
                return new SelectList(StateList, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetCollectionStatuses()
        {
            try
            {
                string sql = "Select * from collectionstatus";
                var CollectionStatusView = DBO.Provider.GetTableView(sql);
                DataTable dt = CollectionStatusView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["CollectionStatusId"]),
                                                 Name = Convert.ToString(dr["CollectionStatus"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetCollectionStatusGroup()
        {
            try
            {
                string sql = "Select * from collectionstatusGroup";
                var collectionstatusGroupView = DBO.Provider.GetTableView(sql);
                DataTable dt = collectionstatusGroupView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["StatusGroupDescription"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetLocation()
        {
            try
            {
                string sql = "Select LocationId, Location from Location";
                var LocationView = DBO.Provider.GetTableView(sql);
                DataTable dt = LocationView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["LocationId"]),
                                                 Name = Convert.ToString(dr["Location"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }

        }

        public static IEnumerable<SelectListItem> GetAttorneyInfo()
        {
            try
            {
                string sql = "Select PKID, AttyCode from AttorneyInfo";
                var AttorneyInfoView = DBO.Provider.GetTableView(sql);
                DataTable dt = AttorneyInfoView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["PKID"]),
                                                 Name = Convert.ToString(dr["AttyCode"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }

        }

        public static IEnumerable<SelectListItem> GetCourtHouse()
        {
            try
            {
                string sql = "Select PKID, CourtId from CourtHouse";
                var CourtHouseView = DBO.Provider.GetTableView(sql);
                DataTable dt = CourtHouseView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["PKID"]),
                                                 Name = Convert.ToString(dr["CourtId"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }

        }


        public static IEnumerable<SelectListItem> GetProcessServer()
        {
            try
            {
                string sql = "Select PKID, ProcessServerNo from ProcessServer";
                var CourtHouseView = DBO.Provider.GetTableView(sql);
                DataTable dt = CourtHouseView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["PKID"]),
                                                 Name = Convert.ToString(dr["ProcessServerNo"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }

        }

        public static IEnumerable<SelectListItem> GetLocationDebt()
        {
            try
            {
                string sql = "Select LocationId, Location, LocationDescr from Location";
                var LocationView = DBO.Provider.GetTableView(sql);
                DataTable dt = LocationView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["LocationId"]),
                                                 Name = Convert.ToString(dr["Location"])+"-"+ Convert.ToString(dr["LocationDescr"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }

        }

        #endregion
    }


}