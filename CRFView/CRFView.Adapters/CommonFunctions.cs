﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRF.BLL;
using System.IO;

namespace CRFView.Adapters
{
    class CommonFunctions
    {
        CRF.BLL.Users.CurrentUser Currentuser = AuthenticationUtility.GetUser();

        public string ExportToCSV(DataTable adatatable, string areportname, ExportColumns columns)
        {
            return ExportToCSV(new DataView(adatatable), areportname, columns);
        }

        public string ExportToCSV(DataView adatatable, string areportname, ExportColumns columns)
        {
            string myspreadsheetname = CRF.BLL.Common.FileOps.GetFileName(ref Currentuser, areportname);
            return ExportToCSV(ref adatatable, ref myspreadsheetname,ref columns, true);
        }
        public static string ExportToCSV(ref DataView adataview, ref string afilename, ref ExportColumns acolumnnames, bool AddHeader)
        {
            string ls = "";
            //ExportColumn scol = new ExportColumn();
            DataTable adatatable = adataview.Table;
            if ((acolumnnames.Count < 1))
            {
                //DataColumn col;
                foreach (DataColumn col in adatatable.Columns)
                {
                    acolumnnames.Add(col.ColumnName);
                }

            }

            string MYfilename = afilename;
            if (System.IO.File.Exists(MYfilename))
            {
                System.IO.File.Delete(MYfilename);
            }

            string s;
            StreamWriter losw = new StreamWriter(MYfilename, false);
            try
            {
                ls = "";
                if ((AddHeader == true))
                {
                    foreach (ExportColumn scol in acolumnnames)
                    {
                        s = scol.ColumnName;
                        if ((scol.FriendlyName.Length > 0))
                        {
                            s = scol.FriendlyName;
                        }

                        if (!(s.Length < 1))
                        {
                            ls = (ls + ('\"'
                                        + (s.Trim() + ('\"' + ","))));
                        }
                        else
                        {
                            // NOT NOT...
                            ls = (ls + ('\"' + ("" + ('\"' + ","))));
                        }

                    }

                    losw.WriteLine(ls);
                }

                DataRow dr;
                Int32 i;
                DataView dv = adataview;
                for (i = 0; (i
                            <= (dv.Count - 1)); i++)
                {
                    dr = dv.ToTable().Rows.Find(i);
                    ls = "";
                    int z = 0;
                    foreach (ExportColumn scol in acolumnnames)
                    {
                        s = scol.ColumnName;
                        if (!(s.Length < 1))
                        {
                            ls = (ls + ('\"'
                                        + (HandleDbNull(dv.Table, s, dr[s]).ToString().Trim() + ('\"' + ","))));
                        }
                        else
                        {
                            // NOT NOT...
                            ls = (ls + ('\"' + ("" + ('\"' + ","))));
                        }

                        z = (z + 1);
                    }

                    losw.WriteLine(ls);
                }

            }
            catch (Exception E)
            {
            
                return null;
            }
            finally
            {
                losw.Close();
            }

            System.Threading.Thread.Sleep(1300);
            return MYfilename;
        }

        public static string ExportToCSV(ref DataTable adatatable, ref string afilename, ref ExportColumns acolumnnames, bool AddHeader)
        {
            string ls = "";
            //ExportColumn scol = new ExportColumn();
            if ((acolumnnames.Count < 1))
            {
                //DataColumn col;
                foreach (DataColumn col in adatatable.Columns)
                {
                    acolumnnames.Add(col.ColumnName);
                }

            }

            string MYfilename = afilename;
            if (System.IO.File.Exists(MYfilename))
            {
                System.IO.File.Delete(MYfilename);
            }

            string s;
            StreamWriter losw = new StreamWriter(MYfilename, false);
            try
            {
                ls = "";
                if ((AddHeader == true))
                {
                    foreach (ExportColumn scol in acolumnnames)
                    {
                        s = scol.ColumnName;
                        if ((scol.FriendlyName.Length > 0))
                        {
                            s = scol.FriendlyName;
                        }

                        if (!(s.Length < 1))
                        {
                            ls = (ls + ('\"'
                                        + (s.Trim() + ('\"' + ","))));
                        }
                        else
                        {
                            // NOT NOT...
                            ls = (ls + ('\"' + ("" + ('\"' + ","))));
                        }

                    }

                    losw.WriteLine(ls);
                }

                DataRow dr;
                Int32 i;
                DataView dv = new DataView(adatatable);
                for (i = 0; (i
                            <= (dv.Count - 1)); i++)
                {
                    dr = dv.ToTable().Rows.Find(i);
                    ls = "";
                    int z = 0;
                    foreach (ExportColumn scol in acolumnnames)
                    {
                        s = scol.ColumnName;
                        if (!(s.Length < 1))
                        {
                            ls = (ls + ('\"'
                                        + (HandleDbNull(dv.Table, s, dr[s]).ToString().Trim() + ('\"' + ","))));
                        }
                        else
                        {
                            // NOT NOT...
                            ls = (ls + ('\"' + ("" + ('\"' + ","))));
                        }

                        z = (z + 1);
                    }

                    // losw.WriteLine(ls.Substring(1, Len(ls) - 1))
                    losw.WriteLine(ls);
                }

            }
            catch (Exception E)
            {
                return null;
            }
            finally
            {
                losw.Close();
            }

            System.Threading.Thread.Sleep(1300);
            return MYfilename;
        }

        public static object HandleDbNull(DataTable adt, string ColumnName, object VALUE)
        {
            try
            {
                if (DBNull.Value.Equals(VALUE))
                {
                    switch (adt.Columns[ColumnName].DataType.Name.ToUpper())
                    {
                        case "DECIMAL":
                            return 0;
                            break;
                        case "MONEY":
                            return 0;
                            break;
                        case "BOOLEAN":
                            return "Y";
                            break;
                        case "FLOAT":
                            return 0;
                            break;
                        case "INT32":
                            return 0;
                            break;
                        case "INT":
                            return 0;
                            break;
                        case "DATE":
                            return "";
                            break;
                        case "DATETIME":
                            return "";
                            break;
                        default:
                            return "";
                            break;
                    }
                }
                else
                {
                    object MYVALUE = VALUE.ToString().Replace("\r\n", "");
                    switch (adt.Columns[ColumnName].DataType.Name.ToUpper())
                    {
                        case "BOOLEAN":
                            if ((Convert.ToBoolean(VALUE) == false))
                            {
                                return "N";
                            }
                            else
                            {
                                return "Y";
                            }

                            break;
                        case "DATE":
                        case "DATETIME":
                            string str = SHORTDATE(MYVALUE.ToString());
                            return str;
                            break;
                        default:
                            return MYVALUE;
                            break;
                    }
                }

            }
            catch (Exception ex)
            {
                return null;
            }

        }

        public static string SHORTDATE(string mydate)
        {
            string Date = "";
            try
            {
                Date = Convert.ToDateTime(mydate).ToShortDateString();
            }
            catch (System.Exception Return)
            {
              
            }
            return Date;
        }

        public string TrimSpace(ref string strInput)
        {
            //  This procedure trims extra space from any part of
            //  a string.
            string str = strInput.Trim();
            return string.Concat(str.Split(' '), "");
        }
    }
}
