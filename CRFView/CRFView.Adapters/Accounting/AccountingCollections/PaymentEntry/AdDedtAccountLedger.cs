﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Accounting.AccountingCollections.PaymentEntry
{
    public class AdDedtAccountLedger
    {
        #region Properties
        public int DebtAccountLedgerId { get; set; }
        public int DebtAccountId { get; set; }
        public int ClientFinDocId { get; set; }
        public int BatchId { get; set; }
        public int LedgerTypeId { get; set; }
        public string LedgerType { get; set; }
        public string CollectionStatus { get; set; }
        public string TrxCode { get; set; }
        public System.DateTime TrxDate { get; set; }
        public System.DateTime ReceiptDate { get; set; }
        public System.DateTime RemitDate { get; set; }
        public System.DateTime InvoicedDate { get; set; }
        public decimal TrxAmt { get; set; }
        public decimal AsgAmt { get; set; }
        public decimal AsgRcvAmt { get; set; }
        public decimal FeeRcvAmt { get; set; }
        public decimal AdjAmt { get; set; }
        public decimal IntRcvAmt { get; set; }
        public decimal CostRcvAmt { get; set; }
        public decimal AttyRcvAmt { get; set; }
        public decimal Rate { get; set; }
        public decimal PmtAmt { get; set; }
        public decimal BillableAmt { get; set; }
        public decimal IntSharedAmt { get; set; }
        public decimal FeeAmt { get; set; }
        public decimal BilledAmt { get; set; }
        public decimal RemitAmt { get; set; }
        public decimal OvrPmtAmt { get; set; }
        public decimal AttyFeeAmt { get; set; }
        public decimal RcvByAgencyAmt { get; set; }
        public decimal RcvByClientAmt { get; set; }
        public decimal FeeByAgencyAmt { get; set; }
        public decimal FeeByClientAmt { get; set; }
        public decimal DueFromClientAmt { get; set; }
        public decimal RemitToClientAmt { get; set; }
        public decimal PrincDueAmt { get; set; }
        public decimal TotalDueAmt { get; set; }
        public bool IsRemitted { get; set; }
        public bool IsRcvdbyClient { get; set; }
        public bool IsOmitFromStmt { get; set; }
        public bool IsOmitFromCashRpt { get; set; }
        public bool IsInterestShared { get; set; }
        public bool IsOnHold { get; set; }
        public bool IsNSF { get; set; }
        public string Reference { get; set; }
        public string LedgerNote { get; set; }
        public int DeskNumId { get; set; }
        public string DeskNum { get; set; }
        public string StmtGrouping { get; set; }
        public int EnteredByUserId { get; set; }
        public string EnteredByUserCode { get; set; }
        public decimal AttyOwedAmt { get; set; }
        public bool IsConverted { get; set; }
        public string InvoiceBreak { get; set; }
        public decimal CollectFeeAmt { get; set; }
        public decimal FeeByAgencyAmtCF { get; set; }
        public decimal FeeByClientAmtCF { get; set; }
        public int AltKeyId { get; set; }
        public string SubLedgerType { get; set; }
        public bool IsAdvanceCC { get; set; }
        public int SubLedgerTypeId { get; set; }
        public decimal ExchangeRate { get; set; }
        public decimal CostRcvAmtOnPIF { get; set; }
        public string BankName { get; set; }
        public string BankAcctNum { get; set; }
        public string CheckMaker { get; set; }
        public decimal IntShareRate { get; set; }
        public decimal RebateAmt { get; set; }
        #endregion
    }
}
