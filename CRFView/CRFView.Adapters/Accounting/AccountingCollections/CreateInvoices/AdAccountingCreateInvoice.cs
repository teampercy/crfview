﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Accounting.AccountingCollections.CreateInvoices
{
    public class AdAccountingCreateInvoice
    {
        #region Properties
        public string ClientId { get; set; }
        public string FromDate { get; set; }
        public string ThruDate { get; set; }
        public string InvoiceCycle { get; set; }
        #endregion

        #region CRUD       
        public static bool TestMethod(AdAccountingCreateInvoice obj_AdCreateInvoice)
        {
            try
            {
                var objCreateInvoice = new cview_Accounting_CreateInvoice();
                objCreateInvoice.ClientId = obj_AdCreateInvoice.ClientId;
                objCreateInvoice.FROMDATE = obj_AdCreateInvoice.FromDate;
                objCreateInvoice.THRUDATE = obj_AdCreateInvoice.ThruDate;
                objCreateInvoice.INVOICECYCLE = obj_AdCreateInvoice.InvoiceCycle;

                DBO.Provider.ExecNonQuery(objCreateInvoice);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
        #endregion
    }
}
