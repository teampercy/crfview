﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using CRF.BLL.Clients;
using CRFView.Adapters.Clients.Client_Management;
using System.IO;
using System.Text.RegularExpressions;

namespace CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices
{
    public class ADClientFinDoc
    {
        #region Properties
        public int Id { get; set; }
        public int BatchId { get; set; }
        public int ContractId { get; set; }
        public int FinDocTypeId { get; set; }
        public int FinDocStatusId { get; set; }
        public System.DateTime DocumentDate { get; set; }
        public string DocumentNo { get; set; }
        public string TrxPeriod { get; set; }
        public decimal TaxRate { get; set; }
        public decimal DiscRate { get; set; }
        public string Note { get; set; }
        public int TotQty { get; set; }
        public decimal TotalRevenue { get; set; }
        public decimal DocumentAmt { get; set; }
        public decimal TotRcvAmt { get; set; }
        public decimal TotAdjAmt { get; set; }
        public decimal TotalDue { get; set; }
        public string InvoiceName { get; set; }
        public string InvoiceAttn { get; set; }
        public string InvoiceAddr1 { get; set; }
        public string InvoiceAddr2 { get; set; }
        public string InvoiceCity { get; set; }
        public string InvoiceState { get; set; }
        public string InvoiceZip { get; set; }
        public string Reference { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
        public string ClientNumber { get; set; }
        public string SubBreak { get; set; }
        public string InvoiceContactTitle { get; set; }
        public string InvoiceEmail { get; set; }
        public string InvoicePeriod { get; set; }
        public System.DateTime InvoiceFromDate { get; set; }
        public System.DateTime InvoiceThruDate { get; set; }
        public string InvoiceCycle { get; set; }
        public bool IsEmailInvoice { get; set; }
        public int TaskHistoryId { get; set; }
        public bool IsPaid { get; set; }
        public int ClientId { get; set; }
        public int MainClientId { get; set; }
        public System.DateTime ReviewDate { get; set; }
        #endregion

        #region CRUD
        public static bool UpdatePaid(string Id)
        {
            try
            {

                //Dim myfindoc As New TABLES.ClientFinDoc
                //DAL.Provider.DAL.Read(myview.RowItem("ClientFinDocId"), myfindoc)
                //myfindoc.IsPaid = True
                //DAL.Provider.DAL.Update(myfindoc)
                var myfindoc = new ClientFinDoc();
                ITable ITable = myfindoc;
                HDS.DAL.Provider.DAL.Read(Id, ref ITable);
                myfindoc = (ClientFinDoc)ITable;
                myfindoc.IsPaid = true;
                ITable = myfindoc;
                HDS.DAL.Provider.DAL.Update( ref ITable);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public static bool FlagUnpaid(string Id)
        {
            try
            {
                var myfindoc = new ClientFinDoc();
                ITable Imyfindoc = myfindoc;
                HDS.DAL.Provider.DAL.Read(Id, ref Imyfindoc);
                myfindoc = (ClientFinDoc)Imyfindoc;
                myfindoc.IsPaid = false;
                Imyfindoc = myfindoc;
                HDS.DAL.Provider.DAL.Update(ref Imyfindoc);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool ResendEmailUpdate(string Id,string PKID, string InvoiceEmail)
        {
            try
            {
                var myfindoc = new ClientFinDoc();
                ITable Imyfindoc = myfindoc;
                DBO.Provider.Read(Id, ref Imyfindoc);
                myfindoc =(ClientFinDoc)Imyfindoc;
                if(myfindoc.IsEmailInvoice == false)
                {
                    return false;
                }
                else
                {
                    myfindoc.InvoiceEmail = InvoiceEmail;
                    Imyfindoc = myfindoc;
                    DBO.Provider.Update(ref Imyfindoc);

                    //Dim mysql As String = "Update Report_HistoryRecipient"
                    //mysql += " Set email = '" & Me.invoiceemail.Text & "' "
                    //mysql += "where HistoryId ='" & myview.RowItem("HistoryId") & "' "
                    //DAL.Provider.DAL.ExecNonQuery(mysql)

                    var mysql = "update Report_HistoryRecipient ";
                    mysql += " Set email = '" + InvoiceEmail + "' ";
                    mysql += " where HistoryId ='" + PKID + "' ";
                    DBO.Provider.ExecNonQuery(mysql);

                    return true;
                }

            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public static bool UpdateClientFinDoc(string Id, bool ChkNewReview, bool ChkNewEmail,
                                            bool ChkDeleteReview, string TxtNewReviewDate, string TxtNewEmail)
        {
            try
            {
                var myfindoc = new ClientFinDoc();
                ITable ITable = myfindoc;
                HDS.DAL.Provider.DAL.Read(Id, ref ITable);
                myfindoc = (ClientFinDoc)ITable;
                if(ChkNewReview==true)
                {
                    myfindoc.ReviewDate = Convert.ToDateTime(TxtNewReviewDate);
                }
                if(ChkDeleteReview==true)
                {
                    myfindoc.ReviewDate = DateTime.MinValue;
                }
                if(ChkNewEmail==true)
                {
                    myfindoc.InvoiceEmail = TxtNewEmail;
                }
                ITable = myfindoc;
                HDS.DAL.Provider.DAL.Update(ref ITable);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
        #endregion

    }
}
