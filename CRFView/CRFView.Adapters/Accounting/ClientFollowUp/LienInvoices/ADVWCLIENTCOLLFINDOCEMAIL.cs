﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices.CollectionInvoicesVM;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices
{
    public class ADVWCLIENTCOLLFINDOCEMAIL
    {
        #region Properties
        public int BillingClientId { get; set; }
        public int ContractId { get; set; }
        public decimal TotRcvAmt { get; set; }
        public decimal TotAdjAmt { get; set; }
        public decimal TotalDue { get; set; }
        public string InvoiceName { get; set; }
        public string InvoiceAttn { get; set; }
        public string InvoiceAddr1 { get; set; }
        public string InvoiceAddr2 { get; set; }
        public string InvoiceCity { get; set; }
        public string InvoiceState { get; set; }
        public string InvoiceZip { get; set; }
        public string CheckNo { get; set; }
        public bool IsBalanceDeduct { get; set; }
        public string Note { get; set; }
        public int ClientFinDocId { get; set; }
        public decimal DocumentAmt { get; set; }
        public System.DateTime DocumentDate { get; set; }
        public string TrxPeriod { get; set; }
        public decimal TaxRate { get; set; }
        public decimal DiscRate { get; set; }
        public decimal PmtAmt { get; set; }
        public decimal FeeAmt { get; set; }
        public decimal InterestShareAmt { get; set; }
        public decimal CourtCostAmt { get; set; }
        public decimal AttyFeeAmt { get; set; }
        public decimal RcvByAgencyAmt { get; set; }
        public decimal RcvByClientAmt { get; set; }
        public decimal FeeByAgencyAmt { get; set; }
        public decimal FeeByClientAmt { get; set; }
        public decimal DueFromClientAmt { get; set; }
        public decimal RemitToClientAmt { get; set; }
        public decimal FeesEarnedAmt { get; set; }
        public decimal FeesDeductedAmt { get; set; }
        public bool IsGrossRemit { get; set; }
        public bool IsIndStmt { get; set; }
        public string StmtGrouping { get; set; }
        public bool IsIndPaidAgencyStmt { get; set; }
        public bool IsIndPaidClientStmt { get; set; }
        public System.DateTime InvoiceFromDate { get; set; }
        public System.DateTime InvoiceThruDate { get; set; }
        public bool IsSeparateStmt { get; set; }
        public string InvoiceCycle { get; set; }
        public string InvoiceBreak { get; set; }
        public int FinDocTypeId { get; set; }
        public string ClientNumber { get; set; }
        public string InvoicePeriod { get; set; }
        public bool IsInterestShared { get; set; }
        public bool IsShowAttyFeesOnStmt { get; set; }
        public bool IsSeparateLegalStmt { get; set; }
        public bool IsCustomInvoice { get; set; }
        public bool IsEmailInvoice { get; set; }
        public int TaskHistoryId { get; set; }
        public bool IsPaid { get; set; }
        public string InvoiceEmail { get; set; }
        public int ClientId { get; set; }
        public string ClientCode { get; set; }
        public string ClientName { get; set; }
        public int reportid { get; set; }
        public string batchid { get; set; }
        public string taskname { get; set; }
        public System.DateTime startdate { get; set; }
        public System.DateTime enddate { get; set; }
        public int status { get; set; }
        public string message { get; set; }
        public System.DateTime dateviewed { get; set; }
        public System.DateTime emaildate { get; set; }
        public int HistoryId { get; set; }
        public bool IsInvoice { get; set; }
        public string vendornumber { get; set; }
        public System.DateTime ReviewDate { get; set; }
        #endregion

        #region CRUD
        public static List<ADVWCLIENTCOLLFINDOCEMAIL> ListVWCLIENTCOLLFINDOCEMAIL()
        {
            ADVWCLIENTCOLLFINDOCEMAIL myentity = new ADVWCLIENTCOLLFINDOCEMAIL();
            TableView EntityList;
            try
            {
                string sql = "SELECT * FROM VWCLIENTCOLLFINDOCEMAIL ORDER BY CLIENTCODE";
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADVWCLIENTCOLLFINDOCEMAIL> CTList = (from DataRow dr in dt.Rows
                                                      select new ADVWCLIENTCOLLFINDOCEMAIL()
                                                      {
                                                          ClientFinDocId = Convert.ToInt32(dr["ClientFinDocId"]),
                                                          ClientCode = Convert.ToString(dr["ClientCode"]),
                                                          ClientName = Convert.ToString(dr["ClientName"]),
                                                          ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
                                                          DocumentDate = ((dr["DocumentDate"] != DBNull.Value ? Convert.ToDateTime(dr["DocumentDate"]) : DateTime.MinValue)),
                                                          IsEmailInvoice = ((dr["IsEmailInvoice"] != DBNull.Value ? Convert.ToBoolean(dr["IsEmailInvoice"]) : false)),
                                                          ReviewDate = ((dr["ReviewDate"] != DBNull.Value ? Convert.ToDateTime(dr["ReviewDate"]) : DateTime.MinValue)),
                                                          DocumentAmt = ((dr["DocumentAmt"] != DBNull.Value ? Convert.ToDecimal(dr["DocumentAmt"].ToString()) : 0)),
                                                          TotalDue = ((dr["TotalDue"] != DBNull.Value ? Convert.ToDecimal(dr["TotalDue"].ToString()) : 0)),
                                                          IsPaid = ((dr["IsPaid"] != DBNull.Value ? Convert.ToBoolean(dr["IsPaid"]) : false)),
                                                          emaildate = ((dr["emaildate"] != DBNull.Value ? Convert.ToDateTime(dr["emaildate"]) : DateTime.MinValue)),
                                                          dateviewed = ((dr["dateviewed"] != DBNull.Value ? Convert.ToDateTime(dr["dateviewed"]) : DateTime.MinValue)),
                                                          InvoiceEmail = Convert.ToString(dr["InvoiceEmail"]),
                                                          HistoryId = Convert.ToInt32(dr["HistoryId"])
                                                      }).ToList();

                return CTList;
            }
            catch (Exception ex)
            {
                return (new List<ADVWCLIENTCOLLFINDOCEMAIL>());
            }
        }

        public static List<ADVWCLIENTCOLLFINDOCEMAIL> FilterListADVWCLIENTFINDOCEMAIL(string RadioSelection,
                                                     string FromDate, string ToDate, string TxtSearch,
                                                     string chkRemit, string chkGross)
        {
            ADVWCLIENTCOLLFINDOCEMAIL myentity = new ADVWCLIENTCOLLFINDOCEMAIL();
            var myfilter = new HDS.DAL.COMMON.FilterBuilder();
            TableView EntityList;
            try
            {
                string sql = "SELECT * FROM VWCLIENTCOLLFINDOCEMAIL";// ORDER BY CLIENTCODE";
                if (RadioSelection == "ReviewDate")
                {
                    sql += " WHERE ReviewDate >= '" + FromDate + "'";
                    sql += "  AND ReviewDate < '" + ToDate + "'";
                }
                else
                {
                    sql += " WHERE DocumentDate >= '" + FromDate + "'";
                    sql += "  AND DocumentDate <= '" + ToDate + "'";
                }
                if (TxtSearch != "" && TxtSearch.Length > 1)
                {
                    myfilter.Clear();
                    myfilter.SearchAnyWords("ClientCode", TxtSearch, "OR");
                    myfilter.SearchAnyWords("ClientName", TxtSearch, "OR");
                    myfilter.SearchAnyWords("InvoiceEmail", TxtSearch, "OR");
                    myfilter.SearchAnyWords("ClientFinDocId", TxtSearch, "OR");
                    var ad = myfilter.GetFilter();
                    sql += " And (" + myfilter.GetFilter() + ")";
                }
                if (RadioSelection == "NotViewed")
                {
                    sql += "  AND ( DateViewed Is Null ) ";


                }
                if (RadioSelection == "Viewed")
                {
                    sql += "  AND ( DateViewed Is Not Null ) ";
                }


                if(chkRemit == "on")
                {
                    sql += "  AND ( IsInvoice = 0 and ISGROSSREMIT = 0) ";
                }
                if(chkGross == "on")
                {
                    sql += "  AND ( IsInvoice = 1 and ISGROSSREMIT = 1) ";
                }


                if (RadioSelection == "UnPaid")
                {
                    sql += "  AND ( IsPaid <>  1 ) ";
                }
                sql += "ORDER BY CLIENTCODE";
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADVWCLIENTCOLLFINDOCEMAIL> CTList = (from DataRow dr in dt.Rows
                                                      select new ADVWCLIENTCOLLFINDOCEMAIL()
                                                      {
                                                          ClientFinDocId = Convert.ToInt32(dr["ClientFinDocId"]),
                                                          ClientCode = Convert.ToString(dr["ClientCode"]),
                                                          ClientName = Convert.ToString(dr["ClientName"]),
                                                          DocumentDate = ((dr["DocumentDate"] != DBNull.Value ? Convert.ToDateTime(dr["DocumentDate"]) : DateTime.MinValue)),
                                                          IsEmailInvoice = ((dr["IsEmailInvoice"] != DBNull.Value ? Convert.ToBoolean(dr["IsEmailInvoice"]) : false)),
                                                          ReviewDate = ((dr["ReviewDate"] != DBNull.Value ? Convert.ToDateTime(dr["ReviewDate"]) : DateTime.MinValue)),
                                                          DocumentAmt = ((dr["DocumentAmt"] != DBNull.Value ? Convert.ToDecimal(dr["DocumentAmt"].ToString()) : 0)),
                                                          TotalDue = ((dr["TotalDue"] != DBNull.Value ? Convert.ToDecimal(dr["TotalDue"].ToString()) : 0)),
                                                          IsPaid = ((dr["IsPaid"] != DBNull.Value ? Convert.ToBoolean(dr["IsPaid"]) : false)),
                                                          emaildate = ((dr["emaildate"] != DBNull.Value ? Convert.ToDateTime(dr["emaildate"]) : DateTime.MinValue)),
                                                          dateviewed = ((dr["dateviewed"] != DBNull.Value ? Convert.ToDateTime(dr["dateviewed"]) : DateTime.MinValue)),
                                                          InvoiceEmail = Convert.ToString(dr["InvoiceEmail"])
                                                      }).ToList();
                return CTList;
            }
            catch (Exception ex)
            {
                return (new List<ADVWCLIENTCOLLFINDOCEMAIL>());
            }
        }

        public static CollectionInvoiceDetailsVM GetDetails(string ClientId,string ClientFinDocId, string HistoryId)
        {
            CollectionInvoiceDetailsVM ObjCollInvoiceDetailsVM = new CollectionInvoiceDetailsVM();
            var myclient = new Client();
            ITable myclientI = myclient;

            var myhistory = new Report_History();
            ITable myhistoryI = myhistory;

            var myfindoc = new ClientCollFinDoc();
            ITable myfindocI = myfindoc;

            HDS.DAL.Provider.DAL.Read(ClientFinDocId, ref myfindocI);
            myfindoc = (ClientCollFinDoc)myfindocI;

            HDS.DAL.Provider.DAL.Read(HistoryId, ref myhistoryI);
            myhistory = (Report_History)myhistoryI;

            HDS.DAL.Provider.DAL.Read(ClientId, ref myclientI);
            myclient = (Client)myclientI;

            var MYSQL = "Select * from vwIssues where clientid ="+ ClientId+ " order by datecreated desc";
            var myds1 = HDS.DAL.Provider.DAL.GetDataSet(MYSQL);
            DataTable dt1 = myds1.Tables[0];
            List<ADvwIssues> IssueList = (from DataRow dr in dt1.Rows
                                          select new ADvwIssues()
                                          {
                                              IssueDescription = ((dr["IssueDescription"] != DBNull.Value ? StripHTML(dr["IssueDescription"].ToString()) : "")),
                                              DateCreated = ((dr["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dr["DateCreated"]) : DateTime.MinValue)),
                                              CreatedBy = Convert.ToString(dr["CreatedBy"]),
                                              AssignedTo = Convert.ToString(dr["AssignedTo"]),
                                              Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                          }).ToList();


            string MYSQL2 = "select pkid,description,url from report_historyattachment where historyid = " + myhistory.PKID + "";
            TableView myTV2;
            myTV2 = DBO.Provider.GetTableView(MYSQL2);
            DataTable dt2 = myTV2.ToTable();
            List<ADreport_historyattachment> AttachmentList = (from DataRow dr in dt2.Rows
                                                               select new ADreport_historyattachment()
                                                               {
                                                                   PKID = ((dr["PKID"] != DBNull.Value ? Convert.ToInt32(dr["PKID"]) : 0)),
                                                                   description = Convert.ToString(dr["description"]),
                                                                   url = Convert.ToString(dr["url"])
                                                               }
                                                               ).ToList();

            ObjCollInvoiceDetailsVM.ObjClient = myclient;
            ObjCollInvoiceDetailsVM.ObjClientCollFinDoc = myfindoc;
            ObjCollInvoiceDetailsVM.ObjReportHistory = myhistory;
            ObjCollInvoiceDetailsVM.Issues = IssueList;
            ObjCollInvoiceDetailsVM.Attachments = AttachmentList;


            return ObjCollInvoiceDetailsVM;
        }
        #endregion

        #region Local Methods
        static string StripHTML(string inputString)
        {
            string temp = Regex.Replace(inputString, "<.*?>", string.Empty);
            int cutLength = temp.Length;
            if (cutLength > 200)
            {
                cutLength = 200;
            }
            temp = temp.Substring(0, cutLength) + " ....";
            return temp;
        }
        #endregion
    }
}
