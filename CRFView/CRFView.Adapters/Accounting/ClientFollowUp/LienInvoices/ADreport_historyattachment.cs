﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using CRF.BLL.Clients;
using CRFView.Adapters.Clients.Client_Management;
using System.IO;
using System.Text.RegularExpressions;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices;

namespace CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices
{
    public class ADreport_historyattachment
    {
        #region Properties
        public int PKID { get; set; }
        public int historyid { get; set; }
        public System.DateTime datecreated { get; set; }
        public string description { get; set; }
        public string url { get; set; }
        public byte[] image { get; set; }
        public System.DateTime dateviewed { get; set; }
        public int viewedbyuserid { get; set; }
        #endregion

        #region CRUD
        public static ADreport_historyattachment ReadReport(string PKId)
        {
            Report_HistoryAttachment myentity = new Report_HistoryAttachment();
            ITable myentityItable = myentity;
            DBO.Provider.Read(PKId, ref myentityItable);
            myentity = (Report_HistoryAttachment)myentityItable;
            AutoMapper.Mapper.CreateMap<Report_HistoryAttachment, ADreport_historyattachment>();
            return (AutoMapper.Mapper.Map<ADreport_historyattachment>(myentity));
        }

        public static void SaveFileToBeViewed(byte[] ByteArray, string filePath)
        {
            File.WriteAllBytes(filePath, ByteArray);
        }
        #endregion


    }
}
