﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using CRF.BLL.Clients;
using CRFView.Adapters.Clients.Client_Management;
using System.IO;
using System.Text.RegularExpressions;

namespace CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices
{
    public class ADReport_History
    {
        #region Properties
        public int PKID { get; set; }
        public int reportid { get; set; }
        public string batchid { get; set; }
        public string taskname { get; set; }
        public System.DateTime startdate { get; set; }
        public System.DateTime enddate { get; set; }
        public int status { get; set; }
        public string message { get; set; }
        public int clientid { get; set; }
        public string clientcode { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
        public string keyvalues { get; set; }
        public System.DateTime dateviewed { get; set; }
        public int viewedbyuserid { get; set; }
        public bool isemailed { get; set; }
        public System.DateTime emaildate { get; set; }
        public int totitems { get; set; }
        public int lastitem { get; set; }
        public int altkeyid { get; set; }
        public int BranchClientId { get; set; }
        #endregion

        #region CRUD
        public static bool UpdateStatus(string PKID)
        {
            try
            {
                var myhistory = new Report_History();
                ITable Imyhistory = myhistory;
                HDS.DAL.Provider.DAL.Read(PKID, ref Imyhistory);
                myhistory = (Report_History)Imyhistory;
                myhistory.status = 3;
                Imyhistory = myhistory;
                DBO.Provider.Update(ref Imyhistory);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public static bool UpdateViewed(string PKID)
        {
            try
            {
                var myhistory = new Report_History();
                ITable Imyhistory = myhistory;
                HDS.DAL.Provider.DAL.Read(PKID, ref Imyhistory);
                myhistory = (Report_History)Imyhistory;
                myhistory.dateviewed = DateTime.Now;
                Imyhistory = myhistory;
                DBO.Provider.Update(ref Imyhistory);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
        #endregion
    }
}
