﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRF.BLL.CRFDB.TABLES;

namespace CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices.CollectionInvoicesVM
{
    public class CollectionInvoiceDetailsVM
    {
        public List<ADvwIssues> Issues { get; set; }
        public List<ADreport_historyattachment> Attachments { get; set; }
        public Client ObjClient { get; set; }
        public Report_History ObjReportHistory { get; set; }
        public ClientCollFinDoc ObjClientCollFinDoc { get; set; }
        public bool ChkNewReview { get; set; } = false;
        public bool ChkDeleteReview { get; set; } = false;
        public bool ChkNewEmail { get; set; } = false;
        public string TxtNewEmail { get; set; }
        public string TxtNewReviewDate { get; set; }
    }
}
