﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using CRF.BLL.Clients;
using CRFView.Adapters.Clients.Client_Management;
using System.IO;
using System.Text.RegularExpressions;

namespace CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices
{
    public class ADvwIssues
    {
        #region Properties
        public string IssueDescription { get; set; }
        public bool IsUrgent { get; set; }
        public int IssueCreatorId { get; set; }
        public int IssueAssignedId { get; set; }
        public System.DateTime DateCreated { get; set; }
        public System.DateTime DateClosed { get; set; }
        public System.DateTime DateUpdated { get; set; }
        public System.DateTime NextContactDate { get; set; }
        public int Id { get; set; }
        public string ClientCode { get; set; }
        public string ClientName { get; set; }
        public int ClientId { get; set; }
        public string CreatedBy { get; set; }
        public string AssignedTo { get; set; }
        #endregion

    }
}
