﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard
{
    public class ADDebtAccountMisc
    {
        #region Properties
        public int PKID { get; set; }
        public int DebtAccountId { get; set; }
        public decimal PymntAmt { get; set; }
        public string EscrowNo { get; set; }
        public decimal CheckAmt { get; set; }
        public string EscrowCompany { get; set; }
        public string CheckNo { get; set; }
        public string EscrowAdd { get; set; }
        public decimal Damages { get; set; }
        public string EscrowCity { get; set; }
        public string EscrowSt { get; set; }
        public string EscrowZip { get; set; }
        public string BankName { get; set; }
        public decimal SettlementAmt { get; set; }
        public string BKAdd { get; set; }
        public string BKCity { get; set; }
        public string BKSt { get; set; }
        public string BKZip { get; set; }
        public string BKCaseNo { get; set; }
        public string Bank { get; set; }
        public string BankAddress { get; set; }
        public string BankCity { get; set; }
        public string BankState { get; set; }
        public string BankZip { get; set; }
        public string AccountNum { get; set; }
        public string PropertyAddress { get; set; }
        public string PropertyCity { get; set; }
        public string PropertyState { get; set; }
        public string PropertyZip { get; set; }
        public string APNNum { get; set; }
        public string DunHist { get; set; }
        public string StHist { get; set; }
        public string DebtorBKName { get; set; }
        public string DebtorWebAdd { get; set; }
        public decimal TotDamages { get; set; }
        public string DebtorFirmName { get; set; }
        public string DebtorAttyName { get; set; }
        public string DebtorAttyAddress { get; set; }
        public string DebtorAttyCity { get; set; }
        public string DebtorAttyState { get; set; }
        public string DebtorAttyZip { get; set; }
        public string DebtorAttyPhone { get; set; }
        public string DebtorAttyEmail { get; set; }
        #endregion

        public static ADDebtAccountMisc GetMiscInfo(DataTable dt)
        {
            try
            {
                List<ADDebtAccountMisc> Misc = (from DataRow dr in dt.Rows
                                                                     select new ADDebtAccountMisc()
                                                                     {
                                                                         PKID = ((dr["PKID"] != DBNull.Value ? Convert.ToInt32(dr["PKID"].ToString()) : 0)),
                                                                         DebtAccountId = ((dr["DebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAccountId"].ToString()) : 0)),
                                                                         PymntAmt = ((dr["PymntAmt"] != DBNull.Value ? Convert.ToDecimal(dr["PymntAmt"].ToString()) : 0)),
                                                                         EscrowNo = ((dr["EscrowNo"] != DBNull.Value ? dr["EscrowNo"].ToString() : "")),
                                                                         CheckAmt = ((dr["CheckAmt"] != DBNull.Value ? Convert.ToDecimal(dr["CheckAmt"].ToString()) : 0)),
                                                                         EscrowCompany = ((dr["EscrowCompany"] != DBNull.Value ? dr["EscrowCompany"].ToString() : "")),
                                                                         CheckNo = ((dr["CheckNo"] != DBNull.Value ? dr["CheckNo"].ToString() : "")),
                                                                         EscrowAdd = ((dr["EscrowAdd"] != DBNull.Value ? dr["EscrowAdd"].ToString() : "")),
                                                                         Damages = ((dr["Damages"] != DBNull.Value ? Convert.ToDecimal(dr["Damages"].ToString()) : 0)),
                                                                         EscrowCity = ((dr["EscrowCity"] != DBNull.Value ? dr["EscrowCity"].ToString() : "")),
                                                                         EscrowSt = ((dr["EscrowSt"] != DBNull.Value ? dr["EscrowSt"].ToString() : "")),
                                                                         EscrowZip = ((dr["EscrowZip"] != DBNull.Value ? dr["EscrowZip"].ToString() : "")),
                                                                         BankName = ((dr["BankName"] != DBNull.Value ? dr["BankName"].ToString() : "")),
                                                                         SettlementAmt = ((dr["SettlementAmt"] != DBNull.Value ? Convert.ToDecimal(dr["SettlementAmt"].ToString()) : 0)),
                                                                         BKAdd = ((dr["BKAdd"] != DBNull.Value ? dr["BKAdd"].ToString() : "")),
                                                                         BKCity = ((dr["BKCity"] != DBNull.Value ? dr["BKCity"].ToString() : "")),
                                                                         BKSt = ((dr["BankName"] != DBNull.Value ? dr["BKSt"].ToString() : "")),
                                                                         BKZip = ((dr["BKZip"] != DBNull.Value ? dr["BKZip"].ToString() : "")),
                                                                         BKCaseNo = ((dr["BKCaseNo"] != DBNull.Value ? dr["BKCaseNo"].ToString() : "")),
                                                                         Bank = ((dr["Bank"] != DBNull.Value ? dr["Bank"].ToString() : "")),
                                                                         BankAddress = ((dr["BankAddress"] != DBNull.Value ? dr["BankAddress"].ToString() : "")),
                                                                         BankCity = ((dr["BankCity"] != DBNull.Value ? dr["BankCity"].ToString() : "")),
                                                                         BankState = ((dr["BankState"] != DBNull.Value ? dr["BankState"].ToString() : "")),
                                                                         BankZip = ((dr["BankZip"] != DBNull.Value ? dr["BankZip"].ToString() : "")),
                                                                         AccountNum = ((dr["AccountNum"] != DBNull.Value ? dr["AccountNum"].ToString() : "")),
                                                                         PropertyAddress = ((dr["PropertyAddress"] != DBNull.Value ? dr["PropertyAddress"].ToString() : "")),
                                                                         PropertyCity = ((dr["PropertyCity"] != DBNull.Value ? dr["PropertyCity"].ToString() : "")),
                                                                         PropertyState = ((dr["PropertyState"] != DBNull.Value ? dr["PropertyState"].ToString() : "")),
                                                                         PropertyZip = ((dr["PropertyZip"] != DBNull.Value ? dr["PropertyZip"].ToString() : "")),
                                                                         APNNum = ((dr["APNNum"] != DBNull.Value ? dr["APNNum"].ToString() : "")),
                                                                         DunHist = ((dr["DunHist"] != DBNull.Value ? dr["DunHist"].ToString() : "")),
                                                                         StHist = ((dr["StHist"] != DBNull.Value ? dr["StHist"].ToString() : "")),
                                                                         DebtorBKName = ((dr["DebtorBKName"] != DBNull.Value ? dr["DebtorBKName"].ToString() : "")),
                                                                         DebtorWebAdd = ((dr["DebtorWebAdd"] != DBNull.Value ? dr["DebtorWebAdd"].ToString() : "")),
                                                                         TotDamages = ((dr["TotDamages"] != DBNull.Value ? Convert.ToDecimal(dr["TotDamages"].ToString()) : 0)),
                                                                         DebtorFirmName = ((dr["DebtorFirmName"] != DBNull.Value ? dr["DebtorFirmName"].ToString() : "")),
                                                                         DebtorAttyName = ((dr["DebtorAttyName"] != DBNull.Value ? dr["DebtorAttyName"].ToString() : "")),
                                                                         DebtorAttyAddress = ((dr["DebtorAttyAddress"] != DBNull.Value ? dr["DebtorAttyAddress"].ToString() : "")),
                                                                         DebtorAttyCity = ((dr["DebtorAttyCity"] != DBNull.Value ? dr["DebtorAttyCity"].ToString() : "")),
                                                                         DebtorAttyState = ((dr["DebtorAttyState"] != DBNull.Value ? dr["DebtorAttyState"].ToString() : "")),
                                                                         DebtorAttyZip = ((dr["DebtorAttyZip"] != DBNull.Value ? dr["DebtorAttyZip"].ToString() : "")),
                                                                         DebtorAttyPhone = ((dr["DebtorAttyPhone"] != DBNull.Value ? dr["DebtorAttyPhone"].ToString() : "")),
                                                                         DebtorAttyEmail = ((dr["DebtorAttyEmail"] != DBNull.Value ? dr["DebtorAttyEmail"].ToString() : "")),


                                                                     }).ToList();
                return Misc.First();
            }
            catch (Exception ex)
            {
                return (new ADDebtAccountMisc());
            }
        }
    }
}
