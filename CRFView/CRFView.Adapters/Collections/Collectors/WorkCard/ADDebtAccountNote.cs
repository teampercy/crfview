﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard
{
    public class ADDebtAccountNote
    {
        #region Properties
        public int DebtAccountNoteId { get; set; }
        public int DebtAccountId { get; set; }
        public int InBound { get; set; }
        public int OwnerId { get; set; }
        public string UserCode { get; set; }
        public System.DateTime NoteDate { get; set; }
        public string Note { get; set; }
        public int NoteTypeId { get; set; }
        #endregion

        #region CRUD
        public static ADDebtAccountNote ReadNote(int id)
        {
            DebtAccountNote myentity = new DebtAccountNote();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (DebtAccountNote)myentityItable;
            AutoMapper.Mapper.CreateMap<DebtAccountNote, ADDebtAccountNote>();
            return (AutoMapper.Mapper.Map<ADDebtAccountNote>(myentity));
        }
        #endregion

    }
}
