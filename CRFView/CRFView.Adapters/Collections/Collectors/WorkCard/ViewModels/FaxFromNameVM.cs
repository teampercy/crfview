﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRF.BLL.Common;
using System.Data;
using CRF.BLL.Providers;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.Providers;
using CRF.BLL.CRFDB.SPROCS;
using HDS.DAL.COMMON;
using CRF.BLL.COMMON;

namespace CRFView.Adapters.Collections.Collectors.WorkCard.ViewModels
{
    public class FaxFromNameVM
    {
        public ADvwAccountList obj_DebtAccountList { get; set; }
        public ADDebtAccount  obj_DebtAccount { get; set; }
        public ADDebt obj_Debt { get; set; }
        public ADLocation obj_Location { get; set; }
        public ADClient obj_Client { get; set; }

        public string LocationDescr { get; set; }
        public string FaxContactName { get; set; }

        public static TableView AccountView(string DebtAccountId)
        {
            var myacc = new DebtAccount();
            ITable IAcc = myacc;
            DBO.Provider.Read(DebtAccountId, ref IAcc);
            myacc = (DebtAccount)IAcc;
            var myds = new DataSet();
            var mysproc = new cview_Accounts_GetAccountInfo();
            mysproc.AccountId = DebtAccountId;
            myds = DBO.Provider.GetDataSet(mysproc);
            TableView tv = DBO.Provider.GetTableView(mysproc);
            return tv;
        }


        public string PrintFax(string DebtAccountId, string FaxCode, string FaxContactName, string LocationDescr)
        {
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            string pdfPath = null, myreport = null, myfaxcontact = null, myfromname = null;
            switch (FaxCode)
            {
                case "CL":
                    myreport = "FaxClient";
                    myfaxcontact = FaxContactName;
                    myfromname = LocationDescr;
                    pdfPath = Reporting.PrintC1Report(user, "CollectionLetters.XML", myreport, AccountView(DebtAccountId), PrintMode.PrintToPDF, "", myfaxcontact, myfromname);
                    break;

                case "DB":
                    myreport = "FaxDebtor";                  
                    myfaxcontact = FaxContactName;
                    myfromname = LocationDescr;
                    pdfPath = Reporting.PrintC1Report(user, "CollectionLetters.XML", myreport, AccountView(DebtAccountId), PrintMode.PrintToPDF, "", myfaxcontact, myfromname);
                    break;
            }

            return pdfPath;
        }

    }
}
