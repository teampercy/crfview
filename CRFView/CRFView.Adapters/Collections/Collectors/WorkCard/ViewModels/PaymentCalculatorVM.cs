﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard.ViewModels
{
    public class PaymentCalculatorVM
    {
        #region Payment Schedule
        public double TotalDue { get; set; }
        public double PmtAmt { get; set; }
        public string Frequency { get; set; }
        public DateTime FirstPmtDate { get; set; }
        public DateTime LastPmtDate { get; set; }
        public double LastPmtAmt { get; set; }
        public int NumOfPmts { get; set; }
        public bool chkNoInt { get; set; }

        public DateTime LastPmtDate2 { get; set; }
        public DateTime LastPmtAmt2 { get; set; }
        #endregion

        #region Payment Amount
        public double TotalDueP { get; set; }
        public string FrequencyP { get; set; }
        public DateTime FirstPmtDateP { get; set; }
        public DateTime LastPmtDateP { get; set; }
        public bool chkNoIntP { get; set; }
        public double PmtAmtP { get; set; }
        public DateTime LastPayDateP { get; set; }
        public int NumOfPmtsP { get; set; }
        #endregion
    }
}
