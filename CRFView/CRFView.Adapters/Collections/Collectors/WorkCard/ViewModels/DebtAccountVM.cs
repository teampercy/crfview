﻿using CRFView.Adapters.Clients.Client_Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Collections.Collectors.WorkCard.ViewModels
{
    public class DebtAccountVM
    {
        #region Properties
        public ADvwAccountList obj_DebtAccount { get; set; }
        public ADCollectionRate obj_CollRate { get; set; }
        public ADClientCollectionInfo obj_CollectionInfo { get; set; }
        public ADDebtAccountMisc objMisc { get; set; }
        public List<ADvwCollectionNotes> ObjNotes { get; set; }
        public List<ADvwCollectionActionHistory> ObjActionHistory { get; set; }
        public List<ADLedgerGrid> ObjLedger { get; set; }
        public List<ADDebtAddress> ObjAddress { get; set; }
        public List<ADDebtAccountCreditReport> ObjCreditReport { get; set; }
        public List<ADDebtAccountAttachment> ObjDocuments { get; set; }

        public bool chkNextContact { get; set; } = false;
        public string NewContactDate { get; set; }
        public string NewPriority { get; set; }
        public bool chkNewDesk { get; set; } = false;
        public string NewLocationId { get; set; }
        public string NewTempOwnerId { get; set; }
        public bool chkTO { get; set; }
        public bool chkDeleteToDesk { get; set; }

        public bool chkInternational { get; set; }
        public bool chkSkip { get; set; }
        public bool chkJudgment{ get; set; }
        public bool chkSecond { get; set; }

        public IEnumerable<SelectListItem> LocationList { get; set; }

        public string BatchType { get; set; }
        public string SubmittedBy { get; set; }

        public string RbtNoteType { get; set; }

        //////////////
        public bool chknextContact { get; set; }
        public bool nextcontactdate { get; set; }

        public ADDebtAccount obj_ADDebtAccount { get; set; }
        #endregion
    }
}
