﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard
{
    public class ADvwCollectionActionHistory
    {
        #region Properties
        public int CollectionActionHistoryId { get; set; }
        public int DebtAccountId { get; set; }
        public int ActionId { get; set; }
        public System.DateTime DateCreated { get; set; }
        public string Description { get; set; }
        public string RequestedBy { get; set; }
        public int RequestedByUserId { get; set; }
        #endregion

        #region CRUD
        public static List<ADvwCollectionActionHistory> GetActionHistoryList(DataTable dt)
        {
            try
            {
                List<ADvwCollectionActionHistory> ActionHistory = (from DataRow dr in dt.Rows
                                                select new ADvwCollectionActionHistory()
                                                {
                                                    CollectionActionHistoryId = ((dr["CollectionActionHistoryId"] != DBNull.Value ? Convert.ToInt32(dr["CollectionActionHistoryId"].ToString()) : 0)),
                                                    DebtAccountId = ((dr["DebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAccountId"].ToString()) : 0)),
                                                    ActionId = ((dr["ActionId"] != DBNull.Value ? Convert.ToInt32(dr["ActionId"].ToString()) : 0)),
                                                    DateCreated = ((dr["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dr["DateCreated"]) : DateTime.MinValue)),
                                                    Description = ((dr["Description"] != DBNull.Value ? dr["Description"].ToString() : "")),
                                                    RequestedBy = ((dr["RequestedBy"] != DBNull.Value ? dr["RequestedBy"].ToString() : "")),
                                                    RequestedByUserId=((dr["RequestedByUserId"] != DBNull.Value ? Convert.ToInt32(dr["RequestedByUserId"].ToString()) : 0))


                                                }).ToList();
                return ActionHistory;
            }
            catch (Exception ex)
            {
                return (new List<ADvwCollectionActionHistory>());
            }
        }
        #endregion
    }
}
