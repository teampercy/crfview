﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard
{
    public class ADLocation
    {
        #region Properties
        public string LocationDescr { get; set; }
        #endregion

        #region CRUD
        public static ADLocation ReadLocation(int LocationId)
        {
            Location myentity = new Location();
            ITable myentityItable = myentity;
            DBO.Provider.Read(LocationId.ToString(), ref myentityItable);
            myentity = (Location)myentityItable;
            AutoMapper.Mapper.CreateMap<Location, ADLocation>();
            return (AutoMapper.Mapper.Map<ADLocation>(myentity));
        }
        #endregion
    }
}
