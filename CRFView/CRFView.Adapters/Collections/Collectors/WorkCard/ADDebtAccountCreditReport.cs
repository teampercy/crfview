﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard
{
    public class ADDebtAccountCreditReport
    {
        #region Properties
        public int Id { get; set; }
        public int DebtAccountId { get; set; }
        public string ContactName { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string SuffixName { get; set; }
        public string SSN { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string ECOACode { get; set; }
        public System.DateTime DateOpened { get; set; }
        public decimal CurrentBalance { get; set; }
        public string AccountStatus { get; set; }
        public string PaymentRating { get; set; }
        public string ComplianceCode { get; set; }
        public System.DateTime BillingDate { get; set; }
        public decimal PastDueAmt { get; set; }
        public System.DateTime FirstDelinqDate { get; set; }
        public string OriginalCreditor { get; set; }
        public System.DateTime RequestedOn { get; set; }
        public System.DateTime LastReportedOn { get; set; }
        public System.DateTime UpdatedOn { get; set; }
        public int AccountStatusId { get; set; }
        public int AddressId { get; set; }
        public string CreditReportKey { get; set; }
        public int DebtAddressTypeId { get; set; }
        public int SeqNo { get; set; }
        public decimal HighestCreditAmt { get; set; }
        public System.DateTime LastPayDate { get; set; }
        public System.DateTime CloseDate { get; set; }
        public string ReportType { get; set; }
        public string CosignerLastName { get; set; }
        public string CosignerFirstName { get; set; }
        public string CosignerMiddleName { get; set; }
        public string CosignerAddressLine1 { get; set; }
        public string CosignerAddressLine2 { get; set; }
        public string CosignerCity { get; set; }
        public string CosignerState { get; set; }
        public string CosignerZip { get; set; }
        public string CosignerECOACode { get; set; }
        public string CosignerSSN { get; set; }
        public string CosignerName { get; set; }
        #endregion

        #region CRUD
        public static List<ADDebtAccountCreditReport> GetDebtAccontInfo(DataTable dt)
        {
            List<ADDebtAccountCreditReport> CreditReport = (from DataRow dr in dt.Rows
                                             select new ADDebtAccountCreditReport()
                                             {
                                                 Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                                 DebtAccountId = ((dr["DebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAccountId"]) : 0)),
                                                 ContactName = ((dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : "")),
                                                 LastName = ((dr["LastName"] != DBNull.Value ? dr["LastName"].ToString() : "")),
                                                 FirstName = ((dr["FirstName"] != DBNull.Value ? dr["FirstName"].ToString() : "")),
                                                 MiddleName = ((dr["MiddleName"] != DBNull.Value ? dr["MiddleName"].ToString() : "")),
                                                 SuffixName = ((dr["SuffixName"] != DBNull.Value ? dr["SuffixName"].ToString() : "")),
                                                 SSN = ((dr["SSN"] != DBNull.Value ? dr["SSN"].ToString() : "")),
                                                 AddressLine1 = ((dr["AddressLine1"] != DBNull.Value ? dr["AddressLine1"].ToString() : "")),
                                                 AddressLine2 = ((dr["AddressLine2"] != DBNull.Value ? dr["AddressLine2"].ToString() : "")),
                                                 City = ((dr["City"] != DBNull.Value ? dr["City"].ToString() : "")),
                                                 State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),
                                                 Zip = ((dr["Zip"] != DBNull.Value ? dr["Zip"].ToString() : "")),
                                                 ECOACode = ((dr["ECOACode"] != DBNull.Value ? dr["ECOACode"].ToString() : "")),
                                                 DateOpened = ((dr["DateOpened"] != DBNull.Value ? Convert.ToDateTime(dr["DateOpened"]) : DateTime.MinValue)),
                                                 CurrentBalance = ((dr["CurrentBalance"] != DBNull.Value ? Convert.ToDecimal(dr["CurrentBalance"]) : 0)),
                                                 AccountStatus = ((dr["AccountStatus"] != DBNull.Value ? dr["AccountStatus"].ToString() : "")),
                                                 PaymentRating = ((dr["PaymentRating"] != DBNull.Value ? dr["PaymentRating"].ToString() : "")),
                                                 ComplianceCode = ((dr["ComplianceCode"] != DBNull.Value ? dr["ComplianceCode"].ToString() : "")),
                                                 BillingDate = ((dr["BillingDate"] != DBNull.Value ? Convert.ToDateTime(dr["BillingDate"]) : DateTime.MinValue)),
                                                 PastDueAmt = ((dr["PastDueAmt"] != DBNull.Value ? Convert.ToDecimal(dr["PastDueAmt"]) : 0)),
                                                 FirstDelinqDate = ((dr["FirstDelinqDate"] != DBNull.Value ? Convert.ToDateTime(dr["FirstDelinqDate"]) : DateTime.MinValue)),
                                                 OriginalCreditor = ((dr["OriginalCreditor"] != DBNull.Value ? dr["OriginalCreditor"].ToString() : "")),
                                                 RequestedOn = ((dr["RequestedOn"] != DBNull.Value ? Convert.ToDateTime(dr["RequestedOn"]) : DateTime.MinValue)),
                                                 LastReportedOn = ((dr["LastReportedOn"] != DBNull.Value ? Convert.ToDateTime(dr["LastReportedOn"]) : DateTime.MinValue)),
                                                 UpdatedOn = ((dr["UpdatedOn"] != DBNull.Value ? Convert.ToDateTime(dr["UpdatedOn"]) : DateTime.MinValue)),
                                                 AccountStatusId = ((dr["AccountStatusId"] != DBNull.Value ? Convert.ToInt32(dr["AccountStatusId"]) : 0)),
                                                 AddressId = ((dr["AddressId"] != DBNull.Value ? Convert.ToInt32(dr["AddressId"]) : 0)),
                                                 CreditReportKey = ((dr["CreditReportKey"] != DBNull.Value ? dr["CreditReportKey"].ToString() : "")),
                                                 DebtAddressTypeId = ((dr["DebtAddressTypeId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAddressTypeId"]) : 0)),
                                                 SeqNo = ((dr["SeqNo"] != DBNull.Value ? Convert.ToInt32(dr["SeqNo"]) : 0)),
                                                 HighestCreditAmt = ((dr["HighestCreditAmt"] != DBNull.Value ? Convert.ToDecimal(dr["HighestCreditAmt"]) : 0)),
                                                 LastPayDate = ((dr["LastPayDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastPayDate"]) : DateTime.MinValue)),
                                                 CloseDate = ((dr["CloseDate"] != DBNull.Value ? Convert.ToDateTime(dr["CloseDate"]) : DateTime.MinValue)),
                                                 ReportType = ((dr["ReportType"] != DBNull.Value ? dr["ReportType"].ToString() : "")),
                                                 CosignerLastName = ((dr["CosignerLastName"] != DBNull.Value ? dr["CosignerLastName"].ToString() : "")),
                                                 CosignerFirstName = ((dr["CosignerFirstName"] != DBNull.Value ? dr["CosignerFirstName"].ToString() : "")),
                                                 CosignerMiddleName = ((dr["CosignerMiddleName"] != DBNull.Value ? dr["CosignerMiddleName"].ToString() : "")),
                                                 CosignerAddressLine1 = ((dr["CosignerAddressLine1"] != DBNull.Value ? dr["CosignerAddressLine1"].ToString() : "")),
                                                 CosignerAddressLine2 = ((dr["CosignerAddressLine2"] != DBNull.Value ? dr["CosignerAddressLine2"].ToString() : "")),
                                                 CosignerCity = ((dr["CosignerCity"] != DBNull.Value ? dr["CosignerCity"].ToString() : "")),
                                                 CosignerState = ((dr["CosignerState"] != DBNull.Value ? dr["CosignerState"].ToString() : "")),
                                                 CosignerZip = ((dr["CosignerZip"] != DBNull.Value ? dr["CosignerZip"].ToString() : "")),
                                                 CosignerECOACode = ((dr["CosignerECOACode"] != DBNull.Value ? dr["CosignerECOACode"].ToString() : "")),
                                                 CosignerSSN = ((dr["CosignerSSN"] != DBNull.Value ? dr["CosignerSSN"].ToString() : "")),
                                                 CosignerName = ((dr["CosignerName"] != DBNull.Value ? dr["CosignerName"].ToString() : "")),

                                             }).ToList();
            return CreditReport;
        }
        #endregion
    }
}
