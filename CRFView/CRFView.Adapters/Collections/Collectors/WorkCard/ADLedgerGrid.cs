﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard
{
    public class ADLedgerGrid
    {
        #region Properties
        public DateTime TrxDate { get; set; }
        public DateTime RemitDate { get; set; }
        public string LedgerType { get; set; }
        public string TrxAmt { get; set; }
        public string AsgAmt { get; set; }
        public string AdjAmt { get; set; }
        public string AsgRcvAmt { get; set; }
        public string FeeRcvAmt { get; set; }
        public string IntRcvAmt { get; set; }
        public string CostRcvAmt { get; set; }
        public string AttyRcvAmt { get; set; }
        public string AttyOwedAmt { get; set; }
        public string Rate { get; set; }
        public string PmtAmt { get; set; }
        public string FeeAmt { get; set; }
        public string IsAdvanceCC { get; set; }
        public DateTime InvoicedDate { get; set; }
        public string SubLedgerType { get; set; }
        public string CollectionStatus { get; set; }
        public string DeskNum { get; set; }
        #endregion

        #region CRUD
        public static List<ADLedgerGrid> GetLedgerList(DataTable dt)
        {
            try
            {
                List<ADLedgerGrid> Ledger = (from DataRow dr in dt.Rows
                                                                   select new ADLedgerGrid()
                                                                   {
                                                                       TrxDate = ((dr["TrxDate"] != DBNull.Value ? Convert.ToDateTime(dr["TrxDate"]) : DateTime.MinValue)),
                                                                       RemitDate = ((dr["RemitDate"] != DBNull.Value ? Convert.ToDateTime(dr["RemitDate"]) : DateTime.MinValue)),
                                                                       LedgerType = ((dr["LedgerType"] != DBNull.Value ? dr["LedgerType"].ToString() : "")),
                                                                       TrxAmt = ((dr["TrxAmt"] != DBNull.Value ? dr["TrxAmt"].ToString() : "")),
                                                                       AsgAmt = ((dr["AsgAmt"] != DBNull.Value ? dr["AsgAmt"].ToString() : "")),
                                                                       AdjAmt = ((dr["AdjAmt"] != DBNull.Value ? dr["AdjAmt"].ToString() : "")),
                                                                       AsgRcvAmt = ((dr["AsgRcvAmt"] != DBNull.Value ? dr["AsgRcvAmt"].ToString() : "")),
                                                                       FeeRcvAmt = ((dr["FeeRcvAmt"] != DBNull.Value ? dr["FeeRcvAmt"].ToString() : "")),
                                                                       IntRcvAmt = ((dr["IntRcvAmt"] != DBNull.Value ? dr["IntRcvAmt"].ToString() : "")),
                                                                       CostRcvAmt = ((dr["CostRcvAmt"] != DBNull.Value ? dr["CostRcvAmt"].ToString() : "")),
                                                                       AttyRcvAmt = ((dr["AttyRcvAmt"] != DBNull.Value ? dr["AttyRcvAmt"].ToString() : "")),
                                                                       AttyOwedAmt = ((dr["AttyOwedAmt"] != DBNull.Value ? dr["AttyOwedAmt"].ToString() : "")),
                                                                       Rate = ((dr["LedgerType"] != DBNull.Value ? dr["LedgerType"].ToString() : "")),
                                                                       PmtAmt = ((dr["PmtAmt"] != DBNull.Value ? dr["PmtAmt"].ToString() : "")),
                                                                       FeeAmt = ((dr["FeeAmt"] != DBNull.Value ? dr["FeeAmt"].ToString() : "")),
                                                                       IsAdvanceCC = ((dr["IsAdvanceCC"] != DBNull.Value ? dr["IsAdvanceCC"].ToString() : "")),
                                                                       InvoicedDate = ((dr["InvoicedDate"] != DBNull.Value ? Convert.ToDateTime(dr["InvoicedDate"]) : DateTime.MinValue)),
                                                                       SubLedgerType = ((dr["SubLedgerType"] != DBNull.Value ? dr["SubLedgerType"].ToString() : "")),
                                                                       CollectionStatus = ((dr["CollectionStatus"] != DBNull.Value ? dr["CollectionStatus"].ToString() : "")),
                                                                       DeskNum = ((dr["DeskNum"] != DBNull.Value ? dr["DeskNum"].ToString() : "")),


                                                                   }).ToList();
                return Ledger;
            }
            catch (Exception ex)
            {
                return (new List<ADLedgerGrid>());
            }
        }
        #endregion
    }
}
