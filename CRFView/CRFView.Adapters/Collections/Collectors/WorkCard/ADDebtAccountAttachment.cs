﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard
{
    public class ADDebtAccountAttachment
    {
        #region Properties
        public int Id { get; set; }
        public int DebtAccountId { get; set; }
        public int UserId { get; set; }
        public bool Processed { get; set; }
        public System.DateTime DateCreated { get; set; }
        public string FileName { get; set; }
        public string DocumentType { get; set; }
        public string DocumentDescription { get; set; }
        public byte[] DocumentImage { get; set; }
        public int BatchDebtAccountId { get; set; }
        public string UserCode { get; set; }
        public bool IsClientView { get; set; }
        #endregion

        #region CRUD
        public static List<ADDebtAccountAttachment> GetDebtAttachments(DataTable dt)
        {
            List<ADDebtAccountAttachment> Documents = (from DataRow dr in dt.Rows
                                             select new ADDebtAccountAttachment()
                                             {
                                                 Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                                 DebtAccountId = ((dr["DebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAccountId"]) : 0)),
                                                 UserId = ((dr["UserId"] != DBNull.Value ? Convert.ToInt32(dr["UserId"]) : 0)),
                                                 Processed = ((dr["Processed"] != DBNull.Value ? Convert.ToBoolean(dr["Processed"]) : false)),
                                                 DateCreated = ((dr["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dr["DateCreated"]) : DateTime.MinValue)),
                                                 FileName = ((dr["FileName"] != DBNull.Value ? dr["FileName"].ToString() : "")),
                                                 DocumentType = ((dr["DocumentType"] != DBNull.Value ? dr["DocumentType"].ToString() : "")),
                                                 DocumentDescription = ((dr["DocumentDescription"] != DBNull.Value ? dr["DocumentDescription"].ToString() : "")),
                                                 BatchDebtAccountId = ((dr["BatchDebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["BatchDebtAccountId"]) : 0)),
                                                 UserCode = ((dr["UserCode"] != DBNull.Value ? dr["UserCode"].ToString() : "")),
                                                 IsClientView = ((dr["IsClientView"] != DBNull.Value ? Convert.ToBoolean(dr["IsClientView"]) : false)),
                                             }).ToList();
            if(Documents.Count==0)
            {
                return (new List<ADDebtAccountAttachment>());
            }
            return Documents;
        }
        #endregion
    }
}
