﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard
{
    public class ADCollectionRate
    {
        public int Id { get; set; }
        public string CollectionRateCode { get; set; }
        public string CollectionRateName { get; set; }
    }
}
