﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard
{
    public class Filter
    {
        #region Properties
        public string DebtAccountId { get; set; }
        public string ServiceCode { get; set; }
        public string AccountNo { get; set; }
        public string DebtorName { get; set; }
        public string ContactName { get; set; }
        public string DebtorAddress { get; set; }
        public string DebtorState { get; set; }
        public string DebtorPhoneNo { get; set; }
        public string DebtorFaxNo { get; set; }
        public string TaxId { get; set; }
        public string Priority { get; set; }
        public string CaseNum { get; set; }
        public string EntityName1 { get; set; }
        public string BKCaseNo { get; set; }
        public string DebtorBKName { get; set; }

        public bool chkReview { get; set; }
        public bool chkAssigned { get; set; }
        public bool chkStatusDate { get; set; }
        public bool chkJudgmentDate { get; set; }
        public bool chkBalance { get; set; }
        public bool chkStatus { get; set; }
        public bool chkStatusType { get; set; }
        public bool chkDesk { get; set; }
        public bool chkToDesk { get; set; }
        public bool chkSCReview { get; set; }
        public bool chkSCJudgmentDate { get; set; }
        public bool chkTrial { get; set; }
        public bool ChkAtty { get; set; }
        public bool chkCourt { get; set; }
        public bool chkServer { get; set; }

        public bool chkFWDCourtDate { get; set; }

        public DateTime NextContactDate { get; set; }
        public DateTime ReferalDate { get; set; }
        public DateTime ReferalDateFrom { get; set; }
        public DateTime ReferalDateTo { get; set; }
        public DateTime NextStatusDate { get; set; }
        public DateTime FWDJudgmentDate { get; set; }
        public DateTime FWDJudgmentDateFrom { get; set; }
        public DateTime FWDJudgmentDateTo { get; set; }
        public DateTime SCReviewDate { get; set; }
        public DateTime SCReviewDateFrom { get; set; }
        public DateTime SCReviewDateTo { get; set; }
        public DateTime SCJudgmentDate { get; set; }
        public DateTime SCJudgmentDateTo { get; set; }
        public DateTime SCJudgmentDateFrom { get; set; }
        public DateTime NextTrialDate { get; set; }
        public DateTime NextTrialDateFrom { get; set; }
        public DateTime NextTrialDateTo { get; set; }
        /// <summary>
        public DateTime NextContactDateTo { get; set; }
        public DateTime NextContactDateFrom { get; set; }
        /// </summary>

        public DateTime FWDCourtDateFrom  { get; set; }
        public DateTime FWDCourtDateTo { get; set; }

        public string Balance { get; set; }
        public string BalanceFrom { get; set; }
        public string BalanceTo { get; set; }

        public string cboStatusCode { get; set; }
        public string cboStatusType { get; set; }
        public string cboDesk { get; set; }
        public string cboToDesk { get; set; }
        public string cboAttyId { get; set; }
        public string cboCourtId { get; set; }
        public string cboProcessServerId { get; set; }

        public string AttorneyRefNumber { get; set; }
        public string CourtMemo { get; set; }
        #endregion
    }
}
