﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard
{
    public class ADDebtAccount
    {
        #region Properties
        public int DebtAccountId { get; set; }
      
        public int BatchId { get; set; }
        public int ClientId { get; set; }
        public int ContractId { get; set; }
        public int DebtId { get; set; }
        public string FileNumber { get; set; }
        public int LocationId { get; set; }
        public string Location { get; set; }
        public int CollectionStatusId { get; set; }
        public string CollectionStatus { get; set; }
        public int TempOwnerId { get; set; }
        public string TempOwner { get; set; }
        public string ClientAgentCode { get; set; }
        public string Priority { get; set; }
        public string AccountNo { get; set; }
        public string CloseCode { get; set; }
        public string DebtAccountNote { get; set; }
        public string ServiceType { get; set; }
        public string ClaimNum { get; set; }
        public int Age { get; set; }
        public bool IsNoCBR { get; set; }
        public bool IsCreditReport { get; set; }
        public System.DateTime ReferalDate { get; set; }
        public System.DateTime LastServiceDate { get; set; }
        public System.DateTime LastPaymentDate { get; set; }
        public System.DateTime LastUpdateDate { get; set; }
        public System.DateTime LastTrustDate { get; set; }
        public System.DateTime NextContactDate { get; set; }
        public int NextContactNo { get; set; }
        public System.DateTime NextLetterDate { get; set; }
        public int NextLetterSeqNo { get; set; }
        public System.DateTime NextStatusDate { get; set; }
        public System.DateTime NextLegalDate { get; set; }
        public System.DateTime InterestFromDate { get; set; }
        public System.DateTime InterestThruDate { get; set; }
        public System.DateTime CloseDate { get; set; }
        public System.DateTime FirstReportDate { get; set; }
        public System.DateTime LastReportDate { get; set; }
        public System.DateTime NextReportDate { get; set; }
        public string CreditReportStatus { get; set; }
        public System.DateTime ClientLastViewDate { get; set; }
        public int ClientLastViewBy { get; set; }
        public bool  IsEstAudit { get; set; }
        public bool  IsSecondary { get; set; }
        public bool  IsSkip { get; set; }
        public bool  IsJudgmentAcct { get; set; }
        public bool  IsIntlAcct { get; set; }
        public int TempId { get; set; }
        public string AltKey { get; set; }
        public decimal FixedRate { get; set; }
        public string FeeCode { get; set; }
        public bool IsRecalcTotals { get; set; }
        public decimal TotAsgAmt { get; set; }
        public decimal TotAsgRcvAmt { get; set; }
        public decimal TotCollFeeAmt { get; set; }
        public decimal TotCollFeeRcvAmt { get; set; }
        public decimal TotAdjAmt { get; set; }
        public decimal TotPmtAmt { get; set; }
        public decimal TotBalAmt { get; set; }
        public decimal InterestRate { get; set; }
        public decimal TotIntAmt { get; set; }
        public decimal TotIntRcvAmt { get; set; }
        public decimal TotCostRcvAmt { get; set; }
        public decimal TotCostExpAmt { get; set; }
        public decimal TotAttyExpAmt { get; set; }
        public decimal TotAttyRcvAmt { get; set; }
        public decimal TotalDue { get; set; }
        public decimal TotBillableAmt { get; set; }
        public decimal TotBilledAmt { get; set; }
        public decimal TotRemitAmt { get; set; }
        public decimal TotFeeAmt { get; set; }
        public decimal TotRcvByAgencyAmt { get; set; }
        public decimal TotRcvByClientAmt { get; set; }
        public decimal TotFeeByAgencyAmt { get; set; }
        public decimal TotFeeByClientAmt { get; set; }
        public decimal TotDueFromClientAmt { get; set; }
        public decimal TotRemitToClientAmt { get; set; }
        public decimal TotOvrPmtAmt { get; set; }
        public decimal TotIntSharedAmt { get; set; }
        public decimal TotFilingFeeAmt { get; set; }
        public decimal TotServiceFeeAmt { get; set; }
        public decimal TotWritFeeAmt { get; set; }
        public decimal TotLevyFeeAmt { get; set; }
        public decimal TotAgencyFeeAmt { get; set; }
        public decimal TotAttySettleAmt { get; set; }
        public decimal TotAttyCourtAmt { get; set; }
        public decimal TotAttyBilledAmt { get; set; }
        public decimal TotAttyFeeOwedAmt { get; set; }
        public decimal TotAttyFeeAmt { get; set; }
        public decimal PromisedPmtAmt { get; set; }
        public System.DateTime PromisedDate { get; set; }
        public System.DateTime LastReminderDate { get; set; }
        public System.DateTime NextPaymentDate { get; set; }
        public int DayInterval { get; set; }
        public int ReminderDays { get; set; }
        public int NumberofPmts { get; set; }
        public int ReminderLetterId { get; set; }
        public bool IsBroken { get; set; }
        public int BatchDebtAccountId { get; set; }
        public string BranchNum { get; set; }
        public int CommRateId { get; set; }
        public int SalesTeamId { get; set; }
        public decimal TotAdjNoCFAmt { get; set; }
        public decimal TotBalNoCFAmt { get; set; }
        public decimal ExchangeRate { get; set; }
        public int PriorityId { get; set; }
        public bool IsRapidRebate { get; set; }
        public bool IsForwarding { get; set; }
        public bool IsSmallClaims { get; set; }
        public bool IsNoCollectFee { get; set; }
        public decimal TotAttyOwedAmt { get; set; }
        public int ParentDebtAccountId { get; set; }
        public bool IsTenDayContactPlan { get; set; }
        public System.DateTime PaymentDueDate { get; set; }
        public decimal UsageFee { get; set; }
        public decimal EarlyTerminationFee { get; set; }
        public string ServiceState { get; set; }

        #endregion

        #region CRUD
        public static ADDebtAccount ReadDebtAccount(int DebtAccountId)
        {
            DebtAccount myentity = new DebtAccount();
            ITable myentityItable = myentity;
            DBO.Provider.Read(DebtAccountId.ToString(), ref myentityItable);
            myentity = (DebtAccount)myentityItable;
            AutoMapper.Mapper.CreateMap<DebtAccount, ADDebtAccount>();
            return (AutoMapper.Mapper.Map<ADDebtAccount>(myentity));
        }

        public static bool UpdateDebtAccount(ADDebtAccount objUpdatedADDebtAccount)
        {
            try
            {
                ADDebtAccount objADDebtAccount = new ADDebtAccount();
                objADDebtAccount = ReadDebtAccount(objUpdatedADDebtAccount.DebtAccountId);
                

                objADDebtAccount.DebtAccountNote = objUpdatedADDebtAccount.DebtAccountNote;
                AutoMapper.Mapper.CreateMap<ADDebtAccount, DebtAccount>();
                DebtAccount myentity = new DebtAccount();
                myentity = AutoMapper.Mapper.Map<DebtAccount>(objADDebtAccount);


                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public static bool UpdateDebtAccountEditView(ADDebtAccount objUpdatedADDebtAccount)
        {
            try
            {
                ADDebtAccount objADDebtAccountOriginal  = ReadDebtAccount(objUpdatedADDebtAccount.DebtAccountId);

                objADDebtAccountOriginal.DebtAccountNote = objUpdatedADDebtAccount.DebtAccountNote;
                objADDebtAccountOriginal.BranchNum = objUpdatedADDebtAccount.BranchNum;
                if(objUpdatedADDebtAccount.CloseDate.ToString() == ""|| objUpdatedADDebtAccount.CloseDate== null)
                {
                }
                else
                {
                    objADDebtAccountOriginal.CloseDate = objUpdatedADDebtAccount.CloseDate;
                }

                if(objUpdatedADDebtAccount.NextStatusDate.ToString() == "" || objUpdatedADDebtAccount.NextStatusDate==null)
                {
                }
                else
                {
                    objADDebtAccountOriginal.NextStatusDate = objUpdatedADDebtAccount.NextStatusDate;
                }


                if (objUpdatedADDebtAccount.PaymentDueDate.ToString() == "" || objUpdatedADDebtAccount.PaymentDueDate == null)
                {
                }
                else
                {
                    objADDebtAccountOriginal.PaymentDueDate = objUpdatedADDebtAccount.PaymentDueDate;
                }

                objADDebtAccountOriginal.IsIntlAcct = objUpdatedADDebtAccount.IsIntlAcct;
                objADDebtAccountOriginal.IsJudgmentAcct = objUpdatedADDebtAccount.IsJudgmentAcct;
                objADDebtAccountOriginal.IsSkip = objUpdatedADDebtAccount.IsSkip;
                objADDebtAccountOriginal.IsSecondary = objUpdatedADDebtAccount.IsSecondary;
                objADDebtAccountOriginal.LastUpdateDate = objUpdatedADDebtAccount.LastUpdateDate;

                AutoMapper.Mapper.CreateMap<ADDebtAccount, DebtAccount>();
                DebtAccount myentity;
                myentity = AutoMapper.Mapper.Map<DebtAccount>(objADDebtAccountOriginal);
                ITable tblI =  myentity;
                DBO.Provider.Update(ref tblI);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool UpdateDebtAccountMoreInfoEditView(ADDebtAccount objUpdatedADDebtAccount)
        {
            try
            {
                ADDebtAccount objADDebtAccountOriginal = ReadDebtAccount(objUpdatedADDebtAccount.DebtAccountId);

                if (objUpdatedADDebtAccount.NextContactDate.ToString() == "" || objUpdatedADDebtAccount.NextContactDate == null)
                {
                }
                else
                {
                    objADDebtAccountOriginal.NextContactDate = objUpdatedADDebtAccount.NextContactDate;
                }

                if (objUpdatedADDebtAccount.LocationId.ToString() == "" || objUpdatedADDebtAccount.LocationId == 0)
                {
                }
                else
                {
                    objADDebtAccountOriginal.LocationId = objUpdatedADDebtAccount.LocationId;
                }

                if (objUpdatedADDebtAccount.TempOwnerId.ToString() == "" || objUpdatedADDebtAccount.TempOwnerId == 0)
                {
                }
                else
                {
                    objADDebtAccountOriginal.TempOwnerId = objUpdatedADDebtAccount.TempOwnerId;
                }

                if (objUpdatedADDebtAccount.Priority.ToString() == "" || objUpdatedADDebtAccount.Priority == null)
                {
                }
                else
                {
                    objADDebtAccountOriginal.Priority = objUpdatedADDebtAccount.Priority;
                }

                AutoMapper.Mapper.CreateMap<ADDebtAccount, DebtAccount>();
                DebtAccount myentity;
                myentity = AutoMapper.Mapper.Map<DebtAccount>(objADDebtAccountOriginal);
                ITable tblI = myentity;
                DBO.Provider.Update(ref tblI);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion
    }
}
