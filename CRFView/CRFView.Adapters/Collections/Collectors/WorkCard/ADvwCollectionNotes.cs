﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard
{
    public class ADvwCollectionNotes
    {
        #region Properties
        public int Id { get; set; }
        public int DebtAccountNoteId { get; set; }
        public int DebtAccountId { get; set; }
        public int InBound { get; set; }
        public int OwnerId { get; set; }
        public string UserCode { get; set; }
        public DateTime NoteDate { get; set; }
        public string Note { get; set; }
        public int NoteTypeId { get; set; }
        public string NoteType { get; set; }
        #endregion

        #region CRUD
        public static List<ADvwCollectionNotes> GetNotes(DataTable dt)
        {
            List<ADvwCollectionNotes> Notes = (from DataRow dr in dt.Rows
                                             select new ADvwCollectionNotes()
                                             {
                                                 Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                                 OwnerId = ((dr["OwnerId"] != DBNull.Value ? Convert.ToInt32(dr["OwnerId"]) : 0)),
                                                 UserCode = ((dr["UserCode"] != DBNull.Value ? dr["UserCode"].ToString() : "")),
                                                 NoteDate = ((dr["NoteDate"] != DBNull.Value ? Convert.ToDateTime(dr["NoteDate"]) : DateTime.MinValue)),
                                                 Note = ((dr["Note"] != DBNull.Value ? dr["Note"].ToString() : "")),
                                                 NoteTypeId = ((dr["NoteTypeId"] != DBNull.Value ? Convert.ToInt32(dr["NoteTypeId"]) : 0)),
                                                 NoteType = ((dr["NoteType"] != DBNull.Value ? dr["NoteType"].ToString() : "")),

                                             }).ToList();
            return Notes;
        }
        #endregion
    }
}
