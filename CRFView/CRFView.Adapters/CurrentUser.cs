﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
   public class CurrentUser
    {
        #region Properties
        //public  int Id { get; set; }
        //public string UserName { get; set; }
        //public string ClientCode { get; set; }
        //public string Email { get; set; } 
        //public string SiteDataFolder { get; set; }
        //public string SettingsFolder { get; set; }
        //public string ErrorLogFolder { get; set; }
        //public string OutputFolder { get; set; }
        //public string UploadFolder { get; set; }
        //public string LastUploadPath { get; set; }
        //public string LastReportPlath { get; set; }
        //public string SMTPHOST { get; set; }
        //public string SMTPUSER { get; set; }
        //public string SMTPPASSWORD { get; set; }
        //public string SMTPFROM { get; set; }
        //public string FAXPRINTER { get; set; }
        //public string REPORTID { get; set; }
        //public string LastLedgerType { get; set; }
        public int Id = -1;
        public string UserName = "Guest";
        public string ClientCode;
        public string Email = "";
        public string SiteDataFolder;
        public string SettingsFolder;
        public string ErrorLogFolder;
        public string OutputFolder;
        public string UploadFolder;
        public string LastUploadPath;
        public string LastReportPlath;
        public string SMTPHOST;
        public string SMTPUSER;
        public string SMTPPASSWORD;
        public string SMTPFROM;
        public string FAXPRINTER;
        public string REPORTID;
        public string LastLedgerType;
        #endregion
    }
}
