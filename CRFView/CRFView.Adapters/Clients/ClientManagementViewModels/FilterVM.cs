﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CRFView.Adapters.Clients.ClientManagementViewModels
{
    public class FilterVM
    {
        public string NextContactDateFROM { get; set; }
        public DateTime NextContactDateTO { get; set; }
        public string ClientName { get; set; }
        public string ContactName { get; set; }
        public string MainClientCode { get; set; }
        public string ClientCode { get; set; }
        public string USERID { get; set; }
        public string TEAMCODE { get; set; }
        public int IssueFilter { get; set; }
        public int IssuesAssignedToId { get; set; }
        public int InactiveFilter { get; set; }
        public string ContactCity { get; set; }
        public string ContactState { get; set; }
        public string EvaluationDateFROM { get; set; }
        public DateTime EvaluationDateTO { get; set; }

        public bool ClientNameSatrtsWith { get; set; }
        public bool ClientNameIncludes { get; set; } = true;
        public string RbtClientNameValue { get; set; }

        public bool ClientCodeInMain { get; set; }
        public bool ClientCodeInEither { get; set; } = true;
        public string RbtClientCodeValue { get; set; }

        public bool ContactNameStartsWith { get; set; }
        public bool ContactNameIncludes { get; set; } = true;
        public string RbtContactNameValue { get; set; }

        public bool ContactCityStartsWith { get; set; }
        public bool ContactCityIncludes { get; set; } = true;
        public string RbtContactCityValue { get; set; }

        public bool ContactStateStartsWith { get; set; }
        public bool ContactStateIncludes { get; set; } = true;
        public string RbtContactStateValue { get; set; }

        public bool ChkNextContactDate { get; set; }
        public bool ChkNEvaluationtDate { get; set; }
        public bool ChkInactives { get; set; } = false;

        public bool BypassIssues { get; set; } = true;
        public bool AllOpenIssues { get; set; }
        public bool MyOpenIssues { get; set; }
        public string IssueValue { get; set; }



    }
}
