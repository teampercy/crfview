﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CRFView.Adapters.Clients.Client_Management;

namespace CRFView.Adapters.Clients.ClientManagementViewModels
{
    public class NoteVM
    {
        #region Properties
        public ADIssues Obj_Note {get; set;}
        #endregion

        #region Constructor
        public NoteVM()
        {
            Obj_Note = new ADIssues();
        }
        #endregion
    }
}
