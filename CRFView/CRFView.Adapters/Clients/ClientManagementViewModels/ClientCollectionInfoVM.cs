﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CRFView.Adapters.Clients.Client_Management;

namespace CRFView.Adapters.Clients.ClientManagementViewModels
{
    public class ClientCollectionInfoVM
    {
        public ADClientCollectionInfo Obj_ADCCI { get; set; }
    }
}
