﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Clients.ClientManagementViewModels
{
    public class NewLienRatesVM
    {
        public string descr { get; set; }
        public string BillableEventName { get; set; }
        public string Id { get; set; }
        public string PriceCode { get; set; }
        public string IsAllInclusivePrice { get; set; }
        public string PrelimPercentage { get; set; }
        public string UsePrelimVolume { get; set; }
        public string BillableEventId { get; set; }
        public string IsFreeFormPricing { get; set; }
    }
}
