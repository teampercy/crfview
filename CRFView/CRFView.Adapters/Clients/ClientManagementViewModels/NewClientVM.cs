﻿using CRFView.Adapters.Clients.Client_Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Clients.ClientManagementViewModels
{
    public class NewClientVM
    {
        public ADClient ADC_obj { get; set; }
        public ADClientBranchSigner ADC_BranchSigner { get; set; }
        public IEnumerable<SelectListItem> MainClientList { get; set; }
        public IEnumerable<SelectListItem> SalesTeamList { get; set; }
        public IEnumerable<SelectListItem> ServiceTeamList { get; set; }
        public IEnumerable<SelectListItem> ClientTypeList { get; set; }
        public bool ChkFollowUp { get; set; } = false;
        public bool ChkEvalDate { get; set; } = true;
        public int ParentClientId { get; set; }
        public int ParentClientCode { get; set; }

        public NewClientVM()
        {
            ADC_obj = new ADClient();
            ADC_BranchSigner = new ADClientBranchSigner();
        }
    }
}
