﻿using CRFView.Adapters.Clients.Client_Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Clients.ClientManagementViewModels
{
    public class ClientEditVM
    {
        public ADClient Obj_ADClient { get; set; }
        public List<ADClientContract> ContractList { get; set; }
        public List<ADClient> BranchList { get; set; }
        public List<ADPortalUsers> UserList { get; set; }
        public List<ADClientContact> ContactList { get; set; }
        public List<ADClientSigner> SignersList { get; set; }
        public List<ADvwIssues> ClosedIssueList { get; set; }
        public List<ADvwIssues> OpenIssueList { get; set; }
        public List<ADvwclientscheduledreports> ReportList { get; set; }
        public IEnumerable<SelectListItem> MainClientList { get; set; }
        public IEnumerable<SelectListItem> SalesTeamList { get; set; }
        public IEnumerable<SelectListItem> ServiceTeamList { get; set; }
        public IEnumerable<SelectListItem> ClientTypeList { get; set; }
        public bool ChkFollowUp { get; set; }
        public bool ChkEvalDate { get; set; }
        public int? ClientLienInfoId { get; set; }
        public int? ClientCollectionInfoId { get; set; }
        public int? ClientImageId { get; set; }
    }
}
