﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Clients.ClientManagementViewModels
{
    public class DocumentsVM
    { 
        public int Id { get; set; }
        public DateTime DateCreated { get; set; }
        public string UserCode { get; set; }
        public string DocumentType { get; set; }
        public string DocumentDescription { get; set; }
    }
}
