﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CRFView.Adapters.Clients.Client_Management;

namespace CRFView.Adapters.Clients.ClientManagementViewModels
{
    public class UserVM
    {
        public ADPortalUsers PortalUser { get; set; }
        public IEnumerable<SelectListItem> ViewGroupList { get; set; }
        public bool IsClientViewManagerView { get; set; } = false;
        public bool IsClientViewAdminView { get; set; } = false;
        public bool IsStaff { get; set; } = false;
        public bool HasGroupAccess { get; set; } = false;
        public string IsAdminManagerStaff { get; set; }


        public UserVM()
        {
            PortalUser = new ADPortalUsers();

        }

        public static void AdminManagerSetting(ref UserVM Obj_UserVm)
        {
            if (Obj_UserVm.PortalUser.IsClientViewAdmin == true )
            {
                Obj_UserVm.IsAdminManagerStaff = "Admin";
            }
            else if (Obj_UserVm.PortalUser.IsClientViewManager == 1 || Obj_UserVm.PortalUser.IsClientViewManager == -1)
            {
                Obj_UserVm.IsAdminManagerStaff = "Manager";
            }
            else
            {
                Obj_UserVm.IsAdminManagerStaff = "Staff";
            }

            if(Obj_UserVm.PortalUser.ClientViewGroupId > 1)
            {
                Obj_UserVm.HasGroupAccess = true;
            }
        }
    }
}
