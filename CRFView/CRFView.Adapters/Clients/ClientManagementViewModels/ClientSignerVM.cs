﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CRFView.Adapters.Clients.Client_Management;

namespace CRFView.Adapters.Clients.ClientManagementViewModels
{
    public class ClientSignerVM
    {
        public ADClientSigners Obj_ClientSigner { get; set; }

        public ClientSignerVM()
        {
            Obj_ClientSigner = new ADClientSigners();
        }
    }
}
