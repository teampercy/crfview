﻿using CRFView.Adapters.Clients.Client_Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Clients.ClientManagementViewModels
{
    public class ClientLienInfoVM
    {
        public ADClientLienInfo obj_ADClientLienInfo { get; set; }
        public int ClientId { get; set; }
    }
}
