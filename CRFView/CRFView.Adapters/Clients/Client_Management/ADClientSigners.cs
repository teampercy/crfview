﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using CRF.BLL.Clients;
using CRFView.Adapters.Clients.Client_Management;

namespace CRFView.Adapters.Clients.Client_Management
{
    public class ADClientSigners
    {
        #region Properties
        public int Id { get; set; }
        public int ClientId { get; set; }
        public string Signer { get; set; }
        public string SignerTitle { get; set; }
        public bool IsDupped { get; set; }
        public byte[] Signature { get; set; }
        public string SignerEmail { get; set; }
        #endregion

        public static ADClientSigners LoadSigner(int id)
        {
            try
            {
                ADClientSigners Obj_ADPortalUsers = new ADClientSigners();
                var myitem = new ClientSigner();
                ITable myitemItable = myitem;
                DBO.Provider.Read(id.ToString(), ref myitemItable);
                myitem = (ClientSigner)myitemItable;
                AutoMapper.Mapper.CreateMap<ClientSigner, ADClientSigners>();
                return (AutoMapper.Mapper.Map<ADClientSigners>(myitem));
            }
            catch (Exception ex)
            {
                return new Client_Management.ADClientSigners();
            }
        }

        public static bool SaveUpdateSigner(ADClientSigners ObjSource)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADClientSigners, ClientSigner>();
                ITable tblSigner;
                if (ObjSource.Id != 0)
                {
                    ADClientSigners loadClientSigner = LoadSigner(Convert.ToInt32(ObjSource.Id));
                    loadClientSigner.Signer = ObjSource.Signer;
                    loadClientSigner.SignerTitle = ObjSource.SignerTitle;
                    loadClientSigner.SignerEmail = ObjSource.SignerEmail;
                    loadClientSigner.Signature = ObjSource.Signature;
                    ClientSigner objClientSigners = AutoMapper.Mapper.Map<ClientSigner>(loadClientSigner);
                    tblSigner = objClientSigners;
                    DBO.Provider.Update(ref tblSigner);
                }
                else
                {
                    ClientSigner objClientSigner = AutoMapper.Mapper.Map<ClientSigner>(ObjSource);
                    tblSigner = objClientSigner;
                    DBO.Provider.Create(ref tblSigner);

                }
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public static bool DeleteSigner(string Id)
        {
            try
            {
                ITable ISigner;
                ClientSigner objSign = new ClientSigner();
                objSign.Id = Convert.ToInt32(Id);
                ISigner = objSign;
                DBO.Provider.Delete(ref ISigner);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
    }
}
