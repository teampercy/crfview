﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Clients.Client_Management
{
    public class ADvwclientscheduledreports
    {
        #region Properties
        public int Id                {get;set;}
        public bool Inactive          {get;set;}
        public int  ClientId          {get;set;}
        public int ScheduledReportId {get;set;}
        public string taskname          {get;set;}
        public bool isdisabled        {get;set;}
        public string EmailTo           {get;set;}
        #endregion


    }
}
