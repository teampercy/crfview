﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using CRF.BLL.Clients;
using CRFView.Adapters.Clients.Client_Management;
using System.IO;
using System.Text.RegularExpressions;

namespace CRFView.Adapters
{
    public class ADClient
    {
        #region Properties
        public int ClientId { get; set; }
        public int ParentClientId { get; set; }
        public string ClientCode { get; set; }
        public string BranchNumber { get; set; }
        public string ClientName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public string PhoneExt { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public string Note { get; set; }
        public System.DateTime? SetupDate { get; set; }
        public System.DateTime? NextContactDate { get; set; } 
        public bool IsActive { get; set; }
        public int ClientTypeId { get; set; }
        public int ClientStatusId { get; set; }
        public bool IsBranch { get; set; }
        public int ServiceTeamId { get; set; }
        public int SalesTeamId { get; set; }
        public string ProdTeam { get; set; }
        public string ContactName { get; set; }
        public string ContactTitle { get; set; }
        public string ContactSalutation { get; set; }
        public System.DateTime? ContactDOB { get; set; }
        public string ContactName2 { get; set; }
        public string ContactTitle2 { get; set; }
        public string ContactSalutation2 { get; set; }
        public string ContactEmail2 { get; set; }
        public string ContactPhoneNo2 { get; set; }
        public System.DateTime? ContactDOB2 { get; set; }
        public string LatUpdatedBy { get; set; }
        public System.DateTime? LastUpdateDate { get; set; }
        public string CCEmail { get; set; }
        public bool IsLienClient { get; set; } = false;
        public bool IsCollectionClient { get; set; } = false;
        public string ContractPhoneNo2Ext { get; set; }
        public bool IsInactive { get; set; }
        public bool IsRentalCo { get; set; }
        public string NTOContact { get; set; }
        public string NTOContactTitle { get; set; }
        public string NTOAdd1 { get; set; }
        public string NTOAdd2 { get; set; }
        public string NTOCity { get; set; }
        public string NTOState { get; set; }
        public string NTOZip { get; set; }
        public string NTOEmail { get; set; }
        public string NTOPhone { get; set; }
        public string NTOFax { get; set; }
        public string NTOAs { get; set; }
        public string BranchAs { get; set; }
        public bool HasLienAgreement { get; set; }
        public bool HasCollectAgreement { get; set; }
        public bool HasPrelimAgreement { get; set; }
        public string NTOPhoneWeb { get; set; }
        public string TaxId { get; set; }
        public System.DateTime? EvaluationDate { get; set; }
        public System.DateTime? CommissionDate { get; set; }

        /// To display Main CLient Code
        public string MainClientCode { get; set; }
        #endregion

        #region CRUD

        public static ADClient GetClientDetails(int id)
        {
            try
            {
                ADClient Obj_ADClient = new ADClient();
                var myitem = new Client();
                ITable myitemItable = myitem;
                DBO.Provider.Read(id.ToString(), ref myitemItable);
                myitem = (Client)myitemItable;
                AutoMapper.Mapper.CreateMap<Client, ADClient>();
                return (AutoMapper.Mapper.Map<ADClient>(myitem));
            }
            catch (Exception ex)
            {
                return new ADClient();
            }
        }

        public static bool DeleteClientWithId(string Id)
        {
            try
            {
                ITable IClient;
                Client objClient = new Client();
                objClient.ClientId = Convert.ToInt32(Id);
                IClient = objClient;
                DBO.Provider.Delete(ref IClient);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        // default list to be shown on index page
        public List<ADClient> ListClient()
        {
            try
            {
                TableView entityList;
                uspbo_CRM_GetClientList Sp_OBJ = new uspbo_CRM_GetClientList();
                Sp_OBJ.IssueFilter = 0;
                Sp_OBJ.InactiveFilter = 0;
                entityList =DBO.Provider.GetTableView(Sp_OBJ);
                DataTable dt = entityList.ToTable();
                List<ADClient> ClientList = (from DataRow dr in dt.Rows
                                             select new ADClient()
                                             {
                                                           ClientId = Convert.ToInt32(dr["ClientId"]),
                                                           MainClientCode = ((dr["MainClientCode"] != DBNull.Value ?dr["MainClientCode"].ToString() : "")),
                                                           ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                                           ClientName = ((dr["ClientName"] != DBNull.Value ? dr["ClientName"].ToString() : "")),
                                                           ContactName = ((dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : "")),
                                                           City = ((dr["City"] != DBNull.Value ? dr["City"].ToString() : "")),
                                                           State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),
                                                           NextContactDate = ((dr["NextContactDate"] != DBNull.Value ? Convert.ToDateTime(dr["NextContactDate"]) : DateTime.MinValue)),
                                                           EvaluationDate = ((dr["EvaluationDate"] != DBNull.Value ? Convert.ToDateTime(dr["EvaluationDate"]) : DateTime.MinValue)),
                                                           PhoneNo = ((dr["PhoneNo"] != DBNull.Value ? dr["PhoneNo"].ToString() : "")),
                                                           IsInactive = ((dr["IsInactive"] != DBNull.Value ? Convert.ToBoolean(dr["IsInactive"]) : false))                                           
                                                       }).ToList();
                return ClientList;
            }
            catch (Exception ex)
            {
                return (new List<ADClient>());
            }
        }

        // filtertrd list
        public List<ADClient> FilteredListClient(FilterVM FilterVM_Obj, CurrentUser CU_Obj=null)
        {
            try
            {
                TableView entityList;
                uspbo_CRM_GetClientList Sp_OBJ = new uspbo_CRM_GetClientList();
                Sp_OBJ.IssueFilter = 0;
                Sp_OBJ.InactiveFilter = 0;

                if((FilterVM_Obj.ClientCode!=null) &&(FilterVM_Obj.ClientCode.Length>0))
                {
                    if(FilterVM_Obj.RbtClientCodeValue!= "Main")
                    {
                        Sp_OBJ.ClientCode = FilterVM_Obj.ClientCode + "%";
                    }
                    else
                    {
                        Sp_OBJ.MainClientCode = FilterVM_Obj.ClientCode + "%";
                    }               
                }

                if ((FilterVM_Obj.ClientName != null) && (FilterVM_Obj.ClientName.Length > 0))
                {
                    if (FilterVM_Obj.RbtClientNameValue == "Includes")
                    {
                        Sp_OBJ.ClientName = "%"+FilterVM_Obj.ClientName + "%";
                    }
                    else
                    {
                        Sp_OBJ.ClientName = FilterVM_Obj.ClientName + "%";
                    }
                }

                if((FilterVM_Obj.TEAMCODE != null) && ( FilterVM_Obj.TEAMCODE.Length>0))
                {
                    Sp_OBJ.TEAMCODE = FilterVM_Obj.TEAMCODE.Trim();
                }

                if((FilterVM_Obj.ContactName != null) && (FilterVM_Obj.ContactName.Length>0))
                {
                    if(FilterVM_Obj.RbtContactNameValue == "Includes")
                    {
                        Sp_OBJ.ContactName="%"+ FilterVM_Obj .ContactName+ "%";
                    }
                    else
                    {
                        Sp_OBJ.ContactName = FilterVM_Obj.ContactName + "%";
                    }
                }

                if ((FilterVM_Obj.ContactCity != null) && ( FilterVM_Obj.ContactCity.Length > 0))
                {
                    if (FilterVM_Obj.RbtContactCityValue == "Includes")
                    {
                        Sp_OBJ.ContactCity = "%" + FilterVM_Obj.ContactCity + "%";
                    }
                    else
                    {
                        Sp_OBJ.ContactCity = FilterVM_Obj.ContactCity + "%";
                    }
                }

                if ((FilterVM_Obj.ContactState != null) && ( FilterVM_Obj.ContactState.Length > 0))
                {
                    if (FilterVM_Obj.RbtContactStateValue == "Includes")
                    {
                        Sp_OBJ.ContactState = "%" + FilterVM_Obj.ContactState + "%";
                    }
                    else
                    {
                        Sp_OBJ.ContactState = FilterVM_Obj.ContactState + "%";
                    }
                }

                if(FilterVM_Obj.ChkNextContactDate==true)
                {
                    string VerifyDate = FilterVM_Obj.NextContactDateTO.ToShortDateString();
                    if (ValidateDate(VerifyDate))
                    {
                        Sp_OBJ.NextContactDateTO = FilterVM_Obj.NextContactDateTO.ToShortDateString();
                    }
                }

                if ( FilterVM_Obj.ChkNEvaluationtDate == true)
                {
                    string VerifyDate = FilterVM_Obj.EvaluationDateTO.ToShortDateString();
                    if (ValidateDate(VerifyDate))
                    {
                        Sp_OBJ.EvaluationDateTO = FilterVM_Obj.EvaluationDateTO.ToShortDateString();
                    }
                }
                Sp_OBJ.IssueFilter = 0;
                if(FilterVM_Obj.IssueValue=="1")
                {
                    Sp_OBJ.IssueFilter = 1;
                }
                if(FilterVM_Obj.IssueValue=="2")
                {
                    Sp_OBJ.IssueFilter = 2;
                    if(CU_Obj!=null)
                    {
                        Sp_OBJ.IssuesAssignedToId = CU_Obj.Id;
                    }
                }
                if(FilterVM_Obj.ChkInactives==true)
                {
                    Sp_OBJ.InactiveFilter = 1;
                }
                else
                {
                    Sp_OBJ.InactiveFilter = 0;
                }

                CurrentUser objCurrentUser=new CurrentUser();
               // objCurrentUser = Session["LoggedInUser"];


                entityList = DBO.Provider.GetTableView(Sp_OBJ);
                DataTable dt = entityList.ToTable();
                List<ADClient> ClientList = (from DataRow dr in dt.Rows
                                             select new ADClient()
                                             {
                                                 ClientId = Convert.ToInt32(dr["ClientId"]),
                                                 MainClientCode = ((dr["MainClientCode"] != DBNull.Value ? dr["MainClientCode"].ToString() : "")),
                                                 ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                                 ClientName = ((dr["ClientName"] != DBNull.Value ? dr["ClientName"].ToString() : "")),
                                                 ContactName = ((dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : "")),
                                                 City = ((dr["City"] != DBNull.Value ? dr["City"].ToString() : "")),
                                                 State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),
                                                 NextContactDate = ((dr["NextContactDate"] != DBNull.Value ? Convert.ToDateTime(dr["NextContactDate"]) : DateTime.MinValue)),
                                                 EvaluationDate = ((dr["EvaluationDate"] != DBNull.Value ? Convert.ToDateTime(dr["EvaluationDate"]) : DateTime.MinValue)),
                                                 PhoneNo = ((dr["PhoneNo"] != DBNull.Value ? dr["PhoneNo"].ToString() : "")),
                                                 IsInactive = ((dr["IsInactive"] != DBNull.Value ? Convert.ToBoolean(dr["IsInactive"]) : false))
                                             }).ToList();
                return ClientList;
            }
            catch (Exception ex)
            {
                return (new List<ADClient>());
            }
        }

        // check if client code already exists while insertung a new client. 
        public bool IsDuplicateClient(string Clientcode)
        {
            string sql = "Select ClientCode from Client Where ClientCode = '" + Clientcode + "'";
            System.Data.DataSet ds = DBO.Provider.GetDataSet(sql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            } 
        }

        // create a new client
        public int SaveNewClient(NewClientVM NewClientVM_obj, CurrentUser CU_Obj)
        {
            try
            {
                ADClient ADClient_Obj = NewClientVM_obj.ADC_obj;
                if (NewClientVM_obj.ChkEvalDate == false)
                {
                    ADClient_Obj.EvaluationDate = null;
                }
                if (NewClientVM_obj.ChkFollowUp == false)
                {
                    ADClient_Obj.NextContactDate = null;
                }
                AutoMapper.Mapper.CreateMap<ADClient, Client>();
                ITable tblClient;
                Client Client_Obj = AutoMapper.Mapper.Map<Client>(ADClient_Obj);
                tblClient = (ITable)Client_Obj;
                DBO.Provider.Create(ref tblClient);
                //uspbo_CRM_CreateClient MYSPROC = new uspbo_CRM_CreateClient();
                //MYSPROC.ClientId = Client_Obj.ClientId;

                uspbo_CRM_CreateClient obj_SP = new uspbo_CRM_CreateClient();
                var returnValue = obj_SP.ClientId = Client_Obj.ClientId;


                AutoMapper.Mapper.CreateMap<CurrentUser,CRF.BLL.Users.CurrentUser>();
                CRF.BLL.Users.CurrentUser CerrUsr_Obj = AutoMapper.Mapper.Map<CRF.BLL.Users.CurrentUser>(CU_Obj);
                TableView EntityList = Processes.CreateNewClient(CerrUsr_Obj, obj_SP);
                return returnValue;

            }
            catch(Exception ex)
            {
                return 0;
            }
        }

        // Load client for editng
        public ClientEditVM LoadClientData(CurrentUser CU_Obj,int id)
        {
            try
            {
                ClientEditVM ClientEditVM_Obj = new ClientEditVM();
                AutoMapper.Mapper.CreateMap<CurrentUser, CRF.BLL.Users.CurrentUser>();
                CRF.BLL.Users.CurrentUser CerrUsr_Obj = AutoMapper.Mapper.Map<CRF.BLL.Users.CurrentUser>(CU_Obj);
                CRF.BLL.Clients.ClientInfo myentityinfo = CRF.BLL.Clients.Processes.GetClientInfo(CerrUsr_Obj, id);
                TableView LienTableView = myentityinfo.LienInfo();
                DataTable dtLien = LienTableView.ToTable();
                if (dtLien.Rows.Count >=1)
                {
                    
                    ClientEditVM_Obj.ClientLienInfoId = Convert.ToInt32(dtLien.Rows[0]["Id"]);
                }
                TableView CollectionTableView = myentityinfo.CollectionInfo();
                DataTable dtCollection = CollectionTableView.ToTable();
                if (dtCollection.Rows.Count>=1)
                {
                    ClientEditVM_Obj.ClientCollectionInfoId = Convert.ToInt32(dtCollection.Rows[0]["Id"]);
                }
                TableView ClientImageTableView = myentityinfo.Logo();
                DataTable dtClientImage = ClientImageTableView.ToTable();
                if (dtClientImage.Rows.Count >= 1)
                {
                    ClientEditVM_Obj.ClientImageId = Convert.ToInt32(dtClientImage.Rows[0]["Id"]);
                }
                else
                {
                    ClientEditVM_Obj.ClientImageId = 0;
                }
                TableView UsersView = myentityinfo.Users();
                DataTable dtUsers = UsersView.ToTable();
                if (dtUsers.Rows.Count >= 1)
                {

                }

                uspbo_CRM_GetClientInfo mysproc = new uspbo_CRM_GetClientInfo();
                mysproc.ClientId =Convert.ToString(id);
                DataSet ds = DBO.Provider.GetDataSet(mysproc);
                DataTable dt_client = ds.Tables[0];
                ClientEditVM_Obj.Obj_ADClient = DataTableToADClient(ds.Tables[0]);
                return ClientEditVM_Obj;
            }
            catch(Exception ex)
            {
               // byte[] bytes = File.ReadAllBytes("");
                return (new ClientEditVM());
            }

        }

        // Update Client Data
        public static bool UpdateClient(ADClient Obj_Cl)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADClient, Client>();
                ITable tblClient;
                Client objClient;
                objClient = AutoMapper.Mapper.Map<Client>(Obj_Cl);
                tblClient = (ITable)objClient;
                DBO.Provider.Update(ref tblClient);
                var mysproc = new uspbo_CRM_UpdateClientNTOAddr();
                mysproc.ClientId = objClient.ClientId.ToString();
                DBO.Provider.ExecNonQuery(mysproc);
                return true;
            }
            catch(Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }

        public static string CreateUpdateClientBranch(ADClient ObjSource)
        {
            try
            {
                ITable tblClient;
                string RecentClientId;
                Client recent = new Client();
                AutoMapper.Mapper.CreateMap<ADClient, Client>();
                if (ObjSource.ClientId !=0)
                {
                    ADClient loadClientBranch = GetClientDetails(Convert.ToInt32(ObjSource.ClientId));

                    loadClientBranch.BranchNumber = ObjSource.BranchNumber;
                    loadClientBranch.ClientName = ObjSource.ClientName;
                    loadClientBranch.AddressLine1 = ObjSource.AddressLine1;
                    loadClientBranch.AddressLine2 = ObjSource.AddressLine2;
                    loadClientBranch.City = ObjSource.City;
                    loadClientBranch.State = ObjSource.State;
                    loadClientBranch.PostalCode = ObjSource.PostalCode;
                    loadClientBranch.PhoneNo = ObjSource.PhoneNo;
                    loadClientBranch.PhoneExt = ObjSource.PhoneExt;
                    loadClientBranch.Fax = ObjSource.Fax;
                    loadClientBranch.Email = ObjSource.Email;
                    loadClientBranch.ContactName = ObjSource.ContactName;
                    loadClientBranch.ContactSalutation = ObjSource.ContactSalutation;
                    loadClientBranch.ContactTitle = ObjSource.ContactTitle;
                    loadClientBranch.ContactDOB = ObjSource.ContactDOB;
                    loadClientBranch.ContactName2 = ObjSource.ContactName2;
                    loadClientBranch.ContactSalutation2 = ObjSource.ContactSalutation2;
                    loadClientBranch.ContactTitle2 = ObjSource.ContactTitle2;
                    loadClientBranch.ContactEmail2 = ObjSource.ContactEmail2;
                    loadClientBranch.ContactPhoneNo2 = ObjSource.ContactPhoneNo2;
                    loadClientBranch.ContractPhoneNo2Ext = ObjSource.ContractPhoneNo2Ext;
                    loadClientBranch.ContactDOB2 = ObjSource.ContactDOB2;
                    loadClientBranch.Note = ObjSource.Note;

                    Client objClient = AutoMapper.Mapper.Map<Client>(loadClientBranch);
                    tblClient = objClient;
                    DBO.Provider.Update(ref tblClient);
                } 
                else
                {
                    ObjSource.IsBranch = true;               
                    Client objClient = new Client();
                    objClient = AutoMapper.Mapper.Map<Client>(ObjSource);
                    tblClient = (ITable)objClient;
                    DBO.Provider.Create(ref tblClient);
                   
                }
                recent = (Client)tblClient;
                RecentClientId = recent.ClientId.ToString();
                return RecentClientId;
            }
            catch(Exception ex)
            {
                return "";
            }
        } 

        // Load client users
        public static List<ADPortalUsers> GetUsersForId(CurrentUser CU_Obj, int id)
        {
            AutoMapper.Mapper.CreateMap<CurrentUser, CRF.BLL.Users.CurrentUser>();
            CRF.BLL.Users.CurrentUser CurrUsr_Obj = AutoMapper.Mapper.Map<CRF.BLL.Users.CurrentUser>(CU_Obj);
            CRF.BLL.Clients.ClientInfo myentityinfo = CRF.BLL.Clients.Processes.GetClientInfo(CurrUsr_Obj, id);
            TableView ViewUsers = myentityinfo.Users();
            DataTable dtUsers = ViewUsers.ToTable();
            List<ADPortalUsers> UsersList = new List<ADPortalUsers>();
            if (dtUsers.Rows.Count >= 1)
            {
                UsersList = (from DataRow dr in dtUsers.Rows
                             select new ADPortalUsers()
                             {
                                 ID = Convert.ToInt32(dr["ID"]),
                                 UserName = ((dr["UserName"] != DBNull.Value ? dr["UserName"].ToString() : "")),
                                 LoginCode = ((dr["LoginCode"] != DBNull.Value ? dr["LoginCode"].ToString() : "")),
                                 Email = ((dr["Email"] != DBNull.Value ? dr["Email"].ToString() : "")),
                                 PhoneNo = (dr["PhoneNo"] != DBNull.Value ? dr["PhoneNo"].ToString() : ""),
                                 DOB = (dr["DOB"] != DBNull.Value ? Convert.ToDateTime( dr["DOB"]) : DateTime.MinValue),
                                 isinactive = ((dr["isinactive"] != DBNull.Value ? Convert.ToBoolean(dr["isinactive"]) : false)),
                                 LastLoginDate = (dr["LastLoginDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastLoginDate"]) : DateTime.MinValue)
                             }).ToList();
            }
            return UsersList;
        }

        // Load Client Contacts
        public static List<ADClientContact> GetContactsForId(CurrentUser CU_Obj, int id)
        {
            AutoMapper.Mapper.CreateMap<CurrentUser, CRF.BLL.Users.CurrentUser>();
            CRF.BLL.Users.CurrentUser CurrUsr_Obj = AutoMapper.Mapper.Map<CRF.BLL.Users.CurrentUser>(CU_Obj);
            CRF.BLL.Clients.ClientInfo myentityinfo = CRF.BLL.Clients.Processes.GetClientInfo(CurrUsr_Obj, id);
            TableView ViewContacts = myentityinfo.Contacts();
            DataTable dtContacts = ViewContacts.ToTable();
            List<ADClientContact> ContactsList = new List<ADClientContact>();
            if (dtContacts.Rows.Count >= 1)
            {
                ContactsList = (from DataRow dr in dtContacts.Rows
                             select new ADClientContact()
                             {
                                 ClientContactId = Convert.ToInt32(dr["ClientContactId"]),
                                 ContactName = ((dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : "")),
                                 ContactTitle = ((dr["ContactTitle"] != DBNull.Value ? dr["ContactTitle"].ToString() : "")),
                                 Telephone1 = ((dr["Telephone1"] != DBNull.Value ? dr["Telephone1"].ToString() : "")),
                                 Email = ((dr["Email"] != DBNull.Value ? dr["Email"].ToString() : "")),
                                 DOB = (dr["DOB"] != DBNull.Value ? Convert.ToDateTime(dr["DOB"]) : DateTime.MinValue)
                             }).ToList();
            }
            return ContactsList;
        }

        // Load Client Signers
        public static List<ADClientSigner> GetSignersForId(CurrentUser CU_Obj, int id)
        {
            AutoMapper.Mapper.CreateMap<CurrentUser, CRF.BLL.Users.CurrentUser>();
            CRF.BLL.Users.CurrentUser CurrUsr_Obj = AutoMapper.Mapper.Map<CRF.BLL.Users.CurrentUser>(CU_Obj);
            CRF.BLL.Clients.ClientInfo myentityinfo = CRF.BLL.Clients.Processes.GetClientInfo(CurrUsr_Obj, id);
            TableView ViewSigners = myentityinfo.Signers();
            DataTable dtSigners = ViewSigners.ToTable();
            List<ADClientSigner> SignersList = new List<ADClientSigner>();
            if (dtSigners.Rows.Count >= 1)
            {
                SignersList = (from DataRow dr in dtSigners.Rows
                                select new ADClientSigner()
                                {
                                    Id = Convert.ToInt32(dr["Id"]),
                                    Signer = ((dr["Signer"] != DBNull.Value ? dr["Signer"].ToString() : "")),
                                    SignerTitle = ((dr["SignerTitle"] != DBNull.Value ? dr["SignerTitle"].ToString() : "")),
                                    SignerEmail = ((dr["SignerEmail"] != DBNull.Value ? dr["SignerEmail"].ToString() : ""))
                                }).ToList();
            }
            return SignersList;
        }


        public static List<ADvwIssues> GetIssuesForId(CurrentUser CU_Obj, int id, string IssueType)
        {
            AutoMapper.Mapper.CreateMap<CurrentUser, CRF.BLL.Users.CurrentUser>();
            CRF.BLL.Users.CurrentUser CurrUsr_Obj = AutoMapper.Mapper.Map<CRF.BLL.Users.CurrentUser>(CU_Obj);
            CRF.BLL.Clients.ClientInfo myentityinfo = CRF.BLL.Clients.Processes.GetClientInfo(CurrUsr_Obj, id);
            DataTable dtIssues;
            if (IssueType == "closed")
            {
                TableView ViewClosedIssues = myentityinfo.ClosedIssues();
                dtIssues = ViewClosedIssues.ToTable();
            }
            else 
            {
                TableView ViewOpenIssues = myentityinfo.OpenIssues();
                dtIssues = ViewOpenIssues.ToTable();
            }
            List<ADvwIssues> IssueList = new List<ADvwIssues>();
            if (dtIssues.Rows.Count >= 1)
            {
                IssueList = (from DataRow dr in dtIssues.Rows
                               select new ADvwIssues()
                               {
                                   Id = Convert.ToInt32(dr["Id"]),
                                   IssueDescription = ((dr["IssueDescription"] != DBNull.Value ? StripHTML(dr["IssueDescription"].ToString()) : "")),
                                   DateCreated = ((dr["DateCreated"] != DBNull.Value ? Convert.ToDateTime( dr["DateCreated"]) : DateTime.MinValue)),
                                   CreatedBy = ((dr["CreatedBy"] != DBNull.Value ?  dr["CreatedBy"].ToString() : "")),
                                   DateClosed = ((dr["DateClosed"] != DBNull.Value ? Convert.ToDateTime(dr["DateClosed"]) : DateTime.MinValue)),
                                   AssignedTo = ((dr["AssignedTo"] != DBNull.Value ? dr["AssignedTo"].ToString() : ""))
                               }).ToList();
            }
            return IssueList;
        }

        public static List<ADvwclientscheduledreports> GetScheduledReportsForId(CurrentUser CU_Obj, int id)
        {
            AutoMapper.Mapper.CreateMap<CurrentUser, CRF.BLL.Users.CurrentUser>();
            CRF.BLL.Users.CurrentUser CurrUsr_Obj = AutoMapper.Mapper.Map<CRF.BLL.Users.CurrentUser>(CU_Obj);
            CRF.BLL.Clients.ClientInfo myentityinfo = CRF.BLL.Clients.Processes.GetClientInfo(CurrUsr_Obj, id);
            TableView ViewScheduledReports = myentityinfo.ScheduledReports();
            DataTable dtScheduledReports = ViewScheduledReports.ToTable();
            List<ADvwclientscheduledreports> ReportList = new List<ADvwclientscheduledreports>();
            if (dtScheduledReports.Rows.Count >= 1)
            {
                ReportList = (from DataRow dr in dtScheduledReports.Rows
                               select new ADvwclientscheduledreports()
                               {
                                   Id = Convert.ToInt32(dr["Id"]),
                                   ClientId = (Convert.ToInt32( dr["ClientId"])),
                                   Inactive = ((dr["Inactive"] != DBNull.Value ? Convert.ToBoolean(dr["Inactive"]) : false)),
                                   ScheduledReportId = Convert.ToInt32( dr["ScheduledReportId"]),
                                   taskname = ((dr["taskname"] != DBNull.Value ? dr["taskname"].ToString() : "")),
                                   isdisabled = ((dr["isdisabled"] != DBNull.Value ? Convert.ToBoolean(dr["isdisabled"]) : false)),
                                   EmailTo = ((dr["EmailTo"] != DBNull.Value ? dr["EmailTo"].ToString() : ""))
                               }).ToList();
            }
            return ReportList;
        }
        
        #endregion

        #region Local Methods
        private bool ValidateDate(string date)
        {
            try
            {
                // for US, alter to suit if splitting on hyphen, comma, etc.
                string[] dateParts = date.Split('/');

                // create new date from the parts; if this does not fail
                // the method will return true and the date is valid
                DateTime testDate = new
                    DateTime(Convert.ToInt32(dateParts[2]),
                    Convert.ToInt32(dateParts[0]),
                    Convert.ToInt32(dateParts[1]));

                return true;
            }
            catch
            {
                // if a test date cannot be created, the
                // method will return false
                return false;
            }
        }

        private ADClient DataTableToADClient(DataTable dt)
        {
            List<ADClient> ClientList = (from DataRow dr in dt.Rows
                                         select new ADClient()
                                         {
                                             ClientId = Convert.ToInt32(dr["ClientId"]),
                                             ParentClientId = Convert.ToInt32(dr["ParentClientId"]),
                                             ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                             ClientName = ((dr["ClientName"] != DBNull.Value ? dr["ClientName"].ToString() : "")),
                                             BranchNumber = ((dr["BranchNumber"] != DBNull.Value ? dr["BranchNumber"].ToString() : "")),
                                             ContactName = ((dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : "")),
                                             AddressLine1 = ((dr["AddressLine1"] != DBNull.Value ? dr["AddressLine1"].ToString() : "")),
                                             AddressLine2 = ((dr["AddressLine2"] != DBNull.Value ? dr["AddressLine2"].ToString() : "")),
                                             City = ((dr["City"] != DBNull.Value ? dr["City"].ToString() : "")),
                                             State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),
                                             PostalCode = ((dr["PostalCode"] != DBNull.Value ? dr["PostalCode"].ToString() : "")),
                                             Email = ((dr["Email"] != DBNull.Value ? dr["Email"].ToString() : "")),
                                             PhoneNo = ((dr["PhoneNo"] != DBNull.Value ? dr["PhoneNo"].ToString() : "")),
                                             PhoneExt = ((dr["PhoneExt"] != DBNull.Value ? dr["PhoneExt"].ToString() : "")),
                                             Mobile = ((dr["Mobile"] != DBNull.Value ? dr["Mobile"].ToString() : "")),
                                             Fax = ((dr["Fax"] != DBNull.Value ? dr["Fax"].ToString() : "")),
                                             Note = ((dr["Note"] != DBNull.Value ? dr["Note"].ToString() : "")),
                                             NextContactDate = ((dr["NextContactDate"] != DBNull.Value ? Convert.ToDateTime(dr["NextContactDate"]) : DateTime.MinValue)),
                                             SetupDate = ((dr["SetupDate"] != DBNull.Value ? Convert.ToDateTime(dr["SetupDate"]) : DateTime.MinValue)),
                                             IsActive = ((dr["IsActive"] != DBNull.Value ? Convert.ToBoolean( dr["IsActive"]) : false)),
                                             ClientTypeId = ((dr["ClientTypeId"] != DBNull.Value ? Convert.ToInt32(dr["ClientTypeId"]) : 0)),
                                             ClientStatusId = Convert.ToInt32(dr["ClientStatusId"]),
                                             IsBranch = ((dr["IsBranch"] != DBNull.Value ? Convert.ToBoolean(dr["IsBranch"]) : false)),
                                             ServiceTeamId = ((dr["ServiceTeamId"] != DBNull.Value ? Convert.ToInt32(dr["ServiceTeamId"]) : 0)),
                                             SalesTeamId = ((dr["SalesTeamId"] != DBNull.Value ? Convert.ToInt32(dr["SalesTeamId"]) : 0)),
                                             ProdTeam = (dr["ProdTeam"] != DBNull.Value ? dr["ProdTeam"].ToString() : ""),
                                             ContactSalutation = (dr["ContactSalutation"] != DBNull.Value ? dr["ContactSalutation"].ToString() : ""),
                                             EvaluationDate = ((dr["EvaluationDate"] != DBNull.Value ? Convert.ToDateTime(dr["EvaluationDate"]) : DateTime.MinValue)),
                                             ContactName2 = (dr["ContactName2"] != DBNull.Value ? dr["ContactName2"].ToString() : ""),
                                             ContactTitle2 = (dr["ContactTitle2"] != DBNull.Value ? dr["ContactTitle2"].ToString() : ""),
                                             ContactSalutation2 = (dr["ContactSalutation2"] != DBNull.Value ? dr["ContactSalutation2"].ToString() : ""),
                                             ContactEmail2 = (dr["ContactEmail2"] != DBNull.Value ? dr["ContactEmail2"].ToString() : ""),
                                             ContactPhoneNo2 = (dr["ContactPhoneNo2"] != DBNull.Value ? dr["ContactPhoneNo2"].ToString() : ""),
                                             ContactDOB2 = ((dr["ContactDOB2"] != DBNull.Value ? Convert.ToDateTime(dr["ContactDOB2"]) : DateTime.MinValue)),
                                             LatUpdatedBy = (dr["LatUpdatedBy"] != DBNull.Value ? dr["LatUpdatedBy"].ToString() : ""),
                                             LastUpdateDate = ((dr["LastUpdateDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastUpdateDate"]) : DateTime.MinValue)),
                                             CCEmail = (dr["CCEmail"] != DBNull.Value ? dr["CCEmail"].ToString() : ""),
                                             IsLienClient = ((dr["IsLienClient"] != DBNull.Value ? Convert.ToBoolean(dr["IsLienClient"]) : false)),
                                             IsCollectionClient = ((dr["IsCollectionClient"] != DBNull.Value ? Convert.ToBoolean(dr["IsCollectionClient"]) : false)),
                                             ContractPhoneNo2Ext = (dr["ContractPhoneNo2Ext"] != DBNull.Value ? dr["ContractPhoneNo2Ext"].ToString() : ""),
                                             IsInactive = ((dr["IsInactive"] != DBNull.Value ? Convert.ToBoolean(dr["IsInactive"]) : false)),
                                             IsRentalCo = ((dr["IsRentalCo"] != DBNull.Value ? Convert.ToBoolean(dr["IsRentalCo"]) : false)),
                                             NTOContact = (dr["NTOContact"] != DBNull.Value ? dr["NTOContact"].ToString() : ""),
                                             NTOContactTitle = (dr["NTOContactTitle"] != DBNull.Value ? dr["NTOContactTitle"].ToString() : ""),
                                             NTOAdd1 = (dr["NTOAdd1"] != DBNull.Value ? dr["NTOAdd1"].ToString() : ""),
                                             NTOAdd2 = (dr["NTOAdd2"] != DBNull.Value ? dr["NTOAdd2"].ToString() : ""),
                                             NTOCity = (dr["NTOCity"] != DBNull.Value ? dr["NTOCity"].ToString() : ""),
                                             NTOState = (dr["NTOState"] != DBNull.Value ? dr["NTOState"].ToString() : ""),
                                             NTOZip = (dr["NTOZip"] != DBNull.Value ? dr["NTOZip"].ToString() : ""),
                                             NTOEmail = (dr["NTOEmail"] != DBNull.Value ? dr["NTOEmail"].ToString() : ""),
                                             NTOPhone = (dr["NTOPhone"] != DBNull.Value ? dr["NTOPhone"].ToString() : ""),
                                             NTOFax = (dr["NTOFax"] != DBNull.Value ? dr["NTOFax"].ToString() : ""),
                                             NTOAs = (dr["NTOAs"] != DBNull.Value ? dr["NTOAs"].ToString() : ""),
                                             BranchAs = (dr["BranchAs"] != DBNull.Value ? dr["BranchAs"].ToString() : ""),
                                             HasLienAgreement = ((dr["HasLienAgreement"] != DBNull.Value ? Convert.ToBoolean(dr["HasLienAgreement"]) : false)),
                                             HasCollectAgreement = ((dr["HasCollectAgreement"] != DBNull.Value ? Convert.ToBoolean(dr["HasCollectAgreement"]) : false)),
                                             HasPrelimAgreement = ((dr["HasPrelimAgreement"] != DBNull.Value ? Convert.ToBoolean(dr["HasPrelimAgreement"]) : false)),
                                             NTOPhoneWeb = (dr["NTOPhoneWeb"] != DBNull.Value ? dr["NTOPhoneWeb"].ToString() : ""),
                                             TaxId = (dr["TaxId"] != DBNull.Value ? dr["TaxId"].ToString() : ""),
                                             CommissionDate = ((dr["CommissionDate"] != DBNull.Value ? Convert.ToDateTime(dr["CommissionDate"]) : DateTime.MinValue)),
                                             ContactTitle = (dr["ContactTitle"] != DBNull.Value ? dr["ContactTitle"].ToString() : ""),
                                             ContactDOB = ((dr["ContactDOB"] != DBNull.Value ? Convert.ToDateTime(dr["ContactDOB"]) : DateTime.MinValue))
                                         }).ToList();
            return ClientList.First();
        }

        static string StripHTML(string inputString)
        {
            //return Regex.Replace
            //  (inputString, "<.*?>", string.Empty);
            string temp = Regex.Replace(inputString, "<.*?>", string.Empty);
            int cutLength = temp.Length;
            if(cutLength>65)
            {
                cutLength = 65;
            }
            temp = temp.Substring(0, cutLength) + " ....";
            return temp;
        }
        #endregion
    }
}
