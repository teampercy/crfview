﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using CRF.BLL.Clients;
using CRFView.Adapters.Clients.Client_Management;
using System.IO;
using System.Text.RegularExpressions;
using System.Text;

namespace CRFView.Adapters
{
   public  class ADClientReports
    {
        #region Properties
        public bool CollectionContacts { get; set; }
        public bool CollectViewUsers { get; set; }
        public bool LienContacts { get; set; }
        public bool LienViewUsers { get; set; }
        public bool TxContacts { get; set; }
        public bool OtherContacts { get; set; }

        #endregion

        #region CRUD Operations

        //Print client Reports
        public static string PrintClientReports(ContactListVM ObjContactList)
        {
            try
            {
                uspbo_CRM_GetEmailList1 objuspbo_CRM_GetEmailList1 = new uspbo_CRM_GetEmailList1();
                objuspbo_CRM_GetEmailList1.CollectionContacts = ObjContactList.chkCollection;
                objuspbo_CRM_GetEmailList1.CollectViewUsers = ObjContactList.chkCollectView;
                objuspbo_CRM_GetEmailList1.LienContacts = ObjContactList.chkLiens;
                objuspbo_CRM_GetEmailList1.LienViewUsers = ObjContactList.chkLienView;
                objuspbo_CRM_GetEmailList1.TxContacts = ObjContactList.chkTexas;
                objuspbo_CRM_GetEmailList1.OtherContacts = ObjContactList.chkOther;
                DataSet ds= CRF.BLL.Providers.DBO.Provider.GetDataSet(objuspbo_CRM_GetEmailList1);
                ExportColumns ex = new ExportColumns();
              //  string MYFILENAME = CommonFunctions.ExportToCSV(ds.Tables[0], "CONTACTLIST", ex);
               string result = ConvertToCSV(ds);
                return result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
        #endregion

        #region Local Methods
        public static string ConvertToCSV(DataSet objDataSet)
        {
            StringBuilder content = new StringBuilder();

            if (objDataSet.Tables.Count >= 1)
            {
                DataTable table = objDataSet.Tables[0];

                if (table.Rows.Count > 0)
                {
                    DataRow dr1 = (DataRow)table.Rows[0];
                    int intColumnCount = dr1.Table.Columns.Count;
                    int index = 1;

                    //add column names
                    foreach (DataColumn item in dr1.Table.Columns)
                    {
                        content.Append(String.Format("\"{0}\"", item.ColumnName));
                        if (index < intColumnCount)
                            content.Append(",");
                        else
                            content.Append("\r\n");
                        index++;
                    }

                    //add column data
                    foreach (DataRow currentRow in table.Rows)
                    {
                        string strRow = string.Empty;
                        for (int y = 0; y <= intColumnCount - 1; y++)
                        {
                            strRow += "\"" + currentRow[y].ToString() + "\"";

                            if (y < intColumnCount - 1 && y >= 0)
                                strRow += ",";
                        }
                        content.Append(strRow + "\r\n");
                    }
                }
            }

            return content.ToString();
        }
        
        public static bool SaveCSVFile(string CSVString, string path)
        {
            try
            {  
                byte[] array = Encoding.ASCII.GetBytes(CSVString);
                File.WriteAllBytes(path, array);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }

        }
        #endregion

    }
}
