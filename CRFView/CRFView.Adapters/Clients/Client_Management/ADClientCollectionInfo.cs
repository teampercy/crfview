﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using CRF.BLL.Clients;
namespace CRFView.Adapters.Clients.Client_Management
{
    public class ADClientCollectionInfo
    {
        #region Properties
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int ServiceTeamId { get; set; }
        public int SalesTeamId { get; set; }
        public int ProdTeamId { get; set; }
        public string LegalName { get; set; }
        public string Entity { get; set; }
        public string DBA { get; set; }
        public DateTime ExpireDate { get; set; }
        public string FBN { get; set; }
        public string LegalAddr1 { get; set; }
        public string LegalAddr2 { get; set; }
        public string LegalCity { get; set; }
        public string LegalState { get; set; }
        public string LegalZip { get; set; }
        public string ContactName { get; set; }
        public string ContactTitle { get; set; }
        public string ContactEmail { get; set; }
        public string ContactPhone { get; set; }
        public string ContactSalutation { get; set; }
        public DateTime ContactDOB { get; set; }
        public string ContactName2 { get; set; }
        public string ContactTitle2 { get; set; }
        public string ContactEmail2 { get; set; }
        public string ContactPhone2 { get; set; }
        public string ContactSalutation2 { get; set; }
        public DateTime ContactDOB2 { get; set; }
        public string InvoiceName { get; set; }
        public string InvoiceAttn { get; set; }
        public string InvoiceAddr1 { get; set; }
        public string InvoiceAddr2 { get; set; }
        public string InvoiceCity { get; set; }
        public string InvoiceState { get; set; }
        public string InvoiceZip { get; set; }
        public string InvoiceEmail { get; set; }
        public string InvoiceFax { get; set; }
        public string RemitName { get; set; }
        public string RemitAttn { get; set; }
        public string RemitAddr1 { get; set; }
        public string RemitAddr2 { get; set; }
        public string RemitCity { get; set; }
        public string RemitState { get; set; }
        public string RemitZip { get; set; }
        public string RemitEmail { get; set; }
        public string RemitFax { get; set; }
        public bool IsActive { get; set; }
        public bool BranchBox { get; set; }
        public bool NoEmail { get; set; }
        public bool NewBusVar { get; set; }
        public bool Visa { get; set; }
        public bool CCMC { get; set; }
        public bool Amex { get; set; }
        public bool Discover { get; set; }
        public bool SCAgreement { get; set; }
        public string LegalVars { get; set; }
        public string ReportVars { get; set; }
        public string MscVars { get; set; }
        public int ContactPlanId { get; set; }
        public bool IsNoCBR { get; set; }
        public int CBRDays { get; set; }
        public bool EmailNotifyPlacements { get; set; }
        public int ITDAvgAge { get; set; }
        public string LastUpdatedBy { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public string InvoiceContactTitle { get; set; }
        public string RemitContactTitle { get; set; }
        public DateTime SetupDate { get; set; }
        public string Venue { get; set; }
        public string ContactAddr1 { get; set; }
        public string ContactAddr2 { get; set; }
        public string ContactCity { get; set; }
        public string ContactState { get; set; }
        public string ContactZip { get; set; }
        public string InvoicePhone { get; set; }
        public string RemitPhone { get; set; }
        public bool WebNoteAccess { get; set; }
        public bool ClientAgentCode { get; set; }
        public bool MiscVar { get; set; }
        public string ContactPhoneExt { get; set; }
        public string ContactPhone2Ext { get; set; }
        public bool AttachDocsOnWeb { get; set; }
        public bool EmailBackup { get; set; }
        public bool EmailReports { get; set; }
        public string ContactCCEmail { get; set; }
        public string SCContactName { get; set; }
        public string SCContactTitle { get; set; }
        public string SCAddr1 { get; set; }
        public string SCAddr2 { get; set; }
        public string SCCity { get; set; }
        public string SCState { get; set; }
        public string SCZip { get; set; }
        public string SCPhone { get; set; }
        public string SCFax { get; set; }
        public string SCEmail { get; set; }
        public string FBNCounty { get; set; }
        public string VendorNumber { get; set; }
        public string NBVar { get; set; }
        public string EmailRptTo { get; set; }
        public bool IsRegAck { get; set; }
        public bool IsIndAck { get; set; }
        public bool IsStatusRpt { get; set; }
        public bool IsCloseReturn { get; set; }
        public string SCContactAs { get; set; }
        public string SIFAuthority { get; set; }
        public string ContactFax { get; set; }
        public string ContactFax2 { get; set; }
        public bool IsStatusGroup { get; set; }
        public bool IsCCNone { get; set; }
        #endregion

        #region CRUD
        public ADClientCollectionInfo LoadCollectionInfo(int? id)
        {
            try
            {
                ClientCollectionInfo myitem = new ClientCollectionInfo();
                ITable myentityItable = (ITable)myitem;
                if (id != null)
                {
                    DBO.Provider.Read(id.ToString(), ref myentityItable);
                }
                AutoMapper.Mapper.CreateMap<ClientCollectionInfo, ADClientCollectionInfo>();
                return (AutoMapper.Mapper.Map<ADClientCollectionInfo>(myitem));
            }
            catch (Exception ex)
            {
                return (new ADClientCollectionInfo());
            }
      
        }

        // Update collection info
        public static bool UpdateCollectionInfo(ADClientCollectionInfo Obj_AdCollection)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADClientCollectionInfo, ClientCollectionInfo>();
                ITable tblClientCollectionInfo;
                if (Obj_AdCollection.Id != 0)
                {
                    AutoMapper.Mapper.CreateMap<ADClientCollectionInfo, ClientCollectionInfo>();
                    ClientCollectionInfo Obj_ClientCollectionInfo = AutoMapper.Mapper.Map<ClientCollectionInfo>(Obj_AdCollection);
                    tblClientCollectionInfo = Obj_ClientCollectionInfo;
                    DBO.Provider.Update(ref tblClientCollectionInfo);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        
        public static ADClientCollectionInfo GetClientCollectionInfo(DataTable dt)
        {
            try
            {
                List<ADClientCollectionInfo> ClientCollectionInfo = (from DataRow dr in dt.Rows
                                             select new ADClientCollectionInfo()
                                             {
                                                 Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32( dr["Id"].ToString()) : 0)),
                                                 ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"].ToString()) : 0)),
                                                 ServiceTeamId = ((dr["ServiceTeamId"] != DBNull.Value ? Convert.ToInt32(dr["ServiceTeamId"].ToString()) : 0)),
                                                 SalesTeamId = ((dr["SalesTeamId"] != DBNull.Value ? Convert.ToInt32(dr["SalesTeamId"].ToString()) : 0)),
                                                 ProdTeamId = ((dr["ProdTeamId"] != DBNull.Value ? Convert.ToInt32(dr["ProdTeamId"].ToString()) : 0)),
                                                 LegalName = ((dr["LegalName"] != DBNull.Value ? dr["LegalName"].ToString() : "")),
                                                 Entity = ((dr["Entity"] != DBNull.Value ? dr["Entity"].ToString() : "")),
                                                 DBA = ((dr["DBA"] != DBNull.Value ? dr["DBA"].ToString() : "")),
                                                 ExpireDate = ((dr["ExpireDate"] != DBNull.Value ? Convert.ToDateTime(dr["ExpireDate"]) : DateTime.MinValue)),
                                                 FBN = ((dr["FBN"] != DBNull.Value ? dr["FBN"].ToString() : "")),
                                                 LegalAddr1 = ((dr["LegalAddr1"] != DBNull.Value ? dr["LegalAddr1"].ToString() : "")),
                                                 LegalAddr2 = ((dr["LegalAddr2"] != DBNull.Value ? dr["LegalAddr2"].ToString() : "")),
                                                 LegalCity = ((dr["LegalCity"] != DBNull.Value ? dr["LegalCity"].ToString() : "")),
                                                 LegalState = ((dr["LegalState"] != DBNull.Value ? dr["LegalState"].ToString() : "")),
                                                 LegalZip = ((dr["LegalZip"] != DBNull.Value ? dr["LegalZip"].ToString() : "")),
                                                 ContactName = ((dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : "")),
                                                 ContactTitle = ((dr["ContactTitle"] != DBNull.Value ? dr["ContactTitle"].ToString() : "")),
                                                 ContactEmail = ((dr["ContactEmail"] != DBNull.Value ? dr["ContactEmail"].ToString() : "")),
                                                 ContactPhone = ((dr["ContactPhone"] != DBNull.Value ? dr["ContactPhone"].ToString() : "")),
                                                 ContactSalutation = ((dr["ContactSalutation"] != DBNull.Value ? dr["ContactSalutation"].ToString() : "")),
                                                 ContactDOB = ((dr["ContactDOB"] != DBNull.Value ? Convert.ToDateTime(dr["ContactDOB"]) : DateTime.MinValue)),
                                                 ContactName2 = ((dr["ContactName2"] != DBNull.Value ? dr["ContactName2"].ToString() : "")),
                                                 ContactTitle2 = ((dr["ContactTitle2"] != DBNull.Value ? dr["ContactTitle2"].ToString() : "")),
                                                 ContactEmail2 = ((dr["ContactEmail2"] != DBNull.Value ? dr["ContactEmail2"].ToString() : "")),
                                                 ContactPhone2 = ((dr["ContactPhone2"] != DBNull.Value ? dr["ContactPhone2"].ToString() : "")),
                                                 ContactSalutation2 = ((dr["ContactSalutation2"] != DBNull.Value ? dr["ContactSalutation2"].ToString() : "")),
                                                 ContactDOB2 = ((dr["ContactDOB2"] != DBNull.Value ? Convert.ToDateTime(dr["ContactDOB2"]) : DateTime.MinValue)),
                                                 InvoiceName = ((dr["InvoiceName"] != DBNull.Value ? dr["InvoiceName"].ToString() : "")),
                                                 InvoiceAttn = ((dr["InvoiceAttn"] != DBNull.Value ? dr["InvoiceAttn"].ToString() : "")),
                                                 InvoiceAddr1 = ((dr["InvoiceAddr1"] != DBNull.Value ? dr["InvoiceAddr1"].ToString() : "")),
                                                 InvoiceAddr2 = ((dr["InvoiceAddr2"] != DBNull.Value ? dr["InvoiceAddr2"].ToString() : "")),
                                                 InvoiceCity = ((dr["InvoiceCity"] != DBNull.Value ? dr["InvoiceCity"].ToString() : "")),
                                                 InvoiceState = ((dr["InvoiceState"] != DBNull.Value ? dr["InvoiceState"].ToString() : "")),
                                                 InvoiceZip = ((dr["InvoiceZip"] != DBNull.Value ? dr["InvoiceZip"].ToString() : "")),
                                                 InvoiceEmail = ((dr["InvoiceEmail"] != DBNull.Value ? dr["InvoiceEmail"].ToString() : "")),
                                                 InvoiceFax = ((dr["InvoiceFax"] != DBNull.Value ? dr["InvoiceFax"].ToString() : "")),
                                                 RemitName = ((dr["RemitName"] != DBNull.Value ? dr["RemitName"].ToString() : "")),
                                                 RemitAttn = ((dr["RemitAttn"] != DBNull.Value ? dr["RemitAttn"].ToString() : "")),
                                                 RemitAddr1 = ((dr["RemitAddr1"] != DBNull.Value ? dr["RemitAddr1"].ToString() : "")),
                                                 RemitAddr2 = ((dr["RemitAddr2"] != DBNull.Value ? dr["RemitAddr2"].ToString() : "")),
                                                 RemitCity = ((dr["RemitCity"] != DBNull.Value ? dr["RemitCity"].ToString() : "")),
                                                 RemitState = ((dr["RemitState"] != DBNull.Value ? dr["RemitState"].ToString() : "")),
                                                 RemitZip = ((dr["RemitZip"] != DBNull.Value ? dr["RemitZip"].ToString() : "")),
                                                 RemitEmail = ((dr["RemitEmail"] != DBNull.Value ? dr["RemitEmail"].ToString() : "")),
                                                 RemitFax = ((dr["RemitFax"] != DBNull.Value ? dr["RemitFax"].ToString() : "")),
                                                 IsActive = ((dr["IsActive"] != DBNull.Value ? Convert.ToBoolean(dr["IsActive"]) : false)),
                                                 BranchBox = ((dr["BranchBox"] != DBNull.Value ? Convert.ToBoolean(dr["BranchBox"]) : false)),
                                                 NoEmail = ((dr["NoEmail"] != DBNull.Value ? Convert.ToBoolean(dr["NoEmail"]) : false)),
                                                 NewBusVar = ((dr["NewBusVar"] != DBNull.Value ? Convert.ToBoolean(dr["NewBusVar"]) : false)),
                                                 Visa = ((dr["Visa"] != DBNull.Value ? Convert.ToBoolean(dr["Visa"]) : false)),
                                                 CCMC = ((dr["CCMC"] != DBNull.Value ? Convert.ToBoolean(dr["CCMC"]) : false)),
                                                 Amex = ((dr["Amex"] != DBNull.Value ? Convert.ToBoolean(dr["Amex"]) : false)),
                                                 Discover = ((dr["Discover"] != DBNull.Value ? Convert.ToBoolean(dr["Discover"]) : false)),
                                                 SCAgreement = ((dr["SCAgreement"] != DBNull.Value ? Convert.ToBoolean(dr["SCAgreement"]) : false)),
                                                 LegalVars = ((dr["LegalVars"] != DBNull.Value ? dr["LegalVars"].ToString() : "")),
                                                 ReportVars = ((dr["ReportVars"] != DBNull.Value ? dr["ReportVars"].ToString() : "")),
                                                 MscVars = ((dr["MscVars"] != DBNull.Value ? dr["MscVars "].ToString() : "")),
                                                 ContactPlanId = ((dr["ContactPlanId"] != DBNull.Value ? Convert.ToInt32(dr["ContactPlanId"].ToString()) : 0)),
                                                 IsNoCBR = ((dr["IsNoCBR"] != DBNull.Value ? Convert.ToBoolean(dr["IsNoCBR"]) : false)),
                                                 CBRDays = ((dr["CBRDays"] != DBNull.Value ? Convert.ToInt32(dr["CBRDays"].ToString()) : 0)),
                                                 EmailNotifyPlacements = ((dr["EmailNotifyPlacements"] != DBNull.Value ? Convert.ToBoolean(dr["EmailNotifyPlacements"]) : false)),
                                                 ITDAvgAge = ((dr["ITDAvgAge"] != DBNull.Value ? Convert.ToInt32(dr["ITDAvgAge"].ToString()) : 0)),
                                                 LastUpdatedBy = ((dr["LastUpdatedBy"] != DBNull.Value ? dr["LastUpdatedBy"].ToString() : "")),
                                                 LastUpdateDate = ((dr["LastUpdateDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastUpdateDate"]) : DateTime.MinValue)),
                                                 InvoiceContactTitle = ((dr["InvoiceContactTitle"] != DBNull.Value ? dr["InvoiceContactTitle"].ToString() : "")),
                                                 RemitContactTitle = ((dr["RemitContactTitle"] != DBNull.Value ? dr["RemitContactTitle"].ToString() : "")),
                                                 SetupDate = ((dr["SetupDate"] != DBNull.Value ? Convert.ToDateTime(dr["SetupDate"]) : DateTime.MinValue)),
                                                 Venue = ((dr["Venue"] != DBNull.Value ? dr["Venue"].ToString() : "")),
                                                 ContactAddr1 = ((dr["ContactAddr1"] != DBNull.Value ? dr["ContactAddr1"].ToString() : "")),
                                                 ContactAddr2 = ((dr["ContactAddr2"] != DBNull.Value ? dr["ContactAddr2"].ToString() : "")),
                                                 ContactCity = ((dr["ContactCity"] != DBNull.Value ? dr["ContactCity"].ToString() : "")),
                                                 ContactState = ((dr["ContactState"] != DBNull.Value ? dr["ContactState"].ToString() : "")),
                                                 ContactZip = ((dr["ContactZip"] != DBNull.Value ? dr["ContactZip"].ToString() : "")),
                                                 InvoicePhone = ((dr["InvoicePhone"] != DBNull.Value ? dr["InvoicePhone"].ToString() : "")),
                                                 RemitPhone = ((dr["RemitPhone"] != DBNull.Value ? dr["RemitPhone"].ToString() : "")),
                                                 WebNoteAccess = ((dr["WebNoteAccess"] != DBNull.Value ? Convert.ToBoolean(dr["WebNoteAccess"]) : false)),
                                                 ClientAgentCode = ((dr["ClientAgentCode"] != DBNull.Value ? Convert.ToBoolean(dr["ClientAgentCode"]) : false)),
                                                 MiscVar = ((dr["MiscVar"] != DBNull.Value ? Convert.ToBoolean(dr["MiscVar"]) : false)),
                                                 ContactPhoneExt = ((dr["ContactPhoneExt"] != DBNull.Value ? dr["ContactPhoneExt"].ToString() : "")),
                                                 ContactPhone2Ext = ((dr["ContactPhone2Ext"] != DBNull.Value ? dr["ContactPhone2Ext"].ToString() : "")),
                                                 AttachDocsOnWeb = ((dr["AttachDocsOnWeb"] != DBNull.Value ? Convert.ToBoolean(dr["AttachDocsOnWeb"]) : false)),
                                                 EmailBackup = ((dr["EmailBackup"] != DBNull.Value ? Convert.ToBoolean(dr["EmailBackup"]) : false)),
                                                 EmailReports = ((dr["EmailReports"] != DBNull.Value ? Convert.ToBoolean(dr["EmailReports"]) : false)),
                                                 ContactCCEmail = ((dr["ContactCCEmail"] != DBNull.Value ? dr["ContactCCEmail"].ToString() : "")),
                                                 SCContactTitle = ((dr["SCContactTitle"] != DBNull.Value ? dr["SCContactTitle"].ToString() : "")),
                                                 SCAddr1 = ((dr["SCAddr1"] != DBNull.Value ? dr["SCAddr1"].ToString() : "")),
                                                 SCAddr2 = ((dr["SCAddr2"] != DBNull.Value ? dr["SCAddr2"].ToString() : "")),
                                                 SCCity = ((dr["SCCity"] != DBNull.Value ? dr["SCCity"].ToString() : "")),
                                                 SCState = ((dr["SCState"] != DBNull.Value ? dr["SCState"].ToString() : "")),
                                                 SCZip = ((dr["SCZip"] != DBNull.Value ? dr["SCZip"].ToString() : "")),
                                                 SCPhone = ((dr["SCPhone"] != DBNull.Value ? dr["SCPhone"].ToString() : "")),
                                                 SCFax = ((dr["SCFax"] != DBNull.Value ? dr["SCFax"].ToString() : "")),
                                                 SCEmail = ((dr["SCEmail"] != DBNull.Value ? dr["SCEmail"].ToString() : "")),
                                                 FBNCounty = ((dr["FBNCounty"] != DBNull.Value ? dr["FBNCounty"].ToString() : "")),
                                                 VendorNumber = ((dr["VendorNumber"] != DBNull.Value ? dr["VendorNumber"].ToString() : "")),
                                                 NBVar = ((dr["NBVar"] != DBNull.Value ? dr["NBVar"].ToString() : "")),
                                                 EmailRptTo = ((dr["EmailRptTo"] != DBNull.Value ? dr["EmailRptTo"].ToString() : "")),
                                                 IsRegAck = ((dr["IsRegAck"] != DBNull.Value ? Convert.ToBoolean(dr["IsRegAck"]) : false)),
                                                 IsIndAck = ((dr["IsIndAck"] != DBNull.Value ? Convert.ToBoolean(dr["IsIndAck"]) : false)),
                                                 IsStatusRpt = ((dr["IsStatusRpt"] != DBNull.Value ? Convert.ToBoolean(dr["IsStatusRpt"]) : false)),
                                                 IsCloseReturn = ((dr["IsCloseReturn"] != DBNull.Value ? Convert.ToBoolean(dr["IsCloseReturn"]) : false)),
                                                 SCContactAs = ((dr["SCContactAs"] != DBNull.Value ? dr["SCContactAs"].ToString() : "")),
                                                 SIFAuthority = ((dr["SIFAuthority"] != DBNull.Value ? dr["SIFAuthority"].ToString() : "")),
                                                 ContactFax = ((dr["ContactFax"] != DBNull.Value ? dr["ContactFax"].ToString() : "")),
                                                 ContactFax2 = ((dr["ContactFax2"] != DBNull.Value ? dr["ContactFax2"].ToString() : "")),
                                                 IsStatusGroup = ((dr["IsStatusGroup"] != DBNull.Value ? Convert.ToBoolean(dr["IsStatusGroup"]) : false)),
                                                 IsCCNone = ((dr["IsCCNone"] != DBNull.Value ? Convert.ToBoolean(dr["IsCCNone"]) : false)),
                                             }).ToList();
                return ClientCollectionInfo.First();
            }
            catch(Exception ex)
            {
                return (new ADClientCollectionInfo());
            }
        }

     
        #endregion
    }
}
