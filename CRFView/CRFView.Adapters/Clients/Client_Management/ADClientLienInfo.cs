﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using CRF.BLL.Clients;
    
namespace CRFView.Adapters.Clients.Client_Management
{
    public class ADClientLienInfo
    {
        #region properties
            public int Id { get; set; }
            public int ClientId { get; set; }
            public int ServiceTeamId { get; set; }
            public int SalesTeamId { get; set; }
            public int ProdTeamId { get; set; }
            public string ContactName { get; set; }
            public string ContactTitle { get; set; }
            public string ContactEmail { get; set; }
            public string ContactPhone { get; set; }
            public string ContactSalutation { get; set; }
            public DateTime ContactDOB { get; set; }
            public string ContactName2 { get; set; }
            public string ContactTitle2 { get; set; }
            public string ContactEmail2 { get; set; }
            public string ContactPhone2 { get; set; }
            public string ContactSalutation2 { get; set; }
            public DateTime ContactDOB2 { get; set; }
            public string InvoiceName { get; set; }
            public string InvoiceAttn { get; set; }
            public string InvoiceAddr1 { get; set; }
            public string InvoiceAddr2 { get; set; }
            public string InvoiceCity { get; set; }
            public string InvoiceState { get; set; }
            public string InvoiceZip { get; set; }
            public string InvoiceEmail { get; set; }
            public string InvoiceFax { get; set; }
            public bool IsActive { get; set; }
            public bool BranchBox { get; set; }
            public bool NoEmail { get; set; }
            public bool UseBranchNTO { get; set; }
            public bool UseBranchAck { get; set; }
            public bool GreenCard { get; set; }
            public bool CustSend { get; set; }
            public bool CustSendReg { get; set; }
            public bool AZCert { get; set; }
            public bool JointCK { get; set; }
            public bool TXApproved { get; set; }
            public bool TXCustSend { get; set; }
            public bool NOGCCall { get; set; }
            public bool NOCustCall { get; set; }
            public bool ActivateOIR { get; set; }
            public bool SendOIR { get; set; }
            public bool SendAck { get; set; }
            public bool SendNTO { get; set; }
            public bool SendNTOList { get; set; }
            public int OtherLglThreshold { get; set; }
            public string LaborType { get; set; }
            public string PhoneExt { get; set; }
            public string NoticePhoneNo { get; set; }
            public string LienContactName { get; set; }
            public string LienAddressLine1 { get; set; }
            public string LienAddressLine2 { get; set; }
            public string LienCity { get; set; }
            public string LienState { get; set; }
            public string LienZip { get; set; }
            public string LienContactTitle { get; set; }
            public string LienPhoneNo { get; set; }
            public string LienFax { get; set; }
            public string LienEmail { get; set; }
            public string LienSalutation { get; set; }
            public string LienSpecialInstruction { get; set; }
            public string LastUpdatedBy { get; set; }
            public DateTime LastUpdateDate { get; set; }
            public string InvoiceContactTitle { get; set; }
            public DateTime SetupDate { get; set; }
            public string ContactFax { get; set; }
            public string ContactFax2 { get; set; }
            public string Note { get; set; }
            public string ContactAddr1 { get; set; }
            public string ContactAddr2 { get; set; }
            public string ContactCity { get; set; }
            public string ContactState { get; set; }
            public string ContactZip { get; set; }
            public decimal LegalInterest { get; set; }
            public string InvoicePhoneNo { get; set; }
            public string ContactPhoneExt { get; set; }
            public string ContactPhone2Ext { get; set; }
            public bool TXPublicOwner { get; set; }
            public bool TXCopy { get; set; }
            public bool FLCertBox { get; set; }
            public bool KYNonStatBox { get; set; }
            public int TXPrelimGrp { get; set; }
            public string ReportEmail { get; set; }
            public bool TXCustSendReg { get; set; }
            public bool NoTitleSearch { get; set; }
            public bool JobAlertReqBox { get; set; }
            public bool IsFATExclude { get; set; }
            public bool SearchCustByRef { get; set; }
            public string NoAutoFaxState { get; set; }
            public string NoticePhoneNoWeb { get; set; }
            public bool EmailReports { get; set; }
            public string EmailReportsTo { get; set; }
            public bool NoDupValidation { get; set; }
            public bool IsCustomCoverLetter { get; set; }
            public bool IsIndividualNTO { get; set; }
            public bool IsCustomReportFile { get; set; }
            public string CustomReportFile { get; set; }
            public bool UseBranchSignature { get; set; }
            public bool SendAlert { get; set; }
            public bool EmailAck { get; set; }
            public string EmailAckTo { get; set; }
            public bool EmailLienAlerts { get; set; }
            public string EmailLienAlertsTo { get; set; }
            public bool IsNOCApproved { get; set; }
            public bool GCSendReg { get; set; }
            public bool DataEntryFee { get; set; }
            public string VAInfo { get; set; }
        #endregion

        #region CRUD
        // Load Lien Info
        public ADClientLienInfo LoadLienInfo(int? id)
        {
            try
            {
                ClientLienInfo myitem = new ClientLienInfo();
                ITable myentityItable = (ITable)myitem;
                if (id != null)
                {
                    DBO.Provider.Read(id.ToString(), ref myentityItable);
                }
                AutoMapper.Mapper.CreateMap<ClientLienInfo, ADClientLienInfo>();
                return (AutoMapper.Mapper.Map<ADClientLienInfo>(myitem));
            }
            catch (Exception ex)
            {
                return (new ADClientLienInfo());
            }
        }

        // Update Lien Info
        public static bool UpdateLienInfo(ADClientLienInfo Obj_AdLien)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADClientLienInfo, ClientLienInfo>();
                ITable tblClientLienInfo;
                if (Obj_AdLien.Id != 0)
                {
                    AutoMapper.Mapper.CreateMap<ADClientLienInfo,ClientLienInfo>();
                    ClientLienInfo Obj_ClientLienInfo = AutoMapper.Mapper.Map<ClientLienInfo>(Obj_AdLien);
                    tblClientLienInfo = Obj_ClientLienInfo;
                    DBO.Provider.Update(ref tblClientLienInfo);
                }
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
        #endregion
    }
}
