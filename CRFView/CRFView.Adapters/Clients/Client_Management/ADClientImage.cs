﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using CRF.BLL.Clients;

namespace CRFView.Adapters.Clients.Client_Management
{
    public class ADClientImage
    {
        #region Properties
        public int Id { get; set; }
        public int ClientId { get; set; }
        public byte[] ImageObject { get; set; }
        public int ImageType { get; set; }
        public string WaiverAddress1 { get; set; } 
        public string WaiverAddress2 { get; set; }
        public string WaiverPhone  { get; set; } 
        public string WaiverFax    { get; set; } 
        public string WaiverFolder { get; set; }
        #endregion

        #region CRUD
        //Read Details from database
        public ADClientImage ReadImage(int Id)
        {
            try
            {
                ClientImage myentity = new ClientImage();
                ITable myentityItable = (ITable)myentity;
                DBO.Provider.Read(Id.ToString(), ref myentityItable);
                myentity = (ClientImage)myentityItable;
                AutoMapper.Mapper.CreateMap<ClientImage, ADClientImage>();
                return (AutoMapper.Mapper.Map<ADClientImage>(myentity));
            }
            catch(Exception ex)
            {
                AutoMapper.Mapper.CreateMap<ClientImage, ADClientImage>();
                return (AutoMapper.Mapper.Map<ADClientImage>(new ClientImage()));
            }
        }

        public static bool SaveUpdateImage(ADClientImage Obj_ClImg)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADClientImage, ClientImage>();
                ITable tblImage;
                ClientImage objImage;
                if (Obj_ClImg.Id != 0)
                {

                    objImage = AutoMapper.Mapper.Map<ClientImage>(Obj_ClImg);
                    tblImage = (ITable)objImage;
                    DBO.Provider.Update(ref tblImage);
                }
                else
                {
                    objImage = AutoMapper.Mapper.Map<ClientImage>(Obj_ClImg);
                    tblImage = (ITable)objImage;
                    DBO.Provider.Create(ref tblImage);
                }
                return true;
              //  objImage = (ClientImage)tblImage;
              //  return objImage.PKID;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }
        #endregion
    }
}
