﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using CRF.BLL.Clients;
using CRFView.Adapters.Clients.Client_Management;

namespace CRFView.Adapters.Clients.Client_Management
{
    public class ADClientContact
    {
        #region Properties
            public int ClientContactId { get; set; }
            public int ClientId { get; set; }
            public string ContactName { get; set; }
            public string ContactTitle { get; set; }
            public string AddressLine1 { get; set; }
            public string AddressLine2 { get; set; }
            public string City { get; set; }
            public string State { get; set; }
            public string PostalCode { get; set; }
            public string Country { get; set; }
            public string Telephone1 { get; set; }
            public string Telephone2 { get; set; }
            public string Fax { get; set; }
            public string Email { get; set; }
            public string Note { get; set; }
            public string TelePhone1Ext { get; set; }
            public string TelePhone2Ext { get; set; }
            public DateTime DOB { get; set; }
        #endregion

        #region CRUD
        public static ADClientContact GetContactDetails(int id)
        {
            try
            {
                ADClientContact Obj_ADPortalUsers = new ADClientContact();
                var myitem = new ClientContact();
                ITable myitemItable = myitem;
                DBO.Provider.Read(id.ToString(), ref myitemItable);
                myitem = (ClientContact)myitemItable;
                AutoMapper.Mapper.CreateMap<ClientContact, ADClientContact>();
                return (AutoMapper.Mapper.Map<ADClientContact>(myitem));
            }
            catch (Exception ex)
            {
                return new Client_Management.ADClientContact();
            }
        }

        public static bool SaveUpdateContact(ADClientContact ObjSource)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADClientContact, ClientContact>();
                ITable tblContact;
                if (ObjSource.ClientContactId != 0)
                {
                    ADClientContact loadClientContact = GetContactDetails(Convert.ToInt32(ObjSource.ClientContactId));
                    loadClientContact.ContactName = ObjSource.ContactName;
                    loadClientContact.ContactTitle = ObjSource.ContactTitle;
                    loadClientContact.Telephone1 = ObjSource.Telephone1;
                    loadClientContact.TelePhone1Ext = ObjSource.TelePhone1Ext;
                    loadClientContact.Email = ObjSource.Email;
                    loadClientContact.DOB = ObjSource.DOB;
                    loadClientContact.Note = ObjSource.Note;
                    loadClientContact.ClientId = ObjSource.ClientId;

                    ClientContact objClientContact = AutoMapper.Mapper.Map<ClientContact>(loadClientContact);
                    tblContact = objClientContact;
                    DBO.Provider.Update(ref tblContact);
                }
                else
                {
                    ClientContact objClientContact = AutoMapper.Mapper.Map<ClientContact>(ObjSource);
                    tblContact = objClientContact;
                    DBO.Provider.Create(ref tblContact);
                }
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public static bool DeleteClientContact(string Id)
        {
            try
            {
                ITable IContact;
                ClientContact objContact = new ClientContact();
                objContact.ClientContactId = Convert.ToInt32(Id);
                IContact = objContact;
                DBO.Provider.Delete(ref IContact);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
        #endregion
    }
}
