﻿using CRFView.Adapters;
using CRFView.Adapters.Clients.Client_Management;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRFView.Controllers.Clients.ClientConfigurations
{
    public class ClientAssociationsController : Controller
    {
        ADAssociations objAssociations = new ADAssociations();
        List<ADAssociations> ResultListAssociations = new List<ADAssociations>();
        // GET: Associations
        public ActionResult Index()
        {
            ResultListAssociations = GetList();

            return PartialView(GetVirtualPath("Index"), ResultListAssociations);
        }
        //Show details on Edit
        public ActionResult ClientAssociationDetails(int id)
        {
            ADAssociations Associationsinfo = objAssociations.ReadClientAssociation(id);
            return View(GetVirtualPath("AssociationsDetails"), Associationsinfo);
        }

        //Save Updated or New Details
        [HttpPost]
        public ActionResult ClientAssociationSave(ADAssociations updatedAssociations)
        {
            objAssociations.SaveClientAssociation(updatedAssociations);
            ResultListAssociations = GetList();
            return PartialView(GetVirtualPath("_AssociationsList"), ResultListAssociations);
        }
        //Show blank details on Add
        public ActionResult ClientAssociationAdd()
        {
            ADAssociations objAssociations = new ADAssociations();
            ModelState.Clear();
            objAssociations.Id = 0;
            return View(GetVirtualPath("AssociationsDetails"), objAssociations);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {
            List<ADAssociations> FilterResult = new List<ADAssociations>();
            List<ADAssociations> ListAssociations = (List<ADAssociations>)Session["ListAssociations"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    FilterResult = SearchByAssociationName(ListAssociations, value);
                }
            }

            return View(GetVirtualPath("_AssociationsList"), FilterResult);
        }

        public List<ADAssociations> SearchByAssociationName(List<ADAssociations> ListAssociations, string Searchvalue)
        {
            List<ADAssociations> FilterList = (from item in ListAssociations
                                               where item.AssociationName.ToLower().Contains(Searchvalue.ToLower())
                                               select item).ToList();
            return FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            ResultListAssociations = GetList();
            return PartialView(GetVirtualPath("_AssociationsList"), ResultListAssociations);
        }

        // Get List from database 
        public List<ADAssociations> GetList()
        {
            List<ADAssociations> lstAssociations = objAssociations.ListClientAssociation();
            Session["ListAssociations"] = lstAssociations;
            SetFilterList();
            return lstAssociations;
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Association Name" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewClients/ClientConfigurations/Associations/" + ViewName + ".cshtml";
            return path;
        }
    }
}
