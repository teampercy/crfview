﻿using CRFView.Adapters;
using CRFView.Adapters.Clients.Client_Management;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRFView.Controllers.Clients.ClientConfigurations
{
    public class ClientUsersClientController : Controller
    {
        ADClientUser objADClientUser = new ADClientUser();

        // // GET: CLientUsers
        public ActionResult Index()
        {
            List<ADClientUser> ClientUsersList = objADClientUser.ClientUserGetList();
            Session["objClientUserList"] = ClientUsersList;
            SetFilterList();
            ViewBag.totalCount = ClientUsersList.Count;
            return PartialView(GetVirtualPath("Index"), ClientUsersList);
        }
        public ActionResult ClientUserAdd()
        {
            objADClientUser = new ADClientUser();
            objADClientUser.ID = 0;
            objADClientUser.ClientList = CommonBindList.GetClientList();
            objADClientUser.ClientViewGroupList = CommonBindList.GetClientViewGroups();
            return View(GetVirtualPath("ClientUserDetails"), objADClientUser);

        }
        [HttpPost]
        public ActionResult Search(string value, string Key, string ddl2Val)
        {
            List<ADClientUser> ClientUserList = (List<ADClientUser>)Session["objClientUserList"];
            List<ADClientUser> ClientUserFilterList = new List<ADClientUser>();
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    ClientUserFilterList = (from item in ClientUserList
                                            where item.ClientCode.ToLower().Contains(value.ToLower())
                                            select item).ToList();
                }
                else if (Key == "2")
                {
                    ClientUserFilterList = (from item in ClientUserList
                                            where item.UserName.ToLower().Contains(value.ToLower())
                                            select item).ToList();
                }
                else if (Key == "3")
                {
                    ClientUserFilterList = (from item in ClientUserList
                                            where item.Email.ToLower().Contains(value.ToLower())
                                            select item).ToList();
                }
                else if (Key == "4")
                {
                    int whiteSpaces = countLetters(value);
                    if(whiteSpaces == 0 || whiteSpaces == 1)
                    {
                        if(whiteSpaces == 0)
                        {
                            ClientUserFilterList = (from item in ClientUserList
                                                    where item.FName.ToLower().Contains(value.ToLower())
                                                    || item.LName.ToLower().Contains(value.ToLower())
                                                    select item).ToList();
                        }
                        if(whiteSpaces == 1)
                        {
                            string[] words = value.Split(' ');
                            foreach (string word in words)
                            {
                                ClientUserFilterList = (from item in ClientUserList
                                                        where item.FName.ToLower().Contains(words[0].ToLower())
                                                        || item.LName.ToLower().Contains(words[1].ToLower())
                                                        select item).ToList();
                            }

                        }
                    }
                }
                else if (Key == "5")
                {
                    bool condition;
                    if(ddl2Val == "1")
                    {
                        condition = true;
                    }
                    else
                    {
                        condition = false;
                    }

                    ClientUserFilterList = (from item in ClientUserList
                                            where item.isinactive == condition
                                            select item).ToList();
                }
                else if(Key == "6")
                {
                    DateTime FDate = Convert.ToDateTime(value);
                    if (ddl2Val == "1")
                    {
                        ClientUserFilterList = (from item in ClientUserList
                                                where item.LastLoginDate.Date == FDate.Date
                                                select item).ToList();
                    }
                    if (ddl2Val == "2")
                    {
                        ClientUserFilterList = (from item in ClientUserList
                                                where item.LastLoginDate != FDate
                                                select item).ToList();
                    }
                    if (ddl2Val == "3")
                    {
                        ClientUserFilterList = (from item in ClientUserList
                                                where item.LastLoginDate > FDate
                                                select item).ToList();
                    }
                    if (ddl2Val == "4")
                    {
                        ClientUserFilterList = (from item in ClientUserList
                                                where item.LastLoginDate >= FDate
                                                select item).ToList();
                    }
                    if (ddl2Val == "5")
                    {
                        ClientUserFilterList = (from item in ClientUserList
                                                where item.LastLoginDate < FDate
                                                select item).ToList();
                    }
                    if (ddl2Val == "6")
                    {
                        ClientUserFilterList = (from item in ClientUserList
                                                where item.LastLoginDate <= FDate
                                                select item).ToList();
                    }
                }

            }
            return View(GetVirtualPath("_ClientUsersList"), ClientUserFilterList);
        }

        public ActionResult ClientUserDetails(int id)
        {
            ADClientUser objClientUsers = new ADClientUser();
            objClientUsers = objADClientUser.ClientUserRead(id);
            objClientUsers.ClientList = CommonBindList.GetClientList();
            objClientUsers.ClientViewGroupList = CommonBindList.GetClientViewGroups();
            objClientUsers.UserSecurity = objADClientUser.UpdateUserSecurity(objClientUsers);
            objClientUsers.HasGroupAccess = objADClientUser.UpdateHasAccess(objClientUsers);
            return View(GetVirtualPath("ClientUserDetails"), objClientUsers);
        }

        [HttpPost]
        public ActionResult ClientUserSave(ADClientUser updatedUser)
        {
            #region UserFieldsUpdates
            updatedUser.InternalUser = 0;
            if (updatedUser.HasGroupAccess == false)
            {
                updatedUser.ClientViewGroupId = 0;
            }
            switch (updatedUser.UserSecurity)
            {
                case "Manager":
                    updatedUser.IsClientViewManager = 1;
                    updatedUser.IsClientViewAdmin = false;
                    break;
                case "Staff":
                    updatedUser.IsClientViewManager = 0;
                    updatedUser.IsClientViewAdmin = false;
                    break;
                case "Admin":
                    updatedUser.IsClientViewAdmin = true;
                    updatedUser.IsClientViewManager = 0;
                    break;
                default:
                    updatedUser.IsClientViewManager = 0;
                    updatedUser.IsClientViewAdmin = false;
                    break;
            }

            objADClientUser.SaveClientUser(updatedUser);
            List<ADClientUser> ClientUserList = objADClientUser.ClientUserGetList();
            Session["objClientUserList"] = ClientUserList;
            return PartialView(GetVirtualPath("_ClientUsersList"), ClientUserList);
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADClientUser> ClientUserList = objADClientUser.ClientUserGetList();
            Session["objClientUserList"] = ClientUserList;
            return PartialView(GetVirtualPath("_ClientUsersList"), ClientUserList);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "ClientCode" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "UserName" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "Email" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "FullName" });
            filterlist.Add(new CommonBindList { Id = 5, Name = "IsInactive" });
            filterlist.Add(new CommonBindList { Id = 6, Name = "LastLoginDate" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        //Delete client user
        [HttpPost]
        public ActionResult DeleteClientUser(string ID)
        {
            bool result = ADClientUser.DeletetClient(ID);
            return Json(result);
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewClients/ClientConfigurations/ClientUsers/" + ViewName + ".cshtml";
            return path;
        }
        #endregion

        #region local methods
        public static int countLetters(string word)
        {
            int count = 0;
            foreach (char c in word)
            {
                if (' ' == c)
                    count++;
            }
            return count;
        }
        #endregion
    }

}
