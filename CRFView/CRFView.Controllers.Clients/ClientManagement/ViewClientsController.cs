﻿using CRFView.Adapters;
using CRFView.Adapters.Clients.Client_Management;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace CRFView.Controllers
{
    public class ViewClientsController: Controller
    {
        ADClient AdClObj = new ADClient();
        ADClientLienInfo Obj_ADClientLienInfo = new ADClientLienInfo();
        ADClientCollectionInfo Obj_ADCCI = new ADClientCollectionInfo();
        ADClientImage Obj_ADCImage = new ADClientImage();

        // Get list of clients 
        public ActionResult Index()       
        {
            List<ADClient> lstClients = GetList();
            ViewClientsVM VmObj = new ViewClientsVM();
            VmObj.ListADClient = lstClients;
            return PartialView(GetVirtualPath("Index"), VmObj);
        }

        public ActionResult RefreshList()
        {
            List<ADClient> lstClients = GetList();
            ViewClientsVM Obj_ViewClientsVM = new ViewClientsVM();
            Obj_ViewClientsVM.ListADClient = lstClients;
            return PartialView(GetVirtualPath("_ViewClientList"), Obj_ViewClientsVM);
        }


        //Show FilterModal
        public ActionResult ShowFilterModal()
        {
            FilterVM FilterVM_Obj = new FilterVM();
            ModelState.Clear();
            return View(GetVirtualPath("_FilterModal"), FilterVM_Obj);
        }

        // Show client list based on filter values
        [HttpPost]
        public ActionResult ApplyFilter(FilterVM FilterVM_Obj)
        {
            CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
            ViewClientsVM VmObj = new ViewClientsVM();
            VmObj.ListADClient = GetFilteredList(FilterVM_Obj, CU_Obj);
            return PartialView(GetVirtualPath("_ViewClientList"), VmObj);
        }

        //Show Create New Client Modal
        public ActionResult ShowCreateNewClientModal()
        {
            NewClientVM NewClientVM_Obj = new NewClientVM();
            BindDropdowns(ref NewClientVM_Obj);
            return View(GetVirtualPath("_CreateNewClient"), NewClientVM_Obj);
        }


        // create a new client
        [HttpPost]
        public ActionResult CreateNewClient(NewClientVM NewClientVM_Obj)
        {
            CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
            int RecentClientId = AdClObj.SaveNewClient(NewClientVM_Obj, CU_Obj);
            //ViewClientsVM VmObj = new ViewClientsVM();
            //List<ADClient> lstClients = GetList();
            //VmObj.ListADClient = lstClients;
            //return PartialView(GetVirtualPath("_ViewClientList"), VmObj);

            ///////////////////////////////////////////////////////////
            ClientEditVM Obj_ClientEditVM = new ClientEditVM();
            Obj_ClientEditVM = AdClObj.LoadClientData(CU_Obj, RecentClientId);
            if (Obj_ClientEditVM.Obj_ADClient.NextContactDate == DateTime.MinValue)
            {
                Obj_ClientEditVM.ChkFollowUp = false;
            }
            else
            {
                Obj_ClientEditVM.ChkFollowUp = true;
            }
            if (Obj_ClientEditVM.Obj_ADClient.EvaluationDate == DateTime.MinValue)
            {
                Obj_ClientEditVM.ChkEvalDate = false;
            }
            else
            {
                Obj_ClientEditVM.ChkEvalDate = true;
            }

            Obj_ClientEditVM.MainClientList = CommonBindList.GetMainClientList();
            Obj_ClientEditVM.SalesTeamList = CommonBindList.GetSalesTeamList();
            Obj_ClientEditVM.ServiceTeamList = CommonBindList.GetServiceTeamList();
            Obj_ClientEditVM.ClientTypeList = CommonBindList.GetClientTypeList();
            Obj_ClientEditVM.ContractList = ADClientContract.GetClientContractForId(CU_Obj, RecentClientId);
            Obj_ClientEditVM.BranchList = ADClientContract.GetBranchesForId(CU_Obj, RecentClientId);
            Obj_ClientEditVM.UserList = ADClient.GetUsersForId(CU_Obj, RecentClientId);
            Obj_ClientEditVM.ContactList = ADClient.GetContactsForId(CU_Obj, RecentClientId);
            Obj_ClientEditVM.SignersList = ADClient.GetSignersForId(CU_Obj, RecentClientId);
            Obj_ClientEditVM.ClosedIssueList = ADClient.GetIssuesForId(CU_Obj, RecentClientId, "closed");
            Obj_ClientEditVM.OpenIssueList = ADClient.GetIssuesForId(CU_Obj, RecentClientId, "open");
            Obj_ClientEditVM.ReportList = ADClient.GetScheduledReportsForId(CU_Obj, RecentClientId);
            return PartialView(GetVirtualPath("_LoadClient"), Obj_ClientEditVM);
            ///////////////////////////////////////////////////////////
        }

        // Check if duplicate client code
        [HttpPost]
        public ActionResult ChechkDuplicateClient(string ClientCode)
        {
            bool result=AdClObj.IsDuplicateClient(ClientCode);
            return Json(result);
        }

        // Load client details
        [HttpPost]
        public ActionResult LoadClient(int id)
        {
            ClientEditVM Obj_ClientEditVM = new ClientEditVM();
            CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
            Obj_ClientEditVM= AdClObj.LoadClientData(CU_Obj, id);
            if(Obj_ClientEditVM.Obj_ADClient.NextContactDate == DateTime.MinValue)
            {
                Obj_ClientEditVM.ChkFollowUp = false;
            }
            else
            {
                Obj_ClientEditVM.ChkFollowUp = true;
            }
            if (Obj_ClientEditVM.Obj_ADClient.EvaluationDate == DateTime.MinValue)
            {
                Obj_ClientEditVM.ChkEvalDate = false;
            }
            else
            {
                Obj_ClientEditVM.ChkEvalDate = true;
            }

            Obj_ClientEditVM.MainClientList = CommonBindList.GetMainClientList();
            Obj_ClientEditVM.SalesTeamList = CommonBindList.GetSalesTeamList();
            Obj_ClientEditVM.ServiceTeamList = CommonBindList.GetServiceTeamList();
            Obj_ClientEditVM.ClientTypeList = CommonBindList.GetClientTypeList();
            Obj_ClientEditVM.ContractList = ADClientContract.GetClientContractForId(CU_Obj, id);
            Obj_ClientEditVM.BranchList = ADClientContract.GetBranchesForId(CU_Obj, id);
            Obj_ClientEditVM.UserList = ADClient.GetUsersForId(CU_Obj, id);
            Obj_ClientEditVM.ContactList = ADClient.GetContactsForId(CU_Obj, id);
            Obj_ClientEditVM.SignersList = ADClient.GetSignersForId(CU_Obj, id);
            Obj_ClientEditVM.ClosedIssueList = ADClient.GetIssuesForId(CU_Obj, id, "closed");
            Obj_ClientEditVM.OpenIssueList = ADClient.GetIssuesForId(CU_Obj, id, "open");
            Obj_ClientEditVM.ReportList = ADClient.GetScheduledReportsForId(CU_Obj, id);
            return PartialView(GetVirtualPath("_LoadClient"), Obj_ClientEditVM);
        }

        [HttpPost]
        public ActionResult EditClient(ClientEditVM Obj_ClEdit)
        {
            if(Obj_ClEdit.ChkEvalDate == false)
            {
                Obj_ClEdit.Obj_ADClient.EvaluationDate = DateTime.MinValue;
            }
            if (Obj_ClEdit.ChkFollowUp == false)
            {
                Obj_ClEdit.Obj_ADClient.NextContactDate = DateTime.MinValue;
            }
            var result = ADClient.UpdateClient(Obj_ClEdit.Obj_ADClient);
            return Json(result);
        }

        [HttpPost]
        public ActionResult ShowLienInfo(int? id)
        {
            ClientLienInfoVM Obj_ClientLienInfoVM = new ClientLienInfoVM();
            Obj_ClientLienInfoVM.obj_ADClientLienInfo = Obj_ADClientLienInfo.LoadLienInfo(id);
            return PartialView(GetVirtualPath("_LienInfo"), Obj_ClientLienInfoVM);
        }

        [HttpPost]
        public ActionResult ShowCollectionInfo(int? id)
        {
            ClientCollectionInfoVM Obj_ClientCollectionInfoVM = new ClientCollectionInfoVM();
            Obj_ClientCollectionInfoVM.Obj_ADCCI = Obj_ADCCI.LoadCollectionInfo(id);
            return PartialView(GetVirtualPath("_CollectionInfo"), Obj_ClientCollectionInfoVM);
        }

        // Show Logo Information
        [HttpPost]
        public ActionResult ShowLogo(int id,int ClientId)
        {
            ClientImageVM Obj_ClientImageVM = new ClientImageVM();
            Obj_ClientImageVM.Obj_ADCImage = Obj_ADCImage.ReadImage(id);
            if (Obj_ClientImageVM.Obj_ADCImage.ClientId == 0)
            {
                Obj_ClientImageVM.Obj_ADCImage.ClientId = ClientId;
            }
            return PartialView(GetVirtualPath("_ClientLogo"), Obj_ClientImageVM);
        }

      //  Create/Update Logo
       [HttpPost]
        public ActionResult CrUpLogo(ClientImageVM OBj_ClImg)
        {
            var result = ADClientImage.SaveUpdateImage(OBj_ClImg.Obj_ADCImage);
            return Json(result);
        }
        //[HttpPost]
        //public ActionResult CrUpLogo(FormCollection asfdf)
        //{
        //    //var result = ADClientImage.SaveUpdateImage(OBj_ClImg.Obj_ADCImage);
        //    return Json(true);
        //}

        // Show Email Blast
        [HttpPost]
        public ActionResult ShowEmailBlast(int? id)
        {
            CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
            return PartialView(GetVirtualPath("_EmailBlast"));

        }

        // Edit Existing lien or collectioj contract


  
        [HttpPost]
        public ActionResult EditContract(int id, int ClientId)
        {
            ClientContractVM Obj_CCVM = new ClientContractVM();
            CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
            ADClientContract.BindDropdowns(ref Obj_CCVM, ClientId);

            Obj_CCVM.Obj_ClientContract = ADClientContract.ReadClientContractType(id);
            if (Obj_CCVM.Obj_ClientContract.ContractTypeId == 1)
            {
                Obj_CCVM.ContractHistoryList = ADClientContract.GetCollectionHistory(CU_Obj, ClientId);
                return PartialView(GetVirtualPath("_NewCollectionContract"), Obj_CCVM);
            } 
            else
            {

                Obj_CCVM.DocumentList = ADClientContract.GetDocuments(CU_Obj, id);
                Obj_CCVM.LienRatesList = ADClientContract.GetLienRate(CU_Obj, id);
                Obj_CCVM.ContractHistoryList = ADClientContract.GetLienHistory(CU_Obj, ClientId);///changed id to ClientId
                return PartialView(GetVirtualPath("_NewLienContract"), Obj_CCVM);
            }
           
       
        }

        [HttpPost]
        public ActionResult LienDropdown(int Id, string PriceCodeName, int BillableEventId, bool IsAllInclusivePrice)
        {
            CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
            List<NewLienRatesVM> rateList = ADClientContract.ratelist(CU_Obj,Id, BillableEventId, IsAllInclusivePrice);
            Session["Id"] = Id.ToString();
            Session["PriceCodeName"] = PriceCodeName;
            return Json(rateList, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ChangeRate(string text,string value, string ContractId)
        {
            string Id = Session["Id"].ToString();
            string PriceCodeNameOld = Session["PriceCodeName"].ToString();
            CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
            var result = ADClientContract.ChangeLienRate( CU_Obj, Id, value, text, PriceCodeNameOld, ContractId);
            return Json(result);
        }


        [HttpPost]
        public ActionResult SaveLienContract(ClientContractVM Obj_CCVM)
        {
            CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
            bool result = ADClientContract.SaveClientLienContract(Obj_CCVM.Obj_ClientContract, CU_Obj);
            return Json(result);
        }

        [HttpPost]
        public ActionResult SaveCollectionContract(ClientContractVM Obj_CCVM)
        {
            CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
            bool result = ADClientContract.SaveClientCollectionContract(Obj_CCVM.Obj_ClientContract, CU_Obj);
            return Json(result);
        }

        // Show modal for new lien contract under contract tab  
        //[HttpPost]
        //public ActionResult ShowNewLienContract(int id)
        //{
        //    ClientContractVM Obj_CCVM = new ClientContractVM();
        //    CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
        //    ADClientContract.BindDropdowns(ref Obj_CCVM);
        //    Obj_CCVM.LienRatesList = ADClientContract.GetLienRate(CU_Obj, id);
        //    Obj_CCVM.ContractHistoryList = ADClientContract.GetLienHistory(CU_Obj, id);
        //    //Obj_CCVM.ContractList= ADClientContract.GetClientContractForId(CU_Obj, id);
        //    return PartialView(GetVirtualPath("_NewLienContract"), Obj_CCVM);
        //}

        public ActionResult ShowNewLienContractModal(string ClientId)
        {
            ClientContractVM Obj_CCVM = new ClientContractVM();
            Obj_CCVM.Obj_ClientContract.BillingClientId = Convert.ToInt32(ClientId);
            CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
            ADClientContract.BindDropdowns(ref Obj_CCVM);
            return PartialView(GetVirtualPath("_NewLienContract"), Obj_CCVM);
        }

        public ActionResult ShowNewCollectionContractModal(string ClientId)
        {
            ClientContractVM Obj_CCVM = new ClientContractVM();
            Obj_CCVM.Obj_ClientContract.BillingClientId = Convert.ToInt32(ClientId);
            CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
            ADClientContract.BindDropdowns(ref Obj_CCVM);
            return PartialView(GetVirtualPath("_NewCollectionContract"), Obj_CCVM);
        }

        [HttpPost]
        public ActionResult ShowNewCollectionContract(int id)
        {
            ClientContractVM Obj_CCVM = new ClientContractVM();
            CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
            ADClientContract.BindDropdowns(ref Obj_CCVM);
            Obj_CCVM.LienRatesList = ADClientContract.GetLienRate(CU_Obj, id);
            Obj_CCVM.ContractHistoryList = ADClientContract.GetLienHistory(CU_Obj, id);
         //   Obj_CCVM.ContractList= ADClientContract.GetClientContractForId(CU_Obj, id);
            return PartialView(GetVirtualPath("_NewCollectionContract"), Obj_CCVM);
        }

        public ActionResult ShowNewBranch(string ParentClientId, string ParentClientCode)
        {
            NewClientVM Obj_NewBranchVM = new NewClientVM();
            Obj_NewBranchVM.ADC_obj.ParentClientId = Convert.ToInt32(ParentClientId);
            Obj_NewBranchVM.ADC_obj.ClientCode = ParentClientCode;
            return PartialView(GetVirtualPath("_BranchInfo"), Obj_NewBranchVM);
        }

        [HttpPost]
        public ActionResult ShowBranchDetails(int id)
        {
            NewClientVM Obj_NewBranchVM = new NewClientVM();
            Obj_NewBranchVM.ADC_obj = ADClient.GetClientDetails(id);
            Obj_NewBranchVM.ADC_BranchSigner = ADClientBranchSigner.GetClientBranchSigner(id.ToString());
            return PartialView(GetVirtualPath("_BranchInfo"), Obj_NewBranchVM);
        }

        [HttpPost]
        public ActionResult EditBranch(int id)
        {
            return PartialView(GetVirtualPath("_BranchInfo"));
        }        

        [HttpPost]
        public ActionResult ShowUser(int id)
        {
            UserVM Obj_UserVm = new UserVM();
            Obj_UserVm.PortalUser = ADPortalUsers.GetUserForId(id);
            Obj_UserVm.ViewGroupList = CommonBindList.GetViewGroups();
            UserVM.AdminManagerSetting(ref Obj_UserVm);
            return PartialView(GetVirtualPath("_User"), Obj_UserVm);
        }

        [HttpPost]
        public ActionResult ShowContact(int id, int ClientId)
        {
            ClientContactVM Obj_ClientContact = new ClientContactVM();
            Obj_ClientContact.Obj_ClientContact = ADClientContact.GetContactDetails(id);
            if(Obj_ClientContact.Obj_ClientContact.ClientId==0)
            {
                Obj_ClientContact.Obj_ClientContact.ClientId = ClientId;
            }
            return PartialView(GetVirtualPath("_Contact"), Obj_ClientContact);
        }

        [HttpPost]
        public ActionResult ShowSigner(int id)
        {
            ClientSignerVM obj_ClientSignerVM = new ClientSignerVM();
            obj_ClientSignerVM.Obj_ClientSigner = ADClientSigners.LoadSigner(id);
            return PartialView(GetVirtualPath("_Signer"), obj_ClientSignerVM);
        }

        // Save/Update client lien info
        [HttpPost]
        public ActionResult SaveLienInfo (ClientLienInfoVM Obj_ClLinInfo)
        {
            var result = ADClientLienInfo.UpdateLienInfo(Obj_ClLinInfo.obj_ADClientLienInfo);
            return Json(result);
        }

        // Save/Update client collection info
        [HttpPost]
        public ActionResult SaveCollectionInfo(ClientCollectionInfoVM Obj_CCInfo)
        {
            var result = ADClientCollectionInfo.UpdateCollectionInfo(Obj_CCInfo.Obj_ADCCI);
            return Json(result);
        }

        [HttpPost]
        public ActionResult ShowNewDoc(string ContractId)
        {
            ClientContractDocumentVM Obj_CCD = new ClientContractDocumentVM();
            Obj_CCD.ContractId = ContractId;
            List<SelectListItem> Options = new List<SelectListItem>()
            {
                new SelectListItem { Selected=true, Text = "Client Info Form", Value = "Client Info Form"},
                new SelectListItem { Text = "Credit Card Auth", Value = "Credit Card Auth" },
                new SelectListItem { Text = "Full Agreement", Value = "Full Agreement" },
                new SelectListItem { Text = "Lien Agreement", Value = "Lien Agreement" },
                new SelectListItem { Text = "Misc", Value = "Misc" },
                new SelectListItem { Text = "Prelim Agreement", Value = "Prelim Agreement" }
            };
            Obj_CCD.DocumentTypeList = Options;                                         
            return PartialView(GetVirtualPath("_DocEdit"), Obj_CCD);
        }

        [HttpPost]
        public ActionResult ShowNewDocCollection(string ContractId)
        {
            ClientContractDocumentVM Obj_CCD = new ClientContractDocumentVM();
            Obj_CCD.ContractId = ContractId;
            List<SelectListItem> Options = new List<SelectListItem>()
            {
                new SelectListItem { Selected=true, Text = "10 Day Agreement", Value = "10 Day Agreement"},
                new SelectListItem { Text = "Client Info Form", Value = "Client Info Form" },
                new SelectListItem { Text = "Collection Agreement", Value = "Collection Agreement" },
                new SelectListItem { Text = "Credit Card Auth", Value = "Credit Card Auth" },
                new SelectListItem { Text = "Misc", Value = "Misc" },
                new SelectListItem { Text = "Small Case Agreement", Value = "Small Case Agreement" }
            };
            Obj_CCD.DocumentTypeList = Options;
            return PartialView(GetVirtualPath("_DocEdit"), Obj_CCD);
        }

        [HttpPost]
        public ActionResult SaveDoc()
        {
            try
            {
                var file = Request.Files[0];
                
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/App_Data/UploadedFiles"), fileName);
                file.SaveAs(path);
                Session["path"] = path;
                return Json(true);
                //return Json(path,JsonRequestBehavior.AllowGet);
            }
            catch(Exception ex)
            {
                return Json(false);
            }
        }

        [HttpPost]
        public ActionResult SaveDocInputs(ClientContractDocumentVM ObjCCD)
        {
            try
            {
                ObjCCD.Obj_Attachment.ContractId = Convert.ToInt32(ObjCCD.ContractId);
                string path = Session["path"].ToString();
                CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
                ADClientContractAttachments.SaveDocProperties(CU_Obj, ObjCCD.Obj_Attachment, path);
                Session["path"] = null;
                return Json(true);
            }
            catch(Exception ex)
            {
                return Json(false);
            }
        }

        [HttpPost]
        public ActionResult CheckFileExtension(string Id)
        {
            var Attachment = ADClientContractAttachments.ReadAttachmentDocument(Id);
            string fileName = Attachment.FileName;
            string extension = fileName.Substring(fileName.IndexOf('.')+1);
            return Json(extension,JsonRequestBehavior.AllowGet);
        }


        public ActionResult ViewDownloadDoc(string Id)
        {

                var Attachment = ADClientContractAttachments.ReadAttachmentDocument(Id);
                var path = Path.Combine(Server.MapPath("~/App_Data/DownloadableFiles"), Attachment.FileName);
                ADClientContractAttachments.SaveFileToBeViewed(Attachment.DocumentImage, path);
                byte[] fileBytes = Attachment.DocumentImage;
                string fileName = path.Substring(path.LastIndexOf("\\") + 1);
                return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
            

          //  return Json(true);
        }

        [HttpPost]
        public ActionResult EditAttachment(string Id, string ContractId)
        {
            ClientContractDocumentVM Obj_CCD = new ClientContractDocumentVM();
            Obj_CCD.ContractId = ContractId;
            List<SelectListItem> Options = new List<SelectListItem>()
            {
                new SelectListItem { Selected=true, Text = "Client Info Form", Value = "Client Info Form"},
                new SelectListItem { Text = "Credit Card Auth", Value = "Credit Card Auth" },
                new SelectListItem { Text = "Full Agreement", Value = "Full Agreement" },
                new SelectListItem { Text = "Lien Agreement", Value = "Lien Agreement" },
                new SelectListItem { Text = "Misc", Value = "Misc" },
                new SelectListItem { Text = "Prelim Agreement", Value = "Prelim Agreement" }
            };
            Obj_CCD.DocumentTypeList = Options;
            var AttachmentObj = ADClientContractAttachments.ReadAttachmentDocument(Id);
            Obj_CCD.Obj_Attachment.Id = AttachmentObj.Id;
            Obj_CCD.Obj_Attachment.ContractId = AttachmentObj.ContractId;
            Obj_CCD.Obj_Attachment.UserId = AttachmentObj.UserId;
            Obj_CCD.Obj_Attachment.UserCode = AttachmentObj.UserCode;
            Obj_CCD.Obj_Attachment.DateCreated = AttachmentObj.DateCreated;
            Obj_CCD.Obj_Attachment.DocumentType = AttachmentObj.DocumentType;
            Obj_CCD.Obj_Attachment.DocumentDescription = AttachmentObj.DocumentDescription;

            return PartialView(GetVirtualPath("_DocEdit"), Obj_CCD);
        }

        [HttpPost]
        public ActionResult UpdateDocumentProperties(ClientContractDocumentVM ObjCCD)
        {
            CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
            bool result = ADClientContractAttachments.UpdateDocProperies(CU_Obj, ObjCCD.Obj_Attachment);
            return Json(result);
        }

        [HttpPost]
        public ActionResult DeleteDoc(string Id)
        {
            bool result = ADClientContractAttachments.DeleteAttachmentDocument(Id);
            return Json(result);
        }

        public ActionResult NewReport(string ClientId)
        {
            ClientScheduledReportsVM Obj_ScheduleReport = new ClientScheduledReportsVM();
            Obj_ScheduleReport.Obj_ScheduledReport.ClientId = Convert.ToInt32(ClientId);
            Obj_ScheduleReport.ReportTypeList = CommonBindList.GetReportTypes();
             return PartialView(GetVirtualPath("_ScheduledReport"), Obj_ScheduleReport);
        }

        [HttpPost]
        public ActionResult SaveSceduledReport(ClientScheduledReportsVM Obj_ScheduleReport)
        {
            var result = ADClientScheduledReports.SaveUpdateScheduledReport(Obj_ScheduleReport.Obj_ScheduledReport);
            return Json(result);
        }

        [HttpPost]
        public ActionResult DeleteReportWithId(string Id)
        {
            var result = ADClientScheduledReports.DeleteScheduledReport(Id);
            return Json(result);
        }

        [HttpPost]
        public ActionResult EditReport(string Id)
        {
            ClientScheduledReportsVM Obj = new ClientScheduledReportsVM();
            Obj.Obj_ScheduledReport = ADClientScheduledReports.ReadScheduledReport(Convert.ToInt32(Id));
            Obj.ReportTypeList = CommonBindList.GetReportTypes();
            return PartialView(GetVirtualPath("_ScheduledReport"), Obj);
        }

        [HttpPost]
        public ActionResult NewSigner(string ClientId)
        {
            ClientSignerVM Obj_ClientSigner = new ClientSignerVM();
            Obj_ClientSigner.Obj_ClientSigner.ClientId = Convert.ToInt32(ClientId);
            return PartialView(GetVirtualPath("_Signer"), Obj_ClientSigner);
        }

        [HttpPost]
        public ActionResult SaveSigner(FormCollection formCollection)
        {
            //foreach (var key in formCollection.AllKeys)
            //{
            //    var value = formCollection[key];
            //    // etc.
            //}

            ClientSignerVM Obj_ClientSigner = new ClientSignerVM();

            if (ModelState.IsValid)
            {
                UpdateModel(Obj_ClientSigner);
            }
            var result = ADClientSigners.SaveUpdateSigner(Obj_ClientSigner.Obj_ClientSigner);
            return Json(result);
        }

        [HttpPost]
        public ActionResult DeleteSigner(string Id)
        {
            var result = ADClientSigners.DeleteSigner(Id);
            return Json(result);
        }

        [HttpPost]
        public ActionResult NewContact(string ClientId)
        {
            ClientContactVM Obj_ClientContact = new ClientContactVM();
            Obj_ClientContact.Obj_ClientContact.ClientId = Convert.ToInt32(ClientId);
            return PartialView(GetVirtualPath("_Contact"), Obj_ClientContact);
        }

        [HttpPost]
        public ActionResult SaveUpdateContact()
        {
            ClientContactVM Obj_ClientContact = new ClientContactVM();
            var result = false;
            if (ModelState.IsValid)
            {
                UpdateModel(Obj_ClientContact);
                result = ADClientContact.SaveUpdateContact(Obj_ClientContact.Obj_ClientContact);
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult DeleteContact(string Id)
        {
            var result = ADClientContact.DeleteClientContact(Id);
            return Json(result);
        }

        [HttpPost]
        public ActionResult NewUser(string ClientId)
        {
            UserVM Obj_User = new UserVM();
            Obj_User.PortalUser.ClientId = Convert.ToInt32(ClientId);
            Obj_User.ViewGroupList = CommonBindList.GetViewGroups();
            UserVM.AdminManagerSetting(ref Obj_User);
            return PartialView(GetVirtualPath("_User"), Obj_User);
        }

        [HttpPost]
        public ActionResult SaveUpdateUser()
        {
            var result = false;
            UserVM Obj_User = new UserVM();
            if(ModelState.IsValid)
            {
                UpdateModel(Obj_User);
                if (Obj_User.HasGroupAccess == false)
                {
                    Obj_User.PortalUser.ClientViewGroupId = 0;
                }
                if (Obj_User.IsAdminManagerStaff == "Manager")
                {
                    Obj_User.PortalUser.IsClientViewManager = 1;
                }
                if (Obj_User.IsAdminManagerStaff == "Admin")
                {
                    Obj_User.PortalUser.IsClientViewAdmin = true;
                }
                Obj_User.PortalUser.InternalUser = 0;
                result = ADPortalUsers.SaveUpdatePortalUser(Obj_User.PortalUser);
            }
            return Json(result);
        }

        [HttpPost]
        public ActionResult SaveBranch()
        {
            NewClientVM Obj_NewUser = new NewClientVM();
            if(ModelState.IsValid)
            {
                UpdateModel(Obj_NewUser);
            }
            //Obj_NewUser.ADC_obj.ParentClientId = 

            var RecentClientId = ADClient.CreateUpdateClientBranch(Obj_NewUser.ADC_obj);
            bool SignatureResult = true; 
            if (Obj_NewUser.ADC_BranchSigner.Signature != null && Obj_NewUser.ADC_BranchSigner.Signature.Length > 0)
            {
                Obj_NewUser.ADC_BranchSigner.ClientId = Convert.ToInt32(RecentClientId);
                SignatureResult = ADClientBranchSigner.CreateUpdateSignature(Obj_NewUser.ADC_BranchSigner, Obj_NewUser.ADC_obj.ContactName);
            } 
            return Json(SignatureResult);
        }

        [HttpPost]
        public ActionResult DeleteBranch(string Id)
        {
            var result = ADClient.DeleteClientWithId(Id);
            return Json(result);
        }

        [HttpPost]
        public ActionResult NewNote(string ClientId)
        {
            NoteVM Obj_NoteVM = new NoteVM();
            Obj_NoteVM.Obj_Note.ClientId = Convert.ToInt32(ClientId);
            return PartialView(GetVirtualPath("_Note"), Obj_NoteVM);
        }

        [HttpPost]
        public ActionResult SaveNote()
        {
            NoteVM Obj_NoteVM = new NoteVM();
            if(ModelState.IsValid)
            {
                TryUpdateModel(Obj_NoteVM);
            }
            CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
            var result =  ADIssues.SaveUpdateClosedIssue(Obj_NoteVM.Obj_Note, CU_Obj);
            return Json(result);
        }

        [HttpPost]
        public ActionResult ShowNoteDetails(string id)
        {
            NoteVM Obj_NoteVM = new NoteVM();
            Obj_NoteVM.Obj_Note = ADIssues.GetIssue(Convert.ToInt32(id));
            return PartialView(GetVirtualPath("_Note"), Obj_NoteVM);
        }

        [HttpPost]
        public ActionResult DeleteNote(string id)
        {
            var result = ADIssues.DeleteIssueWithId(id);
            return Json(result);
        }

        [HttpPost]
        public ActionResult NewIssue(string ClientId)
        {
            IssueVM Obj_IssueVM = new IssueVM();
            Obj_IssueVM.Obj_Issue.ClientId = Convert.ToInt32(ClientId);
            Obj_IssueVM.InternalUserList = CommonBindList.GetInternalUsersOpenIssues();
            return PartialView(GetVirtualPath("_OpenIssue"), Obj_IssueVM);
        }

        [HttpPost]
        public ActionResult SaveIssue()
        {
            IssueVM Obj_IssueVM = new IssueVM();
            if (ModelState.IsValid)
            {
                TryUpdateModel(Obj_IssueVM);
            }
            CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
            var result = ADIssues.SaveUpdateOpenIssue(Obj_IssueVM.Obj_Issue, CU_Obj);
            return Json(result);
        }

        [HttpPost]
        public ActionResult ShowOpenIssueDetails(string id)
        {
            IssueVM Obj_IssueVM = new IssueVM();
            Obj_IssueVM.Obj_Issue = ADIssues.GetIssue(Convert.ToInt32(id));
            Obj_IssueVM.InternalUserList = CommonBindList.GetInternalUsersOpenIssues();
            return PartialView(GetVirtualPath("_OpenIssue"), Obj_IssueVM);
        }

        [HttpPost]
        public ActionResult DeleteIssue(string id)
        {
            var result = ADIssues.DeleteIssueWithId(id);
            return Json(result);
        }

        [HttpPost]
        public ActionResult CloseOpenIssue()
        {
            IssueVM Obj_IssueVM = new IssueVM();
            if (ModelState.IsValid)
            {
                TryUpdateModel(Obj_IssueVM);
            }
            CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
            var result = ADIssues.CloseIssue(Obj_IssueVM.Obj_Issue, CU_Obj);
            return Json(result);
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewClients/ClientManagement/View Clients/" + ViewName + ".cshtml";
            return path;
        }

        // default getList
        public List<ADClient> GetList()
        {
            List<ADClient> lstClients = AdClObj.ListClient();            
            return lstClients;
        }

        //filtered list
        public List<ADClient> GetFilteredList(FilterVM FilterVM_Obj, CurrentUser CU_Obj = null)
        {
            List<ADClient> lstClients = AdClObj.FilteredListClient(FilterVM_Obj, CU_Obj);
            return lstClients;
        }

        //bind dropdowns
        public static void BindDropdowns(ref NewClientVM NewClientVM_Obj)
        {
            NewClientVM_Obj.MainClientList = CommonBindList.GetMainClientList();
            NewClientVM_Obj.SalesTeamList = CommonBindList.GetSalesTeamList();
            NewClientVM_Obj.ServiceTeamList = CommonBindList.GetServiceTeamList();
            NewClientVM_Obj.ClientTypeList = CommonBindList.GetClientTypeList();
        }
    }
}
