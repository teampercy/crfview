﻿using CRFView.Adapters;
using CRFView.Adapters.Clients.Client_Management;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace CRFView.Controllers
{
    public class ClientReportController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            ContactListVM ObjContactList = new ContactListVM();
            return PartialView(GetVirtualPath("Index"), ObjContactList);
        }

        [HttpPost]
        public ActionResult ShowClientReportRecords()
        {
            ContactListVM ObjContactList = new ContactListVM();
            if (ModelState.IsValid)
            {
                TryUpdateModel(ObjContactList);
            }
            string CSVString = ADClientReports.PrintClientReports(ObjContactList);
            var path = Path.Combine(Server.MapPath("~/App_Data/DownloadableFiles"), "EmailList.csv");
            Session["EmailListFilePath"] = path;
            ADClientReports.SaveCSVFile(CSVString,path);
            return Json(true);
        }

        public ActionResult ViewEmailListCSV()
        {
            string filename = Session["EmailListFilePath"].ToString();
            byte[] bytes = System.IO.File.ReadAllBytes(filename);
            //string fileName = path.Substring(path.LastIndexOf("\\") + 1);
            return File(bytes, System.Net.Mime.MediaTypeNames.Application.Octet, filename);
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewClients/ClientManagement/Client Reports/" + ViewName + ".cshtml";
            return path;
        }
    }
}
