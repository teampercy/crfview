﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRFView.Adapters.Accounting.AccountingCollections.CreateInvoices;
using System.Web.Mvc;

namespace CRFView.Controllers.Accounting.AccountingCollections
{
    public class CreateInvoicesController:Controller
    {
        public ActionResult Index()
        {
            return View(GetVirtualPath("Index"));
        }

        public ActionResult ShowModal()
        {
            return PartialView(GetVirtualPath("_CreateInvoice"));
        }

        public ActionResult CreateInvoice(AdAccountingCreateInvoice objCreateInvoice)
        {

            return View();
        } 

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewAccounting/AccountingCollections/CreateInvoices/" + ViewName + ".cshtml";
            return path;
        }
    }
}
