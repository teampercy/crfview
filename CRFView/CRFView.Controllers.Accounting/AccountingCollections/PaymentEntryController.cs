﻿using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters;
using CRFView.Adapters.Accounting.AccountingCollections.PaymentEntry;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices.CollectionInvoicesVM;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices.LienInvoicesVM;
using CRFView.Adapters.Clients.Client_Management;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRFView.Controllers.Accounting.AccountingCollections 
{
    public class PaymentEntryController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        [HttpPost]
        public  ActionResult CheckAccount(string AccountId)
        {
            bool result = AdAccountLedger.CheckAccountExists(AccountId);
            return Json(result);
        }

        [HttpPost]
        public ActionResult Search(string AccountId)
        {
            var obj = AdAccountLedger.GetAccountDetail(AccountId);
            return PartialView(GetVirtualPath("_AccountDetails"), obj);
        }

        [HttpPost]
        public ActionResult GetLedger(string AccountId)
        {
            var obj = AdAccountLedger.GetLedgerDetail(AccountId);
            return PartialView(GetVirtualPath("_LedgerDetails"), obj);

        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewAccounting/AccountingCollections/PaymentEntry/" + ViewName + ".cshtml";
            return path;
        }
    }
}
