﻿using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters;
using CRFView.Adapters.Accounting.AccountingCollections.AuditList.AuditListVM;
using CRFView.Adapters.Accounting.AccountingCollections.PaymentEntry;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices.CollectionInvoicesVM;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices.LienInvoicesVM;
using CRFView.Adapters.Clients.Client_Management;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRFView.Controllers.Accounting.AccountingCollections
{
    public class AuditListController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        [HttpPost]
        public ActionResult ShowModal()
        {
            AuditOptionsVM obj = new AuditOptionsVM();
            obj.States = CommonBindList.GetStates();
            obj.ServiceCodes = CommonBindList.GetCollectionServiceCodes();
            obj.FinDocCycle = CommonBindList.GetFinDocCycleList();
            return PartialView(GetVirtualPath("_AuditListOptions"),obj);
        }

        [HttpPost]
        public ActionResult ProcessAuditList(AuditOptionsVM obj)
        {
            return Json(true);
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewAccounting/AccountingCollections/AuditList/" + ViewName + ".cshtml";
            return path;
        }
    }
}
