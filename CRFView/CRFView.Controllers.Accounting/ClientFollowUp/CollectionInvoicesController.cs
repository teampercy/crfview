﻿using CRFView.Adapters;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices.CollectionInvoicesVM;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices.LienInvoicesVM;
using CRFView.Adapters.Clients.Client_Management;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class CollectionInvoicesController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            CollectionInvoicesListVM obj = new CollectionInvoicesListVM();
            obj.CollectionInvoices = ADVWCLIENTCOLLFINDOCEMAIL.ListVWCLIENTCOLLFINDOCEMAIL();
            return PartialView(GetVirtualPath("Index"), obj);
        }

        [HttpPost]
        public ActionResult Search(FormCollection fc)
        {
            string RadioSelection = fc["RadioSelection"];
            string chkRemit = fc["chkRemit"];
            string chkGross = fc["chkGross"];
            string FromDate = fc["FromDate"];
            string ToDate = fc["ToDate"];
            string TxtSearch = fc["TxtSearch"];
            CollectionInvoicesListVM obj = new CollectionInvoicesListVM();
            obj.CollectionInvoices = ADVWCLIENTCOLLFINDOCEMAIL.FilterListADVWCLIENTFINDOCEMAIL(RadioSelection, FromDate,
                                                                     ToDate, TxtSearch, chkRemit, chkGross);
            return PartialView(GetVirtualPath("_CollectionInvoicesList"), obj);
        }


        [HttpPost]
        public ActionResult ShowDetails(string ClientId,string ClientFinDocId, string HistoryId)
        {
            CollectionInvoiceDetailsVM obj = new CollectionInvoiceDetailsVM();
            obj = ADVWCLIENTCOLLFINDOCEMAIL.GetDetails(ClientId,ClientFinDocId, HistoryId);
            return PartialView(GetVirtualPath("_CollInvoiceDetail"), obj);        
       
        }

        [HttpPost]
        public ActionResult FlagAsPaid(string Id)
        {
            bool result = ADClientCollFinDoc.UpdatePaid(Id);
            return Json(result);
        }


        [HttpPost]
        public ActionResult ResendEmail(string Id, string PKID, string InvoiceEmail)
        {
            bool result;
            result = ADClientCollFinDoc.ResendEmailUpdate(Id, PKID, InvoiceEmail);
            return Json(result);
        }

        [HttpPost]
        public ActionResult FlagUnpaid(string Id)
        {
            bool result = false;
            result = ADClientCollFinDoc.FlagUnpaid(Id);
            return Json(result);
        }

        [HttpPost]
        public ActionResult FlagViewed(string PKID)
        {
            bool result = false;
            result = ADReport_History.UpdateViewed(PKID);
            return Json(result);
        }

        [HttpPost]
        public ActionResult CollInvoiceSave(string Id, bool ChkNewReview, bool ChkNewEmail,
                                    bool ChkDeleteReview, string TxtNewReviewDate, string TxtNewEmail)
        {
            bool result = ADClientCollFinDoc.UpdateClientFinDoc(Id, ChkNewReview, ChkNewEmail,
                                             ChkDeleteReview, TxtNewReviewDate, TxtNewEmail);
            return Json(result);
        }

        [HttpPost]
        public ActionResult ShowAddNote(string ClientId)
        {
            NoteVM Obj_NoteVM = new NoteVM();
            Obj_NoteVM.Obj_Note.ClientId = Convert.ToInt32(ClientId);
            return PartialView(GetVirtualPath("_Note"), Obj_NoteVM);
        }

        [HttpPost]
        public ActionResult SaveNote(NoteVM Obj_NoteVM)
        {
            CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
            bool result = ADIssues.SaveNoteClientFollowup(Obj_NoteVM.Obj_Note, CU_Obj);
            return Json(result);
        }

        public ActionResult ViewAttachment(string PKId)
        {
            var Attachment = ADreport_historyattachment.ReadReport(PKId);
            var path = Path.Combine(Server.MapPath("~/App_Data/DownloadableFiles"), Attachment.url);
            ADreport_historyattachment.SaveFileToBeViewed(Attachment.image, path);
            byte[] fileBytes = Attachment.image;
            string fileName = path.Substring(path.LastIndexOf("\\") + 1);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewAccounting/ClientFollowUp/CollectionInvoices/" + ViewName + ".cshtml";            
            return path;
        }
    }
}
