﻿using CRFView.Adapters;
using CRFView.Adapters.Clients.Client_Management;
using CRFView.Adapters.Collections.Collectors.WorkCard;
using CRFView.Adapters.Collections.Collectors.WorkCard.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace CRFView.Controllers.Collections.Collectors.WorkCard
{
    public class WorkCardController : Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            List<ADvwAccountFilterList> lst = ADvwAccountFilterList.GetInitialList();
            AccountFilterListVM ObjVm = new AccountFilterListVM();
            ObjVm.ObjAccounts = lst;
            return PartialView(GetVirtualPath("Index"), ObjVm);
        }

        [HttpGet]
        public ActionResult ShowFilterModal()
        {
            FilterVM ObjVm = new FilterVM();
            return PartialView(GetVirtualPath("_FilterNew"), ObjVm);
        }

        [HttpPost]
        public ActionResult GetFilterResult(Adapters.Collections.Collectors.WorkCard.Filter FilterObj)
        {
            List<ADvwAccountFilterList> lst = ADvwAccountFilterList.GetFilteredList(FilterObj);
            AccountFilterListVM ObjVm = new AccountFilterListVM();
            ObjVm.ObjAccounts = lst;
            return PartialView(GetVirtualPath("_AccountListNew"), ObjVm);
        }

        [HttpPost]
        public ActionResult LoadDebtAccount(string DebtAccountId)
        {
            DebtAccountVM obj;
            obj = AccountInfo.LoadInfo(DebtAccountId);
            return PartialView(GetVirtualPath("_LoadAccount"),obj);
        }

        [HttpPost]
        public ActionResult GetNoteDetails (string Id)
        {
            NoteVM obj = new NoteVM();
            obj.obj_note = ADDebtAccountNote.ReadNote(Convert.ToInt32(Id));
            return PartialView(GetVirtualPath("_Note"), obj);
        }

        [HttpPost]
        public ActionResult GetAddressDetails(string DebtAddressId)
        {
            AddressVM obj = new AddressVM();
            obj.ObjAddress = ADDebtAddress.ReadAddress(Convert.ToInt32(DebtAddressId));
            return PartialView(GetVirtualPath("_Address"), obj);
        }

        //Read Edit Account Info
        [HttpPost]
        public ActionResult EditAccount(string DebtAccountId)
        {
            EditDebtAccountVM objDebtAccount = new EditDebtAccountVM();
            objDebtAccount.obj_DebtAccountEdit =   ADDebtAccount.ReadDebtAccount(Convert.ToInt32(DebtAccountId));
            objDebtAccount.obj_DebtEdit = ADDebt.ReadDebt(objDebtAccount.obj_DebtAccountEdit.DebtId);
            objDebtAccount.obj_DebtAddressEdit = ADDebtAddress.ReadAddressByDebtAccountId(DebtAccountId);
            return PartialView(GetVirtualPath("_EditAccount"), objDebtAccount);
        }

        //Update Edit Account Info
        [HttpPost]
        public ActionResult SaveAccountDetails(EditDebtAccountVM objUpdateDebtAccount)
        {

            CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
            if(AccountInfo.LogAccountChanges(objUpdateDebtAccount, CU_Obj))
            {
                ADDebtAccount.UpdateDebtAccountEditView(objUpdateDebtAccount.obj_DebtAccountEdit);
                ADDebt.UpdateDebtEditView(objUpdateDebtAccount.obj_DebtEdit, CU_Obj);
                ADDebtAddress.UpdateAddressEditView(objUpdateDebtAccount.obj_DebtAddressEdit);
            }
            DebtAccountVM obj = new DebtAccountVM();
            obj.obj_ADDebtAccount = ADDebtAccount.ReadDebtAccount(Convert.ToInt32(objUpdateDebtAccount.obj_DebtAccountEdit.DebtAccountId));
            obj = AccountInfo.LoadInfo(Convert.ToString(objUpdateDebtAccount.obj_DebtAccountEdit.DebtAccountId));

            return PartialView(GetVirtualPath("_LoadAccount"), obj);
        }

        [HttpPost]
        public ActionResult GetSortedResult(string SortByValue, string SortTypeValue)
        {

            string SortType = "ASC";
            string SortBy = "DebtAccountId";
            if (SortTypeValue == "-1")
            {
                SortType = "ASC";
            }
            else if (SortTypeValue == "1")
            {
                SortType = "DESC";
            }

            if (SortByValue == "-1")
            {
                SortBy = "DebtAccountId "+ SortType;
            }
            else if (SortByValue == "1")
            {
                SortBy = "DebtorName " + SortType;
            }
            else if (SortByValue == "2")
            {
                SortBy = "Contact " + SortType;
            }
            else if (SortByValue == "3")
            {
                SortBy = "DebtorAddress " + SortType;
            }
            else if (SortByValue == "4")
            {
                SortBy = "DebtorRef " + SortType;
            }
            else if (SortByValue == "5")
            {
                SortBy = "StatusCode " + SortType;
            }
            else if (SortByValue == "6")
            {
                SortBy = "Desk " + SortType;
            }
            else if (SortByValue == "7")
            {
                SortBy = "ToDesk " + SortType;
            }
            else if (SortByValue == "8")
            {
                SortBy = "DateAssigned " + SortType;
            }
            else if (SortByValue == "9")
            {
                SortBy = "ReviewDate " + SortType;
            }
            else if (SortByValue == "10")
            {
                SortBy = "LastPayment " + SortType;
            }
            else if (SortByValue == "11")
            {
                SortBy = "CloseDate " + SortType;
            }
            else if (SortByValue == "12")
            {
                SortBy = "AssignmentAmt " + SortType;
            }
            else if (SortByValue == "13")
            {
                SortBy = "CollectFees " + SortType;
            }
            else if (SortByValue == "14")
            {
                SortBy = "Interest " + SortType;
            }
            else if (SortByValue == "15")
            {
                SortBy = "Payments " + SortType;
            }
            else if (SortByValue == "16")
            {
                SortBy = "TotalOwned " + SortType;
            }
            else if (SortByValue == "17")
            {
                SortBy = "SCRevDate " + SortType;
            }
            else if (SortByValue == "18")
            {
                SortBy = "SCTrialDate " + SortType;
            }
            else if (SortByValue == "19")
            {
                SortBy = "SCJudgeDate " + SortType;
            }
            else if (SortByValue == "20")
            {
                SortBy = "FWDCourtDate " + SortType;
            }
            else if (SortByValue == "21")
            {
                SortBy = "FWDJudgeDate " + SortType;
            }

            List<ADvwAccountFilterList> lst = ADvwAccountFilterList.GetSortedList(SortBy);
            AccountFilterListVM ObjVm = new AccountFilterListVM();
            ObjVm.ObjAccounts = lst;
            return PartialView(GetVirtualPath("_AccountListNew"), ObjVm);
        }

        [HttpPost]
        public ActionResult ShowPaymentCalculator(string DebtAccountId, string TotalDue)
        {
            PaymentCalculatorVM obj = new PaymentCalculatorVM();
            obj.TotalDue = Convert.ToDouble(TotalDue);
            obj.TotalDueP = Convert.ToDouble(TotalDue);
            return PartialView(GetVirtualPath("_PaymentCalculator"), obj);
        }

        [HttpPost]
        public ActionResult ShowDBFaxAccountInfo(string DebtAccountId)
        {
            FaxAccountInfoVM obj = new FaxAccountInfoVM();
            obj.obj_Debt = ADDebt.ReadDebt(Convert.ToInt32(DebtAccountId));
            obj.DebtAccountId = Convert.ToInt32(DebtAccountId);
            obj.DBFaxContact = obj.obj_Debt.DebtorName;
            return PartialView(GetVirtualPath("_DBFaxAccountInfo"), obj);
        }

        [HttpPost]
        public ActionResult ShowDBFaxFromName(string DebtAccountId)
        {
            FaxFromNameVM obj = new FaxFromNameVM();
            obj.obj_DebtAccount = ADDebtAccount.ReadDebtAccount(Convert.ToInt32(DebtAccountId));
            obj.obj_Location= ADLocation.ReadLocation(obj.obj_DebtAccount.LocationId);
            obj.LocationDescr = obj.obj_Location.LocationDescr;
            return PartialView(GetVirtualPath("_DBFaxFromName"), obj);
        }

        [HttpPost]
        public ActionResult ShowCLFaxAccountInfo(string DebtAccountId)
        {
            FaxAccountInfoVM obj = new FaxAccountInfoVM();
            obj.obj_DebtAccount = ADDebtAccount.ReadDebtAccount(Convert.ToInt32(DebtAccountId));
            obj.obj_Client= ADClient.GetClientDetails(Convert.ToInt32(obj.obj_DebtAccount.ClientId));
            obj.DebtAccountId = Convert.ToInt32(DebtAccountId);
            obj.CLFaxContact = obj.obj_Client.ContactName;
            return PartialView(GetVirtualPath("_CLFaxAccountInfo"), obj);
        }

        [HttpPost]
        public ActionResult ShowCLFaxFromName(string DebtAccountId)
        {
            FaxFromNameVM obj = new FaxFromNameVM();
            obj.obj_DebtAccount = ADDebtAccount.ReadDebtAccount(Convert.ToInt32(DebtAccountId));
            obj.obj_Location = ADLocation.ReadLocation(obj.obj_DebtAccount.LocationId);
            obj.LocationDescr = obj.obj_Location.LocationDescr;
            return PartialView(GetVirtualPath("_CLFaxFromName"), obj);
        }

        [HttpPost]
        public string FaxPrint(string DebtAccountId,string FaxCode)
        {
            FaxFromNameVM obj =new FaxFromNameVM();
            obj.obj_DebtAccount = ADDebtAccount.ReadDebtAccount(Convert.ToInt32(DebtAccountId));
            obj.obj_Debt= ADDebt.ReadDebt(Convert.ToInt32(DebtAccountId));
            obj.obj_Client = ADClient.GetClientDetails(Convert.ToInt32(obj.obj_DebtAccount.ClientId));

            switch (FaxCode)
            {
                case "CL":
                    obj.FaxContactName = obj.obj_Client.ContactName;
                    break;

                case "DB":
                    obj.FaxContactName = obj.obj_Debt.DebtorName;
                    break;
            }
 
            obj.obj_Location = ADLocation.ReadLocation(obj.obj_DebtAccount.LocationId);
            obj.LocationDescr = obj.obj_Location.LocationDescr;
            
            string PdfUrl = obj.PrintFax(DebtAccountId, FaxCode, obj.FaxContactName, obj.LocationDescr);

            if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
            {
                string path = Path.Combine(Url.Content("~"), "Output/Reports/", System.IO.Path.GetFileName(PdfUrl));
                return (path);
            }
            return "error";
        }

        //Show Client Info
        [HttpPost]
        public ActionResult ShowClientInfo(string ClientId)
        {
            ClientInfoVM obj = new ClientInfoVM();
            ADClientCollectionInfo oj = new ADClientCollectionInfo();
            obj.obj_ClientCollectionInfo = oj.LoadCollectionInfo(Convert.ToInt32(ClientId));
            return PartialView(GetVirtualPath("_ShowClientInfo"), obj);
        }

        //Read Edit More Info
        [HttpPost]
        public ActionResult EditMoreInfo(string DebtAccountId)
        {
            EditMoreInfoVM objMoreInfo = new EditMoreInfoVM();
            objMoreInfo.obj_DebtAccountMoreInfoEdit = ADDebtAccount.ReadDebtAccount(Convert.ToInt32(DebtAccountId));
            return PartialView(GetVirtualPath("_EditMoreInfo"), objMoreInfo);
        }

        //Update Edit More Info
        [HttpPost]
        public ActionResult SaveMoreInfoDetails(EditMoreInfoVM objUpdateDebtAccountMoreInfo)
        {
            ADDebtAccount.UpdateDebtAccountMoreInfoEditView(objUpdateDebtAccountMoreInfo.obj_DebtAccountMoreInfoEdit);
            DebtAccountVM obj = new DebtAccountVM();
            obj.obj_ADDebtAccount = ADDebtAccount.ReadDebtAccount(Convert.ToInt32(objUpdateDebtAccountMoreInfo.obj_DebtAccountMoreInfoEdit.DebtAccountId));
            obj = AccountInfo.LoadInfo(Convert.ToString(objUpdateDebtAccountMoreInfo.obj_DebtAccountMoreInfoEdit.DebtAccountId));
       
            return PartialView(GetVirtualPath("_LoadAccount"), obj);
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewCollections/Collectors/WorkCard/" + ViewName + ".cshtml";
            return path;
        }
    }
}
