
CREATE PROCEDURE [dbo].[cview_Accounts_UpdateActions]
(
	@DebtAccountId int,
	@ActionIds varchar(255),
	@UserId	Int,
	@UserName varchar(50),
	@RETURNVALUE INT = Null OUT
)
AS
	declare @ItemId as integer
	declare @error_var as integer
	DECLARE TABLECURSOR CURSOR FOR 
	select ja.id from CollectionActions ja with (nolock)
	join DBO.FN_STRINGTOTABLE(@Actionids) t
 	ON t.DATAVALUE = ja.ID
	
	
	OPEN TABLECURSOR
	FETCH NEXT FROM TABLECURSOR INTO @ItemId
	WHILE @@FETCH_STATUS = 0
	BEGIN

	EXEC @RETURNVALUE = cview_Accounts_CreateActionHistory @DebtAccountId, @ItemId, @UserId, @UserName
			
	---- knock off client notes from list if web-note actioncode selected
	--UPDATE DebtAccountNote
	--SET CLOSEDATE = GETDATE()
	----SELECT JOBID,DATECREATED,ID
	--From DebtAccountNote jh with (nolock)
	--where jh.ClientView = 1
	--AND ( jh.CloseDate Is Null ) AND ( JH.DebtAccountId = @DebtAccountId and @ItemId = 170 )

	FETCH NEXT FROM TABLECURSOR INTO @ItemId
   
	END
	CLOSE TABLECURSOR
	DEALLOCATE TABLECURSOR

	
windup:

IF @ERROR_VAR = 0 

   BEGIN
      SET @RETURNVALUE = 0
   END
ELSE
   BEGIN
      IF @ERROR_VAR <> 0 
        SET @RETURNVALUE = @ERROR_VAR
      ELSE
        SET @RETURNVALUE = -99 -- UNEXPECTED NR OF RECORDS AFFECTED
   END