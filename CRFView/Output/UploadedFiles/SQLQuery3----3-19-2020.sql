
--  TX Job Review List - JV TX Job Review for CRFView
--	VerifiedDate is Null - only in TX Job Review, not in the TX Notice Request
--	9/4/2018	Changed from TXThreshold to NDThreshold
--	9/5/2018	Added JobStatus.IsCancelled to the filter
--	9/18/2018	Fixed the HasOpenInvoice criteria
---------------------------------------------------------------------------------------------------

CREATE PROCEDURE [dbo].[uspbo_Jobs_GetMonthlyJobListJV] (
	@ClientCode 	    VARCHAR(50)=NULL,
	@JobState			VARCHAR(2),
	@RETURNVALUE		INT = NULL OUT)

as

Declare @ThruDate as datetime
Set @ThruDate = DATEADD(s,-1,DATEADD(mm, DATEDIFF(m,0,GETDATE()) -1,0))
Set @ThruDate = CONVERT(Date, @ThruDate)


create table #list (ListId int primary key identity(1,1),uid int, HasOpenInvoice bit)
insert into #list
( uid )
SELECT DISTINCT 
JobId
FROM  vwJobsListJV vw with (nolock)
JOIN tblRunClient r on vw.ClientId = r.ClientId
WHERE 
(vw.ClientCode = @ClientCode or @ClientCode is Null)
and vw.JobState = @JobState
and JobBalance >= r.NDBalanceThreshold
and DateAssigned is not Null
and VerifiedDate is Null
and Not Exists (Select 1 from JobTexasRequest where JobId = vw.JobId and IsProcessed = 0)
--and Not Exists (Select 1 from JobTexasRequest where JobId = vw.JobId and IsProcessed = 0)
--and Exists (Select 1 from JobInvoice where InvoiceDate <= @ThruDate and Status = 'OP')
and IsNoticeRequestStatus = 1 
and IsNoticeRequestStatusJV = 1
and ((r.UseChangeType = 0 and NoNTO <> 1) or (r.UseChangeType = 1 and IsNull(JobNoNTO, 0) <> 1))
and isnull(vw.IsCancelled, 0) = 0
order by JobId desc

Update #list
set HasOpenInvoice = 1
From #list t
join JobRental r on r.JobId = t.uid
join JobInvoice i on i.JobRentalId = r.id 
where InvoiceDate <= @ThruDate and Status = 'OP'

SELECT 
vw.JobId,
ClientCode,
StatusCode,
JobName,
JobNum,
JobAdd1,
JobCity,
JobState,
JobCounty,
CustRefNum,
ClientCustomer,
GeneralContractor,
OwnrName,
LenderName,
EstBalance,
DeskNum,
CustFax,
GCFax,
DateAssigned,
StartDate,
RevDate,
NoticeSent,
VerifiedDate,
PONum,
LenderRefNum as BondNum,
BranchNum,
JobBalance
from #list l
join vwJobsList vw with (nolock) on vw.JobId = l.uid
where HasOpenInvoice = 1
order by l.listid



drop table #list