﻿using CRFView.Adapters;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Clients.ClientManagement
{
    public class RapidPayClientsController : Controller
    {

        public ActionResult Index()
        {
            List<ADClientRapidPay> list = ADClientRapidPay.GetInitialList();
            RapidPayClientsVM ObjRapidPayClientsVM = new RapidPayClientsVM();
            ObjRapidPayClientsVM.ObjClientRapidPayList = list;
            return PartialView(GetVirtualPath("Index"), ObjRapidPayClientsVM);
        }

        public ActionResult RefreshList()
        {
            List<ADClientRapidPay> list = ADClientRapidPay.GetInitialList();
            RapidPayClientsVM ObjRapidPayClientsVM = new RapidPayClientsVM();
            ObjRapidPayClientsVM.ObjClientRapidPayList = list;
            return PartialView(GetVirtualPath("_RapidPayClientsList"), ObjRapidPayClientsVM);
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModalADDRapidPay()
        {
            RapidPayClientsVM ObjRapidPayClientsVM = new RapidPayClientsVM();
            return PartialView(GetVirtualPath("_ADDRapidPayClients"), ObjRapidPayClientsVM);
        }

        [HttpPost]
        public ActionResult ShowFilterList(RapidPayClientsVM ObjRapidPayClientsVM)
        {
            List<ADClientRapidPay> list = ADClientRapidPay.GetInitialFilterList(ObjRapidPayClientsVM);
            RapidPayClientsVM objRapidVM = new RapidPayClientsVM();
            objRapidVM.ObjClientRapidPayList = list;
            return PartialView(GetVirtualPath("_RapidPayClientsList"), objRapidVM);
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowFilterRapidPayModal()
        {
            RapidPayClientsVM ObjRapidPayClientsVM = new RapidPayClientsVM();
            return PartialView(GetVirtualPath("_ViewFilter"), ObjRapidPayClientsVM);
        }

        //Add New Details
        [HttpPost]
        public int AddRapidPayClients(RapidPayClientsVM ObjRapidPayClientsVM)
        {
            

            return ObjRapidPayClientsVM.objADClientRapidPay.AddRapidPayClients(ObjRapidPayClientsVM.objADClientRapidPay, ObjRapidPayClientsVM);
        }

        //Update New Details
        [HttpPost]
        public int UpdateRapidPayClients(RapidPayClientsVM ObjRapidPayClientsVM)
        {

            return ObjRapidPayClientsVM.objADClientRapidPay.UpdateRapidPayClients(ObjRapidPayClientsVM);
        }



        [HttpPost]
        public ActionResult LoadRapidPayClient(int RapidPayId)
        {

            RapidPayClientsVM ObjRapidPayClientsVM = new RapidPayClientsVM();
            //ObjRapidPayClientsVM.JobId = Convert.ToInt32(RapidPayId);
            DataTable dtRapidPayClientList = ADClientRapidPay.LoadInfo(RapidPayId);

            ObjRapidPayClientsVM.objADClientRapidPay.RapidPayId = ((dtRapidPayClientList.Rows[0]["RapidPayId"] != DBNull.Value ? Convert.ToInt32(dtRapidPayClientList.Rows[0]["RapidPayId"]) : Int32.MinValue));
            ObjRapidPayClientsVM.objADClientRapidPay.ClientId = ((dtRapidPayClientList.Rows[0]["ClientId"] != DBNull.Value ? Convert.ToInt32(dtRapidPayClientList.Rows[0]["ClientId"]) : Int32.MinValue));
            ObjRapidPayClientsVM.objADClientRapidPay.ClientName = ((dtRapidPayClientList.Rows[0]["ClientName"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["ClientName"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.AddressLine1 = ((dtRapidPayClientList.Rows[0]["AddressLine1"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["AddressLine1"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.AddressLine2 = ((dtRapidPayClientList.Rows[0]["AddressLine2"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["AddressLine2"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.City = ((dtRapidPayClientList.Rows[0]["City"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["City"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.State = ((dtRapidPayClientList.Rows[0]["State"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["State"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.PostalCode = ((dtRapidPayClientList.Rows[0]["PostalCode"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["PostalCode"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.Email = ((dtRapidPayClientList.Rows[0]["Email"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["Email"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.PhoneNo = ((dtRapidPayClientList.Rows[0]["PhoneNo"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["PhoneNo"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.PhoneExt = ((dtRapidPayClientList.Rows[0]["PhoneExt"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["PhoneExt"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.Mobile = ((dtRapidPayClientList.Rows[0]["Mobile"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["Mobile"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.Fax = ((dtRapidPayClientList.Rows[0]["Fax"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["Fax"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.Note = ((dtRapidPayClientList.Rows[0]["Note"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["Note"].ToString() : ""));

            ObjRapidPayClientsVM.objADClientRapidPay.NextContactDate = ((dtRapidPayClientList.Rows[0]["NextContactDate"] != DBNull.Value ? Convert.ToDateTime(dtRapidPayClientList.Rows[0]["NextContactDate"]) : DateTime.MinValue));

            ObjRapidPayClientsVM.objADClientRapidPay.ContactName = ((dtRapidPayClientList.Rows[0]["ContactName"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["ContactName"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.ContactTitle = ((dtRapidPayClientList.Rows[0]["ContactTitle"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["ContactTitle"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.ContactSalutation = ((dtRapidPayClientList.Rows[0]["ContactSalutation"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["ContactSalutation"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.ContactDOB = ((dtRapidPayClientList.Rows[0]["ContactDOB"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["ContactDOB"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.LoginCode = ((dtRapidPayClientList.Rows[0]["LoginCode"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["LoginCode"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.LoginPassWord = ((dtRapidPayClientList.Rows[0]["LoginPassWord"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["LoginPassWord"].ToString() : ""));

            ObjRapidPayClientsVM.objADClientRapidPay.CreatedDate = ((dtRapidPayClientList.Rows[0]["CreatedDate"] != DBNull.Value ? Convert.ToDateTime(dtRapidPayClientList.Rows[0]["CreatedDate"]) : DateTime.MinValue));
            ObjRapidPayClientsVM.objADClientRapidPay.SetupDate = ((dtRapidPayClientList.Rows[0]["SetupDate"] != DBNull.Value ? Convert.ToDateTime(dtRapidPayClientList.Rows[0]["SetupDate"]) : DateTime.MinValue));

            ObjRapidPayClientsVM.objADClientRapidPay.IsCancelled = ((dtRapidPayClientList.Rows[0]["IsCancelled"] != DBNull.Value ? Convert.ToBoolean(dtRapidPayClientList.Rows[0]["IsCancelled"]) : false));

            ObjRapidPayClientsVM.objADClientRapidPay.ClientCode = ((dtRapidPayClientList.Rows[0]["ClientCode"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["ClientCode"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.PasswordHint1 = ((dtRapidPayClientList.Rows[0]["PasswordHint1"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["PasswordHint1"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.PasswordHint2 = ((dtRapidPayClientList.Rows[0]["PasswordHint2"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["PasswordHint2"].ToString() : ""));

            ObjRapidPayClientsVM.objADClientRapidPay.OrderDate = ((dtRapidPayClientList.Rows[0]["OrderDate"] != DBNull.Value ? Convert.ToDateTime(dtRapidPayClientList.Rows[0]["OrderDate"]) : DateTime.MinValue));

            ObjRapidPayClientsVM.objADClientRapidPay.IsReorder = ((dtRapidPayClientList.Rows[0]["IsReorder"] != DBNull.Value ? Convert.ToBoolean(dtRapidPayClientList.Rows[0]["IsReorder"]) : false));

            ObjRapidPayClientsVM.objADClientRapidPay.LaborType = ((dtRapidPayClientList.Rows[0]["LaborType"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["LaborType"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.CouponCode = ((dtRapidPayClientList.Rows[0]["CouponCode"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["CouponCode"].ToString() : ""));

            ObjRapidPayClientsVM.JobUnits = ((dtRapidPayClientList.Rows[0]["JobUnits"] != DBNull.Value ? Convert.ToInt32(dtRapidPayClientList.Rows[0]["JobUnits"]) : Int32.MinValue));

            ObjRapidPayClientsVM.UnitPrice = Convert.ToString((dtRapidPayClientList.Rows[0]["UnitPrice"] != DBNull.Value ? Convert.ToDecimal(dtRapidPayClientList.Rows[0]["UnitPrice"]) : 0));

            ObjRapidPayClientsVM.objADClientRapidPay.CCType = ((dtRapidPayClientList.Rows[0]["CCType"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["CCType"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.CCName = ((dtRapidPayClientList.Rows[0]["CCName"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["CCName"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.CCCardNO = ((dtRapidPayClientList.Rows[0]["CCCardNO"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["CCCardNO"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.CCExpDate = ((dtRapidPayClientList.Rows[0]["CCExpDate"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["CCExpDate"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.CCSSIDNo = ((dtRapidPayClientList.Rows[0]["CCSSIDNo"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["CCSSIDNo"].ToString() : ""));

            ObjRapidPayClientsVM.objADClientRapidPay.BillingSameAsMain = ((dtRapidPayClientList.Rows[0]["BillingSameAsMain"] != DBNull.Value ? Convert.ToBoolean(dtRapidPayClientList.Rows[0]["BillingSameAsMain"]) : false));

            ObjRapidPayClientsVM.objADClientRapidPay.BillingAddressLine1 = ((dtRapidPayClientList.Rows[0]["BillingAddressLine1"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["BillingAddressLine1"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.BillingAddressLine2 = ((dtRapidPayClientList.Rows[0]["BillingAddressLine2"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["BillingAddressLine2"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.BillingState = ((dtRapidPayClientList.Rows[0]["BillingState"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["BillingState"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.BillingPostalCode = ((dtRapidPayClientList.Rows[0]["BillingPostalCode"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["BillingPostalCode"].ToString() : ""));

            ObjRapidPayClientsVM.objADClientRapidPay.TotUnitsPurchased = ((dtRapidPayClientList.Rows[0]["TotUnitsPurchased"] != DBNull.Value ? Convert.ToInt32(dtRapidPayClientList.Rows[0]["TotUnitsPurchased"]) : Int32.MinValue));
            ObjRapidPayClientsVM.objADClientRapidPay.TotUnitsUsed = ((dtRapidPayClientList.Rows[0]["TotUnitsUsed"] != DBNull.Value ? Convert.ToInt32(dtRapidPayClientList.Rows[0]["TotUnitsUsed"]) : Int32.MinValue));
            ObjRapidPayClientsVM.objADClientRapidPay.TotUnitsAdjusted = ((dtRapidPayClientList.Rows[0]["TotUnitsAdjusted"] != DBNull.Value ? Convert.ToInt32(dtRapidPayClientList.Rows[0]["TotUnitsAdjusted"]) : Int32.MinValue));

            ObjRapidPayClientsVM.TotPurchaseAmt = Convert.ToString(((dtRapidPayClientList.Rows[0]["TotPurchaseAmt"] != DBNull.Value ? Convert.ToDecimal(dtRapidPayClientList.Rows[0]["TotPurchaseAmt"]) : 0)));

            ObjRapidPayClientsVM.objADClientRapidPay.IsOrderPrinted = ((dtRapidPayClientList.Rows[0]["IsOrderPrinted"] != DBNull.Value ? Convert.ToBoolean(dtRapidPayClientList.Rows[0]["IsOrderPrinted"]) : false));

            ObjRapidPayClientsVM.objADClientRapidPay.BillingName = ((dtRapidPayClientList.Rows[0]["BillingName"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["BillingName"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.BillingCity = ((dtRapidPayClientList.Rows[0]["BillingCity"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["BillingCity"].ToString() : ""));
            ObjRapidPayClientsVM.objADClientRapidPay.RefBy = ((dtRapidPayClientList.Rows[0]["RefBy"] != DBNull.Value ? dtRapidPayClientList.Rows[0]["RefBy"].ToString() : ""));

            return PartialView(GetVirtualPath("_EditRapidPayClient"), ObjRapidPayClientsVM);
        }

        //Print RapidPay Client
        [HttpPost]
        public string PrintRapidPayClient(string RapidPayId)
        {
            ADClientRapidPay objADClientRapidPay = new ADClientRapidPay();
            string PdfUrl = Convert.ToString(objADClientRapidPay.PrintRapidPayClients(RapidPayId));
            if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
            {
                string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                return (path);
            }

            return "error";
        }

        // Check if duplicate client code
        [HttpPost]
        public ActionResult ChechkDuplicateClient(string ClientCode)
        {
            ADClientRapidPay objADClientRapidPay = new ADClientRapidPay();
            bool result = objADClientRapidPay.IsDuplicateClient(ClientCode);
            return Json(result);
        }


        ////Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewClients/ClientManagement/RapidPay Clients/" + ViewName + ".cshtml";
            return path;
        }

    }
}
