﻿using CRF.BLL;
using CRFView.Adapters;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Clients.ClientManagement
{
    public class EmailBlastsController : Controller
    {


        public ActionResult Index()
        {
            return View(GetVirtualPath("Index"));
        }

        public ActionResult ShowModal()
        {
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            EmailBlastsVM objEmailBlastsVM = new EmailBlastsVM();
            objEmailBlastsVM.FromName = user.UserName;
            objEmailBlastsVM.FromEmail = user.Email;
            return PartialView(GetVirtualPath("_EmailBlasts"), objEmailBlastsVM);
        }


        [HttpPost]
        public ActionResult GetEmailBlastList(EmailBlastsVM objEmailBlastsVM)
        {


            objEmailBlastsVM.dtEmailBlastsList = ADBatchClientEmailBlast.GetEmailBlastsDt(objEmailBlastsVM);
            objEmailBlastsVM.EmailBlastsList = ADBatchClientEmailBlast.GetEmailBlastsList(objEmailBlastsVM.dtEmailBlastsList);

            ViewBag.lblRecCount = "Total  (" + objEmailBlastsVM.EmailBlastsList.Count + ") Records";

            return PartialView(GetVirtualPath("_EmailBlastsList"), objEmailBlastsVM);
        }

        public ActionResult SaveEmailBlast(EmailBlastsVM objEmailBlastsVM)
        {
             bool result ;


            objEmailBlastsVM.dtEmailBlastsList = ADBatchClientEmailBlast.GetEmailBlastsDt(objEmailBlastsVM);
            objEmailBlastsVM.EmailBlastsList = ADBatchClientEmailBlast.GetEmailBlastsList(objEmailBlastsVM.dtEmailBlastsList);

            


            result = ADBatchClientEmailBlast.SendEmailBlastDetails(objEmailBlastsVM, objEmailBlastsVM.EmailBlastsList);


            return Json(result);

        }



        ////Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewClients/ClientManagement/EmailBlasts/" + ViewName + ".cshtml";
            return path;
        }


    }
}
