﻿using CRFView.Adapters;
using CRFView.Adapters.Clients.Client_Management;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace CRFView.Controllers.Clients.ClientConfigurations
{
    public class ClientTypesController : Controller
    {
        ADClientTypes objADClientTypes = new ADClientTypes();
        // GET: ClientType
        public ActionResult Index()
        {
            List<ADClientTypes> lstClientType = objADClientTypes.ListClientType();
            Session["ListClientType"] = lstClientType;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"), lstClientType);
        }
        //Show details on Edit
        public ActionResult ClientTypeDetails(int id)
        {
            ADClientTypes objClientType = objADClientTypes.ReadClientType(id);
            return View(GetVirtualPath("ClientTypeDetails"), objClientType);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult ClientTypeSave(ADClientTypes updatedClientType)
        {
            objADClientTypes.SaveClientType(updatedClientType);
            List<ADClientTypes> lstClientType = objADClientTypes.ListClientType();
            Session["ListClientType"] = lstClientType;
            return PartialView(GetVirtualPath("_ClientTypeList"), lstClientType);
        }
        //Show blank details on Add
        public ActionResult ClientTypeAdd()
        {
            ADClientTypes objClientType = new ADClientTypes();
            ModelState.Clear();
            objClientType.ClientTypeId = 0;
            return View(GetVirtualPath("ClientTypeDetails"), objClientType);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {
            List<ADClientTypes> ListClientTypeFilter = new List<ADClientTypes>();
            List<ADClientTypes> ListClientType = (List<ADClientTypes>)Session["ListClientType"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    ListClientTypeFilter = (from item in ListClientType
                                            where item.ClientTypeCode.ToLower().Contains(value.ToLower())
                                            select item).ToList();

                }
                else if (Key == "2")
                {
                    ListClientTypeFilter = (from item in ListClientType
                                            where item.ClientTypeName.ToLower().Contains(value.ToLower())
                                            select item).ToList();
                }
            }
            return View(GetVirtualPath("_ClientTypeList"), ListClientTypeFilter);
        }
        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADClientTypes> lstClientType = objADClientTypes.ListClientType();
            Session["ListClientType"] = lstClientType;
            return PartialView(GetVirtualPath("_ClientTypeList"), lstClientType);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "ClientType Code" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "ClientType Name" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewClients/ClientConfigurations/ClientType/" + ViewName + ".cshtml";
            return path;
        }
    }
}
