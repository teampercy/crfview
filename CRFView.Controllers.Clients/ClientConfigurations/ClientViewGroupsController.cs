﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CRFView.Adapters.Clients.Client_Management;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using CRFView.Adapters;

namespace CRFView.Controllers.Clients.ClientConfigurations
{
    public class ClientViewGroupsController : Controller
    { 
        ADClientViewGroup objADClientViewGroup = new ADClientViewGroup();

        public ActionResult Index()
        {
            List<ADClientViewGroup> ClientViewGroupList = objADClientViewGroup.ListClientViewGroup();
            Session["ListClientViewGroup"] = ClientViewGroupList;
            SetFilterList();
            ViewBag.totalCount = ClientViewGroupList.Count;
            return PartialView(GetVirtualPath("Index"), ClientViewGroupList);
        }

        //[HttpPost]
        //public ActionResult Search(string value, string Key)
        //{
        //    List<ADClientViewGroup> ClientViewGroupList = (List<ADClientViewGroup>)Session["ListClientViewGroup"];
        //    List<ADClientViewGroup> objClientViewGroupFilter = new List<ADClientViewGroup>();

        //    if (value != "" || value != null)
        //    {
        //        if (Key == "1")
        //        {
        //            objClientViewGroupFilter = (from item in ClientViewGroupList
        //                                        where item.ClientViewGroupName.ToLower().Contains(value.ToLower())
        //                                        select item).ToList();
        //        }
        //        else if (Key == "2")
        //        {
        //            objClientViewGroupFilter = (from item in ClientViewGroupList
        //                                        where item.ClientCodeList.ToLower().Contains(value.ToLower())
        //                                        select item).ToList();
        //        }



        //    }
        //    return View(GetVirtualPath("_ClientViewGroupList"), objClientViewGroupFilter);
        //}

        //Show details on Edit
        public ActionResult ClientViewGroupDetails(int id)
        {
            objADClientViewGroup = objADClientViewGroup.ReadClientViewGroup(id);
            ViewBag.ddlAvailableList = CommonBindList.GetClientList();
            ViewBag.ddlSelectedList = CommonBindList.GetSelectedClientList(objADClientViewGroup.ClientCodeList);
            return View(GetVirtualPath("ClientViewGroupDetails"), objADClientViewGroup);
        }

        //Save Updated or New Details
        [HttpPost]
        public ActionResult ClientViewGroupSave(ADClientViewGroup updatedClientViewGroup)
        {
            objADClientViewGroup.SaveClientViewGroup(updatedClientViewGroup);
            List<ADClientViewGroup> ClientViewGroupList = objADClientViewGroup.ListClientViewGroup();
            Session["ListClientViewGroup"] = ClientViewGroupList;
            return PartialView(GetVirtualPath("_ClientViewGroupList"), ClientViewGroupList);
        }

        //Show blank details on Add
        public ActionResult ClientViewGroupAdd()
        {
            ModelState.Clear();
            objADClientViewGroup.Id = 0;
            ViewBag.ddlAvailableList = CommonBindList.GetClientList();
            ViewBag.ddlSelectedList = new List<CommonBindList>();
            return View(GetVirtualPath("ClientViewGroupDetails"), objADClientViewGroup);
        }

        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {
            List<ADClientViewGroup> ClientViewGroupList = (List<ADClientViewGroup>)Session["ListClientViewGroup"];
            List<ADClientViewGroup> objClientViewGroupFilter = new List<ADClientViewGroup>();

            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    objClientViewGroupFilter = (from item in ClientViewGroupList
                                                where item.ClientViewGroupName.ToLower().Contains(value.ToLower())
                                                select item).ToList();
                }
                else if (Key == "2")
                {
                    objClientViewGroupFilter = (from item in ClientViewGroupList
                                                where item.ClientCodeList.ToLower().Contains(value.ToLower())
                                                select item).ToList();
                }



            }
            return View(GetVirtualPath("_ClientViewGroupList"), objClientViewGroupFilter);
        }

        //refresh  Grid
        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADClientViewGroup> ClientViewGroupList = objADClientViewGroup.ListClientViewGroup();
            Session["ListClientViewGroup"] = ClientViewGroupList;
            return PartialView(GetVirtualPath("_ClientViewGroupList"), ClientViewGroupList);
        }


        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "ClientViewGroupName" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "ClientCodeList" });
            //filterlist.Add(new CommonBindList { Id = 3, Name = "Email" });
            //filterlist.Add(new CommonBindList { Id = 4, Name = "FullName" });
            //filterlist.Add(new CommonBindList { Id = 5, Name = "IsInactive" });
            //filterlist.Add(new CommonBindList { Id = 6, Name = "LastLoginDate" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }


        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewClients/ClientConfigurations/ClientViewGroups/" + ViewName + ".cshtml";
            return path;
        }


    }
}
