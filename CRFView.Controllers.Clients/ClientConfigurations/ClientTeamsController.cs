﻿using CRFView.Adapters;
using CRFView.Adapters.Clients.Client_Management;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRFView.Controllers.Clients.ClientConfigurations
{
    public class ClientTeamsController : Controller
    {
        ADTeam objADTeam = new ADTeam();
        // GET: ADTeam
        public ActionResult Index()
        {
            List<ADTeam> lstTeam = objADTeam.ListTeam();
            Session["ListTeam"] = lstTeam;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"), lstTeam);
        }
        //Show details on Edit
        public ActionResult TeamDetails(int id)
        {
            ADTeam objTeam = objADTeam.ReadTeam(id);
            List<CommonBindList> ManagerList = CommonBindList.GetInternalUsersList();
            ViewBag.ManagerList = ManagerList;
            List<CommonBindList> AllInternalUsersList = CommonBindList.GetInternalUsersList();
            ViewBag.AllInternalUsersList = AllInternalUsersList;
            ViewBag.SelectedUsersList = CommonBindList.GetSelectedUsersList(objTeam.UserList, "Internal");
            return View(GetVirtualPath("TeamDetails"), objTeam);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult TeamSave(ADTeam updatedTeam)
        {
            objADTeam.SaveTeam(updatedTeam);
            List<ADTeam> lstTeam = objADTeam.ListTeam();
            Session["ListTeam"] = lstTeam;
            return PartialView(GetVirtualPath("_TeamList"), lstTeam);
        }
        //Show blank details on Add
        public ActionResult TeamAdd()
        {
            ADTeam objTeam = new ADTeam();
            ModelState.Clear();
            objTeam.Id = 0;
            List<CommonBindList> ManagerList = CommonBindList.GetInternalUsersList();
            ViewBag.ManagerList = ManagerList;
            List<CommonBindList> AllInternalUsersList = CommonBindList.GetInternalUsersList();
            ViewBag.AllInternalUsersList = AllInternalUsersList;
            ViewBag.SelectedUsersList = CommonBindList.GetSelectedUsersList("", "Internal");
            return View(GetVirtualPath("TeamDetails"), objTeam);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {
            List<ADTeam> ListTeamFilter = new List<ADTeam>();
            List<ADTeam> ListTeam;
            ListTeam = (List<ADTeam>)Session["ListTeam"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    ListTeamFilter = (from item in ListTeam
                                      where item.TeamCode.ToLower().Contains(value.ToLower())
                                      select item).ToList();
                }
                else if (Key == "2")
                {
                    ListTeamFilter = (from item in ListTeam
                                      where item.TeamName.ToLower().Contains(value.ToLower())
                                      select item).ToList();
                }

            }
            return View(GetVirtualPath("_TeamList"), ListTeamFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Team Code" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Team Name" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADTeam> lstTeam = objADTeam.ListTeam();
            Session["ListTeam"] = lstTeam;
            return PartialView(GetVirtualPath("_TeamList"), lstTeam);
        }

        [HttpPost]
        public ActionResult DeleteTeam (string Id)
        {
            bool result = ADTeam.DeletetTeam(Id);
            return Json(result);
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewClients/ClientConfigurations/Team/" + ViewName + ".cshtml";
            return path;
        }
    }
}
