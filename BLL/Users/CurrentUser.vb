Namespace Users
    Public Class CurrentUser
        Public Id As Integer = -1
        Public UserName As String = "Guest"
        Public ClientCode As String
        Public Email As String = ""
        Public SiteDataFolder As String
        Public SettingsFolder As String
        Public ErrorLogFolder As String
        Public OutputFolder As String
        Public UploadFolder As String
        Public LastUploadPath As String
        Public LastReportPlath As String
        Public SMTPHOST As String
        Public SMTPUSER As String
        Public SMTPPASSWORD As String
        Public SMTPFROM As String
        Public FAXPRINTER As String
        Public REPORTID As String
        Public LastLedgerType As String
    End Class
End Namespace
