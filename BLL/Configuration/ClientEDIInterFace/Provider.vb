Namespace Providers
    Public Class ClientEDIInterface
        Private Shared _isInitialized As Boolean = False
        Private Shared _provider As IClientEDIInterfaceProvider
        ' <summary>
        ' Returns provider
        ' </summary>
        Public Shared ReadOnly Property Provider() As IClientEDIInterfaceProvider
            Get
                Initialize()
                Return _provider
            End Get
        End Property
        Private Shared Sub Initialize()
            If Not _isInitialized Then
                _provider = SingletonProvider(Of Providers.SQLClientEDIInterfaceProvider).Instance
                _isInitialized = True
            End If
        End Sub
        Public Shared Function GetClientEDIInterfaceList() As DAL.COMMON.TableView
            Return Provider.GetClientEDIInterfaceList()
        End Function

    End Class
End Namespace
