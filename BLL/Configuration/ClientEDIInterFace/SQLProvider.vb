Imports System.Configuration.Provider
Namespace Configuration.ClientEDIInterFace
    Public Class SQLClientEDIInterfaceProvider
        Inherits ProviderBase
        Implements IClientEDIInterfaceProvider

        Public Function GetClientEDIInterfaceList() As HDS.DAL.COMMON.TableView Implements IClientEDIInterfaceProvider.GetClientEDIInterfaceList
            Dim myquery As String = "Select client.clientcode, * from ClientInterface join client on ClientInterface.clientid = client.clientid"
            Return Providers.DBO.Provider.GetTableView(myquery)
        End Function

    End Class
End Namespace
