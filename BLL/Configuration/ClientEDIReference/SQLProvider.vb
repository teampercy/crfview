Imports System.Configuration.Provider
Namespace Configuration.ClientEDIReference
    Public Class SQLClientEDIReferenceProvider
        Inherits ProviderBase
        Implements IClientEDIReferenceProvider

        Public Function GetClientEDIReferenceList() As HDS.DAL.COMMON.TableView Implements IClientEDIReferenceProvider.GetClientEDIReferenceList
            Dim myquery As String = "select Client.ClientCode,Client.ClientName,ClientInterface.InterfaceName,* from ClientInterfaceReference, Client ,ClientInterface where ClientInterfaceReference.ClientId = Client.ClientId And ClientInterfaceReference.ClientInterfaceId = ClientInterface.Id"
            Return Providers.DBO.Provider.GetTableView(myquery)
        End Function

    End Class
End Namespace
