Namespace Configuration.ClientReportLog
    Public Interface IClientReportLogProvider
        Function GetClientReportLogList(ByVal FilterValue As String, ByVal FilterDate As String) As DAL.COMMON.TableView
    End Interface
End Namespace
