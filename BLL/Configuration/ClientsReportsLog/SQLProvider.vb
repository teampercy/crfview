Imports System.Configuration.Provider
Namespace Configuration.ClientReportLog
    Public Class SQLClientReportLogProvider
        Inherits ProviderBase
        Implements IClientReportLogProvider

        Public Function GetClientReportLogList(ByVal FilterValue As String, ByVal FilterDate As String) As HDS.DAL.COMMON.TableView Implements IClientReportLogProvider.GetClientReportLogList
            Dim myquery As String = "select  * from Report_History "

            If FilterValue = "Errors" Then
                myquery += " Where status = -1"
            End If
            If FilterValue = "InProgress" Then
                myquery += " Where status = 1"
            End If
            If FilterValue = "Pending" Then
                myquery += " Where status = 2"
            End If
            If FilterValue = "ToBeEmailed" Then
                myquery += " Where status = 3"
            End If
            If FilterValue = "RunDate" Then
                Dim paramdate As DateTime = DateTime.Parse(FilterDate)
                myquery += " Where StartDate >='" + paramdate + "' "
                myquery += " and StartDate <'" + paramdate.AddDays(1) + "' "
            End If
            Return Providers.DBO.Provider.GetTableView(myquery)
        End Function

    End Class
End Namespace
