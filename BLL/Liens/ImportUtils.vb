﻿Namespace Liens.DataEntry.ImportEDI
    Public Class ImportUtils

        Public Shared Function ParseTestImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")

            Dim myClient As New CRFDB.TABLES.Client

            Dim i As Integer
            For i = 7 To 68
                With myClient
                    If sfields(i) <> "" Then
                        .ParentClientId = 16501
                        .ClientCode = "SBR111"
                        .BranchNumber = "PC" & sfields(i)
                        .ClientName = "PC" & sfields(i)
                        .ContactName = sfields(0)
                        .AddressLine1 = sfields(1)
                        .City = sfields(2)
                        .State = sfields(3)
                        .PostalCode = sfields(4)
                        .PhoneNo = ImportUtils.GetPhoneNo(sfields(5))
                        .Email = sfields(6)
                        .IsBranch = True
                        BLL.Providers.DBO.Provider.Create(myClient)
                    Else
                        Exit For
                    End If
                End With
            Next i
            MsgBox("Done")
        End Function
        Public Shared Function ParseStandardImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 65 Or Len(sfields(13)) < 1 Then
                Return False
            End If
            If Len(sfields(13)) < 2 Then
                Return False
            End If

            With abatchjob
                .JobNum = sfields(0)
                .JobName = sfields(1)
                .JobAdd1 = sfields(2)
                .JobAdd2 = sfields(3)
                If .JobAdd1 = Nothing And .JobAdd2 <> Nothing Then
                    .JobAdd1 = .JobAdd2
                    .JobAdd2 = ""
                End If
                .JobCity = sfields(4)
                .JobState = sfields(5)
                .JobZip = sfields(6)
                .EstBalance = sfields(7)
                .StartDate = ImportUtils.GetDate(sfields(8))
                .PONum = sfields(9)

                .CustJobNum = sfields(10)

                .CustRefNum = sfields(12)
                .CustName = sfields(13)
                .CustContactName = sfields(14)
                .CustAdd1 = sfields(15)
                .CustAdd2 = sfields(16)
                If .CustAdd1 = Nothing And .CustAdd2 <> Nothing Then
                    .CustAdd1 = .CustAdd2
                    .CustAdd2 = ""
                End If
                .CustCity = sfields(17)
                .CustState = sfields(18)
                .CustZip = sfields(19)
                .CustPhone1 = ImportUtils.GetPhoneNo(sfields(20))
                .CustPhone2 = ImportUtils.GetPhoneNo(sfields(21))
                .CustId = -1

                .GCRefNum = sfields(22)
                .GCName = sfields(23)
                If .GCName = "SAME AS CUSTOMER" Then
                    .GCName = .CustName
                    .GCContactName = .CustContactName
                    .GCAdd1 = .CustAdd1
                    .GCAdd2 = .CustAdd2
                    .GCCity = .CustCity
                    .GCState = .CustState
                    .GCZip = .CustZip
                    .GCPhone1 = .CustPhone1
                    .GCPhone2 = .CustPhone2
                    .GenId = -1
                Else
                    .GCContactName = sfields(24)
                    .GCAdd1 = sfields(25)
                    .GCAdd2 = sfields(26)
                    .GCCity = sfields(27)
                    .GCState = sfields(28)
                    .GCZip = sfields(29)
                    .GCPhone1 = ImportUtils.GetPhoneNo(sfields(30))
                    .GCPhone2 = ImportUtils.GetPhoneNo(sfields(31))
                    .GenId = -1
                End If

                .OwnrName = sfields(32)
                .OwnrAdd1 = sfields(33)
                .OwnrAdd2 = sfields(34)
                .OwnrCity = sfields(35)
                .OwnrState = sfields(36)
                .OwnrZip = sfields(37)
                .OwnrPhone1 = ImportUtils.GetPhoneNo(sfields(38))

                .BondNum = sfields(39)
                .LenderName = sfields(47)
                .LenderAdd1 = sfields(48)
                .LenderAdd2 = sfields(49)
                .LenderCity = sfields(50)
                .LenderState = sfields(51)
                .LenderZip = sfields(52)
                .LenderPhone1 = ImportUtils.GetPhoneNo(sfields(53))

                If sfields(40) <> "" And sfields(40) <> Nothing Then
                    .LenderName = sfields(40)
                    .LenderAdd1 = sfields(41)
                    .LenderAdd2 = sfields(42)
                    .LenderCity = sfields(43)
                    .LenderState = sfields(44)
                    .LenderZip = sfields(45)
                    .LenderPhone1 = ImportUtils.GetPhoneNo(sfields(46))
                End If

                .SpecialInstruction = sfields(54)

                .PublicJob = False
                .FederalJob = False
                .ResidentialBox = False
                .PrivateJob = False

                .PrelimBox = False
                .VerifyJob = False
                .MechanicLien = False
                .TitleVerifiedBox = False
                .PrelimASIS = False

                If sfields(55) = "T" Then
                    .PublicJob = True
                End If
                If sfields(56) = "T" Then
                    .FederalJob = True
                End If
                If sfields(57) = "T" Then
                    .ResidentialBox = True
                End If
                If sfields(58) = "T" Then
                    .JobActionId = 37
                    .VerifyJob = True
                End If
                If sfields(59) = "T" Then
                    .JobActionId = 39
                    .MechanicLien = True
                End If
                If sfields(60) = "T" Then
                    .JobActionId = 38
                    .TitleVerifiedBox = True
                End If
                If sfields(64) = "T" Then
                    .JobActionId = 44
                    .PrelimASIS = True
                End If

                If .VerifyJob = False And .MechanicLien = False And .TitleVerifiedBox = False And .PrelimASIS = False Then
                    .JobActionId = 36
                    .PrelimBox = True
                End If

                .BranchNum = sfields(61)
                .RANum = sfields(62)
                .SubmittedByUserId = auser.Id
                .BatchId = abatchid
                .SubmittedOn = Now()
                .ClientId = aclientid

                .PlacementSource = "EDI"

            End With
            Return True

        End Function

        Public Shared Function ParseHUB909ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 25 Then
                Return False
            End If

            With abatchjob
                Dim sSql As String = "Select ClientId From Client c with (nolock) Where ClientCode = '" & sfields(0) & "'"
                Dim myview As DAL.COMMON.TableView = DBO.GetTableView(sSql)
                If myview.Count > 0 Then
                    .ClientId = myview.RowItem("ClientId")
                Else
                    .ClientId = aclientid
                End If

                .JobNum = sfields(1)
                .JobName = sfields(2)
                .JobAdd1 = sfields(3)
                .JobAdd2 = sfields(4)
                If .JobAdd1 = Nothing And .JobAdd2 <> Nothing Then
                    .JobAdd1 = .JobAdd2
                    .JobAdd2 = ""
                End If
                .JobCity = sfields(5)
                .JobState = sfields(6)
                .JobZip = sfields(7)
                .EstBalance = sfields(8)
                .StartDate = ImportUtils.GetDate(sfields(9))

                .CustJobNum = sfields(10)

                .CustRefNum = sfields(11)
                .CustName = sfields(12)
                .CustContactName = sfields(13)
                .CustAdd1 = sfields(14)
                .CustAdd2 = sfields(15)
                If .CustAdd1 = Nothing And .CustAdd2 <> Nothing Then
                    .CustAdd1 = .CustAdd2
                    .CustAdd2 = ""
                End If
                .CustCity = sfields(16)
                .CustState = sfields(17)
                .CustZip = sfields(18)
                .CustPhone1 = ImportUtils.GetPhoneNo(sfields(19))
                .CustFax = ImportUtils.GetPhoneNo(sfields(20))
                .CustEmail = sfields(24)
                .CustId = -1

                .PrelimBox = False

                If sfields(21) = "T" Then
                    .PublicJob = True
                End If
                If sfields(22) = "T" Then
                    .FederalJob = True
                End If
                If sfields(23) = "T" Then
                    .ResidentialBox = True
                End If

                .JobActionId = 36

                .SubmittedByUserId = auser.Id
                .BatchId = abatchid
                .SubmittedOn = Now()
                .ClientId = aclientid

                .PlacementSource = "EDI"

            End With
            Return True

        End Function

        Public Shared Function ParseSRF714ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 63 Or Len(sfields(13)) < 1 Then
                Return False
            End If
            If Len(sfields(13)) < 2 Then
                Return False
            End If

            With abatchjob
                .JobNum = sfields(0)
                .JobName = sfields(1)
                .JobAdd1 = sfields(2)
                .JobAdd2 = sfields(3)
                If .JobAdd1 = Nothing And .JobAdd2 <> Nothing Then
                    .JobAdd1 = .JobAdd2
                    .JobAdd2 = ""
                End If
                .JobCity = sfields(4)
                .JobState = sfields(5)
                .JobZip = sfields(6)
                .EstBalance = sfields(7)
                .StartDate = ImportUtils.GetDate(sfields(8))
                .PONum = sfields(9)

                .CustJobNum = sfields(10)

                .CustRefNum = sfields(12)
                .CustName = sfields(13)
                .CustContactName = sfields(14)
                .CustAdd1 = sfields(15)
                .CustAdd2 = sfields(16)
                If .CustAdd1 = Nothing And .CustAdd2 <> Nothing Then
                    .CustAdd1 = .CustAdd2
                    .CustAdd2 = ""
                End If
                .CustCity = sfields(17)
                .CustState = sfields(18)
                .CustZip = sfields(19)
                If Len(sfields(20)) > 10 Then
                    .CustPhone1 = ImportUtils.GetPhoneNo(Left(sfields(20), 10))
                Else
                    .CustPhone1 = ImportUtils.GetPhoneNo(sfields(20))
                End If
                If Len(sfields(20)) > 10 Then
                    .CustPhone2 = ImportUtils.GetPhoneNo(Left(sfields(20), 10))
                Else
                    .CustPhone2 = ImportUtils.GetPhoneNo(sfields(20))
                End If

                .CustId = -1

                .GCRefNum = sfields(22)
                .GCName = sfields(23)
                If .GCName = "SAME AS CUSTOMER" Then
                    .GCName = .CustName
                    .GCContactName = .CustContactName
                    .GCAdd1 = .CustAdd1
                    .GCAdd2 = .CustAdd2
                    .GCCity = .CustCity
                    .GCState = .CustState
                    .GCZip = .CustZip
                    If Len(sfields(20)) > 10 Then
                        .GCPhone1 = ImportUtils.GetPhoneNo(Left(sfields(20), 10))
                    Else
                        .GCPhone1 = ImportUtils.GetPhoneNo(sfields(20))
                    End If
                    If Len(sfields(20)) > 10 Then
                        .GCPhone2 = ImportUtils.GetPhoneNo(Left(sfields(20), 10))
                    Else
                        .GCPhone2 = ImportUtils.GetPhoneNo(sfields(20))
                    End If
                    .GenId = -1
                Else
                    .GCContactName = sfields(24)
                    .GCAdd1 = sfields(25)
                    .GCAdd2 = sfields(26)
                    .GCCity = sfields(27)
                    .GCState = sfields(28)
                    .GCZip = sfields(29)
                    .GCPhone1 = ImportUtils.GetPhoneNo(sfields(30))
                    .GCPhone2 = ImportUtils.GetPhoneNo(sfields(31))
                    .GenId = -1
                End If

                .OwnrName = sfields(32)
                .OwnrAdd1 = sfields(33)
                .OwnrAdd2 = sfields(34)
                .OwnrCity = sfields(35)
                .OwnrState = sfields(36)
                .OwnrZip = sfields(37)
                .OwnrPhone1 = ImportUtils.GetPhoneNo(sfields(38))

                .BondNum = sfields(39)
                .LenderName = sfields(47)
                .LenderAdd1 = sfields(48)
                .LenderAdd2 = sfields(49)
                .LenderCity = sfields(50)
                .LenderState = sfields(51)
                .LenderZip = sfields(52)
                .LenderPhone1 = ImportUtils.GetPhoneNo(sfields(53))

                If sfields(40) <> "" And sfields(40) <> Nothing Then
                    .LenderName = sfields(40)
                    .LenderAdd1 = sfields(41)
                    .LenderAdd2 = sfields(42)
                    .LenderCity = sfields(43)
                    .LenderState = sfields(44)
                    .LenderZip = sfields(45)
                    .LenderPhone1 = ImportUtils.GetPhoneNo(sfields(46))
                End If

                .SpecialInstruction = sfields(54)

                .PublicJob = False
                .FederalJob = False
                .ResidentialBox = False
                .PrivateJob = False

                .PrelimBox = False
                .VerifyJob = False
                .MechanicLien = False
                .TitleVerifiedBox = False
                .PrelimASIS = False

                If sfields(55) = "T" Then
                    .PublicJob = True
                End If
                If sfields(56) = "T" Then
                    .FederalJob = True
                End If
                If sfields(57) = "T" Then
                    .ResidentialBox = True
                End If
                If sfields(58) = "T" Then
                    .JobActionId = 37
                    .VerifyJob = True
                End If
                If sfields(59) = "T" Then
                    .JobActionId = 39
                    .MechanicLien = True
                End If
                If sfields(60) = "T" Then
                    .JobActionId = 38
                    .TitleVerifiedBox = True
                End If
                'If sfields(64) = "T" Then
                '    .JobActionId = 44
                '    .PrelimASIS = True
                'End If

                If .VerifyJob = False And .MechanicLien = False And .TitleVerifiedBox = False Then
                    .JobActionId = 36
                    .PrelimBox = True
                End If

                .BranchNum = sfields(61)
                .RANum = sfields(62)
                .SubmittedByUserId = auser.Id
                .BatchId = abatchid
                .SubmittedOn = Now()
                .ClientId = aclientid

                .PlacementSource = "EDI"

            End With
            Return True

        End Function

        Public Shared Function ParseHertzImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 55 Then
                Return False
            End If
            If Len(sfields(12)) < 2 Then
                Return False
            End If

            With abatchjob
                .JobNum = sfields(2)
                .JobName = sfields(3)
                .JobAdd1 = sfields(4)
                .JobAdd2 = sfields(5)
                If .JobAdd1 = Nothing And .JobAdd2 <> Nothing Then
                    .JobAdd1 = .JobAdd2
                    .JobAdd2 = ""
                End If
                .JobCity = sfields(6)
                .JobState = sfields(7)
                .JobZip = sfields(8)

                .CustPhone1 = ImportUtils.GetPhoneNo(sfields(9) & sfields(10))
                .CustRefNum = sfields(11)
                .CustName = sfields(12)
                .CustAdd1 = sfields(13)
                .CustAdd2 = sfields(14)
                If .CustAdd1 = Nothing And .CustAdd2 <> Nothing Then
                    .CustAdd1 = .CustAdd2
                    .CustAdd2 = ""
                End If
                .CustCity = sfields(15)
                .CustState = sfields(16)
                .CustZip = sfields(17)
                .CustId = -1

                ' 4/2/3013
                If .JobState = "AZ" Then
                    .EstBalance = sfields(18)
                    .EstBalance = .EstBalance * 2
                Else
                    .EstBalance = sfields(18)
                End If

                If sfields(22) <> "" And sfields(22) <> Nothing Then
                    .LenderName = sfields(22)
                    .LenderAdd1 = sfields(23)
                    .LenderAdd2 = sfields(24)
                    .LenderCity = sfields(25)
                    .LenderState = sfields(26)
                    .LenderZip = sfields(27)
                    .LenderPhone1 = ImportUtils.GetPhoneNo(sfields(20) & sfields(21))
                End If

                If sfields(38) <> "" And sfields(38) <> Nothing Then
                    .GCName = sfields(38)
                    If .GCName = "SAME AS CUSTOMER" Then
                        .GCName = .CustName
                        .GCContactName = .CustContactName
                        .GCAdd1 = .CustAdd1
                        .GCAdd2 = .CustAdd2
                        .GCCity = .CustCity
                        .GCState = .CustState
                        .GCZip = .CustZip
                        .GCPhone1 = .CustPhone1
                        .GCPhone2 = .CustPhone2
                        .GenId = -1
                    Else
                        .GCAdd1 = sfields(39)
                        .GCAdd2 = sfields(40)
                        .GCCity = sfields(41)
                        .GCState = sfields(42)
                        .GCZip = sfields(43)
                        .GCPhone1 = ImportUtils.GetPhoneNo(sfields(36) & sfields(37))
                        .GenId = -1
                    End If
                End If

                If sfields(46) <> "" And sfields(46) <> Nothing Then
                    .OwnrName = sfields(46)
                    .OwnrAdd1 = sfields(47)
                    .OwnrAdd2 = sfields(48)
                    .OwnrCity = sfields(49)
                    .OwnrState = sfields(50)
                    .OwnrZip = sfields(51)
                    .OwnrPhone1 = ImportUtils.GetPhoneNo(sfields(44) & sfields(45))
                End If

                .BranchNum = sfields(52)
                .PONum = sfields(55)
                .StartDate = ImportUtils.GetDate(sfields(54))
                .RANum = sfields(53)

                .PrivateJob = True
                .PrelimBox = True
                .SubmittedByUserId = auser.Id
                .BatchId = abatchid
                .SubmittedOn = Now()
                .ClientId = aclientid

                .PlacementSource = "EDI"

            End With
            Return True

        End Function

        Public Shared Function ParseHZN480ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            'If sfields.Length < 33 Then
            '    Return False
            'End If

            If Trim(sfields(0)) <> "" Then

                With abatchjob
                    .JobNum = sfields(12)
                    .JobName = sfields(13)
                    .JobAdd1 = sfields(14)
                    .JobCity = sfields(15)
                    .JobState = sfields(16)
                    .JobZip = sfields(17)

                    .CustPhone1 = ImportUtils.GetPhoneNo(sfields(11))
                    .CustRefNum = sfields(5)
                    .CustName = sfields(6)
                    .CustAdd1 = sfields(7)
                    .CustCity = sfields(8)
                    .CustState = sfields(9)
                    .CustZip = sfields(10)
                    .CustId = -1

                    If sfields(4) <> "" Then
                        .EstBalance = sfields(4)
                    End If

                    If sfields(19) <> "" And sfields(19) <> Nothing Then
                        .GCName = sfields(19)
                        .GCAdd1 = sfields(20)
                        .GCCity = sfields(21)
                        .GCState = sfields(22)
                        .GCZip = sfields(23)
                        .GCPhone1 = ImportUtils.GetPhoneNo(sfields(24))
                        .GenId = -1
                    End If

                    If sfields(25) <> "" And sfields(25) <> Nothing Then
                        .OwnrName = sfields(25)
                        .OwnrAdd1 = sfields(26)
                        .OwnrCity = sfields(27)
                        .OwnrState = sfields(28)
                        .OwnrZip = sfields(29)
                    End If

                    .BranchNum = sfields(0)
                    If sfields(1) = "TRUE" Then
                        .RushOrder = 1
                    End If
                    If sfields(2) = "TRUE" Then
                        .JointCk = 1
                    End If

                    .StartDate = ImportUtils.GetDate(sfields(3))

                    .PrivateJob = True
                    .PrelimBox = True
                    .SubmittedByUserId = auser.Id
                    .BatchId = abatchid
                    .SubmittedOn = Now()
                    .ClientId = aclientid

                    .PlacementSource = "EDI"

                End With

            End If
            Return True
        End Function

        Public Shared Function ParseURL281ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 16 Then
                Return False
            End If
            If Len(sfields(2)) < 2 Then
                Return False
            End If


            If sfields(0) <> "FALSE" Then
                Try
                    With abatchjob
                        .EstBalance = sfields(1)
                        .CustName = sfields(2)
                        .JobAdd1 = sfields(3)
                        '.RefNum = sfields(4)
                        .CustAdd1 = sfields(5)
                        .CustCity = sfields(6)
                        .CustState = sfields(7)
                        .CustZip = sfields(8)
                        .CustPhone1 = ImportUtils.GetPhoneNo(sfields(9))
                        .CustId = -1

                        .StartDate = ImportUtils.GetDate(sfields(10))

                        .JobName = sfields(11)
                        .JobCity = sfields(12)
                        .JobState = sfields(13)
                        .JobZip = sfields(14)
                        .JobNum = sfields(15)
                        .BranchNum = sfields(16)

                        .PrivateJob = True
                        .PrelimBox = True
                        .SubmittedByUserId = auser.Id
                        .BatchId = abatchid
                        .SubmittedOn = Now()
                        .ClientId = aclientid

                        .PlacementSource = "EDI"

                    End With
                Catch ex As Exception
                    Dim s As String = ex.Message
                End Try

            End If
            Return True

        End Function

        Public Shared Function ParseURL425ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 22 Then
                Return False
            End If

            If sfields(0) = "YES" Then

                With abatchjob
                    If sfields(1) = Nothing Then
                        .EstBalance = 0
                    Else
                        .EstBalance = sfields(1)
                    End If
                    .CustName = sfields(2)
                    '.RefNum = sfields(3)

                    .CustAdd1 = sfields(4)
                    .CustCity = sfields(5)
                    .CustState = sfields(6)
                    .CustZip = sfields(7)
                    .CustPhone1 = ImportUtils.GetPhoneNo(sfields(8))
                    .CustId = -1

                    .StartDate = ImportUtils.GetDate(sfields(9))
                    .JobName = sfields(10)
                    .JobAdd1 = sfields(11)
                    .JobCity = sfields(12)
                    .JobState = sfields(13)
                    .JobZip = sfields(14)
                    .JobNum = sfields(15)
                    .BranchNum = sfields(16)

                    If sfields(17) <> "" And sfields(17) <> Nothing Then
                        .GCName = sfields(17)
                        If .GCName = "SAME AS CUSTOMER" Then
                            .GCName = .CustName
                            .GCAdd1 = .CustAdd1
                            .GCCity = .CustCity
                            .GCState = .CustState
                            .GCZip = .CustZip
                            .GCPhone1 = .CustPhone1
                            .GenId = -1
                        Else
                            .GCAdd1 = sfields(18)
                            .GCCity = sfields(19)
                            .GCState = sfields(20)
                            .GCZip = sfields(21)
                            .GenId = -1
                        End If
                    End If

                    .PrivateJob = True
                    .PrelimBox = True
                    .SubmittedByUserId = auser.Id
                    .BatchId = abatchid
                    .SubmittedOn = Now()
                    .ClientId = aclientid

                    .PlacementSource = "EDI"

                End With
            End If
            Return True

        End Function


        Public Shared Function ParseHAN858ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 10 Then
                Return False
            End If
            'If Len(sfields(0)) < 2 Then
            '    Return False
            'End If

            If Trim(sfields(0)) <> "" Then

                With abatchjob
                    .CustName = sfields(0)
                    .CustRefNum = sfields(1)

                    Dim mysproc As New SPROCS.uspbo_LiensDataEntry_GetCustomer_SearchCustByRef
                    mysproc.ClientId = aclientid
                    mysproc.RefNum = .CustRefNum
                    mysproc.CustName = .CustName
                    Dim myview As DAL.COMMON.TableView = DBO.GetTableView(mysproc)
                    If myview.Count <> 0 Then
                        .CustId = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.CustId)
                        .CustAdd1 = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.AddressLine1)
                        .CustAdd2 = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.AddressLine2)
                        .CustCity = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.City)
                        .CustState = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.State)
                        .CustZip = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.PostalCode)
                        .CustPhone1 = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.Telephone1)
                        .CustFax = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.Fax)
                    Else
                        .CustId = -1
                    End If

                    .JobNum = sfields(2)
                    .JobName = sfields(3)
                    .JobAdd1 = sfields(4)
                    .JobCity = sfields(5)
                    .JobState = sfields(6)
                    If .JobState = "" And .JobState = Nothing Then
                        .JobState = "CA"
                    End If
                    .JobZip = sfields(7)

                    .StartDate = ImportUtils.GetDate(sfields(8))
                    .EstBalance = sfields(9)
                    .GenId = -1

                    .PrivateJob = True
                    .PrelimBox = True
                    .SubmittedByUserId = auser.Id
                    .BatchId = abatchid
                    .SubmittedOn = Now()
                    .ClientId = aclientid
                    'If DBNull.Value.Equals(sfields(10)) Then
                    'Else
                    '    .BranchNum = sfields(10)
                    'End If
                    .PlacementSource = "EDI"

                End With
            End If
            Return True

        End Function

        Public Shared Function ParseHAN502ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            Dim myCustomer As TABLES.ClientCustomer

            If sfields.Length < 10 Then
                Return False
            End If
            'If Len(sfields(0)) < 2 Then
            '    Return False
            'End If
            If Trim(sfields(0)) <> "" Then

                With abatchjob
                    .CustName = sfields(0)
                    .CustRefNum = sfields(1)

                    Dim mysproc As New SPROCS.uspbo_LiensDataEntry_GetCustomer_SearchCustByRef
                    mysproc.ClientId = aclientid
                    mysproc.RefNum = .CustRefNum
                    mysproc.CustName = .CustName
                    Dim myview As DAL.COMMON.TableView = DBO.GetTableView(mysproc)
                    If myview.Count > 0 Then
                        .CustId = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.CustId)
                        .CustAdd1 = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.AddressLine1)
                        .CustAdd2 = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.AddressLine2)
                        .CustCity = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.City)
                        .CustState = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.State)
                        .CustZip = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.PostalCode)
                        .CustPhone1 = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.Telephone1)
                        .CustFax = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.Fax)
                    Else
                        .CustId = -1
                        .CustAdd1 = sfields(13)
                        .CustAdd2 = sfields(14)
                        .CustCity = sfields(15)
                        .CustState = sfields(16)
                        .CustZip = sfields(17)
                        .CustPhone1 = sfields(18)
                        .CustFax = sfields(19)

                        myCustomer = New TABLES.ClientCustomer
                        myCustomer.ClientId = aclientid
                        myCustomer.RefNum = .CustRefNum
                        myCustomer.ClientCustomer = .CustName
                        myCustomer.AddressLine1 = .CustAdd1
                        myCustomer.AddressLine2 = .CustAdd2
                        myCustomer.City = .CustCity
                        myCustomer.State = .CustState
                        myCustomer.PostalCode = .CustZip
                        myCustomer.Telephone1 = .CustPhone1
                        myCustomer.Fax = .CustFax
                        BLL.Providers.DBO.Provider.Create(myCustomer)
                        .CustId = myCustomer.CustId
                    End If

                    .JobNum = sfields(2)
                    .JobName = sfields(3)
                    .JobAdd1 = sfields(4)
                    If (sfields(5) <> "" And sfields(5) <> Nothing) Or (sfields(6) <> "" And sfields(6) <> Nothing) Then
                        .SpecialInstruction = Trim(sfields(5)) & " - " & Trim(sfields(6))
                    End If
                    .JobCity = sfields(7)
                    .JobState = sfields(8)
                    .JobZip = sfields(9)

                    .StartDate = ImportUtils.GetDate(sfields(10))
                    .EstBalance = sfields(11)
                    .GenId = -1

                    .PrivateJob = True
                    .PrelimBox = True
                    .SubmittedByUserId = auser.Id
                    .BatchId = abatchid
                    .SubmittedOn = Now()
                    .ClientId = aclientid

                    .PlacementSource = "EDI"

                End With
            End If
            Return True

        End Function

        Public Shared Function ParseHAN602ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 10 Then
                Return False
            End If
            'If Len(sfields(0)) < 2 Then
            '    Return False
            'End If
            If Trim(sfields(0)) <> "" Then

                With abatchjob
                    .CustName = sfields(0)
                    .CustRefNum = sfields(1)

                    Dim mysproc As New SPROCS.uspbo_LiensDataEntry_GetCustomer_SearchCustByRef
                    mysproc.ClientId = aclientid
                    mysproc.RefNum = .CustRefNum
                    mysproc.CustName = .CustName
                    Dim myview As DAL.COMMON.TableView = DBO.GetTableView(mysproc)
                    If myview.Count > 0 Then
                        .CustId = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.CustId)
                        .CustAdd1 = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.AddressLine1)
                        .CustAdd2 = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.AddressLine2)
                        .CustCity = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.City)
                        .CustState = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.State)
                        .CustZip = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.PostalCode)
                        .CustPhone1 = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.Telephone1)
                        .CustFax = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.Fax)
                    Else
                        .CustId = -1
                    End If

                    .JobNum = sfields(2)
                    .JobName = sfields(3)
                    .JobAdd1 = sfields(4)
                    .JobCity = sfields(5)
                    .JobState = sfields(6)
                    If .JobState = "" And .JobState = Nothing Then
                        .JobState = "CA"
                    End If
                    .JobZip = sfields(7)

                    .StartDate = ImportUtils.GetDate(sfields(8))
                    .EstBalance = sfields(9)
                    .GenId = -1

                    .PrivateJob = True
                    .PrelimBox = True
                    .SubmittedByUserId = auser.Id
                    .BatchId = abatchid
                    .SubmittedOn = Now()
                    .ClientId = aclientid

                    .PlacementSource = "EDI"

                End With
            End If
            Return True

        End Function

        Public Shared Function ParseMarcoImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            Dim myCustomer As TABLES.ClientCustomer

            If sfields.Length < 22 Then
                Return False
            End If
            If Len(sfields(0)) < 2 Then
                Return False
            End If
            If Trim(sfields(0)) <> "" Then

                With abatchjob
                    Select Case sfields(0)
                        Case "MCR"
                            .ClientId = 16131   'MCR602
                        Case "ME"
                            .ClientId = 16136   'MEC602
                        Case "MEC"
                            .ClientId = 16136   'MEC602
                        Case "MRC"
                            .ClientId = 16140   'MRC602
                        Case "MCP"
                            .ClientId = 16871   'MCP602
                        Case "RIG"
                            .ClientId = 16140   'MRC602
                        Case Else
                            .ClientId = 0
                    End Select

                    ' Added 11/12/13
                    If .ClientId = 0 Then
                        Return False
                    End If

                    .CustRefNum = sfields(4)
                    .CustName = sfields(5)

                    Dim mysproc As New SPROCS.uspbo_LiensDataEntry_GetCustomer_SearchCustByRef
                    mysproc.ClientId = .ClientId
                    mysproc.RefNum = .CustRefNum
                    mysproc.CustName = .CustName
                    Dim myview As DAL.COMMON.TableView = DBO.GetTableView(mysproc)
                    If myview.Count > 0 Then
                        .CustId = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.CustId)
                        .CustAdd1 = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.AddressLine1)
                        .CustAdd2 = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.AddressLine2)
                        .CustCity = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.City)
                        .CustState = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.State)
                        .CustZip = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.PostalCode)
                        .CustPhone1 = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.Telephone1)
                    Else
                        .CustId = -1
                        .CustAdd1 = sfields(6)
                        .CustAdd2 = sfields(7)
                        .CustCity = sfields(8)
                        .CustState = sfields(9)
                        .CustZip = sfields(10)
                        .CustPhone1 = sfields(11)

                        myCustomer = New TABLES.ClientCustomer
                        myCustomer.ClientId = .ClientId
                        myCustomer.RefNum = .CustRefNum
                        myCustomer.ClientCustomer = .CustName
                        myCustomer.AddressLine1 = .CustAdd1
                        myCustomer.AddressLine2 = .CustAdd2
                        myCustomer.City = .CustCity
                        myCustomer.State = .CustState
                        myCustomer.PostalCode = .CustZip
                        myCustomer.Telephone1 = .CustPhone1
                        BLL.Providers.DBO.Provider.Create(myCustomer)
                        .CustId = myCustomer.CustId
                    End If

                    .JobNum = sfields(1)

                    .JobName = sfields(12)
                    .JobAdd1 = sfields(13)
                    .JobCity = sfields(14)
                    .JobState = sfields(15)
                    .JobZip = sfields(16)

                    .StartDate = ImportUtils.GetDate(sfields(17))
                    .EstBalance = sfields(19)
                    .PONum = sfields(21)
                    .GenId = -1

                    .PrivateJob = True
                    .PrelimBox = True
                    .SubmittedByUserId = auser.Id
                    .BatchId = abatchid
                    .SubmittedOn = Now()

                    .PlacementSource = "EDI"

                    'If Trim(sfields(3)) = "AMEND" Then
                    If Mid(sfields(3), 6, 7) = "AMENDED" Then
                        .IsAmendment = True
                        .JobId = IsExistingJob(.ClientId, .JobNum)

                    End If

                End With
            End If
            Return True

        End Function

        Public Shared Function ParseSBRTX1ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            Dim myCustomer As TABLES.ClientCustomer

            If sfields.Length < 26 Then
                Return False
            End If
            If Len(sfields(1)) < 2 Then
                Return False
            End If
            If Trim(sfields(1)) <> "" Then

                With abatchjob
                    .ClientId = aclientid

                    .CustRefNum = sfields(1)
                    .CustName = sfields(2)

                    Dim mysproc As New SPROCS.uspbo_LiensDataEntry_GetCustomer_SearchCustByRef
                    mysproc.ClientId = .ClientId
                    mysproc.RefNum = .CustRefNum
                    mysproc.CustName = .CustName
                    Dim myview As DAL.COMMON.TableView = DBO.GetTableView(mysproc)
                    If myview.Count > 0 Then
                        .CustId = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.CustId)
                        .CustAdd1 = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.AddressLine1)
                        .CustAdd2 = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.AddressLine2)
                        .CustCity = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.City)
                        .CustState = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.State)
                        .CustZip = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.PostalCode)
                        .CustPhone1 = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.Telephone1)
                    Else
                        .CustId = -1
                        .CustAdd1 = sfields(3)
                        .CustAdd2 = sfields(4)
                        .CustCity = sfields(5)
                        .CustState = sfields(6)
                        .CustZip = sfields(7)
                        If Len(sfields(8)) > 1 Then
                            .CustPhone1 = sfields(8) & "-" & sfields(9)
                        End If
                        If Len(sfields(10)) > 1 Then
                            .CustFax = sfields(10) & "-" & sfields(11)
                        End If

                        myCustomer = New TABLES.ClientCustomer
                        myCustomer.ClientId = .ClientId
                        myCustomer.RefNum = .CustRefNum
                        myCustomer.ClientCustomer = .CustName
                        myCustomer.AddressLine1 = .CustAdd1
                        myCustomer.AddressLine2 = .CustAdd2
                        myCustomer.City = .CustCity
                        myCustomer.State = .CustState
                        myCustomer.PostalCode = .CustZip
                        myCustomer.Telephone1 = .CustPhone1
                        BLL.Providers.DBO.Provider.Create(myCustomer)
                        .CustId = myCustomer.CustId
                    End If

                    .JobNum = sfields(12)

                    .JobName = sfields(14)
                    .JobAdd1 = sfields(15)
                    .JobAdd2 = sfields(16)
                    .JobCity = sfields(17)
                    .JobState = sfields(18)
                    .JobZip = sfields(19)
                    .CustJobNum = sfields(20)
                    .BranchNum = sfields(22)

                    .StartDate = ImportUtils.GetDate(sfields(24))
                    .EstBalance = sfields(25)
                    .GenId = -1

                    .PrivateJob = True
                    .PrelimBox = True
                    .SubmittedByUserId = auser.Id
                    .BatchId = abatchid
                    .SubmittedOn = Now()

                    .PlacementSource = "EDI"

                End With
            End If
            Return True

        End Function
        Public Shared Function ParseBCL831ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 47 Then
                Return False
            End If

            ' 6/13/13 took out cust phone check - And Len(sfields(9)) > 1 
            ' 5/21/16 took out cust zip check - And Len(sfields(7)) > 1 
            If Len(sfields(5)) > 1 And Len(sfields(6)) > 1 Then

                With abatchjob
                    .ClientId = aclientid

                    .JobNum = sfields(11)
                    If Left(Trim(sfields(12)), 4) = "P.O." Or Left(Trim(sfields(12)), 4) = "P O " Or Left(Trim(sfields(12)), 6) = "PO Box" Then
                        .JobAdd1 = sfields(12)
                        .JobName = sfields(13)
                    ElseIf IsNumeric(Left(Trim(sfields(12)), 1)) Then
                        .JobAdd1 = sfields(12)
                        .JobName = sfields(13)
                    Else
                        .JobName = sfields(12)
                        .JobAdd1 = sfields(13)
                    End If

                    .JobCity = sfields(14)
                    .JobState = Left(Trim(sfields(15)), 2)
                    .JobZip = sfields(16)
                    .EstBalance = sfields(0)

                    .StartDate = ImportUtils.GetDate(sfields(41)).ToString("d")

                    .CustRefNum = sfields(1)
                    .CustName = sfields(2)
                    .CustAdd1 = sfields(3)
                    .CustCity = sfields(5)
                    .CustState = sfields(6)
                    .CustZip = sfields(7)
                    '.CustContactName = sfields(8)
                    .CustPhone1 = ImportUtils.GetPhoneNo(sfields(9))
                    .CustSpecialInstruction = sfields(10)

                    Dim sAddr As String

                    '   Owner Info - When name and addr has value
                    If Len(sfields(17)) > 1 Then
                        .OwnrName = sfields(17)
                    End If

                    If Len(sfields(18)) > 1 And sfields(44) = "TRUE" Then

                        sAddr = Trim(sfields(18))

                        ' Parse addr
                        Dim sString As String() = ImportUtils.StringParser(sAddr, "|")

                        If IsNumeric(Left(Trim(sString(0)), 1)) Or Left(Trim(sString(0)), 4) = "P.O." Or Left(Trim(sString(0)), 4) = "P O " Or Left(Trim(sString(0)), 6) = "PO Box" Then
                            .OwnrAdd1 = Trim(sString(0))
                            .OwnrCity = Trim(sString(1))
                            .OwnrState = Left(Trim(sString(2)), 2)
                            .OwnrZip = Trim(sString(3))
                        Else
                            .OwnrName = Trim(sfields(16)) & ", " & Trim(sString(0))
                            .OwnrAdd1 = Trim(sString(1))
                            .OwnrCity = Trim(sString(2))
                            If IsNumeric(Left(Trim(sString(3)), 1)) Then
                                .OwnrState = Right(Trim(sString(2)), 2)
                            Else
                                .OwnrState = Left(Trim(sString(3)), 2)
                            End If
                            If IsNumeric(Right(Trim(sString(3)), 1)) Then
                                If InStr(Trim(sString(3)), "-") > 0 Then
                                    .OwnrZip = Right(Trim(sString(3)), 10)
                                Else
                                    .OwnrZip = Right(Trim(sString(3)), 5)
                                End If
                            Else
                                .OwnrZip = "00000"
                            End If
                        End If
                        If Len(sfields(19)) > 1 Then
                            .OwnrPhone1 = ImportUtils.GetPhoneNo(sfields(19))
                        End If
                    End If

                    '   GC Info - When name has value
                    If Len(sfields(20)) > 1 Then
                        .GCName = sfields(20)
                    End If
                    If Len(sfields(21)) > 1 And sfields(45) = "TRUE" Then
                        sAddr = Trim(sfields(21))

                        ' Parse addr
                        Dim sString2 As String() = ImportUtils.StringParser(sAddr, "|")

                        If IsNumeric(Left(Trim(sString2(0)), 1)) Or Left(Trim(sString2(0)), 4) = "P.O." Or Left(Trim(sString2(0)), 4) = "P O " Or Left(Trim(sString2(0)), 6) = "PO Box" Then
                            .GCAdd1 = Trim(sString2(0))
                            .GCCity = Trim(sString2(1))
                            .GCState = Left(Trim(sString2(2)), 2)
                            .GCZip = Trim(sString2(3))
                        Else
                            .GCName = Trim(sfields(16)) & ", " & Trim(sString2(0))
                            .GCAdd1 = Trim(sString2(1))
                            .GCCity = Trim(sString2(2))

                            If IsNumeric(Left(Trim(sString2(3)), 1)) Then
                                .GCState = Right(Trim(sString2(2)), 2)
                            Else
                                .GCState = Left(Trim(sString2(3)), 2)
                            End If
                            If IsNumeric(Right(Trim(sString2(3)), 1)) Then
                                If InStr(Trim(sString2(3)), "-") > 0 Then
                                    .GCZip = Right(Trim(sString2(3)), 10)
                                Else
                                    .GCZip = Right(Trim(sString2(3)), 5)
                                End If
                            Else
                                .GCZip = "00000"
                            End If
                        End If
                        If Len(sfields(22)) > 1 Then
                            .GCPhone1 = ImportUtils.GetPhoneNo(sfields(22))
                        End If
                    End If

                    '   Lender Info - When name has value
                    If Len(sfields(24)) > 1 Then
                        .LenderName = sfields(24)
                    End If

                    If Len(sfields(25)) > 1 And sfields(46) = "TRUE" Then
                        sAddr = Trim(sfields(25))

                        ' Parse addr
                        Dim sString3 As String() = ImportUtils.StringParser(sAddr, "|")

                        If IsNumeric(Left(Trim(sString3(0)), 1)) Or Left(Trim(sString3(0)), 4) = "P.O." Or Left(Trim(sString3(0)), 4) = "P O " Or Left(Trim(sString3(0)), 6) = "PO Box" Then
                            .LenderAdd1 = Trim(sString3(0))
                            .LenderCity = Trim(sString3(1))
                            .LenderState = Left(Trim(sString3(2)), 2)
                            .LenderZip = Trim(sString3(3))
                        Else
                            .LenderName = Trim(sfields(16)) & ", " & Trim(sString3(0))
                            .LenderAdd1 = Trim(sString3(1))
                            .LenderCity = Trim(sString3(2))

                            If IsNumeric(Left(Trim(sString3(3)), 1)) Then
                                .LenderState = Right(Trim(sString3(2)), 2)
                            Else
                                .LenderState = Left(Trim(sString3(3)), 2)
                            End If
                            If IsNumeric(Right(Trim(sString3(3)), 1)) Then
                                If InStr(Trim(sString3(3)), "-") > 0 Then
                                    .LenderZip = Right(Trim(sString3(3)), 10)
                                Else
                                    .LenderZip = Right(Trim(sString3(3)), 5)
                                End If
                            Else
                                .LenderZip = "00000"
                            End If
                        End If
                        If Len(sfields(26)) > 1 Then
                            .LenderPhone1 = ImportUtils.GetPhoneNo(sfields(26))
                        End If
                    End If

                    '   Other Owner Info to Designee fields
                    If Len(sfields(34)) > 1 Then
                        .DesigneeName = sfields(34)
                    End If

                    If Len(sfields(35)) > 1 Then
                        If Left(Trim(sfields(35)), 4) = "P.O." Or Left(Trim(sfields(35)), 4) = "P O " Or Left(Trim(sfields(35)), 6) = "PO Box" Then
                            .DesigneeAdd1 = sfields(35)
                            .DesigneeCity = sfields(37)
                        ElseIf IsNumeric(Left(Trim(sfields(35)), 1)) Then
                            .DesigneeAdd1 = sfields(35)
                            .DesigneeCity = sfields(37)
                        Else
                            .DesigneeName = sfields(34) & ", " & sfields(35)
                            .DesigneeAdd1 = sfields(36)
                            .DesigneeCity = sfields(37)
                        End If

                        If Len(sfields(38)) > 1 Then
                            .DesigneeZip = sfields(38)
                        Else
                            .DesigneeZip = "00000"
                        End If
                    End If

                    .CustId = -1

                    .PrelimBox = True
                    .SubmittedByUserId = auser.Id
                    .BatchId = abatchid
                    .SubmittedOn = Now()

                    .PlacementSource = "EDI"

                End With
            End If

            Return True

        End Function
        Public Shared Function ParseBCR510ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 19 Then
                Return False
            End If
            If Len(sfields(1)) < 1 Then
                Return False
            End If

            With abatchjob
                .ClientId = aclientid
                Dim iPos As Integer = InStr(sfields(0), ",")
                .BranchNum = Mid(sfields(0), 1, iPos - 1) & "-" & sfields(1)

                .JobNum = sfields(2)
                .JobName = sfields(12)
                .JobAdd1 = sfields(13)
                .JobCity = sfields(14)
                .JobState = sfields(15)
                .JobZip = sfields(16)
                .EstBalance = sfields(19)
                .PONum = sfields(18)
                .StartDate = ImportUtils.GetDate(sfields(11))

                .CustName = sfields(4)
                .CustAdd1 = sfields(5)
                .CustCity = sfields(6)
                .CustState = sfields(7)
                .CustZip = sfields(8)
                .CustPhone1 = ImportUtils.GetPhoneNo(sfields(9))

                .CustId = -1

                .PrelimBox = True
                .SubmittedByUserId = auser.Id
                .BatchId = abatchid
                .SubmittedOn = Now()

                .PlacementSource = "EDI"

            End With
            Return True

        End Function

        Public Shared Function ParseNESImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 21 Then
                Return False
            End If

            If Trim(sfields(3)) = "" And Trim(sfields(4)) = "" Then
                Return False
            End If

            With abatchjob
                .ClientId = aclientid

                .JobNum = Trim(sfields(3))
                .JobName = Trim(sfields(4))

                If Trim(sfields(5)) <> "" And Trim(sfields(6)) = "" Then
                    .JobAdd1 = Trim(sfields(5))
                Else
                    .JobAdd1 = Trim(sfields(6))
                End If
                .JobAdd2 = Trim(sfields(7))
                .JobCity = Trim(sfields(8))
                .JobState = Trim(sfields(9))
                .JobZip = Trim(sfields(10))

                .StartDate = ImportUtils.GetDate(sfields(11))
                .EstBalance = sfields(12)
                .BranchNum = sfields(2)

                .CustRefNum = sfields(0)
                .CustName = Trim(sfields(1))
                Dim mysproc As New SPROCS.uspbo_LiensDataEntry_GetCustomer_SearchCustByRef
                mysproc.ClientId = .ClientId
                mysproc.RefNum = .CustRefNum
                mysproc.CustName = .CustName
                Dim myview As DAL.COMMON.TableView = DBO.GetTableView(mysproc)
                If myview.Count > 0 Then
                    .CustId = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.CustId)
                    .CustAdd1 = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.AddressLine1)
                    .CustAdd2 = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.AddressLine2)
                    .CustCity = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.City)
                    .CustState = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.State)
                    .CustZip = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.PostalCode)
                    .CustPhone1 = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.Telephone1)
                Else
                    .CustId = -1
                    .CustAdd1 = sfields(13)
                    .CustAdd2 = Trim(sfields(14))
                    .CustCity = sfields(15)
                    .CustState = sfields(16)
                    .CustZip = sfields(17)
                    .CustPhone1 = ImportUtils.GetPhoneNo(sfields(18))
                End If

                .PrelimBox = True
                .SubmittedByUserId = auser.Id
                .BatchId = abatchid
                .SubmittedOn = Now()

                .PlacementSource = "EDI"

            End With

            Return True
        End Function
        Public Shared Function ParseRSGImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 17 Then
                Return False
            End If
            If Len(sfields(0)) < 1 Then
                Return False
            End If
            Dim sSql As String = "Select ClientId From Client with (nolock) Where ClientCode = '" & sfields(0) & "'"
            Dim myview As DAL.COMMON.TableView = DBO.GetTableView(sSql)
            If myview.Count <= 0 Then
                Return False
            End If

            With abatchjob
                .ClientId = myview.RowItem("ClientId")

                .CustName = sfields(1)
                .CustAdd1 = sfields(2)
                .CustCity = sfields(3)
                .CustState = sfields(4)
                .CustZip = sfields(5)
                .CustPhone1 = ImportUtils.GetPhoneNo(sfields(6))
                .CustFax = ImportUtils.GetPhoneNo(sfields(7))
                .CustId = -1

                .JobNum = sfields(9)
                .JobName = sfields(8)
                .JobAdd1 = sfields(11)
                .JobAdd2 = sfields(12)
                .JobCity = sfields(13)
                .JobState = sfields(14)
                .JobZip = sfields(15)
                .EstBalance = sfields(16)
                .StartDate = ImportUtils.GetDate(sfields(10))

                .PrelimBox = True
                .SubmittedByUserId = auser.Id
                .BatchId = abatchid
                .SubmittedOn = Now()

                .PlacementSource = "EDI"

            End With
            Return True

        End Function
        Public Shared Function ParseProbuildImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 16 Then
                Return False
            End If
            If Len(sfields(0)) < 1 Then
                Return False
            End If
            Dim sSql As String = "Select ClientId From Client with (nolock) Where ClientCode = '" & sfields(16) & "'"
            Dim myview As DAL.COMMON.TableView = DBO.GetTableView(sSql)
            If myview.Count <= 0 Then
                Return False
            End If

            If Len(sfields(12)) > 0 Then    'Skip if EstBalance is blank
                With abatchjob
                    .ClientId = myview.RowItem("ClientId")

                    .CustName = sfields(0)
                    .CustAdd1 = sfields(1)
                    .CustAdd2 = sfields(2)
                    .CustCity = sfields(3)
                    .CustState = sfields(4)
                    .CustZip = sfields(5)
                    .CustPhone1 = ImportUtils.GetPhoneNo(sfields(6))
                    .CustId = -1

                    .JobNum = sfields(7)
                    .JobName = sfields(8)
                    .JobAdd1 = sfields(9)
                    .JobCity = sfields(10)
                    .JobState = sfields(14)
                    .JobZip = sfields(15)
                    .JobCounty = sfields(11)
                    .EstBalance = sfields(12)
                    .StartDate = ImportUtils.GetDate(sfields(13))

                    .PrelimBox = True
                    .SubmittedByUserId = auser.Id
                    .BatchId = abatchid
                    .SubmittedOn = Now()

                    .PlacementSource = "EDI"

                End With
            End If

            Return True

        End Function
        Public Shared Function ParseGWB562ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 29 Then
                Return False
            End If
            If Len(sfields(1)) < 1 Then
                Return False
            End If

            With abatchjob
                .ClientId = aclientid

                .CustRefNum = Left(sfields(1), 5)
                .CustName = sfields(16)
                .CustAdd1 = sfields(17)
                .CustCity = sfields(19)
                .CustState = sfields(20)
                .CustZip = sfields(21)
                .CustPhone1 = ImportUtils.GetPhoneNo(sfields(22))
                .CustId = -1

                If Left(sfields(25), 3) = "GC:" Then
                    Dim sSql As String = "Select * from ClientGeneralContractor with (nolock) Where ClientId = " & aclientid & " and GeneralContractor = '" & Mid(Replace(sfields(25), "'", "''"), 5) & "'"
                    Dim myview As DAL.COMMON.TableView = DBO.GetTableView(sSql)
                    If myview.Count > 0 Then
                        .GenId = myview.RowItem("GenId")
                        .GCRefNum = myview.RowItem("RefNum")
                        .GCName = myview.RowItem("GeneralContractor")
                        .GCContactName = myview.RowItem("ContactName")
                        .GCAdd1 = myview.RowItem("AddressLine1")
                        .GCAdd2 = myview.RowItem("AddressLine2")
                        .GCCity = myview.RowItem("City")
                        .GCState = myview.RowItem("State")
                        .GCZip = myview.RowItem("Postalcode")
                        .GCPhone1 = myview.RowItem("Telephone1")
                        .GCPhone2 = myview.RowItem("Telephone2")
                        .GCFax = myview.RowItem("Fax")
                    Else
                        .GCName = Mid(sfields(25), 5)
                    End If
                End If

                .JobNum = Trim(.CustRefNum) & "-" & Trim(sfields(2))
                .JobName = sfields(23)
                .JobAdd1 = sfields(24)
                .JobCity = sfields(26)
                .JobState = sfields(27)
                .JobZip = sfields(28)
                .EstBalance = sfields(6)

                ' 10/15/2014
                If sfields(29) = "x" Or sfields(29) = "X" Then
                    .JointCk = True
                Else
                    .JointCk = False
                End If

                If Len(sfields(12)) > 0 Then
                    .StartDate = ImportUtils.GetDate(sfields(12))
                Else
                    .StartDate = Today
                End If

                .PrelimBox = True
                .SubmittedByUserId = auser.Id
                .BatchId = abatchid
                .SubmittedOn = Now()

                .PlacementSource = "EDI"

            End With
            Return True

        End Function
        Public Shared Function ParseHCR510ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 11 Then
                Return False
            End If

            If Trim(sfields(0)) <> "" Then

                With abatchjob

                    Dim sSql As String = "Select * from ClientCustomer with (nolock) Where ClientId = " & aclientid & " and ClientCustomer = '" & Replace(Left(sfields(9), 100), "'", "''") & "'"

                    Dim myview As DAL.COMMON.TableView = DBO.GetTableView(sSql)
                    If myview.Count > 0 Then
                        .CustId = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.CustId)
                        .CustName = sfields(9)
                        .CustAdd1 = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.AddressLine1)
                        .CustAdd2 = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.AddressLine2)
                        .CustCity = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.City)
                        .CustState = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.State)
                        .CustZip = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.PostalCode)
                        .CustPhone1 = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.Telephone1)
                        .CustFax = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.Fax)
                    Else
                        .CustId = -1
                    End If

                    .JobNum = sfields(0)
                    .JobName = sfields(1)
                    .JobAdd1 = sfields(2)
                    .JobCity = sfields(3)
                    .JobState = sfields(4)
                    .JobZip = sfields(5)

                    .StartDate = ImportUtils.GetDate(sfields(7))
                    .EstBalance = sfields(6)
                    .PONum = sfields(8)

                    .GenId = -1

                    .PrelimBox = True
                    .SubmittedByUserId = auser.Id
                    .BatchId = abatchid
                    .SubmittedOn = Now()
                    .ClientId = aclientid

                    .PlacementSource = "EDI"

                End With
            End If

            Return True

        End Function

        Public Shared Function ParseUSC626ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 74 Or Len(sfields(13)) < 1 Then
                Return False
            End If
            If Len(sfields(13)) < 2 Then
                Return False
            End If

            With abatchjob
                .JobNum = sfields(0)
                .JobName = sfields(1)
                .JobAdd1 = sfields(2)
                .JobAdd2 = sfields(3)
                If .JobAdd1 = Nothing And .JobAdd2 <> Nothing Then
                    .JobAdd1 = .JobAdd2
                    .JobAdd2 = ""
                End If
                .JobCity = sfields(4)
                .JobState = sfields(5)
                .JobZip = sfields(6)
                .EstBalance = sfields(7)
                .StartDate = ImportUtils.GetDate(sfields(8))

                .CustJobNum = sfields(10)
                '
                '   7/16/2015 - Changed from PO_NUM to CUST_PO_NUM
                .PONum = sfields(11)
                .CustRefNum = sfields(12)
                .CustName = sfields(13)
                .CustContactName = sfields(14)
                .CustAdd1 = sfields(15)
                .CustAdd2 = sfields(16)
                If .CustAdd1 = Nothing And .CustAdd2 <> Nothing Then
                    .CustAdd1 = .CustAdd2
                    .CustAdd2 = ""
                End If
                .CustCity = sfields(17)
                .CustState = sfields(18)
                .CustZip = sfields(19)
                .CustPhone1 = ImportUtils.GetPhoneNo(sfields(20))
                .CustPhone2 = ImportUtils.GetPhoneNo(sfields(21))
                .CustId = -1

                .GCRefNum = sfields(22)
                .GCName = sfields(23)
                If .GCName = "SAME AS CUSTOMER" Then
                    .GCName = .CustName
                    .GCContactName = .CustContactName
                    .GCAdd1 = .CustAdd1
                    .GCAdd2 = .CustAdd2
                    .GCCity = .CustCity
                    .GCState = .CustState
                    .GCZip = .CustZip
                    .GCPhone1 = .CustPhone1
                    .GCPhone2 = .CustPhone2
                    .GenId = -1
                Else
                    .GCContactName = sfields(24)
                    .GCAdd1 = sfields(25)
                    .GCAdd2 = sfields(26)
                    .GCCity = sfields(27)
                    .GCState = sfields(28)
                    .GCZip = sfields(29)
                    .GCPhone1 = ImportUtils.GetPhoneNo(sfields(30))
                    .GCPhone2 = ImportUtils.GetPhoneNo(sfields(31))
                    .GenId = -1
                End If

                .OwnrName = sfields(32)
                .OwnrAdd1 = sfields(33)
                .OwnrAdd2 = sfields(34)
                .OwnrCity = sfields(35)
                .OwnrState = sfields(36)
                .OwnrZip = sfields(37)
                .OwnrPhone1 = ImportUtils.GetPhoneNo(sfields(38))

                .BondNum = sfields(39)

                ' 4/4/2016 
                If sfields(47) <> "NONE REPORTED" Then
                    .LenderName = sfields(47)
                    .LenderAdd1 = sfields(48)
                    .LenderAdd2 = sfields(49)
                    .LenderCity = sfields(50)
                    .LenderState = sfields(51)
                    .LenderZip = sfields(52)
                    .LenderPhone1 = ImportUtils.GetPhoneNo(sfields(53))
                End If
                If sfields(40) <> "NONE REPORTED" And sfields(47) = "NONE REPORTED" Then
                    .LenderName = sfields(40)
                    .LenderAdd1 = sfields(41)
                    .LenderAdd2 = sfields(42)
                    .LenderCity = sfields(43)
                    .LenderState = sfields(44)
                    .LenderZip = sfields(45)
                    .LenderPhone1 = ImportUtils.GetPhoneNo(sfields(46))
                End If
                If sfields(40) <> "NONE REPORTED" And sfields(47) <> "NONE REPORTED" Then
                    .DesigneeName = sfields(40)
                    .DesigneeAdd1 = sfields(41)
                    .DesigneeAdd2 = sfields(42)
                    .DesigneeCity = sfields(43)
                    .DesigneeState = sfields(44)
                    .DesigneeZip = sfields(45)
                    .DesigneePhone1 = ImportUtils.GetPhoneNo(sfields(46))
                End If

                .SpecialInstruction = sfields(54)

                .PublicJob = False
                .FederalJob = False
                .ResidentialBox = False
                .PrivateJob = False

                .PrelimBox = False
                .VerifyJob = False
                .MechanicLien = False
                .TitleVerifiedBox = False
                .PrelimASIS = False

                If sfields(55) = "T" Then
                    .PublicJob = True
                End If
                If sfields(56) = "T" Then
                    .FederalJob = True
                End If
                If sfields(57) = "T" Then
                    .ResidentialBox = True
                End If
                If sfields(58) = "T" Then
                    .JobActionId = 37
                    .VerifyJob = True
                End If
                If sfields(59) = "T" Then
                    .JobActionId = 39
                    .MechanicLien = True
                End If
                If sfields(60) = "T" Then
                    .JobActionId = 38
                    .TitleVerifiedBox = True
                End If
                If sfields(64) = "T" Then
                    .JobActionId = 44
                    .PrelimASIS = True
                End If

                If .VerifyJob = False And .MechanicLien = False And .TitleVerifiedBox = False And .PrelimASIS = False Then
                    .JobActionId = 36
                    .PrelimBox = True
                End If

                .BranchNum = sfields(61)
                .RANum = sfields(62)
                .CustEmail = sfields(71)
                .GCEmail = sfields(72)
                If sfields(73) = "TRUE" Then
                    .GreenCard = 1
                Else
                    .GreenCard = 0
                End If

                .SubmittedByUserId = auser.Id
                .BatchId = abatchid
                .SubmittedOn = Now()
                .ClientId = aclientid

                .PlacementSource = "EDI"

            End With
            Return True

        End Function

        Public Shared Function ParseFBMImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 23 Then
                Return False
            End If
            If Len(sfields(1)) < 1 Then
                Return False
            End If

            With abatchjob
                .ClientId = aclientid
                .BranchNum = sfields(0)
                .CustRefNum = sfields(1)
                .CustName = sfields(2)
                .CustAdd1 = sfields(3)
                .CustAdd2 = sfields(4)
                .CustCity = sfields(5)
                .CustState = sfields(6)
                .CustZip = sfields(7)
                .CustPhone1 = ImportUtils.GetPhoneNo(sfields(8))
                .CustFax = ImportUtils.GetPhoneNo(sfields(9))
                .CustId = -1

                If Left(sfields(13), 3) = "GC:" Then
                    Dim sSql As String = "Select * from ClientGeneralContractor with (nolock) Where ClientId = " & aclientid & " and GeneralContractor = '" & Mid(Replace(sfields(13), "'", "''"), 5) & "'"
                    Dim myview As DAL.COMMON.TableView = DBO.GetTableView(sSql)
                    If myview.Count > 0 Then
                        .GenId = myview.RowItem("GenId")
                        .GCRefNum = myview.RowItem("RefNum")
                        .GCName = myview.RowItem("GeneralContractor")
                        .GCContactName = myview.RowItem("ContactName")
                        .GCAdd1 = myview.RowItem("AddressLine1")
                        .GCAdd2 = myview.RowItem("AddressLine2")
                        .GCCity = myview.RowItem("City")
                        .GCState = myview.RowItem("State")
                        .GCZip = myview.RowItem("Postalcode")
                        .GCPhone1 = myview.RowItem("Telephone1")
                        .GCPhone2 = myview.RowItem("Telephone2")
                        .GCFax = myview.RowItem("Fax")
                    Else
                        .GCName = Mid(sfields(13), 5)
                    End If
                End If

                .JobNum = sfields(10)
                .JobName = sfields(11)
                .JobAdd1 = sfields(12)

                If Left(sfields(13), 3) <> "GC:" Then
                    .JobAdd2 = sfields(13)
                Else
                    .JobAdd2 = ""
                End If

                .JobCity = sfields(15)
                .JobState = sfields(16)
                .JobZip = sfields(17)
                .EstBalance = sfields(19)
                If sfields(20) = "Y" Then
                    .JointCk = True
                Else
                    .JointCk = False
                End If
                .StartDate = ImportUtils.GetDate(sfields(23))

                '   11/16/2021
                If .JobState = "UT" Then
                    .VerifyJob = True
                    .PrelimBox = False
                Else
                    .VerifyJob = False
                    .PrelimBox = True
                End If

                '.PrelimBox = True
                .SubmittedByUserId = auser.Id
                .BatchId = abatchid
                .SubmittedOn = Now()

                .PlacementSource = "EDI"

            End With
            Return True

        End Function
        Public Shared Function ParseFBM253ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 20 Then
                Return False
            End If
            If Len(sfields(1)) < 1 Then
                Return False
            End If

            With abatchjob
                .ClientId = aclientid
                .BranchNum = sfields(0)
                .CustRefNum = sfields(1)
                .CustName = sfields(2)
                .CustAdd1 = sfields(3)
                .CustAdd2 = sfields(4)
                .CustCity = sfields(5)
                .CustState = sfields(6)
                .CustZip = sfields(7)
                .CustPhone1 = ImportUtils.GetPhoneNo(sfields(8))
                .CustFax = ImportUtils.GetPhoneNo(sfields(9))
                .CustId = -1

                If Len(sfields(11)) > 0 Then
                    Dim sSql As String = "Select * from ClientGeneralContractor with (nolock) Where ClientId = " & aclientid & " and GeneralContractor = '" & Trim(sfields(11)) & "'"
                    Dim myview As DAL.COMMON.TableView = DBO.GetTableView(sSql)
                    If myview.Count > 0 Then
                        .GenId = myview.RowItem("GenId")
                        .GCRefNum = myview.RowItem("RefNum")
                        .GCName = myview.RowItem("GeneralContractor")
                        .GCContactName = myview.RowItem("ContactName")
                        .GCAdd1 = myview.RowItem("AddressLine1")
                        .GCAdd2 = myview.RowItem("AddressLine2")
                        .GCCity = myview.RowItem("City")
                        .GCState = myview.RowItem("State")
                        .GCZip = myview.RowItem("Postalcode")
                        .GCPhone1 = myview.RowItem("Telephone1")
                        .GCPhone2 = myview.RowItem("Telephone2")
                        .GCFax = myview.RowItem("Fax")
                    Else
                        .GCName = Trim(sfields(11))
                    End If
                End If

                .JobNum = sfields(10)
                .JobName = sfields(12)
                .JobAdd1 = sfields(13)
                .JobCity = sfields(15)
                .JobState = sfields(16)
                .JobZip = sfields(17)
                .EstBalance = sfields(18)

                .StartDate = ImportUtils.GetDate(sfields(19))

                .PrelimBox = True
                .ResidentialBox = True
                .PrivateJob = False
                .PublicJob = False
                .SubmittedByUserId = auser.Id
                .BatchId = abatchid
                .SubmittedOn = Now()

                .PlacementSource = "EDI"

            End With
            Return True

        End Function
        Public Shared Function ParseBeaconImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob, ByVal ainterfaceid As Integer) As Integer
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 65 Then
                Return 0
            End If

            If (sfields(62).ToUpper = "X" Or sfields(63).ToUpper = "X" Or sfields(64).ToUpper = "X") And Len(sfields(6)) > 0 Then
                With abatchjob

                    Dim sSql As String = "Select * from vwClientInterfaceReference where InterfaceName = 'Beacon' and InputField = '" & sfields(4) & "'"
                    Dim myview As DAL.COMMON.TableView = DBO.GetTableView(sSql)
                    If myview.Count > 0 Then
                        .ClientId = myview.RowItem("ClientId")
                    End If

                    .BranchNum = sfields(4)
                    .JobNum = Trim(sfields(48))
                    .JobName = Trim(sfields(49))
                    .JobAdd1 = Trim(sfields(50))
                    .JobAdd2 = Trim(sfields(51))
                    If .JobAdd2 = Nothing And Trim(sfields(52)) <> Nothing Then
                        .JobAdd2 = Trim(sfields(52))
                    End If
                    .JobCity = Trim(sfields(53))
                    .JobState = sfields(54)
                    .JobZip = Trim(sfields(55))
                    .EstBalance = sfields(57)
                    .StartDate = ImportUtils.GetDate(sfields(56))
                    .PONum = Trim(sfields(9))

                    .CustRefNum = Trim(sfields(5))
                    .CustName = Trim(sfields(6))
                    .CustAdd1 = Trim(sfields(7))
                    .CustAdd2 = Trim(sfields(8))
                    If .CustAdd1 = Nothing And .CustAdd2 <> Nothing Then
                        .CustAdd1 = .CustAdd2
                        .CustAdd2 = ""
                    End If
                    .CustCity = Trim(sfields(10))
                    .CustState = sfields(11)
                    .CustZip = sfields(12)
                    .CustPhone1 = ImportUtils.GetPhoneNo(sfields(13))

                    .CustId = -1

                    .GCName = Trim(sfields(31))
                    .GCContactName = Trim(sfields(34))
                    .GCAdd1 = Trim(sfields(32))
                    .GCAdd2 = Trim(sfields(33))
                    .GCCity = Trim(sfields(35))
                    .GCState = sfields(36)
                    .GCZip = Trim(sfields(37))
                    If sfields(38) <> "000-000-0000" Then
                        .GCPhone1 = ImportUtils.GetPhoneNo(sfields(38))
                    End If

                    .GenId = -1

                    .OwnrName = Trim(sfields(15))
                    .OwnrAdd1 = Trim(sfields(16))
                    .OwnrAdd2 = Trim(sfields(17))
                    .OwnrCity = Trim(sfields(19))
                    .OwnrState = sfields(20)
                    .OwnrZip = Trim(sfields(21))
                    If sfields(22) <> "000-000-0000" Then
                        .OwnrPhone1 = ImportUtils.GetPhoneNo(sfields(22))
                    End If
                    '
                    '   1/12/17 Save Lender or Bond to Lender info. If both exist, save Bond info to Other Lender

                    If Trim(sfields(23)) <> "" Then
                        .BondNum = Trim(sfields(39))
                        .LenderName = Trim(sfields(23))
                        .LenderAdd1 = Trim(sfields(24))
                        .LenderAdd2 = Trim(sfields(25))
                        .LenderCity = Trim(sfields(27))
                        .LenderState = sfields(28)
                        .LenderZip = Trim(sfields(29))
                        .LenderPhone1 = ImportUtils.GetPhoneNo(sfields(30))
                    End If
                    If (Trim(sfields(23)) = "" Or Trim(sfields(23)) Is Nothing) And Trim(sfields(40)) <> "" Then
                        .BondNum = sfields(39)
                        .LenderName = Trim(sfields(40))
                        .LenderAdd1 = Trim(sfields(41))
                        .LenderAdd2 = Trim(sfields(42))
                        .LenderCity = Trim(sfields(44))
                        .LenderState = sfields(45)
                        .LenderZip = Trim(sfields(46))
                        If sfields(47) <> "000-000-0000" Then
                            .LenderPhone1 = ImportUtils.GetPhoneNo(sfields(47))
                        End If
                    End If
                    If Trim(sfields(23)) <> "" And Trim(sfields(40)) <> "" Then
                        .BondNum = sfields(39)
                        .DesigneeName = Trim(sfields(40))
                        .DesigneeAdd1 = Trim(sfields(41))
                        .DesigneeAdd2 = Trim(sfields(42))
                        .DesigneeCity = Trim(sfields(44))
                        .DesigneeState = sfields(45)
                        .DesigneeZip = Trim(sfields(46))
                        If sfields(47) <> "000-000-0000" Then
                            .DesigneePhone1 = ImportUtils.GetPhoneNo(sfields(47))
                        End If
                    End If

                    .PublicJob = False
                    .FederalJob = False
                    .ResidentialBox = False
                    .PrivateJob = False
                    .VerifyOWOnlySend = False
                    .PrelimBox = False
                    .PrelimASIS = False

                    If sfields(60).ToUpper = "X" Then
                        .PublicJob = True
                    End If
                    If sfields(59).ToUpper = "X" Then
                        .FederalJob = True
                    End If
                    If sfields(58).ToUpper = "X" Then
                        .ResidentialBox = True
                    End If
                    If sfields(61).ToUpper = "X" Then
                        .GreenCard = True
                    End If
                    If sfields(63).ToUpper = "X" Then
                        .JobActionId = 435
                        .VerifyOWOnlySend = True
                    End If

                    ' 5/8/2019
                    If sfields(62).ToUpper = "X" And .JobState = "TX" Then
                        .JobActionId = 38
                        .VerifyJobASIS = True
                    ElseIf sfields(62).ToUpper = "X" And .JobState = "TN" Then
                        .JobActionId = 38
                        .VerifyJobASIS = True
                    ElseIf sfields(62).ToUpper = "X" Then
                        .JobActionId = 44
                        .PrelimASIS = True
                    End If

                    ' 5/8/2019
                    If sfields(64).ToUpper = "X" And .JobState = "TX" Then
                        .JobActionId = 37
                        .VerifyJob = True
                    ElseIf sfields(64).ToUpper = "X" And .JobState = "TN" Then
                        .JobActionId = 37
                        .VerifyJob = True
                    ElseIf sfields(64).ToUpper = "X" Then
                        .JobActionId = 36
                        .PrelimBox = True
                    End If

                    '   5/8/2019 hardcoded client to BCN000
                    sSql = "Select * from Portal_Users Where ClientId = 20621 and UserName = '" & sfields(65) & "'"
                    Dim myview2 As DAL.COMMON.TableView = DBO.GetTableView(sSql)
                    If myview2.Count > 0 Then
                        .SubmittedByUserId = myview2.RowItem("Id")
                    Else
                        .SubmittedByUserId = 0
                    End If
                    .BatchId = abatchid
                    .SubmittedOn = Now()

                    .PlacementSource = "EDI"
                End With

                Return 1
            Else
                Return 2
            End If

        End Function

        Public Shared Function ParseSBI916ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 65 Or Len(sfields(13)) < 1 Then
                Return False
            End If
            If Len(sfields(13)) < 2 Then
                Return False
            End If

            With abatchjob
                .JobNum = sfields(0)
                .JobName = sfields(1)
                .JobAdd1 = sfields(2)
                .JobAdd2 = sfields(3)
                If .JobAdd1 = Nothing And .JobAdd2 <> Nothing Then
                    .JobAdd1 = .JobAdd2
                    .JobAdd2 = ""
                End If
                .JobCity = sfields(4)
                .JobState = sfields(5)

                Dim iPos As Integer

                If Len(sfields(6)) > 5 Then
                    iPos = InStr(sfields(6), "-")
                    .JobZip = String.Format("{0:0000}", Mid(sfields(6), 1, iPos - 1)) & Mid(sfields(6), iPos)
                Else
                    .JobZip = String.Format("{0:0000}", sfields(6))
                End If

                .EstBalance = sfields(7)
                .StartDate = ImportUtils.GetDate(sfields(8))
                .PONum = sfields(9)

                .CustJobNum = sfields(10)

                .CustRefNum = sfields(12)
                .CustName = sfields(13)
                .CustContactName = sfields(14)
                .CustAdd1 = sfields(15)
                .CustAdd2 = sfields(16)
                If .CustAdd1 = Nothing And .CustAdd2 <> Nothing Then
                    .CustAdd1 = .CustAdd2
                    .CustAdd2 = ""
                End If
                .CustCity = sfields(17)
                .CustState = sfields(18)
                If Len(sfields(19)) > 5 Then
                    iPos = InStr(sfields(19), "-")
                    .CustZip = String.Format("{0:0000}", Mid(sfields(19), 1, iPos - 1)) & Mid(sfields(19), iPos)
                Else
                    .CustZip = String.Format("{0:0000}", sfields(19))
                End If
                .CustPhone1 = ImportUtils.GetPhoneNo(sfields(20))
                .CustPhone2 = ImportUtils.GetPhoneNo(sfields(21))
                .CustId = -1

                .GCRefNum = sfields(22)
                .GCName = sfields(23)
                If .GCName = "SAME AS CUSTOMER" Then
                    .GCName = .CustName
                    .GCContactName = .CustContactName
                    .GCAdd1 = .CustAdd1
                    .GCAdd2 = .CustAdd2
                    .GCCity = .CustCity
                    .GCState = .CustState
                    .GCZip = .CustZip
                    .GCPhone1 = .CustPhone1
                    .GCPhone2 = .CustPhone2
                    .GenId = -1
                Else
                    .GCContactName = sfields(24)
                    .GCAdd1 = sfields(25)
                    .GCAdd2 = sfields(26)
                    .GCCity = sfields(27)
                    .GCState = sfields(28)
                    If Len(sfields(29)) > 5 Then
                        iPos = InStr(sfields(29), "-")
                        .GCZip = String.Format("{0:0000}", Mid(sfields(29), 1, iPos - 1)) & Mid(sfields(29), iPos)
                    Else
                        .GCZip = String.Format("{0:0000}", sfields(29))
                    End If

                    .StartDate = ImportUtils.GetDate(sfields(8))
                    .PONum = sfields(9)
                    .GCPhone1 = ImportUtils.GetPhoneNo(sfields(30))
                    .GCPhone2 = ImportUtils.GetPhoneNo(sfields(31))
                    .GenId = -1
                End If

                .OwnrName = sfields(32)
                .OwnrAdd1 = sfields(33)
                .OwnrAdd2 = sfields(34)
                .OwnrCity = sfields(35)
                .OwnrState = sfields(36)

                If Len(sfields(37)) > 5 Then
                    iPos = InStr(sfields(37), "-")
                    .OwnrZip = String.Format("{0:0000}", Mid(sfields(37), 1, iPos - 1)) & Mid(sfields(37), iPos)
                Else
                    .OwnrZip = String.Format("{0:0000}", sfields(37))
                End If

                .OwnrPhone1 = ImportUtils.GetPhoneNo(sfields(38))

                ' If lender addr1 is not blank
                If Trim(sfields(48)) <> "" Then
                    .BondNum = sfields(39)
                    .LenderName = sfields(47)
                    .LenderAdd1 = sfields(48)
                    .LenderAdd2 = sfields(49)
                    .LenderCity = sfields(50)
                    .LenderState = sfields(51)

                    If Len(sfields(52)) > 5 Then
                        iPos = InStr(sfields(52), "-")
                        .LenderZip = String.Format("{0:0000}", Mid(sfields(52), 1, iPos - 1)) & Mid(sfields(52), iPos)
                    Else
                        .LenderZip = String.Format("{0:0000}", sfields(52))
                    End If

                    .LenderPhone1 = ImportUtils.GetPhoneNo(sfields(53))
                End If

                ' If bond Addr1 is not blank
                If sfields(41) <> "" And sfields(41) <> Nothing Then
                    .LenderName = sfields(40)
                    .LenderAdd1 = sfields(41)
                    .LenderAdd2 = sfields(42)
                    .LenderCity = sfields(43)
                    .LenderState = sfields(44)

                    If Len(sfields(45)) > 5 Then
                        iPos = InStr(sfields(45), "-")
                        .LenderZip = String.Format("{0:0000}", Mid(sfields(45), 1, iPos - 1)) & Mid(sfields(45), iPos)
                    Else
                        .LenderZip = String.Format("{0:0000}", sfields(45))
                    End If
                    .LenderPhone1 = ImportUtils.GetPhoneNo(sfields(46))
                End If

                .SpecialInstruction = sfields(54)

                .PublicJob = False
                .FederalJob = False
                .ResidentialBox = False
                .PrivateJob = False

                .PrelimBox = False
                .VerifyJob = False
                .MechanicLien = False
                .TitleVerifiedBox = False
                .PrelimASIS = False
                '
                '   3/5/2018 Client replaced PublicJob for ReturnReceipt and FederalJob for JointCheck
                '
                If sfields(55) = "T" Then
                    .GreenCard = True
                End If
                If sfields(56) = "T" Then
                    .JointCk = True
                End If
                If sfields(57) = "T" Then
                    .ResidentialBox = True
                End If
                If sfields(58) = "T" Then
                    .JobActionId = 37
                    .VerifyJob = True
                End If
                If sfields(59) = "T" Then
                    .JobActionId = 39
                    .MechanicLien = True
                End If
                If sfields(60) = "T" Then
                    .JobActionId = 38
                    .TitleVerifiedBox = True
                End If
                If sfields(64) = "T" Then
                    .JobActionId = 44
                    .PrelimASIS = True
                End If

                If .VerifyJob = False And .MechanicLien = False And .TitleVerifiedBox = False And .PrelimASIS = False Then
                    .JobActionId = 36
                    .PrelimBox = True
                End If

                .BranchNum = sfields(61)
                .RANum = sfields(62)
                .SubmittedByUserId = auser.Id
                .BatchId = abatchid
                .SubmittedOn = Now()
                .ClientId = aclientid

                .PlacementSource = "EDI"

            End With
            Return True

        End Function

        Public Shared Function ParseSlakeyImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 65 Or Len(sfields(61)) < 1 Then
                Return False
            End If
            If Len(sfields(61)) < 2 Then
                Return False
            End If

            With abatchjob

                Dim sSql As String = "Select * from vwClientInterfaceReference where InterfaceName = 'SLAKEY' and InputField = '" & sfields(61) & "'"
                Dim myview As DAL.COMMON.TableView = DBO.GetTableView(sSql)
                If myview.Count > 0 Then
                    .ClientId = myview.RowItem("ClientId")
                End If

                .JobNum = sfields(0)
                .JobName = sfields(1)
                .JobAdd1 = sfields(2)
                .JobAdd2 = sfields(3)
                If .JobAdd1 = Nothing And .JobAdd2 <> Nothing Then
                    .JobAdd1 = .JobAdd2
                    .JobAdd2 = ""
                End If
                .JobCity = sfields(4)
                .JobState = sfields(5)

                Dim iPos As Integer

                If Len(sfields(6)) > 5 Then
                    iPos = InStr(sfields(6), "-")
                    .JobZip = String.Format("{0:0000}", Mid(sfields(6), 1, iPos - 1)) & Mid(sfields(6), iPos)
                Else
                    .JobZip = String.Format("{0:0000}", sfields(6))
                End If

                .EstBalance = sfields(7)
                .StartDate = ImportUtils.GetDate(sfields(8))
                .PONum = sfields(9)

                .CustJobNum = sfields(10)

                .CustRefNum = sfields(12)
                .CustName = sfields(13)
                .CustContactName = sfields(14)
                .CustAdd1 = sfields(15)
                .CustAdd2 = sfields(16)
                If .CustAdd1 = Nothing And .CustAdd2 <> Nothing Then
                    .CustAdd1 = .CustAdd2
                    .CustAdd2 = ""
                End If
                .CustCity = sfields(17)
                .CustState = sfields(18)
                If Len(sfields(19)) > 5 Then
                    iPos = InStr(sfields(19), "-")
                    .CustZip = String.Format("{0:0000}", Mid(sfields(19), 1, iPos - 1)) & Mid(sfields(19), iPos)
                Else
                    .CustZip = String.Format("{0:0000}", sfields(19))
                End If
                .CustPhone1 = ImportUtils.GetPhoneNo(sfields(20))
                .CustPhone2 = ImportUtils.GetPhoneNo(sfields(21))
                .CustId = -1

                .GCRefNum = sfields(22)
                .GCName = sfields(23)
                If .GCName = "SAME AS CUSTOMER" Then
                    .GCName = .CustName
                    .GCContactName = .CustContactName
                    .GCAdd1 = .CustAdd1
                    .GCAdd2 = .CustAdd2
                    .GCCity = .CustCity
                    .GCState = .CustState
                    .GCZip = .CustZip
                    .GCPhone1 = .CustPhone1
                    .GCPhone2 = .CustPhone2
                    .GenId = -1
                Else
                    .GCContactName = sfields(24)
                    .GCAdd1 = sfields(25)
                    .GCAdd2 = sfields(26)
                    .GCCity = sfields(27)
                    .GCState = sfields(28)
                    If Len(sfields(29)) > 5 Then
                        iPos = InStr(sfields(29), "-")
                        .GCZip = String.Format("{0:0000}", Mid(sfields(29), 1, iPos - 1)) & Mid(sfields(29), iPos)
                    Else
                        .GCZip = String.Format("{0:0000}", sfields(29))
                    End If

                    .StartDate = ImportUtils.GetDate(sfields(8))
                    .PONum = sfields(9)
                    .GCPhone1 = ImportUtils.GetPhoneNo(sfields(30))
                    .GCPhone2 = ImportUtils.GetPhoneNo(sfields(31))
                    .GenId = -1
                End If

                .OwnrName = sfields(32)
                .OwnrAdd1 = sfields(33)
                .OwnrAdd2 = sfields(34)
                .OwnrCity = sfields(35)
                .OwnrState = sfields(36)

                If Len(sfields(37)) > 5 Then
                    iPos = InStr(sfields(6), "-")
                    .OwnrZip = String.Format("{0:0000}", Mid(sfields(37), 1, iPos - 1)) & Mid(sfields(37), iPos)
                Else
                    .OwnrZip = String.Format("{0:0000}", sfields(37))
                End If

                .OwnrPhone1 = ImportUtils.GetPhoneNo(sfields(38))

                ' If lender addr1 is not blank
                If Trim(sfields(48)) <> "" Then
                    .BondNum = sfields(39)
                    .LenderName = sfields(47)
                    .LenderAdd1 = sfields(48)
                    .LenderAdd2 = sfields(49)
                    .LenderCity = sfields(50)
                    .LenderState = sfields(51)

                    If Len(sfields(52)) > 5 Then
                        iPos = InStr(sfields(52), "-")
                        .LenderZip = String.Format("{0:0000}", Mid(sfields(52), 1, iPos - 1)) & Mid(sfields(52), iPos)
                    Else
                        .LenderZip = String.Format("{0:0000}", sfields(52))
                    End If

                    .LenderPhone1 = ImportUtils.GetPhoneNo(sfields(53))
                End If

                ' If bond Addr1 is not blank
                If sfields(41) <> "" And sfields(41) <> Nothing Then
                    .LenderName = sfields(40)
                    .LenderAdd1 = sfields(41)
                    .LenderAdd2 = sfields(42)
                    .LenderCity = sfields(43)
                    .LenderState = sfields(44)

                    If Len(sfields(45)) > 5 Then
                        iPos = InStr(sfields(45), "-")
                        .LenderZip = String.Format("{0:0000}", Mid(sfields(45), 1, iPos - 1)) & Mid(sfields(45), iPos)
                    Else
                        .LenderZip = String.Format("{0:0000}", sfields(45))
                    End If
                    .LenderPhone1 = ImportUtils.GetPhoneNo(sfields(46))
                End If

                .SpecialInstruction = sfields(54)

                .PublicJob = False
                .FederalJob = False
                .ResidentialBox = False
                .PrivateJob = False

                .PrelimBox = False
                .VerifyJob = False
                .MechanicLien = False
                .TitleVerifiedBox = False
                .PrelimASIS = False

                If sfields(55) = "T" Then
                    .PublicJob = True
                End If
                If sfields(56) = "T" Then
                    .FederalJob = True
                End If
                If sfields(57) = "T" Then
                    .ResidentialBox = True
                End If
                If sfields(58) = "T" Then
                    .JobActionId = 37
                    .VerifyJob = True
                End If
                If sfields(59) = "T" Then
                    .JobActionId = 39
                    .MechanicLien = True
                End If
                If sfields(60) = "T" Then
                    .JobActionId = 38
                    .TitleVerifiedBox = True
                End If
                If sfields(64) = "T" Then
                    .JobActionId = 44
                    .PrelimASIS = True
                End If

                If .VerifyJob = False And .MechanicLien = False And .TitleVerifiedBox = False And .PrelimASIS = False Then
                    .JobActionId = 36
                    .PrelimBox = True
                End If

                .BranchNum = sfields(61)
                .RANum = sfields(62)
                .SubmittedByUserId = auser.Id
                .BatchId = abatchid
                .SubmittedOn = Now()

                .PlacementSource = "EDI"
            End With

            Return True


        End Function

        Public Shared Function ParseEliteRoofingImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 54 Or Len(sfields(0)) < 1 Then
                Return False
            End If


            With abatchjob
                .JobNum = sfields(4)
                '           .JobName = sfields(1)
                .JobAdd1 = sfields(14)
                .JobAdd2 = sfields(15)
                If .JobAdd1 = Nothing And .JobAdd2 <> Nothing Then
                    .JobAdd1 = .JobAdd2
                    .JobAdd2 = ""
                End If
                .JobCity = sfields(16)
                .JobState = sfields(17)
                .JobZip = String.Format("{0:0000}", sfields(18))

                .EstBalance = sfields(2)
                .StartDate = ImportUtils.GetDate(sfields(3))
                .PONum = sfields(5)
                .APNNum = sfields(19)
                '.CustJobNum = sfields(10)

                .CustRefNum = sfields(0)
                .CustName = sfields(1)
                '.CustContactName = sfields(14)
                .CustAdd1 = sfields(7)
                .CustAdd2 = sfields(8)
                If .CustAdd1 = Nothing And .CustAdd2 <> Nothing Then
                    .CustAdd1 = .CustAdd2
                    .CustAdd2 = ""
                End If
                .CustCity = sfields(9)
                .CustState = sfields(10)
                .CustZip = String.Format("{0:0000}", sfields(11))

                .CustPhone1 = ImportUtils.GetPhoneNo(sfields(12))
                .CustFax = ImportUtils.GetPhoneNo(sfields(13))
                .CustId = -1

                '.GCRefNum = sfields(22)
                .GCName = sfields(26)
                .GCAdd1 = sfields(27)
                .GCAdd2 = sfields(28)
                .GCCity = sfields(29)
                .GCState = sfields(30)
                .GCZip = String.Format("{0:0000}", sfields(31))
                .GCPhone1 = ImportUtils.GetPhoneNo(sfields(32))
                .GCEmail = sfields(33)
                .GenId = -1

                .OwnrName = sfields(20)
                .OwnrAdd1 = sfields(21)
                .OwnrAdd2 = sfields(22)
                .OwnrCity = sfields(23)
                .OwnrState = sfields(24)
                .OwnrZip = String.Format("{0:0000}", sfields(25))


                ' If lender addr1 is not blank
                If Trim(sfields(38)) <> "" Then
                    .BondNum = sfields(46)
                    .LenderName = sfields(38)
                    .LenderAdd1 = sfields(39)
                    .LenderAdd2 = sfields(40)
                    .LenderCity = sfields(41)
                    .LenderState = sfields(42)
                    .LenderZip = String.Format("{0:0000}", sfields(52))
                    .LenderPhone1 = ImportUtils.GetPhoneNo(sfields(44))
                    .LenderFax = ImportUtils.GetPhoneNo(sfields(45))
                End If

                ' If Other Legal Party type is not null
                If Trim(sfields(47)) <> "" Then
                    .FilterKey = sfields(47)    'temporarily use BatchJob.FilterKey to handle LegalPartyType
                    .DesigneeName = sfields(48)
                    .DesigneeAdd1 = sfields(49)
                    .DesigneeAdd2 = sfields(50)
                    .DesigneeCity = sfields(51)
                    .DesigneeState = sfields(52)
                    .DesigneeZip = String.Format("{0:0000}", sfields(53))
                End If

                '            .SpecialInstruction = sfields(54)

                If sfields(47).ToUpper = "S" Then
                    .PublicJob = True
                Else
                    .PublicJob = False
                End If

                .FederalJob = False
                .ResidentialBox = False
                .PrivateJob = False

                .PrelimBox = False
                .VerifyJob = False
                .MechanicLien = False
                .TitleVerifiedBox = False
                .PrelimASIS = False

                If sfields(37).ToUpper = "YES" Then
                    .JobActionId = 37
                    .VerifyJob = True
                End If

                If sfields(36).ToUpper = "YES" Then
                    .JobActionId = 38
                    .TitleVerifiedBox = True
                End If
                If sfields(34).ToUpper = "YES" Then
                    .JobActionId = 44
                    .PrelimASIS = True
                End If

                If .VerifyJob = False And .TitleVerifiedBox = False And .PrelimASIS = False Then
                    .JobActionId = 36
                    .PrelimBox = True
                End If

                .BranchNum = sfields(6)
                .SubmittedByUserId = auser.Id
                .BatchId = abatchid
                .SubmittedOn = Now()
                .ClientId = aclientid

                .PlacementSource = "EDI"

            End With
            Return True

        End Function

        Public Shared Function ParseIESImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 96 Or Len(Trim(sfields(12))) < 1 Then
                Return False
            End If

            With abatchjob

                Dim sSql As String = "Select * from vwClientInterfaceReference where InterfaceName = 'IES' and InputField = '" & Trim(sfields(3)) & "'"
                Dim myview As DAL.COMMON.TableView = DBO.GetTableView(sSql)
                If myview.Count > 0 Then
                    .ClientId = myview.RowItem("ClientId")
                End If

                .JobNum = Trim(sfields(12))
                .JobName = Trim(sfields(13))
                .JobAdd1 = Trim(sfields(14))
                .JobAdd2 = Trim(sfields(15))
                If .JobAdd1 = Nothing And .JobAdd2 <> Nothing Then
                    .JobAdd1 = .JobAdd2
                    .JobAdd2 = ""
                End If
                .JobCity = Trim(sfields(16))
                .JobState = Trim(sfields(17))

                Dim iPos As Integer

                If Len(Trim(sfields(18))) > 5 Then
                    iPos = InStr(sfields(18), "-")
                    .JobZip = String.Format("{0:0000}", Mid(sfields(18), 1, iPos - 1)) & Mid(Trim(sfields(18)), iPos)
                Else
                    .JobZip = String.Format("{0:0000}", Trim(sfields(18)))
                End If

                .EstBalance = sfields(20)
                .StartDate = ImportUtils.GetDate(sfields(2))

                .CustRefNum = Trim(sfields(5))
                .CustName = Trim(sfields(6))
                .CustAdd1 = Trim(sfields(7))
                .CustAdd2 = Trim(sfields(8))
                If .CustAdd1 = Nothing And .CustAdd2 <> Nothing Then
                    .CustAdd1 = .CustAdd2
                    .CustAdd2 = ""
                End If
                .CustCity = Trim(sfields(9))
                .CustState = Trim(sfields(10))
                If Len(Trim(sfields(11))) > 5 Then
                    iPos = InStr(Trim(sfields(11)), "-")
                    .CustZip = String.Format("{0:0000}", Mid(sfields(11), 1, iPos - 1)) & Mid(Trim(sfields(11)), iPos)
                Else
                    .CustZip = String.Format("{0:0000}", Trim(sfields(11)))
                End If

                .CustId = -1

                If Len(Trim(sfields(36))) > 3 Then
                    .GCName = Trim(sfields(36))
                    .GCAdd1 = Trim(sfields(37))
                    .GCAdd2 = Trim(sfields(38))
                    .GCCity = Trim(sfields(39))
                    .GCState = Trim(sfields(40))
                    If Len(Trim(sfields(41))) > 5 Then
                        iPos = InStr(sfields(41), "-")
                        .GCZip = String.Format("{0:0000}", Mid(sfields(41), 1, iPos - 1)) & Mid(Trim(sfields(41)), iPos)
                    Else
                        .GCZip = String.Format("{0:0000}", Trim(sfields(41)))
                    End If

                    .GenId = -1
                End If

                If Len(Trim(sfields(30))) > 3 Then
                    .OwnrName = Trim(sfields(30))
                    .OwnrAdd1 = Trim(sfields(31))
                    .OwnrAdd2 = Trim(sfields(32))
                    .OwnrCity = Trim(sfields(33))
                    .OwnrState = Trim(sfields(34))
                    If Len(Trim(sfields(35))) > 5 Then
                        iPos = InStr(sfields(35), "-")
                        .OwnrZip = String.Format("{0:0000}", Mid(sfields(35), 1, iPos - 1)) & Mid(Trim(sfields(35)), iPos)
                    Else
                        .OwnrZip = String.Format("{0:0000}", Trim(sfields(35)))
                    End If
                End If

                ' If lender addr1 is not blank
                If Trim(sfields(24)) <> "" And Trim(sfields(24)) <> "N/A" And Trim(sfields(24)) <> "NONE" Then
                    .LenderName = Trim(sfields(24))
                    .LenderAdd1 = Trim(sfields(25))
                    .LenderAdd2 = Trim(sfields(26))
                    .LenderCity = Trim(sfields(27))
                    .LenderState = Trim(sfields(28))
                    If Len(Trim(sfields(29))) > 5 Then
                        iPos = InStr(sfields(29), "-")
                        .LenderZip = String.Format("{0:0000}", Mid(sfields(29), 1, iPos - 1)) & Mid(Trim(sfields(29)), iPos)
                    Else
                        .LenderZip = String.Format("{0:0000}", Trim(sfields(29)))
                    End If
                End If


                .PublicJob = False
                .FederalJob = False
                .ResidentialBox = False
                .PrivateJob = False

                .PrelimBox = False
                .VerifyJob = False
                .MechanicLien = False
                .TitleVerifiedBox = False
                .PrelimASIS = False


                If Trim(sfields(23)) = "A" Then
                    .JobActionId = 44
                    .PrelimASIS = True
                End If

                If .VerifyJob = False And .TitleVerifiedBox = False And .PrelimASIS = False Then
                    .JobActionId = 36
                    .PrelimBox = True
                End If

                .BranchNum = Trim(sfields(3))
                .SubmittedByUserId = auser.Id
                .BatchId = abatchid
                .SubmittedOn = Now()


                .PlacementSource = "EDI"

            End With
            Return True

        End Function

        Public Shared Function ParseDMGImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 65 Or Len(sfields(13)) < 1 Then
                Return False
            End If
            If Len(sfields(13)) < 2 Then
                Return False
            End If

            With abatchjob
                .JobNum = sfields(0)
                .JobName = sfields(1)
                .JobAdd1 = sfields(2)
                .JobAdd2 = sfields(3)
                If .JobAdd1 = Nothing And .JobAdd2 <> Nothing Then
                    .JobAdd1 = .JobAdd2
                    .JobAdd2 = ""
                End If
                .JobCity = sfields(4)
                .JobState = sfields(5)
                .JobZip = sfields(6)
                .EstBalance = sfields(7)
                .StartDate = ImportUtils.GetDate(sfields(8))
                .PONum = sfields(9)

                .CustJobNum = sfields(10)

                .CustRefNum = sfields(12)
                .CustName = sfields(13)
                .CustContactName = sfields(14)
                .CustAdd1 = sfields(15)
                .CustAdd2 = sfields(16)
                If .CustAdd1 = Nothing And .CustAdd2 <> Nothing Then
                    .CustAdd1 = .CustAdd2
                    .CustAdd2 = ""
                End If
                .CustCity = sfields(17)
                .CustState = sfields(18)
                .CustZip = sfields(19)
                .CustPhone1 = ImportUtils.GetPhoneNo(sfields(20))
                .CustPhone2 = ImportUtils.GetPhoneNo(sfields(21))
                .CustId = -1

                .GCRefNum = sfields(22)
                .GCName = sfields(23)
                If .GCName = "SAME AS CUSTOMER" Then
                    .GCName = .CustName
                    .GCContactName = .CustContactName
                    .GCAdd1 = .CustAdd1
                    .GCAdd2 = .CustAdd2
                    .GCCity = .CustCity
                    .GCState = .CustState
                    .GCZip = .CustZip
                    .GCPhone1 = .CustPhone1
                    .GCPhone2 = .CustPhone2
                    .GenId = -1
                Else
                    .GCContactName = sfields(24)
                    .GCAdd1 = sfields(25)
                    .GCAdd2 = sfields(26)
                    .GCCity = sfields(27)
                    .GCState = sfields(28)
                    .GCZip = sfields(29)
                    .GCPhone1 = ImportUtils.GetPhoneNo(sfields(30))
                    .GCPhone2 = ImportUtils.GetPhoneNo(sfields(31))
                    .GenId = -1
                End If

                .OwnrName = sfields(32)
                .OwnrAdd1 = sfields(33)
                .OwnrAdd2 = sfields(34)
                .OwnrCity = sfields(35)
                .OwnrState = sfields(36)
                .OwnrZip = sfields(37)
                .OwnrPhone1 = ImportUtils.GetPhoneNo(sfields(38))

                .BondNum = sfields(39)
                .LenderName = sfields(47)
                .LenderAdd1 = sfields(48)
                .LenderAdd2 = sfields(49)
                .LenderCity = sfields(50)
                .LenderState = sfields(51)
                .LenderZip = sfields(52)
                .LenderPhone1 = ImportUtils.GetPhoneNo(sfields(53))

                If sfields(40) <> "" And sfields(40) <> Nothing Then
                    .LenderName = sfields(40)
                    .LenderAdd1 = sfields(41)
                    .LenderAdd2 = sfields(42)
                    .LenderCity = sfields(43)
                    .LenderState = sfields(44)
                    .LenderZip = sfields(45)
                    .LenderPhone1 = ImportUtils.GetPhoneNo(sfields(46))
                End If

                .SpecialInstruction = sfields(54)

                .PublicJob = False
                .FederalJob = False
                .ResidentialBox = False
                .PrivateJob = False

                .PrelimBox = False
                .VerifyJob = False
                .MechanicLien = False
                .TitleVerifiedBox = False
                .PrelimASIS = False

                If sfields(55) = "T" Then
                    .PublicJob = True
                End If
                If sfields(56) = "T" Then
                    .FederalJob = True
                End If
                If sfields(57) = "T" Then
                    .ResidentialBox = True
                End If
                If sfields(58) = "T" Then
                    .JobActionId = 37
                    .VerifyJob = True
                End If
                If sfields(59) = "T" Then
                    .JobActionId = 39
                    .MechanicLien = True
                End If
                If sfields(60) = "T" Then
                    .JobActionId = 38
                    .TitleVerifiedBox = True
                End If
                If sfields(64) = "T" Then
                    .JobActionId = 44
                    .PrelimASIS = True
                End If

                If .VerifyJob = False And .MechanicLien = False And .TitleVerifiedBox = False And .PrelimASIS = False Then
                    .JobActionId = 36
                    .PrelimBox = True
                End If

                .BranchNum = sfields(61)
                .RANum = sfields(62)
                .SubmittedByUserId = auser.Id
                .BatchId = abatchid
                .SubmittedOn = Now()
                .ClientId = aclientid

                .PlacementSource = "EDI"

                ' Other owner
                If sfields(65) <> "" And sfields(65) <> Nothing Then
                    .DesigneeName = sfields(65)
                    .DesigneeAdd1 = sfields(66)
                    .DesigneeAdd2 = sfields(67)
                    .DesigneeCity = sfields(68)
                    .DesigneeState = sfields(69)
                    .DesigneeZip = sfields(70)
                End If

                .EndDate = ImportUtils.GetDate(sfields(71))

            End With
            Return True

        End Function
        Public Shared Function ParseCTL770ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 22 Or Len(sfields(12)) < 1 Then
                Return False
            End If

            With abatchjob
                .JobNum = sfields(0)
                .JobName = sfields(1)
                .JobAdd1 = sfields(2)
                .JobAdd2 = sfields(3)
                If .JobAdd1 = Nothing And .JobAdd2 <> Nothing Then
                    .JobAdd1 = .JobAdd2
                    .JobAdd2 = ""
                End If
                .JobCity = sfields(4)
                .JobState = sfields(5)
                .JobZip = sfields(6)
                .EstBalance = sfields(7)
                .StartDate = ImportUtils.GetDate(sfields(8))
                .PONum = sfields(9)

                .CustJobNum = sfields(10)

                .CustRefNum = sfields(12)
                .CustName = sfields(13)
                .CustContactName = sfields(14)
                .CustAdd1 = sfields(15)
                .CustAdd2 = sfields(16)
                If .CustAdd1 = Nothing And .CustAdd2 <> Nothing Then
                    .CustAdd1 = .CustAdd2
                    .CustAdd2 = ""
                End If
                .CustCity = sfields(17)
                .CustState = sfields(18)
                .CustZip = sfields(19)
                .CustPhone1 = ImportUtils.GetPhoneNo(sfields(20))
                .CustPhone2 = ImportUtils.GetPhoneNo(sfields(21))
                .CustId = -1

                .PrelimBox = True
                .VerifyJob = False

                If sfields(22) = "x" Then
                    .JobActionId = 37
                    .VerifyJob = True
                End If

                If .VerifyJob = False Then
                    .JobActionId = 36
                    .PrelimBox = True
                End If

                .SubmittedByUserId = auser.Id
                .BatchId = abatchid
                .SubmittedOn = Now()
                .ClientId = aclientid

                .PlacementSource = "EDI"

            End With
            Return True

        End Function

        Public Shared Function ParseCER925ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 7 Then
                Return False
            End If
            If Len(sfields(3)) < 2 Then
                Return False
            End If
            If Trim(sfields(3)) <> "" Then

                With abatchjob
                    .ClientId = aclientid

                    .CustRefNum = sfields(1)
                    .CustName = sfields(0)

                    Dim mysproc As New SPROCS.uspbo_LiensDataEntry_GetCustomer_SearchCustByRef
                    mysproc.ClientId = .ClientId
                    mysproc.RefNum = .CustRefNum
                    mysproc.CustName = .CustName
                    Dim myview As DAL.COMMON.TableView = DBO.GetTableView(mysproc)
                    If myview.Count > 0 Then
                        .CustId = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.CustId)
                        .CustAdd1 = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.AddressLine1)
                        .CustAdd2 = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.AddressLine2)
                        .CustCity = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.City)
                        .CustState = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.State)
                        .CustZip = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.PostalCode)
                        .CustPhone1 = myview.RowItem(CRFDB.TABLES.ClientCustomer.ColumnNames.Telephone1)
                    End If

                    '   1201 VOYAGER DR, LIVERMORE  
                    .JobName = sfields(5)
                    If Len(sfields(3)) <> 0 Then
                        Dim iPos As Int16 = InStr(1, sfields(3), ",")
                        .JobAdd1 = Mid(sfields(3), 1, iPos - 1)
                        .JobCity = Trim(Mid(sfields(3), iPos + 1))
                    End If

                    .JobState = "CA"

                    .PONum = sfields(4)
                    .StartDate = ImportUtils.GetDate(sfields(6))
                    .EstBalance = sfields(7)
                    .GenId = -1

                    .PrivateJob = True
                    .PrelimBox = True
                    .SubmittedByUserId = auser.Id
                    .BatchId = abatchid
                    .SubmittedOn = Now()

                    .PlacementSource = "EDI"

                End With
            End If
            Return True

        End Function
        Public Shared Function ParseCEDImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 24 Then
                Return False
            End If
            If Len(sfields(4)) < 1 Then
                Return False
            End If

            With abatchjob
                .ClientId = aclientid
                .BranchNum = sfields(2)
                '.CustRefNum = sfields(1)
                .CustName = sfields(3)
                .CustAdd1 = sfields(4)
                .CustCity = sfields(5)
                .CustState = sfields(6)
                .CustZip = sfields(7)
                .CustEmail = sfields(8)
                .CustPhone1 = ImportUtils.GetPhoneNo(sfields(9))
                .CustFax = ImportUtils.GetPhoneNo(sfields(10))
                .CustId = -1

                .JobNum = sfields(23)
                .JobName = sfields(11)
                .JobAdd1 = sfields(12)
                .JobCity = sfields(13)
                .JobState = sfields(14)
                .JobZip = sfields(15)

                .GenId = -1
                .GCName = sfields(16)
                .GCAdd1 = sfields(17)
                .GCCity = sfields(18)
                .GCState = sfields(19)
                .GCZip = sfields(20)

                .EstBalance = sfields(21)

                .StartDate = ImportUtils.GetDate(sfields(22))

                .PrelimBox = True
                .PrivateJob = True

                .SubmittedByUserId = auser.Id
                .BatchId = abatchid
                .SubmittedOn = Now()

                .PlacementSource = "EDI"

            End With
            Return True

        End Function

        Public Shared Function ParseTTE248ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 21 Then
                Return False
            End If
            If Len(sfields(1)) < 1 Then
                Return False
            End If

            With abatchjob
                .ClientId = aclientid
                .BranchNum = sfields(0)
                .CustRefNum = sfields(1)
                .CustName = sfields(2)
                .CustContactName = sfields(3)
                .CustAdd1 = sfields(4)
                .CustCity = sfields(5)
                .CustState = sfields(6)
                .CustZip = sfields(7)
                '.CustEmail = sfields(8)
                .CustPhone1 = ImportUtils.GetPhoneNo(sfields(8))
                .CustFax = ImportUtils.GetPhoneNo(sfields(9))
                .CustId = -1

                .JobNum = sfields(15)
                '.JobName = sfields(10)
                Dim iPos As Int16 = InStr(1, sfields(10), ",")
                Dim iPos2 As Int16 = InStr(iPos + 1, sfields(10), ",")
                .JobAdd1 = Mid(sfields(10), 1, iPos - 1)
                If iPos2 = 0 Then
                    .JobCity = Trim(Mid(sfields(10), iPos + 1))
                Else
                    .JobCity = Trim(Mid(sfields(10), iPos + 1, iPos2 - 1))
                End If

                .JobState = sfields(11)
                '.JobZip = sfields(15)

                .EstBalance = sfields(21)
                .PONum = sfields(19)
                .StartDate = ImportUtils.GetDate(sfields(12))

                .PrelimBox = True
                .PrivateJob = True

                .SubmittedByUserId = auser.Id
                .BatchId = abatchid
                .SubmittedOn = Now()

                .PlacementSource = "EDI"

            End With
            Return True

        End Function

        Public Shared Function ParseTPR567ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 20 Then
                Return False
            End If
            If Len(sfields(0)) < 1 Then
                Return False
            End If

            With abatchjob
                .ClientId = aclientid
                .JobNum = sfields(1)
                .RANum = sfields(0)

                .CustRefNum = sfields(2)
                .CustName = sfields(3)
                .CustAdd1 = sfields(4)
                .CustCity = sfields(5)
                .CustState = sfields(6)
                .CustZip = sfields(7)
                .CustPhone1 = ImportUtils.GetPhoneNo(sfields(13))
                .CustId = -1

                .StartDate = ImportUtils.GetDate(sfields(10))

                .JobName = sfields(14)

                Dim iPos As Int16 = InStr(1, sfields(15), ",")
                .JobAdd1 = Mid(sfields(15), 1, iPos - 1)
                If iPos > 0 Then
                    .JobCity = Mid(sfields(15), iPos + 2)

                End If

                .JobState = sfields(17)
                .JobZip = sfields(18)

                .PONum = sfields(8)
                .BranchNum = sfields(11)
                .CustJobNum = sfields(12)
                .EstBalance = Trim(sfields(20))

                .PrelimBox = True
                .PrivateJob = True

                .SubmittedByUserId = auser.Id
                .BatchId = abatchid
                .SubmittedOn = Now()

                .PlacementSource = "EDI"

            End With
            Return True

        End Function
        Public Shared Function HandleIESOLP(ByRef abatchjobid As Integer, ByVal aline As String) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")

            Dim iPos As Integer
            Dim myBatchJobLPS As TABLES.BatchJobLegalParties

            '   GC2
            If Len(Trim(sfields(43))) > 1 Then
                myBatchJobLPS = New TABLES.BatchJobLegalParties
                myBatchJobLPS.BatchJobId = abatchjobid
                myBatchJobLPS.TypeCode = "GC"

                myBatchJobLPS.AddressName = Trim(sfields(43))
                myBatchJobLPS.AddressLine1 = Trim(sfields(44))
                myBatchJobLPS.AddressLine2 = Trim(sfields(45))
                myBatchJobLPS.City = Trim(sfields(46))
                myBatchJobLPS.State = Trim(sfields(47))
                If Len(Trim(sfields(48))) > 5 Then
                    iPos = InStr(sfields(48), "-")
                    myBatchJobLPS.PostalCode = String.Format("{0:0000}", Mid(sfields(48), 1, iPos - 1)) & Mid(Trim(sfields(48)), iPos)
                Else
                    myBatchJobLPS.PostalCode = String.Format("{0:0000}", Trim(sfields(48)))
                End If

                myBatchJobLPS.IsPrimary = 0
                BLL.Providers.DBO.Provider.Create(myBatchJobLPS)
            End If
            '   GC3
            If Len(Trim(sfields(61))) > 1 Then
                myBatchJobLPS = New TABLES.BatchJobLegalParties
                myBatchJobLPS.BatchJobId = abatchjobid
                myBatchJobLPS.TypeCode = "GC"

                myBatchJobLPS.AddressName = Trim(sfields(61))
                myBatchJobLPS.AddressLine1 = Trim(sfields(62))
                myBatchJobLPS.AddressLine2 = Trim(sfields(63))
                myBatchJobLPS.City = Trim(sfields(64))
                myBatchJobLPS.State = Trim(sfields(65))
                If Len(Trim(sfields(66))) > 5 Then
                    iPos = InStr(sfields(66), "-")
                    myBatchJobLPS.PostalCode = String.Format("{0:0000}", Mid(sfields(66), 1, iPos - 1)) & Mid(Trim(sfields(66)), iPos)
                Else
                    myBatchJobLPS.PostalCode = String.Format("{0:0000}", Trim(sfields(66)))
                End If

                myBatchJobLPS.IsPrimary = 0
                BLL.Providers.DBO.Provider.Create(myBatchJobLPS)
            End If
            '   GC4
            If Len(Trim(sfields(79))) > 1 Then
                myBatchJobLPS = New TABLES.BatchJobLegalParties
                myBatchJobLPS.BatchJobId = abatchjobid
                myBatchJobLPS.TypeCode = "GC"

                myBatchJobLPS.AddressName = Trim(sfields(79))
                myBatchJobLPS.AddressLine1 = Trim(sfields(80))
                myBatchJobLPS.AddressLine2 = Trim(sfields(81))
                myBatchJobLPS.City = Trim(sfields(82))
                myBatchJobLPS.State = Trim(sfields(83))
                If Len(Trim(sfields(84))) > 5 Then
                    iPos = InStr(sfields(84), "-")
                    myBatchJobLPS.PostalCode = String.Format("{0:0000}", Mid(sfields(84), 1, iPos - 1)) & Mid(Trim(sfields(84)), iPos)
                Else
                    myBatchJobLPS.PostalCode = String.Format("{0:0000}", Trim(sfields(84)))
                End If

                myBatchJobLPS.IsPrimary = 0
                BLL.Providers.DBO.Provider.Create(myBatchJobLPS)
            End If
            '   OW2
            If Len(Trim(sfields(49))) > 1 Then
                myBatchJobLPS = New TABLES.BatchJobLegalParties
                myBatchJobLPS.BatchJobId = abatchjobid
                myBatchJobLPS.TypeCode = "OW"

                myBatchJobLPS.AddressName = Trim(sfields(49))
                myBatchJobLPS.AddressLine1 = Trim(sfields(50))
                myBatchJobLPS.AddressLine2 = Trim(sfields(51))
                myBatchJobLPS.City = Trim(sfields(52))
                myBatchJobLPS.State = Trim(sfields(53))
                If Len(Trim(sfields(54))) > 5 Then
                    iPos = InStr(sfields(54), "-")
                    myBatchJobLPS.PostalCode = String.Format("{0:0000}", Mid(sfields(54), 1, iPos - 1)) & Mid(Trim(sfields(54)), iPos)
                Else
                    myBatchJobLPS.PostalCode = String.Format("{0:0000}", Trim(sfields(54)))
                End If

                myBatchJobLPS.IsPrimary = 0
                BLL.Providers.DBO.Provider.Create(myBatchJobLPS)
            End If
            '   OW3
            If Len(Trim(sfields(67))) > 1 Then
                myBatchJobLPS = New TABLES.BatchJobLegalParties
                myBatchJobLPS.BatchJobId = abatchjobid
                myBatchJobLPS.TypeCode = "OW"

                myBatchJobLPS.AddressName = Trim(sfields(67))
                myBatchJobLPS.AddressLine1 = Trim(sfields(68))
                myBatchJobLPS.AddressLine2 = Trim(sfields(69))
                myBatchJobLPS.City = Trim(sfields(70))
                myBatchJobLPS.State = Trim(sfields(71))
                If Len(Trim(sfields(72))) > 5 Then
                    iPos = InStr(sfields(72), "-")
                    myBatchJobLPS.PostalCode = String.Format("{0:0000}", Mid(sfields(72), 1, iPos - 1)) & Mid(Trim(sfields(72)), iPos)
                Else
                    myBatchJobLPS.PostalCode = String.Format("{0:0000}", Trim(sfields(72)))
                End If

                myBatchJobLPS.IsPrimary = 0
                BLL.Providers.DBO.Provider.Create(myBatchJobLPS)
            End If
            '   OW4
            If Len(Trim(sfields(85))) > 1 Then
                myBatchJobLPS = New TABLES.BatchJobLegalParties
                myBatchJobLPS.BatchJobId = abatchjobid
                myBatchJobLPS.TypeCode = "OW"

                myBatchJobLPS.AddressName = Trim(sfields(85))
                myBatchJobLPS.AddressLine1 = Trim(sfields(86))
                myBatchJobLPS.AddressLine2 = Trim(sfields(87))
                myBatchJobLPS.City = Trim(sfields(88))
                myBatchJobLPS.State = Trim(sfields(89))
                If Len(Trim(sfields(90))) > 5 Then
                    iPos = InStr(sfields(90), "-")
                    myBatchJobLPS.PostalCode = String.Format("{0:0000}", Mid(sfields(90), 1, iPos - 1)) & Mid(Trim(sfields(90)), iPos)
                Else
                    myBatchJobLPS.PostalCode = String.Format("{0:0000}", Trim(sfields(90)))
                End If

                myBatchJobLPS.IsPrimary = 0
                BLL.Providers.DBO.Provider.Create(myBatchJobLPS)
            End If
            '   LE2
            If Len(Trim(sfields(55))) > 1 Then
                myBatchJobLPS = New TABLES.BatchJobLegalParties
                myBatchJobLPS.BatchJobId = abatchjobid
                myBatchJobLPS.TypeCode = "LE"

                myBatchJobLPS.AddressName = Trim(sfields(55))
                myBatchJobLPS.AddressLine1 = Trim(sfields(56))
                myBatchJobLPS.AddressLine2 = Trim(sfields(57))
                myBatchJobLPS.City = Trim(sfields(58))
                myBatchJobLPS.State = Trim(sfields(59))
                If Len(Trim(sfields(60))) > 5 Then
                    iPos = InStr(sfields(60), "-")
                    myBatchJobLPS.PostalCode = String.Format("{0:0000}", Mid(sfields(60), 1, iPos - 1)) & Mid(Trim(sfields(60)), iPos)
                Else
                    myBatchJobLPS.PostalCode = String.Format("{0:0000}", Trim(sfields(60)))
                End If

                myBatchJobLPS.IsPrimary = 0
                BLL.Providers.DBO.Provider.Create(myBatchJobLPS)
            End If
            '   LE3
            If Len(Trim(sfields(73))) > 1 Then
                myBatchJobLPS = New TABLES.BatchJobLegalParties
                myBatchJobLPS.BatchJobId = abatchjobid
                myBatchJobLPS.TypeCode = "LE"

                myBatchJobLPS.AddressName = Trim(sfields(73))
                myBatchJobLPS.AddressLine1 = Trim(sfields(74))
                myBatchJobLPS.AddressLine2 = Trim(sfields(75))
                myBatchJobLPS.City = Trim(sfields(76))
                myBatchJobLPS.State = Trim(sfields(77))
                If Len(Trim(sfields(78))) > 5 Then
                    iPos = InStr(sfields(78), "-")
                    myBatchJobLPS.PostalCode = String.Format("{0:0000}", Mid(sfields(78), 1, iPos - 1)) & Mid(Trim(sfields(78)), iPos)
                Else
                    myBatchJobLPS.PostalCode = String.Format("{0:0000}", Trim(sfields(78)))
                End If

                myBatchJobLPS.IsPrimary = 0
                BLL.Providers.DBO.Provider.Create(myBatchJobLPS)
            End If
            '   LE4
            If Len(Trim(sfields(91))) > 1 Then
                myBatchJobLPS = New TABLES.BatchJobLegalParties
                myBatchJobLPS.BatchJobId = abatchjobid
                myBatchJobLPS.TypeCode = "LE"

                myBatchJobLPS.AddressName = Trim(sfields(91))
                myBatchJobLPS.AddressLine1 = Trim(sfields(92))
                myBatchJobLPS.AddressLine2 = Trim(sfields(93))
                myBatchJobLPS.City = Trim(sfields(94))
                myBatchJobLPS.State = Trim(sfields(95))
                If Len(Trim(sfields(96))) > 5 Then
                    iPos = InStr(sfields(96), "-")
                    myBatchJobLPS.PostalCode = String.Format("{0:0000}", Mid(sfields(96), 1, iPos - 1)) & Mid(Trim(sfields(96)), iPos)
                Else
                    myBatchJobLPS.PostalCode = String.Format("{0:0000}", Trim(sfields(96)))
                End If

                myBatchJobLPS.IsPrimary = 0
                BLL.Providers.DBO.Provider.Create(myBatchJobLPS)
            End If
            Return True


        End Function
        Public Shared Function IsExistingJob(ByVal aclientid As Integer, ByVal aJobNum As String) As Integer
            Dim sSql As String = "Select Id From Job j with (nolock) Where Clientid = " & aclientid & " And JobNum = '" & aJobNum & "'"
            Dim myview As DAL.COMMON.TableView = DBO.GetTableView(sSql)
            If myview.Count > 0 Then
                Return myview.RowItem("Id")
            Else
                Return 0
            End If
        End Function
        Public Shared Function GetBoolean(ByVal aflag As Object) As Boolean
            Try
                Return Boolean.Parse(aflag)
            Catch ex As Exception
                Return False
            End Try
        End Function
        Public Shared Function GetDecimal(ByVal aamt As Object) As Decimal
            Try
                Return Decimal.Parse(aamt)
            Catch ex As Exception
                Return 0
            End Try
        End Function
        Public Shared Function GetPhoneNo(ByVal avalue As Object) As String
            Try
                Return HDS.WINLIB.Common.UnFormat.PhoneNo(avalue)
            Catch ex As Exception
                Return ""
            End Try
        End Function
        Public Shared Function GetDate(ByVal adate As String) As Date
            Try
                If IsDate(adate) Then
                    Return Date.Parse(adate)
                Else
                    Return Date.MinValue
                End If
            Catch ex As Exception
                Return Date.MinValue
            End Try
        End Function
        Public Shared Function FormatPhoneNo(ByVal avalue As Object) As String
            Try
                Return HDS.WINLIB.Common.Format.PhoneNo(avalue)
            Catch ex As Exception
                Return ""
            End Try
        End Function
        Public Shared Function FormatDate(ByVal adate As Object) As String

            Try
                If IsDate(adate) And adate <> Date.MinValue Then
                    Dim s As String() = Strings.Split(Date.Parse(adate).ToShortDateString, "/")
                    Dim y As String = Right("00" & s(0), 2) & "/" & Right("00" & s(1), 2) & "/" & Right("00" & s(2), 2)
                    Return y
                Else
                    Return String.Empty
                End If
            Catch ex As Exception
                Return String.Empty
            End Try
        End Function
        Public Shared Function FormatDecimal(ByVal aamt As Object) As String

            Try
                Return Strings.FormatCurrency(Decimal.Parse(aamt), 2)
            Catch ex As Exception
                Return Strings.FormatCurrency(Decimal.Parse(0), 2)
            End Try
        End Function
        Public Shared Function StringParser(ByVal aline As String, ByVal adelimiter As String) As String()
            Dim fields As Array = Nothing
            Dim rawFields() As String = aline.Split(Convert.ToChar("|"))
            ' recombine any with quotes
            RecombineQuotedFields(rawFields, adelimiter)
            ' remove the extra elements
            ExtractNullArrayElements(rawFields, fields)
            Return fields

        End Function
        Public Shared Function CSVParser(ByVal aline As String, ByVal adelimiter As String) As String()
            Dim fields As Array = Nothing
            Dim rawFields() As String = aline.Split(Convert.ToChar(","))
            ' recombine any with quotes
            RecombineQuotedFields(rawFields, adelimiter)
            ' remove the extra elements
            ExtractNullArrayElements(rawFields, fields)
            Return fields

        End Function
        Private Shared Sub RecombineQuotedFields(ByRef fields() As String, ByVal adelimiter As String)
            Dim _quotechar As String = Strings.Chr(34)
            Dim _delimiter As String = adelimiter
            Dim firstChar As Char
            Dim lastChar As Char
            Dim maxFieldIndex As Int32 = fields.GetLength(0) - 1
            ' start seaching
            For x As Int32 = 0 To maxFieldIndex
                ' get the potential delimitters
                If fields(x).Length > 0 Then
                    firstChar = fields(x).Chars(0)
                    lastChar = fields(x).Chars(fields(x).Length - 1)
                Else
                    firstChar = Nothing
                    lastChar = Nothing
                End If
                ' start the comparisons
                If firstChar = _quotechar Then
                    ' we started with a valid quote character
                    If (firstChar = lastChar) Then
                        ' strip off the matched quotes
                        fields.SetValue(fields(x).Substring(1, fields(x).Length - 2), x)
                    Else
                        Dim startIndex As Int32 = x
                        Dim quoteChar As Char = firstChar
                        Do
                            ' skip to the next item
                            x += 1
                            ' get the new "endpoints"
                            firstChar = fields(x).Chars(0)
                            lastChar = fields(x).Chars(fields(x).Length - 1)
                            ' this field better not start with a quote
                            If firstChar = _quotechar And fields(x).Length > 1 Then
                                Throw New ApplicationException("There was an unclosed quotation mark.")
                            End If
                            ' recombine the items
                            fields.SetValue(String.Concat(fields(startIndex).ToString, _delimiter, fields(x).ToString), startIndex)
                            ' flush the unused array element
                            Array.Clear(fields, x, 1)
                        Loop Until (lastChar = quoteChar)
                        ' strip off the outer quotes
                        fields.SetValue(fields(startIndex).Substring(1, fields(startIndex).Length - 2), startIndex)
                    End If
                End If
            Next
        End Sub
        Private Shared Sub ExtractNullArrayElements(ByRef input() As String, ByRef output As Array)
            Dim x As Int32
            Dim maxInputIndex As Int32 = input.Length - 1
            Dim count As Int32 = 0
            Dim mark As Int32 = 0
            ' get the actual field count
            For x = 0 To maxInputIndex
                If Not input(x) Is Nothing Then count += 1
            Next
            ' resize the output array
            output = Array.CreateInstance(GetType(String), count)
            For x = 0 To maxInputIndex
                If Not (input(x) Is Nothing) Then
                    ' save the value and incriment the book mark
                    output.SetValue(input(x), mark)
                    mark += 1
                End If
            Next
        End Sub

        Public Shared Function ParseCEDSacramentoImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 68 Or Len(sfields(7)) < 1 Then
                Return False
            End If
            If Len(sfields(7)) < 2 Then
                Return False
            End If

            With abatchjob
                Dim sSql As String = "Select * from vwClientInterfaceReference where InterfaceName = 'CEDSacramento' and InputField = '" & sfields(7) & "'"
                Dim myview As DAL.COMMON.TableView = DBO.GetTableView(sSql)
                If myview.Count > 0 Then
                    .ClientId = myview.RowItem("ClientId")
                End If

                .CustRefNum = sfields(0)
                .CustName = sfields(1)
                .CustAdd1 = sfields(3)
                .CustCity = sfields(4)
                .CustState = sfields(5)
                .CustZip = sfields(6)
                .CustId = -1

                .BranchNum = sfields(7)
                .JobNum = sfields(8)
                .JobName = sfields(10)
                .JobAdd1 = sfields(11)
                .JobAdd2 = sfields(12)
                If .JobAdd1 = Nothing And .JobAdd2 <> Nothing Then
                    .JobAdd1 = .JobAdd2
                    .JobAdd2 = ""
                End If
                .JobCity = sfields(13)
                .JobState = sfields(14)
                .JobZip = sfields(15)

                .EstBalance = sfields(20)
                .PONum = sfields(23)
                .CustJobNum = sfields(24)
                .StartDate = ImportUtils.GetDate(sfields(25))
                .EndDate = ImportUtils.GetDate(sfields(26))

                '   Lender
                If sfields(27) <> "" And sfields(27) <> Nothing Then
                    .LenderName = sfields(27)
                    .LenderAdd1 = sfields(30)
                    .LenderAdd2 = sfields(31)
                    .LenderCity = sfields(32)
                    .LenderState = sfields(33)
                    .LenderZip = sfields(34)
                    .LenderPhone1 = ImportUtils.GetPhoneNo(sfields(35))
                End If

                If (sfields(38) <> "" And sfields(38) <> Nothing) And (sfields(27) = "" And sfields(27) = Nothing) Then
                    .LenderName = sfields(38)
                    .LenderAdd1 = sfields(41)
                    .LenderAdd2 = sfields(42)
                    .LenderCity = sfields(43)
                    .LenderState = sfields(44)
                    .LenderZip = sfields(45)
                    .LenderPhone1 = ImportUtils.GetPhoneNo(sfields(46))
                    .BondNum = sfields(48)
                End If

                '   GC
                .GCName = sfields(49)
                .GCContactName = sfields(24)
                .GCAdd1 = sfields(52)
                .GCAdd2 = sfields(53)
                .GCCity = sfields(54)
                .GCState = sfields(55)
                .GCZip = sfields(56)
                .GCPhone1 = ImportUtils.GetPhoneNo(sfields(57))
                .GCEmail = sfields(58)
                .GenId = -1

                '   Owner
                .OwnrName = sfields(59)
                .OwnrAdd1 = sfields(62)
                .OwnrAdd2 = sfields(63)
                .OwnrCity = sfields(64)
                .OwnrState = sfields(65)
                .OwnrZip = sfields(66)
                .OwnrPhone1 = ImportUtils.GetPhoneNo(sfields(67))

                .PrivateJob = True

                .PrelimBox = True
                .JobActionId = 36

                .SubmittedByUserId = auser.Id
                .BatchId = abatchid
                .SubmittedOn = Now()

                .PlacementSource = "EDI"

            End With
            Return True

        End Function

        Public Shared Function ParseFBM470ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 24 Then
                Return False
            End If
            If Len(sfields(3)) < 2 Then
                Return False
            End If

            With abatchjob
                .ClientId = aclientid
                .JobNum = sfields(12)

                .CustRefNum = sfields(3)
                .CustName = sfields(4)
                .CustAdd1 = sfields(5)

                If Len(sfields(6)) > 0 Then
                    .CustAdd2 = sfields(6)
                End If

                .CustCity = sfields(7)
                .CustState = sfields(8)
                .CustZip = sfields(9)
                .CustPhone1 = ImportUtils.GetPhoneNo(sfields(10))
                .CustFax = ImportUtils.GetPhoneNo(sfields(11))
                .CustId = -1

                .StartDate = ImportUtils.GetDate(sfields(22))

                .JobName = sfields(13)
                .JobAdd1 = sfields(14)

                If Len(sfields(15)) > 0 Then
                    .JobAdd2 = sfields(15)
                End If

                .JobCity = sfields(16)
                .JobState = sfields(17)
                .JobZip = sfields(18)

                If Len(sfields(23)) > 0 Then
                    .GCName = sfields(23)
                End If

                .BranchNum = sfields(2)
                .EstBalance = Trim(sfields(20))

                .PrelimBox = True
                .PrivateJob = True

                .SubmittedByUserId = auser.Id
                .BatchId = abatchid
                .SubmittedOn = Now()

                .PlacementSource = "EDI"

            End With
            Return True

        End Function
        Public Shared Function ParseSAB925ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchjob As CRFDB.TABLES.BatchJob) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 42 Or Len(sfields(0)) < 1 Then
                Return False
            End If

            With abatchjob
                .JobNum = sfields(0)
                .JobName = sfields(1)
                .JobAdd1 = sfields(2)
                .JobCity = sfields(3)
                .JobState = sfields(4)
                .JobZip = sfields(5)
                .EstBalance = sfields(6)
                .StartDate = ImportUtils.GetDate(sfields(7))

                .CustName = sfields(8)
                .CustAdd1 = sfields(9)
                .CustAdd2 = sfields(10)
                If .CustAdd1 = Nothing And .CustAdd2 <> Nothing Then
                    .CustAdd1 = .CustAdd2
                    .CustAdd2 = ""
                End If
                .CustCity = sfields(11)
                .CustState = sfields(12)
                .CustZip = sfields(13)
                .CustPhone1 = ImportUtils.GetPhoneNo(sfields(14))
                .CustId = -1

                If Len(sfields(15)) > 0 Then
                    .GCName = sfields(15)
                    .GCAdd1 = sfields(16)
                    .GCAdd2 = sfields(17)
                    .GCCity = sfields(18)
                    .GCState = sfields(19)
                    .GCZip = sfields(20)
                    .GenId = -1
                End If

                If Len(sfields(21)) > 0 Then
                    .OwnrName = sfields(21)
                    .OwnrAdd1 = sfields(22)
                    .OwnrAdd2 = sfields(23)
                    .OwnrCity = sfields(24)
                    .OwnrState = sfields(25)
                    .OwnrZip = sfields(26)
                End If

                ' 12/7/2021 same lender & Bond Co logic as Standard
                If Len(sfields(28)) > 0 Then
                    .BondNum = sfields(27)
                    .LenderName = sfields(35)
                    .LenderAdd1 = sfields(36)
                    .LenderAdd2 = sfields(37)
                    .LenderCity = sfields(38)
                    .LenderState = sfields(39)
                    .LenderZip = sfields(40)
                    .LenderPhone1 = ImportUtils.GetPhoneNo(sfields(41))
                End If

                If sfields(28) <> "" And sfields(28) <> Nothing Then
                    .BondNum = sfields(27)
                    .LenderName = sfields(28)
                    .LenderAdd1 = sfields(29)
                    .LenderAdd2 = sfields(30)
                    .LenderCity = sfields(31)
                    .LenderState = sfields(32)
                    .LenderZip = sfields(33)
                    .LenderPhone1 = ImportUtils.GetPhoneNo(sfields(34))
                End If

                .PrivateJob = True
                .PrelimBox = False
                .PrelimASIS = False

                If sfields(42) = "T" Then
                    .PrelimASIS = True
                    .JobActionId = 44
                Else
                    .PrelimBox = True
                    .JobActionId = 36
                End If

                .SubmittedByUserId = auser.Id
                .BatchId = abatchid
                .SubmittedOn = Now()
                .ClientId = aclientid

                .PlacementSource = "EDI"

            End With
            Return True

        End Function
    End Class
End Namespace