﻿Imports System.Text.RegularExpressions
Imports System.Globalization
Namespace Liens.DataEntry.ImportEDI
    Public Class ImportFiles
        Public Shared Function ParseCustomer(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef acustomer As CRFDB.TABLES.BatchCustomer) As Boolean
            Dim sfields As String() = ImportFiles.StringParser(aline, "{")

            With acustomer
                .ClientId = aclientid
                .CustRefNum = sfields(1)
                .CustomerName = Trim(sfields(2))
                If sfields(3) <> "000000" Then
                    .NARPAcctNo = sfields(3)
                End If
                .ParentCompany = Trim(sfields(4))
                .AddressLine1 = Trim(sfields(5))
                .AddressLine2 = Trim(sfields(6))
                .City = Trim(sfields(7))
                .State = sfields(8)
                .ZipCode = sfields(9)
                .ContactName = Trim(sfields(10))
                .AreaCode = sfields(11)
                .PhoneNo = sfields(12)
                .FaxAreaCode = sfields(13)
                .FaxPhoneNo = sfields(14)
                .Status = sfields(15)
                .BranchNum = sfields(16)
                .SmanNum = sfields(17)
                .DNNFlag = sfields(18)
                .ChangeType = sfields(21)
                .DateCreated = Now()
            End With
            Return True

        End Function

        Public Shared Function ParseCustomerAnalytic(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef acustomeranalytic As CRFDB.TABLES.BatchCustomerAnalytic) As Boolean
            Dim sfields As String() = ImportFiles.StringParser(aline, "{")

            With acustomeranalytic

                .CustRefNum = sfields(1)
                .TotalExposure = sfields(2)
                .OldestInvoiceDate = ImportFiles.GetFormatedDate(sfields(3), "yyyyMMdd")
                .PendingCredit = sfields(4)
                .CurrentBalance = sfields(5)
                .Bucket30Day = sfields(6)
                .Bucket60Day = sfields(7)
                .Bucket90Day = sfields(8)
                .Bucket120Day = sfields(9)
                .Bucket150Day = sfields(10)
                .YTDRevenue = sfields(11)
                .TotalBalance = sfields(12)
                .LastPayDate = ImportFiles.GetFormatedDate(sfields(14), "yyyyMMdd")
                .NSFDate = ImportFiles.GetFormatedDate(sfields(15), "yyyyMMdd")
                If IsNumeric(sfields(16)) Then
                    .WeightedAvgOpenDays = sfields(16)
                Else
                    .WeightedAvgOpenDays = 0
                End If
                If IsNumeric(sfields(17)) Then
                    .WeightedAvgPayDays = sfields(17)
                Else
                    .WeightedAvgPayDays = 0
                End If

                .Tenure = sfields(25)
                .DateCreated = Now()
            End With
            Return True

        End Function
        Public Shared Function ParseJobSite(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef ajobsite As CRFDB.TABLES.BatchJobSite) As Boolean
            Dim sfields As String() = ImportFiles.StringParser(aline, "{")

            With ajobsite
                .CustRefNum = Trim(sfields(1))
                .JobNum = Trim(sfields(2))
                .JobName = Trim(sfields(5))
                .JobAddr1 = Trim(sfields(6))
                .JobAddr2 = Trim(sfields(7))
                .JobCity = Trim(sfields(8))
                .JobState = Trim(sfields(9))
                .JobZip = Trim(sfields(10))
                .DateCreated = Now()

            End With
            Return True

        End Function
        Public Shared Function ParseRental(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef ajobview As CRFDB.TABLES.BatchJobRental) As Boolean
            Dim sfields As String() = ImportFiles.StringParser(aline, "{")

            With ajobview
                .CustRefNum = sfields(1)
                .RANum = sfields(2)
                .BranchNum = sfields(3)
                .JobNum = sfields(4)
                .Status = sfields(7)
                .ContractType = sfields(8)
                .StartDate = ImportFiles.GetFormatedDate(sfields(9), "yyyyMMdd")
                .EndDate = ImportFiles.GetFormatedDate(sfields(10), "yyyyMMdd")
                .ChangeType = sfields(11)
                .DateCreated = Now()
            End With
            Return True

        End Function
        Public Shared Function ParseInvoice(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef ajobviewinv As CRFDB.TABLES.BatchJobInvoice) As Boolean
            Dim sfields As String() = ImportFiles.StringParser(aline, "{")

            With ajobviewinv
                .CustRefNum = sfields(1)
                .BranchNum = sfields(2)
                .InvoiceNum = sfields(3)
                .SeqNo = sfields(4)
                .ContractType = sfields(5)
                .InvoiceDate = ImportFiles.GetFormatedDate(sfields(7), "yyyyMMdd")
                .ClientStatus = sfields(8)
                .NSFFlag = sfields(9)
                .UCCFlag = sfields(10)
                If Trim(sfields(11)) = "" Then
                    .OriginalBalance = 0
                Else
                    .OriginalBalance = sfields(11)
                End If
                If Trim(sfields(12)) = "" Then
                    .CurrentBalance = 0
                Else
                    .CurrentBalance = sfields(12)
                End If
                If Trim(sfields(13)) = "" Then
                    .OrigWriteOffAmt = 0
                Else
                    .OrigWriteOffAmt = sfields(13)
                End If
                If Trim(sfields(14)) = "" Then
                    .Recoveries = 0
                Else
                    .Recoveries = sfields(14)
                End If
                If Trim(sfields(15)) = "" Then
                    .Adjustments = 0
                Else
                    .Adjustments = sfields(15)
                End If
                If Trim(sfields(16)) = "" Then
                    .WriteOffAmt = 0
                Else
                    .WriteOffAmt = sfields(16)
                End If

                .ChangeType = sfields(17)
                .DateCreated = Now()
            End With
            Return True

        End Function


        Public Shared Function IsExistingJob(ByVal aclientid As Integer, ByVal aJobNum As String) As Integer
            Dim sSql As String = "Select Id From Job j with (nolock) Where Clientid = " & aclientid & " And JobNum = '" & aJobNum & "'"
            Dim myview As DAL.COMMON.TableView = DBO.GetTableView(sSql)
            If myview.Count > 0 Then
                Return myview.RowItem("Id")
            Else
                Return 0
            End If
        End Function
        Public Shared Function GetBoolean(ByVal aflag As Object) As Boolean
            Try
                Return Boolean.Parse(aflag)
            Catch ex As Exception
                Return False
            End Try
        End Function
        Public Shared Function GetDecimal(ByVal aamt As Object) As Decimal
            Try
                Return Decimal.Parse(aamt)
            Catch ex As Exception
                Return 0
            End Try
        End Function
        Public Shared Function GetPhoneNo(ByVal avalue As Object) As String
            Try
                Return HDS.WINLIB.Common.UnFormat.PhoneNo(avalue)
            Catch ex As Exception
                Return ""
            End Try
        End Function
        Public Shared Function GetDate(ByVal adate As String) As Date
            Try
                If IsDate(adate) Then
                    Return Date.Parse(adate)
                Else
                    Return Date.MinValue
                End If
            Catch ex As Exception
                Return Date.MinValue
            End Try
        End Function
        Public Shared Function GetFormatedDate(ByVal adate As String, ByVal aformat As String) As Date
            Try
                Dim provider As CultureInfo = CultureInfo.InvariantCulture
                Return Date.ParseExact(adate, aformat, provider)

            Catch ex As Exception
                Return Date.MinValue
            End Try
        End Function
        Public Shared Function FormatPhoneNo(ByVal avalue As Object) As String
            Try
                Return HDS.WINLIB.Common.Format.PhoneNo(avalue)
            Catch ex As Exception
                Return ""
            End Try
        End Function
        Public Shared Function FormatDate(ByVal adate As Object) As String

            Try
                If IsDate(adate) And adate <> Date.MinValue Then
                    Dim s As String() = Strings.Split(Date.Parse(adate).ToShortDateString, "/")
                    Dim y As String = Right("00" & s(0), 2) & "/" & Right("00" & s(1), 2) & "/" & Right("00" & s(2), 2)
                    Return y
                Else
                    Return String.Empty
                End If
            Catch ex As Exception
                Return String.Empty
            End Try
        End Function
        Public Shared Function FormatDecimal(ByVal aamt As Object) As String

            Try
                Return Strings.FormatCurrency(Decimal.Parse(aamt), 2)
            Catch ex As Exception
                Return Strings.FormatCurrency(Decimal.Parse(0), 2)
            End Try
        End Function
        Public Shared Function StringParser(ByVal aline As String, ByVal adelimiter As String) As String()
            Dim fields As Array = Nothing
            Dim rawFields() As String = aline.Split(Convert.ToChar("{"))
            ' recombine any with quotes
            RecombineQuotedFields(rawFields, adelimiter)
            ' remove the extra elements
            ExtractNullArrayElements(rawFields, fields)
            Return fields

        End Function
        Public Shared Function CSVParser(ByVal aline As String, ByVal adelimiter As String) As String()
            Dim fields As Array = Nothing
            Dim rawFields() As String = aline.Split(Convert.ToChar(","))
            ' recombine any with quotes
            RecombineQuotedFields(rawFields, adelimiter)
            ' remove the extra elements
            ExtractNullArrayElements(rawFields, fields)
            Return fields

        End Function
        Private Shared Sub RecombineQuotedFields(ByRef fields() As String, ByVal adelimiter As String)
            Dim _quotechar As String = Strings.Chr(34)
            Dim _delimiter As String = adelimiter
            Dim firstChar As Char
            Dim lastChar As Char
            Dim maxFieldIndex As Int32 = fields.GetLength(0) - 1
            ' start seaching
            For x As Int32 = 0 To maxFieldIndex
                ' get the potential delimitters
                If fields(x).Length > 0 Then
                    firstChar = fields(x).Chars(0)
                    lastChar = fields(x).Chars(fields(x).Length - 1)
                Else
                    firstChar = Nothing
                    lastChar = Nothing
                End If
                ' start the comparisons
                If firstChar = _quotechar Then
                    ' we started with a valid quote character
                    If (firstChar = lastChar) Then
                        ' strip off the matched quotes
                        fields.SetValue(fields(x).Substring(1, fields(x).Length - 2), x)
                    Else
                        Dim startIndex As Int32 = x
                        Dim quoteChar As Char = firstChar
                        Do
                            ' skip to the next item
                            x += 1
                            ' get the new "endpoints"
                            firstChar = fields(x).Chars(0)
                            lastChar = fields(x).Chars(fields(x).Length - 1)
                            ' this field better not start with a quote
                            If firstChar = _quotechar And fields(x).Length > 1 Then
                                Throw New ApplicationException("There was an unclosed quotation mark.")
                            End If
                            ' recombine the items
                            fields.SetValue(String.Concat(fields(startIndex).ToString, _delimiter, fields(x).ToString), startIndex)
                            ' flush the unused array element
                            Array.Clear(fields, x, 1)
                        Loop Until (lastChar = quoteChar)
                        ' strip off the outer quotes
                        fields.SetValue(fields(startIndex).Substring(1, fields(startIndex).Length - 2), startIndex)
                    End If
                End If
            Next
        End Sub
        Private Shared Sub ExtractNullArrayElements(ByRef input() As String, ByRef output As Array)
            Dim x As Int32
            Dim maxInputIndex As Int32 = input.Length - 1
            Dim count As Int32 = 0
            Dim mark As Int32 = 0
            ' get the actual field count
            For x = 0 To maxInputIndex
                If Not input(x) Is Nothing Then count += 1
            Next
            ' resize the output array
            output = Array.CreateInstance(GetType(String), count)
            For x = 0 To maxInputIndex
                If Not (input(x) Is Nothing) Then
                    ' save the value and incriment the book mark
                    output.SetValue(input(x), mark)
                    mark += 1
                End If
            Next
        End Sub

    End Class
End Namespace