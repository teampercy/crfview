Imports HDS
Imports HDS.CVPORTAL
Imports HDS.CVPORTAL.BLL
Imports HDS.CVPORTAL.BLL.CVDB.VIEWS
Imports HDS.CVPORTAL.BLL.CVDB.TABLES
Imports HDS.Reporting
Imports HDS.Reporting.COMMON
Namespace Collectview.ClientView
    Public Class Processes
        Public Shared Function GetTrustAuditLists(ByRef auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal asproc As CVDB.SPROCS.cview_Accounting_GetAuditList, ByVal printmode As HDS.Reporting.COMMON.PrintMode, ByVal spreadsheet As Boolean) As Integer
            With asproc
            End With
            Dim MYVIEW As DAL.COMMON.TableView = DBO.GetTableView(asproc)
            akeyvals.Add("RECORDCOUNT", MYVIEW.Count)
            If MYVIEW.Count > 0 Then
                Accounting.Reports.TrustAuditLists(auser, akeyvals, MYVIEW, printmode, spreadsheet)
            End If
            Return MYVIEW.Count

        End Function
        Public Shared Function CreateInvoice(ByRef auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal asproc As CVDB.SPROCS.cview_Accounting_CreateInvoice) As Integer
            With asproc
            End With
            Return DBO.ExecScalar(asproc)

        End Function
        Public Shared Function VoidInvoice(ByRef auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal asproc As CVDB.SPROCS.cview_Accounting_VoidInvoice) As Integer
            With asproc
            End With
            Return DBO.ExecScalar(asproc)

        End Function
        Public Shared Function GetTrustRecaps(ByRef auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal asproc As CVDB.SPROCS.cview_Accounting_GetFinDocs, ByVal printmode As HDS.Reporting.COMMON.PrintMode, ByVal spreadsheet As Boolean) As Integer
            With asproc
            End With
            Dim MYVIEW As DAL.COMMON.TableView = DBO.GetTableView(asproc)
            akeyvals.Add("RECORDCOUNT", MYVIEW.Count)
            If MYVIEW.Count > 0 Then
                Accounting.Reports.TrustAuditLists(auser, akeyvals, MYVIEW, printmode, spreadsheet)
            End If
            Return MYVIEW.Count

        End Function
        Public Shared Function GetInvoices(ByRef auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal asproc As CVDB.SPROCS.cview_Accounting_GetFinDocs, ByVal printmode As HDS.Reporting.COMMON.PrintMode, ByVal spreadsheet As Boolean) As Integer
            With asproc
            End With
            Dim MYVIEW As DAL.COMMON.TableView = DBO.GetTableView(asproc)
            akeyvals.Add("RECORDCOUNT", MYVIEW.Count)
            If MYVIEW.Count > 0 Then
                Accounting.Reports.TrustAuditLists(auser, akeyvals, MYVIEW, printmode, spreadsheet)
            End If
            Return MYVIEW.Count

        End Function
    End Class
End Namespace
