Imports HDS
Imports HDS.CVPORTAL
Imports HDS.CVPORTAL.BLL
Imports HDS.CVPORTAL.BLL.CVDB.VIEWS
Imports HDS.CVPORTAL.BLL.CVDB.TABLES
Imports HDS.Reporting
Imports HDS.Reporting.COMMON
Namespace Collectview.ClientView
    Public Class Reports
        Public Shared Function TrustAuditLists(ByRef auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal MYview As DAL.COMMON.TableView, ByVal aprintmode As HDS.Reporting.COMMON.PrintMode, ByVal aspreadsheet As Boolean) As String
            If MYview.Count < 1 Then
                Return Nothing
            End If

            akeyvals.Clear()

            Dim myreportpath As String = auser.SettingsFolder & "Reports\AgencyReports.xml"
            Dim myreportname As String = "PaymentNotice"
            If aprintmode <> HDS.Reporting.COMMON.PrintMode.NoReport Then
                MYC1RPT.Render(auser.UserName, auser.OutputFolder, myreportpath, myreportname, MYview, aprintmode)
            End If

            If aspreadsheet = True Then
                Dim myspreadsheetpath As String = Utils.GetFileName(auser, "", myreportname)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.ServiceCode)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.FileNumber)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.AccountNo)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.DebtorName)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.TrxDate)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.LedgerType)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.Rate)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.RcvByAgencyAmt)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.FeeByAgencyAmt)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.RcvByClientAmt)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.FeeByClientAmt)
                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    akeyvals.Add("SPREADSHEET", myspreadsheetpath)
                Else
                    akeyvals.Add("SPREADSHEET", Nothing)
                End If

            End If
            Return Nothing

        End Function
        Public Shared Function TrustInvoice(ByRef auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal MYview As DAL.COMMON.TableView, ByVal aprintmode As HDS.Reporting.COMMON.PrintMode, ByVal aspreadsheet As Boolean) As String
            If MYview.Count < 1 Then
                Return Nothing
            End If

            akeyvals.Clear()

            Dim myreportpath As String = auser.SettingsFolder & "Reports\AgencyReports.xml"
            Dim myreportname As String = "PaymentNotice"
            If aprintmode <> HDS.Reporting.COMMON.PrintMode.NoReport Then
                MYC1RPT.Render(auser.UserName, auser.OutputFolder, myreportpath, myreportname, MYview, aprintmode)
            End If

            If aspreadsheet = True Then
                Dim myspreadsheetpath As String = Utils.GetFileName(auser, "", myreportname)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.ServiceCode)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.FileNumber)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.AccountNo)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.DebtorName)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.TrxDate)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.LedgerType)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.Rate)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.RcvByAgencyAmt)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.FeeByAgencyAmt)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.RcvByClientAmt)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.FeeByClientAmt)
                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    akeyvals.Add("SPREADSHEET", myspreadsheetpath)
                Else
                    akeyvals.Add("SPREADSHEET", Nothing)
                End If

            End If
            Return Nothing

        End Function
        Public Shared Function TrustStatement(ByRef auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal MYview As DAL.COMMON.TableView, ByVal aprintmode As HDS.Reporting.COMMON.PrintMode, ByVal aspreadsheet As Boolean) As String
            If MYview.Count < 1 Then
                Return Nothing
            End If

            akeyvals.Clear()

            Dim myreportpath As String = auser.SettingsFolder & "Reports\AgencyReports.xml"
            Dim myreportname As String = "PaymentNotice"
            If aprintmode <> HDS.Reporting.COMMON.PrintMode.NoReport Then
                MYC1RPT.Render(auser.UserName, auser.OutputFolder, myreportpath, myreportname, MYview, aprintmode)
            End If

            If aspreadsheet = True Then
                Dim myspreadsheetpath As String = Utils.GetFileName(auser, "", myreportname)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.ServiceCode)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.FileNumber)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.AccountNo)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.DebtorName)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.TrxDate)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.LedgerType)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.Rate)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.RcvByAgencyAmt)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.FeeByAgencyAmt)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.RcvByClientAmt)
                MYview.ExportColumns.Add(vwDebtAccountLedger.ColumnNames.FeeByClientAmt)
                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    akeyvals.Add("SPREADSHEET", myspreadsheetpath)
                Else
                    akeyvals.Add("SPREADSHEET", Nothing)
                End If

            End If
            Return Nothing

        End Function
    End Class
End Namespace
