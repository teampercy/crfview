Imports HDS
Imports HDS.Reporting
Imports HDS.Reporting.COMMON

Namespace Collectview.Accounting
    Public Class Reports
        Public Shared Function TrustAuditLists(ByRef auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal MYview As DAL.COMMON.TableView, ByVal aprintmode As HDS.Reporting.COMMON.PrintMode, ByVal aspreadsheet As Boolean) As String
            If MYview.Count < 1 Then
                Return Nothing
            End If

            Globals.SetC1RptLogo()
            akeyvals.Clear()

            Dim myreportpath As String = auser.SettingsFolder & "Reports\CollectViewReports.xml"
            Dim myreportname As String = "TrustAuditList"
            If aprintmode <> HDS.Reporting.COMMON.PrintMode.NoReport Then
                MYC1RPT.Render(auser.UserName, auser.OutputFolder, myreportpath, myreportname, MYview, aprintmode)
            End If

            If aspreadsheet = True Then
                Dim myspreadsheetpath As String = Utils.GetFileName(auser, "", myreportname)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.ClientCode)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.FileNumber)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.AccountNo)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.DebtorName)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.TrxDate)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.LedgerType)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.Rate)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.RcvByAgencyAmt)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.FeeByAgencyAmt)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.RcvByClientAmt)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.FeeByClientAmt)
                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    akeyvals.Add("SPREADSHEET", myspreadsheetpath)
                Else
                    akeyvals.Add("SPREADSHEET", Nothing)
                End If

            End If
            Return Nothing

        End Function
        Public Shared Function TrustInvoice(ByRef auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal MYview As DAL.COMMON.TableView, ByVal aprintmode As HDS.Reporting.COMMON.PrintMode, ByVal aspreadsheet As Boolean) As String
            If MYview.Count < 1 Then
                Return Nothing
            End If
            Globals.SetC1RptLogo()
            akeyvals.Clear()

            Dim myreportpath As String = auser.SettingsFolder & "Reports\CollectViewReports.xml"
            Dim myreportname As String = "TrustInvoice"
            If aprintmode <> HDS.Reporting.COMMON.PrintMode.NoReport Then
                MYC1RPT.Render(auser.UserName, auser.OutputFolder, myreportpath, myreportname, MYview, aprintmode)
            End If

            If aspreadsheet = True Then
                Dim myspreadsheetpath As String = Utils.GetFileName(auser, "", myreportname)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.ClientCode)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.FileNumber)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.AccountNo)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.DebtorName)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.TrxDate)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.LedgerType)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.Rate)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.RcvByAgencyAmt)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.FeeByAgencyAmt)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.RcvByClientAmt)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.FeeByClientAmt)
                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    akeyvals.Add("SPREADSHEET", myspreadsheetpath)
                Else
                    akeyvals.Add("SPREADSHEET", Nothing)
                End If

            End If
            Return Nothing

        End Function
        Public Shared Function TrustStatement(ByRef auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal MYview As DAL.COMMON.TableView, ByVal aprintmode As HDS.Reporting.COMMON.PrintMode, ByVal aspreadsheet As Boolean) As String
            If MYview.Count < 1 Then
                Return Nothing
            End If
            Globals.SetC1RptLogo()
            akeyvals.Clear()

            Dim myreportpath As String = auser.SettingsFolder & "Reports\CollectViewReports.xml"
            Dim myreportname As String = "TrustStatement"
            If aprintmode <> HDS.Reporting.COMMON.PrintMode.NoReport Then
                MYC1RPT.Render(auser.UserName, auser.OutputFolder, myreportpath, myreportname, MYview, aprintmode)
            End If

            If aspreadsheet = True Then
                Dim myspreadsheetpath As String = Utils.GetFileName(auser, "", myreportname)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.ClientCode)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.FileNumber)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.AccountNo)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.DebtorName)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.TrxDate)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.LedgerType)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.Rate)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.RcvByAgencyAmt)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.FeeByAgencyAmt)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.RcvByClientAmt)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.FeeByClientAmt)
                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    akeyvals.Add("SPREADSHEET", myspreadsheetpath)
                Else
                    akeyvals.Add("SPREADSHEET", Nothing)
                End If

            End If
            Return Nothing

        End Function
    End Class
End Namespace
