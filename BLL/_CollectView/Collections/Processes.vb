Imports HDS
Imports HDS.Reporting
Imports HDS.Reporting.COMMON
Namespace Collectview.Collections
    Public Class Processes
        Public Shared Function GetAccountInfo(ByVal auser As Users.CurrentUser, ByVal aitemid As String) As AccountInfo
            Return New AccountInfo(auser, aitemid)
        End Function
        Public Shared Function GetAccountList(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.cview_Accounts_GetAccountList) As DAL.COMMON.TableView
            Return DBO.GetTableView(asproc)
        End Function
        Public Shared Function PostAccountUpdate(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.cview_Accounts_Update) As Boolean
            Return DBO.ExecScalar(asproc)
        End Function
        Public Shared Function PostLedgerUpdate(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.cview_Accounts_Update) As Boolean
            Return DBO.ExecScalar(asproc)
        End Function
        'Public Shared Function PostMastStatusChange(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.cview_Accounts_MassChange) As Boolean
        '    Return DBO.ExecScalar(asproc)
        'End Function
        Public Shared Function PostPayToAgency(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.cview_Accounts_LedgerEntry) As DAL.COMMON.TableView
            asproc.LedgerType = "PMTA"
            Return DBO.GetTableView(asproc)
        End Function
        Public Shared Function PostPayToClient(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.cview_Accounts_LedgerEntry) As DAL.COMMON.TableView
            asproc.LedgerType = "PMTC"
            Return DBO.GetTableView(asproc)
        End Function
        Public Shared Function PostLedgerReveral(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.cview_Accounts_LedgerReversal) As Boolean
            Return DBO.ExecScalar(asproc)
        End Function
        Public Shared Function PostLedgerEntry(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.cview_Accounts_LedgerEntry) As DAL.COMMON.TableView
            asproc.LedgerType = "BADJ"
            Return DBO.GetTableView(asproc)
        End Function
        'Public Shared Function PostResultUpdate(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.cview_Accounts_ResultUpdate) As Boolean
        '    Return DBO.ExecScalar(asproc)
        'End Function
        Public Shared Function ReassignService(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.cview_Accounts_ReassignService) As Boolean
            Return DBO.ExecScalar(asproc)
        End Function
        Public Shared Function GetColectionStatus(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Return Queries.GetCollectionStatusList
        End Function
        Public Shared Function GetCollectionClients(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Return Queries.GetAllClients
        End Function
        Public Shared Function GetLocations(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Return Queries.GetLocationList
        End Function
        Public Shared Function GetUserList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Return Queries.GetOwnerList
        End Function
        Public Shared Function MergeLetter(ByVal auser As Users.CurrentUser, ByRef akeyvals As HDS.DAL.COMMON.KeyValues, ByVal aaccountid As String, ByVal aletterid As String) As String
            'Dim mysproc As New SPROCS.cview_Accounts_GetLetterInfo
            'mysproc.AccountId = aaccountid
            'mysproc.LetterId = aletterid
            'Dim myds As DataSet = DBO.GetDataSet(mysproc)
            'If myds.Tables(1).Rows.Count < 1 Then Return Nothing
            'If myds.Tables(0).Rows.Count < 1 Then Return Nothing
            'Dim mydata As HDS.DAL.COMMON.TableView
            'mydata = New HDS.DAL.COMMON.TableView(myds.Tables(0))
            'Dim mydocdt As HDS.DAL.COMMON.TableView
            'Dim mydoc As New TABLES.Letter
            'mydocdt = New HDS.DAL.COMMON.TableView(myds.Tables(1))
            'mydocdt.FillEntity(mydoc)
            'If IsNothing(mydoc.LetterImage) Then Return Nothing

            'Dim myrtfDOC As String = RTFLIB.GetDoc(RTFLIB.Transform(mydoc.LetterImage))
            'Dim mymergefields As ArrayList = RTFLIB.GetMergeFields(myrtfDOC)
            'Dim myfield As String
            'For Each myfield In mymergefields
            '    Dim myvalue As String = ""
            '    Select Case myfield
            '        Case "CURRENTDATE"
            '            myvalue = Today
            '        Case Else
            '            If mydata.Table.Columns.Contains(myfield) Then
            '                myvalue = mydata.RowItem(myfield)
            '            End If
            '    End Select
            '    myrtfDOC = RTFLIB.Replace(myrtfDOC, "[" & myfield & "]", myvalue)
            'Next

            'Return myrtfDOC
            Return Nothing

        End Function
        Public Shared Function PrintClosedReport(ByVal auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal asproc As SPROCS.cview_Reports_GetClosedAccountList, ByVal printmode As HDS.Reporting.COMMON.PrintMode, ByVal spreadsheet As Boolean) As Integer
            Dim MYVIEW As DAL.COMMON.TableView = DBO.GetTableView(asproc)
            akeyvals.Add("RECORDCOUNT", MYVIEW.Count)
            If MYVIEW.Count > 0 Then
                Collections.Reports.AccountsClosed(auser, akeyvals, MYVIEW, printmode, spreadsheet)
            End If
            Return MYVIEW.Count

        End Function
        Public Shared Function PrintMasterReport(ByVal auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal asproc As SPROCS.cview_Reports_GetAccountList, ByVal printmode As HDS.Reporting.COMMON.PrintMode, ByVal spreadsheet As Boolean) As Integer
            Dim MYVIEW As DAL.COMMON.TableView = DBO.GetTableView(asproc)
            akeyvals.Add("RECORDCOUNT", MYVIEW.Count)
            If MYVIEW.Count > 0 Then
                Collections.Reports.AccountsMaster(auser, akeyvals, MYVIEW, printmode, spreadsheet)
            End If
            Return MYVIEW.Count

        End Function
        Public Shared Function PrintPaymentReport(ByVal auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal asproc As SPROCS.cview_Reports_GetPaymentList, ByVal printmode As HDS.Reporting.COMMON.PrintMode, ByVal spreadsheet As Boolean) As Integer
            Dim MYVIEW As DAL.COMMON.TableView = DBO.GetTableView(asproc)
            akeyvals.Add("RECORDCOUNT", MYVIEW.Count)
            If MYVIEW.Count > 0 Then
                Collections.Reports.AccountsPayment(auser, akeyvals, MYVIEW, printmode, spreadsheet)
            End If
            Return MYVIEW.Count

        End Function
    End Class
End Namespace
