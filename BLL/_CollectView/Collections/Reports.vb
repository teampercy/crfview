Imports HDS
Imports HDS.Reporting.COMMON
Namespace Collectview.Collections
    Public Class Reports
        Public Shared Function AccountsClosed(ByRef auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal MYview As DAL.COMMON.TableView, ByVal printmode As PrintMode, ByVal aspreadsheet As Boolean) As String

            If MYview.Count < 1 Then
                Return Nothing
            End If
            Globals.SetC1RptLogo()
            Dim myreportpath As String = auser.SettingsFolder & "Reports\AgencyReports.xml"
            Dim myreportname As String = "CloseNotice"
            If printmode <> HDS.Reporting.COMMON.PrintMode.NoReport Then
                MYC1RPT.Render(auser.UserName, auser.OutputFolder, myreportpath, myreportname, MYview, printmode)
            End If

            If aspreadsheet = True Then
                Dim myspreadsheetpath As String = Utils.GetFileName(auser, myreportname, "", "")
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.FileNumber)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.ServiceCode)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.AccountNo)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.DebtorName)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.ContactName)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.AddressLine1)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.AddressLine2)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.City)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.State)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.PostalCode)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.PhoneNo)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.Fax)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.Email)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.ReferalDate)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.LastServiceDate)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.LastPaymentDate)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.TotAsgAmt)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.TotPmtAmt)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.TotAdjAmt)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.TotBalAmt)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.CollectionStatus)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.CloseDate)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.CloseCode)
                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    akeyvals.Add("SPREADSHEET", myspreadsheetpath)
                Else
                    akeyvals.Add("SPREADSHEET", Nothing)
                End If

            End If

            Return Nothing

        End Function
        Public Shared Function AccountsPayment(ByRef auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal MYview As DAL.COMMON.TableView, ByVal printmode As PrintMode, ByVal aspreadsheet As Boolean) As String
            If MYview.Count < 1 Then
                Return Nothing
            End If
            Globals.SetC1RptLogo()

            Dim myreportpath As String = auser.SettingsFolder & "Reports\AgencyReports.xml"
            Dim myreportname As String = "PaymentNotice"
            If printmode <> HDS.Reporting.COMMON.PrintMode.NoReport Then
                MYC1RPT.Render(auser.UserName, auser.OutputFolder, myreportpath, myreportname, MYview, printmode)
            End If


            If aspreadsheet = True Then
                Dim myspreadsheetpath As String = Utils.GetFileName(auser, "", myreportname)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.ClientCode)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.FileNumber)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.AccountNo)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.DebtorName)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.TrxDate)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.LedgerType)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.Rate)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.RcvByAgencyAmt)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.FeeByAgencyAmt)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.RcvByClientAmt)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.FeeByClientAmt)
                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    akeyvals.Add("SPREADSHEET", myspreadsheetpath)
                Else
                    akeyvals.Add("SPREADSHEET", Nothing)
                End If

            End If
            Return Nothing

        End Function
        Public Shared Function AccountsMaster(ByRef auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal MYview As DAL.COMMON.TableView, ByVal printmode As HDS.Reporting.COMMON.PrintMode, ByVal aspreadsheet As Boolean) As String
            If MYview.Count < 1 Then
                Return Nothing
            End If

            Globals.SetC1RptLogo()
            Dim myreportpath As String = auser.SettingsFolder & "Reports\AgencyReports.xml"
            Dim myreportname As String = "InventoryNotice"
            If printmode <> HDS.Reporting.COMMON.PrintMode.NoReport Then
                MYC1RPT.Render(auser.UserName, auser.OutputFolder, myreportpath, myreportname, MYview, printmode)
            End If

            If aspreadsheet = True Then
                Dim myspreadsheetpath As String = Utils.GetFileName(auser, "", myreportname)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.FileNumber)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.ServiceCode)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.AccountNo)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.DebtorName)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.ContactName)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.AddressLine1)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.AddressLine2)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.City)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.State)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.PostalCode)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.PhoneNo)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.Fax)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.Email)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.ReferalDate)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.LastServiceDate)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.LastPaymentDate)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.TotAsgAmt)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.TotPmtAmt)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.TotAdjAmt)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.TotBalAmt)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.CollectionStatus)
                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    akeyvals.Add("SPREADSHEET", myspreadsheetpath)
                Else
                    akeyvals.Add("SPREADSHEET", Nothing)
                End If

            End If

            Return Nothing

        End Function
        Public Shared Function BatchList(ByVal auser As Users.CurrentUser, ByVal MYview As DAL.COMMON.TableView, ByVal printmode As HDS.Reporting.COMMON.PrintMode) As String
            If MYview.Count < 1 Then
                Return Nothing
            End If
            Globals.SetC1RptLogo()
            Dim myreportpath As String = auser.SettingsFolder & "Reports\AgencyReports.xml"
            Dim myreportname As String = "PaymentNotice"
            If printmode <> HDS.Reporting.COMMON.PrintMode.NoReport Then
                MYC1RPT.Render(auser.UserName, auser.OutputFolder, myreportpath, myreportname, MYview, printmode)
            End If

            If printmode.PrinttoExcel Then
                Dim myspreadsheetpath As String = Utils.GetFileName(auser, "", myreportname)
                MYview.ExportColumns.Add(vwBatchDebtAccountList.ColumnNames.ServiceCode)
                MYview.ExportColumns.Add(vwBatchDebtAccountList.ColumnNames.AccountNo)
                MYview.ExportColumns.Add(vwBatchDebtAccountList.ColumnNames.DebtorName)
                MYview.ExportColumns.Add(vwBatchDebtAccountList.ColumnNames.ContactName)
                MYview.ExportColumns.Add(vwBatchDebtAccountList.ColumnNames.AddressLine1)
                MYview.ExportColumns.Add(vwBatchDebtAccountList.ColumnNames.AddressLine2)
                MYview.ExportColumns.Add(vwBatchDebtAccountList.ColumnNames.City)
                MYview.ExportColumns.Add(vwBatchDebtAccountList.ColumnNames.State)
                MYview.ExportColumns.Add(vwBatchDebtAccountList.ColumnNames.PostalCode)
                MYview.ExportColumns.Add(vwBatchDebtAccountList.ColumnNames.PhoneNo)
                MYview.ExportColumns.Add(vwBatchDebtAccountList.ColumnNames.Fax)
                MYview.ExportColumns.Add(vwBatchDebtAccountList.ColumnNames.Email)
                MYview.ExportColumns.Add(vwBatchDebtAccountList.ColumnNames.ReferalDate)
                MYview.ExportColumns.Add(vwBatchDebtAccountList.ColumnNames.LastServiceDate)
                MYview.ExportColumns.Add(vwBatchDebtAccountList.ColumnNames.LastPaymentDate)
                MYview.ExportColumns.Add(vwBatchDebtAccountList.ColumnNames.TotAsgAmt)
                MYview.ExportColumns.Add(vwBatchDebtAccountList.ColumnNames.BatchId)
                MYview.ExportColumns.Add(vwBatchDebtAccountList.ColumnNames.BatchDebtAccountId)
                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    Return myspreadsheetpath
                Else
                    Return Nothing
                End If

            End If

            Return Nothing

        End Function
    End Class
End Namespace
