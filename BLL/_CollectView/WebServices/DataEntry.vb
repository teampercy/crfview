Imports HDS
Imports HDS.CVPORTAL
Imports HDS.CVPORTAL.BLL
Imports HDS.CVPORTAL.BLL.CVDB.VIEWS
Imports HDS.CVPORTAL.BLL.CVDB.TABLES
Imports HDS.Reporting
Imports HDS.Reporting.COMMON
Namespace Collectview.WebServices
    Public Class DataEntry
        Public Shared Function Request(ByVal arequeststream As String) As String
            Dim file As String = System.IO.Path.GetTempFileName
            HDS.DAL.COMMON.FileOps.WriteToFile(file, arequeststream, True)
            Dim ds As New DataSet
            Try
                ds.ReadXml(file)

            Catch ex As Exception
                Return "-1,REQUEST ERROR " & ex.Message
            End Try
            If Login(ds) = False Then
                Return "-2,Login Failed"
            End If

            Dim sresults As String = ""
            sresults += Placements(ds) & vbCrLf
            sresults += Update(ds) & vbCrLf
            sresults += Inquiry(ds) & vbCrLf

            Return sresults

        End Function
        Private Shared Function Login(ByVal ads As DataSet) As Boolean
            Dim dt As DataTable = GetTable(ads, "login")
            If IsNothing(dt) = True Then
                Return False
            End If

            ' TO DO: INSERT LOGIN LOGIC HERE

            Return True
        End Function
        Private Shared Function Placements(ByVal ads As DataSet) As String
            Dim dt As DataTable = GetTable(ads, "placement")
            If IsNothing(dt) Then
                Return "0 Placements"
            End If
            ' TO DO: INSERT PLACEMENT LOGIC HERE

            Return dt.Rows.Count & " Placements"

        End Function
        Private Shared Function Update(ByVal ads As DataSet) As String
            Dim dt As DataTable = GetTable(ads, "update")
            If IsNothing(dt) Then
                Return "0 Update"
            End If
            ' TO DO: INSERT PLACEMENT LOGIC HERE

            Return dt.Rows.Count & " Update"

        End Function
        Private Shared Function Inquiry(ByVal ads As DataSet) As String
            Dim dt As DataTable = GetTable(ads, "inquiry")
            If IsNothing(dt) Then
                Return "0 Inquiry"
            End If
            ' TO DO: INSERT PLACEMENT LOGIC HERE

            Return dt.Rows.Count & " Inquiry"

        End Function

        Private Shared Function GetTable(ByVal ads As DataSet, ByVal atablename As String) As DataTable
            Dim dt As DataTable
            For Each dt In ads.Tables
                If UCase(dt.TableName) = UCase(atablename) Then
                    Return dt
                End If
            Next
            Return Nothing

        End Function
    End Class
End Namespace

