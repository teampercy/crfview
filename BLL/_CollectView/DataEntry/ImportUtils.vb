Imports HDS
Imports HDS.CVPORTAL
Imports HDS.CVPORTAL.BLL
Imports HDS.CVPORTAL.BLL.CVDB.VIEWS
Imports HDS.CVPORTAL.BLL.CVDB.TABLES
Imports HDS.Reporting
Imports HDS.Reporting.COMMON

Namespace Collectview.DataEntry
    Public Class ImportUtils
        Public Shared Function ParseStandardImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatchid As Integer, ByVal aline As String, ByRef abatchitem As CVDB.TABLES.BatchDebtAccount) As Boolean

            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 64 Or Len(sfields(13)) < 1 Then
                Return False
            End If

            With abatchitem

                '.JobNum = sfields(0)
                '.JobName = sfields(1)
                '.JobAdd1 = sfields(2)
                '.JobAdd2 = sfields(3)
                'If .JobAdd1 = Nothing And .JobAdd2 <> Nothing Then
                '    .JobAdd1 = .JobAdd2
                '    .JobAdd2 = ""
                'End If
                '.JobCity = sfields(4)
                '.JobState = sfields(5)
                '.JobZip = sfields(6)
                '.EstBalance = sfields(7)
                '.StartDate = ImportUtils.GetDate(sfields(8))
                '.PONum = sfields(9)

                '.CustJobNum = sfields(10)

                '.CustRefNum = sfields(12)
                '.CustName = sfields(13)
                '.CustContactName = sfields(14)
                '.CustAdd1 = sfields(15)
                '.CustAdd2 = sfields(16)
                'If .CustAdd1 = Nothing And .CustAdd2 <> Nothing Then
                '    .CustAdd1 = .CustAdd2
                '    .CustAdd2 = ""
                'End If
                '.CustCity = sfields(17)
                '.CustState = sfields(18)
                '.CustZip = sfields(19)
                '.CustPhone1 = ImportUtils.GetPhoneNo(sfields(20))
                '.CustPhone2 = ImportUtils.GetPhoneNo(sfields(21))
                '.CustId = -1

                'If sfields(40) <> "" And sfields(40) = Nothing Then
                '    .GCRefNum = sfields(21)
                '    .GCName = sfields(22)
                '    If .GCName = "SAME AS CUSTOMER" Then
                '        .GCName = .CustName
                '        .GCContactName = .CustContactName
                '        .GCAdd1 = .CustAdd1
                '        .GCAdd2 = .CustAdd2
                '        .GCCity = .CustCity
                '        .GCState = .CustState
                '        .GCZip = .CustZip
                '        .GCPhone1 = .CustPhone1
                '        .GCPhone2 = .CustPhone2
                '        .GenId = -1
                '    Else
                '        .GCContactName = sfields(23)
                '        .GCAdd1 = sfields(24)
                '        .GCAdd2 = sfields(25)
                '        .GCCity = sfields(26)
                '        .GCState = sfields(27)
                '        .GCZip = sfields(28)
                '        .GCPhone1 = ImportUtils.GetPhoneNo(sfields(29))
                '        .GCPhone2 = ImportUtils.GetPhoneNo(sfields(30))
                '        .GenId = -1
                '    End If
                'End If
                '.OwnrName = sfields(32)
                '.OwnrAdd1 = sfields(33)
                '.OwnrAdd2 = sfields(34)
                '.OwnrCity = sfields(35)
                '.OwnrState = sfields(36)
                '.OwnrZip = sfields(37)
                '.OwnrPhone1 = ImportUtils.GetPhoneNo(sfields(38))

                '.BondNum = sfields(39)
                '.LenderName = sfields(47)
                '.LenderAdd1 = sfields(48)
                '.LenderAdd2 = sfields(49)
                '.LenderCity = sfields(50)
                '.LenderState = sfields(51)
                '.LenderZip = sfields(52)
                '.LenderPhone1 = ImportUtils.GetPhoneNo(sfields(53))

                'If sfields(40) <> "" And sfields(40) = Nothing Then
                '    .LenderName = sfields(40)
                '    .LenderAdd1 = sfields(41)
                '    .LenderAdd2 = sfields(42)
                '    .LenderCity = sfields(43)
                '    .LenderState = sfields(44)
                '    .LenderZip = sfields(45)
                '    .LenderPhone1 = ImportUtils.GetPhoneNo(sfields(46))
                'End If

                '.SpecialInstruction = sfields(54)

                '.PublicJob = False
                '.FederalJob = False
                '.ResidentialBox = False
                '.PrivateJob = False

                '.PrelimBox = False
                '.VerifyJob = False
                '.MechanicLien = False
                '.TitleVerifiedBox = False
                '.PrelimASIS = False

                'If sfields(55) = "T" Then
                '    .PublicJob = True
                'End If
                'If sfields(56) = "T" Then
                '    .FederalJob = True
                'End If
                'If sfields(57) = "T" Then
                '    .ResidentialBox = True
                'End If
                'If sfields(58) = "T" Then
                '    .VerifyJob = True
                'End If
                'If sfields(59) = "T" Then
                '    .MechanicLien = True
                'End If
                'If sfields(60) = "T" Then
                '    .TitleVerifiedBox = True
                'End If

                'If .VerifyJob = False And .TitleVerifiedBox = False Then
                '    .PrelimBox = True
                'End If

                '.BranchNum = sfields(61)
                '.SubmittedByUserId = auser.Id
                '.BatchId = abatchid
                '.SubmittedOn = Now()
                '.ClientId = aclientid

                ' .PlacementSource = "EDI"

            End With

            Return True

        End Function

        Public Shared Function GetBoolean(ByVal aflag As Object) As Boolean
            Try
                Return Boolean.Parse(aflag)
            Catch ex As Exception
                Return False
            End Try
        End Function
        Public Shared Function GetDecimal(ByVal aamt As Object) As Decimal
            Try
                Return Decimal.Parse(aamt)
            Catch ex As Exception
                Return 0
            End Try
        End Function
        Public Shared Function GetPhoneNo(ByVal avalue As Object) As String
            Try
                Return HDS.WINLIB.Common.UnFormat.Numbers(avalue, "0123456789")
            Catch ex As Exception
                Return ""

            End Try
        End Function
        Public Shared Function GetDate(ByVal adate As String) As Date
            Try
                If IsDate(adate) Then
                    Return Date.Parse(adate)
                Else
                    Return Date.MinValue
                End If
            Catch ex As Exception
                Return Date.MinValue
            End Try
        End Function
        Public Shared Function FormatPhoneNo(ByVal avalue As Object) As String
            Try
                Return HDS.WINLIB.Common.Format.PhoneNo(avalue)
            Catch ex As Exception
                Return ""
            End Try
        End Function
        Public Shared Function FormatDate(ByVal adate As Object) As String

            Try
                If IsDate(adate) And adate <> Date.MinValue Then
                    Dim s As String() = Strings.Split(Date.Parse(adate).ToShortDateString, "/")
                    Dim y As String = Right("00" & s(0), 2) & "/" & Right("00" & s(1), 2) & "/" & Right("00" & s(2), 2)
                    Return y
                Else
                    Return String.Empty
                End If
            Catch ex As Exception
                Return String.Empty
            End Try
        End Function
        Public Shared Function FormatDecimal(ByVal aamt As Object) As String

            Try
                Return Strings.FormatCurrency(Decimal.Parse(aamt), 2)
            Catch ex As Exception
                Return Strings.FormatCurrency(Decimal.Parse(0), 2)
            End Try
        End Function
        Public Shared Function CSVParser(ByVal aline As String, ByVal adelimiter As String) As String()
            Dim fields As Array = Nothing
            Dim rawFields() As String = aline.Split(Convert.ToChar(","))
            ' recombine any with quotes
            RecombineQuotedFields(rawFields, adelimiter)
            ' remove the extra elements
            ExtractNullArrayElements(rawFields, fields)
            Return fields

        End Function
        Private Shared Sub RecombineQuotedFields(ByRef fields() As String, ByVal adelimiter As String)
            Dim _quotechar As String = Strings.Chr(34)
            Dim _delimiter As String = adelimiter
            Dim firstChar As Char
            Dim lastChar As Char
            Dim maxFieldIndex As Int32 = fields.GetLength(0) - 1
            ' start seaching
            For x As Int32 = 0 To maxFieldIndex
                ' get the potential delimitters
                If fields(x).Length > 0 Then
                    firstChar = fields(x).Chars(0)
                    lastChar = fields(x).Chars(fields(x).Length - 1)
                Else
                    firstChar = Nothing
                    lastChar = Nothing
                End If
                ' start the comparisons
                If firstChar = _quotechar Then
                    ' we started with a valid quote character
                    If (firstChar = lastChar) Then
                        ' strip off the matched quotes
                        fields.SetValue(fields(x).Substring(1, fields(x).Length - 2), x)
                    Else
                        Dim startIndex As Int32 = x
                        Dim quoteChar As Char = firstChar
                        Do
                            ' skip to the next item
                            x += 1
                            ' get the new "endpoints"
                            firstChar = fields(x).Chars(0)
                            lastChar = fields(x).Chars(fields(x).Length - 1)
                            ' this field better not start with a quote
                            If firstChar = _quotechar And fields(x).Length > 1 Then
                                Throw New ApplicationException("There was an unclosed quotation mark.")
                            End If
                            ' recombine the items
                            fields.SetValue(String.Concat(fields(startIndex).ToString, _delimiter, fields(x).ToString), startIndex)
                            ' flush the unused array element
                            Array.Clear(fields, x, 1)
                        Loop Until (lastChar = quoteChar)
                        ' strip off the outer quotes
                        fields.SetValue(fields(startIndex).Substring(1, fields(startIndex).Length - 2), startIndex)
                    End If
                End If
            Next
        End Sub
        Private Shared Sub ExtractNullArrayElements(ByRef input() As String, ByRef output As Array)
            Dim x As Int32
            Dim maxInputIndex As Int32 = input.Length - 1
            Dim count As Int32 = 0
            Dim mark As Int32 = 0
            ' get the actual field count
            For x = 0 To maxInputIndex
                If Not input(x) Is Nothing Then count += 1
            Next
            ' resize the output array
            output = Array.CreateInstance(GetType(String), count)
            For x = 0 To maxInputIndex
                If Not (input(x) Is Nothing) Then
                    ' save the value and incriment the book mark
                    output.SetValue(input(x), mark)
                    mark += 1
                End If
            Next
        End Sub

    End Class
End Namespace
