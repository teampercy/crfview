Imports HDS
Imports HDS.CVPORTAL
Imports HDS.CVPORTAL.BLL
Imports HDS.CVPORTAL.BLL.CVDB
Imports HDS.CVPORTAL.BLL.CVDB.VIEWS
Imports HDS.CVPORTAL.BLL.CVDB.TABLES
Imports HDS.Reporting
Imports HDS.Reporting.COMMON
Namespace Collectview.DataEntry
    Public Class Processes
        Public Shared Function GetMyOpenBatchList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Return Nothing
        End Function
        Public Shared Function GetBatchList(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CVDB.SPROCS.cview_Reports_GetAccountList, ByVal aprintmode As HDS.Reporting.COMMON.PrintMode, ByVal aspreadsheet As Boolean) As Integer
            Return Nothing
        End Function
        Public Shared Function GetWebPlacements(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CVDB.SPROCS.cview_Reports_GetAccountList, ByVal aprintmode As HDS.Reporting.COMMON.PrintMode, ByVal aspreadsheet As Boolean) As Integer
            Return Nothing
        End Function
        Public Shared Function PostWebPlacements(ByVal auser As Users.CurrentUser) As DataSet
            Return Nothing
        End Function
        Public Shared Function GetNewAccountInfo(ByVal auser As Users.CurrentUser, ByVal anewaccountid As String) As DataSet
            Return Nothing
        End Function
        Public Shared Function GetNewAccountList(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CVDB.SPROCS.cview_Reports_GetAccountsInBatches) As DAL.COMMON.TableView
            Return Nothing
        End Function
        Public Shared Function GetCollectionClients(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Return Nothing
        End Function
        Public Shared Function GetAccountInBatches(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CVDB.SPROCS.cview_Reports_GetAccountsInBatches) As DAL.COMMON.TableView
            Return Nothing
        End Function
        Public Shared Function GetUserList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Return Nothing
        End Function
        Public Shared Function GetLoaderList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Dim myquery As New DAL.COMMON.Query(New TABLES.ClientInterface)
            Dim myview As DAL.COMMON.TableView = DBO.GetTableView(myquery.SelectAll)
            Return myview
        End Function
        Public Shared Function GetOpenBatchListByDate(ByVal auser As Users.CurrentUser, ByVal afromdate As Date, ByVal athrudate As String) As DAL.COMMON.TableView
            Dim myquery As New DAL.COMMON.Query(New TABLES.Batch)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, TABLES.Batch.ColumnNames.BatchStatus, DAL.COMMON.Conditions._IsNotEqual, "CLS")
            myquery.AddDateRange(DAL.COMMON.LogicalOperators._And, TABLES.Batch.ColumnNames.BatchDate, afromdate, athrudate)
            Dim myview As DAL.COMMON.TableView = DBO.GetTableView(myquery.BuildQuery)
            myview.Sort = TABLES.Batch.ColumnNames.BatchDate & " DESC"
            Return myview
        End Function
        Public Shared Function GetBatchListByDate(ByVal auser As Users.CurrentUser, ByVal afromdate As Date, ByVal athrudate As String) As DAL.COMMON.TableView
            Dim myquery As New DAL.COMMON.Query(New TABLES.Batch)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, TABLES.Batch.ColumnNames.BatchStatus, DAL.COMMON.Conditions._IsEqual, "CLS")
            myquery.AddDateRange(DAL.COMMON.LogicalOperators._And, TABLES.Batch.ColumnNames.BatchDate, afromdate, athrudate)
            Dim myview As DAL.COMMON.TableView = DBO.GetTableView(myquery.BuildQuery)
            myview.Sort = TABLES.Batch.ColumnNames.BatchDate & " DESC"
            Return myview
        End Function
        Public Shared Function ImportFile(ByVal auser As Users.CurrentUser, ByVal aloaderid As String, ByVal afilename As String) As Boolean
            ' log file
            If System.IO.File.Exists(afilename) = False Then Return 1

            Dim myfile As New TABLES.Portal_UserUploads
            myfile.UserName = auser.UserName
            myfile.ClientLoaderId = aloaderid
            myfile.CreatedOn = Now()
            myfile.FilePath = afilename
            DBO.Create(myfile)

            Dim myBatch As New TABLES.Batch
            Dim myds As New DataSet

            'Dim myinfo As New SPROCS.uspbo_Processing_GetInterfaceforUpload
            'myinfo.UserUploadId = myfile.Id
            'Dim myds As DataSet = DBO.GetDataSet(myinfo)
            'If myds.Tables(1).Rows.Count <> 1 Then Return 2
            'myinfo.UserUploadId = myfile.ClientLoaderId

            '' create a batch
            Dim myservice As New DAL.COMMON.TableView(myds.Tables(5))
            myBatch.FileDate = System.IO.File.GetCreationTime(myfile.FilePath)
            myBatch.BatchDate = Now()
            myBatch.BatchType = "EDI"
            myBatch.ProcessOn = Now()
            myBatch.TimeStarted = Now()
            myBatch.TimeEnded = Now()
            myBatch.ServiceCode = myservice.RowItem("ServiceCode")
            myBatch.UploadId = myfile.Id
            myBatch.ClientLoaderId = myfile.ClientLoaderId
            DBO.Create(myBatch)

            ' import the data

            'Dim myobj As New Processing.EDILoaders.FedexImport(myfile, myBatch, myds)
            'myobj.Import()

            ' close the batch
            myBatch.TimeEnded = Now()
            myBatch.BatchStatus = "OPEN"
            DBO.Update(myBatch)

            Return 0

        End Function
        Public Shared Function PostBatch(ByVal auser As Users.CurrentUser, ByVal abatchid As String) As Integer
            'Dim mysproc As New SPROCS.DV_Processing_PostBatch
            'mysproc.BATCHID = abatchid
            'DBO.CommandTimeOut = 16000
            'Return DBO.ExecNonQuery(mysproc)

        End Function
        Public Shared Function ReleaseBatch(ByVal auser As Users.CurrentUser, ByVal abatchid As String) As Integer
            'Dim mysproc As New SPROCS.DV_Processing_PostBatch
            'mysproc.BATCHID = abatchid
            'DBO.CommandTimeOut = 16000
            'Return DBO.ExecNonQuery(mysproc)

        End Function


    End Class
End Namespace
