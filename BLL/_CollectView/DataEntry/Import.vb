Imports HDS
Imports HDS.CVPORTAL
Imports HDS.CVPORTAL.BLL
Imports HDS.CVPORTAL.BLL.CVDB.VIEWS
Imports HDS.CVPORTAL.BLL.CVDB.TABLES
Imports HDS.Reporting
Imports HDS.Reporting.COMMON
Imports System.IO
Namespace Collectview.DataEntry
    Public Class Import
        Public Function ImportXML(ByVal auser As Users.CurrentUser, ByVal afilepath As String, ByVal aclientid As String, ByVal ainterfaceid As String, ByVal abatchid As Integer) As Boolean

            Dim s As String = afilepath
            Dim sr As New StreamReader(afilepath)
            Dim mycount As Integer = 0
            Dim myreturn As Boolean = True
            Dim myline As String
            Do While sr.Peek > -1 And myreturn = True
                myline = sr.ReadLine
                If mycount > 0 Then
                    myreturn = ProcessLine(auser, afilepath, aclientid, ainterfaceid, abatchid, myline)
                End If
                mycount += 1
            Loop
            Return myreturn

        End Function
        Public Function ImportCSV(ByVal auser As Users.CurrentUser, ByVal afilepath As String, ByVal aclientid As String, ByVal ainterfaceid As String, ByVal abatchid As Integer) As Boolean

            Dim s As String = afilepath
            Dim sr As New StreamReader(afilepath)
            Dim mycount As Integer = 0
            Dim myreturn As Boolean = True
            Dim myline As String
            Do While sr.Peek > -1 And myreturn = True
                myline = sr.ReadLine
                If mycount > 0 Then
                    myreturn = ProcessLine(auser, afilepath, aclientid, ainterfaceid, abatchid, myline)
                End If
                mycount += 1
            Loop
            Return myreturn

        End Function
        Public Shared Function ProcessLine(ByVal auser As Users.CurrentUser, ByVal afilepath As String, ByVal aclientid As String, ByVal ainterfaceid As String, ByVal abatchid As Integer, ByVal aline As String) As Boolean

            Dim mybatchitem As New CVDB.TABLES.BatchDebtAccount
            'mybatchitem.SubmittedOn = Now
            'mybatchitem.LastUpdateOn = Today

            If ImportUtils.ParseStandardImportLine(auser, aclientid, abatchid, aline, mybatchitem) = True Then
                DBO.Create(mybatchitem)
                Return True
            Else
                Return False
            End If

        End Function

    End Class

End Namespace
