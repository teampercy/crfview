Namespace CollectView
    Public Class Utils
        'Public Shared Function GetFilePath(ByRef auser As Users.CurrentUser, ByVal akeyvalue As String, ByVal afilename As String) As String
        '    Dim mydir As String = System.IO.Path.GetDirectoryName(GetKeyValue(akeyvalue))
        '    Try
        '        If System.IO.Directory.Exists(mydir) = False Then
        '            System.IO.Directory.CreateDirectory(mydir)
        '        End If
        '    Catch
        '        mydir = System.IO.Path.GetDirectoryName(auser.OutputFolder)
        '    End Try


        '    Return mydir & "\" & TrimSpace(afilename)

        'End Function
        Public Shared Function GetFileName(ByRef auser As Users.CurrentUser, ByVal akeyvalue As String, ByVal aprefix As String, Optional ByVal aext As String = ".CSV", Optional ByVal suppressusername As Boolean = False) As String
            Dim mydir As String = System.IO.Path.GetDirectoryName(GetKeyValue(akeyvalue))
            Try
                If System.IO.Directory.Exists(mydir) = False Then
                    System.IO.Directory.CreateDirectory(mydir)
                End If
            Catch
                mydir = auser.OutputFolder
            End Try

            Dim S As String
            If suppressusername = True Then
                S = aprefix
            Else
                S = auser.UserName & "-" & aprefix
            End If
            Return mydir & "\" & TrimSpace(S & "-" & Format(Now(), "yyyyMMddHHmmss") & aext)

        End Function
        Public Shared Function TrimSpace(ByRef strInput As String) As String
            ' This procedure trims extra space from any part of
            ' a string.
            Dim str As String = strInput.Trim
            Return Join(str.Split(" "), "")

        End Function
        Public Shared Function GetKeyValue(ByVal akey As String) As String
            'Dim MYSPROC As New SPROCS.cview_System_GetValueforKey
            'MYSPROC.Key = akey
            'Dim MYVIEW As DAL.COMMON.TableView = DBO.GetTableView(MYSPROC)
            'If MYVIEW.Count = 1 Then
            '    Return MYVIEW.RowItem("KEYVALUE")
            'Else
            '    Return Nothing
            'End If
        End Function
        Public Shared Sub SetKeyValue(ByVal akey As String, ByVal avalue As String)
            'Dim MYSPROC As New SPROCS.cview_System_SetValueforKey
            'MYSPROC.Key = akey
            'MYSPROC.Value = avalue
            'DBO.ExecNonQuery(MYSPROC)

        End Sub
        Public Shared Sub DeleteKeyValue(ByVal akey As String)
            'Dim MYSPROC As New SPROCS.cview_System_DeleteKeyValue
            'MYSPROC.Key = akey
            'DBO.ExecNonQuery(MYSPROC)

        End Sub
        Public Shared Function GetDefaultPrinter() As String
            Dim myprt As New System.Windows.Forms.PrintDialog
            Return myprt.PrinterSettings.PrinterName

        End Function
       
        Public Shared Function SendOutLookEmail(ByVal fromAddress As String, ByVal fromName As String, ByVal Subject As String, ByVal Body As String, ByVal recipients As Collection, ByVal attachments As Collection) As Boolean
            Dim s As String = ""
            Dim toe As String = String.Empty
            For Each s In recipients
                'add an attachment from the filesystem
                Dim ss As String() = Strings.Split(s, "~")
                If toe.Length > 1 Then
                    toe += ";" & ss(0)
                Else
                    toe = ss(0)
                End If
            Next

            Dim attach As String = String.Empty
            For Each s In attachments
                'add an attachment from the filesystem
                If attach.Length > 1 Then
                    attach += ";" & s
                Else
                    attach = s
                End If
            Next

            Dim Appl As Object
            Dim out As Object
            Try
                Appl = CreateObject("Outlook.Application")

                '  For Each s In recipients
                out = Appl.CreateItem(0)

                'add an attachment from the filesystem
                Dim ss As String() = Strings.Split(s, "~")

                With out
                    .Subject = Subject
                    .Body = Body
                    '==============================================
                    'to use html in the email instead of plain text
                    'use .HTMLBody instead of .body
                    '==============================================
                    Dim x As String
                    For Each x In attachments
                        .Attachments.add(x)
                    Next
                    'If attach <> "" Then
                    '    .Attachments.add(attach)
                    'End If
                    .To = toe

                    .Send()

                End With
                'Next

            Catch ex As Exception
                MsgBox("Error " & Err.Number & ": " & ex.Message, MsgBoxStyle.Critical, "Error")
            End Try
        End Function
        'Public Shared Function GetCurrentUser() As HDS.CVPORTAL.BLL.Users.CurrentUser
        '    With System.Configuration.ConfigurationManager.AppSettings

        '        Dim myuser As New HDS.CVPORTAL.BLL.Users.CurrentUser
        '        'myuser.Id = Globals.CurrentUser.Id
        '        'myuser.UserName = Globals.CurrentUser.UserName
        '        'myuser.Email = Globals.CurrentUser.Email
        '        'myuser.OutputFolder = .Get("OUTPUT")
        '        'myuser.SettingsFolder = .Get("SETTINGS")
        '        'myuser.UploadFolder = .Get("UPLOAD")
        '        Return myuser
        '    End With

        'End Function
        Public Shared Sub PrintPDF(ByVal AFILENAME As String)
            If System.IO.File.Exists(AFILENAME) = False Then Exit Sub

            With New Process
                .StartInfo.Verb = "print"
                .StartInfo.CreateNoWindow = False
                .StartInfo.FileName = AFILENAME
                .Start()
                .WaitForExit(10000)
                .CloseMainWindow()
                .Close()
            End With

        End Sub
        Public Shared Sub ViewPDF(ByVal AFILENAME As String)
            If System.IO.File.Exists(AFILENAME) = False Then Exit Sub

            Dim s As String = "explorer " & AFILENAME
            Shell(s, AppWinStyle.MaximizedFocus)


        End Sub
    End Class

End Namespace

