Imports System.Data
Namespace Collectview.Clients
    Public Class ClientInfo
        Dim myds As DataSet
        Dim myclient As DAL.COMMON.TableView
        Dim mycollinfo As DAL.COMMON.TableView
        Dim mylieninfo As DAL.COMMON.TableView
        Dim mycontracts As DAL.COMMON.TableView
        Dim myusers As DAL.COMMON.TableView
        Dim mybranches As DAL.COMMON.TableView
        Dim myopenissues As DAL.COMMON.TableView
        Dim myclosedissues As DAL.COMMON.TableView
        Public Sub New(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer)
            Dim mysproc As New SPROCS.cview_Clients_GetClientInfo
            mysproc.ClientId = aitemid
            myds = DBO.GetDataSet(mysproc)
            myclient = New DAL.COMMON.TableView(myds.Tables(0), "")
            mycontracts = New DAL.COMMON.TableView(myds.Tables(1), "")
            mybranches = New DAL.COMMON.TableView(myds.Tables(2), "")
            myopenissues = New DAL.COMMON.TableView(myds.Tables(3), "")
            myclosedissues = New DAL.COMMON.TableView(myds.Tables(4), "")
            myusers = New DAL.COMMON.TableView(myds.Tables(5), "")

        End Sub
        Public Function ClientId() As Integer
            Return myclient.RowItem(TABLES.Client.ColumnNames.ClientId)
        End Function
        Public Function ClientCode() As String
            Return myclient.RowItem(TABLES.Client.ColumnNames.ClientCode)
        End Function
        Public Function ParentClientId() As Integer
            Return myclient.RowItem(TABLES.Client.ColumnNames.ParentClientId)
        End Function
        Public Function ClientName() As String
            Return myclient.RowItem(TABLES.Client.ColumnNames.ClientName)
        End Function
        Public Function CollectionInfo() As DAL.COMMON.TableView
            Return mycollinfo
        End Function
        Public Function LienInfo() As DAL.COMMON.TableView
            Return mylieninfo
        End Function
        Public Function Contracts() As DAL.COMMON.TableView
            Return mycontracts
        End Function
        Public Function Branches() As DAL.COMMON.TableView
            Return mybranches
        End Function
        Public Function OpenIssues() As DAL.COMMON.TableView
            Return myopenissues
        End Function
        Public Function ClosedIssues() As DAL.COMMON.TableView
            Return myclosedissues
        End Function
        Public Function Users() As DAL.COMMON.TableView
            Return myusers
        End Function
    End Class
End Namespace
