Imports System.Windows.Forms
Imports System.Threading
Imports System.Configuration.ConfigurationManager
Imports System.Data
Imports CRF.BLL.Users

Public Module Globals

    Public CurrentUser As New CurrentUser
    Public DBO As DAL.Providers.IDAL = DAL.Providers.DataAccessFactory.DataAccess("PORTAL", AppSettings.Get("SQLCLIENT"))
    'Public DBO As DAL.Providers.IDAL = DAL.Providers.DataAccessFactory.DataAccess("TestConString", AppSettings.Get("SQLCLIENT"))

    Private MYC1RPT As New HDS.Reporting.C1Reports.C1ReportProvider
    Public Function GetEntity(ByVal sql As String, ByRef myitem As HDS.DAL.Providers.ITable) As Boolean
        Dim myvw As DAL.COMMON.TableView = Globals.DBO.GetTableView(sql)
        If myvw.Count = 1 Then
            myvw.FillEntity(myitem)
            Return True
        Else
            Return False
        End If
    End Function


    Public Function GetCurrentUser() As BLL.Users.CurrentUser
        With System.Configuration.ConfigurationManager.AppSettings
            Dim myuser As New BLL.Users.CurrentUser
            myuser.Id = Globals.CurrentUser.Id
            myuser.UserName = Globals.CurrentUser.UserName
            myuser.Email = Globals.CurrentUser.Email
            myuser.OutputFolder = .Get("OUTPUT")
            myuser.SettingsFolder = .Get("SETTINGS")
            myuser.UploadFolder = .Get("UPLOAD")
            myuser.LastLedgerType = .Get("LASTLEDGERTYPE")
            Return myuser
        End With

    End Function
    Private Sub SetC1RptLogo()
        Dim MYAGENCY As New BLL.CRFDB.TABLES.Agency
        DBO.Read(1, MYAGENCY)
        Dim mylogo As New HDS.WINLIB.Controls.ImageViewer
        mylogo.LoadImage(MYAGENCY.AgencyLogo)
        MYC1RPT.ReportLogo = mylogo.PictureBox1.Image
        MYC1RPT.ReportFooter = MYAGENCY.AgencyRptFooter

    End Sub
    Public Function RenderC1Report(ByVal myuser As BLL.Users.CurrentUser, ByVal adatatable As DataTable, ByVal areportdef As String, ByVal areportname As String, Optional ByVal aprintmode As BLL.COMMON.PrintMode = BLL.COMMON.PrintMode.PrintToPDF, Optional ByVal areportprefix As String = "", Optional ByVal asubtitle1 As String = "", Optional ByVal asubtitle2 As String = "", Optional ByVal aisLandscape As Boolean = False) As String
        Try
            'Dim myuser As BLL.Users.CurrentUser = GetCurrentUser()
            Dim myreportpath As String = myuser.SettingsFolder & "\REPORTS\" & areportdef
            Dim myoutputfolder As String = myuser.OutputFolder & "\"

            Dim MYC1RPT As New HDS.Reporting.C1Reports.C1ReportProvider
            Dim MYAGENCY As New CRFDB.TABLES.Agency
            DBO.Read(1, MYAGENCY)
            Dim mylogo As New HDS.WINLIB.Controls.ImageViewer
            mylogo.LoadImage(MYAGENCY.AgencyLogo)
            MYC1RPT.ReportLogo = mylogo.PictureBox1.Image
            MYC1RPT.ReportFooter = MYAGENCY.AgencyRptFooter
            'If aisLandscape = True Then
            '    MYC1RPT.C1R.Document.DefaultPageSettings.Landscape = True
            'Else
            '    MYC1RPT.C1R.Document.DefaultPageSettings.Landscape = False
            'End If
            Dim adataview As New DataView(adatatable)
            MYC1RPT.Render("", myoutputfolder, myreportpath, areportname, adataview, aprintmode, areportprefix, asubtitle1, asubtitle2)
            Return MYC1RPT.ReportPath

        Catch ex As Exception
            Return MYC1RPT.ReportPath
        End Try

    End Function







End Module
