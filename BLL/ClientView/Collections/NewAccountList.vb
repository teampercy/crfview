Namespace ClientView.Collection
    Public Class NewAccountList
        Inherits Common.DoListPage
        Dim _clientinfo As CRM.ClientView.ClientInfoForUser
        Public SprocParams As New CRFDB.SPROCS.uspbo_ClientView_GetAccountSubmitted
        Public Sub New(ByVal auser As Users.CurrentUser)
            MyBase.New()
            Me.CurrentUser = auser
            _clientinfo = New CRM.ClientView.ClientInfoForUser(Me.CurrentUser.Id)
            SprocParams.UserId = CurrentUser.Id

        End Sub
        Public ReadOnly Property ClientInfo() As CRM.ClientView.ClientInfoForUser
            Get
                Return _clientinfo
            End Get
        End Property
        Public Overrides Function GetView() As DAL.COMMON.TableView
            With SprocParams
                .UserId = Me.CurrentUser.Id
                .BatchDebtAccountId = 0
            End With
            Dim myds As DataSet = DBO.GetDataSet(SprocParams)
            Return New DAL.COMMON.TableView(myds.Tables(0), "")

        End Function
        Public Function GetAckforBatchItem(ByVal aitemid As Integer) As String
            Dim SprocParams As New CRFDB.SPROCS.cview_ClientView_GetAccountsSubmitted
            With SprocParams
                .UserId = Me.CurrentUser.Id
                .BatchDebtAccountId = aitemid
            End With
            Dim myview As DAL.COMMON.TableView = DBO.GetTableView(SprocParams)
            Dim myreport As New BLL.Common.StandardReport
            myreport.ReportDefPath = Me.CurrentUser.SettingsFolder & "Reports\WebReportsCollections.xml"
            myreport.OutputFolder = Me.CurrentUser.OutputFolder
            myreport.UserName = Me.CurrentUser.UserName
            myreport.ReportName = "AccountsAckIndividual"
            myreport.TableIndex = 1
            myreport.DataView = myview
            myreport.PrintMode = Common.PrintMode.PrintToPDF
            myreport.Sort = ""
            myreport.FilePrefix = GetReportPrefix(Me.CurrentUser)
            myreport.RenderReport()
            Return myreport.ReportFileName

        End Function
        Public Function GetAccountsSubmitted() As String
            Dim SprocParams As New CRFDB.SPROCS.cview_ClientView_GetAccountsSubmitted
            With SprocParams
                .UserId = Me.CurrentUser.Id
            End With

            Dim myview As DAL.COMMON.TableView = DBO.GetTableView(SprocParams)
            Dim myreport As New BLL.Common.StandardReport
            myreport.ReportDefPath = Me.CurrentUser.SettingsFolder & "Reports\WebReportsCollections.xml"
            myreport.OutputFolder = Me.CurrentUser.OutputFolder
            myreport.UserName = Me.CurrentUser.UserName
            myreport.ReportName = "AccountsSubmitted"
            myreport.TableIndex = 1
            myreport.DataView = myview
            myreport.PrintMode = Common.PrintMode.PrintToPDF
            myreport.Sort = ""
            myreport.FilePrefix = GetReportPrefix(Me.CurrentUser)
            myreport.RenderReport()
            Return myreport.ReportFileName

        End Function
        Public Shared Function GetReportPrefix(ByVal auser As Users.CurrentUser) As String
            Dim myuser As New ClientView.ClientUserInfo(auser.Id)
            Dim myprefix As String = myuser.Client.ClientCode
            myprefix += "-" & myuser.UserInfo.LoginCode
            myprefix += "-" & myuser.UserInfo.ID
            Return myprefix
        End Function

    End Class

End Namespace
