Namespace ClientView.Collection
    Public Class Processes
        Public Shared Function GetAccountInfo(ByVal auser As Users.CurrentUser, ByVal aitemId As Integer) As ClientView.Collection.AccountInfo
            Return New ClientView.Collection.AccountInfo(auser, aitemId)
        End Function
        Public Shared Function GetClosedReport(ByVal auser As Users.CurrentUser, ByVal aclientid As Integer, ByVal acontractid As Integer, ByVal asproc As CRFDB.SPROCS.uspbo_ClientView_GetAccountInventories, ByVal printmode As Common.PrintMode) As String
            With asproc
                .UserId = auser.Id
                .ContractId = acontractid
                .SelectByLastCloseDate = True
            End With

            Return ClientView.Collection.Reports.AccountsClosed(auser, DBO.GetTableView(asproc), printmode)

        End Function
        Public Shared Function GetMasterReport(ByVal auser As Users.CurrentUser, ByVal aclientid As Integer, ByVal acontractid As Integer, ByVal asproc As CRFDB.SPROCS.uspbo_ClientView_GetAccountInventories, ByVal printmode As Common.PrintMode) As String
            With asproc
                .UserId = auser.Id
                .ContractId = acontractid
                .SelectByReferalDate = True
            End With

            Return ClientView.Collection.Reports.AccountsMaster(auser, DBO.GetTableView(asproc), printmode)

        End Function
        Public Shared Function GetPaymentReport(ByVal auser As Users.CurrentUser, ByVal aclientid As Integer, ByVal acontractid As Integer, ByVal asproc As CRFDB.SPROCS.uspbo_ClientView_GetPaymentList, ByVal printmode As Common.PrintMode) As String
            With asproc
                .ContractId = acontractid
            End With
            Return ClientView.Collection.Reports.AccountsPayment(auser, DBO.GetTableView(asproc), printmode)

        End Function
        Public Shared Function GetAccountSubmittedReport(ByVal auser As Users.CurrentUser, ByVal aclientid As Integer, ByVal acontractid As Integer, ByVal asproc As CRFDB.SPROCS.uspbo_ClientView_GetAccountsSubmitted, ByVal printmode As Common.PrintMode) As String
            With asproc
                .UserId = auser.Id
                .ContractId = acontractid
            End With
            Return ClientView.Collection.Reports.AccountsSubmitted(auser, DBO.GetTableView(asproc), printmode)

        End Function
    End Class
End Namespace
