Namespace ClientView.Lien
    Public Class JobTexasRequest
        Inherits CRFDB.TABLES.JobTexasRequest
        Dim ObjectState As New ObjectState
        Dim _haslienservice As Boolean = True
        Dim _clientid As Integer
        Dim _contractid As Integer
        Dim myds As DataSet
        Public CurrentUser As Users.CurrentUser
        Public Sub New(ByVal auser As Users.CurrentUser)
            MyBase.New()
            Me.CurrentUser = auser
        End Sub
        Public Function SelectById(ByVal keyvalue As String) As Boolean
            Me.ObjectState.IsValid = DBO.Read(keyvalue, Me)
            Me.ObjectState.LastErrorMessage = DBO.LastError
            Return Me.ObjectState.IsValid
        End Function
        Public Function SelectFiltered(ByVal asql As String) As DAL.COMMON.TableView
            Return DBO.GetTableView(asql)
        End Function
        Public Function Insert() As Boolean
            Me.ObjectState.IsValid = DBO.Create(Me)
            Me.ObjectState.LastErrorMessage = DBO.LastError
            Return Me.ObjectState.IsValid
        End Function
        Public Function Update() As Boolean
            Me.ObjectState.IsValid = DBO.Update(Me)
            Me.ObjectState.LastErrorMessage = DBO.LastError
            Return Me.ObjectState.IsValid
        End Function
        Public Function Delete() As Boolean
            Me.ObjectState.IsValid = DBO.Create(Me)
            Me.ObjectState.LastErrorMessage = DBO.LastError
            Return Me.ObjectState.IsValid
        End Function
    End Class

End Namespace
