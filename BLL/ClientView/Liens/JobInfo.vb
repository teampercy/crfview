Namespace ClientView.Lien
    Public Class JobInfo
        Inherits CRFDB.TABLES.Job
        Dim ObjectState As New ObjectState
        Public SprocParams As New CRFDB.SPROCS.uspbo_ClientView_GetJobInfo
        Public myds As DataSet
        Dim _clientinfo As CRM.ClientView.ClientInfoForUser
        Public CurrentUser As Users.CurrentUser
        Public Sub New(ByVal auser As Users.CurrentUser)
            MyBase.New()
            Me.CurrentUser = auser
            _clientinfo = New CRM.ClientView.ClientInfoForUser(Me.CurrentUser.Id)

        End Sub
        Public ReadOnly Property ClientInfo() As CRM.ClientView.ClientInfoForUser
            Get
                Return _clientinfo
            End Get
        End Property
        Public Function SelectById(ByVal keyvalue As String) As Boolean
            SprocParams.JobId = keyvalue
            myds = DBO.GetDataSet(SprocParams)
            Me.ObjectState.IsValid = DBO.Read(keyvalue, Me)
            Me.ObjectState.LastErrorMessage = DBO.LastError
            Return Me.ObjectState.IsValid
        End Function
        Public Function Job() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(0), "")
        End Function
        Public Function Notes() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(1), "")
        End Function
        Public Function NoticeHistory() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(2), "")
        End Function
        Public Function LegalParties() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(3), "")
        End Function
        Public Function Customer() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(4), "")
        End Function
        Public Function GeneralContractor() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(5), "")
        End Function
        Public Function Client() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(6), "")
        End Function
        Public Function ClientLienInfo() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(7), "")
        End Function
        Public Function WaiverHistory() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(8), "")
        End Function
        Public Function JobAlerts() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(9), "")
        End Function
        Public Function JobTexasRequests() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(10), "")
        End Function
        Public Function OtherNoticeLog() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(11), "")
        End Function
        Public Function StateInfo() As DAL.COMMON.TableView
            Return CRF.BLL.Queries.GetView(New CRFDB.TABLES.StateInfo, True)
        End Function
        Public Function StateWaiverForms() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(13), "")
        End Function
        Public Function ClientSigners() As DAL.COMMON.TableView
            Dim mytable As New CRFDB.TABLES.ClientSigner
            Dim myquery As New DAL.COMMON.Query(mytable)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, TABLES.ClientSigner.ColumnNames.ClientId, DAL.COMMON.Conditions._IsEqual, Me.ClientId)
            Return DBO.GetTableView(myquery.BuildQuery)
        End Function
        Public Function StateOtherNotices() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(12), "")
        End Function
        Public Function StateOtherNoticeFields() As DAL.COMMON.TableView
            Return CRF.BLL.Queries.GetView(New CRFDB.TABLES.StateFormFields, True)
        End Function
        Public Function GetNotesForJob() As String
            Dim mytable As New CRFDB.VIEWS.vwJobNotes
            Dim myquery As New DAL.COMMON.Query(mytable)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, vwJobNotes.ColumnNames.JobId, DAL.COMMON.Conditions._IsEqual, Me.Id)
            Dim sql As String = myquery.BuildQuery
            Dim myreport As New BLL.Common.StandardReport
            myreport.ReportDefPath = Me.CurrentUser.SettingsFolder & "Reports\WebReportsLiens.xml"
            myreport.OutputFolder = Me.CurrentUser.OutputFolder
            myreport.UserName = Me.CurrentUser.UserName
            myreport.ReportName = "JobNotes"
            myreport.TableIndex = 1
            myreport.DataView = DBO.GetTableView(sql)
            myreport.PrintMode = Common.PrintMode.PrintToPDF
            myreport.Sort = ""
            myreport.RenderReport()
            If myreport.ReportFileName.Length < 1 Then
                Return Nothing
            Else
                Return myreport.ReportFileName
            End If
        End Function
        Public Function IsTexasJob() As Boolean
            Dim myview As New DAL.COMMON.TableView(myds.Tables(0), "")
            Dim MYSTATUS As String = myview.RowItem("StatusCode").ToString.ToUpper.Trim
            If myview.RowItem("JobState") = "TX" Then
                Select Case MYSTATUS
                    Case "AVO", "CJB"
                        Return True
                    Case Else
                        Return False
                End Select
            End If
            Return False
        End Function
    End Class
End Namespace
