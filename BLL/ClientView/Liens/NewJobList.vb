Namespace ClientView.Lien
    Public Class NewJobList
        Inherits Common.DoListPage
        Public SprocParams As New CRFDB.SPROCS.uspbo_ClientView_GetJobsSubmitted
        Dim _clientinfo As CRM.ClientView.ClientInfoForUser
        Public Sub New(ByVal auser As Users.CurrentUser)
            MyBase.New()
            Me.CurrentUser = auser
            _clientinfo = New CRM.ClientView.ClientInfoForUser(Me.CurrentUser.Id)
            SprocParams.SubmittedByUserId = CurrentUser.Id

        End Sub
        Public ReadOnly Property ClientInfo() As CRM.ClientView.ClientInfoForUser
            Get
                Return _clientinfo
            End Get
        End Property
        Public Overrides Function GetView() As DAL.COMMON.TableView

            With SprocParams
                .SubmittedByUserId = Me.CurrentUser.Id
                .BatchJobId = 0
            End With

            Dim myds As DataSet = DBO.GetDataSet(SprocParams)
            Return New DAL.COMMON.TableView(myds.Tables(0), "")

        End Function
        Public Function GetAckforBatchItem(ByVal aitemid As Integer) As String
            With SprocParams
                .SubmittedByUserId = Me.CurrentUser.Id
                .BatchJobId = aitemid
            End With

            Dim myreport As New BLL.Common.StandardReport
            myreport.ReportDefPath = Me.CurrentUser.SettingsFolder & "Reports\WebReportsLiens.xml"
            myreport.OutputFolder = Me.CurrentUser.OutputFolder
            myreport.UserName = Me.CurrentUser.UserName
            myreport.ReportName = "NewJobAck"
            myreport.TableIndex = 1
            myreport.DataView = DBO.GetTableView(Me.SprocParams)
            myreport.PrintMode = Common.PrintMode.PrintToPDF
            myreport.Sort = ""
            myreport.RenderReport()
            If myreport.ReportFileName.Length < 1 Then
                Return Nothing
            Else
                Return myreport.ReportFileName
            End If
        End Function
        Public Function GetJobsSubmitted() As String
            With SprocParams
                .SubmittedByUserId = Me.CurrentUser.Id
                .BatchJobId = 0
            End With
            Dim myview As DAL.COMMON.TableView = DBO.GetTableView(SprocParams)
            Dim myreport As New BLL.Common.StandardReport
            myreport.ReportDefPath = Me.CurrentUser.SettingsFolder & "Reports\WebReportsLiens.xml"
            myreport.OutputFolder = Me.CurrentUser.OutputFolder
            myreport.UserName = Me.CurrentUser.UserName
            myreport.ReportName = "JobsPlaced"
            myreport.TableIndex = 1
            myreport.DataView = myview
            myreport.PrintMode = Common.PrintMode.PrintToPDF
            myreport.Sort = ""
            myreport.RenderReport()
            Return myreport.ReportFileName

        End Function
    End Class
End Namespace
