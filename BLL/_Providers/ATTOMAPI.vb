﻿Imports System.Configuration
Imports System.IO
Imports System.Net
Imports System.Net.HttpWebResponse
Imports System.Xml
Imports System




Public Class ATTOMAPI

    Public Sub New()

    End Sub
    Private add1 As String
    Private add2 As String


    Public Class Apidata

        Public apn As String
        Public zoningType As String
        Public siteZoningIdent As String
        Public countrySubd As String
        Public countrysecsubd As String
        Public country As String
        Public line1 As String
        Public line2 As String
        Public locality As String
        Public postal1 As String
        Public latitude As String
        Public longitude As String
        Public propclass As String
        Public owner As String
        Public type As String
        Public description As String
        Public firstNameAndMi As String
        Public lastName As String
        Public owner2 As String
        Public mailingAddressOneLine As String
        Public amount As String
        Public interestRate As String
        Public deedType As String
        Public CompanyName As String
        Public owner2firstNameAndMi As String
        Public owner2lastName As String


    End Class


    Public Shared Function VerifyProperty(add1 As String, add2 As String) As String
        Try
            Dim result As String
            ServicePointManager.SecurityProtocol = DirectCast(3072, SecurityProtocolType)
            '  System.Net.ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls
            ''^^^**Commented by pooja for Do not hit api **^^^
            'Dim myReq As HttpWebRequest = DirectCast(WebRequest.Create("https://api.gateway.attomdata.com/propertyapi/v1.0.0/property/expandedprofile?address1='" + add1 + "'&address2='" + add2 + "'"), HttpWebRequest)
            Dim myReq As HttpWebRequest = DirectCast(WebRequest.Create("https://api.gateway.attomdata.com/propertyapi/v1.0.0/property/expandedprofile?address1=" + add1 + "&address2=" + add2 + ""), HttpWebRequest)

            myReq.Method = "GET"
            Dim key As String
            key = ConfigurationManager.AppSettings("AtomKey").ToString()
            ' myReq.Headers("Authorization") = "Basic " + Convert.ToBase64String(enc.GetBytes(Convert.ToString(USER_NAME & Convert.ToString(":")) & PASSWORD))
            myReq.Headers("apikey") = key
            'myReq.ContentType = "application/x-www-form-urlencoded;charset=UTF-8"
            'myReq.Headers("Accept:") = "application/xml"
            myReq.Accept = "application/xml"

            myReq.ContentType = "application/xml"

            '^^^**      **^^^

            Using response As WebResponse = myReq.GetResponse()
                Using reader As StreamReader = New StreamReader(response.GetResponseStream())
                    result = reader.ReadToEnd()
                    'result = "<Response><status><version>1.0.0</version><code>0</code><msg>SuccessWithResult</msg><total>1</total><page>1</page><pagesize>10</pagesize></status><property>   <identifier><obPropId>18471319108031</obPropId><fips>08031</fips><apn>0219204018000</apn><apnOrig>219204018000</apnOrig><attomId>184713191</attomId></identifier><lot> <depth>0</depth><frontage>0</frontage><lotnum>31</lotnum><lotsize1>0.1077</lotsize1><lotsize2>4690</lotsize2><pooltype>NONE</pooltype></lot><area><blockNum>36</blockNum><countrysecsubd>Denver County</countrysecsubd><countyuse1>113</countyuse1><muncode>DE</muncode><munname>DENVER</munname><srvyRange>68W</srvyRange><srvySection>19</srvySection><srvyTownship>03S</srvyTownship><subdname>BERKELEY</subdname><subdtractnum>0</subdtractnum><taxcodearea>0</taxcodearea></area><address><country>US</country><countrySubd>CO</countrySubd><line1>4529 WINONA CT</line1><line2>DENVER, CO 80212</line2><locality>Denver</locality><matchCode>ExaStr</matchCode><oneLine>4529 WINONA CT, DENVER, CO 80212</oneLine><postal1>80212</postal1><postal2>2512</postal2><postal3>C037</postal3></address><location><accuracy>Street</accuracy><elevation>0</elevation><latitude>39.778904</latitude><longitude>-105.047624</longitude><distance>0</distance><geoid>CO08031, CS0891007, DB0803360, ND0000119198, ND0004861239, PL0820000, SB0000076114, SB0000076155, SB0000076161, SB0000135819, SB0000143772, ZI80212</geoid></location><summary><absenteeInd>OWNER OCCUPIED</absenteeInd><propclass>Single Family Residence / Townhouse</propclass><propsubtype>RESIDENTIAL</propsubtype><proptype>SFR</proptype><yearbuilt>1900</yearbuilt><propLandUse>SFR</propLandUse><propIndicator>10</propIndicator><legal1>BERKELEY B36 L31 &amp; S/2 OF L32 EXC REAR 8FT TO CITY</legal1></summary></property></Response>"
                    'result = "<Response><status><version>1.0.0</version><code>0</code><msg>SuccessWithResult</msg><total>1</total><page>1</page><pagesize>10</pagesize><responseDateTime>2020-12-08T12:07:07</responseDateTime><transactionID>44d2dad0-1383-47a7-b8f4-e7e3bf856be0</transactionID></status><echoed_fields><jobID></jobID><loanNumber></loanNumber><preparedBy></preparedBy><resellerID></resellerID><preparedFor></preparedFor></echoed_fields><property><identifier><obPropId>18471319108031</obPropId><fips>08031</fips><apn>0219204018000</apn><attomId>184713191</attomId></identifier><lot><depth>0</depth><frontage>0</frontage><lotNum>31</lotNum><lotSize1>0.1077</lotSize1><lotSize2>4690</lotSize2><zoningType>Residential</zoningType><siteZoningIdent>U-SU-C1</siteZoningIdent></lot><area><blockNum>36</blockNum><countrySecSubd>Denver County</countrySecSubd><countyUse1>113</countyUse1><munCode>DE</munCode><munName>DENVER</munName><srvyRange>68W</srvyRange><srvySection>19</srvySection><srvyTownship>03S</srvyTownship><subdName>BERKELEY</subdName><subdTractNum>0</subdTractNum><taxCodeArea>0</taxCodeArea><censusTractIdent>102</censusTractIdent><censusBlockGroup>3</censusBlockGroup></area><address><country>US</country><countrySubd>CO</countrySubd><line1>4529 WINONA CT</line1><line2>DENVER, CO 80212</line2><locality>Denver</locality><matchCode>ExaStr</matchCode><oneLine>4529 WINONA CT, DENVER, CO 80212</oneLine><postal1>80212</postal1><postal2>2512</postal2><postal3>C037</postal3><stateFips>08</stateFips></address><location><accuracy>Street</accuracy><elevation>0</elevation><latitude>39.778904</latitude><longitude>-105.047624</longitude><distance>0</distance><geoid>CO08031, CS0891007, DB0803360, ND0000119198, ND0004861239, PL0820000, SB0000076114, SB0000076155, SB0000076161, SB0000135819, SB0000143772, ZI80212</geoid></location><summary><absenteeInd>OWNER OCCUPIED</absenteeInd><propClass>Single Family Residence / Townhouse</propClass><propSubType>RESIDENTIAL</propSubType><propType>SFR</propType><yearBuilt>1900</yearBuilt><propLandUse>SFR</propLandUse><propIndicator>10</propIndicator><legal1>BERKELEY B36 L31 &amp; S/2 OF L32 EXC REAR 8FT TO CITY</legal1><quitClaimFlag>False</quitClaimFlag><REOflag></REOflag></summary><utilities><heatingType>FORCED AIR</heatingType></utilities><sale><sellerName>STCHERBAK,LIDIA</sellerName><saleSearchDate>2015-8-21</saleSearchDate><saleTransDate>2015-8-21</saleTransDate><armsLengthIdent>1</armsLengthIdent><recorderThroughDate>2018-7-5</recorderThroughDate><amount><saleAmt>345000</saleAmt><saleRecDate>2015-8-25</saleRecDate><saleDisclosureType>0</saleDisclosureType><saleDocNum>0000119310</saleDocNum><saleTransType>Resale</saleTransType></amount><calculation><pricePerBed>172500</pricePerBed><pricePerSizeUnit>369</pricePerSizeUnit></calculation></sale><building><size><bldgSize>1414</bldgSize><grossSize>1414</grossSize><grossSizeAdjusted>934</grossSizeAdjusted><groundFloorSize>934</groundFloorSize><livingSize>934</livingSize><sizeInd>LIVING SQFT </sizeInd><universalSize>934</universalSize><atticSize>0</atticSize></size><rooms><bathFixtures>0</bathFixtures><baths1qtr>0</baths1qtr><baths3qtr>0</baths3qtr><bathsCalc>1</bathsCalc><bathsFull>1</bathsFull><bathsHalf>0</bathsHalf><bathsTotal>1</bathsTotal><beds>2</beds><roomsTotal>5</roomsTotal></rooms><interior><bsmtSize>480</bsmtSize><bsmtType>UNFINISHED</bsmtType><bsmtFinishedPercent>0</bsmtFinishedPercent><fplcCount>1</fplcCount><fplcInd>Y</fplcInd><fplcType>YES</fplcType></interior><construction><condition>AVERAGE</condition><wallType>BRICK</wallType><propertyStructureMajorImprovementsYear>0</propertyStructureMajorImprovementsYear></construction><parking><garageType>DETACHED GARAGE</garageType><prkgSize>240</prkgSize><prkgSpaces>0</prkgSpaces><prkgType>GARAGE DETACHED</prkgType></parking><summary><levels>1</levels><storyDesc>TYPE UNKNOWN</storyDesc><unitsCount>1</unitsCount><viewCode></viewCode></summary></building><assessment><appraised><apprImprValue>0</apprImprValue><apprLandValue>0</apprLandValue><apprTtlValue>0</apprTtlValue></appraised><assessed><assdImprValue>11276</assdImprValue><assdLandValue>19634</assdLandValue><assdTtlValue>30910</assdTtlValue></assessed><market><mktImprValue>157700</mktImprValue><mktLandValue>274600</mktLandValue><mktTtlValue>432300</mktTtlValue></market><tax><taxAmt>2229</taxAmt><taxPerSizeUnit>2.39</taxPerSizeUnit><taxYear>2019</taxYear><exemption><ExemptionAmount1>0</ExemptionAmount1><ExemptionAmount2>0</ExemptionAmount2><ExemptionAmount3>0</ExemptionAmount3><ExemptionAmount4>0</ExemptionAmount4><ExemptionAmount5>0</ExemptionAmount5></exemption></tax><delinquentyear>0</delinquentyear><improvementPercent>36</improvementPercent><fullCashValue>0</fullCashValue><owner><type>INDIVIDUAL</type><description>Individual</description><owner1><lastName>MATTICE</lastName><firstNameAndMi>MICHAEL S</firstNameAndMi></owner1><owner2><lastName>MATTICE</lastName><firstNameAndMi>SHAREE</firstNameAndMi></owner2><absenteeOwnerStatus>O</absenteeOwnerStatus><mailingAddressOneLine>4529 WINONA CT, DENVER,, CO 80212-2512</mailingAddressOneLine></owner><mortgage><FirstConcurrent><amount>0</amount><interestRate>0</interestRate><deedType>WD</deedType></FirstConcurrent><SecondConcurrent><amount>0</amount></SecondConcurrent><ThirdConcurrent><amount>0</amount></ThirdConcurrent><title><companyName>FIRST AMERICAN TITLE</companyName><companyCode>18618</companyCode></title></mortgage></assessment><vintage><lastModified>2020-1-10</lastModified><pubDate>2020-1-16</pubDate></vintage></property></Response>"
                    'result = "<Response><status><version>1.0.0</version><code>0</code><msg>SuccessWithResult</msg><total>1</total><page>1</page><pagesize>10</pagesize><responseDateTime>2020-12-08T12:07:07</responseDateTime><transactionID>44d2dad0-1383-47a7-b8f4-e7e3bf856be0</transactionID></status><echoed_fields><jobID></jobID><loanNumber></loanNumber><preparedBy></preparedBy><resellerID></resellerID><preparedFor></preparedFor></echoed_fields><property><identifier><obPropId>18471319108031</obPropId><fips>08031</fips><apn>0219204018000</apn><attomId>184713191</attomId></identifier><lot><depth>0</depth><frontage>0</frontage><lotNum>31</lotNum><lotSize1>0.1077</lotSize1><lotSize2>4690</lotSize2><zoningType>Residential</zoningType></lot><area><blockNum>36</blockNum><countrySecSubd>Denver County</countrySecSubd><countyUse1>113</countyUse1><munCode>DE</munCode><munName>DENVER</munName><srvyRange>68W</srvyRange><srvySection>19</srvySection><srvyTownship>03S</srvyTownship><subdName>BERKELEY</subdName><subdTractNum>0</subdTractNum><taxCodeArea>0</taxCodeArea><censusTractIdent>102</censusTractIdent><censusBlockGroup>3</censusBlockGroup></area><address><country>US</country><countrySubd>CO</countrySubd><line1>4529 WINONA CT</line1><line2>DENVER, CO 80212</line2><locality>Denver</locality><matchCode>ExaStr</matchCode><oneLine>4529 WINONA CT, DENVER, CO 80212</oneLine><postal1>80212</postal1><postal2>2512</postal2><postal3>C037</postal3><stateFips>08</stateFips></address><location><accuracy>Street</accuracy><elevation>0</elevation><latitude>39.778904</latitude><longitude>-105.047624</longitude><distance>0</distance><geoid>CO08031, CS0891007, DB0803360, ND0000119198, ND0004861239, PL0820000, SB0000076114, SB0000076155, SB0000076161, SB0000135819, SB0000143772, ZI80212</geoid></location><summary><absenteeInd>OWNER OCCUPIED</absenteeInd><propClass>Single Family Residence / Townhouse</propClass><propSubType>RESIDENTIAL</propSubType><propType>SFR</propType><yearBuilt>1900</yearBuilt><propLandUse>SFR</propLandUse><propIndicator>10</propIndicator><legal1>BERKELEY B36 L31 &amp; S/2 OF L32 EXC REAR 8FT TO CITY</legal1><quitClaimFlag>False</quitClaimFlag><REOflag></REOflag></summary><utilities><heatingType>FORCED AIR</heatingType></utilities><sale><sellerName>STCHERBAK,LIDIA</sellerName><saleSearchDate>2015-8-21</saleSearchDate><saleTransDate>2015-8-21</saleTransDate><armsLengthIdent>1</armsLengthIdent><recorderThroughDate>2018-7-5</recorderThroughDate><amount><saleAmt>345000</saleAmt><saleRecDate>2015-8-25</saleRecDate><saleDisclosureType>0</saleDisclosureType><saleDocNum>0000119310</saleDocNum><saleTransType>Resale</saleTransType></amount><calculation><pricePerBed>172500</pricePerBed><pricePerSizeUnit>369</pricePerSizeUnit></calculation></sale><building><size><bldgSize>1414</bldgSize><grossSize>1414</grossSize><grossSizeAdjusted>934</grossSizeAdjusted><groundFloorSize>934</groundFloorSize><livingSize>934</livingSize><sizeInd>LIVING SQFT </sizeInd><universalSize>934</universalSize><atticSize>0</atticSize></size><rooms><bathFixtures>0</bathFixtures><baths1qtr>0</baths1qtr><baths3qtr>0</baths3qtr><bathsCalc>1</bathsCalc><bathsFull>1</bathsFull><bathsHalf>0</bathsHalf><bathsTotal>1</bathsTotal><beds>2</beds><roomsTotal>5</roomsTotal></rooms><interior><bsmtSize>480</bsmtSize><bsmtType>UNFINISHED</bsmtType><bsmtFinishedPercent>0</bsmtFinishedPercent><fplcCount>1</fplcCount><fplcInd>Y</fplcInd><fplcType>YES</fplcType></interior><construction><condition>AVERAGE</condition><wallType>BRICK</wallType><propertyStructureMajorImprovementsYear>0</propertyStructureMajorImprovementsYear></construction><parking><garageType>DETACHED GARAGE</garageType><prkgSize>240</prkgSize><prkgSpaces>0</prkgSpaces><prkgType>GARAGE DETACHED</prkgType></parking><summary><levels>1</levels><storyDesc>TYPE UNKNOWN</storyDesc><unitsCount>1</unitsCount><viewCode></viewCode></summary></building><assessment><appraised><apprImprValue>0</apprImprValue><apprLandValue>0</apprLandValue><apprTtlValue>0</apprTtlValue></appraised><assessed><assdImprValue>11276</assdImprValue><assdLandValue>19634</assdLandValue><assdTtlValue>30910</assdTtlValue></assessed><market><mktImprValue>157700</mktImprValue><mktLandValue>274600</mktLandValue><mktTtlValue>432300</mktTtlValue></market><tax><taxAmt>2229</taxAmt><taxPerSizeUnit>2.39</taxPerSizeUnit><taxYear>2019</taxYear><exemption><ExemptionAmount1>0</ExemptionAmount1><ExemptionAmount2>0</ExemptionAmount2><ExemptionAmount3>0</ExemptionAmount3><ExemptionAmount4>0</ExemptionAmount4><ExemptionAmount5>0</ExemptionAmount5></exemption></tax><delinquentyear>0</delinquentyear><improvementPercent>36</improvementPercent><fullCashValue>0</fullCashValue><owner><type>INDIVIDUAL</type><description>Individual</description><owner1><lastName>MATTICE</lastName><firstNameAndMi>MICHAEL S</firstNameAndMi></owner1><owner2><lastName>MATTICE</lastName><firstNameAndMi>SHAREE</firstNameAndMi></owner2><absenteeOwnerStatus>O</absenteeOwnerStatus><mailingAddressOneLine>4529 WINONA CT, DENVER,, CO 80212-2512</mailingAddressOneLine></owner><mortgage><FirstConcurrent><amount>0</amount><interestRate>0</interestRate><deedType>WD</deedType></FirstConcurrent><SecondConcurrent><amount>0</amount></SecondConcurrent><ThirdConcurrent><amount>0</amount></ThirdConcurrent><title><companyName>FIRST AMERICAN TITLE</companyName><companyCode>18618</companyCode></title></mortgage></assessment><vintage><lastModified>2020-1-10</lastModified><pubDate>2020-1-16</pubDate></vintage></property></Response>"

                    'System.Diagnostics.Debug.WriteLine(result)
                    ' result = "bad request"


                End Using
            End Using
            Return result
        Catch ex As Exception
            Dim result = ex.Message


            Console.Error.WriteLine("AddressApi LOG", "result from api ", result.ToString, Now)
            ' Return False
        End Try

        'Return result
    End Function

End Class
