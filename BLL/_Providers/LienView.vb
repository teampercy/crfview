﻿Imports Microsoft.VisualBasic
Imports System.Text
Imports System.Net.Mail
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Net
Imports System.IO
Imports System.Globalization
Imports System.Data
Imports System.Xml
Imports CRF.CLIENTVIEW.BLL.CRFDB
Imports CRF.CLIENTVIEW.BLL.CRFDB.VIEWS
Imports System.Configuration.ConfigurationManager
Imports System.Reflection
Namespace LienView
    Public Class Provider
        Public Shared Function PrintInvoice(ByVal myuser As BLL.Users.CurrentUser, ByVal adatatable As DataTable, ByVal areportdef As String, ByVal areportname As String, Optional ByVal aprintmode As BLL.COMMON.PrintMode = BLL.COMMON.PrintMode.PrintToPDF, Optional ByVal areportprefix As String = "", Optional ByVal asubtitle1 As String = "", Optional ByVal asubtitle2 As String = "", Optional ByVal aisLandscape As Boolean = False) As String
            Dim myreport As New Liens.Reports.PrintInvoiceXML
            ' myreport.ExecuteC1Report()
            myreport.RenderC1Report(myuser, adatatable, areportdef, areportname, aprintmode, "", asubtitle1, asubtitle2)
            Return myreport.ReportPath

            'PrelimAckBranchDaily
            'Dim fromdate As Date = "05/09/2019"
            'Dim thrudate As Date = "05/10/2019"

            'PrelimAckDaily
            'Dim fromdate As Date = "05/28/2019"
            'Dim thrudate As Date = "05/29/2019"

            'Dim report As New BLL.ScheduledReports.DayEndPrelimAcks(fromdate, thrudate)
            'report.DoTask("", 3)

            'DayEndPrelimSentReport
            'Dim fromdate As Date = "03/25/2019"
            'Dim thrudate As Date = "03/26/2019"
            'Dim DayEndPrelimSentReport As New BLL.ScheduledReports.DayEndPrelimSent
            'DayEndPrelimSentReport.DoTask("uspbo_LiensDayEnd_GetPrelimSentByEmail '" & fromdate & "','" & thrudate & "'", 3)

            'Return True

        End Function
    End Class
End Namespace