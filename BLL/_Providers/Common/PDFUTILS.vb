Imports PdfSharp
Imports PdfSharp.Drawing
Imports PdfSharp.Drawing.Layout
Imports PdfSharp.Pdf
Imports PdfSharp.Pdf.IO
Namespace COMMON
    Public Class PDFUTILS
        Public Shared Function TEXT2PDF(ByVal ATEXT As String, ByVal AFILENAME As String) As String
            ' Create a new PDF document
            Dim document As PdfDocument = New PdfDocument

            ' Create an empty page
            Dim page As PdfPage = document.AddPage

            '' Get an XGraphics object for drawing
            Dim gfx As XGraphics = XGraphics.FromPdfPage(page)
            Dim tx As New XTextFormatter(gfx)

            '' Draw crossing lines
            'Dim pen As XPen = New XPen(XColor.FromArgb(255, 0, 0))
            'gfx.DrawLine(pen, New XPoint(0, 0), New XPoint(page.Width.Point, page.Height.Point))
            'gfx.DrawLine(pen, New XPoint(page.Width.Point, 0), New XPoint(0, page.Height.Point))

            '' Draw an ellipse
            'gfx.DrawEllipse(pen, 3 * page.Width.Point / 10, 3 * page.Height.Point / 10, 2 * page.Width.Point / 5, 2 * page.Height.Point / 5)

            ' Create a font
            Dim font As XFont = New XFont("Arial", 9, XFontStyle.Regular)
            ' Draw the text
            'gfx.DrawString(ATEXT, font, XBrushes.Black, _
            'New XRect(0, 0, page.Width.Point, page.Height.Point), XStringFormat.Center)
            'tx.DrawString(ATEXT, font, XBrushes.Black, New XRect(0, 0, page.Width.Point, page.Height.Point), XStringFormat.Center)
            Dim rect As New XRect(0, 0, page.Width.Point, page.Height.Point)
            gfx.DrawRectangle(XBrushes.SeaShell, rect)
            tx.Alignment = XParagraphAlignment.Justify
            tx.DrawString(ATEXT, font, XBrushes.Black, rect, XStringFormat.TopLeft)

            ' Save the document...
            Dim filename As String = AFILENAME
            document.Save(filename)
            Return filename

        End Function
        Public Shared Function Combine(ByVal afiles As ArrayList, ByVal adestfilename As String) As Boolean

            Try

                If System.IO.Directory.Exists(Application.StartupPath & "\TEMP\") Then
                    System.IO.Directory.Delete(Application.StartupPath & "\TEMP\", True)
                End If

                System.IO.Directory.CreateDirectory(Application.StartupPath & "\TEMP\")

                Dim FILE As String
                For Each FILE In afiles
                    Dim OUTFILE As String = Application.StartupPath & "\TEMP\" & System.IO.Path.GetFileName(FILE)
                    If System.IO.File.Exists(FILE) = True Then
                        System.IO.File.Copy(FILE, OUTFILE, True)
                    End If
                Next

                Dim S As String = Application.StartupPath & "\PDFTK  " & Application.StartupPath & "\TEMP\*.PDF CAT OUTPUT " & Application.StartupPath & "\TEMP\ONE.PDF"

                Shell(S, AppWinStyle.Hide, True)
                Application.DoEvents()

                System.IO.File.Copy(Application.StartupPath & "\TEMP\ONE.PDF", adestfilename)

            Catch EX As Exception
                MsgBox(EX.Message, MsgBoxStyle.Critical)
                Return False
            Finally
            End Try
            Return True

        End Function

    End Class

End Namespace
