Imports System.Text
Namespace Common
    Public Class ErrorHandler
        Public Shared Sub Logger(ByVal auser As CRF.BLL.Users.CurrentUser, ByVal aoperation As String, ByVal ex As Exception)
            Dim err As String = BuildMessage(auser, aoperation, ex)
            CRF.BLL.Common.FileOps.Writefile(GetFileName(auser, aoperation), err)

        End Sub
        Private Shared Function GetFileName(ByVal auser As CRF.BLL.Users.CurrentUser, ByVal aoperation As String) As String
            Dim sfilename As String = auser.ErrorLogFolder & auser.UserName & "-" & aoperation
            sfilename += "-" & Format(Now(), "yyyyMMddhhmmss")
            sfilename += ".HTML"
            If System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(sfilename)) = False Then
                System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(sfilename))
            End If

            Return sfilename

        End Function
        Private Shared Function BuildMessage(ByVal auser As CRF.BLL.Users.CurrentUser, ByVal aoperation As String, ByVal ex As Exception) As String

            Dim strMessage As New StringBuilder
            strMessage.Append("<!DOCTYPE HTML PUBLIC ""-//W3C//DTD HTML 4.01 Transitional//EN"">")
            strMessage.Append("<html>")
            strMessage.Append("<head>")
            strMessage.Append("<title>Page Error</title>")
            strMessage.Append("<meta http-equiv=""Content-Type"" content=""text/html; charset=iso-8859-1"">")
            strMessage.Append("<style type=""text/css"">")
            strMessage.Append("<!--")
            strMessage.Append(".basix {")
            strMessage.Append("font-family: Verdana, Arial, Helvetica, sans-serif;")
            strMessage.Append("font-size: 12px;")
            strMessage.Append("}")
            strMessage.Append(".header1 {")
            strMessage.Append("font-family: Verdana, Arial, Helvetica, sans-serif;")
            strMessage.Append("font-size: 12px;")
            strMessage.Append("font-weight: bold;")
            strMessage.Append("color: #000099;")
            strMessage.Append("}")
            strMessage.Append(".tlbbkground1 {")
            strMessage.Append("background-color:  #000099;")
            strMessage.Append("}")
            strMessage.Append("-->")
            strMessage.Append("</style>")
            strMessage.Append("</head>")
            strMessage.Append("<body>")
            strMessage.Append("<table width=""85%"" border=""0"" align=""center"" cellpadding=""5"" cellspacing=""1"" class=""tlbbkground1"">")
            strMessage.Append("<tr bgcolor=""#eeeeee"">")
            strMessage.Append("<td colspan=""2"" class=""header1"">Page Error</td>")
            strMessage.Append("</tr>")
            strMessage.Append("<tr>")
            strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Time</td>")
            strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & System.DateTime.Now & " EST</td>")
            strMessage.Append("</tr>")
            strMessage.Append("<tr>")
            strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Error Message</td>")
            strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & ex.Message.ToString & "</td>")
            strMessage.Append("</tr>")

            If aoperation.Length > 1 Then
                strMessage.Append("<tr>")
                strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Operation</td>")
                strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & aoperation & "</td>")
                strMessage.Append("</tr>")
            End If


            If ex.Source.Length > 1 Then
                strMessage.Append("<tr>")
                strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Source</td>")
                strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & ex.Source.ToString & "</td>")
                strMessage.Append("</tr>")
            End If

            If ex.StackTrace.Length > 1 Then
                strMessage.Append("<tr>")
                strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Stack Trace</td>")
                strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & ex.StackTrace.ToString & "</td>")
                strMessage.Append("</tr>")
            End If

            If IsNothing(ex.InnerException) = False Then
                strMessage.Append("<tr>")
                strMessage.Append("<td width=""100"" align=""right"" bgcolor=""#eeeeee"" class=""header1"" nowrap>Inner Exception</td>")
                strMessage.Append("<td bgcolor=""#FFFFFF"" class=""basix"">" & ex.InnerException.ToString & "</td>")
                strMessage.Append("</tr>")
            End If

            strMessage.Append("</table>")
            strMessage.Append("</body>")
            strMessage.Append("</html>")
            Return strMessage.ToString

        End Function
    End Class
End Namespace

