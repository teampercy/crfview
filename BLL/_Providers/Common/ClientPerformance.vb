Imports System
Imports System.Windows.Forms
Imports C1
Imports System.Drawing
Imports System.Drawing.Imaging
Imports ZedGraph
Imports C1.C1Report
Namespace Common.Reports
    Public Class ClientPerformance
        Inherits Common.StandardReport
        Dim m_rtf As RichTextBox
        Dim m_filename As String
        Dim m_formfolder As String
        Dim _reportpath As String
        Dim _settingsfolder As String
        Dim _reportdefpath As String
        Dim _username As String
        Dim srtf As String
        Public CurrentUser As Users.CurrentUser
        Dim myds As DataSet
        Dim mysitestats As HDS.DAL.COMMON.TableView
        Public Sub New(ByVal auser As Users.CurrentUser)
            MyBase.New()
            Me.CurrentUser = auser
            Me.OutputFolder = auser.OutputFolder
            Me.SettingsFolder = auser.SettingsFolder
            Me.UserName = auser.UserName
            Me.ReportDefPath = Me.SettingsFolder & "Reports\SiteStats.xml"
            Me.ReportName = "CLIENTSTATS"

        End Sub
        Public Function ExecuteC1Report() As String
            Dim mysproc As New CRF.BLL.CRFDB.SPROCS.uspbo_CRM_GetPerformance
            myds = DBO.GetDataSet(mysproc)
            mysitestats = New HDS.DAL.COMMON.TableView(myds.Tables(0))
            If mysitestats.Count = 1 Then
                Me.DataView = mysitestats
                With Me
                    .TableIndex = 0
                    .RenderReport()
                    .ReportPath = .ReportFileName
                    Return .ReportFileName

                End With
            Else
                Return Nothing
            End If


        End Function
        Public Overrides Sub StartSection(ByVal sender As Object, ByVal e As C1.C1Report.ReportEventArgs)

            Dim C1R As C1.C1Report.C1Report = sender
            Try
                If HasField("CHART1", e) = True Then
                    Dim ZIMAGE As Field = C1R.Fields("CHART1")
                    ZIMAGE.Picture = GetChart1(New HDS.DAL.COMMON.TableView(myds.Tables(3)))
                    ZIMAGE.PictureScale = PictureScaleEnum.Stretch
                End If
                If HasField("CHART2", e) = True Then
                    Dim ZIMAGE As Field = C1R.Fields("CHART2")
                    ZIMAGE.Visible = False

                    '    ZIMAGE.Picture = GetChart2(New HDS.DAL.COMMON.TableView(myds.Tables(3)))
                    '    ZIMAGE.PictureScale = PictureScaleEnum.Stretch
                End If
                If HasField("CHART3", e) = True Then
                    Dim ZIMAGE As Field = C1R.Fields("CHART3")
                    ZIMAGE.Visible = False

                    '    ZIMAGE.Picture = GetChart3(New HDS.DAL.COMMON.TableView(myds.Tables(3)))
                    '   ZIMAGE.PictureScale = PictureScaleEnum.Stretch
                End If
                If HasField("CHART4", e) = True Then
                    Dim ZIMAGE As Field = C1R.Fields("CHART4")
                    ZIMAGE.Visible = False
                    '  ZIMAGE.Picture = GetChart4(New HDS.DAL.COMMON.TableView(myds.Tables(3)))
                    ' ZIMAGE.PictureScale = PictureScaleEnum.Stretch
                End If

            Catch ex As Exception
                Dim s As String = String.Empty

            End Try

        End Sub
        Private Function HasField(ByVal aname As String, ByVal e As C1.C1Report.ReportEventArgs) As Boolean
            Try
                Dim Zfield As Field = C1R.Fields(aname)
                If UCase(Zfield.Name) = UCase(aname) And e.Section = Zfield.Section Then
                    Return True
                Else
                    Return False
                End If
            Catch
                Return False
            End Try

        End Function
        Friend Function GetChart1(ByVal data As HDS.DAL.COMMON.TableView)
            Dim zgc As New ZedGraphControl
            zgc.Dock = DockStyle.Fill
            zgc.Height = 500
            zgc.Width = 500

            data.MoveFirst()
            Dim y(19) As Double
            ' Make up some random data points
            Dim labels(19) As String
            Dim idx As Integer = 0
            Dim S As String = String.Empty & "Ranking" & Chr(10) & Chr(10)

            Do Until idx >= 10
                Dim i As String = "00" & idx + 1
                Dim u As Integer = data.RowItem("TotalFees")
                S += "" & Right(i, 2).ToString.Trim & "-" & data.RowItem("ParentClient") & " " & Strings.FormatCurrency(data.RowItem("TotalFees") / 1000, 0) & Chr(10)
                labels(idx) = idx + 1
                y(idx) = u
                data.MoveNext()
                idx += 1
            Loop

            Dim myPane As GraphPane = zgc.GraphPane

            ' Set the titles and axis labels
            myPane.Title.Text = "Top 10 Client By Revenue ( YTD) "
            myPane.XAxis.Title.Text = "Rank"
            myPane.YAxis.Title.Text = "Dollars"

      
            ' Generate a red bar with "Curve 1" in the legend
            Dim myBar As BarItem = myPane.AddBar("", Nothing, y, Color.RosyBrown)
            myBar.Bar.Fill = New Fill(Color.Red, Color.White, Color.RosyBrown)


            '' Generate a black line with "Curve 4" in the legend
            Dim myCurve As LineItem = myPane.AddCurve("", _
               Nothing, y, Color.Black, SymbolType.Circle)
            myCurve.Line.Fill = New Fill(Color.White, Color.LightSkyBlue, -45.0F)

            ' Fix up the curve attributes a little
            myCurve.Symbol.Size = 8.0F
            myCurve.Symbol.Fill = New Fill(Color.White)
            myCurve.Line.Width = 2.0F

            ' Draw the X tics between the labels instead of at the labels
            myPane.XAxis.MajorTic.IsBetweenLabels = True

            ' Set the XAxis labels
            myPane.XAxis.Scale.TextLabels = labels
            ' Set the XAxis to Text type
            myPane.XAxis.Type = AxisType.Text

            ' Fill the axis area with a gradient
            myPane.Chart.Fill = New Fill(Color.White, _
               Color.FromArgb(255, 255, 166), 90.0F)
            ' Fill the pane area with a solid color
            myPane.Fill = New Fill(Color.FromArgb(250, 250, 255))


            Dim TOTAL As Integer = 10000

            ' Make a text label to highlight the total value
            'S += "02 - ABC200" & Chr(10)
            'S += "03 - ABC200" & Chr(10)
            'S += "04 - ABC200" & Chr(10)
            'S += "05 - ABC200" & Chr(10)
            'S += "06 - ABC200" & Chr(10)
            'S += "07 - ABC200" & Chr(10)
            'S += "08 - ABC200" & Chr(10)
            'S += "09 - ABC200" & Chr(10)
            'S += "10 - ABC200"

            Dim text As New TextObj(S, _
                        0.65F, 0.45F, CoordType.PaneFraction)
            text.Location.AlignH = AlignH.Left
            text.Location.AlignV = AlignV.Bottom
            text.FontSpec.Border.IsVisible = False
            text.FontSpec.Fill = New Fill(Color.White, Color.PowderBlue, 45.0F)
            text.FontSpec.StringAlignment = StringAlignment.Near
            text.Location.Height = 100

            myPane.GraphObjList.Add(text)

            ' Create a drop shadow for the total value text item
            Dim text2 As New TextObj(text)
            text2.FontSpec.Fill = New Fill(Color.Black)
            text2.Location.X += 0.008F
            text2.Location.Y += 0.01F
            myPane.GraphObjList.Add(text2)

            ' Calculate the Axis Scale Ranges
            zgc.AxisChange()
            Dim myimg As Bitmap = zgc.GetImage
            Return myimg

        End Function
        Friend Function GetChart4(ByVal data As HDS.DAL.COMMON.TableView)
            Dim zgc As New ZedGraphControl
            zgc.Dock = DockStyle.Fill
            zgc.Height = 500
            zgc.Width = 500

            Dim myPane As GraphPane = zgc.GraphPane

            ' Set the title and axis labels
            myPane.Title.Text = "Horizontal Bars with Value Labels Above Each Bar"
            myPane.XAxis.Title.Text = "Some Random Thing"
            myPane.YAxis.Title.Text = "Position Number"

            ' Create data points for three BarItems using Random data
            Dim list As New PointPairList()
            Dim list2 As New PointPairList()
            Dim list3 As New PointPairList()
            Dim rand As New Random()

            Dim i As Integer = 0
            While i < 11
                Dim y As Double = DirectCast(i, Integer) + 5
                Dim x As Double = rand.NextDouble() * 1000
                Dim x2 As Double = rand.NextDouble() * 1000
                Dim x3 As Double = rand.NextDouble() * 1000
                list.Add(x, y)
                list2.Add(x2, y)
                list3.Add(x3, y)
                System.Math.Max(System.Threading.Interlocked.Increment(i), i - 1)
            End While

            ' Create the three BarItems, change the fill properties so the angle is at 90
            ' degrees for horizontal bars
            Dim myCurve As BarItem = myPane.AddBar("curve 1", list, Color.Blue)
            myCurve.Bar.Fill = New Fill(Color.Blue, Color.White, Color.Blue, 90)
            Dim myCurve2 As BarItem = myPane.AddBar("curve 2", list2, Color.Red)
            myCurve2.Bar.Fill = New Fill(Color.Red, Color.White, Color.Red, 90)
            Dim myCurve3 As BarItem = myPane.AddBar("curve 3", list3, Color.Green)
            myCurve3.Bar.Fill = New Fill(Color.Green, Color.White, Color.Green, 90)

            ' Set BarBase to the YAxis for horizontal bars
            myPane.BarSettings.Base = BarBase.Y

            ' Fill the chart background with a color gradient
            myPane.Chart.Fill = New Fill(Color.White, Color.FromArgb(255, 255, 166), 45.0F)

            
            zgc.AxisChange()

            ' Create TextObj's to provide labels for each bar
            BarItem.CreateBarLabels(myPane, False, "f0")
            zgc.AxisChange()
            Dim myimg As Bitmap = zgc.GetImage
            Return myimg

        End Function

        ' Call this method from the Form_Load method, passing your ZedGraphControl
        Friend Function GetChart2(ByVal data As HDS.DAL.COMMON.TableView)
            Dim zgc As New ZedGraphControl
            zgc.Height = 500
            zgc.Width = 500
            Dim myPane As GraphPane = zgc.GraphPane

            ' Set the GraphPane title
            myPane.Title.Text = "2004 ZedGraph Sales by Region" & Chr(10) & "($M)"
            myPane.Title.FontSpec.IsItalic = True
            myPane.Title.FontSpec.Size = 24.0F
            myPane.Title.FontSpec.Family = "Times New Roman"

            ' Fill the pane background with a color gradient
            myPane.Fill = New Fill(Color.White, Color.Goldenrod, 45.0F)
            ' No fill for the chart background
            myPane.Chart.Fill.Type = FillType.None

            ' Set the legend to an arbitrary location
            myPane.Legend.Position = LegendPos.Float
            myPane.Legend.Location = New Location(0.95F, 0.15F, CoordType.PaneFraction, _
                        AlignH.Right, AlignV.Top)
            myPane.Legend.FontSpec.Size = 10.0F
            myPane.Legend.IsHStack = False

            ' Add some pie slices
            Dim segment1 As PieItem = myPane.AddPieSlice(20, Color.Navy, Color.White, 45.0F, 0, "North")
            Dim segment3 As PieItem = myPane.AddPieSlice(30, Color.Purple, Color.White, 45.0F, 0.0, "East")
            Dim segment4 As PieItem = myPane.AddPieSlice(10.21, Color.LimeGreen, Color.White, 45.0F, 0, "West")
            Dim segment2 As PieItem = myPane.AddPieSlice(40, Color.SandyBrown, Color.White, 45.0F, 0.2, "South")
            Dim segment6 As PieItem = myPane.AddPieSlice(250, Color.Red, Color.White, 45.0F, 0, "Europe")
            Dim segment7 As PieItem = myPane.AddPieSlice(50, Color.Blue, Color.White, 45.0F, 0.2, "Pac Rim")
            Dim segment8 As PieItem = myPane.AddPieSlice(400, Color.Green, Color.White, 45.0F, 0, "South America")
            Dim segment9 As PieItem = myPane.AddPieSlice(50, Color.Yellow, Color.White, 45.0F, 0.2, "Africa")

            segment2.LabelDetail.FontSpec.FontColor = Color.Red

            ' Sum up the pie values                                                               
            Dim curves As CurveList = myPane.CurveList
            Dim total As Double = 0, i As Integer
            Dim pie As PieItem
            For i = 0 To curves.Count - 1
                pie = curves(i)
                total += pie.Value
            Next i

            ' Make a text label to highlight the total value
            Dim text As New TextObj("Total 2004 Sales" + Chr(10) + "$" + total.ToString() + "M", _
                        0.18F, 0.4F, CoordType.PaneFraction)
            text.Location.AlignH = AlignH.Center
            text.Location.AlignV = AlignV.Bottom
            text.FontSpec.Border.IsVisible = False
            text.FontSpec.Fill = New Fill(Color.White, Color.FromArgb(255, 100, 100), 45.0F)
            text.FontSpec.StringAlignment = StringAlignment.Center
            myPane.GraphObjList.Add(text)

            ' Create a drop shadow for the total value text item
            Dim text2 As New TextObj(text)
            text2.FontSpec.Fill = New Fill(Color.Black)
            text2.Location.X += 0.008F
            text2.Location.Y += 0.01F
            myPane.GraphObjList.Add(text2)

            ' Calculate the Axis Scale Ranges
            zgc.AxisChange()
            Dim myimg As Bitmap = zgc.GetImage
            Return myimg

        End Function
        Friend Function GetChart3(ByVal data As HDS.DAL.COMMON.TableView)
            Dim zgc As New ZedGraphControl
            zgc.Height = 500
            zgc.Width = 500
            Dim myPane As GraphPane = zgc.GraphPane

            ' Set the title and axis labels
            myPane.Title.Text = "2004 ZedGraph Sales by Region" & Chr(10) & "($M)"
            myPane.Title.FontSpec.IsItalic = True
            myPane.Title.FontSpec.Size = 24.0F
            myPane.Title.FontSpec.Family = "Times New Roman"

            Dim list As New PointPairList()
            Dim list2 As New PointPairList()
            Dim list3 As New PointPairList()
            Dim rand As New Random()

            ' Generate random data for three curves
            Dim i As Integer = 0
            While i < 24
                Dim x As Double = DirectCast(i, Integer)
                Dim y As Double = rand.NextDouble() * 1000
                Dim y2 As Double = rand.NextDouble() * 1000
                Dim y3 As Double = rand.NextDouble() * 1000
                list.Add(x, y)
                list2.Add(x, y2)
                list3.Add(x, y3)
                System.Math.Max(System.Threading.Interlocked.Increment(i), i - 1)
            End While

            ' create the curves
            Dim myCurve As BarItem = myPane.AddBar("curve 1", list, Color.Blue)
            Dim myCurve2 As BarItem = myPane.AddBar("curve 2", list2, Color.Red)
            Dim myCurve3 As BarItem = myPane.AddBar("curve 3", list3, Color.Green)

            ' Fill the axis background with a color gradient
            myPane.Chart.Fill = New Fill(Color.White, Color.FromArgb(255, 255, 166), 45.0F)

            zgc.AxisChange()

            ' expand the range of the Y axis slightly to accommodate the labels
            myPane.YAxis.Scale.Max += myPane.YAxis.Scale.MajorStep

            ' Create TextObj's to provide labels for each bar
            BarItem.CreateBarLabels(myPane, False, "f0")
            Dim myimg As Bitmap = zgc.GetImage
            Return myimg

        End Function
    End Class

End Namespace
