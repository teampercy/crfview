Namespace ClientView.Collection
    Public Class Reports
        Public Shared Function AccountsClosed(ByVal auser As Users.CurrentUser, ByVal MYview As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode) As String
            Dim myreportname As String = "AccountsClosed"

            If MYview.Count < 1 Then
                Return Nothing
            End If

            If printmode = Common.PrintMode.PrintToPDF Then
                Dim myreport As New BLL.Common.StandardReport
                myreport.ReportDefPath = auser.SettingsFolder & "Reports\WebReportsCollections.xml"
                myreport.OutputFolder = auser.OutputFolder
                myreport.UserName = auser.UserName
                myreport.ReportName = "AccountsClosed"
                myreport.TableIndex = 1
                myreport.DataView = MYview
                myreport.PrintMode = printmode
                myreport.Sort = ""
                myreport.FilePrefix = GetReportPrefix(auser)

                myreport.RenderReport()
                If myreport.ReportFileName.Length < 1 Then
                    Return Nothing
                Else
                    Return myreport.ReportFileName
                End If
            End If

            If COMMON.PrintMode.PrinttoExcel Then
                Dim myspreadsheetpath As String = GetFileName(auser, myreportname)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.FileNumber)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.ServiceCode)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.AccountNo)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.DebtorName)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.ContactName)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.AddressLine1)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.AddressLine2)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.City)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.State)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.PostalCode)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.PhoneNo)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.Fax)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.Email)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.ReferalDate)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.LastServiceDate)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.LastPaymentDate)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.TotAsgAmt)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.TotPmtAmt)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.TotAdjAmt)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.TotBalAmt)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.CollectionStatus)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.CloseDate)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.CloseCode)
                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    Return myspreadsheetpath
                Else
                    Return Nothing
                End If

            End If

            Return Nothing

        End Function
        Public Shared Function AccountsPayment(ByVal auser As Users.CurrentUser, ByVal MYview As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode) As String
            Dim myreportname As String = "AccountsPayment"

            If MYview.Count < 1 Then
                Return Nothing
            End If
            If printmode = Common.PrintMode.PrintToPDF Then
                Dim myreport As New BLL.Common.StandardReport
                myreport.ReportDefPath = auser.SettingsFolder & "Reports\WebReportsCollections.xml"
                myreport.OutputFolder = auser.OutputFolder
                myreport.UserName = auser.UserName
                myreport.ReportName = "AccountsPayment"
                myreport.TableIndex = 1
                myreport.DataView = MYview
                myreport.PrintMode = Common.PrintMode.PrintToPDF
                myreport.Sort = ""
                myreport.FilePrefix = GetReportPrefix(auser)

                myreport.RenderReport()
                Return myreport.ReportFileName
            End If
            If COMMON.PrintMode.PrinttoExcel Then
                Dim myspreadsheetpath As String = GetFileName(auser, myreportname)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.ClientCode)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.FileNumber)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.AccountNo)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.DebtorName)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.TrxDate)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.LedgerType)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.Rate)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.RcvByAgencyAmt)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.FeeByAgencyAmt)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.RcvByClientAmt)
                MYview.ExportColumns.Add(vwAccountLedger.ColumnNames.FeeByClientAmt)
                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    Return myspreadsheetpath
                Else
                    Return Nothing
                End If

            End If
            Return Nothing

        End Function
        Public Shared Function AccountsMaster(ByVal auser As Users.CurrentUser, ByVal MYview As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode) As String
            Dim myreportname As String = "AccountsMaster"

            If MYview.Count < 1 Then
                Return Nothing
            End If

            If printmode = Common.PrintMode.PrintToPDF Then
                Dim myreport As New BLL.Common.StandardReport
                myreport.ReportDefPath = auser.SettingsFolder & "Reports\WebReportsCollections.xml"
                myreport.OutputFolder = auser.OutputFolder
                myreport.UserName = auser.UserName
                myreport.ReportName = "AccountsMaster"
                myreport.TableIndex = 1
                myreport.DataView = MYview
                myreport.PrintMode = printmode
                myreport.Sort = ""
                myreport.FilePrefix = GetReportPrefix(auser)

                myreport.RenderReport()
                If myreport.ReportFileName.Length < 1 Then
                    Return Nothing
                Else
                    Return myreport.ReportFileName
                End If

            End If
            If COMMON.PrintMode.PrinttoExcel Then
                Dim myspreadsheetpath As String = GetFileName(auser, myreportname)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.FileNumber)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.ServiceCode)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.AccountNo)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.DebtorName)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.ContactName)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.AddressLine1)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.AddressLine2)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.City)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.State)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.PostalCode)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.PhoneNo)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.Fax)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.Email)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.ReferalDate)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.LastServiceDate)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.LastPaymentDate)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.TotAsgAmt)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.TotPmtAmt)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.TotAdjAmt)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.TotBalAmt)
                MYview.ExportColumns.Add(vwAccountList.ColumnNames.CollectionStatus)
                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    Return myspreadsheetpath
                Else
                    Return Nothing
                End If

            End If

            Return Nothing

        End Function
        Public Shared Function AccountsSubmitted(ByVal auser As Users.CurrentUser, ByVal MYview As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode) As String
            Dim myreportname As String = "AccountsSubmitted"

            If MYview.Count < 1 Then
                Return Nothing
            End If

            If printmode = Common.PrintMode.PrintToPDF Then
                Dim myreport As New BLL.Common.StandardReport
                myreport.ReportDefPath = auser.SettingsFolder & "Reports\WebReportsCollections.xml"
                myreport.OutputFolder = auser.OutputFolder
                myreport.UserName = auser.UserName
                myreport.ReportName = "AccountsSubmitted"
                myreport.TableIndex = 1
                myreport.DataView = MYview
                myreport.PrintMode = printmode
                myreport.Sort = ""
                myreport.FilePrefix = GetReportPrefix(auser)

                myreport.RenderReport()
                If myreport.ReportFileName.Length < 1 Then
                    Return Nothing
                Else
                    Return myreport.ReportFileName
                End If

            End If
            If COMMON.PrintMode.PrinttoExcel Then
                Dim myspreadsheetpath As String = GetFileName(auser, myreportname)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.ServiceCode)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.AccountNo)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.DebtorName)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.ContactName)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.AddressLine1)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.AddressLine2)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.City)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.State)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.PostalCode)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.PhoneNo)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.Fax)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.Email)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.ReferalDate)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.LastServiceDate)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.LastPaymentDate)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.TotAsgAmt)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.AccountNote)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.BatchId)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.BatchDebtAccountId)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.SubmittedByUserId)
                MYview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.ClientAgentCode)

                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    Return myspreadsheetpath
                Else
                    Return Nothing
                End If

            End If

            Return Nothing

        End Function

        Public Shared Function GetReportPrefix(ByVal auser As Users.CurrentUser) As String
            Dim myuser As New ClientView.ClientUserInfo(auser.Id)
            Dim myprefix As String = myuser.Client.ClientCode
            myprefix += "-" & myuser.UserInfo.LoginCode
            myprefix += "-" & myuser.UserInfo.ID
            Return myprefix
        End Function
        Public Shared Function GetFileName(ByVal auser As Users.CurrentUser, ByVal areportname As String) As String
            Dim myuser As New ClientView.ClientUserInfo(auser.Id)
            Dim myprefix As String = myuser.Client.ClientCode
            myprefix += "-" & myuser.UserInfo.LoginCode
            myprefix += "-" & myuser.UserInfo.ID
            myprefix += "-" & areportname
            If System.IO.Directory.Exists(auser.OutputFolder) = False Then
                System.IO.Directory.CreateDirectory(auser.OutputFolder)
            End If
            Return auser.OutputFolder & Common.FileOps.TrimSpace(myprefix & "-" & Format(Now(), "yyyyMMddhhmmss") & ".CSV")
        End Function

    End Class
End Namespace
