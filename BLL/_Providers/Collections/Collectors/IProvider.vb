Imports CRF.BLL.Users

Namespace Providers
    Public Interface ICollectionsCollectorsProvider
        Function GetAccountInfo(ByVal auser As Users.CurrentUser, ByVal anewaccountid As String) As DataSet
        Function GetAccountList(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Accounts_GetAccountList) As DAL.COMMON.TableView
        Function PostLedgerEntry(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Accounts_LedgerEntry) As Boolean
        Function PostLedgerReversal(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Accounts_LedgerReversal) As Boolean
        Function PostPaymenttoAgency(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Accounts_PaymentToAgency) As Boolean
        Function PostPaymenttoClient(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Accounts_PaymentToClient) As Boolean
        Function ReassignService(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Accounts_ReassignService) As Boolean
        Function PostAccountUpdate(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Accounts_Update) As Boolean
        Function GetInventoryList(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Reports_GetAccountList) As DataSet
        Function GetClosedList(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Reports_GetClosedAccountList) As DataSet
        Function GetPaymentList(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_Reports_GetPaymentList) As DataSet
        Function PrintMasterReport(ByVal auser As Users.CurrentUser, ByVal aprintmode As COMMON.PrintMode, ByVal aview As DAL.COMMON.TableView) As String
        Function PrintClosedReport(ByVal auser As Users.CurrentUser, ByVal aprintmode As COMMON.PrintMode, ByVal aview As DAL.COMMON.TableView) As String
        Function PrintPaymentReport(ByVal auser As Users.CurrentUser, ByVal aprintmode As COMMON.PrintMode, ByVal aview As DAL.COMMON.TableView) As String
        Function GetCollectionClients(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
        Function GetUserList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
        Function GetLocations(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
        Function GetColectionStatus(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
        Function PrintCollectionLetter(ByVal auser As Users.CurrentUser, ByVal akeyid As Integer, ByVal areportdef As String, ByVal areportname As String, ByVal aprintmode As HDS.Reporting.COMMON.PrintMode, Optional ByVal areportprefix As String = "", Optional ByVal asubtitle1 As String = "", Optional ByVal asubtitle2 As String = "", Optional ByVal asubreportname As String = "") As String
        Function PrintLetter(ByVal auser As Users.CurrentUser, ByVal akeyid As Integer, ByVal areportdef As String, ByVal areportname As String, ByVal aprintmode As HDS.Reporting.COMMON.PrintMode, Optional ByVal areportprefix As String = "", Optional ByVal asubtitle1 As String = "", Optional ByVal asubtitle2 As String = "", Optional ByVal asubreportname As String = "") As String
        Function PrintLetter(auser As CurrentUser, aitemid As Integer, aaddressid As Integer, akeyid As Integer, alocationid As Integer) As String
    End Interface
End Namespace
