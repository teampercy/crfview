Namespace Providers
    Public Interface ICollectionsDataEntryProvider
        Function GetMyOpenBatchList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
        Function PrintBatchList(ByVal auser As Users.CurrentUser, ByVal aprintmode As Common.PrintMode, ByVal aview As DAL.COMMON.TableView) As String
        Function PrintBatchAccountInfo(ByVal auser As Users.CurrentUser, ByVal aview As DAL.COMMON.TableView, ByVal aprintmode As Common.PrintMode) As String
        Function GetAccountsSubmittedthruWeb(ByVal auser As Users.CurrentUser, ByVal MyView As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode) As String
        Function PostBatches(ByVal auser As Users.CurrentUser) As Boolean
        Function PostWebPlacements(ByVal auser As Users.CurrentUser) As DataSet
        Function GetAccountInBatches(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.cview_Reports_GetAccountsInBatches) As DAL.COMMON.TableView
        Function ImportEDI(ByVal auser As Users.CurrentUser, ByVal afilepath As String, ByVal acilentid As String, ByVal ainterfaceid As String) As Boolean
        Function GetNewAccountInfo(ByVal auser As Users.CurrentUser, ByVal anewaccountid As String) As DataSet
        Function GetNewAccountList(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.cview_Reports_GetAccountsInBatches) As DAL.COMMON.TableView
        Function GetCollectionClients(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
        Function GetUserList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
    End Interface
End Namespace
