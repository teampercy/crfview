Imports Microsoft.VisualBasic

Imports System
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Web.Configuration
Imports System.Configuration.ConfigurationManager
Namespace Providers
    Public Class DBO
        Public Shared ReadOnly Property Provider() As DAL.Providers.IDAL
            Get
                Return DAL.Providers.DataAccessFactory.DataAccess("PORTAL", AppSettings.Get("SQLCLIENT"))
            End Get
        End Property
        Public Shared Function GetDataTable(ByVal ObjectName As String, ByVal maxrows As Integer, ByVal filter As String, ByVal sort As String) As System.Data.DataTable
            Dim MYSQL As String = DBO.Provider.GetSQL(ObjectName, maxrows, filter, sort)
            Return DBO.Provider.GetDataSet(MYSQL).Tables(0)
        End Function
        Public Shared Function FillEntity(ByVal dr As DataRow, ByRef entity As Object) As Object
            DBO.Provider.FillEntity(dr, entity)
            Return entity
        End Function
        Public Shared Function GetItem(ByVal aobjectname As String, ByRef aentity As Object, ByVal akeyname As String, ByVal avalue As String)
            Dim MYSQL As String = DBO.Provider.GetSQL(aobjectname, 1, akeyname & "='" & avalue & "'", "")
            Dim myds As DataSet = DBO.Provider.GetDataSet(MYSQL)
            If myds.Tables(0).Rows.Count > 0 Then
                DBO.Provider.FillEntity(myds.Tables(0).Rows.Item(0), aentity)
            End If
            Return aentity

        End Function
        Public Shared Function GetItem(ByVal aobjectname As String, ByRef aentity As Object, ByVal filter As String)
            Dim MYSQL As String = DBO.Provider.GetSQL(aobjectname, 1, filter, "")
            Dim myds As DataSet = DBO.Provider.GetDataSet(MYSQL)
            If myds.Tables(0).Rows.Count > 0 Then
                DBO.Provider.FillEntity(myds.Tables(0).Rows.Item(0), aentity)
            End If
            Return aentity

        End Function
        Public Shared Function GetUniqueId() As String
            Dim MYID As Guid = System.Guid.NewGuid()
            Dim S As String = DateTime.Now.ToString().GetHashCode().ToString("x").ToUpper

            Return S


        End Function
    End Class
    
End Namespace
