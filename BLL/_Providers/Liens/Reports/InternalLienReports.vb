Namespace InternalLien
    Public Class Reports
        Public Shared Function GetClientList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
            Dim sql As String = "Select ClientId As ClientId,  ClientCode, ClientCode + '-' + ClientName As ClientName from Client Where IsLienClient = 1 and IsBranch = 0"
            Return DBO.GetTableView(sql)

        End Function
        Public Shared Function GetReportList(ByVal auser As Users.CurrentUser, ByVal areportcategory As String) As DAL.COMMON.TableView
            Dim sql As String = "Select ReportName, Description,  PrintFrom, ReportCategory, ReportFilename, Reporttype from vwLienReportsList Where ReportCategory = '" & areportcategory.ToString & "' And PrintFrom in ('B', 'R') Order By Description"

            Return DBO.GetTableView(sql)
        End Function

        Public Shared Function GetStatusList(ByVal auser As Users.CurrentUser, ByVal bystatusgroup As Boolean, ByVal astatusgroupid As Integer) As DAL.COMMON.TableView
            Dim sql As String
            If bystatusgroup = True Then
                sql = "Select JobStatus, JobStatusDescr, StatusGroup from JobStatus"
            Else
                sql = "Select JobStatus, JobStatusDescr, StatusGroup from JobStatus Where StatusGroup = " & astatusgroupid.ToString
            End If
            Return DBO.GetTableView(sql)
        End Function
        Public Shared Function MailingLog(ByVal auser As Users.CurrentUser, ByVal MYview As DAL.COMMON.TableView, ByVal asubtitle1 As String, ByVal printmode As Common.PrintMode) As String
            Dim myreportname As String = "MailingLog"

            If MYview.Count < 1 Then
                Return Nothing
            End If

            If printmode = Common.PrintMode.PrintToPDF Then
                Dim myreport As New BLL.Common.StandardReport
                myreport.ReportDefPath = auser.SettingsFolder & "Reports\WebReportsLiens.xml"
                myreport.OutputFolder = auser.OutputFolder
                myreport.UserName = auser.UserName
                myreport.ReportName = "JobsPlaced"
                myreport.TableIndex = 1
                myreport.DataView = MYview
                myreport.PrintMode = printmode
                myreport.Sort = ""
                myreport.SubTitle1 = asubtitle1
                myreport.FilePrefix = GetReportPrefix(auser)

                myreport.RenderReport()
                If myreport.ReportFileName.Length < 1 Then
                    Return Nothing
                Else
                    Return myreport.ReportFileName
                End If
            End If

            If printmode.PrinttoExcel Then
                Dim myspreadsheetpath As String = GetFileName(auser, myreportname)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.ClientCode)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobId)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobNum)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobName)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobAdd1)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobCity)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobState)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobZip)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.JobCounty)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.APNNum)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.EstBalance)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.ClientCustomer)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.CustRefNum)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.GCName)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.OwnerName)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.BranchNum)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.DateAssigned)
                MYview.ExportColumns.Add(vwJobInventory.ColumnNames.StatusCode)

                If MYview.ExportToCSV(myspreadsheetpath) = True Then
                    Return myspreadsheetpath
                Else
                    Return Nothing
                End If

            End If

            Return Nothing

        End Function
        Public Shared Function PrintLienReport(ByVal auser As Users.CurrentUser, ByVal aprintmode As Common.PrintMode, ByVal aview As DAL.COMMON.TableView, ByVal areportfilename As String, ByVal areportname As String) As String
            Dim myreport As New BLL.Common.StandardReport

            myreport.ReportDefPath = auser.SettingsFolder & "Reports\" & areportfilename
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.ReportName = areportname
            myreport.TableIndex = 1
            myreport.DataView = aview
            myreport.PrintMode = aprintmode
            myreport.Sort = ""
            myreport.RenderReport()
            Return myreport.ReportFileName

        End Function

        Public Shared Function GetJobInfo(ByVal auser As Users.CurrentUser, ByVal aitemId As Integer) As Liens.JobInfo
            Return New Liens.JobInfo(auser, aitemId)
        End Function
        Public Shared Function GetCountyRecorderByNum(ByVal auser As Users.CurrentUser, ByVal arecordernum As String) As TABLES.CountyRecorder
            Dim myview As New TABLES.CountyRecorder
            Dim myquery As New HDS.DAL.COMMON.Query(myview)
            Dim mysql As String
            myquery.AddCondition(HDS.DAL.COMMON.LogicalOperators._And, CRFDB.TABLES.CountyRecorder.ColumnNames.CountyRecorderNum, HDS.DAL.COMMON.Conditions._IsEqual, arecordernum)
            mysql = myquery.BuildQuery
            Dim myvw As DAL.COMMON.TableView = DBO.GetTableView(myquery.BuildQuery)
            If myvw.Count > 0 Then
                myvw.FillEntity(myview)
            End If
            Return myview

        End Function
        Public Shared Function UpdateJobStatus(ByVal auser As Users.CurrentUser, ByVal aentity As TABLES.Job, ByVal asproc As SPROCS.uspbo_Jobs_UpdateJobStatus) As Object
            Return DBO.ExecScalar(asproc)
        End Function

        Public Shared Function GetReportPrefix(ByVal auser As Users.CurrentUser) As String
            Dim myuser As New ClientView.ClientUserInfo(auser.Id)
            Dim myprefix As String = myuser.Client.ClientCode
            myprefix += "-" & myuser.UserInfo.LoginCode
            myprefix += "-" & myuser.UserInfo.ID
            Return myprefix
        End Function
        Public Shared Function GetFileName(ByVal auser As Users.CurrentUser, ByVal areportname As String) As String
            Dim myuser As New ClientView.ClientUserInfo(auser.Id)
            Dim myprefix As String = myuser.Client.ClientCode
            myprefix += "-" & myuser.UserInfo.LoginCode
            myprefix += "-" & myuser.UserInfo.ID
            myprefix += "-" & areportname
            If System.IO.Directory.Exists(auser.OutputFolder) = False Then
                System.IO.Directory.CreateDirectory(auser.OutputFolder)
            End If
            Return auser.OutputFolder & Common.FileOps.TrimSpace(myprefix & "-" & Format(Now(), "yyyyMMddhhmmss") & ".CSV")
        End Function

    End Class
End Namespace
