Imports System
Imports System.Windows.Forms
Imports C1
Imports C1.C1Report
Namespace Liens.Reports
    Public Class JobOtherNotice
        Inherits Common.StandardReport
        Dim m_rtf As RichTextBox
        Dim m_filename As String
        Dim m_formfolder As String
        Dim _reportpath As String
        Dim _settingsfolder As String
        Dim _reportdefpath As String
        Dim _username As String
        Dim srtf As String
        Public CurrentUser As Users.CurrentUser
        Dim mynoticeinfo As HDS.DAL.COMMON.TableView
        Dim myform As HDS.DAL.COMMON.TableView

        Public Sub New(ByVal auser As Users.CurrentUser, Optional ByVal areportdef As String = "Reports\WebLienNotices.xml")
            MyBase.New()
            Me.CurrentUser = auser
            Me.OutputFolder = auser.OutputFolder
            Me.SettingsFolder = auser.SettingsFolder
            Me.UserName = auser.UserName
            Me.ReportDefPath = Me.SettingsFolder & areportdef

        End Sub
        Public Function ExecuteC1Report(ByVal MYDS As DataSet) As String
            mynoticeinfo = New HDS.DAL.COMMON.TableView(MYDS.Tables(0))
            myform = New HDS.DAL.COMMON.TableView(MYDS.Tables(1))
            If mynoticeinfo.Count = 1 Then
                Me.ReportName = myform.RowItem("ReportName")
                Me.DataView = mynoticeinfo
                With Me
                    .FilePrefix = ClientView.Lien.Report.GetReportPrefix(Me.CurrentUser) & "-" & mynoticeinfo.RowItem("JobId") & "-" & Me.ReportName
                    .TableIndex = 0
                    .RenderReport()
                    .ReportPath = .ReportFileName
                    Return .ReportFileName

                End With
            Else
                Return Nothing
            End If


        End Function
        Public Function ExecuteC1Report(ByVal aitemid As String) As String
            Dim mysproc As New SPROCS.uspbo_Jobs_GetOtherNoticeInfo
            mysproc.Id = aitemid
            Return ExecuteC1Report(DBO.GetDataSet(mysproc))


        End Function
        Public Overrides Sub StartSection(ByVal sender As Object, ByVal e As C1.C1Report.ReportEventArgs)



        End Sub


    End Class

End Namespace
