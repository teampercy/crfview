Namespace Providers
    Public Interface ILiensDataEntryProvider
        Function GetMyOpenBatchList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
        Function PrintBatchList(ByVal auser As Users.CurrentUser, ByVal aprintmode As Common.PrintMode, ByVal aview As DAL.COMMON.TableView) As String
        Function PrintBatchJobInfo(ByVal auser As Users.CurrentUser, ByVal aview As DAL.COMMON.TableView, ByVal aprintmode As Common.PrintMode) As String
        Function PrintBatchOtherLegals(ByVal auser As Users.CurrentUser, ByVal aview As DAL.COMMON.TableView, ByVal aprintmode As Common.PrintMode) As String
        Function GetWebJobsSubmitted(ByVal auser As Users.CurrentUser, ByVal MyView As DAL.COMMON.TableView, ByVal printmode As Common.PrintMode, Optional ByVal filename As String = "WebJobsSubmitted") As String
        Function PostWebPlacements(ByVal auser As Users.CurrentUser) As DataSet
        Function GetReleasedBatches(ByVal auser As Users.CurrentUser) As DataSet
        Function PostBatches(ByVal auser As Users.CurrentUser) As Boolean
        Function GetJobsInBatches(ByVal auser As Users.CurrentUser, ByVal asproc As BLL.CRFDB.SPROCS.uspbo_LiensDataEntry_GetJobsInBatches) As DAL.COMMON.TableView
        Function GetNewJobInfo(ByVal auser As Users.CurrentUser, ByVal anewjobid As String) As DataSet
        Function GetStateInfo(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
        Function GetLienClients(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
        Function GetCustomersForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String) As DAL.COMMON.TableView
        Function GetGContractorsForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String) As DAL.COMMON.TableView
        Function GetNewJobEntryActions(ByVal auser As Users.CurrentUser, ByVal IsTXJob As Boolean) As DAL.COMMON.TableView
        Function GetUserList(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
        Function GetLienInterfaces(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
        Function GetCustomersByNameForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal afilter As String) As DataTable
        Function GetCustomersByRefForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal afilter As String) As DataTable
        Function GetGCByNameForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal afilter As String) As DataTable
        Function GetCityStateforZip(ByVal auser As Users.CurrentUser, ByVal azip As String) As DAL.COMMON.TableView
        Function GetStateInfoforState(ByVal auser As Users.CurrentUser, ByVal astateinitial As String) As TABLES.StateInfo
        Function GetLienInfoforClient(ByVal auser As Users.CurrentUser, ByVal aclientid As Integer) As CRF.BLL.CRFDB.TABLES.ClientLienInfo
        Function GetClientInfoforClient(ByVal auser As Users.CurrentUser, ByVal aclientid As Integer) As CRF.BLL.CRFDB.TABLES.Client

    End Interface
End Namespace
