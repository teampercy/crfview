Namespace Providers
    Public Interface ILiensClientViewProvider
        Function GetCustomersByNameForUser(ByVal auser As Users.CurrentUser, ByVal afilter As String) As DAL.COMMON.TableView
        Function GetCustomersByRefForUser(ByVal auser As Users.CurrentUser, ByVal afilter As String) As DAL.COMMON.TableView
        Function GetGCByNameForUser(ByVal auser As Users.CurrentUser, ByVal afilter As String) As DAL.COMMON.TableView
        Function GetCustomersByNameForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal afilter As String) As DAL.COMMON.TableView
        Function GetCustomersByRefForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal afilter As String) As DAL.COMMON.TableView
        Function GetGCByNameForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal afilter As String) As DAL.COMMON.TableView
        Function GetCityStateforZip(ByVal auser As Users.CurrentUser, ByVal azip As String) As DAL.COMMON.TableView
        Function GetClientViewGroups(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
        Function GetClientforUser(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
        Function GetClientSignersforUser(ByVal auser As Users.CurrentUser) As DAL.COMMON.TableView
        Function GetOtherNoticeForms(ByVal auser As Users.CurrentUser, ByVal astateinitials As String) As DAL.COMMON.TableView
        Function GetWaiverFormsForState(ByVal auser As Users.CurrentUser, ByVal astateinitials As String) As DAL.COMMON.TableView
        Function GetSignersForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String) As DAL.COMMON.TableView
        Function GetUserInfo(ByVal auser As Users.CurrentUser) As ClientView.ClientUserInfo
        Function GetJobInfo(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As CRF.BLL.CRFDB.VIEWS.vwJobInfo
        Function GetJobNotes(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As DAL.COMMON.TableView
        Function GetJobNoticesSent(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As DAL.COMMON.TableView
        Function GetJobWaiverLog(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As DAL.COMMON.TableView
        Function GetJobOtherNoticeLog(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As DAL.COMMON.TableView
        Function PrintTexasRequestAck(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String
        Function PrintJobAck(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String
        Function PrintJobsSubmitted(ByVal auser As Users.CurrentUser) As String
        Function PrintNoJobWaiver(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String
        Function PrintJobWaiver(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String
        Function PrintPrelimNotice(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String
        Function PrintOtherNotice(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String
        Function PrintJobsSubmitted(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.uspbo_ClientView_GetJobInventory, ByVal amode As Common.PrintMode) As String
        Function PrintOpenItems(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.uspbo_ClientView_GetJobInventory, ByVal amode As Common.PrintMode) As String
        Function PrintNoticeSent(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.uspbo_ClientView_GetJobInventory, ByVal amode As Common.PrintMode) As String
        Function PrintTexasPending(ByVal auser As Users.CurrentUser, ByVal asproc As SPROCS.uspbo_ClientView_GetTexasRequests, ByVal amode As Common.PrintMode) As String
        Function PrintJobNotes(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String
        Function PrintTexasStatements(ByVal auser As Users.CurrentUser, ByVal asproc As CRFDB.SPROCS.uspbo_ClientView_GetTexasStatements)

    End Interface
End Namespace
