Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Web.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Xml
Imports System.Xml.Schema
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections.Specialized
Imports System.Configuration.Provider
Imports System.Web.Hosting
Imports System.Web.Management
Imports System.Security.Permissions
Imports System.Text
Imports System.Text.RegularExpressions
Imports System.Security.Cryptography
Namespace Providers
    Friend Class SQLLiensClientViewProvider
        Inherits ProviderBase
        Implements ILiensClientViewProvider

        Public Function GetSignersForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String) As HDS.DAL.COMMON.TableView Implements ILiensClientViewProvider.GetSignersForClient
            Dim myquery As New DAL.COMMON.Query(New CRFDB.TABLES.ClientSigner)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, CRFDB.TABLES.ClientSigner.ColumnNames.ClientId, DAL.COMMON.Conditions._IsEqual, aclientid)
            Return Providers.DBO.Provider.GetTableView(myquery.BuildQuery)
        End Function
        Public Function GetWaiverFormsForState(ByVal auser As Users.CurrentUser, ByVal astateinitials As String) As HDS.DAL.COMMON.TableView Implements ILiensClientViewProvider.GetWaiverFormsForState
            Dim myquery As New DAL.COMMON.Query(New CRFDB.TABLES.StateForms)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, CRFDB.TABLES.StateForms.ColumnNames.StateCode, DAL.COMMON.Conditions._IsEqual, astateinitials)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, CRFDB.TABLES.StateForms.ColumnNames.FormType, DAL.COMMON.Conditions._IsEqual, "W")
            Return Providers.DBO.Provider.GetTableView(myquery.BuildQuery)
        End Function
        Public Function GetOtherNoticeForms(ByVal auser As Users.CurrentUser, ByVal astateinitials As String) As HDS.DAL.COMMON.TableView Implements ILiensClientViewProvider.GetOtherNoticeForms
            Dim myquery As New DAL.COMMON.Query(New CRFDB.TABLES.StateFormNotices)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, CRFDB.TABLES.StateFormNotices.ColumnNames.StateCode, DAL.COMMON.Conditions._IsEqual, astateinitials)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, CRFDB.TABLES.StateFormNotices.ColumnNames.ClientView, DAL.COMMON.Conditions._IsNotEqual, 0)
            Return Providers.DBO.Provider.GetTableView(myquery.BuildQuery)
        End Function
        Public Function PrintNoJobWaiver(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String Implements ILiensClientViewProvider.PrintNoJobWaiver
            Dim myreport As New Liens.Reports.NoJobWaiverNotice(auser, aitemid)
            myreport.ExecuteC1Report()
            Return myreport.ReportPath
        End Function
        Public Function PrintJobWaiver(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String Implements ILiensClientViewProvider.PrintJobWaiver
            Dim myreport As New Liens.Reports.JobWaiverNotice(auser, aitemid)
            myreport.ExecuteC1Report()
            Return myreport.ReportPath
        End Function
        Public Function PrintPrelimNotice(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String Implements ILiensClientViewProvider.PrintPrelimNotice
            Dim myreport As New Liens.Reports.JobPrelimNotice(auser)
            myreport.ExecuteC1Report(aitemid)
            Return myreport.ReportPath
        End Function
        Public Function PrintOtherNotice(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String Implements ILiensClientViewProvider.PrintOtherNotice
            Dim myreport As New Liens.Reports.JobOtherNotice(auser)
            myreport.ExecuteC1Report(aitemid)
            Return myreport.ReportPath
        End Function
        Public Function PrintJobsSubmitted(ByVal auser As Users.CurrentUser, ByVal asproc As CRFDB.SPROCS.uspbo_ClientView_GetJobInventory, ByVal amode As COMMON.PrintMode) As String Implements ILiensClientViewProvider.PrintJobsSubmitted
            Dim stitle As String = "From " & Strings.FormatDateTime(asproc.FromDate, DateFormat.ShortDate) & " to " & Strings.FormatDateTime(asproc.ThruDate, DateFormat.ShortDate)
            Return ClientView.Lien.Report.JobsPlaced(auser, Providers.DBO.Provider.GetTableView(asproc), stitle, amode)
        End Function
        Public Function PrintNoticeSent(ByVal auser As Users.CurrentUser, ByVal asproc As CRFDB.SPROCS.uspbo_ClientView_GetJobInventory, ByVal amode As COMMON.PrintMode) As String Implements ILiensClientViewProvider.PrintNoticeSent
            Dim stitle As String = "From " & Strings.FormatDateTime(asproc.FromDate, DateFormat.ShortDate) & " to " & Strings.FormatDateTime(asproc.ThruDate, DateFormat.ShortDate)
            Return ClientView.Lien.Report.NoticeSent(auser, Providers.DBO.Provider.GetTableView(asproc), stitle, amode)
        End Function
        Public Function PrintOpenItems(ByVal auser As Users.CurrentUser, ByVal asproc As CRFDB.SPROCS.uspbo_ClientView_GetJobInventory, ByVal amode As COMMON.PrintMode) As String Implements ILiensClientViewProvider.PrintOpenItems
            Return ClientView.Lien.Report.OpenItem(auser, Providers.DBO.Provider.GetTableView(asproc), amode)
        End Function
        Public Function PrintTexasPending(ByVal auser As Users.CurrentUser, ByVal asproc As CRFDB.SPROCS.uspbo_ClientView_GetTexasRequests, ByVal amode As COMMON.PrintMode) As String Implements ILiensClientViewProvider.PrintTexasPending
            Return ClientView.Lien.Report.TexasNoticeSent(auser, Providers.DBO.Provider.GetTableView(asproc), amode)
        End Function
        Public Function PrintJobNotes(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String Implements ILiensClientViewProvider.PrintJobNotes
            Dim mytable As New CRFDB.VIEWS.vwJobNoteHistory
            Dim myquery As New DAL.COMMON.Query(mytable)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, CRF.BLL.CRFDB.VIEWS.vwJobNoteHistory.ColumnNames.JobId, DAL.COMMON.Conditions._IsEqual, aitemid)
            myquery.AddOrderBy(vwJobNoteHistory.ColumnNames.DateCreated, DAL.COMMON.Direction.ASC)
            Dim sql As String = myquery.BuildQuery
            Dim ads As DataSet = DBO.Provider.GetDataSet(sql)
            If ads.Tables(0).Rows.Count < 1 Then
                Return Nothing
            End If
            Return Common.Reporting.PrintDataReport(auser, "JobNotes.DRL", "JobNotes", ads, Common.PrintMode.PrintToPDF, auser.UserName, "", "")

        End Function

        Public Function GetJobInfo(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As CRFDB.VIEWS.vwJobInfo Implements ILiensClientViewProvider.GetJobInfo
            Dim mytable As New CRFDB.VIEWS.vwJobInfo
            Dim myquery As New DAL.COMMON.Query(mytable)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, vwJobNotes.ColumnNames.JobId, DAL.COMMON.Conditions._IsEqual, aitemid)
            Dim sql As String = myquery.BuildQuery
            Dim myview As DAL.COMMON.TableView = Providers.DBO.Provider.GetTableView(sql)
            myview.FillEntity(mytable)
            Return mytable

        End Function
        Public Function GetJobNotes(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As HDS.DAL.COMMON.TableView Implements ILiensClientViewProvider.GetJobNotes

        End Function

        Public Function GetJobNoticesSent(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As HDS.DAL.COMMON.TableView Implements ILiensClientViewProvider.GetJobNoticesSent

        End Function

        Public Function GetJobOtherNoticeLog(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As HDS.DAL.COMMON.TableView Implements ILiensClientViewProvider.GetJobOtherNoticeLog

        End Function

        Public Function GetJobWaiverLog(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As HDS.DAL.COMMON.TableView Implements ILiensClientViewProvider.GetJobWaiverLog

        End Function
        Public Function GetCityStateforZip(ByVal auser As Users.CurrentUser, ByVal azip As String) As HDS.DAL.COMMON.TableView Implements ILiensClientViewProvider.GetCityStateforZip
            Dim MYtable As New CRF.BLL.CRFDB.TABLES.ZipCode
            Dim myquery As New HDS.DAL.COMMON.Query(MYtable)
            myquery.AddCondition(HDS.DAL.COMMON.LogicalOperators._And, CRF.BLL.CRFDB.TABLES.ZipCode.ColumnNames.ZIPCode, HDS.DAL.COMMON.Conditions._IsEqual, azip)
            Return Providers.DBO.Provider.GetTableView(myquery.BuildQuery)

        End Function
        Public Function GetClientforUser(ByVal auser As Users.CurrentUser) As HDS.DAL.COMMON.TableView Implements ILiensClientViewProvider.GetClientforUser
            Dim mysproc As New CRFDB.SPROCS.uspbo_Clientview_GetClientListForUser
            mysproc.UserId = auser.Id
            Return Providers.DBO.Provider.GetTableView(mysproc)
        End Function

        Public Function GetClientViewGroups(ByVal auser As Users.CurrentUser) As HDS.DAL.COMMON.TableView Implements ILiensClientViewProvider.GetClientViewGroups
            Dim myquery As New DAL.COMMON.Query(New CRFDB.TABLES.ClientViewGroup)
            Dim MYVIEW As HDS.DAL.COMMON.TableView = Providers.DBO.Provider.GetTableView(myquery.SelectAll)
            MYVIEW.Sort = "ClientViewGroupName"
            Return MYVIEW

        End Function

        Public Function GetClientSignersforUser(ByVal auser As Users.CurrentUser) As HDS.DAL.COMMON.TableView Implements ILiensClientViewProvider.GetClientSignersforUser
            Dim mysproc As New CRFDB.SPROCS.uspbo_Clientview_GetClientSignersForUser
            mysproc.UserId = auser.Id
            Dim MYVIEW As HDS.DAL.COMMON.TableView = Providers.DBO.Provider.GetTableView(mysproc)
            MYVIEW.Sort = "Signer"
            Return MYVIEW

        End Function
        Public Function GetCustomersByNameForUser(ByVal auser As Users.CurrentUser, ByVal afilter As String) As HDS.DAL.COMMON.TableView Implements ILiensClientViewProvider.GetCustomersByNameForUser
            Dim mysproc As New CRFDB.SPROCS.uspbo_Clientview_GetCustomerByNameForUser
            mysproc.UserId = auser.Id
            mysproc.Name = afilter
            Return Providers.DBO.Provider.GetTableView(mysproc)

        End Function
        Public Function GetCustomersByRefForUser(ByVal auser As Users.CurrentUser, ByVal afilter As String) As HDS.DAL.COMMON.TableView Implements ILiensClientViewProvider.GetCustomersByRefForUser
            Dim mysproc As New CRFDB.SPROCS.uspbo_Clientview_GetCustomerByRefForUser
            mysproc.UserId = auser.Id
            mysproc.Ref = afilter
            Return Providers.DBO.Provider.GetTableView(mysproc)

        End Function
        Public Function GetGCByNameForUser(ByVal auser As Users.CurrentUser, ByVal afilter As String) As HDS.DAL.COMMON.TableView Implements ILiensClientViewProvider.GetGCByNameForUser
            Dim mysproc As New CRFDB.SPROCS.uspbo_Clientview_GetGCByNameForUser
            mysproc.UserId = auser.Id
            mysproc.Name = afilter
            Return Providers.DBO.Provider.GetTableView(mysproc)

        End Function
        Public Function GetGCByNameForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal afilter As String) As HDS.DAL.COMMON.TableView Implements ILiensClientViewProvider.GetGCByNameForClient

        End Function
        Public Function GetCustomersByNameForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal afilter As String) As HDS.DAL.COMMON.TableView Implements ILiensClientViewProvider.GetCustomersByNameForClient

        End Function

        Public Function GetCustomersByRefForClient(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal afilter As String) As HDS.DAL.COMMON.TableView Implements ILiensClientViewProvider.GetCustomersByRefForClient

        End Function
        Public Function GetUserInfo(ByVal auser As Users.CurrentUser) As ClientView.ClientUserInfo Implements ILiensClientViewProvider.GetUserInfo
            Return New ClientView.ClientUserInfo(auser.Id)

        End Function

        Public Function PrintJobAck(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String Implements ILiensClientViewProvider.PrintJobAck
            Dim mysproc As New CRFDB.SPROCS.uspbo_ClientView_GetJobsSubmitted
            With mysproc
                .SubmittedByUserId = auser.Id
                .BatchJobId = aitemid
            End With

            Dim myreport As New BLL.COMMON.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\WebReportsLiens.xml"
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.ReportName = "NewJobAck"
            myreport.FilePrefix = GetReportPrefix(auser)
            myreport.TableIndex = 0
            myreport.DataView = Providers.DBO.Provider.GetTableView(mysproc)
            myreport.PrintMode = COMMON.PrintMode.PrintToPDF
            myreport.Sort = ""
            myreport.RenderReport()
            If myreport.ReportFileName.Length < 1 Then
                Return Nothing
            Else
                Return myreport.ReportFileName
            End If
        End Function

        Public Function PrintJobsSubmitted(ByVal auser As Users.CurrentUser) As String Implements ILiensClientViewProvider.PrintJobsSubmitted
            Dim mysproc As New CRFDB.SPROCS.uspbo_ClientView_GetJobsSubmitted
            With mysproc
                .SubmittedByUserId = auser.Id
                .BatchJobId = 0
            End With

            Dim myreport As New BLL.COMMON.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\WebReportsLiens.xml"
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.ReportName = "JobsSubmitted"
            myreport.FilePrefix = GetReportPrefix(auser)
            myreport.SubTitle1 = "Jobs Placed on: " & Strings.FormatDateTime(Today, DateFormat.ShortDate)

            myreport.TableIndex = 0
            myreport.DataView = Providers.DBO.Provider.GetTableView(mysproc)
            myreport.PrintMode = COMMON.PrintMode.PrintToPDF
            myreport.Sort = ""
            myreport.RenderReport()
            If myreport.ReportFileName.Length < 1 Then
                Return Nothing
            Else
                Return myreport.ReportFileName
            End If
        End Function
        Public Shared Function GetReportPrefix(ByVal auser As Users.CurrentUser) As String
            Dim myuser As New ClientView.ClientUserInfo(auser.Id)
            Dim myprefix As String = myuser.Client.ClientCode
            myprefix += "-" & myuser.UserInfo.LoginCode
            myprefix += "-" & myuser.UserInfo.ID
            Return myprefix
        End Function

        Public Function PrintTexasRequestAck(ByVal auser As Users.CurrentUser, ByVal aitemid As Integer) As String Implements ILiensClientViewProvider.PrintTexasRequestAck
            Dim mytable As New CRFDB.VIEWS.vwJobTexasRequests
            Dim myquery As New DAL.COMMON.Query(mytable)
            myquery.AddCondition(DAL.COMMON.LogicalOperators._And, vwJobTexasRequests.ColumnNames.RequestId, DAL.COMMON.Conditions._IsEqual, aitemid)
            Dim sql As String = myquery.BuildQuery
            Dim myreport As New BLL.COMMON.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\WebReportsLiens.xml"
            myreport.OutputFolder = auser.OutputFolder
            myreport.UserName = auser.UserName
            myreport.ReportName = "TexasRequestAck"
            myreport.FilePrefix = GetReportPrefix(auser)
            myreport.TableIndex = 1
            myreport.DataView = Providers.DBO.Provider.GetTableView(sql)
            myreport.PrintMode = COMMON.PrintMode.PrintToPDF
            myreport.Sort = ""
            myreport.RenderReport()
            If myreport.ReportFileName.Length < 1 Then
                Return Nothing
            Else
                Return myreport.ReportFileName
            End If
        End Function

        Public Function PrintTexasStatements(ByVal auser As Users.CurrentUser, ByVal asproc As CRFDB.SPROCS.uspbo_ClientView_GetTexasStatements) As Object Implements ILiensClientViewProvider.PrintTexasStatements

            Dim ads As DataSet = DBO.Provider.GetDataSet(asproc)
            If ads.Tables(0).Rows.Count < 1 Then
                Return Nothing
            End If
            Return Common.Reporting.PrintDataReport(auser, "TexasStatement.DRL", "TexasStatements", ads, Common.PrintMode.PrintToPDF, auser.UserName, "", "")

        End Function
    End Class
End Namespace
