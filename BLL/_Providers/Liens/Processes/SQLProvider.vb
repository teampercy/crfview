Imports Microsoft.VisualBasic
Imports System
Imports System.Data
Imports System.Web.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Xml
Imports System.Xml.Schema
Imports System.IO
Imports System.Collections.Generic
Imports System.Collections.Specialized
Imports System.Configuration.Provider
Imports System.Web.Hosting
Imports System.Web.Management
Imports System.Security.Permissions
Imports System.Text
Imports System.Security.Cryptography
Imports CRF.BLL.Providers
Imports System.Windows.Forms
Imports HDS.DAL.COMMON
Namespace Providers
    Friend Class SQLLiensProcessesProvider
        Inherits ProviderBase
        Implements ILiensProcessesProvider
        Public Function GetDailyActivity(ByVal auser As Users.CurrentUser, ByVal afromdate As Date, ByVal athrudate As Date, ByVal adestpath As String) As Boolean Implements ILiensProcessesProvider.GetDailyActivity
            Dim mydest As String = adestpath & "\CLIENTCOPIES\"
            Common.FileOps.DirectoryClean(mydest, Date.Parse("12/30/2099"))
            If System.IO.Directory.Exists(mydest) = False Then
                System.IO.Directory.CreateDirectory(mydest)
            End If

            Dim mynoticehistory As String = auser.OutputFolder & "NOTICEHISTORY\"
            Dim myoutfolder As String = auser.OutputFolder & "CLIENTCOPIES\"
            Common.FileOps.DirectoryClean(myoutfolder, Date.MaxValue)
            If System.IO.Directory.Exists(myoutfolder) = False Then
                System.IO.Directory.CreateDirectory(myoutfolder)
            End If

            Dim mysproc As New SPROCS.uspbo_LiensDayEnd_GetDailyActivity
            mysproc.FromDate = Strings.FormatDateTime(afromdate.ToString, DateFormat.ShortDate)
            mysproc.ThruDate = Strings.FormatDateTime(athrudate.ToString, DateFormat.ShortDate)
            Dim myview As HDS.DAL.COMMON.TableView = DBO.Provider.GetTableView(mysproc)
            Do Until myview.EOF

                Dim myclientcode As String = myview.RowItem("NTOClientCode")
                Dim myclientID As String = myview.RowItem("NTOClientID")
                Dim mysproc1 As New SPROCS.uspbo_LiensDayEnd_GetDailyActivityByClient
                mysproc1.FromDate = Strings.FormatDateTime(afromdate.ToString, DateFormat.ShortDate)
                mysproc1.ThruDate = Strings.FormatDateTime(athrudate.ToString, DateFormat.ShortDate)
                mysproc1.ClientId = myclientID

                Dim myds As DataSet = DBO.Provider.GetDataSet(mysproc1)

                Dim myview1 As New HDS.DAL.COMMON.TableView(myds.Tables(0))
                Dim myview2 As New HDS.DAL.COMMON.TableView(myds.Tables(1))
                auser.OutputFolder = myoutfolder & myclientcode & "\"
                Common.FileOps.DirectoryClean(auser.OutputFolder, Date.MaxValue)
                If System.IO.Directory.Exists(auser.OutputFolder) = False Then
                    System.IO.Directory.CreateDirectory(auser.OutputFolder)
                End If

                Dim MYSUBTITLE1 As String = "Activity for " & Strings.FormatDateTime(mysproc1.FromDate, DateFormat.ShortDate)
                Dim filelist As New ArrayList
                Dim sfile As String = ""

                sfile = Common.Reporting.PrintC1Report(auser, "WebReportsLiens.xml", "JobsPlaced", myview1, Common.PrintMode.PrintToPDF, myclientcode, MYSUBTITLE1)
                filelist.Add(sfile)
                sfile = Common.Reporting.PrintC1Report(auser, "WebReportsLiens.xml", "NoticeSent", myview2, Common.PrintMode.PrintToPDF, myclientcode, MYSUBTITLE1)
                filelist.Add(sfile)
                myview2.RowFilter = "TypeCode='OW' AND IsPrimary=1"
                myview2.MoveFirst()
                Do Until myview2.EOF
                    Dim MYNOTICE As String = mynoticehistory & myview2.RowItem("NoticeImage")
                    If System.IO.File.Exists(MYNOTICE) Then
                        Dim mydest1 As String = auser.OutputFolder & myview2.RowItem("JobId") & "-" & myview2.RowItem("FormCode") & ".PDF"
                        If System.IO.File.Exists(mydest1) = False Then
                            System.IO.File.Copy(MYNOTICE, mydest1)
                            filelist.Add(mydest1)
                        End If
                    End If
                    myview2.MoveNext()
                Loop
                myview.MoveNext()
                Dim mydestfilename = mydest & myclientcode & "-LiensDailyActivity" & ".pdf"
                Common.PDFUTILS.Combine(filelist, mydestfilename)
            Loop

            Dim MYSPROC2 As New BLL.CRFDB.SPROCS.uspbo_LiensDayEnd_GetFATExtract
            Dim MYVIEW3 As HDS.DAL.COMMON.TableView = Providers.DBO.Provider.GetTableView(MYSPROC2)
            myoutfolder = adestpath & "\FATFILE\"
            Common.FileOps.DirectoryClean(myoutfolder, Date.MaxValue)
            If System.IO.Directory.Exists(myoutfolder) = False Then
                System.IO.Directory.CreateDirectory(myoutfolder)
            End If
            MYVIEW3.ExportToCSV(myoutfolder & "\FATEXTRACT.CSV")

        End Function
        Public Function GetDailyPrelimNotices(ByVal auser As Users.CurrentUser, ByVal afromdate As Date, ByVal athrudate As Date, ByVal adestpath As String) As Boolean Implements ILiensProcessesProvider.GetDailyPrelimNotices
            Dim mydest As String = adestpath
            Dim mynoticehistory As String = auser.OutputFolder & "NOTICEHISTORY\"
            If System.IO.Directory.Exists(mynoticehistory) = False Then
                System.IO.Directory.CreateDirectory(mynoticehistory)
            End If

            Dim mysproc As New SPROCS.uspbo_LiensDayEnd_GetNotices
            mysproc.FromDate = Strings.FormatDateTime(afromdate.ToString, DateFormat.ShortDate)
            mysproc.ThruDate = Strings.FormatDateTime(athrudate.ToString, DateFormat.ShortDate)
           
            Liens.PrelimNotices.PrintNotices(auser, DBO.Provider.GetTableView(mysproc), afromdate, athrudate, False, True, Nothing, CRF.BLL.Common.PrintMode.PrintToPDF)

            Common.FileOps.DirectoryClean(mydest, Date.MaxValue)
            If System.IO.Directory.Exists(mydest) = False Then
                System.IO.Directory.CreateDirectory(mydest)
            End If

            Common.FileOps.DirectoryCopy(mynoticehistory, mydest, "*.*", True, Date.MaxValue)

        End Function
        Public Function GetTexasStatements(ByVal auser As Users.CurrentUser, ByVal asproc As CRFDB.SPROCS.uspbo_PrelimNotices_GetTexasStatements, ByVal aprintmode As Common.PrintMode, ByVal adestpath As String) As Object Implements ILiensProcessesProvider.GetTexasStatements

            Dim ads As DataSet = DBO.Provider.GetDataSet(asproc)
            If ads.Tables(0).Rows.Count < 1 Then
                Return Nothing
            End If

            Return Common.Reporting.PrintDataReport(auser, "TexasStatement.DRL", "TexasStatements", ads, aprintmode, auser.UserName, "", "")

        End Function
        Public Function PrintPrelimNotice(ByVal auser As Users.CurrentUser, ByVal anoticeid As String, ByVal aprintmode As Common.PrintMode) As Object Implements ILiensProcessesProvider.PrintPrelimNotice
            Dim mydir1 As String = auser.OutputFolder & "PRELIMNOTICES\"
            If System.IO.Directory.Exists(mydir1) = False Then
                System.IO.Directory.CreateDirectory(mydir1)
            End If

            Dim myhistory As New CRFDB.TABLES.JobNoticeHistory
            Dim myrequest As New CRFDB.TABLES.JobNoticeRequest
            Dim myform As New TABLES.JobNoticeRequestForms

            Dim mysproc As New SPROCS.uspbo_PrelimNotices_GetNoticeInfo
            mysproc.JobNoticeHistoryId = anoticeid
            Dim myds As DataSet = DBO.Provider.GetDataSet(mysproc)

            Dim myview As New DAL.COMMON.TableView(myds.Tables(0))
            Dim mycover As New DAL.COMMON.TableView(myds.Tables(0))
            Dim myview1 As New DAL.COMMON.TableView(myds.Tables(2))
            myview1.FillEntity(myhistory)
            Dim myview2 As New DAL.COMMON.TableView(myds.Tables(3))
            myview2.FillEntity(myrequest)

            myhistory.LastPrinted = Now()
            
            Dim mydate As String = Format(myview.RowItem(VIEWS.vwJobNoticeRequestInfo.ColumnNames.DatePrinted), "yyyyMMdd")
          
            Dim myforms As New DAL.COMMON.TableView(myds.Tables(1))
            Dim myfiles As New ArrayList
            Dim myreport As New BLL.Common.StandardReport
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\PrelimNotice.xml"
            myreport.OutputFolder = mydir1
            myreport.UserName = auser.UserName
            myreport.ReportDefPath = auser.SettingsFolder & "Reports\PrelimNotice.xml"
            myreport.OutputFolder = mydir1
            myreport.UserName = auser.UserName

            Dim mynotice As New Liens.Reports.JobPrelimNotice(auser)
            mynotice.ReportDefPath = auser.SettingsFolder & "Reports\PrelimNotice.xml"
            mynotice.OutputFolder = mydir1
            mynotice.UserName = auser.UserName
            mynotice.PrintMode = aprintmode

            myforms.MoveFirst()
            myforms.Sort = "SeqNo"
            Dim myfilename As String = ""
            myforms.Sort = "SEQNO DESC"
            Do Until myforms.EOF
               
                myforms.FillEntity(myform)
                myreport.ReportName = myform.ReportName
                myreport.DataView = myview
                myreport.PrintMode = aprintmode
                myreport.Sort = ""

                myfilename = myview.RowItem(VIEWS.vwJobNoticeRequestInfo.ColumnNames.PrintMajorGroup) & myview.RowItem(VIEWS.vwJobNoticeRequestInfo.ColumnNames.PrintMajorGroup)
                myfilename += myview.RowItem(VIEWS.vwJobNoticeRequestInfo.ColumnNames.PrintMajorGroup) & myview.RowItem(VIEWS.vwJobNoticeRequestInfo.ColumnNames.PrintGroup)
                myfilename += "-" & myview.RowItem(VIEWS.vwJobNoticeRequestInfo.ColumnNames.ClientCode)
                myfilename += "-" & myview.RowItem(VIEWS.vwJobNoticeRequestInfo.ColumnNames.JobId)
                myfilename += "-" & myview.RowItem(VIEWS.vwJobNoticeRequestInfo.ColumnNames.JobNoticeHistoryId)
                myfilename += "-" & myforms.RowItem(TABLES.JobNoticeRequestForms.ColumnNames.SeqNo)

                myreport.FilePrefix = myfilename
                If myreport.IsValid = True Then
                    Select Case myform.SeqNo
                        Case 1
                            myreport.DataView = mycover
                            myreport.RenderReport(False)
                            myfiles.Add(myreport.ReportFileName)

                        Case 2
                            mynotice.ExecuteC1Report(myview, myfilename)
                            myfiles.Add(mynotice.ReportFileName)

                        Case 4
                            Dim myview3 As New DAL.COMMON.TableView(myds.Tables(2))
                            myreport.DataView = myview3
                            myreport.RenderReport(False)
                            myfiles.Add(myreport.ReportFileName)

                        Case 5
                            If myds.Tables(4).Rows.Count = 1 Then
                                Dim mysp As New BLL.CRFDB.SPROCS.uspbo_PrelimNotices_GetTexasStatement
                                mysp.JobNoticeHistoryId = anoticeid
                                Dim myds1 As DataSet = DBO.Provider.GetDataSet(mysp)
                                Dim myuser As New CRF.BLL.Users.CurrentUser
                                myuser.UserName = auser.UserName
                                myuser.OutputFolder = mydir1
                                myuser.SettingsFolder = auser.SettingsFolder
                                Dim myfile As String = Common.Reporting.PrintDataReport(myuser, "TexasStatement.DRL", "TexasStatement", myds1, Common.PrintMode.PrintToPDF, myfilename, "", "")
                                myfiles.Add(myfile)
                            End If

                        Case Else
                            myreport.DataView = myview

                            myreport.RenderReport(False)
                            myfiles.Add(myreport.ReportFileName)
                    End Select
                End If
                myforms.MoveNext()
            Loop

            DBO.Provider.Update(myhistory)

            Return True

        End Function
        Public Function PrintMailLog(ByVal auser As Users.CurrentUser, ByVal asproc As CRFDB.SPROCS.uspbo_PrelimNotices_GetNoticesByDateRequested, ByVal areportprefix As String, ByVal aprintmode As COMMON.PrintMode) As Object Implements ILiensProcessesProvider.PrintMailLog
            Dim mydir1 As String = auser.OutputFolder & "PRELIMNOTICES\"
            If System.IO.Directory.Exists(mydir1) = False Then
                System.IO.Directory.CreateDirectory(mydir1)
            End If
            auser.OutputFolder = mydir1
            Dim myview As HDS.DAL.COMMON.TableView = DBO.Provider.GetTableView(asproc)
            Dim myreportdef As String = "PrelimNotice.xml"
            Dim myreportname As String = "MailingLog"
            Dim myreportprefix As String = asproc.PrintMajorGroup & asproc.PrintGroup & "-ZZZZZZ"
            Dim myrpt As String = CRF.BLL.Common.Reporting.PrintC1Report(auser, myreportdef, myreportname, myview, aprintmode, myreportprefix, "", "")
            Return myrpt

        End Function
        Public Function GetRequestedPrelimNotices(ByVal auser As Users.CurrentUser, ByVal asproc As CRFDB.SPROCS.uspbo_PrelimNotices_GetNoticesByDateRequested, ByVal ageneratecrt As Boolean) As HDS.DAL.COMMON.TableView Implements ILiensProcessesProvider.GetRequestedPrelimNotices
            Dim mydir1 As String = auser.OutputFolder & "PRELIMNOTICES\"
            If System.IO.Directory.Exists(mydir1) = False Then
                System.IO.Directory.CreateDirectory(mydir1)
            End If
            If asproc.RunType = 0 Then
                Common.FileOps.DirectoryClean(mydir1, Date.MaxValue)
            End If

            Dim aview As HDS.DAL.COMMON.TableView = DBO.Provider.GetTableView(asproc)
            If ageneratecrt = True Then
                aview.MoveFirst()
                Do Until aview.EOF
                    Dim mynoticeid As String = aview.RowItem(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.JobNoticeHistoryId)
                    GetNextCERT(mynoticeid)
                    aview.MoveNext()
                Loop
            End If

            asproc.RunType = 1
            aview = DBO.Provider.GetTableView(asproc)
            aview.MoveFirst()
            Dim myspreadsheetpath As String = Common.FileOps.GetFileName(auser, "CERTFILE", ".CSV", True)
            aview.ExportColumns.Add("SEQNO")
            aview.ExportColumns.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.JobId)
            aview.ExportColumns.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.CoverName)
            aview.ExportColumns.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.CoverAdd1)
            aview.ExportColumns.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.CoverAdd2)
            aview.ExportColumns.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.CoverCity)
            aview.ExportColumns.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.CoverState)
            aview.ExportColumns.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.CoverZip)
            aview.ExportColumns.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.ClientCode)
            aview.ExportColumns.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.CertNum)
            aview.ExportColumns.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.Greencard)
            aview.ExportToCSV(myspreadsheetpath)

            aview.MoveFirst()

            
            Return aview

        End Function
        Private Shared Function GetNextCERT(ByVal anoticeid As String) As Object

            ' CHECK IF THIS NOTICE ALREADY HAS A CERT
            Dim myusscodes As New TABLES.USSCodes
            Dim li As New HDS.DAL.COMMON.KeyValue
            DBO.Provider.Read(1, myusscodes)
            If myusscodes.Id < 1 Then
                Throw New Exception("USSCODES TABLE EMPTY")
            End If

            Dim myhistory As New CRFDB.TABLES.JobNoticeHistory
            DBO.Provider.Read(anoticeid, myhistory)
            If myhistory.Id <> anoticeid Then
                Throw New Exception("ERROR READING JOBNOTICEHISTORY")
            End If

            myhistory.CertNum = ""
           
            Dim strServiceTypeCode As String
            Dim strPackageID As String
            Dim strCustomerID As String
            Dim lngMaxPackageID As Long
            Dim vntCode(1, 19)
            Dim x As Integer
            Dim strCode As String
            Dim y As Integer, z As Integer
            Dim strTmp As String
            'Set the first row of the Array
            For x = 0 To 19
                vntCode(0, x) = -(x + 1) + 21
            Next

            strServiceTypeCode = Strings.Right("000000000" + myusscodes.ServiceTypeCode, 2)
            strCustomerID = Strings.Right("000000000" + myusscodes.CustomerID, 9)
            strPackageID = Strings.Right("000000000" + myusscodes.PackageID, 8)
            lngMaxPackageID = myusscodes.MaxPackageID

            If CLng(strPackageID) > lngMaxPackageID Then
                Throw New Exception("MAXIMUM NUMBER OF CERT CODES REACHED")
            End If

            strCode = strServiceTypeCode & strCustomerID & strPackageID

            'Make sure that we have 19 Digits
            If Len(strCode) <> 19 Then
                Throw New Exception("NOT ENOUGH DIGITS FOR THE SERVICE TYPE CODE")
            End If

            'Add the Code to the Array
            For x = 0 To 19
                vntCode(1, x) = Mid(strCode, x + 1, 1)
            Next

            'Now we'll add up the even codes
            For x = 0 To 19
                Select Case x
                    Case 0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20
                        y = y + vntCode(1, x)
                End Select
            Next

            y = y * 3

            'Now we'll add up the odd codes
            For x = 0 To 19
                Select Case x
                    Case 1, 3, 5, 7, 9, 11, 13, 15, 17
                        z = z + vntCode(1, x)
                End Select
            Next

            z = z + y

            'Get the right most number
            z = Strings.Right(z, 1)

            'The Check digit is the number that added to z = 10
            If z = 0 Then
                z = 0
            Else
                z = 10 - z
            End If

            strTmp = strCode & z

            myhistory.CertNum = Strings.Left(strTmp, 4) & " " & Mid(strTmp, 5, 4) & " " & Mid(strTmp, 9, 4) & " " & Mid(strTmp, 13, 4) & " " & Mid(strTmp, 17, 4)
            DBO.Provider.Update(myhistory)

            myusscodes.PackageID = myusscodes.PackageID + 1
            DBO.Provider.Update(myusscodes)

            Return True

        End Function

    End Class

End Namespace
