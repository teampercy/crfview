Namespace Providers
    Public Interface ILiensProcessesProvider
        Function GetDailyPrelimNotices(ByVal auser As Users.CurrentUser, ByVal afromdate As Date, ByVal athrudate As Date, ByVal adestpath As String) As Boolean
        Function GetDailyActivity(ByVal auser As Users.CurrentUser, ByVal afromdate As Date, ByVal athrudate As Date, ByVal adestpath As String) As Boolean
        Function GetTexasStatements(ByVal auser As Users.CurrentUser, ByVal asproc As CRFDB.SPROCS.uspbo_PrelimNotices_GetTexasStatements, ByVal aprintmode As CRF.BLL.Common.PrintMode, ByVal adestpath As String)
        Function GetRequestedPrelimNotices(ByVal auser As Users.CurrentUser, ByVal asproc As CRFDB.SPROCS.uspbo_PrelimNotices_GetNoticesByDateRequested, ByVal ageneratecrt As Boolean) As HDS.DAL.COMMON.TableView
        Function PrintPrelimNotice(ByVal auser As Users.CurrentUser, ByVal anoticeid As String, ByVal aprintmode As CRF.BLL.Common.PrintMode) As Object
        Function PrintMailLog(ByVal auser As Users.CurrentUser, ByVal asproc As CRFDB.SPROCS.uspbo_PrelimNotices_GetNoticesByDateRequested, ByVal areportprefix As String, ByVal aprintmode As CRF.BLL.Common.PrintMode) As Object

    End Interface
End Namespace
