Imports Microsoft.VisualBasic

Imports System
Imports System.Collections.Generic
Imports System.Configuration
Imports System.Web.Configuration
Namespace Providers
    Public Class LiensProcesses
        Private Shared _isInitialized As Boolean = False
        Private Shared _provider As ILiensProcessesProvider
        ' <summary>
        ' Returns provider
        ' </summary>
        Public Shared ReadOnly Property Provider() As ILiensProcessesProvider
            Get
                Initialize()
                Return _provider

            End Get
        End Property
        Private Shared Sub Initialize()
            If Not _isInitialized Then
                _provider = SingletonProvider(Of Providers.SQLLiensProcessesProvider).Instance
                _isInitialized = True
            End If
        End Sub
        Public Shared Function GetDailyActivity(ByVal auser As Users.CurrentUser, ByVal afromdate As Date, ByVal athrudate As Date, ByVal adestpath As String) As Boolean
            Return Provider.GetDailyActivity(auser, afromdate, athrudate, adestpath)
        End Function
        Public Shared Function GetDailyPrelimNotices(ByVal auser As Users.CurrentUser, ByVal afromdate As Date, ByVal athrudate As Date, ByVal adestpath As String) As Boolean
            Return Provider.GetDailyPrelimNotices(auser, afromdate, athrudate, adestpath)
        End Function
        Public Shared Function GetTexasStatements(ByVal auser As Users.CurrentUser, ByVal asproc As CRFDB.SPROCS.uspbo_PrelimNotices_GetTexasStatements, ByVal aprintmode As Common.PrintMode, ByVal adestpath As String)
            Return Provider.GetTexasStatements(auser, asproc, aprintmode, adestpath)
        End Function
        Public Shared Function GetRequestedPrelimNotices(ByVal auser As Users.CurrentUser, ByVal asproc As CRFDB.SPROCS.uspbo_PrelimNotices_GetNoticesByDateRequested, ByVal ageneratecrt As Boolean)
            Return Provider.GetRequestedPrelimNotices(auser, asproc, ageneratecrt)
        End Function
        Public Shared Function PrintPrelimNotice(ByVal auser As Users.CurrentUser, ByVal anoticeid As String, ByVal aprintmode As CRF.BLL.Common.PrintMode) As Object
            Return Provider.PrintPrelimNotice(auser, anoticeid, aprintmode)
        End Function
        Public Shared Function PrintMailLog(ByVal auser As Users.CurrentUser, ByVal asproc As CRFDB.SPROCS.uspbo_PrelimNotices_GetNoticesByDateRequested, ByVal areportprefix As String, ByVal aprintmode As CRF.BLL.Common.PrintMode) As Object
            Return Provider.PrintMailLog(auser, asproc, areportprefix, aprintmode)
        End Function

    End Class
End Namespace
