Namespace CRM.RapidPay
    Public Class Client
        Inherits CRFDB.TABLES.ClientRapidPay
        Dim ObjectState As New ObjectState
        Public Sub New()
            MyBase.New()
        End Sub
        Public Function SelectById(ByVal keyvalue As String) As Boolean
            Me.ObjectState.IsValid = DBO.Read(keyvalue, Me)
            Me.ObjectState.LastErrorMessage = DBO.LastError
            Return Me.ObjectState.IsValid

        End Function
        Public Function Insert() As Boolean
            Me.ObjectState.IsValid = DBO.Create(Me)
            Me.ObjectState.LastErrorMessage = DBO.LastError
            Return Me.ObjectState.IsValid

        End Function
        Public Function Delete() As Boolean
            Me.ObjectState.IsValid = DBO.Delete(Me)
            Me.ObjectState.LastErrorMessage = DBO.LastError
            Return Me.ObjectState.IsValid

        End Function
        Public Function Update() As Boolean
            Me.ObjectState.IsValid = DBO.Update(Me)
            Me.ObjectState.LastErrorMessage = DBO.LastError
            Return Me.ObjectState.IsValid

        End Function
        Public ReadOnly Property UnitsLeft() As Integer
            Get

            End Get
        End Property
    End Class
End Namespace
