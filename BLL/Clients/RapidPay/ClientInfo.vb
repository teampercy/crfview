Namespace CRM.ViewClients
    Public Class ClientsInfo
        Inherits CRFDB.TABLES.Client
        Dim ObjectState As New ObjectState
        Public SprocParams As New CRFDB.SPROCS.uspbo_CRM_GetClientInfo
        Dim myds As DataSet
        Public CurrentUser As Users.CurrentUser
        Public Sub New(ByVal auser As Users.CurrentUser)
            MyBase.New()
            Me.CurrentUser = auser
     
        End Sub
        Public Function SelectById(ByVal keyvalue As String) As Boolean
            SprocParams.ClientId = keyvalue
            myds = DBO.GetDataSet(SprocParams)
            Me.ObjectState.IsValid = DBO.Read(keyvalue, Me)
            Me.ObjectState.LastErrorMessage = DBO.LastError
            Return Me.ObjectState.IsValid
        End Function
           Public Function CollectionInfo() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(1), "")
        End Function
        Public Function LienInfo() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(2), "")
        End Function
        Public Function Contracts() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(3), "")
        End Function
        Public Function Branches() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(4), "")
        End Function
        Public Function OpenIssues() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(5), "")
        End Function
        Public Function ClosedIssues() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(6), "")
        End Function
        Public Function Users() As DAL.COMMON.TableView
            Return New DAL.COMMON.TableView(myds.Tables(7), "")
        End Function
    End Class
End Namespace
