Namespace CRM.RapidPay
    Public Class Processes
        Public Shared Function SetUpAsClient(ByVal RapidPayId As Integer) As Boolean
            Dim mysproc As New CRFDB.SPROCS.uspbo_CRM_RapidPayCreateClient
            mysproc.RapidPayClientId = RapidPayId
            DBO.ExecNonQuery(mysproc)
            Return True
        End Function
        Public Shared Function OrderRequest(ByVal ClientId As Integer, ByVal Units As Integer, ByVal UnitPrice As Decimal) As Boolean
            Dim mysproc As New CRFDB.SPROCS.uspbo_CRM_RapidPayAdditionalOrderRequest
            mysproc.RapidPayClientId = ClientId
            mysproc.JobQty = Units
            mysproc.UnitPrice = UnitPrice
            DBO.ExecNonQuery(mysproc)
            Return True
        End Function
        Public Shared Function PendingList() As DAL.COMMON.TableView
            Dim mysproc As New CRFDB.SPROCS.uspbo_CRM_RapidPayGetPendingList
            Return DBO.GetTableView(mysproc)
        End Function

    End Class
End Namespace
