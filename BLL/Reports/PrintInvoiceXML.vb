﻿Imports System
Imports System.Windows.Forms
Imports C1.C1Report
Imports CRF.BLL.Users

Namespace Liens.Reports
    Public Class PrintInvoiceXML
        Inherits HDS.Reporting.C1Reports.Common.StandardReport
        Dim m_rtf As RichTextBox
        Dim m_filename As String
        Dim m_formfolder As String
        Dim _reportpath As String
        Dim _settingsfolder As String
        Dim _reportdefpath As String
        Dim _username As String
        Dim srtf As String
        Dim mywaiverinfo As BLL.ClientView.Lien.JobWaiverInfo
        Dim myimage As New HDS.WINLIB.Controls.ImageViewer
        Dim Client As HDS.DAL.COMMON.TableView
        Dim ClientImage As HDS.DAL.COMMON.TableView
        Dim dummyTable As HDS.DAL.COMMON.TableView
        Dim flexfieldtext As String
        Public CurrentUser As CurrentUser

        Public Sub New()
            MyBase.New()
            CurrentUser = Globals.GetCurrentUser
            'mywaiverinfo = New BLL.ClientView.Lien.JobWaiverInfo(aitemid)
            Me.OutputFolder = CurrentUser.OutputFolder & "\"
            Me.SettingsFolder = CurrentUser.SettingsFolder & "\"
            Me.UserName = CurrentUser.UserName

        End Sub

        Public Function RenderC1Report(ByVal myuser As BLL.Users.CurrentUser, ByVal adatatable As DataTable, ByVal areportdef As String, ByVal areportname As String, Optional ByVal aprintmode As BLL.COMMON.PrintMode = BLL.COMMON.PrintMode.PrintToPDF, Optional ByVal areportprefix As String = "", Optional ByVal asubtitle1 As String = "", Optional ByVal asubtitle2 As String = "", Optional ByVal aisLandscape As Boolean = False) As String
            Try
                'Dim myuser As BLL.Users.CurrentUser = GetCurrentUser()
                Dim myreportpath As String = myuser.SettingsFolder & "\REPORTS\" & areportdef
                Dim myoutputfolder As String = myuser.OutputFolder & "\"
                'dummyTable = New HDS.DAL.COMMON.TableView(adatatable)
                'Me.DataView = dummyTable


                ' Dim MYC1RPT As New HDS.Reporting.C1Reports.C1ReportProvider
                'Dim MYC1RPT As New HDS.Reporting.C1Reports.Common.StandardReport
                'With Me
                ''.UserName = myuser.UserName
                '.ReportDefPath = myreportpath

                ''.OutputFolder = myoutputfolder
                ''.SettingsFolder = myuser.SettingsFolder


                'Dim MYAGENCY As New CRFDB.TABLES.Agency
                ''DBO.Read(1, MYAGENCY)
                'DAL.Provider.DAL.Read(1, MYAGENCY)


                'Dim mylogo As New HDS.WINLIB.Controls.ImageViewer
                'mylogo.LoadImage(MYAGENCY.AgencyLogo)
                '.ReportLogo = mylogo.PictureBox1.Image
                '.ReportFooter = MYAGENCY.AgencyRptFooter
                ''If aisLandscape = True Then
                ''    MYC1RPT.C1R.Document.DefaultPageSettings.Landscape = True
                ''Else
                ''    MYC1RPT.C1R.Document.DefaultPageSettings.Landscape = False
                ''End If
                'Dim adataview As New DataView(adatatable)
                '.RenderMode = aprintmode
                '.DataView = adataview
                '.ReportName = areportname
                '.FilePrefix = "WAIVER-123"
                '.TableIndex = 0
                '.RenderReport()
                '.ReportPath = .ReportFileName

                '' MYC1RPT.ReportPath = MYC1RPT.ReportFileName
                ''MYC1RPT.Render("", myoutputfolder, myreportpath, areportname, adataview, aprintmode, areportprefix, asubtitle1, asubtitle2)
                'Return .ReportPath
                Me.UserName = myuser.UserName

                Me.ReportDefPath = myreportpath

                Me.OutputFolder = myoutputfolder
                Me.SettingsFolder = myuser.SettingsFolder
                'Me.ReportDefPath = myreportpath
                dummyTable = New HDS.DAL.COMMON.TableView(adatatable)
                    Me.ReportName = areportname
                    'The DataView property used only for to retrived Detail Section from Flex report. We not used any value from Table(0).
                    Me.DataView = dummyTable
                    Dim MYAGENCY As New CRFDB.TABLES.Agency
                    DBO.Read(1, MYAGENCY)
                    Dim mylogo As New HDS.WINLIB.Controls.ImageViewer
                    mylogo.LoadImage(MYAGENCY.AgencyLogo)

                With Me
                    .ReportLogo = mylogo.PictureBox1.Image
                    .ReportFooter = MYAGENCY.AgencyRptFooter
                    .FilePrefix = ""
                    .TableIndex = 0
                    .RenderReport()
                    .ReportPath = .ReportFileName
                    Return .ReportFileName
                End With
            Catch ex As Exception
                Return ex.ToString()
            End Try

        End Function
        Public Overrides Sub StartSection(ByVal sender As Object, ByVal e As C1.C1Report.ReportEventArgs)

            Dim C1R As C1.C1Report.C1Report = sender

CheckHeader:

            Try
                'If HasField("CLIENTLOGO", e) = True Then
                '    Dim ZLOGO As Field = C1R.Fields("CLIENTLOGO")
                '    If ZLOGO.Section = e.Section Then
                '        'ZLOGO.Picture = GetLogo()
                '        ZLOGO.PictureScale = PictureScaleEnum.Scale
                '        If IsNothing(ZLOGO.Picture) = True Then
                '            ZLOGO.Calculated = True
                '            ZLOGO.Visible = True
                '        Else
                '            ZLOGO.Calculated = True
                '            ZLOGO.Visible = True
                '        End If

                '    End If
                'End If

                'If HasField("CLIENTADDRESS", e) = True Then
                '    Dim ZADDR As Field = C1R.Fields("CLIENTADDRESS")
                '    If ZADDR.Section = e.Section Then
                '        ZADDR.Calculated = True
                '        'ZADDR.RTF = True
                '        ZADDR.Visible = True
                '        'ZADDR.Font.Bold = True
                '        'ZADDR.Value = GetAddress(ZADDR.Value)
                '    End If
                'End If

                'If HasField("WAIVERADDRESS", e) = True Then
                '    Dim ZWAIVERADDRESS As Field = C1R.Fields("WAIVERADDRESS")
                '    If ZWAIVERADDRESS.Section = e.Section Then
                '        ZWAIVERADDRESS.Calculated = True
                '        'ZWAIVERADDRESS.RTF = True
                '        ZWAIVERADDRESS.Visible = True
                '        'ZWAIVERADDRESS.Value = GetWaiverReportDetails(ZWAIVERADDRESS.Value)
                '    End If
                'End If

                'To get Report Heading
                If HasField("Text4", e) = True Then
                    Dim ZWAIVERHEADING As Field = C1R.Fields("Text4")
                    If ZWAIVERHEADING.Section = e.Section Then
                        ZWAIVERHEADING.Calculated = True
                        ZWAIVERHEADING.Visible = True
                        ZWAIVERHEADING.ForeColor = Drawing.Color.Navy

                        ZWAIVERHEADING.Value = ZWAIVERHEADING.Value
                    End If
                End If
                If HasField("Text1", e) = True Then
                    Dim labal1 As Field = C1R.Fields("Text1")
                    If labal1.Section = e.Section Then
                        labal1.Calculated = True
                        labal1.Visible = True
                        labal1.ForeColor = Drawing.Color.Navy
                        labal1.Value = labal1.Value
                    End If
                End If

                If HasField("Text2", e) = True Then
                    Dim labal2 As Field = C1R.Fields("Text2")
                    If labal2.Section = e.Section Then
                        labal2.Calculated = True
                        labal2.Visible = True
                        labal2.ForeColor = Drawing.Color.Navy
                        labal2.Value = labal2.Value
                    End If
                End If

                If HasField("Text3", e) = True Then
                    Dim labal3 As Field = C1R.Fields("Text3")
                    If labal3.Section = e.Section Then
                        labal3.Calculated = True
                        labal3.Visible = True
                        labal3.ForeColor = Drawing.Color.Navy
                        labal3.Value = labal3.Value
                    End If
                End If

                If HasField("Text80", e) = True Then
                    Dim labal4 As Field = C1R.Fields("Text80")
                    If labal4.Section = e.Section Then
                        labal4.Calculated = True
                        labal4.Visible = True
                        labal4.ForeColor = Drawing.Color.Navy
                        labal4.Value = labal4.Value
                    End If
                End If

                'If HasField("Line13", e) = True Then
                '    Dim labal5 As Field = C1R.Fields("Line13")
                '    If labal5.Section = e.Section Then
                '        labal5.Calculated = True
                '        labal5.Visible = True
                '        labal5.ForeColor = Drawing.Color.Navy
                '        labal5.LineWidth = 5
                '        labal5.LineSlant = BorderStyle.FixedSingle

                '        'labal5.Value = labal5.Value
                '    End If
                'End If
                If HasField("Line13", e) = True Then
                    Dim ZWAIVERREPORTDETAILLS As Field = C1R.Fields("Line13")
                    If ZWAIVERREPORTDETAILLS.Section = e.Section Then
                        ZWAIVERREPORTDETAILLS.Calculated = True
                        ZWAIVERREPORTDETAILLS.Visible = True
                        ZWAIVERREPORTDETAILLS.CanGrow = True
                        'ZWAIVERREPORTDETAILLS.CanShrink = True
                        ZWAIVERREPORTDETAILLS.Height = 5
                        ZWAIVERREPORTDETAILLS.LineSpacing = 100
                        ZWAIVERREPORTDETAILLS.BorderStyle = BorderStyleEnum.Solid
                        ZWAIVERREPORTDETAILLS.Anchor = AnchorEnum.Bottom
                        ZWAIVERREPORTDETAILLS.BorderColor = Drawing.Color.Black

                        'ZWAIVERREPORTDETAILLS.Value = ZWAIVERREPORTDETAILLS.Value
                    End If
                End If

                If HasField("SubTitle2", e) = True Then
                    Dim labal6 As Field = C1R.Fields("SubTitle2")
                    If labal6.Section = e.Section Then
                        labal6.Calculated = True
                        labal6.Visible = True
                        labal6.ForeColor = Drawing.Color.Navy
                        labal6.Format = FormatDateTime(Now)

                        'labal6.Value = labal6.Value
                    End If
                End If
                If HasField("SubTitle1", e) = True Then
                    Dim labal7 As Field = C1R.Fields("SubTitle1")
                    If labal7.Section = e.Section Then
                        labal7.Calculated = True
                        labal7.Visible = True
                        labal7.ForeColor = Drawing.Color.Navy
                        'labal7.Value = labal7.Value
                    End If
                End If

                If HasField("Field1", e) = True Then
                    Dim labal8 As Field = C1R.Fields("Field1")
                    If labal8.Section = e.Section Then
                        labal8.Calculated = True
                        labal8.Visible = True
                        labal8.CanGrow = True
                        'ZWAIVERREPORTDETAILLS.CanShrink = True
                        labal8.Height = 5
                        labal8.LineSpacing = 100
                        labal8.BorderStyle = BorderStyleEnum.Solid
                        labal8.Anchor = AnchorEnum.Bottom
                        labal8.BorderColor = Drawing.Color.Black
                        ' labal8.Value = labal8.Value
                    End If
                End If
                If HasField("Text", e) = True Then
                    Dim labal9 As Field = C1R.Fields("Text")
                    If labal9.Section = e.Section Then
                        labal9.Calculated = True
                        labal9.Visible = True
                        labal9.ForeColor = Drawing.Color.Navy
                        'labal9.Value = labal9.Value
                    End If
                End If


                ''To get Report Heading- Report:GANP
                'If HasField("SIGNATURETEXT1", e) = True Then
                '    Dim ZWAIVERHEADING1 As Field = C1R.Fields("SIGNATURETEXT1")
                '    If ZWAIVERHEADING1.Section = e.Section Then
                '        ZWAIVERHEADING1.Calculated = True
                '        ZWAIVERHEADING1.Visible = True
                '        'ZWAIVERHEADING1.Value = GetWaiverHeading(ZWAIVERHEADING1.Value)
                '    End If
                'End If

                ''To get Report Details
                'If HasField("FIELD2", e) = True Then
                '    Dim ZWAIVERREPORTDETAILLS As Field = C1R.Fields("FIELD2")
                '    If ZWAIVERREPORTDETAILLS.Section = e.Section Then
                '        ZWAIVERREPORTDETAILLS.Calculated = True
                '        ZWAIVERREPORTDETAILLS.Visible = True
                '        ZWAIVERREPORTDETAILLS.CanGrow = True
                '        ZWAIVERREPORTDETAILLS.CanShrink = True
                '        ' ZWAIVERREPORTDETAILLS.Value = GetWaiverReportDetails(ZWAIVERREPORTDETAILLS.Value)
                '    End If
                'End If

                ''To get Report Details
                'If HasField("WAIVERTEXT", e) = True Then
                '    Dim ZWAIVERREPORTDETAILLS1 As Field = C1R.Fields("WAIVERTEXT")
                '    If ZWAIVERREPORTDETAILLS1.Section = e.Section Then
                '        ZWAIVERREPORTDETAILLS1.Calculated = True
                '        ZWAIVERREPORTDETAILLS1.Visible = True
                '        ZWAIVERREPORTDETAILLS1.CanGrow = True
                '        ZWAIVERREPORTDETAILLS1.CanShrink = True
                '        'ZWAIVERREPORTDETAILLS1.Value = GetWaiverReportDetails(ZWAIVERREPORTDETAILLS1.Value)
                '    End If
                'End If

                'If HasField("CLIENTSIGNATURELOGO", e) = True Then
                '    If IsNothing(mywaiverinfo.ClientSigner.Signature) = False Then
                '        Dim ZSIGLOGO As Field = C1R.Fields("CLIENTSIGNATURELOGO")
                '        If ZSIGLOGO.Section = e.Section Then
                '            'ZSIGLOGO.Picture = GetSignatureLogo()
                '            ZSIGLOGO.Visible = True
                '        End If
                '    End If
                'End If

                'If HasField("SIGNATURETEXT", e) = True Then
                '    Dim ZSIGTEXT As Field = C1R.Fields("SIGNATURETEXT")
                '    If ZSIGTEXT.Section = e.Section Then
                '        ZSIGTEXT.Calculated = True
                '        ZSIGTEXT.Visible = True
                '        'ZSIGTEXT.Value = GetSignature(ZSIGTEXT.Value)
                '    End If
                'End If

                'If HasField("JOBWAIVERLOGIDBARCODE", e) = True Then
                '    Dim ZWAIVERBARCODE As Field = C1R.Fields("JOBWAIVERLOGIDBARCODE")
                '    If ZWAIVERBARCODE.Section = e.Section Then
                '        ZWAIVERBARCODE.Calculated = True
                '        'ZWAIVERBARCODE.RTF = True
                '        ZWAIVERBARCODE.Visible = True
                '        ZWAIVERBARCODE.BarCode = BarCodeEnum.Code_128auto
                '        'ZWAIVERBARCODE.Value = GetJobWaiverLogId(ZWAIVERBARCODE.Value)
                '        'ZWAIVERBARCODE.Value = mywaiverinfo.JobWaiverLog.JobWaiverLogId
                '    End If
                'End If

                'If HasField("NOTARYTEXT", e) = True Then
                '    Dim ZNOTARY As Field = C1R.Fields("NOTARYTEXT")
                '    If ZNOTARY.Section = e.Section Then
                '        If mywaiverinfo.JobWaiverLog.Notary = True Then
                '            ZNOTARY.Visible = False
                '            ZNOTARY.Calculated = True
                '            ' ZNOTARY.Value = GetNotary(ZNOTARY.Value)
                '        Else
                '            ZNOTARY.Visible = False
                '            ZNOTARY.Value = ""
                '        End If
                '    End If
                'End If

                'If HasField("JOBWAIVERLOGID", e) = True Then
                '    Dim ZWAIVERLOGID As Field = C1R.Fields("JOBWAIVERLOGID")
                '    If ZWAIVERLOGID.Section = e.Section Then
                '        ZWAIVERLOGID.Calculated = True
                '        ZWAIVERLOGID.Visible = False
                '        'ZWAIVERLOGID.Value = GetJobWaiverLogId(ZWAIVERLOGID.Value)
                '    End If
                'End If

            Catch EX As Exception
                Throw New ApplicationException(EX.Message)
            End Try

        End Sub
        Private Function HasField(ByVal aname As String, ByVal e As C1.C1Report.ReportEventArgs) As Boolean
            Try
                Dim Zfield As Field = C1R.Fields(aname)
                If UCase(Zfield.Name) = UCase(aname) And e.Section = Zfield.Section Then
                    Return True
                Else
                    Return False
                End If
            Catch
                Return False
            End Try
        End Function

    End Class
End Namespace