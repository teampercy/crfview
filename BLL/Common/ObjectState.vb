Public Class ObjectState
    Public _IsValid As Boolean
    Public IsNew As Boolean
    Public Errors As Collection
    Public LastErrorMessage As String
    Public Sub New()
        Errors = New Collection
        IsValid = False
        IsNew = True
    End Sub
    Public Property IsValid() As Boolean
        Get
            If _IsValid = True And Errors.Count = 0 Then
                Return True
            Else
                Return False
            End If
        End Get
        Set(ByVal Value As Boolean)
            _IsValid = Value
        End Set
    End Property
End Class
