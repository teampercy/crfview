Imports System.Reflection
Namespace Common
    Friend Class KeyValueReplace
        Dim myvalues As Collection
        Public Sub New()
            myvalues = New Collection
        End Sub 'New
        Public Sub AddData(ByVal key As String, ByVal val As String, ByVal format As String)
            Dim newval As New KeyValue
            newval.Key = key
            newval.Value = val
            newval.Formatting = format
            myvalues.Add(newval, key)
        End Sub 'AddData
        Public Function Replace(ByVal atext As String) As String
            Dim pat As String = ""
            Dim newval As New KeyValue
            Dim s As String = atext
            For Each newval In myvalues
                s = Strings.Replace(s, "[" & UCase(newval.Key) & "]", newval.Value)
            Next
            Return s
        End Function
        Private Function GetValue(ByVal apropertyname As String, ByRef aentity As Object, ByVal adefault As String) As Object
            Try

                Dim t As Type = aentity.GetType()

                'Dim info As PropertyInfo = t.GetProperty(apropertyname)
                Dim info As PropertyInfo = aentity.GetType.GetProperty(apropertyname)

                Dim s As Object = info.GetValue(aentity, Nothing)
                Return s
            Catch EX As Exception
                Return adefault
            End Try

        End Function
    End Class
    Public Class KeyValue
        Public Key As String
        Public Value As String
        Public Formatting As String
    End Class
End Namespace

