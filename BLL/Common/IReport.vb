Namespace Common
    Public Interface IReport
        Property DataSource() As DataSet
        Property RenderMode() As PrintMode
        Property ExportColumns() As Collection
        Property UserName() As String
        Property ReportPrefix() As String
        Property SettingsFolder() As String
        Property ReportDefPath() As String
        Property ReportPath() As String
        Property SpreadSheetPath() As String
        Property ReportName() As String
        Property TableIndex() As Integer
        Property Filter() As String
        Property Sort() As String
        Property ReportTitle() As String
        Property SubTitle1() As String
        Property SubTitle2() As String
        Property ReportFooter() As Object
        Property ReportLogo() As System.Drawing.Image
        Property OutputFolder() As String
        Function Render() As Boolean
        Function Execute(ByVal adataset As DataSet, Optional ByVal atableindex As Integer = 0, Optional ByVal Export As Boolean = False, Optional ByVal PrintMode As PrintMode = PrintMode.PrintToPDF, Optional ByVal exportcolumns As Collection = Nothing) As Boolean

    End Interface
   
End Namespace
