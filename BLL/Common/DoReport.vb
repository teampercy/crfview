Namespace Common
    Public Class DoReport
        Implements IReport
        Dim _ds As DataSet
        Dim _dv As DataView
        Dim _renderMode As Common.PrintMode = PrintMode.PrintToPDF
        Dim _filter As String
        Dim _tableindex As Integer
        Dim _reportname As String = ""
        Dim _username As String = ""
        Dim _reportprefix As String = ""
        Dim _settingsfolder As String = ""
        Dim _outputfolder As String = ""
        Dim _reporttitle As String = ""
        Dim _reportpath As String = ""
        Dim _spreadsheetpath As String = ""
        Dim _reportdefpath As String = ""
        Dim _sort As String = ""
        Dim _subtitle1 As String = ""
        Dim _subtitle2 As String = ""
        Dim _userid As String = ""
        Dim m_reportfooter As Object
        Dim m_reportlogo As System.Drawing.Image
        Dim _exportColumns As New Collection
        Dim MYAGENCY As New CRFDB.TABLES.Agency
        Public ReportDef As String
        Public Sub New()
            DBO.Read(1, MYAGENCY)
            'Dim mylogo As New HDS.WINCORE.Controls.ImageViewer
            ''mylogo.LoadImage(MYAGENCY.AgencyLogo)
            ''Me.Report.ReportLogo = mylogo.PictureBox1.Image
            ''Me.Report.ReportFooter = MYAGENCY.AgencyRptFooter

        End Sub
        Public Property Filter() As String Implements IReport.Filter
            Get
                Return _filter
            End Get
            Set(ByVal Value As String)
                _filter = Value
            End Set
        End Property
        Public Property OutPutFolder() As String Implements IReport.OutputFolder
            Get
                Return _outputfolder
            End Get
            Set(ByVal Value As String)
                _outputfolder = Value
            End Set
        End Property
        Public Property ReportTitle() As String Implements IReport.ReportTitle
            Get
                Return _reporttitle
            End Get
            Set(ByVal Value As String)
                _reporttitle = Value
            End Set
        End Property
        Public Property Sort() As String Implements IReport.Sort
            Get
                Return _sort
            End Get
            Set(ByVal Value As String)
                _sort = Value
            End Set
        End Property
        Public Property SubTitle1() As String Implements IReport.SubTitle1
            Get
                Return _subtitle1
            End Get
            Set(ByVal Value As String)
                _subtitle1 = Value
            End Set
        End Property
        Public Property SubTitle2() As String Implements IReport.SubTitle2
            Get
                Return _subtitle2
            End Get
            Set(ByVal Value As String)
                _subtitle2 = Value
            End Set
        End Property
        Public Property DataSource() As System.Data.DataSet Implements IReport.DataSource
            Get
                Return _ds
            End Get
            Set(ByVal Value As System.Data.DataSet)
                _ds = Value
            End Set
        End Property
        Public Overridable Function Render() As Boolean Implements IReport.Render

        End Function
        Public Function Execute(Optional ByVal Export As Boolean = False, Optional ByVal PrintMode As PrintMode = PrintMode.PrintToPDF, Optional ByVal exportcolumns As Collection = Nothing) As Boolean
            If Me.DataSource.Tables(0).Rows.Count > 0 Then
                Me.ReportPath = Me.Render
                Me.RenderMode = PrintMode
                If Export = True Then
                    Me.SpreadSheetPath = DAL.Transformers.Export.ExportToCSV(Me.DataSource.Tables(0), GetFileName(Me.ReportName), exportcolumns)
                End If
                Return True
            End If
            Return False

        End Function
        Public Function ExecuteC1Report(ByVal PDFReport As Boolean, Optional ByVal Export As Boolean = False, Optional ByVal PrintMode As PrintMode = PrintMode.PrintToPDF, Optional ByVal exportcolumns As Collection = Nothing) As Boolean
            If Me.DataSource.Tables(Me.TableIndex).Rows.Count < 1 Then
                Return False
            End If

            If PDFReport = True Then
                Dim MyReport As New StandardReport(Me.ReportDefPath, Me.OutPutFolder)
                MyReport.ReportName = ReportName
                MyReport.FilePrefix = UserName
                MyReport.SubTitle1 = Me.SubTitle1
                MyReport.SubTitle2 = Me.SubTitle2
                MyReport.DataSource = Me.DataSource
                MyReport.DataView = New DataView(Me.DataSource.Tables(Me.TableIndex))
                MyReport.PrintMode = Me.RenderMode
                MyReport.FilePrefix = Me.ReportPrefix
                If Me.ReportPrefix.Trim.Length < 1 Then
                    MyReport.FilePrefix = Me.UserName
                End If

                MyReport.Filter = Filter
                MyReport.Sort = Me.Sort
                MyReport.TableIndex = Me.TableIndex
                MyReport.ReportLogo = Me.ReportLogo
                MyReport.ReportFooter = Me.ReportFooter
                MyReport.RenderReport()
                Me.ReportPath = MyReport.ReportFileName

            End If

            Return True

        End Function

        Public Property SettingsFolder() As String Implements IReport.SettingsFolder
            Get
                Return _settingsfolder
            End Get
            Set(ByVal Value As String)
                _settingsfolder = Value
            End Set
        End Property
        Public Property TableIndex() As Integer Implements IReport.TableIndex
            Get
                Return _tableindex
            End Get
            Set(ByVal Value As Integer)
                _tableindex = Value
            End Set
        End Property
        Public Property UserName() As String Implements IReport.UserName
            Get
                Return _username
            End Get
            Set(ByVal Value As String)
                _username = Value
            End Set
        End Property
        Public Property ReportName() As String Implements IReport.ReportName
            Get
                Return _reportname
            End Get
            Set(ByVal Value As String)
                _reportname = Value
            End Set
        End Property
        Public Property SpreadSheetPath() As String Implements IReport.SpreadSheetPath
            Get
                If _spreadsheetpath.Trim.Length < 1 Then
                    _spreadsheetpath = Me.GetFileName(Me.ReportName)
                End If
                Return _spreadsheetpath
            End Get
            Set(ByVal Value As String)
                _spreadsheetpath = Value
            End Set
        End Property
        Public Property ReportPath() As String Implements IReport.ReportPath
            Get
                Return _reportpath
            End Get
            Set(ByVal Value As String)
                _reportpath = Value
            End Set
        End Property
        Public Property ReportDefPath() As String Implements IReport.ReportDefPath
            Get
                Return _reportdefpath
            End Get
            Set(ByVal Value As String)
                _reportdefpath = Value
            End Set
        End Property
        Public Function GetFileName(ByVal aprefix As String, Optional ByVal aext As String = ".CSV") As String
            Dim lsfile = Me.UserName & "-" & aprefix & "-" & Strings.Format(Now(), "yyyyMMddhhmmss") & aext
            Return _outputfolder & TrimSpace(lsfile)
        End Function
        Private Function TrimSpace(ByRef strInput As String) As String
            ' This procedure trims extra space from any part of
            ' a string.
            Dim str As String = strInput.Trim
            Return Join(str.Split(" "), "")

        End Function
        Public Function Execute(ByVal adataset As System.Data.DataSet, Optional ByVal atableindex As Integer = 0, Optional ByVal Export As Boolean = False, Optional ByVal PrintMode As PrintMode = PrintMode.PrintToPDF, Optional ByVal exportcolumns As Microsoft.VisualBasic.Collection = Nothing) As Boolean Implements IReport.Execute
            Me.DataSource = adataset
            Me.TableIndex = atableindex
            If Me.DataSource.Tables(atableindex).Rows.Count > 0 Then
                Me.RenderMode = PrintMode
                Me.ReportPath = Me.Render
                Me.SpreadSheetPath = ""
                If IsNothing(_exportColumns) = True Then
                    Return True
                End If
                'If Export = True And _exportColumns.Count > 0 Then
                '    Me.SpreadSheetPath = Common.Export.ExportToCSV(Me.DataSource.Tables(0), GetFileName(Me.ReportName), _exportColumns)
                'End If

                Return True
            End If
            Return False

        End Function
        Public Property ExportColumns() As Microsoft.VisualBasic.Collection Implements IReport.ExportColumns
            Get
                Return _exportColumns
            End Get
            Set(ByVal Value As Microsoft.VisualBasic.Collection)
                _exportColumns = Value
            End Set
        End Property

        Public Property RenderMode() As PrintMode Implements IReport.RenderMode
            Get
                Return _renderMode
            End Get
            Set(ByVal Value As PrintMode)
                _renderMode = Value
            End Set
        End Property

        Public Property ReportFooter() As Object Implements IReport.ReportFooter
            Get
                Return m_reportfooter
            End Get
            Set(ByVal Value As Object)
                m_reportfooter = Value
            End Set
        End Property

        Public Property ReportLogo() As System.Drawing.Image Implements IReport.ReportLogo
            Get
                Return m_reportlogo
            End Get
            Set(ByVal Value As System.Drawing.Image)
                m_reportlogo = Value
            End Set
        End Property

        Public Property ReportPrefix() As String Implements IReport.ReportPrefix
            Get
                Return _reportprefix
            End Get
            Set(ByVal Value As String)
                _reportprefix = Value
            End Set
        End Property
    End Class

End Namespace
