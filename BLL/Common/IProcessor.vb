Namespace Common
    Public Interface IProcessor
        Property UserId() As String
        Property UserName() As String
        Property SettingsFolder() As String
        Property OutputFolder() As String
        Function Execute() As Boolean

    End Interface
End Namespace
