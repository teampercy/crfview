﻿Imports System.Management
Imports System.Net.Mail
Imports System.Windows.Forms

Namespace CRFView

    Public Class CRFView
        Public Shared Function GetFilePath(ByRef auser As Users.CurrentUser, ByVal akeyvalue As String, ByVal afilename As String) As String
            Dim mydir As String = System.IO.Path.GetDirectoryName(GetKeyValue(akeyvalue))
            Try
                If System.IO.Directory.Exists(mydir) = False Then
                    System.IO.Directory.CreateDirectory(mydir)
                End If
            Catch
                mydir = System.IO.Path.GetDirectoryName(auser.OutputFolder)
            End Try


            Return mydir & "\" & TrimSpace(afilename)

        End Function
        Public Shared Function GetFileName(ByRef auser As Users.CurrentUser, ByVal akeyvalue As String, ByVal aprefix As String, Optional ByVal aext As String = ".CSV", Optional ByVal suppressusername As Boolean = False) As String
            Dim mydir As String = System.IO.Path.GetDirectoryName(GetKeyValue(akeyvalue))
            Try
                If System.IO.Directory.Exists(mydir) = False Then
                    System.IO.Directory.CreateDirectory(mydir)
                End If
            Catch
                mydir = auser.OutputFolder
            End Try

            Dim S As String
            If suppressusername = True Then
                S = aprefix
            Else
                S = auser.UserName & "-" & aprefix
            End If
            Return mydir & "\" & TrimSpace(S & "-" & Format(Now(), "yyyyMMddHHmmss") & aext)

        End Function
        Public Shared Function TrimSpace(ByRef strInput As String) As String
            ' This procedure trims extra space from any part of
            ' a string.
            Dim str As String = strInput.Trim
            Return Join(str.Split(" "), "")

        End Function
        Public Shared Function GetKeyValue(ByVal akey As String) As String
            Dim MYSPROC As New SPROCS.uspbo_System_GetValueforKey
            MYSPROC.Key = akey
            Dim MYVIEW As DAL.COMMON.TableView = DBO.GetTableView(MYSPROC)
            If MYVIEW.Count = 1 Then
                Return MYVIEW.RowItem("KEYVALUE")
            Else
                Return Nothing
            End If
        End Function
        Public Shared Sub SetKeyValue(ByVal akey As String, ByVal avalue As String)
            Dim MYSPROC As New SPROCS.uspbo_System_SetValueforKey
            MYSPROC.Key = akey
            MYSPROC.Value = avalue
            DBO.ExecNonQuery(MYSPROC)

        End Sub
        Public Shared Sub DeleteKeyValue(ByVal akey As String)
            Dim MYSPROC As New SPROCS.uspbo_System_DeleteKeyValue
            MYSPROC.Key = akey
            DBO.ExecNonQuery(MYSPROC)

        End Sub
        Public Shared Function SetDefaultPrinter(ByVal PrinterName As String) As String


            'Declare WMI Variables
            Dim MgmtObject As ManagementObject
            Dim MgmtCollection As ManagementObjectCollection
            Dim MgmtSearcher As ManagementObjectSearcher
            Dim ReturnBoolean As Boolean = False

            'Perform the search for printers and return the listing as a collection
            MgmtSearcher = New ManagementObjectSearcher("Select * from Win32_Printer")
            MgmtCollection = MgmtSearcher.Get

            'Enumerate Objects To Find Printer
            For Each MgmtObject In MgmtCollection
                'Look for a match
                If MgmtObject.Item("name").ToString = PrinterName Then
                    'Set Default Printer
                    Dim TempObject() As Object 'Temporary Object for InvokeMethod. Holds no purpose.
                    MgmtObject.InvokeMethod("SetDefaultPrinter", TempObject)

                    'Set Success Value and Exit For..Next Loop
                    ReturnBoolean = True
                    Exit For
                End If
            Next

            'Return Success Value
            Return ReturnBoolean


        End Function
        Public Shared Function SendOutLookEmail(ByVal fromAddress As String, ByVal fromName As String, ByVal Subject As String, ByVal Body As String, ByVal recipients As Collection, ByVal attachments As Collection, ByVal IsSend As Boolean) As Boolean
            Dim s As String
            Dim toe As String = String.Empty
            For Each s In recipients
                'add an attachment from the filesystem
                Dim ss As String() = Strings.Split(s, "~")
                If toe.Length > 1 Then
                    toe += ";" & ss(0)
                Else
                    toe = ss(0)
                End If
            Next

            Dim attach As String = String.Empty
            For Each s In attachments
                'add an attachment from the filesystem
                If attach.Length > 1 Then
                    attach += ";" & s
                Else
                    attach = s
                End If
            Next

            Dim Appl As Object
            Dim out As Object
            Try
                Appl = CreateObject("Outlook.Application")

                '  For Each s In recipients
                out = Appl.CreateItem(0)

                'add an attachment from the filesystem
                Dim ss As String() = Strings.Split(s, "~")

                With out
                    .Subject = Subject
                    .Body = Body
                    '==============================================
                    'to use html in the email instead of plain text
                    'use .HTMLBody instead of .body
                    '==============================================
                    Dim x As String
                    For Each x In attachments
                        .Attachments.add(x)
                    Next
                    'If attach <> "" Then
                    '    .Attachments.add(attach)
                    'End If
                    .To = toe

                    If IsSend = True Then
                        .send()
                    Else
                        .save()
                    End If
                End With
                'Next

            Catch ex As Exception
                MsgBox("Error " & Err.Number & ": " & ex.Message, MsgBoxStyle.Critical, "Error")
            End Try
        End Function
        Public Shared Function SendEmail(ByVal fromAddress As String, ByVal fromName As String, ByVal smtpserver As String, ByVal smtplogin As String, ByVal smtppassword As String, ByVal Subject As String, ByVal Body As String, ByVal recipients As Collection, ByVal attachments As Collection) As Boolean
            Dim s As String
            'Dim toe As String = String.Empty
            'For Each s In recipients
            '    'add an attachment from the filesystem
            '    Dim ss As String() = Strings.Split(s, "~")
            '    If toe.Length > 1 Then
            '        toe += ";" & ss(0)
            '    Else
            '        toe = ss(0)
            '    End If
            'Next

            'Dim attach As String = String.Empty
            ''For Each s In attachments
            '    'add an attachment from the filesystem
            '    If attach.Length > 1 Then
            '        attach += ";" & s
            '    Else
            '        attach = s
            '    End If
            'Next

            Dim msg As New MailMessage
            msg.From = New MailAddress(fromAddress, fromName)
            msg.Subject = Subject
            msg.Body = Body
            Dim sa As Object = Nothing
            Dim sr As Object = Nothing

            For Each sa In attachments
                msg.Attachments.Add(New Attachment(sa))
            Next

            For Each sr In recipients
                Dim ss As String() = Strings.Split(sr, ";")
                Dim toaddr As New System.Net.Mail.MailAddress(ss(0))
                msg.To.Add(toaddr)
            Next

            Try
                Dim _smtp As New SmtpClient(smtpserver)
                If IsNothing(smtplogin) = False And Len(smtplogin) > 5 Then
                    _smtp.Port = ""
                    _smtp.Credentials = New System.Net.NetworkCredential(smtplogin, smtppassword)
                    _smtp.EnableSsl = 0
                End If
                _smtp.Send(msg)
                _smtp.Dispose()

            Catch ex As Exception
                MsgBox("Error " & Err.Number & ": " & ex.Message, MsgBoxStyle.Critical, "Error")
            End Try
        End Function
        Public Shared Function GetCurrentUser() As CRF.BLL.Users.CurrentUser
            With System.Configuration.ConfigurationManager.AppSettings

                Dim myuser As New CRF.BLL.Users.CurrentUser
                myuser.Id = Globals.CurrentUser.Id
                myuser.UserName = Globals.CurrentUser.UserName
                myuser.Email = Globals.CurrentUser.Email
                myuser.OutputFolder = .Get("OUTPUT")
                myuser.SettingsFolder = .Get("SETTINGS")
                myuser.UploadFolder = .Get("UPLOAD")
                myuser.FAXPRINTER = .Get("FAXPRINTER")
                myuser.SiteDataFolder = .Get("SITEPATH")
                Return myuser
            End With

        End Function
        Public Shared Sub PrintPDF(ByVal AFILENAME As String)
            If System.IO.File.Exists(AFILENAME) = False Then Exit Sub

            With New Process
                .StartInfo.Verb = "print"
                .StartInfo.CreateNoWindow = False
                .StartInfo.FileName = AFILENAME
                .Start()
                .WaitForExit(10000)
                .CloseMainWindow()
                .Close()
            End With

        End Sub
        Public Shared Sub ViewPDF(ByVal AFILENAME As String)
            If System.IO.File.Exists(AFILENAME) = False Then Exit Sub

            Dim s As String = "explorer " & AFILENAME
            Shell(s, AppWinStyle.MaximizedFocus)


        End Sub




        Public Shared Function GetFormattedPhoneNumber(ByVal value As String) As String

            If String.IsNullOrEmpty(value) Then
                Return String.Empty
            End If

            value = New System.Text.RegularExpressions.Regex("\D").Replace(value, String.Empty)

            value = value.TrimStart("1")

            If value.Length = 7 Then
                Return Convert.ToInt64(value).ToString("###-####")
            End If
            If value.Length = 10 Then
                Return Convert.ToInt64(value).ToString("###-###-####")
            End If
            If value.Length > 10 Then
                Return Convert.ToInt64(value).ToString("###-###-#### " + New String("#", (value.Length - 10)))
            End If

            Return value

        End Function



        Public Shared Function SendSMTPEmail(ByVal sender As String, ByVal Subject As String, ByVal Body As String, ByVal recipient As String) As Boolean
            Dim myitem As New TABLES.Report_Settings
            DBO.Read(1, myitem)
            Try

                Dim msg As New MailMessage

                msg.To.Add(recipient)
                msg.From = New MailAddress(sender)
                msg.Subject = Subject
                msg.Body = Body


                With myitem
                    Dim _smtp As New SmtpClient(.smtpserver)
                    If IsNothing(.smtplogin) = False And Len(.smtplogin) > 5 Then
                        _smtp.Port = .smtpport
                        _smtp.Credentials = New System.Net.NetworkCredential(.smtplogin, .smtppassword)
                        _smtp.EnableSsl = .smtpenablessl
                    End If
                    _smtp.Send(msg)
                    _smtp.Dispose()
                End With

                Return True

            Catch ex As Exception

                Return False
            End Try

            Return True
        End Function



        Public Shared Function ValidStateZip(ByVal sState As String, ByVal sZipcode As String) As Boolean

            Dim sSql As String = "Select distinct StateAbbr From ZipCode with (nolock) Where ZipCode = '" & sZipcode & "'"
            Dim myview As DAL.COMMON.TableView = DBO.GetTableView(sSql)
            If myview.Count > 0 Then
                If Trim(sState) <> myview.RowItem("StateAbbr") Then
                    Return False
                Else
                    Return True
                End If
            Else
                Return False
            End If
        End Function


        Public Shared Function ValidCityZip(ByVal sCity As String, ByVal sZipcode As String) As Boolean
            Dim sSql As String = "Select dbo.fn_ValidCityZip('" & sZipcode & "', '" & sCity & "') as Valid"
            Dim myview As DAL.COMMON.TableView = DBO.GetTableView(sSql)
            Dim iValid As Boolean = myview.RowItem("Valid")

            Return iValid
        End Function
        Public Shared Function GetText(ByVal value As String) As String
            Dim test As New RichTextBox
            Dim s1 As String
            If (value.StartsWith("{\rtf")) Then

                test.Rtf = value
                s1 = LSet(test.Text, 200)
            Else
                s1 = LSet(value, 200)
            End If


            's1 = New String(test.Text, 200)
            's1 = test.Text(200)



            Return s1

        End Function
        Public Shared Function GetText1(ByVal value As String) As String
            Dim test As New RichTextBox
            Dim s1 As String
            If (value.StartsWith("{\rtf")) Then

                test.Rtf = value
                s1 = test.Text
            Else
                s1 = value
            End If


            's1 = New String(test.Text, 200)
            's1 = test.Text(200)



            Return s1

        End Function
        <STAThread()>
        Public Shared Function btnCopyToClipboard(ByVal JobAdd1 As String) As String

            Clipboard.SetText(JobAdd1)
            Return 0
        End Function

    End Class

End Namespace
