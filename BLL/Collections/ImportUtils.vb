﻿Namespace Collections.DataEntry.ImportEDI
    Public Class ImportUtils

        Public Shared Function ParseCSR312ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatch As TABLES.Batch, ByVal aline As String, ByRef aitem As TABLES.BatchDebtAccount) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 12 Or Len(sfields(0)) < 1 Then
                Return False
            End If

            With aitem
                .AccountNo = sfields(0)
                .DebtorName = sfields(1)
                .ContactName = sfields(3)
                .AddressLine1 = sfields(5)
                .AddressLine2 = sfields(6)
                .City = sfields(8)
                .State = sfields(9)
                .PostalCode = sfields(10)
                .PhoneNo = ImportUtils.GetPhoneNo(sfields(4))
                .TotAsgAmt = ImportUtils.GetDecimal(sfields(11))
                .LastServiceDate = ImportUtils.GetDate(sfields(15))
                .LastPaymentDate = ImportUtils.GetDate(sfields(17))
                .BatchType = "EDI"
            End With

            Return True

        End Function
        Public Shared Function ParseDISCUSImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatch As TABLES.Batch, ByVal aline As String, ByRef aitem As TABLES.BatchDebtAccount, ByRef aiteminvoice As TABLES.BatchDebtAccountInvoice) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 9 Or Len(sfields(0)) < 1 Then
                Return False
            End If

            With aitem
                .AccountNo = sfields(0)
                .DebtorName = sfields(1)
                .AddressLine1 = sfields(2)
                .City = sfields(3)
                .State = sfields(4)
                .PostalCode = sfields(5)
                .LastServiceDate = FormatDate(DateAdd(DateInterval.Day, -40, Now()))
                .BatchType = "EDI"
            End With

            With aiteminvoice
                .InvoiceNo = sfields(6)
                .InvoiceDate = ImportUtils.GetDate(sfields(7))
                .BalDue = ImportUtils.GetDecimal(sfields(8))
            End With

            Return True

        End Function
        Public Shared Function ParseUPT713ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatch As TABLES.Batch, ByVal aline As String, ByRef aitem As TABLES.BatchDebtAccount, ByRef aiteminvoice As TABLES.BatchDebtAccountInvoice) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 30 Or Len(sfields(0)) < 1 Then
                Return False
            End If

            With aitem
                .AccountNo = sfields(2)
                .DebtorName = sfields(3)
                .PhoneNo = ImportUtils.GetPhoneNo(sfields(6))
                .AddressLine1 = sfields(24)
                .City = sfields(25)
                .State = sfields(26)
                .PostalCode = sfields(27)
                .LastServiceDate = ImportUtils.GetDate(sfields(1))
                .BatchType = "EDI"
            End With

            With aiteminvoice
                .InvoiceNo = sfields(0)
                .InvoiceDate = ImportUtils.GetDate(sfields(1))
                .BalDue = ImportUtils.GetDecimal(sfields(13))
            End With

            Return True

        End Function
        Public Shared Function ParseSTP713ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatch As TABLES.Batch, ByVal aline As String, ByRef aitem As TABLES.BatchDebtAccount) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 16 Or Len(sfields(0)) < 1 Then
                Return False
            End If

            If Strings.Left(sfields(1).ToUpper, 4) = "CUST" Then
                aitem.AccountNo = "-1"
                Return True
            End If

            With aitem
                .AccountNo = sfields(0)
                .DebtorName = sfields(1)
                .TaxId = sfields(7)
                .LastServiceDate = ImportUtils.GetDate(sfields(3))
                .AddressLine1 = sfields(8)
                .AddressLine2 = sfields(9)
                .City = sfields(10)
                .State = sfields(11)
                .PostalCode = sfields(12)
                If Left(sfields(13), 1) <> "*" Then
                    .PhoneNo = ImportUtils.GetPhoneNo(sfields(13))
                End If

                .Cell = ImportUtils.GetPhoneNo(sfields(14))

                .Email = sfields(15)
                .TotAsgAmt = ImportUtils.GetDecimal(sfields(2))
                .BatchType = "EDI"
            End With

            Return True

        End Function
        Public Shared Function ParseAFP001ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatch As TABLES.Batch, ByVal aline As String, ByRef aitem As TABLES.BatchDebtAccount) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 11 Or Len(sfields(0)) < 1 Then
                Return False
            End If

            If Strings.Left(sfields(0).ToUpper, 4) = "CUST" Then
                aitem.AccountNo = "-1"
                Return True
            End If

            With aitem
                .AccountNo = sfields(0)
                .DebtorName = sfields(2)
                .ContactName = sfields(1)
                .AddressLine1 = sfields(9)
                .City = sfields(10)
                .State = "TX"
                .PostalCode = sfields(11)
                .PhoneNo = ImportUtils.GetPhoneNo(sfields(4))
                .TotAsgAmt = ImportUtils.GetDecimal(sfields(3))
                .LastServiceDate = DateAdd(DateInterval.Day, 14, ImportUtils.GetDate(sfields(6)))
                .AccountNote = sfields(7) & "-" & sfields(8)
                .IsEstAudit = False
                .BatchType = "EDI"


            End With

            Return True

        End Function

        Public Shared Function ParseDNI001ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatch As TABLES.Batch, ByVal aline As String, ByRef aitem As TABLES.BatchDebtAccount) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 21 Or Len(sfields(0)) < 1 Then
                Return False
            End If

            With aitem
                .AccountNo = sfields(0)
                .DebtorName = sfields(1)
                .ContactName = sfields(2)
                .AddressLine1 = sfields(4)
                .City = sfields(5)
                .State = sfields(6)
                .PostalCode = sfields(7)
                .PhoneNo = ImportUtils.GetPhoneNo(sfields(3))
                .TotAsgAmt = ImportUtils.GetDecimal(sfields(11))
                .LastServiceDate = ImportUtils.GetDate(sfields(10))
                .AccountNote = sfields(8) & "-" & sfields(9)
                .ContactName_2 = sfields(12)
                .AddressLine1_2 = sfields(13)
                .AddressLine2_2 = sfields(14)
                .City_2 = sfields(15)
                .State_2 = sfields(16)
                .PostalCode_2 = sfields(17)
                .PhoneNo_2 = ImportUtils.GetPhoneNo(sfields(18))
                .Email_2 = sfields(19)
                .IsEstAudit = False
                If sfields.Length > 21 Then
                    If sfields(21) = True Then
                        .IsEstAudit = True
                    End If
                End If

                .BatchType = "EDI"

            End With

            Return True

        End Function
        Public Shared Function ParsePLD818ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatch As TABLES.Batch, ByVal aline As String, ByRef aitem As TABLES.BatchDebtAccount) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 17 Then
                Return False
            End If

            With aitem
                .AccountNo = sfields(0)
                .ContactName = sfields(1)
                .DebtorName = sfields(2)
                .AddressLine1 = sfields(3)
                .City = sfields(4)
                .State = sfields(5)
                .PostalCode = sfields(6)
                .PhoneNo = ImportUtils.GetPhoneNo(sfields(7))
                If InStr(sfields(8), ";") > 1 Then
                    .PhoneNo2 = ImportUtils.GetPhoneNo(Mid(sfields(8), 1, InStr(sfields(8), ";") - 1))
                    .Cell = ImportUtils.GetPhoneNo(Mid(sfields(8), InStr(sfields(8), ";") + 1))
                Else
                    .PhoneNo2 = ImportUtils.GetPhoneNo(sfields(8))
                End If
                .Email = sfields(9)

                '.TotAsgAmt = ImportUtils.GetDecimal(sfields(10))
                .TotAsgAmt = sfields(10)
                If ImportUtils.GetDate(sfields(13)) = "#12:00:00 AM#" Then
                    .LastServiceDate = Nothing
                Else
                    .LastServiceDate = ImportUtils.GetDate(sfields(13))
                End If

                ' 11/16/15 Deleted the Note field (column 14)
                .AddressLine1_2 = sfields(14)
                .City_2 = sfields(15)
                .State_2 = sfields(16)
                .PostalCode_2 = sfields(17)

                .BatchType = "EDI"
            End With

            Return True

        End Function

        Public Shared Function ParseStandardImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatch As TABLES.Batch, ByVal aline As String, ByRef aitem As TABLES.BatchDebtAccount) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 16 Or Len(sfields(0)) < 1 Then
                Return False
            End If

            With aitem
                .AccountNo = sfields(0)
                .ContactName = sfields(1)
                .DebtorName = sfields(2)
                .AddressLine1 = sfields(9)
                '.AddressLine2 = sfields(4)
                .City = sfields(10)
                '.State = sfields(6)
                .State = "TX"
                .PostalCode = sfields(11)
                .PhoneNo = ImportUtils.GetPhoneNo(sfields(4))
                '.Fax = ImportUtils.GetPhoneNo(sfields(4))
                .Email = sfields(12)
                .TotAsgAmt = ImportUtils.GetDecimal(sfields(3))
                .AccountNote = sfields(7) + "-" + sfields(8)
                '.BranchNum = sfields(13)
                .LastServiceDate = ImportUtils.GetDate(sfields(5))
                .LastPaymentDate = ImportUtils.GetDate(sfields(6))
                .BatchType = "EDI"
            End With

            Return True

        End Function

        Public Shared Function ParseSPM972ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatch As TABLES.Batch, ByVal aline As String, ByRef aitem As TABLES.BatchDebtAccount) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 61 Or Len(sfields(4)) < 1 Then
                Return False
            End If

            'If Len(sfields(1)) < 2 Then
            '    Return False
            'End If

            With aitem
                .AccountNo = sfields(4)
                If InStr(sfields(5), ";") > 0 Then
                    .DebtorName = Replace(sfields(5), ";", "")
                Else
                    .DebtorName = sfields(5)
                End If

                If Len(sfields(7)) > 0 And Len(sfields(12)) > 0 Then
                    .AddressLine1 = sfields(8) & " " & sfields(7) & " " & sfields(12)
                ElseIf Len(sfields(7)) = 0 And Len(sfields(12)) > 0 Then
                    .AddressLine1 = sfields(8) & " " & sfields(12)
                ElseIf Len(sfields(7)) > 0 And Len(sfields(12)) = 0 Then
                    .AddressLine1 = sfields(8) & " " & sfields(7) & " " & sfields(17)
                ElseIf Len(sfields(7)) = 0 And Len(sfields(12)) = 0 Then
                    .AddressLine1 = sfields(8) & " " & sfields(17)
                End If

                .City = sfields(10)
                .State = sfields(11)
                If Len(sfields(15)) = 4 Then
                    .PostalCode = "0" & sfields(15) & "-" & sfields(16)
                Else
                    .PostalCode = sfields(15) & "-" & sfields(16)
                End If
                .PhoneNo = ImportUtils.GetPhoneNo(sfields(60))
                .Cell = ImportUtils.GetPhoneNo(sfields(58))

                If Len(sfields(39)) > 1 Then
                    .LastPaymentDate = ImportUtils.GetDate(sfields(39))
                End If

                Dim dAge As Double
                dAge = sfields(38) * -1
                .LastServiceDate = DateAdd(DateInterval.Day, dAge, ImportUtils.GetDate(sfields(0)))
                .TotAsgAmt = ImportUtils.GetDecimal(sfields(41))
                .AccountNote = sfields(2)
                .BatchType = "EDI"
            End With

            Return True

        End Function

        Public Shared Function ParseGEX713ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatch As TABLES.Batch, ByVal aline As String, ByRef aitem As TABLES.BatchDebtAccount) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 51 Or Len(sfields(0)) < 1 Then
                Return False
            End If

            With aitem
                .ClientGroupNum = sfields(0)
                .AccountNo = sfields(12)
                .DebtorName = sfields(1)
                .ContactName = Trim(sfields(41))
                .AddressLine1 = sfields(23)
                .AddressLine2 = Trim(sfields(24))
                .City = sfields(25)
                .State = sfields(26)
                .Email = sfields(51)

                If Len(sfields(28)) > 0 Then
                    .PostalCode = sfields(27) & sfields(28)
                Else
                    .PostalCode = sfields(27)
                End If

                .TaxId = sfields(37)
                .PhoneNo = ImportUtils.GetPhoneNo(sfields(40))

                If Len(sfields(44)) > 0 Then
                    .LastServiceDate = ImportUtils.GetDate(sfields(44))
                Else
                    .LastServiceDate = ImportUtils.GetDate(sfields(36))
                End If

                .TotAsgAmt = ImportUtils.GetDecimal(sfields(11))
                .UsageFee = ImportUtils.GetDecimal(sfields(18))
                .EarlyTerminationFee = ImportUtils.GetDecimal(sfields(15))
                .ServiceState = sfields(32)

                .BatchType = "EDI"
            End With

            Return True

        End Function

        Public Shared Function ParseGEX714ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatch As TABLES.Batch, ByVal aline As String, ByRef aitem As TABLES.BatchDebtAccount) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 35 Or Len(sfields(0)) < 1 Then
                Return False
            End If

            With aitem
                .AccountNo = sfields(2)
                .DebtorName = sfields(1)
                '.ContactName = sfields(2) & " " & sfields(3)
                .AddressLine1 = sfields(27)
                .AddressLine2 = sfields(28)
                .City = sfields(29)
                .State = sfields(30)
                .PostalCode = sfields(31)
                .Email = sfields(35)
                .PhoneNo = ImportUtils.GetPhoneNo(sfields(33) & sfields(34))

                .LastServiceDate = ImportUtils.GetDate(sfields(10))

                '.TotAsgAmt = ImportUtils.GetDecimal(sfields(13))
                '.UsageFee = ImportUtils.GetDecimal(sfields(14))
                '.EarlyTerminationFee = ImportUtils.GetDecimal(sfields(19))
                ' 7/12/2016
                .TotAsgAmt = sfields(13)
                If sfields(14) <> " $-   " Then
                    .UsageFee = sfields(14)
                End If

                If sfields(19) <> " $-   " Then
                    .EarlyTerminationFee = sfields(19)
                End If

                .ServiceState = sfields(25)

                If Len(sfields(21)) > 0 Then
                    .Note = sfields(21)
                End If

                .BatchType = "EDI"
            End With

            Return True

        End Function
        Public Shared Function ParseLANGImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatch As TABLES.Batch, ByVal aline As String, ByRef aitem As TABLES.BatchDebtAccount) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 20 Or Len(sfields(0)) < 1 Then
                Return False
            End If

            With aitem
                .AccountNo = sfields(0)
                .DebtorName = sfields(1)
                .ContactName = sfields(2)
                .AddressLine1 = sfields(3)
                .City = sfields(4)
                .State = sfields(5)
                .PostalCode = sfields(6)
                .PhoneNo = ImportUtils.GetPhoneNo(sfields(7))

                .TotAsgAmt = ImportUtils.GetDecimal(sfields(8))
                .LastServiceDate = ImportUtils.GetDate(sfields(18))
                .ClientId = GetClientId(sfields(19))
                .IsTenDayContactPlan = True
                .BatchType = "EDI"
            End With

            Return True

        End Function
        Public Shared Function ParseSparksImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatch As TABLES.Batch, ByVal aline As String, ByRef aitem As TABLES.BatchDebtAccount) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 25 Or Len(sfields(0)) < 1 Then
                Return False
            End If

            With aitem
                .AccountNo = sfields(0)
                .DebtorName = sfields(3)
                .ContactName = sfields(4)
                .AddressLine1 = sfields(5)
                .City = sfields(7)
                .State = sfields(8)
                .PostalCode = sfields(9)
                .PhoneNo = ImportUtils.GetPhoneNo(sfields(10))
                .Cell = ImportUtils.GetPhoneNo(sfields(11))
                .TotAsgAmt = ImportUtils.GetDecimal(sfields(19))
                .LastServiceDate = ImportUtils.GetDate(sfields(17))
                .BatchType = "EDI"

            End With

            Return True

        End Function
        Public Shared Function ParseNIG002ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatch As TABLES.Batch, ByVal aline As String, ByRef aitem As TABLES.BatchDebtAccount) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 15 Then
                Return False
            End If

            If Len(Trim(sfields(0))) = 0 Then
                Return False
            End If

            With aitem
                .AccountNo = sfields(0)
                .TaxId = sfields(1)
                .DebtorName = sfields(2)
                .PhoneNo = ImportUtils.GetPhoneNo(sfields(3))
                .AddressLine1 = sfields(4)
                .City = sfields(5)
                .State = sfields(6)
                Dim ivalue As Integer
                ivalue = CInt(sfields(7))

                .PostalCode = String.Format("{0:00000}", ivalue)

                .TotAsgAmt = ImportUtils.GetDecimal(sfields(12))
                .LastServiceDate = ImportUtils.GetDate(sfields(9))
                .AccountNote = "ClaimantLossDate: " & ImportUtils.GetDate(sfields(8)) & "~Name: " & Trim(sfields(10)) & "~Note: " & Trim(sfields(11))
                .BatchType = "EDI"

            End With

            Return True

        End Function
        Public Shared Function ParseABS858ImportLine(ByVal auser As Users.CurrentUser, ByVal aclientid As String, ByVal abatch As TABLES.Batch, ByVal aline As String, ByRef aitem As TABLES.BatchDebtAccount) As Boolean
            Dim sfields As String() = ImportUtils.CSVParser(aline, ",")
            If sfields.Length < 12 Or Len(sfields(0)) < 1 Then
                Return False
            End If

            With aitem
                '.ClientId = .ClientId = GetClientId(sfields(0))

                .AccountNo = sfields(1)
                .BranchNum = sfields(2)
                .DebtorName = sfields(3)
                .AccountNote = "DBA: " & sfields(4)
                .AddressLine1 = sfields(5)

                .City = sfields(6)
                .State = sfields(7)
                .PostalCode = sfields(8)
                .LastServiceDate = ImportUtils.GetDate(sfields(9))
                .TotAsgAmt = ImportUtils.GetDecimal(sfields(10))
                .PhoneNo = ImportUtils.GetPhoneNo(sfields(11))

                .BatchType = "EDI"
            End With

            Return True

        End Function
        Public Shared Function GetClientId(ByVal aClientCode As String) As Integer
            Dim sSql As String = "Select ClientId From Client with (nolock) Where ClientCode = '" & aClientCode & "'"
            Dim myview As DAL.COMMON.TableView = DBO.GetTableView(sSql)
            If myview.Count > 0 Then
                Return myview.RowItem("ClientId")
            Else
                Return 0
            End If
        End Function

        Public Shared Function GetExportFile(ByVal myview As DAL.COMMON.TableView)
            Dim myspreadsheetpath As String = BLL.COMMON.FileOps.GetFileName(Globals.GetCurrentUser, "AccounsSubmitted")
            myview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.ServiceCode)
            myview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.AccountNo)
            myview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.DebtorName)
            myview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.ContactName)
            myview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.AddressLine1)
            myview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.AddressLine2)
            myview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.City)
            myview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.State)
            myview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.PostalCode)
            myview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.PhoneNo)
            myview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.Fax)
            myview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.Email)
            myview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.ReferalDate)
            myview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.LastServiceDate)
            myview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.LastPaymentDate)
            myview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.TotAsgAmt)
            myview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.AccountNote)
            myview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.BatchId)
            myview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.BatchDebtAccountId)
            myview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.SubmittedByUserId)
            myview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.ClientAgentCode)
            myview.ExportColumns.Add(vwBatchDebtAccount.ColumnNames.ServiceType)
            If myview.ExportToCSV(myspreadsheetpath) = True Then
                Return myspreadsheetpath
            Else
                Return Nothing
            End If

        End Function
        Public Shared Function GetBoolean(ByVal aflag As Object) As Boolean
            Try
                Return Boolean.Parse(aflag)
            Catch ex As Exception
                Return False
            End Try
        End Function
        Public Shared Function GetDecimal(ByVal aamt As Object) As Decimal
            Try
                Return HDS.WINLIB.Common.UnFormat.Amount(aamt)
            Catch ex As Exception
                Return 0
            End Try
        End Function
        Public Shared Function GetPhoneNo(ByVal avalue As Object) As String
            Try
                Return HDS.WINLIB.Common.UnFormat.PhoneNo(avalue)
            Catch ex As Exception
                Return ""
            End Try
        End Function
        Public Shared Function GetDate(ByVal adate As String) As Date
            Try
                If IsValidDate(adate) Then
                    If DatePart(DateInterval.Year, Date.Parse(adate)) < 1000 Then
                        Return Date.MinValue
                    Else
                        Return Date.Parse(adate)
                    End If
                Else
                    Return Date.MinValue
                End If

            Catch ex As Exception
                Return Date.MinValue
            End Try
        End Function

        Private Shared Function IsValidDate(ByVal adate As String) As Boolean
            Dim dt As Date = Date.MinValue
            Date.TryParse(adate, dt)
            If dt > Date.MinValue Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Shared Function FormatPhoneNo(ByVal avalue As Object) As String
            Try
                Return HDS.WINLIB.Common.Format.PhoneNo(avalue)
            Catch ex As Exception
                Return ""
            End Try
        End Function
        Public Shared Function FormatDate(ByVal adate As Object) As String

            Try
                If IsDate(adate) And adate <> Date.MinValue Then
                    Dim s As String() = Strings.Split(Date.Parse(adate).ToShortDateString, "/")
                    Dim y As String = Right("00" & s(0), 2) & "/" & Right("00" & s(1), 2) & "/" & Right("00" & s(2), 2)
                    Return y
                Else
                    Return String.Empty
                End If
            Catch ex As Exception
                Return String.Empty
            End Try
        End Function
        Public Shared Function FormatDecimal(ByVal aamt As Object) As String

            Try
                Return Strings.FormatCurrency(Decimal.Parse(aamt), 2)
            Catch ex As Exception
                Return Strings.FormatCurrency(Decimal.Parse(0), 2)
            End Try
        End Function
        Public Shared Function CSVParser(ByVal aline As String, ByVal adelimiter As String) As String()
            Dim fields As Array = Nothing
            Dim rawFields() As String = aline.Split(Convert.ToChar(","))
            ' recombine any with quotes
            RecombineQuotedFields(rawFields, adelimiter)
            ' remove the extra elements
            ExtractNullArrayElements(rawFields, fields)
            Return fields

        End Function
        Private Shared Sub RecombineQuotedFields(ByRef fields() As String, ByVal adelimiter As String)
            Dim _quotechar As String = Strings.Chr(34)
            Dim _delimiter As String = adelimiter
            Dim firstChar As Char
            Dim lastChar As Char
            Dim maxFieldIndex As Int32 = fields.GetLength(0) - 1
            ' start seaching
            For x As Int32 = 0 To maxFieldIndex
                ' get the potential delimitters
                If fields(x).Length > 0 Then
                    firstChar = fields(x).Chars(0)
                    lastChar = fields(x).Chars(fields(x).Length - 1)
                Else
                    firstChar = Nothing
                    lastChar = Nothing
                End If
                ' start the comparisons
                If firstChar = _quotechar Then
                    ' we started with a valid quote character
                    If (firstChar = lastChar) Then
                        ' strip off the matched quotes
                        fields.SetValue(fields(x).Substring(1, fields(x).Length - 2), x)
                    Else
                        Dim startIndex As Int32 = x
                        Dim quoteChar As Char = firstChar
                        Do
                            ' skip to the next item
                            x += 1
                            ' get the new "endpoints"
                            firstChar = fields(x).Chars(0)
                            lastChar = fields(x).Chars(fields(x).Length - 1)
                            ' this field better not start with a quote
                            If firstChar = _quotechar And fields(x).Length > 1 Then
                                Throw New ApplicationException("There was an unclosed quotation mark.")
                            End If
                            ' recombine the items
                            fields.SetValue(String.Concat(fields(startIndex).ToString, _delimiter, fields(x).ToString), startIndex)
                            ' flush the unused array element
                            Array.Clear(fields, x, 1)
                        Loop Until (lastChar = quoteChar)
                        ' strip off the outer quotes
                        fields.SetValue(fields(startIndex).Substring(1, fields(startIndex).Length - 2), startIndex)
                    End If
                End If
            Next
        End Sub
        Private Shared Sub ExtractNullArrayElements(ByRef input() As String, ByRef output As Array)
            Dim x As Int32
            Dim maxInputIndex As Int32 = input.Length - 1
            Dim count As Int32 = 0
            Dim mark As Int32 = 0
            ' get the actual field count
            For x = 0 To maxInputIndex
                If Not input(x) Is Nothing Then count += 1
            Next
            ' resize the output array
            output = Array.CreateInstance(GetType(String), count)
            For x = 0 To maxInputIndex
                If Not (input(x) Is Nothing) Then
                    ' save the value and incriment the book mark
                    output.SetValue(input(x), mark)
                    mark += 1
                End If
            Next
        End Sub

    End Class
End Namespace

