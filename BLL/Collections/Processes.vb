Namespace Collections
    Public Class Processes
        Public Shared Function GetClientList(ByVal auser As Users.CurrentUser) As DAL.Common.TableView
            Dim sql As String = "Select ClientId As ClientId,  ClientCode, ClientCode + '-' + ClientName As ClientName from Client Where IsCollectionClient = 1 and IsBranch = 0"
            Return DBO.GetTableView(sql)
        End Function
        Public Shared Function GetAccountInfo(ByVal auser As Users.CurrentUser, ByVal aitemId As Integer) As Collections.AccountInfo
            Return New Collections.AccountInfo(auser, aitemId)
        End Function
        Public Shared Function GetAccountMaster(ByRef auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal asproc As SPROCS.uspbo_Reports_GetAccountList, ByVal printmode As Common.PrintMode, ByVal spreadsheet As Boolean) As Integer
            With asproc
            End With
            Dim MYVIEW As DAL.COMMON.TableView = DBO.GetTableView(asproc)
            akeyvals.Add("RECORDCOUNT", MYVIEW.Count)
            If MYVIEW.Count > 0 Then
                Collections.Reports.AccountsMaster(auser, akeyvals, MYVIEW, printmode, spreadsheet)
            End If
            Return MYVIEW.Count

        End Function
        Public Shared Function GetAccountsClosed(ByRef auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal asproc As SPROCS.uspbo_Reports_GetAccountList, ByVal printmode As Common.PrintMode, ByVal spreadsheet As Boolean) As Integer
            With asproc
            End With
            Dim MYVIEW As DAL.COMMON.TableView = DBO.GetTableView(asproc)
            akeyvals.Add("RECORDCOUNT", MYVIEW.Count)
            If MYVIEW.Count > 0 Then
                Collections.Reports.AccountsClosed(auser, akeyvals, MYVIEW, printmode, spreadsheet)
            End If
            Return MYVIEW.Count

        End Function
        Public Shared Function GetPaymentList(ByRef auser As Users.CurrentUser, ByRef akeyvals As DAL.COMMON.KeyValues, ByVal asproc As SPROCS.uspbo_Reports_GetPaymentList, ByVal printmode As Common.PrintMode, ByVal spreadsheet As Boolean) As Integer
            With asproc
            End With
            Dim MYVIEW As DAL.COMMON.TableView = DBO.GetTableView(asproc)
            akeyvals.Add("RECORDCOUNT", MYVIEW.Count)
            If MYVIEW.Count > 0 Then
                Collections.Reports.AccountsPayment(auser, akeyvals, MYVIEW, printmode, spreadsheet)
            End If
            Return MYVIEW.Count

        End Function
        Public Shared Function GetReportList(ByVal auser As Users.CurrentUser, ByVal areportcategory As String) As HDS.DAL.COMMON.TableView
            Dim sql As String = "Select ReportName, Description,  PrintFrom, ReportCategory, ReportFilename, Reporttype from vwCollectionReportsList Where ReportCategory = '" & areportcategory.ToString & "' And PrintFrom in ('B', 'R') Order By Description"
            Return DBO.GetTableView(sql)
        End Function
    End Class
End Namespace
