USE [QACRFDB]
GO
/****** Object:  View [dbo].[vwAccountFilterList]    Script Date: 5/16/2019 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

drop view vwAccountFilterList
Go
CREATE VIEW [dbo].[vwAccountFilterList]
AS
SELECT        dbo.DebtAccount.DebtAccountId, dbo.Client.ClientCode, dbo.DebtAccount.ClientId, dbo.DebtAccount.LocationId, dbo.DebtAccount.CollectionStatusId, dbo.DebtAccount.Location, dbo.DebtAccount.CollectionStatus, 
                         dbo.DebtAccount.TempOwnerId, dbo.DebtAccount.TempOwner, dbo.DebtAccount.Priority, dbo.DebtAccount.AccountNo, dbo.DebtAccount.ReferalDate, dbo.DebtAccount.NextContactDate, 
                         dbo.DebtAccount.NextStatusDate, dbo.DebtAccount.CloseDate, dbo.DebtAccount.TotBalAmt, dbo.DebtAddress.IsMainAddress, dbo.DebtAddress.ContactName, dbo.DebtAddress.AddressLine1, 
                         dbo.DebtAddress.City, dbo.DebtAddress.State, dbo.DebtAddress.PostalCode, dbo.DebtAddress.PhoneNo, dbo.DebtAddress.Fax, dbo.DebtAccountLegalInfo.AttyId, dbo.DebtAccountSmallClaims.TrialDate, 
                         dbo.DebtAccountSmallClaims.JudgmentDate, dbo.DebtAccountSmallClaims.CourtId, dbo.DebtAccountSmallClaims.ProcessServerId, dbo.DebtAccountSmallClaims.CaseNum, 
                         dbo.DebtAccountSmallClaims.SCReviewDate, dbo.DebtAccountMisc.DebtorBKName, dbo.DebtAccountMisc.BKCaseNo, dbo.DebtAccountSmallClaims.EntityName1, dbo.Debt.DebtorName, dbo.Debt.TaxId, 
                         dbo.DebtAccountLegalInfo.FwdJudgmentDate, dbo.DebtAccount.DebtId, dbo.DebtAddress.SSN, dbo.Debt.MatchingKey, dbo.DebtAddress.AddressKey, dbo.DebtAccount.BranchNum, dbo.DebtAccount.FileNumber, 
                         dbo.CollectionStatusGroup.StatusGroupDescription, dbo.CollectionStatusGroup.Id AS StatusGroupId, dbo.DebtAccountLegalInfo.AttyRef, dbo.DebtAccountLegalInfo.CourtMemo, 
                         dbo.DebtAccountLegalInfo.FwdCourtDate,
						 dbo.DebtAccount.LastPaymentDate,
						 dbo.DebtAccount.TotAsgAmt,
						 dbo.DebtAccount.TotCollFeeAmt,
						 dbo.DebtAccount.TotPmtAmt,
						 dbo.DebtAccount.TotIntAmt
FROM            dbo.CollectionStatusGroup RIGHT OUTER JOIN
                         dbo.CollectionStatus ON dbo.CollectionStatusGroup.Id = dbo.CollectionStatus.StatusGroup RIGHT OUTER JOIN
                         dbo.Client INNER JOIN
                         dbo.DebtAccount ON dbo.Client.ClientId = dbo.DebtAccount.ClientId INNER JOIN
                         dbo.Debt ON dbo.DebtAccount.DebtId = dbo.Debt.DebtId INNER JOIN
                         dbo.DebtAddress ON dbo.Debt.DebtId = dbo.DebtAddress.DebtId ON dbo.CollectionStatus.CollectionStatusId = dbo.DebtAccount.CollectionStatusId LEFT OUTER JOIN
                         dbo.DebtAccountLegalInfo ON dbo.DebtAccount.DebtAccountId = dbo.DebtAccountLegalInfo.DebtAccountId LEFT OUTER JOIN
                         dbo.DebtAccountMisc ON dbo.DebtAccount.DebtAccountId = dbo.DebtAccountMisc.DebtAccountId LEFT OUTER JOIN
                         dbo.DebtAccountSmallClaims ON dbo.DebtAccount.DebtAccountId = dbo.DebtAccountSmallClaims.DebtAccountId
WHERE        (dbo.DebtAddress.IsMainAddress = 1)

GO
/****** Object:  StoredProcedure [dbo].[cview_Accounts_GetAccountFilterList]    Script Date: 5/16/2019 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER OFF
GO


drop proc cview_Accounts_GetAccountFilterList
Go
CREATE PROCEDURE [dbo].[cview_Accounts_GetAccountFilterList] 
(
	@SERVICECODE          	VARCHAR(20)=NULL,
	@DebtorName 	      	VARCHAR(50)=NULL,
	@DebtAccountId 	      	VARCHAR(50)=NULL,
	@FileNumber 	      	VARCHAR(50)=NULL,
	@ADDRESSLINE1	      	VARCHAR(30)= NULL,
	@CITY		      		VARCHAR(30)= NULL,
	@STATE		      		VARCHAR(2)=NULL ,
	@PHONENO	      		VARCHAR(20)=NULL,
	@FAX	      			VARCHAR(20)=NULL,
	@TaxId	      			VARCHAR(20)=NULL,
	@ACCOUNTNO            	VARCHAR(20)=NULL,
	@CollectionStatusId     INT = 0,
	@LocationId     	    INT = 0,
	@TempDeskId     	    INT = 0,
	@Priority     	      	VARCHAR(10)=NULL,
	@StatusGroup			INT = 0,
	@NextContactDate		varchar(20) = NULL,
	@NextStatusDate			varchar(20) = NULL,
	@ReferalDate			varchar(20) = NULL,
	@SCReviewDate			varchar(20) = NULL,
	@TrialDate				varchar(20) = NULL,
	@BalAmt					VARCHAR(50)=NULL,
	@AttyId					INT = 0,
	@CourtId				INT = 0,	
	@ProcessServerId		INT = 0,
	@CaseNum           		VARCHAR(50)=NULL,
	@EntityName        		VARCHAR(50)=NULL,
	@ContactName			VARCHAR(50)=NULL,	
	@BKCaseNo			    VARCHAR(50)=NULL,
	@DebtorBKName			VARCHAR(50)=NULL,	
	@SCJudgmentDate			varchar(20) = NULL,		
	@FWDJudgmentDate		varchar(20) = NULL,	
	@IsFetchByAddr			INT = 0,
	@StatusGroupId			INT = 0	,	
	@RETURNVALUE			INT = NULL OUT,

	--- New Parameters
	@NextContactDateFrom		varchar(20) = NULL,
	@NextContactDateTo		varchar(20) = NULL,
	@BalAmtFrom					VARCHAR(50)=NULL,
	@BalAmtTo					VARCHAR(50)=NULL,
	@ReferalDateFrom			varchar(20) = NULL,
	@ReferalDateTo			varchar(20) = NULL,
	@SCReviewDateFrom			varchar(20) = NULL,
	@SCReviewDateTo			varchar(20) = NULL,
	@TrialDateFrom				varchar(20) = NULL,
	@TrialDateTo				varchar(20) = NULL,
	@SCJudgmentDateFrom			varchar(20) = NULL,	
	@SCJudgmentDateTo			varchar(20) = NULL,
	@FWDJudgmentDateFrom		varchar(20) = NULL,
	@FWDJudgmentDateTo		varchar(20) = NULL,
	@FWDCourtDateTo			varchar(20)=NULL,
	@FWDCourtDateFrom			varchar(20)=NULL,
	@CourtMemo					varchar(50)=NULL,
	@AttorneyRefNumber			varchar(20)=NULL

	 ,@SortColType VARCHAR(50)='DebtAccountId DESC'     
		
)

as

--DECLARE @CLIENTID AS INTEGER
--SELECT @CLIENTID = Clientid from client (NOLOCK) 
--where clientcode = @SERVICECODE and IsBranch = 0

DECLARE @MINDebtAccountID AS INTEGER
SELECT @MINDebtAccountID = MAX(DebtAccountID) FROM DebtAccount
set @MINDebtAccountID = @MINDebtAccountID - 200
print @MINDebtAccountID

IF @DebtAccountId = 0
begin
SET @DebtAccountId = NUll
end

If @IsFetchByAddr = 0
Begin
SELECT
DebtAccountId,
VW.CLIENTCODE,
DebtorName,
AccountNo,
AddressLine1,
City,
State,
PostalCode,
PhoneNo,
--FileNumber,
ReferalDate,
CollectionStatus,
Location,
NextContactDate,
Priority,
TotBalAmt,
Fax,
SSN,
CloseDate,
NextStatusDate,
SCReviewDate,
TrialDate,
CaseNum,
EntityName1,
ContactName,
TempOwner,
BKCaseNo,
DebtorBKName,
JudgmentDate,
FWDJudgmentDate,
StatusGroupDescription,
TempOwnerId,
LastPaymentDate,
TotAsgAmt,
TotCollFeeAmt,
TotPmtAmt,
FwdCourtDate,
CollectionStatusId,
TotIntAmt

FROM  vwAccountFilterList vw (nolock)
where
( ( vw.DebtAccountId > @MinDebtAccountId and @DebtAccountId = -1) 
	or ( vw.DebtAccountId =  @DebtAccountId ) OR @DebtAccountId IS NULL )
AND ( vw.ClientCode Like @SERVICECODE OR @ServiceCode IS NULL)
AND ( vw.AccountNo Like  @AccountNo OR @AccountNo IS NULL)
AND ( vw.DebtorName Like @DebtorName  OR @DebtorName IS NULL)
--AND ( vw.ContactName Like @ContactName  OR @ContactName IS NULL)
--AND ( vw.AddressLine1 Like @AddressLine1  OR @AddressLine1 IS NULL)
--AND ( vw.State = @State  OR @State IS NULL)
--AND ( vw.PhoneNo Like  @PhoneNo  OR @PhoneNo IS NULL)
--AND ( vw.Fax Like  @Fax  OR @Fax IS NULL)
--AND ( vw.SSN Like  @TaxId  OR @TaxId IS NULL)
AND ( vw.Priority = @Priority OR @Priority IS NULL)
AND ( vw.EntityName1 Like @EntityName OR @EntityName IS NULL)
AND ( vw.CaseNum Like @CaseNum OR @CaseNum IS NULL)
AND ( vw.BKCaseNo Like @BKCaseNo OR @BKCaseNo IS NULL)
AND ( vw.DebtorBKName Like @DebtorBKName OR @DebtorBKName IS NULL)
AND ( vw.AttyId = @AttyId OR @AttyId = 0)
AND ( vw.CourtId = @CourtId OR @CourtId = 0)
AND ( vw.ProcessServerId = @ProcessServerId OR @ProcessServerId = 0)
AND ( (VW.[NextContactDate] BETWEEN @NextContactDateFrom AND @NextContactDateTo) OR (@NextContactDateFrom IS NULL OR @NextContactDateTo IS NULL))
AND ( VW.[NextStatusDate] <= @NextStatusDate OR @NextStatusDate IS NULL)
AND ( (VW.[ReferalDate] BETWEEN @ReferalDateFrom AND @ReferalDateTo) OR (@ReferalDateFrom IS NULL OR @ReferalDateTo IS NULL))
AND ( (VW.[SCReviewDate] BETWEEN @SCReviewDateFrom AND @SCReviewDateTo) OR (@SCReviewDateFrom IS NULL OR @SCReviewDateTo IS NULL))
AND ( (VW.[TrialDate] BETWEEN @TrialDateFrom AND @TrialDateTo ) OR (@TrialDateFrom IS NULL OR @TrialDateTo IS NULL))
AND ( (vw.TotBalAmt BETWEEN @BalAmtFrom AND @BalAmtTo) OR (@BalAmtFrom IS NULL OR @BalAmtTo IS NULL))
AND ( vw.LocationId = @LocationId OR @LocationId = 0)
AND ( vw.CollectionStatusId = @CollectionStatusId OR @CollectionStatusId = 0)
AND ( vw.StatusGroupId = @StatusGroupId OR @StatusGroupId = 0)
AND ( vw.TempOwnerId = @TempDeskId OR @TempDeskId = 0)
AND ( (VW.[JudgmentDate] BETWEEN @SCJudgmentDateFrom AND @SCJudgmentDateTo) OR (@SCJudgmentDate IS NULL OR @SCJudgmentDate IS NULL))
AND ( (VW.[FWDJudgmentDate] BETWEEN @FWDJudgmentDateFrom AND @FWDJudgmentDateTo ) OR (@FWDJudgmentDateFrom IS NULL OR @FWDJudgmentDateTo IS NULL))
AND  ((VW.[FWDCourtDate] BETWEEN @FWDCourtDateFrom AND @FWDCourtDateTo ) OR (@FWDCourtDateFrom IS NULL OR @FWDCourtDateTo IS NULL))
AND ( vw.CourtMemo Like @CourtMemo OR @CourtMemo IS NULL)
AND ( vw.AttyRef Like @AttorneyRefNumber OR @AttorneyRefNumber IS NULL)
--ORDER BY VW.DebtAccountId DESC
ORDER BY
 CASE WHEN @SortColType = 'DebtAccountId ASC' THEN  DebtAccountId END,                      
 CASE WHEN @SortColType = 'DebtAccountId DESC' THEN  DebtAccountId END DESC,                      
 
 CASE WHEN @SortColType = 'DebtorName ASC' THEN  DebtorName END,                      
 CASE WHEN @SortColType = 'DebtorName DESC' THEN DebtorName END DESC ,                
  
 CASE WHEN @SortColType = 'Contact ASC' THEN ContactName END,                      
 CASE WHEN @SortColType = 'Contact DESC' THEN ContactName END DESC ,   

 CASE WHEN @SortColType = 'DebtorAddress ASC' THEN AddressLine1 END,                      
 CASE WHEN @SortColType = 'DebtorAddress DESC' THEN AddressLine1 END DESC ,  
  
 CASE WHEN @SortColType = 'DebtorRef ASC' THEN AccountNo END,                      
 CASE WHEN @SortColType = 'DebtorRef DESC' THEN AccountNo END DESC ,  

 CASE WHEN @SortColType = 'StatusCode ASC' THEN CollectionStatusId END,                      
 CASE WHEN @SortColType = 'StatusCode DESC' THEN CollectionStatusId END DESC ,  

 CASE WHEN @SortColType = 'Desk ASC' THEN DebtAccountId END,                      
 CASE WHEN @SortColType = 'Desk DESC' THEN DebtAccountId END DESC ,  

 CASE WHEN @SortColType = 'ToDesk ASC' THEN TempOwnerId END,                      
 CASE WHEN @SortColType = 'ToDesk DESC' THEN TempOwnerId END DESC ,  
  
 CASE WHEN @SortColType = 'DateAssigned ASC' THEN  isnull(ReferalDate,cast('2079/01/01' as datetime)) END,                    
 CASE WHEN @SortColType = 'DateAssigned DESC' THEN ReferalDate END DESC,   

  CASE WHEN @SortColType = 'ReviewDate ASC' THEN  isnull(NextContactDate,cast('2079/01/01' as datetime)) END,                    
  CASE WHEN @SortColType = 'ReviewDate DESC' THEN NextContactDate END DESC,   

  CASE WHEN @SortColType = 'LastPayment ASC' THEN  isnull(LastPaymentDate,cast('2079/01/01' as datetime)) END,                    
  CASE WHEN @SortColType = 'LastPayment DESC' THEN LastPaymentDate END DESC,   

  CASE WHEN @SortColType = 'CloseDate ASC' THEN  isnull(CloseDate,cast('2079/01/01' as datetime)) END,                    
  CASE WHEN @SortColType = 'CloseDate DESC' THEN CloseDate END DESC,   

 CASE WHEN @SortColType = 'AssignmentAmt ASC' THEN TotAsgAmt END,                      
 CASE WHEN @SortColType = 'AssignmentAmt DESC' THEN TotAsgAmt END DESC , 

 CASE WHEN @SortColType = 'CollectFees ASC' THEN TotCollFeeAmt END,                      
 CASE WHEN @SortColType = 'CollectFees DESC' THEN TotCollFeeAmt END DESC , 

 CASE WHEN @SortColType = 'Interest ASC' THEN TotIntAmt END,                      
 CASE WHEN @SortColType = 'Interest DESC' THEN TotIntAmt END DESC , 

 CASE WHEN @SortColType = 'Payments ASC' THEN TotPmtAmt END,                      
 CASE WHEN @SortColType = 'Payments DESC' THEN TotPmtAmt END DESC , 

 CASE WHEN @SortColType = 'TotalOwned ASC' THEN TotBalAmt END,                      
 CASE WHEN @SortColType = 'TotalOwned DESC' THEN TotBalAmt END DESC , 

  CASE WHEN @SortColType = 'SCRevDate ASC' THEN  isnull(SCReviewDate,cast('2079/01/01' as datetime)) END,                    
  CASE WHEN @SortColType = 'SCRevDate DESC' THEN SCReviewDate END DESC,   
    
  CASE WHEN @SortColType = 'SCTrialDate ASC' THEN  isnull(TrialDate,cast('2079/01/01' as datetime)) END,                    
  CASE WHEN @SortColType = 'SCTrialDate DESC' THEN TrialDate END DESC,   
      
  CASE WHEN @SortColType = 'FWDCourtDate ASC' THEN  isnull(FwdCourtDate,cast('2079/01/01' as datetime)) END,                    
  CASE WHEN @SortColType = 'FWDCourtDate DESC' THEN FwdCourtDate END DESC,   

   CASE WHEN @SortColType = 'FWDJudgeDate ASC' THEN  isnull(FwdJudgmentDate,cast('2079/01/01' as datetime)) END,                    
  CASE WHEN @SortColType = 'FWDJudgeDate DESC' THEN FwdJudgmentDate END DESC 
End

If @IsFetchByAddr = 1
Begin
SELECT
DebtAccountId,
VW.CLIENTCODE,
DebtorName,
AccountNo,
da.AddressLine1,
da.City,
da.State,
da.PostalCode,
da.PhoneNo,
--FileNumber,
ReferalDate,
CollectionStatus,
Location,
NextContactDate,
Priority,
TotBalAmt,
da.Fax,
da.SSN,
CloseDate,
NextStatusDate,
SCReviewDate,
TrialDate,
CaseNum,
EntityName1,
da.ContactName,
TempOwner,
BKCaseNo,
DebtorBKName,
JudgmentDate,
FWDJudgmentDate,
StatusGroupDescription,
TempOwnerId,
LastPaymentDate,
TotAsgAmt,
TotCollFeeAmt,
TotPmtAmt,
FwdCourtDate,
CollectionStatusId,
TotIntAmt

FROM  vwAccountFilterList vw (nolock)
Join DebtAddress da on vw.DebtId = da.DebtId 
where
( ( vw.DebtAccountId > @MinDebtAccountId and @DebtAccountId = -1) 
	or ( vw.DebtAccountId =  @DebtAccountId ) OR @DebtAccountId IS NULL )
AND ( vw.ClientCode Like @SERVICECODE OR @ServiceCode IS NULL)
AND ( vw.AccountNo Like  @AccountNo OR @AccountNo IS NULL)
AND ( vw.DebtorName Like @DebtorName  OR @DebtorName IS NULL)	
AND ( da.ContactName Like @ContactName  OR @ContactName IS NULL)
AND ( da.AddressLine1 Like @AddressLine1  OR @AddressLine1 IS NULL)
AND ( da.State = @State  OR @State IS NULL)
AND ( da.PhoneNo Like  @PhoneNo  OR @PhoneNo IS NULL)
AND ( da.Fax Like  @Fax  OR @Fax IS NULL)
AND ( vw.SSN Like  @TaxId  OR @TaxId IS NULL)
AND ( vw.Priority = @Priority OR @Priority IS NULL)
AND ( vw.EntityName1 Like @EntityName OR @EntityName IS NULL)
AND ( vw.CaseNum Like @CaseNum OR @CaseNum IS NULL)
AND ( vw.BKCaseNo Like @BKCaseNo OR @BKCaseNo IS NULL)
AND ( vw.DebtorBKName Like @DebtorBKName OR @DebtorBKName IS NULL)
AND ( vw.AttyId = @AttyId OR @AttyId = 0)
AND ( vw.CourtId = @CourtId OR @CourtId = 0)
AND ( vw.ProcessServerId = @ProcessServerId OR @ProcessServerId = 0)
AND ( (VW.[NextContactDate] BETWEEN @NextContactDateFrom AND @NextContactDateTo) OR (@NextContactDateFrom IS NULL OR @NextContactDateTo IS NULL))
AND ( VW.[NextStatusDate] <= @NextStatusDate OR @NextStatusDate IS NULL)
AND ( (VW.[ReferalDate] BETWEEN @ReferalDateFrom AND @ReferalDateTo) OR (@ReferalDateFrom IS NULL OR @ReferalDateTo IS NULL))
AND ( (VW.[SCReviewDate] BETWEEN @SCReviewDateFrom AND @SCReviewDateTo) OR (@SCReviewDateFrom IS NULL OR @SCReviewDateTo IS NULL))
AND ( (VW.[TrialDate] BETWEEN @TrialDateFrom AND @TrialDateTo ) OR (@TrialDateFrom IS NULL OR @TrialDateTo IS NULL))
AND ( (vw.TotBalAmt BETWEEN @BalAmtFrom AND @BalAmtTo) OR (@BalAmtFrom IS NULL OR @BalAmtTo IS NULL))
AND ( vw.LocationId = @LocationId OR @LocationId = 0)
AND ( vw.CollectionStatusId = @CollectionStatusId OR @CollectionStatusId = 0)
AND ( vw.StatusGroupId = @StatusGroupId OR @StatusGroupId = 0)
AND ( vw.TempOwnerId = @TempDeskId OR @TempDeskId = 0)
AND ( (VW.[JudgmentDate] BETWEEN @SCJudgmentDateFrom AND @SCJudgmentDateTo) OR (@SCJudgmentDate IS NULL OR @SCJudgmentDate IS NULL))
AND ( (VW.[FWDJudgmentDate] BETWEEN @FWDJudgmentDateFrom AND @FWDJudgmentDateTo ) OR (@FWDJudgmentDateFrom IS NULL OR @FWDJudgmentDateTo IS NULL))
AND  ((VW.[FWDCourtDate] BETWEEN @FWDCourtDateFrom AND @FWDCourtDateTo ) OR (@FWDCourtDateFrom IS NULL OR @FWDCourtDateTo IS NULL))
AND ( vw.CourtMemo Like @CourtMemo OR @CourtMemo IS NULL)
AND ( vw.AttyRef Like @AttorneyRefNumber OR @AttorneyRefNumber IS NULL)
--ORDER BY VW.DebtAccountId DESC
ORDER BY
 CASE WHEN @SortColType = 'DebtAccountId ASC' THEN  DebtAccountId END,                      
 CASE WHEN @SortColType = 'DebtAccountId DESC' THEN  DebtAccountId END DESC,                      
 
 CASE WHEN @SortColType = 'DebtorName ASC' THEN  DebtorName END,                      
 CASE WHEN @SortColType = 'DebtorName DESC' THEN DebtorName END DESC ,                
  
 CASE WHEN @SortColType = 'Contact ASC' THEN da.ContactName END,                      
 CASE WHEN @SortColType = 'Contact DESC' THEN da.ContactName END DESC ,   

 CASE WHEN @SortColType = 'DebtorAddress ASC' THEN da.AddressLine1 END,                      
 CASE WHEN @SortColType = 'DebtorAddress DESC' THEN da.AddressLine1 END DESC ,  
  
 CASE WHEN @SortColType = 'DebtorRef ASC' THEN AccountNo END,                      
 CASE WHEN @SortColType = 'DebtorRef DESC' THEN AccountNo END DESC ,  

 CASE WHEN @SortColType = 'StatusCode ASC' THEN CollectionStatusId END,                      
 CASE WHEN @SortColType = 'StatusCode DESC' THEN CollectionStatusId END DESC ,  

 CASE WHEN @SortColType = 'Desk ASC' THEN DebtAccountId END,                      
 CASE WHEN @SortColType = 'Desk DESC' THEN DebtAccountId END DESC ,  

 CASE WHEN @SortColType = 'ToDesk ASC' THEN TempOwnerId END,                      
 CASE WHEN @SortColType = 'ToDesk DESC' THEN TempOwnerId END DESC ,  
  
 CASE WHEN @SortColType = 'DateAssigned ASC' THEN  isnull(ReferalDate,cast('2079/01/01' as datetime)) END,                    
 CASE WHEN @SortColType = 'DateAssigned DESC' THEN ReferalDate END DESC,   

  CASE WHEN @SortColType = 'ReviewDate ASC' THEN  isnull(NextContactDate,cast('2079/01/01' as datetime)) END,                    
  CASE WHEN @SortColType = 'ReviewDate DESC' THEN NextContactDate END DESC,   

  CASE WHEN @SortColType = 'LastPayment ASC' THEN  isnull(LastPaymentDate,cast('2079/01/01' as datetime)) END,                    
  CASE WHEN @SortColType = 'LastPayment DESC' THEN LastPaymentDate END DESC,   

  CASE WHEN @SortColType = 'CloseDate ASC' THEN  isnull(CloseDate,cast('2079/01/01' as datetime)) END,                    
  CASE WHEN @SortColType = 'CloseDate DESC' THEN CloseDate END DESC,   

 CASE WHEN @SortColType = 'AssignmentAmt ASC' THEN TotAsgAmt END,                      
 CASE WHEN @SortColType = 'AssignmentAmt DESC' THEN TotAsgAmt END DESC , 

 CASE WHEN @SortColType = 'CollectFees ASC' THEN TotCollFeeAmt END,                      
 CASE WHEN @SortColType = 'CollectFees DESC' THEN TotCollFeeAmt END DESC , 

 CASE WHEN @SortColType = 'Interest ASC' THEN TotIntAmt END,                      
 CASE WHEN @SortColType = 'Interest DESC' THEN TotIntAmt END DESC , 

 CASE WHEN @SortColType = 'Payments ASC' THEN TotPmtAmt END,                      
 CASE WHEN @SortColType = 'Payments DESC' THEN TotPmtAmt END DESC , 

 CASE WHEN @SortColType = 'TotalOwned ASC' THEN TotBalAmt END,                      
 CASE WHEN @SortColType = 'TotalOwned DESC' THEN TotBalAmt END DESC , 

  CASE WHEN @SortColType = 'SCRevDate ASC' THEN  isnull(SCReviewDate,cast('2079/01/01' as datetime)) END,                    
  CASE WHEN @SortColType = 'SCRevDate DESC' THEN SCReviewDate END DESC,   
    
  CASE WHEN @SortColType = 'SCTrialDate ASC' THEN  isnull(TrialDate,cast('2079/01/01' as datetime)) END,                    
  CASE WHEN @SortColType = 'SCTrialDate DESC' THEN TrialDate END DESC,   

    CASE WHEN @SortColType = 'SCJudgeDate ASC' THEN  isnull(JudgmentDate,cast('2079/01/01' as datetime)) END,                    
  CASE WHEN @SortColType = 'SCJudgeDate DESC' THEN JudgmentDate END DESC ,
      
  CASE WHEN @SortColType = 'FWDCourtDate ASC' THEN  isnull(FwdCourtDate,cast('2079/01/01' as datetime)) END,                    
  CASE WHEN @SortColType = 'FWDCourtDate DESC' THEN FwdCourtDate END DESC,   

   CASE WHEN @SortColType = 'FWDJudgeDate ASC' THEN  isnull(FwdJudgmentDate,cast('2079/01/01' as datetime)) END,                    
  CASE WHEN @SortColType = 'FWDJudgeDate DESC' THEN FwdJudgmentDate END DESC 

End