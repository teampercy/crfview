﻿
using CRFView.Adapters;
using CRFView.Adapters.Liens.DayEnd.ClientAcknowledgments.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;



namespace CRFView.Controllers.Liens.DayEnd.ClientAcknowledgments
{
    public class ClientAcknowledgmentsController:Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()

        {
            //TestVM objTestVM = new TestVM();
            ClientAcknowledgmentsVM objClientAcknowledgmentsVM = new ClientAcknowledgmentsVM();
            return PartialView(GetVirtualPath("_ClientAcknowledgments"), objClientAcknowledgmentsVM);
        }



        //Print CollectionReports 
        [HttpPost]
        public string ClientAcknowledgmentsPrint(ClientAcknowledgmentsVM objClientAcknowledgmentsVM)
        {

            try
            {
                string PdfUrl = null;

                PdfUrl = ADvwJobInventory.PrintClientAcknowledgments(objClientAcknowledgmentsVM);


                if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
                {
                    string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                    string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                    return (path);
                }
            }
            catch (Exception ex)
            {
                return "error";
            }
            return "error";
        }



        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/DayEnd/ClientAcknowledgments/" + ViewName + ".cshtml";
            return path;
        }

    }
}
