﻿using CRFView.Adapters;
using CRFView.Adapters.Collections.DataEntry.ImportEDI.ViewModels;
using CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.DayEnds.EndDateUpdate
{
    public class EndDateUpdateController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }
        [HttpPost]
        public ActionResult ShowModal()
        {
            ImportEDIVM objImportEDIVM = new ImportEDIVM();

            return PartialView(GetVirtualPath("_EndDateUpdate"), objImportEDIVM);
        }
        //Save ImportEDI Document
        [HttpPost]
        public ActionResult SaveEndDateUpdate()
        {
            try
            {
                var currDate = DateTime.Now.ToString("yyyyMMddHHmmss");
                var file = Request.Files[0];
                //var fileName = Path.GetFileName(file.FileName);
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var fileExt = Path.GetExtension(file.FileName);
                fileName = fileName + "_" + currDate + fileExt;
                var path = Path.Combine(Server.MapPath("~/Output/UploadedFiles"), fileName);
                file.SaveAs(path);
                Session["ImportEDIDocumentPath"] = path;
                return Json(true);
            }
            catch (Exception ex)
            {
                return Json(false);
            }
        }

        //Import EDI
        [HttpPost]
        public int EDIEndDateUpdate(ImportEDIVM objImportEDIVM)
        {
            int result = 0;
            try
            {
                objImportEDIVM.FileName = Convert.ToString(Session["ImportEDIDocumentPath"]);
                if (!String.IsNullOrEmpty(objImportEDIVM.FileName))
                {
                      result = ADClientInterface.EndDateUpdateEDI(objImportEDIVM);
                }
           
                else
                {
                    return -1;
                }
            }
            catch (Exception ex)
            {
                return -1;
            }
            return 1;
        }


        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/DayEnd/EndDateUpdate/" + ViewName + ".cshtml";
            return path;
        }
    }
}
