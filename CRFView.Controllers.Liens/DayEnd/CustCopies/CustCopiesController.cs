﻿using CRFView.Adapters;
using CRFView.Adapters.Liens.DayEnd.CustCopies.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.DayEnd.CustCopies
{
    public class CustCopiesController : Controller
    {


        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            CustCopiesVM objCustCopiesVM = new CustCopiesVM();
            return PartialView(GetVirtualPath("_CustCopies"), objCustCopiesVM);
        }


        //Print CustCopies 
        [HttpPost]
        public string CustCopiesPrint(CustCopiesVM objCustCopiesVM)
        {

            try
            {
                string PdfUrl = null;

                PdfUrl = ADvwJobInventory.PrintCustCopies(objCustCopiesVM);


                if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
                {
                    string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                    string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                    return (path);
                }
            }
            catch (Exception ex)
            {
                return "error";
            }
            return "error";
        }


        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/DayEnd/CustCopies/" + ViewName + ".cshtml";
            return path;
        }


    }
}
