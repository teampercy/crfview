﻿using CRFView.Adapters;
using CRFView.Adapters.Liens.DayEnd.NoticeCopies.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.DayEnd.NoticeCopies
{
    public class NoticeCopiesController : Controller
    {


        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            NoticeCopiesVM objNoticeCopiesVM = new NoticeCopiesVM();
            return PartialView(GetVirtualPath("_NoticeCopies"), objNoticeCopiesVM);
        }

        //Print NoticeCopies 
        [HttpPost]
        public string NoticeCopiesPrint(NoticeCopiesVM objNoticeCopiesVM)
        {
            try
            {
                string PdfUrl = null;

                PdfUrl = ADvwJobInventory.PrintNoticeCopies(objNoticeCopiesVM);


                if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
                {
                    string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                    string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                    return (path);
                }
            }
            catch (Exception ex)
            {
                return "error";
            }
            return "error";
        }



        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/DayEnd/NoticeCopies/" + ViewName + ".cshtml";
            return path;
        }



    }


}
