﻿//using CRFView.Adapters.Liens.
using System;
using System.Collections.Generic;
using CRFView.Adapters.Liens.DayEnds.AutoEmail.ViewModels;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CRFView.Adapters.Liens.DayEnds;
using System.IO;
using CRFView.Adapters;

namespace CRFView.Controllers.Liens.DayEnds
{
   public class AutoEmailController:Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return View(GetVirtualPath("Index"));
        }

        [HttpPost]
        public ActionResult ShowModal()
        {

           AutoEmailVM objAutoEmailVM = new AutoEmailVM();


            return PartialView(GetVirtualPath("_AutoEmail"), objAutoEmailVM);

        }
        //Print CollectionReports
        [HttpPost]
        public string AutoEmailPrint(AutoEmailVM objAutoEmailVM)
        {

            try
            {
                string PdfUrl = null;
                
                PdfUrl = Adapters.ADvwJobInfo.PrintAutoEmail(objAutoEmailVM);

                objAutoEmailVM.Message = "Completed";

                return objAutoEmailVM.Message;
            }
            catch (Exception ex)
            {
                return "error";
            }
           
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/DayEnd/AutoEmail/" + ViewName + ".cshtml";
            return path;
        }
    }
}
