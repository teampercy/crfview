﻿using CRFView.Adapters;
using CRFView.Adapters.Liens.LiensDayEnd.AutoFax.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.LinesDayEnd.AutoFax
{
  public  class AutoFaxController: Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        [HttpPost]
        public ActionResult ShowModal()
        {

            AutoFaxVM objAutoFaxVM = new AutoFaxVM();


            return PartialView(GetVirtualPath("_AutoFax"), objAutoFaxVM);

        }
        
        [HttpPost]
        public string AutoFaxPrint(AutoFaxVM objAutoFaxVM)
        {

            try
            {
                string PdfUrl = null;

                PdfUrl = ADvwJobInfo.PrintAutoFax(objAutoFaxVM);


                if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
                {
                    string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                    string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                    return (path);
                }
            }
            catch (Exception ex)
            {
                return "error";
            }
            return "error";
        }


        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/DayEnd/AutoFax/" + ViewName + ".cshtml";
            return path;
        }
    }
}
