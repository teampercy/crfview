﻿using CRFView.Adapters;
using CRFView.Adapters.Liens.JVTXNoticeRequest.PrintLetters.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.NoticeCycle.PrintLetters
{
    public class NoticeCyclePrintLettersController:Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }
        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            JVTXPrintLettersVM objJVTXPrintLettersVM = new JVTXPrintLettersVM();

            DataTable dtGetTXNoticesByDateRequested = ADJob.GetTXNoticesByDateRequested();

            if (dtGetTXNoticesByDateRequested.Rows.Count < 1)
            {
                ViewBag.lblMessage = "No Notices Selected, Please Run Extract.";
                ViewBag.btnOK = "Hide OK";
            }
            else
            {
                ViewBag.lblMessage = string.Empty;
                ViewBag.btnOK = "Show OK";
            }

            return PartialView(GetVirtualPath("_NoticeCyclePrintLetters"), objJVTXPrintLettersVM);
        }

        //JVTX Letters Print
        [HttpPost]
        public ActionResult NoticeCycleLettersPrint(JVTXPrintLettersVM objJVTXPrintLettersVM)
        {
            int result = 0;
            try
            {
                result = ADJob.PrintNoticeCycleLetters(objJVTXPrintLettersVM);
            }
            catch (Exception ex)
            {
                result = -1;
            }
            return Json(result);
        }


        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/NoticeCycle/PrintLetters/" + ViewName + ".cshtml";
            return path;
        }
    }
}
