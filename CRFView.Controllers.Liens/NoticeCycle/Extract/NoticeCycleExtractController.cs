﻿using CRFView.Adapters;
using CRFView.Adapters.Liens.JVTXNoticeRequest.Extract.ViewModels;
using CRFView.Adapters.Liens.NoticeCycle.Extract.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.NoticeCycle.Extract
{
     public class NoticeCycleExtractController:Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }


        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            NoticeCycleExtractVM objNoticeCycleExtractVM = new NoticeCycleExtractVM();

            DataTable dtGetNoticesByDateRequested = ADJob.GetNoticesByDateRequested();

            if (dtGetNoticesByDateRequested.Rows.Count > 0)
            {
                ViewBag.lblMessage = "The Prior Print Cycle was not closed, Please Run Close Cycle.";
                ViewBag.btnOK = "Hide OK";
            }
            else
            {
                objNoticeCycleExtractVM.objUSPSCertNum = ADUSPSCertNum.ReadUSPSCertNum(1);
                objNoticeCycleExtractVM.LastCertNumber = objNoticeCycleExtractVM.objUSPSCertNum.SeqNo;

                ViewBag.lblMessage = string.Empty;
                ViewBag.btnOK = "Show OK";
            }

            return PartialView(GetVirtualPath("_NoticeCycleExtract"), objNoticeCycleExtractVM);
        }


        //JV TX Extract Letters
        [HttpPost]
        public ActionResult NoticeCycleExtractLetters(NoticeCycleExtractVM objNoticeCycleExtractVM)
        {
            int result = 0;
            try
            {
                if (objNoticeCycleExtractVM.chkAssignCerts == true)
                {
                    objNoticeCycleExtractVM.objUSPSCertNum = ADUSPSCertNum.ReadUSPSCertNum(1);
                    objNoticeCycleExtractVM.objUSPSCertNum.SeqNo = objNoticeCycleExtractVM.LastCertNumber;
                    objNoticeCycleExtractVM.objUSPSCertNum.Id = 1;
                    ADUSPSCertNum.SaveUSPSCertNum(objNoticeCycleExtractVM.objUSPSCertNum);
                }

                result = ADJob.ProcessNoticeCycleExtractLetters(objNoticeCycleExtractVM);
            }
            catch (Exception ex)
            {
                result = -0;
            }
            return Json(result);
        }



        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/NoticeCycle/Extract/" + ViewName + ".cshtml";
            return path;
        }



    }
}
