﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using CRFView.Adapters;
using CRFView.Adapters.Liens.Managements.DeleteGC.ViewModels;
using System.Web.Mvc;


namespace CRFView.Controllers.Liens.Managements.DeleteGC
{
   
    public class DeleteGCController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
          
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            DeleteGCVM objDeleteGCVM = new DeleteGCVM();
            ViewBag.lblRecCount = "";
            //Session["objDeleteGCVM"] = null;
            Session["GenId"] = null;
            Session["GeneralContractor"] = null;
            Session["RefNum"] = null;
            Session["AddressLine1"] = 
            Session["AddressLine2"] = 
            Session["City"]  = null;
            Session["State"] = null;
            Session["PostalCode"] = null;
            return PartialView(GetVirtualPath("_DeleteGC"), objDeleteGCVM);
        }

        [HttpPost]
        public ActionResult LoadDeleteGCLookup(string ClientCode)
        {

            DeleteGCVM objDeleteGCVM = new DeleteGCVM();
           objDeleteGCVM.ClientCode = Convert.ToInt32(ClientCode);

            Session["clientId"] = objDeleteGCVM.ClientCode;

            ViewBag.lblRecCount = "";
            return PartialView(GetVirtualPath("_GCLookup"), objDeleteGCVM);
        }

        public ActionResult GetDeleteGC(DeleteGCVM objDeleteGCVM)
        {


            objDeleteGCVM.dtDeleteGCList = ADClientGeneralContractor.GetDeleteGCDt(objDeleteGCVM);
            objDeleteGCVM.DeleteGCList = ADClientGeneralContractor.GetDeleteGCList(objDeleteGCVM.dtDeleteGCList);

           ViewBag.lblRecCount = "Total  (" + objDeleteGCVM.DeleteGCList.Count + ") Records";

            return PartialView(GetVirtualPath("_GCLookup"), objDeleteGCVM);
        }
        [HttpPost]
        public ActionResult LoadDeleteGCRepLookup(string ClientCode)
        {
            
            DeleteGCVM objDeleteGCVM = new DeleteGCVM();
            objDeleteGCVM.ClientCode = Convert.ToInt32(ClientCode);

            ViewBag.lblRecCount = "";

            return PartialView(GetVirtualPath("_GCReplaceLookup"), objDeleteGCVM);
        }

        public ActionResult GetDeleteGCRep(DeleteGCVM objDeleteGCVM)
        {


            objDeleteGCVM.dtDeleteGCList = ADClientGeneralContractor.GetDeleteGCDt(objDeleteGCVM);
            objDeleteGCVM.DeleteGCList = ADClientGeneralContractor.GetDeleteGCList(objDeleteGCVM.dtDeleteGCList);

             ViewBag.lblRecCount = "Total  (" + objDeleteGCVM.DeleteGCList.Count + ") Records";

            return PartialView(GetVirtualPath("_GCReplaceLookup"), objDeleteGCVM);
        }

        [HttpPost]
        public ActionResult LoadDeleteGC(string Id)
        {
            //DeleteGCVM objDeleteGCVM = null;
            //if (Session["objDeleteGCVM"] == null)
            //{
            //    objDeleteGCVM = new DeleteGCVM();

            //}
            //else
            //{
            //    objDeleteGCVM = (DeleteGCVM)Session["objDeleteGCVM"];
            //}

            DeleteGCVM objDeleteGCVM = new DeleteGCVM();




            if (Session["clientId"] != null)
            {
                objDeleteGCVM.ClientCode = Convert.ToInt32(Session["clientId"]);
            }

            objDeleteGCVM.ObjDeleteGC = ADClientGeneralContractor.ReadClientGeneralContractor(Convert.ToInt32(Id));

            objDeleteGCVM.ObjDeleteGC.DelGCName = objDeleteGCVM.ObjDeleteGC.GeneralContractor;
            objDeleteGCVM.ObjDeleteGC.DelGenId =  objDeleteGCVM.ObjDeleteGC.GenId;
            objDeleteGCVM.ObjDeleteGC.DelRefNum = objDeleteGCVM.ObjDeleteGC.RefNum;
            objDeleteGCVM.ObjDeleteGC.DelAdd1 = objDeleteGCVM.ObjDeleteGC.AddressLine1;
            objDeleteGCVM.ObjDeleteGC.DelAdd2 = objDeleteGCVM.ObjDeleteGC.AddressLine2;
            objDeleteGCVM.ObjDeleteGC.DelCity = objDeleteGCVM.ObjDeleteGC.City;
            objDeleteGCVM.ObjDeleteGC.DelState = objDeleteGCVM.ObjDeleteGC.State;
            objDeleteGCVM.ObjDeleteGC.DelZip = objDeleteGCVM.ObjDeleteGC.PostalCode;

            Session["GeneralContractor"] = objDeleteGCVM.ObjDeleteGC.DelGCName;
            Session["GenId"] = objDeleteGCVM.ObjDeleteGC.DelGenId;
            Session["RefNum"] = objDeleteGCVM.ObjDeleteGC.DelRefNum;
            Session["AddressLine1"] = objDeleteGCVM.ObjDeleteGC.DelAdd1;
            Session["AddressLine2"] = objDeleteGCVM.ObjDeleteGC.DelAdd2;
            Session["City"] = objDeleteGCVM.ObjDeleteGC.DelCity;
            Session["State"] = objDeleteGCVM.ObjDeleteGC.DelState;
            Session["PostalCode"] = objDeleteGCVM.ObjDeleteGC.DelZip;
            //Session["objDeleteGCVM"] = objDeleteGCVM;
            ViewBag.lblRecCount = "";

            return PartialView(GetVirtualPath("_DeleteGC"), objDeleteGCVM);
        }
        [HttpPost]
        public ActionResult LoadDeleteGCReplacec(string Id)
        {
            //DeleteGCVM objDeleteGCVM = null;
            //if (Session["objDeleteGCVM"] == null)
            //{
            //    objDeleteGCVM = new DeleteGCVM();

            //}
            //else
            //{
            //    objDeleteGCVM = (DeleteGCVM)Session["objDeleteGCVM"];
            //}

            DeleteGCVM objDeleteGCVM = new DeleteGCVM();

            if (Session["clientId"] != null)
            {
                objDeleteGCVM.ClientCode = Convert.ToInt32(Session["clientId"]);
            }

            objDeleteGCVM.ObjDeleteGC = ADClientGeneralContractor.ReadClientGeneralContractor(Convert.ToInt32(Id));

            if (Session["clientId"] != null)
            {
                objDeleteGCVM.ClientCode = Convert.ToInt32(Session["clientId"]);
            }
            if (Session["GeneralContractor"] != null)
            {
                objDeleteGCVM.ObjDeleteGC.DelGCName = Convert.ToString(Session["GeneralContractor"]);
            }
            if (Session["GenId"] != null)
            {
                objDeleteGCVM.ObjDeleteGC.DelGenId = Convert.ToString(Session["GenId"]);
            }
            if (Session["RefNum"] != null)
            {
                objDeleteGCVM.ObjDeleteGC.DelRefNum = Convert.ToString(Session["RefNum"]);
            }
            if (Session["AddressLine1"] != null)
            {
                objDeleteGCVM.ObjDeleteGC.DelAdd1 = Convert.ToString(Session["AddressLine1"]);
            }
            if (Session["AddressLine2"] != null)
            {
                objDeleteGCVM.ObjDeleteGC.DelAdd2 = Convert.ToString(Session["AddressLine2"]);
            }
            if (Session["City"] != null)
            {
                objDeleteGCVM.ObjDeleteGC.DelCity = Convert.ToString(Session["City"]);
            }
            if (Session["State"] != null)
            {
                objDeleteGCVM.ObjDeleteGC.DelState = Convert.ToString(Session["State"]);
            }

            if (Session["PostalCode"] != null)
            {
                objDeleteGCVM.ObjDeleteGC.DelZip = Convert.ToString(Session["PostalCode"]);
            }


            objDeleteGCVM.ObjDeleteGC.RepGCName = objDeleteGCVM.ObjDeleteGC.GeneralContractor;
            objDeleteGCVM.ObjDeleteGC.RepGenId = objDeleteGCVM.ObjDeleteGC.GenId;
            objDeleteGCVM.ObjDeleteGC.RepRefNum = objDeleteGCVM.ObjDeleteGC.RefNum;
            objDeleteGCVM.ObjDeleteGC.RepAdd1 = objDeleteGCVM.ObjDeleteGC.AddressLine1;
            objDeleteGCVM.ObjDeleteGC.RepAdd2 = objDeleteGCVM.ObjDeleteGC.AddressLine2;
            objDeleteGCVM.ObjDeleteGC.RepCity = objDeleteGCVM.ObjDeleteGC.City;
            objDeleteGCVM.ObjDeleteGC.RepState = objDeleteGCVM.ObjDeleteGC.State;
            objDeleteGCVM.ObjDeleteGC.RepZip = objDeleteGCVM.ObjDeleteGC.PostalCode;
            if (objDeleteGCVM.ObjDeleteGC.DelGenId == objDeleteGCVM.ObjDeleteGC.RepGenId)
            {
                objDeleteGCVM.message = "Replacing General contractor cannot be the same deleting General contractor";
                objDeleteGCVM.ObjDeleteGC.RepGCName = null;
                objDeleteGCVM.ObjDeleteGC.RepGenId = null;
                objDeleteGCVM.ObjDeleteGC.RepRefNum = null;
                objDeleteGCVM.ObjDeleteGC.RepAdd1 = null;
                objDeleteGCVM.ObjDeleteGC.RepAdd2 = null;
                objDeleteGCVM.ObjDeleteGC.RepCity = null;
                objDeleteGCVM.ObjDeleteGC.RepState = null;
                objDeleteGCVM.ObjDeleteGC.RepZip = null;
            }
            //Session["objDeleteGCVM"] = objDeleteGCVM;
            //ViewBag.lblRecCount = "";
            return PartialView(GetVirtualPath("_DeleteGC"), objDeleteGCVM);
        }
        //Delete customer
        [HttpPost]
        public ActionResult DeleteGC(DeleteGCVM objDeleteGCVM)
        {
            int result = 0;
            try
            {
                result = ADClientGeneralContractor.Delete_GC(objDeleteGCVM);

                return Json(result);
            }
            catch
            {
                result = -1;
                return Json(result);
            }

        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/Managements/DeleteGC/" + ViewName + ".cshtml";

            return path;
        }
    }
}
