﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CRFView.Adapters.Liens.Managements.DeleteCustomer.ViewModels;
using System.Threading.Tasks;
using System.Web.Mvc;
using CRFView.Adapters;
using System.Data;

namespace CRFView.Controllers.Liens.Managements.DeleteCustomer
{
    public class DeleteCustomerController : Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            
            DeleteCustomerVM objDeleteCustomerVM = new DeleteCustomerVM();
            //Session["objDeleteCustomerVM"] = null;


            Session["CustId"] = null;
            Session["ClientCustomer"] = null;
            Session["RefNum"] = null;
            Session["AddressLine1"] = null;
            Session["AddressLine2"] = null;
            Session["City"] = null;
            Session["State"] = null;
            Session["PostalCode"] = null;

            return PartialView(GetVirtualPath("_DeleteCustomer"), objDeleteCustomerVM);
        }

        
        public ActionResult ShowModalCustomerLookup(string ClientId)
        {
            DeleteCustomerVM objDeleteCustomerVM = new DeleteCustomerVM();
            objDeleteCustomerVM.ClientId = Convert.ToString(ClientId);
            objDeleteCustomerVM.Delvalue = 1;
            Session["clientId"] = objDeleteCustomerVM.ClientId;
            ViewBag.lblRecCount = "";
            return PartialView(GetVirtualPath("_CustomerLookUp"), objDeleteCustomerVM);
        }

        public ActionResult ShowModalRepCustomerLookup(string ClientId)
        {
            DeleteCustomerVM objDeleteCustomerVM = new DeleteCustomerVM();
            objDeleteCustomerVM.ClientId = Convert.ToString(ClientId);
            objDeleteCustomerVM.Repvalue = 2;
            Session["clientId"] = objDeleteCustomerVM.ClientId;
            ViewBag.lblRecCount = "";
            return PartialView(GetVirtualPath("_CustomerLookUp"), objDeleteCustomerVM);
        }

        public ActionResult GetCustomerLookUpList(DeleteCustomerVM objDeleteCustomerVM)
        {
            objDeleteCustomerVM.dtCustomerLoupList = ADClientCustomer.GetCustomerLookUpDt(objDeleteCustomerVM);
            objDeleteCustomerVM.ClientCustomerList = ADClientCustomer.GetCustomerLookUpList(objDeleteCustomerVM.dtCustomerLoupList);

            //if (objDeleteCustomerVM.dtCustomerLoupList.Rows.Count < 1)
            //{
            //    objDeleteCustomerVM.Message = "Not Found";
            //}
            ViewBag.lblRecCount = "Total  (" + objDeleteCustomerVM.ClientCustomerList.Count + ") Records";

            return PartialView(GetVirtualPath("_CustomerLookUp"), objDeleteCustomerVM);
        }

        [HttpPost]
        public ActionResult LoadDeleteCustomer(string Id, int Delvalue)
        {
            DeleteCustomerVM objDeleteCustomerVM = new DeleteCustomerVM();


            if (Session["clientId"] != null)
            {
                objDeleteCustomerVM.ClientId = Convert.ToString(Session["clientId"]);
            }

            objDeleteCustomerVM.objADClientCustomer = ADClientCustomer.ReadClientCustomer(Convert.ToInt32(Id));

            if (Delvalue == 1)
            {
                objDeleteCustomerVM.DelCustId = objDeleteCustomerVM.objADClientCustomer.CustId;
                objDeleteCustomerVM.DelCustName = objDeleteCustomerVM.objADClientCustomer.ClientCustomer;
                objDeleteCustomerVM.DelRefNum = objDeleteCustomerVM.objADClientCustomer.RefNum;
                objDeleteCustomerVM.DelAddr1 = objDeleteCustomerVM.objADClientCustomer.AddressLine1;
                objDeleteCustomerVM.DelAddr2 = objDeleteCustomerVM.objADClientCustomer.AddressLine2;
                objDeleteCustomerVM.DelCity = objDeleteCustomerVM.objADClientCustomer.City;
                objDeleteCustomerVM.DelState = objDeleteCustomerVM.objADClientCustomer.State;
                objDeleteCustomerVM.DelZip = objDeleteCustomerVM.objADClientCustomer.PostalCode;

                Session["CustId"] = objDeleteCustomerVM.DelCustId;
                Session["ClientCustomer"] = objDeleteCustomerVM.DelCustName;
                Session["RefNum"] = objDeleteCustomerVM.DelRefNum;
                Session["AddressLine1"] = objDeleteCustomerVM.DelAddr1;
                Session["AddressLine2"] = objDeleteCustomerVM.DelAddr2;
                Session["City"] = objDeleteCustomerVM.DelCity;
                Session["State"] = objDeleteCustomerVM.DelState;
                Session["PostalCode"] = objDeleteCustomerVM.DelZip;
            }

            else
            {
                if (Session["CustId"] != null)
                {
                    objDeleteCustomerVM.DelCustId = Convert.ToInt32(Session["CustId"]);
                }
                if (Session["ClientCustomer"] != null)
                {
                    objDeleteCustomerVM.DelCustName = Convert.ToString(Session["ClientCustomer"]);
                }
                if (Session["RefNum"] != null)
                {
                    objDeleteCustomerVM.DelRefNum = Convert.ToString(Session["RefNum"]);
                }
                if (Session["AddressLine1"] != null)
                {
                    objDeleteCustomerVM.DelAddr1 = Convert.ToString(Session["AddressLine1"]);
                }
                if (Session["AddressLine2"] != null)
                {
                    objDeleteCustomerVM.DelAddr2 = Convert.ToString(Session["AddressLine2"]);
                }
                if (Session["City"] != null)
                {
                    objDeleteCustomerVM.DelCity = Convert.ToString(Session["City"]);
                }
                if (Session["State"] != null)
                {
                    objDeleteCustomerVM.DelState = Convert.ToString(Session["State"]);
                }
                if (Session["PostalCode"] != null)
                {
                    objDeleteCustomerVM.DelZip = Convert.ToString(Session["PostalCode"]);
                }

                objDeleteCustomerVM.RepCustId = objDeleteCustomerVM.objADClientCustomer.CustId;
                objDeleteCustomerVM.RepCustName = objDeleteCustomerVM.objADClientCustomer.ClientCustomer;
                objDeleteCustomerVM.RepRefNum = objDeleteCustomerVM.objADClientCustomer.RefNum;
                objDeleteCustomerVM.RepAddr1 = objDeleteCustomerVM.objADClientCustomer.AddressLine1;
                objDeleteCustomerVM.RepAddr2 = objDeleteCustomerVM.objADClientCustomer.AddressLine2;
                objDeleteCustomerVM.RepCity = objDeleteCustomerVM.objADClientCustomer.City;
                objDeleteCustomerVM.RepState = objDeleteCustomerVM.objADClientCustomer.State;
                objDeleteCustomerVM.RepZip = objDeleteCustomerVM.objADClientCustomer.PostalCode;

                if (objDeleteCustomerVM.DelCustId == objDeleteCustomerVM.RepCustId)
                {
                    objDeleteCustomerVM.Message = "Replacing customer cannot be the same deleting customer";
                    objDeleteCustomerVM.RepCustId = 0;
                    objDeleteCustomerVM.RepCustName = null;
                    objDeleteCustomerVM.RepRefNum = null;
                    objDeleteCustomerVM.RepAddr1 = null;
                    objDeleteCustomerVM.RepAddr2 = null;
                    objDeleteCustomerVM.RepCity = null;
                    objDeleteCustomerVM.RepState = null;
                    objDeleteCustomerVM.RepZip = null;
                }

            }
            ViewBag.lblRecCount = "";
            return PartialView(GetVirtualPath("_DeleteCustomer"), objDeleteCustomerVM);
        }



        //[HttpPost]
        //public ActionResult LoadDeleteCustomer(string Id, int Delvalue)
        //{
        //    DeleteCustomerVM objDeleteCustomerVM = null;

        //    if (Session["objDeleteCustomerVM"] == null)
        //    {
        //        objDeleteCustomerVM = new DeleteCustomerVM();
        //    }
        //    else
        //    {
        //        objDeleteCustomerVM = (DeleteCustomerVM)Session["objDeleteCustomerVM"];
        //    }



        //    if (Session["clientId"] != null)
        //    {
        //        objDeleteCustomerVM.ClientId = Convert.ToString(Session["clientId"]);
        //    }

        //    objDeleteCustomerVM.objADClientCustomer = ADClientCustomer.ReadClientCustomer(Convert.ToInt32(Id));

        //    if (Delvalue == 1)
        //    {
        //        objDeleteCustomerVM.DelCustId = objDeleteCustomerVM.objADClientCustomer.CustId;
        //        objDeleteCustomerVM.DelCustName = objDeleteCustomerVM.objADClientCustomer.ClientCustomer;
        //        objDeleteCustomerVM.DelRefNum = objDeleteCustomerVM.objADClientCustomer.RefNum;
        //        objDeleteCustomerVM.DelAddr1 = objDeleteCustomerVM.objADClientCustomer.AddressLine1;
        //        objDeleteCustomerVM.DelAddr2 = objDeleteCustomerVM.objADClientCustomer.AddressLine2;
        //        objDeleteCustomerVM.DelCity = objDeleteCustomerVM.objADClientCustomer.City;
        //        objDeleteCustomerVM.DelState = objDeleteCustomerVM.objADClientCustomer.State;
        //        objDeleteCustomerVM.DelZip = objDeleteCustomerVM.objADClientCustomer.PostalCode;

        //        Session["objDeleteCustomerVM"] = objDeleteCustomerVM;

        //    }

        //    else
        //    {

        //        objDeleteCustomerVM.RepCustId = objDeleteCustomerVM.objADClientCustomer.CustId;
        //        objDeleteCustomerVM.RepCustName = objDeleteCustomerVM.objADClientCustomer.ClientCustomer;
        //        objDeleteCustomerVM.RepRefNum = objDeleteCustomerVM.objADClientCustomer.RefNum;
        //        objDeleteCustomerVM.RepAddr1 = objDeleteCustomerVM.objADClientCustomer.AddressLine1;
        //        objDeleteCustomerVM.RepAddr2 = objDeleteCustomerVM.objADClientCustomer.AddressLine2;
        //        objDeleteCustomerVM.RepCity = objDeleteCustomerVM.objADClientCustomer.City;
        //        objDeleteCustomerVM.RepState = objDeleteCustomerVM.objADClientCustomer.State;
        //        objDeleteCustomerVM.RepZip = objDeleteCustomerVM.objADClientCustomer.PostalCode;

        //        if (objDeleteCustomerVM.DelCustId == objDeleteCustomerVM.RepCustId)
        //        {
        //            objDeleteCustomerVM.Message = "Replacing customer cannot be the same deleting customer";
        //            objDeleteCustomerVM.RepCustId = 0;
        //            objDeleteCustomerVM.RepCustName = null;
        //            objDeleteCustomerVM.RepRefNum = null;
        //            objDeleteCustomerVM.RepAddr1 = null;
        //            objDeleteCustomerVM.RepAddr2 = null;
        //            objDeleteCustomerVM.RepCity = null;
        //            objDeleteCustomerVM.RepState = null;
        //            objDeleteCustomerVM.RepZip = null;
        //        }
        //        Session["objDeleteCustomerVM"] = objDeleteCustomerVM;


        //    }
        //    ViewBag.lblRecCount = "";
        //    return PartialView(GetVirtualPath("_DeleteCustomer"), objDeleteCustomerVM);
        //}

        //Delete customer
        [HttpPost]
        public ActionResult DeleteCustomerDetails(DeleteCustomerVM objDeleteCustomerVM)
        {
            int result = 0;
            try
            {
                result = ADClientCustomer.GetDetails_DeleteCustomer(objDeleteCustomerVM);
            
                return Json(result);
            }
            catch
            {
                result = -1;
                return Json(result);
            }

        }



        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/Managements/DeleteCustomer/" + ViewName + ".cshtml";

            return path;
        }

    }
}
