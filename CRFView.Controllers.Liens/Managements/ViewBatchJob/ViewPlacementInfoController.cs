﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRFView.Adapters.Liens.Managements.ViewBatchJob.ViewModels;
using System.Text;
using System.Data;
using CRFView.Adapters;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.Managements.ViewBatchJob
{
    public class ViewPlacementInfoController : Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            ViewPlacementInfoVM objViewPlacementInfoVM = new ViewPlacementInfoVM();
            return PartialView(GetVirtualPath("_ViewPlacementInfo"), objViewPlacementInfoVM);
        }

        [HttpPost]
        public ActionResult GetViewPlacementInfoByJobId(string JobId)
        {
            ViewPlacementInfoVM objViewPlacementInfoVM = new ViewPlacementInfoVM();
            objViewPlacementInfoVM.ByJobId = JobId;

            DataTable dtViewPlacementInfoList = ADvwJobPlacementInfo.GetViewPlacementInfoList(JobId);

            if (dtViewPlacementInfoList.Rows.Count > 0)
            {
                objViewPlacementInfoVM.Message = "Success";


                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.JobId = ((dtViewPlacementInfoList.Rows[0]["JobId"] != DBNull.Value ? Convert.ToInt32(dtViewPlacementInfoList.Rows[0]["JobId"]) : 0));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.ClientCode = ((dtViewPlacementInfoList.Rows[0]["ClientCode"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["ClientCode"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.SubmittedOn = ((dtViewPlacementInfoList.Rows[0]["SubmittedOn"] != DBNull.Value ? Convert.ToDateTime(dtViewPlacementInfoList.Rows[0]["SubmittedOn"]) : DateTime.MinValue));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.JobNum = ((dtViewPlacementInfoList.Rows[0]["JobNum"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["JobNum"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.JobName = ((dtViewPlacementInfoList.Rows[0]["JobName"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["JobName"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.JobAdd1 = ((dtViewPlacementInfoList.Rows[0]["JobAdd1"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["JobAdd1"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.JobAdd2 = ((dtViewPlacementInfoList.Rows[0]["JobAdd2"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["JobAdd2"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.JobCity = ((dtViewPlacementInfoList.Rows[0]["JobCity"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["JobCity"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.JobState = ((dtViewPlacementInfoList.Rows[0]["JobState"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["JobState"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.JobZip = ((dtViewPlacementInfoList.Rows[0]["JobZip"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["JobZip"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.RushOrder = ((dtViewPlacementInfoList.Rows[0]["RushOrder"] != DBNull.Value ? Convert.ToBoolean(dtViewPlacementInfoList.Rows[0]["RushOrder"]) : false));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.RushOrderML = ((dtViewPlacementInfoList.Rows[0]["RushOrderML"] != DBNull.Value ? Convert.ToBoolean(dtViewPlacementInfoList.Rows[0]["RushOrderML"]) : false));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.BatchId = ((dtViewPlacementInfoList.Rows[0]["BatchId"] != DBNull.Value ? Convert.ToInt32(dtViewPlacementInfoList.Rows[0]["BatchId"]) : 0));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.BatchJobId = ((dtViewPlacementInfoList.Rows[0]["BatchJobId"] != DBNull.Value ? Convert.ToInt32(dtViewPlacementInfoList.Rows[0]["BatchJobId"]) : 0));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.PlacementSource = ((dtViewPlacementInfoList.Rows[0]["PlacementSource"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["PlacementSource"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.SubmittedBy = ((dtViewPlacementInfoList.Rows[0]["SubmittedBy"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["SubmittedBy"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.SameAsCust = ((dtViewPlacementInfoList.Rows[0]["SameAsCust"] != DBNull.Value ? Convert.ToBoolean(dtViewPlacementInfoList.Rows[0]["SameAsCust"]) : false));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.OwnerSourceGC = ((dtViewPlacementInfoList.Rows[0]["OwnerSourceGC"] != DBNull.Value ? Convert.ToBoolean(dtViewPlacementInfoList.Rows[0]["OwnerSourceGC"]) : false));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.GCName = ((dtViewPlacementInfoList.Rows[0]["GCName"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["GCName"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.GCContactName = ((dtViewPlacementInfoList.Rows[0]["GCContactName"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["GCContactName"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.GCAdd1 = ((dtViewPlacementInfoList.Rows[0]["GCAdd1"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["GCAdd1"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.GCAdd2 = ((dtViewPlacementInfoList.Rows[0]["GCAdd2"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["GCAdd2"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.GCCity = ((dtViewPlacementInfoList.Rows[0]["GCCity"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["GCCity"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.GCState = ((dtViewPlacementInfoList.Rows[0]["GCState"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["GCState"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.GCZip = ((dtViewPlacementInfoList.Rows[0]["GCZip"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["GCZip"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.GCPhone1 = ((dtViewPlacementInfoList.Rows[0]["GCPhone1"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["GCPhone1"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.GCFax = ((dtViewPlacementInfoList.Rows[0]["GCFax"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["GCFax"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.GCRefNum = ((dtViewPlacementInfoList.Rows[0]["GCRefNum"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["GCRefNum"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.GCPhone2 = ((dtViewPlacementInfoList.Rows[0]["GCPhone2"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["GCPhone2"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.OwnrName = ((dtViewPlacementInfoList.Rows[0]["OwnrName"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["OwnrName"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.OwnrAdd1 = ((dtViewPlacementInfoList.Rows[0]["OwnrAdd1"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["OwnrAdd1"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.OwnrAdd2 = ((dtViewPlacementInfoList.Rows[0]["OwnrAdd2"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["OwnrAdd2"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.OwnrCity = ((dtViewPlacementInfoList.Rows[0]["OwnrCity"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["OwnrCity"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.OwnrState = ((dtViewPlacementInfoList.Rows[0]["OwnrState"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["OwnrState"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.OwnrZip = ((dtViewPlacementInfoList.Rows[0]["OwnrZip"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["OwnrZip"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.OwnrPhone1 = ((dtViewPlacementInfoList.Rows[0]["OwnrPhone1"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["OwnrPhone1"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.OwnrPhone2 = ((dtViewPlacementInfoList.Rows[0]["OwnrPhone2"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["OwnrPhone2"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.OwnrFax = ((dtViewPlacementInfoList.Rows[0]["OwnrFax"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["OwnrFax"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.LenderName = ((dtViewPlacementInfoList.Rows[0]["LenderName"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["LenderName"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.LenderAdd1 = ((dtViewPlacementInfoList.Rows[0]["LenderAdd1"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["LenderAdd1"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.LenderAdd2 = ((dtViewPlacementInfoList.Rows[0]["LenderAdd2"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["LenderAdd2"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.LenderCity = ((dtViewPlacementInfoList.Rows[0]["LenderCity"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["LenderCity"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.LenderState = ((dtViewPlacementInfoList.Rows[0]["LenderState"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["LenderState"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.LenderZip = ((dtViewPlacementInfoList.Rows[0]["LenderZip"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["LenderZip"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.LenderPhone1 = ((dtViewPlacementInfoList.Rows[0]["LenderPhone1"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["LenderPhone1"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.LenderPhone2 = ((dtViewPlacementInfoList.Rows[0]["LenderPhone2"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["LenderPhone2"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.LenderFax = ((dtViewPlacementInfoList.Rows[0]["LenderFax"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["LenderFax"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.BondNum = ((dtViewPlacementInfoList.Rows[0]["BondNum"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["BondNum"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.SameAsGC = ((dtViewPlacementInfoList.Rows[0]["SameAsGC"] != DBNull.Value ? Convert.ToBoolean(dtViewPlacementInfoList.Rows[0]["SameAsGC"]) : false));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.IsError = ((dtViewPlacementInfoList.Rows[0]["IsError"] != DBNull.Value ? Convert.ToBoolean(dtViewPlacementInfoList.Rows[0]["IsError"]) : false));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.ErrorDescription = ((dtViewPlacementInfoList.Rows[0]["ErrorDescription"] != DBNull.Value ? dtViewPlacementInfoList.Rows[0]["ErrorDescription"].ToString() : ""));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.SubmittedByUserId = ((dtViewPlacementInfoList.Rows[0]["SubmittedByUserId"] != DBNull.Value ? Convert.ToInt32(dtViewPlacementInfoList.Rows[0]["SubmittedByUserId"]) : 0));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.JobActionId = ((dtViewPlacementInfoList.Rows[0]["JobActionId"] != DBNull.Value ? Convert.ToInt32(dtViewPlacementInfoList.Rows[0]["JobActionId"]) : 0));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.SuretyBox = ((dtViewPlacementInfoList.Rows[0]["SuretyBox"] != DBNull.Value ? Convert.ToBoolean(dtViewPlacementInfoList.Rows[0]["SuretyBox"]) : false));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.MLAgent = ((dtViewPlacementInfoList.Rows[0]["MLAgent"] != DBNull.Value ? Convert.ToBoolean(dtViewPlacementInfoList.Rows[0]["MLAgent"]) : false));
                objViewPlacementInfoVM.ObjADvwJobPlacementInfo.Id = ((dtViewPlacementInfoList.Rows[0]["Id"] != DBNull.Value ? Convert.ToInt32(dtViewPlacementInfoList.Rows[0]["Id"]) : 0));

                objViewPlacementInfoVM.mybatchjobid = ((dtViewPlacementInfoList.Rows[0]["BatchJobId"] != DBNull.Value ? Convert.ToInt32(dtViewPlacementInfoList.Rows[0]["BatchJobId"]) : 0));


                //objDeleteCustomerVM.dtCustomerLoupList = ADClientCustomer.GetCustomerLookUpDt(objDeleteCustomerVM);
                //objDeleteCustomerVM.ClientCustomerList = ADClientCustomer.GetCustomerLookUpList(objDeleteCustomerVM.dtCustomerLoupList);

                DataTable dtBatchJobLegalPartiesList = ADBatchJobLegalParties.BatchJobLegalPartiesList(Convert.ToString(objViewPlacementInfoVM.ObjADvwJobPlacementInfo.Id));
                if (dtBatchJobLegalPartiesList.Rows.Count > 0)
                {
                    objViewPlacementInfoVM.BatchJobLegalPartiesList = ADBatchJobLegalParties.BatchJobLegalPartiesList(dtBatchJobLegalPartiesList);
                }


                DataTable dtJobActionsList = ADvwJobPlacementInfo.JobActionsList(Convert.ToString(objViewPlacementInfoVM.ObjADvwJobPlacementInfo.JobActionId));
                if (dtJobActionsList.Rows.Count > 0)
                {
                    objViewPlacementInfoVM.JobType = ((dtJobActionsList.Rows[0]["Description"] != DBNull.Value ? dtJobActionsList.Rows[0]["Description"].ToString() : ""));
                }




            }
            else
            {
                objViewPlacementInfoVM.Message = "Job Not Found, Try Again.";
            }

            return PartialView(GetVirtualPath("_ViewPlacementInfo"), objViewPlacementInfoVM);
        }


        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/Managements/ViewBatchJob/" + ViewName + ".cshtml";
            return path;
        }


    }
}
