﻿using CRF.BLL.CRFDB.TABLES;
using CRFView.Adapters;
using CRFView.Adapters.Liens.Managements.ChangeJobInfo.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.Managements.ChangeJobInfo
{
    public class ChangeJobInfoController : Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            ChangeJobInfoVM ObjChangeJobInfoVM = new ChangeJobInfoVM();
            return PartialView(GetVirtualPath("_ChangeJobInfo"), ObjChangeJobInfoVM);
        }

        //Get ChangeJobInfoDetails By JobId
        /// <summary>
        /// 
        /// </summary>
        /// <param name="JobId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetChangeJobInfoByJobId(string JobId)
        {
            ChangeJobInfoVM objChangeJobInfoVM = new ChangeJobInfoVM();
            objChangeJobInfoVM.JobId = JobId;

            DataTable dtChangeJobInfoList = ADJob.GetChangeJobInfoList(JobId);

            if (dtChangeJobInfoList.Rows.Count > 0)
            {
                objChangeJobInfoVM.Message = "Success";



                objChangeJobInfoVM.objvwJobsList.ClientCode = ((dtChangeJobInfoList.Rows[0]["ClientCode"] != DBNull.Value ? dtChangeJobInfoList.Rows[0]["ClientCode"].ToString() : ""));
                objChangeJobInfoVM.objvwJobsList.ClientName = ((dtChangeJobInfoList.Rows[0]["ClientName"] != DBNull.Value ? dtChangeJobInfoList.Rows[0]["ClientName"].ToString() : ""));
                objChangeJobInfoVM.objvwJobsList.JobId = ((dtChangeJobInfoList.Rows[0]["JobId"] != DBNull.Value ? Convert.ToInt32(dtChangeJobInfoList.Rows[0]["JobId"]) : 0));
                objChangeJobInfoVM.objvwJobsList.JobNum = ((dtChangeJobInfoList.Rows[0]["JobNum"] != DBNull.Value ? dtChangeJobInfoList.Rows[0]["JobNum"].ToString() : ""));
                //objChangeJobInfoVM.objvwJobsList.EstBalance = ((dtChangeJobInfoList.Rows[0]["EstBalance"] != DBNull.Value ? Convert.ToDecimal(dtChangeJobInfoList.Rows[0]["EstBalance"]) : 0));
                objChangeJobInfoVM.objvwJobsList.DateAssigned = ((dtChangeJobInfoList.Rows[0]["DateAssigned"] != DBNull.Value ? Convert.ToDateTime(dtChangeJobInfoList.Rows[0]["DateAssigned"]) : DateTime.MinValue));
                objChangeJobInfoVM.objvwJobsList.JobName = ((dtChangeJobInfoList.Rows[0]["JobName"] != DBNull.Value ? dtChangeJobInfoList.Rows[0]["JobName"].ToString() : ""));
                objChangeJobInfoVM.objvwJobsList.DeskNum = ((dtChangeJobInfoList.Rows[0]["DeskNum"] != DBNull.Value ? dtChangeJobInfoList.Rows[0]["DeskNum"].ToString() : ""));
                objChangeJobInfoVM.objvwJobsList.RevDate = ((dtChangeJobInfoList.Rows[0]["RevDate"] != DBNull.Value ? Convert.ToDateTime(dtChangeJobInfoList.Rows[0]["RevDate"]) : DateTime.MinValue));
                objChangeJobInfoVM.objvwJobsList.JobAdd1 = ((dtChangeJobInfoList.Rows[0]["JobAdd1"] != DBNull.Value ? dtChangeJobInfoList.Rows[0]["JobAdd1"].ToString() : ""));
                objChangeJobInfoVM.objvwJobsList.NoticeSent = ((dtChangeJobInfoList.Rows[0]["NoticeSent"] != DBNull.Value ? Convert.ToDateTime(dtChangeJobInfoList.Rows[0]["NoticeSent"]) : DateTime.MinValue));
                objChangeJobInfoVM.objvwJobsList.JobAdd2 = ((dtChangeJobInfoList.Rows[0]["JobAdd2"] != DBNull.Value ? dtChangeJobInfoList.Rows[0]["JobAdd2"].ToString() : ""));
                objChangeJobInfoVM.objvwJobsList.EndDate = ((dtChangeJobInfoList.Rows[0]["EndDate"] != DBNull.Value ? Convert.ToDateTime(dtChangeJobInfoList.Rows[0]["EndDate"]) : DateTime.MinValue));
                objChangeJobInfoVM.objvwJobsList.JobCity = ((dtChangeJobInfoList.Rows[0]["JobCity"] != DBNull.Value ? dtChangeJobInfoList.Rows[0]["JobCity"].ToString() : ""));
                objChangeJobInfoVM.objvwJobsList.JobState = ((dtChangeJobInfoList.Rows[0]["JobState"] != DBNull.Value ? dtChangeJobInfoList.Rows[0]["JobState"].ToString() : ""));
                objChangeJobInfoVM.objvwJobsList.JobZip = ((dtChangeJobInfoList.Rows[0]["JobZip"] != DBNull.Value ? dtChangeJobInfoList.Rows[0]["JobZip"].ToString() : ""));
                objChangeJobInfoVM.objvwJobsList.StatusCode = ((dtChangeJobInfoList.Rows[0]["StatusCode"] != DBNull.Value ? dtChangeJobInfoList.Rows[0]["StatusCode"].ToString() : ""));
                objChangeJobInfoVM.objvwJobsList.StartDate = ((dtChangeJobInfoList.Rows[0]["StartDate"] != DBNull.Value ? Convert.ToDateTime(dtChangeJobInfoList.Rows[0]["StartDate"]) : DateTime.MinValue));
                objChangeJobInfoVM.objvwJobsList.VerifiedDate = ((dtChangeJobInfoList.Rows[0]["VerifiedDate"] != DBNull.Value ? Convert.ToDateTime(dtChangeJobInfoList.Rows[0]["VerifiedDate"]) : DateTime.MinValue));
               
                objChangeJobInfoVM.objvwJobsList.OwnerName = ((dtChangeJobInfoList.Rows[0]["OwnerName"] != DBNull.Value ? dtChangeJobInfoList.Rows[0]["OwnerName"].ToString() : ""));
                objChangeJobInfoVM.objvwJobsList.LenderName = ((dtChangeJobInfoList.Rows[0]["LenderName"] != DBNull.Value ? dtChangeJobInfoList.Rows[0]["LenderName"].ToString() : ""));
                objChangeJobInfoVM.objvwJobsList.BranchNumber = ((dtChangeJobInfoList.Rows[0]["BranchNumber"] != DBNull.Value ? dtChangeJobInfoList.Rows[0]["BranchNumber"].ToString() : ""));
                objChangeJobInfoVM.objvwJobsList.JointCk = ((dtChangeJobInfoList.Rows[0]["JointCk"] != DBNull.Value ? Convert.ToBoolean(dtChangeJobInfoList.Rows[0]["JointCk"]) : false));
                objChangeJobInfoVM.objvwJobsList.GreenCard = ((dtChangeJobInfoList.Rows[0]["GreenCard"] != DBNull.Value ? Convert.ToBoolean(dtChangeJobInfoList.Rows[0]["GreenCard"]) : false));
                objChangeJobInfoVM.objvwJobsList.IsJobView = ((dtChangeJobInfoList.Rows[0]["IsJobView"] != DBNull.Value ? Convert.ToBoolean(dtChangeJobInfoList.Rows[0]["IsJobView"]) : false));
                objChangeJobInfoVM.objvwJobsList.NCInfo = ((dtChangeJobInfoList.Rows[0]["NCInfo"] != DBNull.Value ? Convert.ToBoolean(dtChangeJobInfoList.Rows[0]["NCInfo"]) : false));


                objChangeJobInfoVM.chkGreencard = objChangeJobInfoVM.objvwJobsList.GreenCard;
                objChangeJobInfoVM.chkJointCk = objChangeJobInfoVM.objvwJobsList.JointCk;
                objChangeJobInfoVM.chkIsJobview = objChangeJobInfoVM.objvwJobsList.IsJobView;
                objChangeJobInfoVM.myPrivateJob = objChangeJobInfoVM.objvwJobsList.PrivateJob;
                objChangeJobInfoVM.myResidentialBox = objChangeJobInfoVM.objvwJobsList.ResidentialBox;
                objChangeJobInfoVM.myPublicBox = objChangeJobInfoVM.objvwJobsList.PublicJob;

            }
            else
            {
                objChangeJobInfoVM.Message = "Requested Job is not found";
            }



            return PartialView(GetVirtualPath("_ChangeJobInfo"), objChangeJobInfoVM);
        }


        //Update ChangeJobInfo
        [HttpPost]
        public ActionResult UpdateChangeJobInfo(ChangeJobInfoVM objChangeJobInfoVM)
        {
            int result = 0;
            string JobId = objChangeJobInfoVM.JobId;
            string JobState = objChangeJobInfoVM.objvwJobsList.JobState;
            try
            {
         
                DataTable dtChangeStateInfo = ADStateInfo.GetChangeRead(JobState);

                if ((objChangeJobInfoVM.chkGreencard == false) && (objChangeJobInfoVM.myPrivateJob == true || objChangeJobInfoVM.myResidentialBox == true) && (Convert.ToBoolean(dtChangeStateInfo.Rows[0]["ReturnReceiptPrivate"]) == true))
                {
                    objChangeJobInfoVM.Message = "The state requires return receipt on this job";
                }
                else if ((objChangeJobInfoVM.chkGreencard == false) && (objChangeJobInfoVM.myPublicBox == true) && (Convert.ToBoolean(dtChangeStateInfo.Rows[0]["ReturnReceiptPublic"]) == true))
                {
                    objChangeJobInfoVM.Message = "The state requires return receipt on this job";
                }

                //Client objClient = new Client();
                //Job objJob = new Job();
                objChangeJobInfoVM.objADJob = ADJob.ReadJob(Convert.ToInt32(JobId));

                result = ADJob.UpdateJob(objChangeJobInfoVM);

                if(objChangeJobInfoVM.chkDeleteNoticeHistory == true)
                {
                    result = ADJob.SaveJob_ChangeJobInfo_1(objChangeJobInfoVM);
                }


                objChangeJobInfoVM.objADClient = ADClient.GetClientDetails(Convert.ToInt32(JobId));
                //DBO.Read(myjob.ClientId, myClient)

                if(objChangeJobInfoVM.objADJob.IsJobView == true && objChangeJobInfoVM.objADClient.NoRentalInfo == false)
                {
                    result = ADJob.SaveJob_ChangeJobInfo2(JobId);
                }
                else
                {
                    result = ADJob.SaveJob_ChangeJobInfo_2ElseCon(JobId);
                }

                //if (objJob.IsJobView == true && objClient.NoRentalInfo == false)
                //{
                //    result = ADJob.SaveJob_ChangeJobInfo2(JobId);
                //}
                //else
                //{
                //    result = ADJob.SaveJob_ChangeJobInfo_2ElseCon(JobId);
                //}

                objChangeJobInfoVM.objADJob = ADJob.ReadJob(Convert.ToInt32(JobId));

            
                return Json(result);
            }
            catch
            {
                result = -1;
                return Json(result);
            }

        }


        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/Managements/ChangeJobInfo/" + ViewName + ".cshtml";

            return path;
        }

    }
}
