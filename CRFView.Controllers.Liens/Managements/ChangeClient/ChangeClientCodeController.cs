﻿using System;
using CRFView.Adapters;
using CRFView.Adapters.Liens.Managements.ChangeClient.ViewModels;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Web.Mvc;
using System.Threading.Tasks;
using CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels;
using CRF.BLL.CRFDB.TABLES;
using CRFView.Adapters.Liens.Verifiers.WorkCard;

namespace CRFView.Controllers.Liens.Managements.ChangeClient
{
    public class ChangeClientCodeController : Controller

    {
        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            ChangeClientCodeVM objChangeClientCodeVM = new ChangeClientCodeVM();
            return PartialView(GetVirtualPath("_ChangeClientCode"), objChangeClientCodeVM);
        }


        public string getdetails(string JobId)
        {
            ChangeClientCodeVM objChangeClientCodeVM = new ChangeClientCodeVM();

            var Obj = ADJob.ReadJob(Convert.ToInt32(JobId));


            DateTime mynoticesent = Obj.NoticeSent;
            DateTime mydateassigned = Obj.DateAssigned;
            var Diff = ((TimeSpan)(mynoticesent - mydateassigned)).Days;
            if (mynoticesent >= mydateassigned && Diff > 30)
            {
                objChangeClientCodeVM.MessageDiff = "Warning, a Notice has been Sent";
                return objChangeClientCodeVM.MessageDiff;
            }

            return "";
        }


        //Get clientCodeJobInfoDetails By JobId
        /// <summary>
        /// 
        /// </summary>
        /// <param name="JobId"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult GetLienJobInfoByJobId(string JobId)
        {
            ChangeClientCodeVM objChangeClientCodeVM = new ChangeClientCodeVM();
            objChangeClientCodeVM.JobId = JobId;

            DataTable dtvwJobInfoList = ADJob.GetvwJobInfoList(JobId);

            if (dtvwJobInfoList.Rows.Count > 0)
            {
                objChangeClientCodeVM.Message = "Success";
                


                objChangeClientCodeVM.objvwJobsList.ClientCode = ((dtvwJobInfoList.Rows[0]["ClientCode"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["ClientCode"].ToString() : ""));
                objChangeClientCodeVM.objvwJobsList.ClientName = ((dtvwJobInfoList.Rows[0]["ClientName"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["ClientName"].ToString() : ""));
                objChangeClientCodeVM.objvwJobsList.JobId= ((dtvwJobInfoList.Rows[0]["JobId"] != DBNull.Value ? Convert.ToInt32(dtvwJobInfoList.Rows[0]["JobId"]) : 0));
                objChangeClientCodeVM.objvwJobsList.JobNum = ((dtvwJobInfoList.Rows[0]["JobNum"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobNum"].ToString() : ""));
                objChangeClientCodeVM.objvwJobsList.EstBalance = ((dtvwJobInfoList.Rows[0]["EstBalance"] != DBNull.Value ? Convert.ToDecimal(dtvwJobInfoList.Rows[0]["EstBalance"]) : 0));
                objChangeClientCodeVM.objvwJobsList.DateAssigned = ((dtvwJobInfoList.Rows[0]["DateAssigned"] != DBNull.Value ? Convert.ToDateTime(dtvwJobInfoList.Rows[0]["DateAssigned"]) : DateTime.MinValue));
                objChangeClientCodeVM.objvwJobsList.JobName = ((dtvwJobInfoList.Rows[0]["JobName"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobName"].ToString() : ""));
                objChangeClientCodeVM.objvwJobsList.DeskNum = ((dtvwJobInfoList.Rows[0]["DeskNum"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["DeskNum"].ToString() : ""));
                objChangeClientCodeVM.objvwJobsList.RevDate = ((dtvwJobInfoList.Rows[0]["RevDate"] != DBNull.Value ? Convert.ToDateTime(dtvwJobInfoList.Rows[0]["RevDate"]) : DateTime.MinValue));
                objChangeClientCodeVM.objvwJobsList.JobAdd1 = ((dtvwJobInfoList.Rows[0]["JobAdd1"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobAdd1"].ToString() : ""));
                objChangeClientCodeVM.objvwJobsList.NoticeSent = ((dtvwJobInfoList.Rows[0]["NoticeSent"] != DBNull.Value ? Convert.ToDateTime(dtvwJobInfoList.Rows[0]["NoticeSent"]) : DateTime.MinValue));
                objChangeClientCodeVM.objvwJobsList.JobAdd2 = ((dtvwJobInfoList.Rows[0]["JobAdd2"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobAdd2"].ToString() : ""));
                objChangeClientCodeVM.objvwJobsList.EndDate = ((dtvwJobInfoList.Rows[0]["EndDate"] != DBNull.Value ? Convert.ToDateTime(dtvwJobInfoList.Rows[0]["EndDate"]) : DateTime.MinValue));
                objChangeClientCodeVM.objvwJobsList.JobCity = ((dtvwJobInfoList.Rows[0]["JobCity"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobCity"].ToString() : ""));
                objChangeClientCodeVM.objvwJobsList.JobState = ((dtvwJobInfoList.Rows[0]["JobState"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobState"].ToString() : ""));
                objChangeClientCodeVM.objvwJobsList.JobZip = ((dtvwJobInfoList.Rows[0]["JobZip"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobZip"].ToString() : ""));
                objChangeClientCodeVM.objvwJobsList.StatusCode = ((dtvwJobInfoList.Rows[0]["StatusCode"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["StatusCode"].ToString() : ""));
                objChangeClientCodeVM.objvwJobsList.RushOrder = ((dtvwJobInfoList.Rows[0]["RushOrder"] != DBNull.Value ? Convert.ToBoolean(dtvwJobInfoList.Rows[0]["RushOrder"]) : false));
                objChangeClientCodeVM.objvwJobsList.PrivateJob = ((dtvwJobInfoList.Rows[0]["PrivateJob"] != DBNull.Value ? Convert.ToBoolean(dtvwJobInfoList.Rows[0]["PrivateJob"]) : false));
                objChangeClientCodeVM.objvwJobsList.PublicJob = ((dtvwJobInfoList.Rows[0]["PublicJob"] != DBNull.Value ? Convert.ToBoolean(dtvwJobInfoList.Rows[0]["PublicJob"]) : false));
                objChangeClientCodeVM.objvwJobsList.ResidentialBox = ((dtvwJobInfoList.Rows[0]["ResidentialBox"] != DBNull.Value ? Convert.ToBoolean(dtvwJobInfoList.Rows[0]["ResidentialBox"]) : false));
                objChangeClientCodeVM.objvwJobsList.FederalJob = ((dtvwJobInfoList.Rows[0]["FederalJob"] != DBNull.Value ? Convert.ToBoolean(dtvwJobInfoList.Rows[0]["FederalJob"]) : false));
                objChangeClientCodeVM.objvwJobsList.CustName = ((dtvwJobInfoList.Rows[0]["CustName"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["CustName"].ToString() : ""));
                objChangeClientCodeVM.objvwJobsList.CustPhNUm = ((dtvwJobInfoList.Rows[0]["CustPhNUm"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["CustPhNUm"].ToString() : ""));
                objChangeClientCodeVM.objvwJobsList.CustFaxNum = ((dtvwJobInfoList.Rows[0]["CustFaxNum"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["CustFaxNum"].ToString() : ""));
                objChangeClientCodeVM.objvwJobsList.GCName = ((dtvwJobInfoList.Rows[0]["GCName"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["GCName"].ToString() : ""));
                objChangeClientCodeVM.objvwJobsList.GCPhNum = ((dtvwJobInfoList.Rows[0]["GCPhNum"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["GCPhNum"].ToString() : ""));
                objChangeClientCodeVM.objvwJobsList.GCFaxNum = ((dtvwJobInfoList.Rows[0]["GCFaxNum"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["GCFaxNum"].ToString() : ""));
                objChangeClientCodeVM.objvwJobsList.OwnerName = ((dtvwJobInfoList.Rows[0]["OwnerName"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["OwnerName"].ToString() : ""));
            }
            else
            {
                objChangeClientCodeVM.Message = "Requested Job is not found";
            }

            return PartialView(GetVirtualPath("_ChangeClientCode"), objChangeClientCodeVM);
        }



       //Update ChangeLienJobInfo Info
        [HttpPost]
        public ActionResult SaveChangeClientCodeInfo(ChangeClientCodeVM ObjUpdateChangeClientCodeVM)
        {
            ADJob.UpdateChangeClientCodeInfo(ObjUpdateChangeClientCodeVM);
            ChangeClientCodeVM ObjChange = new ChangeClientCodeVM();


            return PartialView(GetVirtualPath("_ChangeClientCode"), ObjChange);
        }



        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/Managements/ChangeClient/" + ViewName + ".cshtml";

            return path;
        }

    }
}
