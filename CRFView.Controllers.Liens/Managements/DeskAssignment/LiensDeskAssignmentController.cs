﻿using CRFView.Adapters;
using CRFView.Adapters.Collections.Management.DeskAssignment.ViewModels;
using CRFView.Adapters.Liens.Managements.DeskAssignment;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.Managements.DeskAssignment
{
    public class LiensDeskAssignmentController:Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));

        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            ADJob objADJob = new ADJob();
            DeskAssignmentVM objDeskAssignmentVM = new DeskAssignmentVM();

            ViewBag.lblRecCount = "";

           return PartialView(GetVirtualPath("_DeskAssignment"), objDeskAssignmentVM);
        }
        //Get DeskAssignment List
        [HttpPost]
        public ActionResult LiensDeskAssignmentList(DeskAssignmentVM objDeskAssignmentVM)
         {
           
            objDeskAssignmentVM.dtDeskAssignmentList = ADVWLienReportsInfo.GetAccountDetailDt(objDeskAssignmentVM);

            objDeskAssignmentVM.LiensDeskAssignmentList = ADVWLienReportsInfo.GetDetailList(objDeskAssignmentVM.dtDeskAssignmentList);

            ViewBag.lblRecCount = "Total  (" + objDeskAssignmentVM.LiensDeskAssignmentList.Count + ") Accounts Selected";

            return PartialView(GetVirtualPath("_DeskAssignment"), objDeskAssignmentVM);
        }
        //Get DeskAssignment Details for Edit/Update
        [HttpPost]
        public ActionResult GetDeskAssignmentDetails(string JobId)
        {
            ADJob objADJob = new ADJob();
            DeskAssignmentVM objDeskAssignmentVM = new DeskAssignmentVM();
            objDeskAssignmentVM.objADJob = ADJob.ReadJob(Convert.ToInt32(JobId));
            objDeskAssignmentVM.JobId = JobId;
            objDeskAssignmentVM.ddlLiensNewDeskId = Convert.ToString(objDeskAssignmentVM.objADJob.DeskNumId);
            objDeskAssignmentVM.ddlLiensNewActionId = Convert.ToString(objDeskAssignmentVM.objADJob.StatusCodeId);

            return PartialView(GetVirtualPath("_EditDeskAssignment"), objDeskAssignmentVM);
        }


        //Update DeskAssignment Details
        [HttpPost]
        public ActionResult UpdateDeskAssignment(DeskAssignmentVM objDeskAssignmentVM)

        {
            ADJob objADJob = new ADJob();
            int result = 1;

            if (objDeskAssignmentVM.RbtNewOption == "New Desk" && Convert.ToInt32(objDeskAssignmentVM.ddlLiensNewDeskId) > 0)
            {
                objDeskAssignmentVM.JobId = Convert.ToString(objDeskAssignmentVM.JobId);
                objDeskAssignmentVM.objADJob.DeskNumId = Convert.ToInt32(objDeskAssignmentVM.ddlLiensNewDeskId);
                result = ADJob.SaveDebtAccount_DeskAssignment_Desk(objDeskAssignmentVM);
            }
            else if (objDeskAssignmentVM.RbtNewOption == "New Action" && Convert.ToInt32(objDeskAssignmentVM.ddlLiensNewActionId) > 0)
            {
                objDeskAssignmentVM.JobId= Convert.ToString(objDeskAssignmentVM.JobId);
                objDeskAssignmentVM.objADJob.StatusCodeId = Convert.ToInt32(objDeskAssignmentVM.ddlLiensNewActionId);
                result = ADJob.SaveDebtAccount_DeskAssignment_Action(objDeskAssignmentVM);
            }

            //Obj_LegalCostVM.objDebtAccountLegalCostEdit.DebtAccountId = Convert.ToInt32(Obj_LegalCostVM.DebtAccountId);
            //Obj_LegalCostVM.objDebtAccountLegalCostEdit.Id = Convert.ToInt32(Obj_LegalCostVM.Id);
            //result = ADDebtAccountLegalCost.SaveDebtAccountLegalCost(Obj_LegalCostVM.objDebtAccountLegalCostEdit);

            return Json(result);
        }

        //Load DeskAssignment Modal
        [HttpPost]
        public ActionResult DeskAssignmentLoad(DeskAssignmentVM objDeskAssignmentVM)
        {

            ViewBag.lblRecCount = "";
            return PartialView(GetVirtualPath("_DeskAssignment"), objDeskAssignmentVM);
        }

        //Get VirtualPath for view
        public string GetVirtualPath(String ViewName)
        {
            string path = "~/Views/CRFViewLiens/Managements/DeskAssignment/" + ViewName + ".cshtml";
            return path;
        }
    }
}
