﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CRFView.Adapters;
using CRFView.Adapters.Liens.Managements.ChangeCustInfo.ViewModels;

namespace CRFView.Controllers.Liens.Managements.ChangeCustInfo
{
    public class ChangeCustInfoController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));

        }
        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {

            ChangeCustInfoVM objChangeCustInfoVM = new ChangeCustInfoVM();
            ViewBag.lblRecCount = "";

            return PartialView(GetVirtualPath("_ChangeCustInfo"), objChangeCustInfoVM);
        }

        [HttpPost]
        public ActionResult LoadChangeCustLookup(string ClientCode)
        {

            ChangeCustInfoVM objChangeCustInfoVM = new ChangeCustInfoVM();
            objChangeCustInfoVM.ClientCode = Convert.ToInt32(ClientCode);

            Session["clientId"] = objChangeCustInfoVM.ClientCode;

            ViewBag.lblRecCount = "";
            return PartialView(GetVirtualPath("_ChangeCustInfoLookup"), objChangeCustInfoVM);
        }

        [HttpPost]
        public ActionResult GetCustomerLookUpList(ChangeCustInfoVM objChangeCustInfoVM)
        {
            objChangeCustInfoVM.dtChangeCustInfoList = ADClientCustomer.GetCustomerLookUpDt(objChangeCustInfoVM);
            objChangeCustInfoVM.ClientCustomerList = ADClientCustomer.GetChangecUstInfoLookUpList(objChangeCustInfoVM.dtChangeCustInfoList);

         
            ViewBag.lblRecCount = "Total  (" + objChangeCustInfoVM.ClientCustomerList.Count + ") Records";

            return PartialView(GetVirtualPath("_ChangeCustInfoLookup"), objChangeCustInfoVM);
        }


        [HttpPost]
        public ActionResult LoadChangeCustInfo(string Id)
        {
            ChangeCustInfoVM objChangeCustInfoVM = new ChangeCustInfoVM();
            

            if (Session["clientId"] != null)
            {
                objChangeCustInfoVM.ClientCode = Convert.ToInt32(Session["clientId"]);
            }

            objChangeCustInfoVM.ObjChangeCustInfo = ADClientCustomer.ReadClientCustomer(Convert.ToInt32(Id));

            objChangeCustInfoVM.ObjChangeCustInfo.CustId = (Convert.ToInt32(Id));
            objChangeCustInfoVM.ObjChangeCustInfo.ClientCustomer = objChangeCustInfoVM.ObjChangeCustInfo.ClientCustomer;
            objChangeCustInfoVM.ObjChangeCustInfo.RefNum = objChangeCustInfoVM.ObjChangeCustInfo.RefNum;
            objChangeCustInfoVM.ObjChangeCustInfo.ContactName = objChangeCustInfoVM.ObjChangeCustInfo.ContactName;
            objChangeCustInfoVM.ObjChangeCustInfo.AddressLine1 = objChangeCustInfoVM.ObjChangeCustInfo.AddressLine1;
            objChangeCustInfoVM.ObjChangeCustInfo.AddressLine2 = objChangeCustInfoVM.ObjChangeCustInfo.AddressLine2;
            objChangeCustInfoVM.ObjChangeCustInfo.City = objChangeCustInfoVM.ObjChangeCustInfo.City;
            objChangeCustInfoVM.ObjChangeCustInfo.State = objChangeCustInfoVM.ObjChangeCustInfo.State;
            objChangeCustInfoVM.ObjChangeCustInfo.PostalCode = objChangeCustInfoVM.ObjChangeCustInfo.PostalCode;
            objChangeCustInfoVM.ObjChangeCustInfo.Telephone1 = objChangeCustInfoVM.ObjChangeCustInfo.Telephone1;
            objChangeCustInfoVM.ObjChangeCustInfo.Fax = objChangeCustInfoVM.ObjChangeCustInfo.Fax;
            objChangeCustInfoVM.ObjChangeCustInfo.Email = objChangeCustInfoVM.ObjChangeCustInfo.Email;

          
            ViewBag.lblRecCount = "";
            return PartialView(GetVirtualPath("_ChangeCustInfo"), objChangeCustInfoVM);
        }

        public ActionResult UpdateChangeCustInfo(ChangeCustInfoVM objChangeCustInfoVM)
        {
            int result = 0;
            try
            {
                result = ADClientCustomer.UpdateCustomer(objChangeCustInfoVM);

                return Json(result);
            }
            catch
            {
                result = -1;
                return Json(result);
            }

        }

        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/Managements/ChangeCustInfo/" + ViewName + ".cshtml";

            return path;
        }
    }
}
