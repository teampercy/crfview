﻿using CRFView.Adapters;
using CRFView.Adapters.Liens.Managements.TXUtility.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.Managements.TXUtility
{
    public class TXUtilityController : Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            TXUtilityVM ObjTXUtilityVM = new TXUtilityVM();
            return PartialView(GetVirtualPath("_TXUtility"), ObjTXUtilityVM);
        }

        //Worker_DoTheWork
        [HttpPost]
        public ActionResult GetTXUtilityDetails(TXUtilityVM objTXUtilityVM)
        {
            int result = 0;

            try
            {
                result = ADTXUtility.GetTXUtility(objTXUtilityVM);
                objTXUtilityVM.Message = "Processed Succesfully";
                return Json(result);
            }
            catch
            {
                result = -1;
                //objDNNJobsVM.Message = "Process Failed Error Encountered";
                return Json(result);
            }

        }


        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/Managements/TXUtility/" + ViewName + ".cshtml";

            return path;
        }

    }
}
