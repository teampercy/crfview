﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using CRFView.Adapters;
using CRFView.Adapters.Liens.Managements.ChangeDateRequested.ViewModels;
using System.Web.Mvc;


namespace CRFView.Controllers.Liens.Managements.ChangeDateRequested
{
    public class ChangeDateRequestedController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
            
        }
        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            
            ChangeDateRequestedVM objChangeDateRequestedVM = new ChangeDateRequestedVM();
            //ViewBag.lblRecCount = "";

            return PartialView(GetVirtualPath("_ChangeDateRequested"), objChangeDateRequestedVM);
        }

        public ActionResult GetChangeDateRequested(ChangeDateRequestedVM objChangeDateRequestedVM)
        {
          

            objChangeDateRequestedVM.dtChangeDateRequestedList = ADvwJobPrelimRequests.GetChangeDateRequestedDt(objChangeDateRequestedVM);
            objChangeDateRequestedVM.ChangeDateRequestedList = ADvwJobPrelimRequests.GetChangeDateRequestedList(objChangeDateRequestedVM.dtChangeDateRequestedList);

           // ViewBag.lblRecCount = "Total  (" + objChangeDateRequestedVM.ChangeDateRequestedList.Count + ") Records";

            return PartialView(GetVirtualPath("_ChangeDateRequested"), objChangeDateRequestedVM);
        }

        [HttpPost]
        public ActionResult LoadChangeDateRequested(string JobId, DateTime Date)
        {
           
            ChangeDateRequestedVM objChangeDateRequestedVM = new ChangeDateRequestedVM();
            objChangeDateRequestedVM.JobId = JobId;

            DataTable dtvwJobInfoList = ADvwJobPrelimRequests.LoadInfo(JobId);

            

            objChangeDateRequestedVM.ObjChangeDateRequested.ClientCode = ((dtvwJobInfoList.Rows[0]["ClientCode"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["ClientCode"].ToString() : ""));
            objChangeDateRequestedVM.ObjChangeDateRequested.ClientName = ((dtvwJobInfoList.Rows[0]["ClientName"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["ClientName"].ToString() : ""));
            objChangeDateRequestedVM.ObjChangeDateRequested.JobId = ((dtvwJobInfoList.Rows[0]["JobId"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobId"].ToString() : ""));
            objChangeDateRequestedVM.ObjChangeDateRequested.JobNum = ((dtvwJobInfoList.Rows[0]["JobNum"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobNum"].ToString() : ""));
            objChangeDateRequestedVM.ObjChangeDateRequested.EstBalance = ((dtvwJobInfoList.Rows[0]["EstBalance"] != DBNull.Value ? Convert.ToDecimal(dtvwJobInfoList.Rows[0]["EstBalance"]) : 0));
            objChangeDateRequestedVM.ObjChangeDateRequested.DateAssigned = ((dtvwJobInfoList.Rows[0]["DateAssigned"] != DBNull.Value ? Convert.ToDateTime(dtvwJobInfoList.Rows[0]["DateAssigned"]) : DateTime.MinValue));
            objChangeDateRequestedVM.ObjChangeDateRequested.JobName = ((dtvwJobInfoList.Rows[0]["JobName"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobName"].ToString() : ""));
             objChangeDateRequestedVM.ObjChangeDateRequested.StartDate = ((dtvwJobInfoList.Rows[0]["StartDate"] != DBNull.Value ? Convert.ToDateTime(dtvwJobInfoList.Rows[0]["StartDate"]) : DateTime.MinValue));
             objChangeDateRequestedVM.ObjChangeDateRequested.JobAdd1 = ((dtvwJobInfoList.Rows[0]["JobAdd1"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobAdd1"].ToString() : ""));
           objChangeDateRequestedVM.ObjChangeDateRequested.JobCity = ((dtvwJobInfoList.Rows[0]["JobCity"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobCity"].ToString() : ""));
             objChangeDateRequestedVM.ObjChangeDateRequested.JobState = ((dtvwJobInfoList.Rows[0]["JobState"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobState"].ToString() : ""));
            objChangeDateRequestedVM.ObjChangeDateRequested.LenderName = ((dtvwJobInfoList.Rows[0]["LenderName"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["LenderName"].ToString() : ""));
            objChangeDateRequestedVM.ObjChangeDateRequested.StatusCode = ((dtvwJobInfoList.Rows[0]["StatusCode"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["StatusCode"].ToString() : ""));
            objChangeDateRequestedVM.ObjChangeDateRequested.CustName = ((dtvwJobInfoList.Rows[0]["CustName"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["CustName"].ToString() : ""));
           objChangeDateRequestedVM.ObjChangeDateRequested.GCName = ((dtvwJobInfoList.Rows[0]["GCName"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["GCName"].ToString() : ""));
            objChangeDateRequestedVM.ObjChangeDateRequested.BranchNumber = ((dtvwJobInfoList.Rows[0]["BranchNumber"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["BranchNumber"].ToString() : ""));
            objChangeDateRequestedVM.ObjChangeDateRequested.OwnerName = ((dtvwJobInfoList.Rows[0]["OwnerName"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["OwnerName"].ToString() : ""));
            objChangeDateRequestedVM.ObjChangeDateRequested.DeskNum = ((dtvwJobInfoList.Rows[0]["DeskNum"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["DeskNum"].ToString() : ""));
            objChangeDateRequestedVM.ObjChangeDateRequested.VerifiedDate = ((dtvwJobInfoList.Rows[0]["VerifiedDate"] != DBNull.Value ? Convert.ToDateTime(dtvwJobInfoList.Rows[0]["VerifiedDate"]) : DateTime.MinValue));

            objChangeDateRequestedVM.ObjChangeDateRequested.JobZip = ((dtvwJobInfoList.Rows[0]["JobZip"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobZip"].ToString() : ""));
            objChangeDateRequestedVM.DateTimePicker2 = Date;
           
            return PartialView(GetVirtualPath("_EditChangeDateRequested"), objChangeDateRequestedVM);
        }
        [HttpPost]
        public ActionResult ChangeDateRequestedLoad(ChangeDateRequestedVM objChangeDateRequestedVM)
        {
            //ViewBag.lblRecCount = "";
            return PartialView(GetVirtualPath("_ChangeDateRequested"), objChangeDateRequestedVM);
        }

        [HttpPost]
        public ActionResult UpdateChangeDateRequested(ChangeDateRequestedVM objChangeDateRequestedVM)
        {
            
            int result = 1;
           

            objChangeDateRequestedVM.ObjChangeDateRequested.JobId = Convert.ToString(objChangeDateRequestedVM.ObjChangeDateRequested.JobId);
            objChangeDateRequestedVM.ObjChangeDateRequested.DateRequested = Convert.ToDateTime(objChangeDateRequestedVM.DateTimePicker2);


            result = ADvwJobPrelimRequests.UpdateChangeDateRequested_Action(objChangeDateRequestedVM);
           

            
            return Json(result);
        }
        [HttpPost]
        public ActionResult UpdateChangeDateRequestedCheck(string JobId, DateTime Date)
        {

            int result = 1;
          
            ChangeDateRequestedVM objChangeDateRequestedVM = new ChangeDateRequestedVM();
            objChangeDateRequestedVM.JobId = JobId;
            objChangeDateRequestedVM.DateTimePicker2 = Date;


           result = ADvwJobPrelimRequests.UpdateChangeDateRequested_Action(objChangeDateRequestedVM);



            return Json(result);
        }

        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/Managements/ChangeDateRequested/" + ViewName + ".cshtml";

            return path;
        }
    }
}
