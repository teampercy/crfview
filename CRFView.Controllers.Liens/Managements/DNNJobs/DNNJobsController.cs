﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using CRFView.Adapters.Liens.Managements.DNNJobs.ViewModels;
using System.Threading.Tasks;
using System.Web.Mvc;
using CRFView.Adapters;

namespace CRFView.Controllers.Liens.Managements.DNNJobs
{
    public class DNNJobsController : Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            DNNJobsVM ObjDNNJobsVM = new DNNJobsVM();
            return PartialView(GetVirtualPath("_DNNJobs"), ObjDNNJobsVM);
        }

        //Worker_DoTheWork
        [HttpPost]
        public ActionResult GetJobView_DNNJobsDetails(DNNJobsVM objDNNJobsVM)
        {
            int result = 0;
            try
            {
                result = ADDNNJobs.GetJobView_DNNJobs(objDNNJobsVM);
                objDNNJobsVM.Message = "Processed Succesfully";
                return Json(result);
            }
            catch
            {
                result = -1;
                objDNNJobsVM.Message = "Process Failed Error Encountered";
                return Json(result);
            }

        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/Managements/DNNJobs/" + ViewName + ".cshtml";

            return path;
        }

    }
}
