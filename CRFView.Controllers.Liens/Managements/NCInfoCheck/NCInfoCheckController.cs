﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using CRFView.Adapters;
using System.Web.Mvc;
using CRFView.Adapters.Liens.Managements.NCInfoCheck.ViewModels;


namespace CRFView.Controllers.Liens.Managements.NCInfoCheck
{
    public class NCInfoCheckController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {

            NCInfoCheckVM objNCInfoCheckVM = new NCInfoCheckVM();
            ViewBag.lblRecCount = "";
            objNCInfoCheckVM.chkAllClient = true;
            return PartialView(GetVirtualPath("_NCInfoCheck"), objNCInfoCheckVM);
        }

        [HttpPost]
        public ActionResult GetNCInfoCheck(NCInfoCheckVM objNCInfoCheckVM)
        {


            objNCInfoCheckVM.dtNCInfoCheckList = ADvmJobInfo.GetNCInfoCheckDt(objNCInfoCheckVM);
            objNCInfoCheckVM.NCInfoCheckList = ADvmJobInfo.GetNCInfoCheckList(objNCInfoCheckVM.dtNCInfoCheckList);

          
           

            ViewBag.lblRecCount = "Total  (" + objNCInfoCheckVM.NCInfoCheckList.Count + ") Records";

            return PartialView(GetVirtualPath("_NCInfoCheck"), objNCInfoCheckVM);
        }

        [HttpPost]
        public ActionResult LoadNCInfoCheck(string JobId, bool NCInfo)
        {

            NCInfoCheckVM objNCInfoCheckVM = new NCInfoCheckVM();
            objNCInfoCheckVM.JobId = Convert.ToInt32(JobId);
            
            DataTable dtvwJobInfoList = ADvmJobInfo.LoadInfo(JobId);



            objNCInfoCheckVM.ObjNCInfoCheck.ClientCode = ((dtvwJobInfoList.Rows[0]["ClientCode"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["ClientCode"].ToString() : ""));
            objNCInfoCheckVM.ObjNCInfoCheck.ClientName = ((dtvwJobInfoList.Rows[0]["ClientName"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["ClientName"].ToString() : ""));
            objNCInfoCheckVM.ObjNCInfoCheck.JobId = ((dtvwJobInfoList.Rows[0]["JobId"] != DBNull.Value ? Convert.ToInt32(dtvwJobInfoList.Rows[0]["JobId"]) : Int32.MinValue));
            objNCInfoCheckVM.ObjNCInfoCheck.JobNum = ((dtvwJobInfoList.Rows[0]["JobNum"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobNum"].ToString() : ""));
            objNCInfoCheckVM.ObjNCInfoCheck.EstBalance = ((dtvwJobInfoList.Rows[0]["EstBalance"] != DBNull.Value ? Convert.ToDecimal(dtvwJobInfoList.Rows[0]["EstBalance"]) : 0));
            objNCInfoCheckVM.ObjNCInfoCheck.DateAssigned = ((dtvwJobInfoList.Rows[0]["DateAssigned"] != DBNull.Value ? Convert.ToDateTime(dtvwJobInfoList.Rows[0]["DateAssigned"]) : DateTime.MinValue));
            objNCInfoCheckVM.ObjNCInfoCheck.JobName = ((dtvwJobInfoList.Rows[0]["JobName"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobName"].ToString() : ""));
            objNCInfoCheckVM.ObjNCInfoCheck.StartDate = ((dtvwJobInfoList.Rows[0]["StartDate"] != DBNull.Value ? Convert.ToDateTime(dtvwJobInfoList.Rows[0]["StartDate"]) : DateTime.MinValue));
            objNCInfoCheckVM.ObjNCInfoCheck.JobAdd1 = ((dtvwJobInfoList.Rows[0]["JobAdd1"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobAdd1"].ToString() : ""));
            objNCInfoCheckVM.ObjNCInfoCheck.JobCity = ((dtvwJobInfoList.Rows[0]["JobCity"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobCity"].ToString() : ""));
            objNCInfoCheckVM.ObjNCInfoCheck.JobState = ((dtvwJobInfoList.Rows[0]["JobState"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobState"].ToString() : ""));
            objNCInfoCheckVM.ObjNCInfoCheck.LenderName = ((dtvwJobInfoList.Rows[0]["LenderName"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["LenderName"].ToString() : ""));
            objNCInfoCheckVM.ObjNCInfoCheck.StatusCode = ((dtvwJobInfoList.Rows[0]["StatusCode"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["StatusCode"].ToString() : ""));
            objNCInfoCheckVM.ObjNCInfoCheck.CustName = ((dtvwJobInfoList.Rows[0]["CustName"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["CustName"].ToString() : ""));
            objNCInfoCheckVM.ObjNCInfoCheck.GCName = ((dtvwJobInfoList.Rows[0]["GCName"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["GCName"].ToString() : ""));
            objNCInfoCheckVM.ObjNCInfoCheck.BranchNumber = ((dtvwJobInfoList.Rows[0]["BranchNumber"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["BranchNumber"].ToString() : ""));
            objNCInfoCheckVM.ObjNCInfoCheck.OwnerName = ((dtvwJobInfoList.Rows[0]["OwnerName"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["OwnerName"].ToString() : ""));
            objNCInfoCheckVM.ObjNCInfoCheck.DeskNum = ((dtvwJobInfoList.Rows[0]["DeskNum"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["DeskNum"].ToString() : ""));
            objNCInfoCheckVM.ObjNCInfoCheck.VerifiedDate = ((dtvwJobInfoList.Rows[0]["VerifiedDate"] != DBNull.Value ? Convert.ToDateTime(dtvwJobInfoList.Rows[0]["VerifiedDate"]) : DateTime.MinValue));
            objNCInfoCheckVM.ObjNCInfoCheck.NCInfo = ((dtvwJobInfoList.Rows[0]["NCInfo"] != DBNull.Value ? Convert.ToBoolean(dtvwJobInfoList.Rows[0]["NCInfo"]) : false));

            objNCInfoCheckVM.ObjNCInfoCheck.JobZip = ((dtvwJobInfoList.Rows[0]["JobZip"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobZip"].ToString() : ""));
            objNCInfoCheckVM.chkNCInfo = NCInfo;

            // objNCInfoCheckVM.DateTimePicker2 = Date;

            return PartialView(GetVirtualPath("_EditNCInfoCheck"), objNCInfoCheckVM);
        }


        //Update NCInfoCheck
        [HttpPost]
        public ActionResult UpdateNCInfoCheck(NCInfoCheckVM objNCInfoCheckVM)
        {
            int result = 0;
            string JobId =Convert.ToString(objNCInfoCheckVM.ObjNCInfoCheck.JobId);
          // bool chkNCInfo = objNCInfoCheckVM.NCInfoCheckList.NCInfo;
            try
            {

                //DataTable dtChangeStateInfo = ADStateInfo.GetChangeRead(JobState);

                //if ((objChangeJobInfoVM.chkGreencard == false) && (objChangeJobInfoVM.myPrivateJob == true || objChangeJobInfoVM.myResidentialBox == true) && (Convert.ToBoolean(dtChangeStateInfo.Rows[0]["ReturnReceiptPrivate"]) == true))
                //{
                //    objChangeJobInfoVM.Message = "The state requires return receipt on this job";
                //}
                //else if ((objChangeJobInfoVM.chkGreencard == false) && (objChangeJobInfoVM.myPublicBox == true) && (Convert.ToBoolean(dtChangeStateInfo.Rows[0]["ReturnReceiptPublic"]) == true))
                //{
                //    objChangeJobInfoVM.Message = "The state requires return receipt on this job";
                //}

                //Client objClient = new Client();
                //Job objJob = new Job();
                objNCInfoCheckVM.ObjNCInfoCheck = ADvmJobInfo.ReadJob(Convert.ToInt32(JobId));

                result = ADvmJobInfo.UpdateJob(objNCInfoCheckVM);

                //if (objNCInfoCheckVM.chkDeleteNoticeHistory == true)
                //{
                //    result = ADJob.SaveJob_ChangeJobInfo_1(objNCInfoCheckVM);
                //}


                //objNCInfoCheckVM.ObjNCInfoCheck = ADClient.GetClientDetails(Convert.ToInt32(JobId));
                ////DBO.Read(myjob.ClientId, myClient)

                //if (objNCInfoCheckVM.ObjNCInfoCheck.IsJobView == true && objNCInfoCheckVM.objADClient.NoRentalInfo == false)
                //{
                //    result = ADJob.SaveJob_ChangeJobInfo2(JobId);
                //}
                //else
                //{
                //    result = ADJob.SaveJob_ChangeJobInfo_2ElseCon(JobId);
                //}

                //if (objJob.IsJobView == true && objClient.NoRentalInfo == false)
                //{
                //    result = ADJob.SaveJob_ChangeJobInfo2(JobId);
                //}
                //else
                //{
                //    result = ADJob.SaveJob_ChangeJobInfo_2ElseCon(JobId);
                //}

                objNCInfoCheckVM.ObjNCInfoCheck = ADvmJobInfo.ReadJob(Convert.ToInt32(JobId));


                return Json(result);
            }
            catch
            {
                result = -1;
                return Json(result);
            }

        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/Managements/NCInfoCheck/" + ViewName + ".cshtml";

            return path;
        }
    }
}
