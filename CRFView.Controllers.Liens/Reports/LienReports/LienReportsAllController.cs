﻿using CRFView.Adapters;
using CRFView.Adapters.Liens.Reports.LienReports.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.Reports.LienReports
{
    public class LienReportsAllController : Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            LienReportsVM ObjLienReportsVM = new LienReportsVM();
            return PartialView(GetVirtualPath("_LienReports"), ObjLienReportsVM);
        }

        //Get Action History Codes List by Acction Type
        public ActionResult GetReportIdList(string ReportCategoryValue)
        {
            List<CommonBindList> ReportIdList = new List<CommonBindList>();
            try
            {
                ReportIdList = CommonBindList.GetReportIdvwLienReportsListByCategoryType(ReportCategoryValue);
            }
            catch (Exception ex)
            {

            }
            return Json(ReportIdList, JsonRequestBehavior.AllowGet);
        }

        //Print LienReports
        [HttpPost]
        public string LienReportsPrint(LienReportsVM ObjLienReportsVM)
        {
            try
            {
                string PdfUrl = null;

                PdfUrl = ADLienReports.PrintLienReports(ObjLienReportsVM);

                if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
                {
                    string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                    string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                    return (path);
                }
            }
            catch (Exception ex)
            {
                return "error";
            }
            return "error";
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/Reports/LienReports/" + ViewName + ".cshtml";
            return path;
        }

    }
}
