﻿using CRFView.Adapters;
using CRFView.Adapters.Liens.DayEnds.PublicException.ViewModels;
using CRFView.Adapters.Liens.Managements.ChangeDateRequested.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.DayEnd.PublicException
{
    public class PublicExceptionController : Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }


        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {

            PublicExceptionVM objPublicExceptionVM = new PublicExceptionVM();

            return PartialView(GetVirtualPath("_PublicException"), objPublicExceptionVM);
        }


        public ActionResult GetPublicExceptionNonPublic(PublicExceptionVM objPublicExceptionVM)
        {
            
            objPublicExceptionVM.dtNonPublicExceptionList = ADPublicException.GetNonPublicExceotionDt(objPublicExceptionVM);
            objPublicExceptionVM.NonPublicExceptionList = ADPublicException.GetNonPublicExceotionList(objPublicExceptionVM.dtNonPublicExceptionList);

          
            return PartialView(GetVirtualPath("_PublicException"), objPublicExceptionVM);
        }

          public ActionResult GetPublicExceptionPublic(PublicExceptionVM objPublicExceptionVM)
        {
            
            objPublicExceptionVM.dtNonPublicExceptionList = ADPublicException.GetPublicExceotionDt(objPublicExceptionVM);
            objPublicExceptionVM.NonPublicExceptionList = ADPublicException.GetNonPublicExceotionList(objPublicExceptionVM.dtNonPublicExceptionList);
           
            return PartialView(GetVirtualPath("_PublicException"), objPublicExceptionVM);
        }

        [HttpPost]
        public ActionResult LoadPublicException(string JobId)
        {

            PublicExceptionVM objPublicExceptionVM = new PublicExceptionVM();
            ChangeDateRequestedVM objChangeDateRequestedVM = new ChangeDateRequestedVM();
            //objPublicExceptionVM.JobId = JobId;

            DataTable dtvwJobInfoList = ADPublicException.LoadInfo(JobId);

            

            objChangeDateRequestedVM.ObjChangeDateRequested.ClientCode = ((dtvwJobInfoList.Rows[0]["ClientCode"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["ClientCode"].ToString() : ""));
            objChangeDateRequestedVM.ObjChangeDateRequested.ClientName = ((dtvwJobInfoList.Rows[0]["ClientName"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["ClientName"].ToString() : ""));
            objChangeDateRequestedVM.ObjChangeDateRequested.JobId = ((dtvwJobInfoList.Rows[0]["JobId"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobId"].ToString() : ""));
            objChangeDateRequestedVM.ObjChangeDateRequested.JobNum = ((dtvwJobInfoList.Rows[0]["JobNum"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobNum"].ToString() : ""));
            objChangeDateRequestedVM.ObjChangeDateRequested.EstBalance = ((dtvwJobInfoList.Rows[0]["EstBalance"] != DBNull.Value ? Convert.ToDecimal(dtvwJobInfoList.Rows[0]["EstBalance"]) : 0));
            objChangeDateRequestedVM.ObjChangeDateRequested.DateAssigned = ((dtvwJobInfoList.Rows[0]["DateAssigned"] != DBNull.Value ? Convert.ToDateTime(dtvwJobInfoList.Rows[0]["DateAssigned"]) : DateTime.MinValue));
            objChangeDateRequestedVM.ObjChangeDateRequested.JobName = ((dtvwJobInfoList.Rows[0]["JobName"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobName"].ToString() : ""));
            objChangeDateRequestedVM.ObjChangeDateRequested.StartDate = ((dtvwJobInfoList.Rows[0]["StartDate"] != DBNull.Value ? Convert.ToDateTime(dtvwJobInfoList.Rows[0]["StartDate"]) : DateTime.MinValue));
            objChangeDateRequestedVM.ObjChangeDateRequested.JobAdd1 = ((dtvwJobInfoList.Rows[0]["JobAdd1"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobAdd1"].ToString() : ""));
            objChangeDateRequestedVM.ObjChangeDateRequested.JobCity = ((dtvwJobInfoList.Rows[0]["JobCity"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobCity"].ToString() : ""));
            objChangeDateRequestedVM.ObjChangeDateRequested.JobState = ((dtvwJobInfoList.Rows[0]["JobState"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["JobState"].ToString() : ""));
            objChangeDateRequestedVM.ObjChangeDateRequested.StatusCode = ((dtvwJobInfoList.Rows[0]["StatusCode"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["StatusCode"].ToString() : ""));
            objChangeDateRequestedVM.ObjChangeDateRequested.BranchNumber = ((dtvwJobInfoList.Rows[0]["BranchNumber"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["BranchNumber"].ToString() : ""));
            objChangeDateRequestedVM.ObjChangeDateRequested.OwnerName = ((dtvwJobInfoList.Rows[0]["OwnerName"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["OwnerName"].ToString() : ""));
            objChangeDateRequestedVM.ObjChangeDateRequested.DeskNum = ((dtvwJobInfoList.Rows[0]["DeskNum"] != DBNull.Value ? dtvwJobInfoList.Rows[0]["DeskNum"].ToString() : ""));
            
            return PartialView(GetVirtualPath("_EditPublicException"), objChangeDateRequestedVM);
        }

        [HttpPost]
        public ActionResult UpdatePublicException(ChangeDateRequestedVM objChangeDateRequestedVM)
        {

            int result = 1;


            objChangeDateRequestedVM.ObjChangeDateRequested.JobId = Convert.ToString(objChangeDateRequestedVM.ObjChangeDateRequested.JobId);
           
            result = ADPublicException.UpdatePublicException_Action(objChangeDateRequestedVM);



            return Json(result);
        }
    
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/DayEnds/PublicException/" + ViewName + ".cshtml";
            return path;
        }


    }
}
