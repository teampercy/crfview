﻿//using CRFView.Adapters.Liens.
using System;
using System.Collections.Generic;
using CRFView.Adapters.Liens.DayEnds.AutoEmail.ViewModels;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CRFView.Adapters.Liens.DayEnds;
using System.IO;

namespace CRFView.Controllers.Liens.DayEnds
{
   public class AutoEmailController:Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return View(GetVirtualPath("Index"));
        }

        [HttpPost]
        public ActionResult ShowModal()
        {

           AutoEmailVM objAutoEmailVM = new AutoEmailVM();


            return PartialView(GetVirtualPath("_AutoEmail"), objAutoEmailVM);

        }
        //Print CollectionReports
        //[HttpPost]
        //public string AutoEmailPrint(AutoEmailVM objAutoFaxVM)
        //{

        //    try
        //    {
        //        string PdfUrl = null;

        //        PdfUrl = ADvwJobInfo.PrintAutoEmail(objAutoFaxVM);


        //        if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
        //        {
        //            string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
        //            string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
        //            return (path);
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        return "error";
        //    }
        //    return "error";
        //}


        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/DayEnds/AutoEmail/" + ViewName + ".cshtml";
            return path;
        }
    }
}
