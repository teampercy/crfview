﻿using CRFView.Adapters;
using CRFView.Adapters.Liens.TXNoticeCycle.SmartMailerFile.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.TXNoticeCycle.SmartMailerFile
{
    public class SmartMailerFileController : Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }


        public ActionResult ShowModal()
        {

            SmartMailerFileVM objSmartMailerFileVM = new SmartMailerFileVM();
            return PartialView(GetVirtualPath("_SmartMailerFile"), objSmartMailerFileVM);
        }


        [HttpPost]
        public string SmartMailerFileDetails(SmartMailerFileVM objSmartMailerFileVM)
        {
            try
            {
                string PdfUrl = null;

                  PdfUrl = ADJobTexasRequest.PrintSmartMailerFile(objSmartMailerFileVM);


                if (PdfUrl == "True")
                {
                    return "CSVFilesuccess";
                }
                else
                {
                    objSmartMailerFileVM.Message = "No Records Selected, Please Run Extract";
                }

                if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
                {
                    string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                    string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                    return (path);
                }
            }
            catch (Exception ex)
            {
                return "error";
            }
            return objSmartMailerFileVM.Message ;
        }



        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/TXNoticeCycle/SmartMailerFile/" + ViewName + ".cshtml";
            return path;
        }


    }
}
