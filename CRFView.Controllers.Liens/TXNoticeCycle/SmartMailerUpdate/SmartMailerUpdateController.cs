﻿using CRFView.Adapters;
using CRFView.Adapters.Liens.TXNoticeCycle.SmartMailerUpdate.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.TXNoticeCycle.SmartMailerUpdate
{
    public class SmartMailerUpdateController : Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }


        public ActionResult ShowModal()
        {

            SmartMailerUpdateVM objSmartMailerUpdateVM = new SmartMailerUpdateVM();
            return PartialView(GetVirtualPath("_SmartMailerUpdate"), objSmartMailerUpdateVM);
        }

        //Save File Details
        [HttpPost]
        public bool SaveSmartMailerUpdate(SmartMailerUpdateVM ObjSmartMailerUpdateVM)
        {
            try
            {
                string path = Convert.ToString(Session["path"]);
                if (!String.IsNullOrEmpty(path))
                {
                    string afilepath = Convert.ToString(Session["path"]);
                    bool s = ADSmartMailerUpdate.GetDataSmartMailerUpdate(afilepath);

                    if (s == true)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                }
                else
                {
                    return false;
                }
                   


            }
            catch (Exception ex)
            {
                return false;
            }
        }


        //Save File Details
        [HttpPost]
        public ActionResult SaveDocument()
        {
            try
            {
                var file = Request.Files[0];
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/Output/UploadedFiles"), fileName);
                file.SaveAs(path);
                Session["path"] = path;
                return Json(true);
            }
            catch (Exception ex)
            {
                return Json(false);
            }
        }




        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/TXNoticeCycle/SmartMailerUpdate/" + ViewName + ".cshtml";
            return path;
        }


    }
}
