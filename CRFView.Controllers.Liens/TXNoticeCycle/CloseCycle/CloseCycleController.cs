﻿using CRFView.Adapters;
using CRFView.Adapters.Liens.TXNoticeCycle.CloseCycle.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.TXNoticeCycle.CloseCycle
{
    public class CloseCycleController : Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }


        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            CloseCycleVM objCloseCycleVM = new CloseCycleVM();

            DataTable dtGetNoticesByDateRequested = ADJob.GetNoticesCycleByDateRequested();

            if (dtGetNoticesByDateRequested.Rows.Count < 1)
            {
                objCloseCycleVM.Message = "No Texas Notices Selected, Please Run Extract.";
                //ViewBag.lblMessage = "No Texas Notices Selected, Please Run Extract.";
                //objCloseCycleVM.Message = ViewBag.lblMessage;
                //ViewBag.btnOK = "Hide OK";

            }
            else
            {
                objCloseCycleVM.Message = "Press Submit to Close the Current Print Cycle.";
                //ViewBag.lblMessage = "Press Submit to Close the Current Print Cycle.";
                //ViewBag.btnOK = "Show OK";
            }

            return PartialView(GetVirtualPath("_CloseCycle"), objCloseCycleVM);
        }



        //JV TX Request Close Cycle
        [HttpPost]
        public ActionResult CloseCycle(CloseCycleVM objCloseCycleVM)
        {
            int result = 0;
            try
            {
                result = ADJob.RequestJVTXCloseCycle();
            }
            catch (Exception ex)
            {
                result = -1;
            }
            return Json(result);
        }




        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/TXNoticeCycle/CloseCycle/" + ViewName + ".cshtml";
            return path;
        }


    }
}
