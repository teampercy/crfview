﻿using CRFView.Adapters;
using CRFView.Adapters.Liens.TXNoticeCycle.PrintStatements.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.TXNoticeCycle.PrintStatements
{
    public class TXNoticePrintStatementsController : Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        public ActionResult ShowModal()
        {

            TXNoticePrintStatementsVM objTXNoticePrintStatementsVM = new TXNoticePrintStatementsVM();
            return PartialView(GetVirtualPath("_TXNoticePrintStatements"), objTXNoticePrintStatementsVM);
        }


        [HttpPost]
        public string PrintTXNoticePrintStatementsDetails(TXNoticePrintStatementsVM objTXNoticePrintStatementsVM)
        {
            try
            {
                string PdfUrl = null;

               PdfUrl = ADvwJobTexasRequestHistory.PrintTXNoticePrintStatementsReport(objTXNoticePrintStatementsVM);

                if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
                {
                    string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                    string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                    return (path);
                }
            }
            catch (Exception ex)
            {
                return "error";
            }
            return "error";
        }




        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/TXNoticeCycle/PrintStatements/" + ViewName + ".cshtml";
            return path;
        }



    }
}
