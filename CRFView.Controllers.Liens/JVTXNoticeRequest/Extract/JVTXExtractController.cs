﻿using CRFView.Adapters;
using CRFView.Adapters.Liens.JVTXNoticeRequest.Extract.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.JVTXNoticeRequest.Extract
{
    public class JVTXExtractController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }


        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            JVTXExtractLettersVM objJVTXExtractLettersVM = new JVTXExtractLettersVM();

            DataTable dtGetNoticesByDateRequested = ADJob.GetNoticesByDateRequested();

            if (dtGetNoticesByDateRequested.Rows.Count > 0)
            {
                ViewBag.lblMessage = "The Prior Print Cycle was not closed, Please Run Close Cycle.";
                ViewBag.btnOK = "Hide OK";
            }
            else
            {
                objJVTXExtractLettersVM.objUSPSCertNum = ADUSPSCertNum.ReadUSPSCertNum(1);
                objJVTXExtractLettersVM.LastCertNumber = objJVTXExtractLettersVM.objUSPSCertNum.SeqNo;

                ViewBag.lblMessage = string.Empty;
                ViewBag.btnOK = "Show OK";
            }

            return PartialView(GetVirtualPath("_JVTXExtractLetters"), objJVTXExtractLettersVM);
        }


        //JV TX Extract Letters
        [HttpPost]
        public ActionResult JVTXExtractLetters(JVTXExtractLettersVM objJVTXExtractLettersVM)
        {
            int result = 0;
            try
            {
                if(objJVTXExtractLettersVM.chkAssignCerts == true)
                {
                    objJVTXExtractLettersVM.objUSPSCertNum = ADUSPSCertNum.ReadUSPSCertNum(1);
                    objJVTXExtractLettersVM.objUSPSCertNum.SeqNo = objJVTXExtractLettersVM.LastCertNumber;
                    objJVTXExtractLettersVM.objUSPSCertNum.Id = 1;
                    ADUSPSCertNum.SaveUSPSCertNum(objJVTXExtractLettersVM.objUSPSCertNum);
                }

                result = ADJob.ProcessJVTXExtractLetters(objJVTXExtractLettersVM);
            }
            catch (Exception ex)
            {
                result = -1;
            }
            return Json(result);
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/JVTXNoticeRequest/Extract/" + ViewName + ".cshtml";
            return path;
        }
    }
}
