﻿using CRFView.Adapters;
using CRFView.Adapters.Liens.JVTXNoticeRequest.CloseCycle.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.JVTXNoticeRequest.CloseCycle
{
    public class JVTXCloseCycleController  : Controller

    {

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }


        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            JVTXCloseCycleVM objJVTXCloseCycleVM = new JVTXCloseCycleVM();

            DataTable dtGetNoticesByDateRequested = ADJob.GetNoticesByDateRequested();

            if (dtGetNoticesByDateRequested.Rows.Count < 1)
            {
                ViewBag.lblMessage = "No Texas Notices Selected, Please Run Extract.";
                ViewBag.btnOK = "Hide OK";
            }
            else
            {
                ViewBag.lblMessage = "Press Submit to Close the Current Print Cycle.";
                ViewBag.btnOK = "Show OK";
            }

            return PartialView(GetVirtualPath("_JVTXCloseCycle"), objJVTXCloseCycleVM);
        }


        //JV TX Request Close Cycle
        [HttpPost]
        public ActionResult JVTXRequestCloseCycle(JVTXCloseCycleVM objJVTXJVTXCloseCycleVM)
        {
            int result = 0;
            try
            {
                result = ADJob.RequestJVTXCloseCycle();
            }
            catch (Exception ex)
            {
                result = -1;
            }
            return Json(result);
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/JVTXNoticeRequest/CloseCycle/" + ViewName + ".cshtml";
            return path;
        }

    }
}
