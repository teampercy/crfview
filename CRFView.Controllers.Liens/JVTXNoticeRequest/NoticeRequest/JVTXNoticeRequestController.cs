﻿using CRFView.Adapters;
using CRFView.Adapters.Liens.JVTXNoticeRequest.NoticeRequest.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.JVTXNoticeRequest.NoticeRequest
{
    public class JVTXNoticeRequestController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            JVTXNoticeRequestVM objJVTXNoticeRequestVM = new JVTXNoticeRequestVM();

            ViewBag.lblMessage = "Press OK to Start TX Auto Notice Request Process.";
            ViewBag.lblMessage += Environment.NewLine;
            ViewBag.btnOK = "Show OK";

            return PartialView(GetVirtualPath("_JVTXNoticeRequest"), objJVTXNoticeRequestVM);
        }


        //JV TX Notice Request Process
        [HttpPost]
        public ActionResult JVTXNoticeRequestProcess(JVTXNoticeRequestVM objJVTXNoticeRequestVM)
        {
            int result = 0;
            try
            {
                result = ADJob.ProcessJVTXNoticeRequest();
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return Json(result);
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/JVTXNoticeRequest/NoticeRequest/" + ViewName + ".cshtml";
            return path;
        }

    }
}
