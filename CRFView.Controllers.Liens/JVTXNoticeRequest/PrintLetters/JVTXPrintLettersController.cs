﻿using CRFView.Adapters;
using CRFView.Adapters.Liens.JVTXNoticeRequest.PrintLetters.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.JVTXNoticeRequest.PrintLetters
{
    public class JVTXPrintLettersController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }


        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            JVTXPrintLettersVM objJVTXPrintLettersVM = new JVTXPrintLettersVM();

            DataTable dtGetTXNoticesByDateRequested= ADJob.GetTXNoticesByDateRequested();

            if (dtGetTXNoticesByDateRequested.Rows.Count < 1)
            {
                ViewBag.lblMessage = "No Texas Notices Selected, Please Run Extract.";
                ViewBag.btnOK = "Hide OK";
            }
            else
            {
                ViewBag.lblMessage = string.Empty;
                ViewBag.btnOK = "Show OK";
            }

            return PartialView(GetVirtualPath("_JVTXPrintLetters"), objJVTXPrintLettersVM);
        }


        //JVTX Letters Print
        [HttpPost]
        public string JVTXLettersPrint(JVTXPrintLettersVM objJVTXPrintLettersVM)
        {
            string PdfUrl = null;
            try
            {
                PdfUrl = ADJob.PrintJVTXLetters(objJVTXPrintLettersVM);
                if (PdfUrl == "1")
                {
                    return "1";
                }
            }
            catch (Exception ex)
            {
                return "error";
            }
            return "error";
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/JVTXNoticeRequest/PrintLetters/" + ViewName + ".cshtml";
            return path;
        }

    }
}
