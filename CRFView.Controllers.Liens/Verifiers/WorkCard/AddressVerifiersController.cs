﻿using CRFView.Adapters.PropertySearch.AddressVerifiers;
using CRFView.Adapters.PropertySearch.AddressVerifiers.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.Verifiers.WorkCard
{
  public class AddressVerifiersController : Controller
    {
          

        public ActionResult Index()
        {
            AddressVerifiersVM objAddressVerifiersVM = new AddressVerifiersVM();
            return PartialView(GetVirtualPath("Index"), objAddressVerifiersVM);
        }


        public ActionResult VerifyAddress(AddressVerifiersVM objAddressVerifiersVM)
        {
            ADAddressVerifiers objADAddressVerifiers = new ADAddressVerifiers();
            string add1 = objAddressVerifiersVM.JobAddr1 + " " + objAddressVerifiersVM.JobAddr2;
            string add2 = objAddressVerifiersVM.JobCity + " " + objAddressVerifiersVM.StateId + " " + objAddressVerifiersVM.JobZip;

            //string result = ADAddressVerifiers.AddressVerifiers(add1, add2);
            objADAddressVerifiers = ADAddressVerifiers.AddressVerifiers(objAddressVerifiersVM);

            if (objADAddressVerifiers == null)
            {
                return Json(0);
            }

            else
            {
                return Json(objADAddressVerifiers);
            }
            

           // return PartialView(GetVirtualPath("_AddressVerifiersResult"), objADAddressVerifiers);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/PropertySearch/AddressVerifiers/" + ViewName + ".cshtml";
            return path;
        }

    }
}
