﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CRFView.Adapters;
using CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels;
using CRFView.Adapters.Liens.Verifiers.WorkCard;
using CRF.BLL.Providers;
using CRF.BLL.CRFDB.TABLES;
using System.IO;
using System.Text.RegularExpressions;
using System.Runtime.InteropServices;
using CRFView;
using CRFView.Adapters.Liens.DataEntry.MailReturns.ViewModels;
using System.Diagnostics;
using CRFView.Adapters.Clients.Client_Management;
using System.Net.Configuration;
using System.Net;
using System.Net.Mail;
using System.Configuration;
using System.Net.Configuration;
using System.Web;
using System.Data;
using CRFView.Adapters.Liens.Managements.DeskAssignment;
//using CRFView.Adapters.Clients.ClientManagementViewModels;
//using CRFView.Adapters.Collections.Collectors.WorkCard.ViewModels;

namespace CRFView.Controllers.Liens.Verifiers.WorkCard
{
    public class WorkCardLiensController : Controller
    {
       
        ADvwJobsList ObjAdJoblist = new ADvwJobsList();
        JobVM ObjJobVM;
        [HttpPost]
        public ActionResult Index()
        {
             ///Session["FilterList"] = null;

            //if (Session["Filters"] != null)
                Session["Filters"] = null;
            //List<ADvwAccountFilterList> lst = ADvwAccountFilterList.GetInitialList();
            string SortBy = "JobId DESC";
            //CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels.FilterJobsVM ObjFilter = new CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels.FilterJobsVM();
            Adapters.Liens.Verifiers.WorkCard.Filter ObjFilter = new Adapters.Liens.Verifiers.WorkCard.Filter();
            ObjFilter.IsWebNoteList = false;
            ObjFilter.IsTXReviewList = false;
            List<ADvwJobsList> list = ADvwJobsList.GetFilteredList(ObjFilter, SortBy);
            JobListVM ObjVm = new JobListVM();
            ObjVm.ObjJobList = list;
            Session["JobList"] = list;
            ObjVm.FilterResult = true;
            ObjVm.WebNoteResult = false;
            ObjVm.JVWebNoteListResult = false;
            ObjVm.TXReviewResult = false;
            return PartialView(GetVirtualPath("Index"), ObjVm);
        }


        public ActionResult Refresh()
        {
            string SortBy = "JobId DESC";
            List<ADvwJobsList> list = ADvwJobsList.GetInitialList(SortBy);
            JobListVM ObjVm = new JobListVM();
            ObjVm.ObjJobList = list;
            Session["JobList"] = list;
            ObjVm.FilterResult = true;
            ObjVm.WebNoteResult = false;
            ObjVm.JVWebNoteListResult = false;
            ObjVm.TXReviewResult = false;
            return PartialView(GetVirtualPath("_JobListNew"), ObjVm);
        }
        public ActionResult BackTolist()
        {
            //string SortBy = "DebtAccountId DESC";
            FilterJobsVM ObjFilter = new FilterJobsVM();
            JobListVM ObjVm = new JobListVM();
            if (Session["FilterResult"] != null)
            {
                ObjVm.ObjJobList = (List<ADvwJobsList>)Session["FilterResult"];
                //ObjVm = (JobListVM)Session["FilterList"];
                //ObjFilter.FilterObj.IsTXReviewList = false;
                ObjVm.FilterResult = true;
                ObjVm.WebNoteResult = false;
                ObjVm.JVWebNoteListResult = false;
                ObjVm.TXReviewResult = false;
                (Session["TXReviewList"]) = null;
            }
            else if (Session["WebNoteList"] != null)
            {
                ObjVm.ObjWebNotesList = (List<ADvwJobsList>)Session["WebNoteList"];
                //ObjVm = (JobListVM)Session["FilterList"];
                //ObjFilter.FilterObj.IsTXReviewList = false;
                ObjVm.FilterResult = false;
                ObjVm.WebNoteResult = true;
                ObjVm.JVWebNoteListResult = false;
                ObjVm.TXReviewResult = false;
                (Session["WebNoteList"]) = null;
            }
            else if (Session["TXReviewList"] != null)
            {
                ObjVm.objGetMonthlyJobListJV = (List<ADvwJobsList>)Session["TXReviewList"];
                //ObjVm = (JobListVM)Session["FilterList"];
                //ObjFilter.FilterObj.IsTXReviewList = false;
                ObjVm.FilterResult = false;
                ObjVm.WebNoteResult = false;
                ObjVm.TXReviewResult = true;
                (Session["TXReviewList"]) = null;
            }
            else if (Session["JVWebNoteList"] != null)
            {
                ObjVm.objGetMonthlyJobListJV = (List<ADvwJobsList>)Session["JVWebNoteList"];
                //ObjVm = (JobListVM)Session["FilterList"];
                //ObjFilter.FilterObj.IsTXReviewList = false;
                ObjVm.FilterResult = false;
                ObjVm.WebNoteResult = false;
                ObjVm.JVWebNoteListResult = false;
                ObjVm.TXReviewResult = true;
                (Session["TXReviewList"]) = null;
            }

            else
            {
             
                    ObjVm.ObjJobList = (List<ADvwJobsList>)Session["JobList"];
                    //ObjVm = (JobListVM)Session["FilterList"];
                    //ObjFilter.FilterObj.IsTXReviewList = false;
                    ObjVm.FilterResult = true;
                    ObjVm.WebNoteResult = false;
                    ObjVm.JVWebNoteListResult = false;
                    ObjVm.TXReviewResult = false;
              
            }

            return PartialView(GetVirtualPath("Index"), ObjVm);
        }

        [HttpGet]
        public ActionResult ShowJobFilterModal()
        {
         
            FilterJobsVM ObjVm = new FilterJobsVM();
           
            ObjVm.FilterObj = new Adapters.Liens.Verifiers.WorkCard.Filter();

            
            return PartialView(GetVirtualPath("_Filter"), ObjVm);
        }
         
        [HttpPost]
        public ActionResult GetFilterResult(CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels.FilterJobsVM ObjFilter)
        {
            Session["Filters"] = ObjFilter;
            string SortBy = "JobId DESC";
            List<ADvwJobsList> lst = ADvwJobsList.GetFilteredList(ObjFilter.FilterObj, SortBy);
            JobListVM ObjVm = new JobListVM();
            ObjVm.ObjJobList = lst;
            Session["FilterResult"] = lst;
            ObjVm.FilterResult = true;
            ObjVm.WebNoteResult = false;
            ObjVm.JVWebNoteListResult = false;
            ObjVm.TXReviewResult = false;
            return PartialView(GetVirtualPath("_JobListNew"), ObjVm);
        }

        [HttpPost]
        public ActionResult GetWebNoteList(CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels.FilterJobsVM ObjFilter)
        {
            Session["Filters"] = ObjFilter;
            ObjFilter.FilterObj.IsWebNoteList = true;
            ObjFilter.FilterObj.IsTXReviewList = false;
            string SortBy = "JobId DESC";
            if (string.IsNullOrEmpty(ObjFilter.FilterObj.JobId))
            {
                ObjFilter.FilterObj.JobId = "0";
            }
           
            List <ADvwJobsList> lst = ADvwJobsList.GetFilteredList(ObjFilter.FilterObj,SortBy);
            JobListVM ObjVm = new JobListVM();
            Session["WebNoteList"] = lst;
            ObjVm.ObjWebNotesList = lst;
            ObjVm.FilterResult = false;
            ObjVm.WebNoteResult = true;
            ObjVm.JVWebNoteListResult = false;
            ObjVm.TXReviewResult = false;
            return PartialView(GetVirtualPath("_JobListNew"), ObjVm);
        }

        [HttpPost]
        public ActionResult GetJVWebNoteList(CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels.FilterJobsVM ObjFilter)
        {
            Session["Filters"] = ObjFilter;
            ObjFilter.FilterObj.IsWebNoteList = false;
            ObjFilter.FilterObj.IsJVWebNoteList = true;
            ObjFilter.FilterObj.IsTXReviewList = false;
            string SortBy = "JobId DESC";
            if (string.IsNullOrEmpty(ObjFilter.FilterObj.JobId))
            {
                ObjFilter.FilterObj.JobId = "0";
            }

            List<ADvwJobsList> lst = ADvwJobsList.GetFilteredList(ObjFilter.FilterObj, SortBy);
            JobListVM ObjVm = new JobListVM();
            Session["JVWebNoteList"] = lst;
            ObjVm.ObjWebNotesList = lst;
            ObjVm.FilterResult = false;
            ObjVm.WebNoteResult = false;
            ObjVm.JVWebNoteListResult = true;
            ObjVm.TXReviewResult = false;

            return PartialView(GetVirtualPath("_JobListNew"), ObjVm);
        }

        [HttpPost]
        public ActionResult GetTXReviewList(CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels.FilterJobsVM ObjFilter)
        {
            Session["Filters"] = ObjFilter;
            ObjFilter.FilterObj.IsWebNoteList = false;
            ObjFilter.FilterObj.IsTXReviewList = true;
            string SortBy = "JobId DESC";
            if (string.IsNullOrEmpty(ObjFilter.FilterObj.JobId))
            {
                ObjFilter.FilterObj.JobId = "0";
            }

            List <ADvwJobsList> lst = ADvwJobsList.GetFilteredList(ObjFilter.FilterObj,SortBy);
            JobListVM ObjVm = new JobListVM();
            Session["TXReviewList"] = lst;
            ObjVm.objGetMonthlyJobListJV = lst;
            ObjVm.FilterResult = false;
            ObjVm.WebNoteResult = false;
            ObjVm.JVWebNoteListResult = false;
            ObjVm.TXReviewResult = true;
            return PartialView(GetVirtualPath("_JobListNew"), ObjVm);
        }

        //Get Sorted Result
        [HttpPost]
        public ActionResult GetSortedResult(string SortByValue, string SortTypeValue)
        {

            string SortType = "ASC";
            string SortBy = "JobId";
            if (SortTypeValue == "-1")
            {
                SortType = "ASC";
            }
            else if (SortTypeValue == "1")
            {
                SortType = "DESC";
            }

            if (SortByValue == "-1" || string.IsNullOrEmpty(SortByValue))
            {
                SortBy = "JobId " + SortType;
            }
            else if (SortByValue == "1")
            {
                SortBy = "ClientCode " + SortType;
            }
            else if (SortByValue == "2")
            {
                SortBy = "DeskNum " + SortType;
            }
            else if (SortByValue == "3")
            {
                SortBy = "JobName " + SortType;
            }
            else if (SortByValue == "4")
            {
                SortBy = "JobAdd1 " + SortType;
            }
            else if (SortByValue == "5")
            {
                SortBy = "StatusCode " + SortType;
            }
            else if (SortByValue == "6")
            {
                SortBy = "CustId " + SortType;
            }
            else if (SortByValue == "7")
            {
                SortBy = "ClientCustomer " + SortType;
            }
            else if (SortByValue == "8")
            {
                SortBy = "GeneralContractor " + SortType;
            }
            else if (SortByValue == "9")
            {
                SortBy = "DateAssigned " + SortType;
            }
            else if (SortByValue == "10")
            {
                SortBy = "StartDate " + SortType;
            }
            else if (SortByValue == "11")
            {
                SortBy = "RevDate " + SortType;
            }
            else if (SortByValue == "12")
            {
                SortBy = "NoticeDeadlineDate " + SortType;
            }
            else if (SortByValue == "13")
            {
                SortBy = "NoticeSent " + SortType;
            }
            else if (SortByValue == "14")
            {
                SortBy = "NTODays " + SortType;
            }
            else if (SortByValue == "15")
            {
                SortBy = "EstBalance " + SortType;
            }
            else if (SortByValue == "16")
            {
                SortBy = "JobBalance " + SortType;
            }
            else if (SortByValue == "17")
            {
                SortBy = "PropertyType " + SortType;
            }
            else if (SortByValue == "18")
            {
                SortBy = "JVStatusCode " + SortType;
            }
            else if (SortByValue == "19")
            {
                SortBy = "BranchNum " + SortType;
            }
            else if (SortByValue == "20")
            {
                SortBy = "Document " + SortType;
            }

            JobListVM ObjVm = new JobListVM();
            CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels.FilterJobsVM ObjFilter = (CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels.FilterJobsVM)Session["Filters"];
            Adapters.Liens.Verifiers.WorkCard.Filter ObjFilters = new Adapters.Liens.Verifiers.WorkCard.Filter();
            List<ADvwJobsList> list;
            if (ObjFilter == null)
            {
                ObjFilters.IsWebNoteList = false;
                ObjFilters.IsJVWebNoteList = false;
                ObjFilters.IsTXReviewList = false;

             
                list = ADvwJobsList.GetFilteredList(ObjFilters, SortBy);
                ObjVm.ObjJobList = list;
                ObjVm.FilterResult = true;
                ObjVm.WebNoteResult = false;
                ObjVm.JVWebNoteListResult = false;
                ObjVm.TXReviewResult = false;
            }
            else
            {
                list = ADvwJobsList.GetFilteredList(ObjFilter.FilterObj, SortBy);

                ObjVm.FilterResult = false;
                //List<ADvwJobsList> list = ADvwJobsList.GetInitialList(SortBy);
                //List<ADvwJobsList> list = ADvwJobsList.GetJobList(SortBy);
                if (ObjFilter.FilterObj.IsWebNoteList == true || ObjFilter.FilterObj.IsJVWebNoteList == true)
                {
                    ObjVm.ObjWebNotesList = list;
                }
                else if (ObjFilter.FilterObj.IsTXReviewList == true)
                {
                    ObjVm.objGetMonthlyJobListJV = list;
                }
                else
                {
                    ObjVm.ObjJobList = list;
                    ObjVm.FilterResult = true;
                }



                ObjVm.WebNoteResult = ObjFilter.FilterObj.IsWebNoteList;
                ObjVm.JVWebNoteListResult = ObjFilter.FilterObj.IsJVWebNoteList;
                ObjVm.TXReviewResult = ObjFilter.FilterObj.IsTXReviewList;
            }
            //Session["FilterList"] = ObjVm;
            return PartialView(GetVirtualPath("_JobListNew"), ObjVm);
        }

        [HttpPost]
        public ActionResult LoadJob(string JobID)
        {
           // JobVM ObjJobVM;
            ObjJobVM = JobInfo.LoadJobInfo(JobID);
            List<ADvwJobsList> lst = ADvwJobsList.GetMatchedList(JobID);
            Session["JobInfo"] = ObjJobVM;
            //JobInfo ObjJobInfo = new JobInfo();

            if(ObjJobVM.ObjvwJobList.SendAsIsBox == true)
            {
                ObjJobVM.ObjvwJobList.JobType = "Send As Is";
            }
            if(ObjJobVM.ObjvwJobList.PrelimBox == true)
            {
                ObjJobVM.ObjvwJobList.JobType = "Prelim Box";
            }
            if(ObjJobVM.ObjvwJobList.TitleVerifiedBox == true)
            {
                ObjJobVM.ObjvwJobList.JobType = "Verify As Is";
            }
            if (ObjJobVM.ObjvwJobList.VerifyJob == true)
            {
                ObjJobVM.ObjvwJobList.JobType = "Verify Job";
            }
            if (ObjJobVM.ObjvwJobList.VerifyOWOnly == true)
            {
                ObjJobVM.ObjvwJobList.JobType = "Verify OW Only";
            }
            if (ObjJobVM.ObjvwJobList.VerifyOWOnlySend == true)
            {
                ObjJobVM.ObjvwJobList.JobType = "Verify OW Only/Send";
            }
            if (ObjJobVM.ObjvwJobList.VerifyOWOnlyTX == true)
            {
                ObjJobVM.ObjvwJobList.JobType = "Verify OW Only TX";
            }
            ViewBag.btnPrelimNoticetFlg = true;
            ViewBag.btnDeleteNoticeFlg = true;
            Session["JobType"] = ObjJobVM.ObjvwJobList.JobType;

            if (ObjJobVM.ObjvwJobList.JobType == "Prelim Box" ||
             (ObjJobVM.ObjvwJobList.JobType == "Send As Is") ||
             (ObjJobVM.ObjvwJobList.JobType == "Verify OW Only/Send") ||
             (ObjJobVM.ObjvwJobList.JobType == "Verify OW Only" && ObjJobVM.ObjvwJobList.JobState != "TX" && ObjJobVM.ObjvwJobList.VerifiedDate == DateTime.MinValue) ||
             (ObjJobVM.ObjvwJobList.JobType == "Verify Job" && ObjJobVM.ObjvwJobList.JobState != "TX" && ObjJobVM.ObjvwJobList.VerifiedDate == DateTime.MinValue))
            {
                ViewBag.btnTexasRequestFlg = false;
            }


            if(ObjJobVM.ObjvwJobList.JobType == "Verify As Is" ||
            (ObjJobVM.ObjvwJobList.JobType == "Verify OW Only" && ObjJobVM.ObjvwJobList.VerifiedDate == DateTime.MaxValue) ||
            (ObjJobVM.ObjvwJobList.JobType == "Verify Job" &&  ObjJobVM.ObjvwJobList.VerifiedDate == DateTime.MaxValue && ObjJobVM.ObjvwJobList.StatusCode == "ARV"))
            {
                ViewBag.btnNoticeFlg = false;
                ViewBag.btnJobNoticeFlg = false;
            }
            else
            {
                ViewBag.btnNoticeFlg = true;
                ViewBag.btnJobNoticeFlg = true;
            }



            if (ObjJobVM.ObjvwJobList.JobType == "Verify As Is" || (ObjJobVM.ObjvwJobList.JobType == "Verify Job"  && ObjJobVM.ObjvwJobList.JobState != "TX") || (ObjJobVM.ObjvwJobList.JobType == "Verify OW Only" && ObjJobVM.ObjvwJobList.JobState != "TX"))
                {
                ViewBag.btnTexasRequest = "Notice";
            }
            else
            {
                ViewBag.btnTexasRequest = "TX Req";
            }

            if (ObjJobVM.ObjvwJobList.JobType == "Verify As Is" || 
                (ObjJobVM.ObjvwJobList.JobType == "Verify OW Only") ||
                (ObjJobVM.ObjvwJobList.JobType == "Verify OW Only TX") ||
                (ObjJobVM.ObjvwJobList.JobType == "Verify Job") ||
                (ObjJobVM.ObjvwJobList.JobType == "Verify"))
            {
                ViewBag.btnNotice = "Verify";
                ViewBag.btnJobNotice = "Verify";
            }
            else
            {
                ViewBag.btnNotice = "Notice";
                ViewBag.btnJobNotice = "Notice";
            }

            ObjJobVM.ObjvwJobList.JobContactPhoneNo = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(ObjJobVM.ObjvwJobList.JobContactPhoneNo);
            ObjJobVM.ObjvwJobList.CustPhNUm = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(ObjJobVM.ObjvwJobList.CustPhNUm);
            ObjJobVM.ObjvwJobList.OwnerPhNum = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(ObjJobVM.ObjvwJobList.OwnerPhNum);
            ObjJobVM.ObjvwJobList.LenderPhone = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(ObjJobVM.ObjvwJobList.LenderPhone);
            ObjJobVM.ObjvwJobList.GCPhNum = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(ObjJobVM.ObjvwJobList.GCPhNum);

            
            //Read Job for Misc Info
            ObjJobVM.ObjJobList = ADJob.ReadJob(Convert.ToInt32(JobID));
            ViewBag.SelectedAlertList = ObjJobVM.objADJobAlerts;
            Session["SelectedAlertList"] = ViewBag.SelectedAlertList;
            //ViewBag.btnTexasRequest = "TX Req";
            //ViewBag.btnNotice = "TX Req";
            ViewBag.btnNoticeFlg = true;
            if(ObjJobVM.obj_ClientEdit.IsRentalCo == false)
            {
                ViewBag.EquipRate = false;
                ViewBag.EquipRental = false;
                ViewBag.RANum = false;
              
            }
            else
            {
                ViewBag.EquipRate = true;
                ViewBag.EquipRental = true;
                ViewBag.RANum = true;
            }

            //change color of customer,GC,Owner,Lender section
            if(ObjJobVM.dtStateInfo.Rows.Count >= 1)
            {
                if(ObjJobVM.objADStateInfo.CustServBox == true)
                {
                    ViewBag.Customer = true;
                }
                else
                {
                    ViewBag.Customer = false;
                }

                if (ObjJobVM.objADStateInfo.GCServBox == true)
                {
                    ViewBag.GC = true;
                    ViewBag.GC1 = false;
                }
                else
                {
                    ViewBag.GC = false;
                    ViewBag.GC1 = false;
                }

                if (ObjJobVM.objADStateInfo.LenderServBox == true)
                {
                    ViewBag.Lender = true;
                }
                else
                {
                    ViewBag.Lender = false;
                }

                if (ObjJobVM.objADStateInfo.OwnerServBox == true)
                {
                    ViewBag.Owner = true;
                }
                else
                {
                    ViewBag.Owner = false;
                }
                if (ObjJobVM.ObjvwJobList.JobType == "Verify Job")
                {
                    ViewBag.btnPrelimNoticetFlg = false;
                    ViewBag.btnDeleteNoticeFlg = false;
                }

            }

           // Session["NOCustCall"] = ObjJobVM.obj_ClientLienInfo.NOCustCall;
            if (ObjJobVM.obj_ClientCustomer.NoCallCust == true || ObjJobVM.ObjvwJobList.OwnerSourceCustFax == true || ObjJobVM.ObjvwJobList.OwnerSourceCust == true || ObjJobVM.obj_ClientLienInfo.NOCustCall == true)
            {
                ViewBag.Customer1 = true;
            }
            //change color of JobInfo section
            if (ObjJobVM.ObjvwJobList.EstBalance > 49999)
            {
                ViewBag.JobInfo = "LightSteelBlue";
            }
            
            else if(ObjJobVM.ObjvwJobList.EstBalance > 0 && ObjJobVM.ObjvwJobList.EstBalance < 100)
            {
                ViewBag.JobInfo = "red";
            }
            else
            {
                ViewBag.JobInfo = "Gainsboro";
            }

            if (ObjJobVM.obj_ClientCustomer.NoNTO = true || ObjJobVM.ObjvwJobList.NoNTO == true)
            {
                ViewBag.btnNotice = "Notice";
                ViewBag.Customer2 = true;
            }
             

            //if (ObjJobVM.dtGC.Rows.Count > 0)
            //{

            
            //    if (ObjJobVM.objADClientGeneralContractor.NoCallGC == true || ObjJobVM.obj_ClientCustomer.NoCallsCustGc == true || ObjJobVM.ObjvwJobList.OwnerSourceGC == true || ObjJobVM.ObjvwJobList.OwnerSourceGCFax == true || ObjJobVM.obj_ClientLienInfo.NOCustCall == true)
            //    {
            //        ViewBag.GC1 = true;
            //    }

            //}
          if(ObjJobVM.ObjJobList.SpecialInstruction == "")
            {
                ViewBag.btnSpecInstr = false;
            }
            else
            {
                ViewBag.btnSpecInstr = true;
            }

            if (ObjJobVM.ObjDocuments.Count > 0)
            {
                ViewBag.btnDocs = true;
            }
            else
            {
                ViewBag.btnDocs = false;
            }

            ViewBag.CustFax = ADvwJobInfo.NoCustFax(JobID);
            ViewBag.GCFax = ADvwJobInfo.NoGCFax(JobID);
            ViewBag.CustEmail = ADvwJobInfo.NoCustEmail(JobID);
            ViewBag.GCEmail = ADvwJobInfo.NoGCEmail(JobID);

            bool IsNoCallCustBranch = ADvwJobInfo.BranchCustflag(ObjJobVM.ObjvwJobList.BranchNumber, ObjJobVM.ObjvwJobList.ClientId);
            if (IsNoCallCustBranch == true)
            {

                ViewBag.IsNoCallCustBranch = true;
                ViewBag.Customer1 = true;
                ViewBag.Customer = false;
                ViewBag.CustEmail = true;
                ViewBag.CustFax = true;
            }
            bool IsNoCallGCBranch = ADvwJobInfo.BranchGcflag(ObjJobVM.ObjvwJobList.BranchNumber, ObjJobVM.ObjvwJobList.ClientId);
            if (IsNoCallGCBranch == true)
            {

                ViewBag.IsNoCallGCBranch = true;
                ViewBag.GC1 = true;
                ViewBag.GC = false;
                ViewBag.GCEmail = true;
                ViewBag.GCFax = true;
            }
            //else
            //{
            //    ViewBag.IsNoCallGCBranch = false;
            //    ViewBag.GC1 = true;
            //    ViewBag.GC = false;
            //    ViewBag.GCEmail = true;
            //    ViewBag.GCFax = true;
            //}

            if (ObjJobVM.dtGC.Rows.Count > 0)
            {


                if (ObjJobVM.objADClientGeneralContractor.NoCallGC == true || ObjJobVM.obj_ClientCustomer.NoCallsCustGc == true || ObjJobVM.ObjvwJobList.OwnerSourceGC == true || ObjJobVM.ObjvwJobList.OwnerSourceGCFax == true || ObjJobVM.obj_ClientLienInfo.NOCustCall == true)
                {
                    ViewBag.GC1 = true;
                    ViewBag.GC = false;
                }

            }
            Session["IsBillableLegalDesc"] = ObjJobVM.objADStateInfo.IsBillableLegalDesc;

            if (ObjJobVM.dtJobMatchList.Rows.Count < 1)
            {
                ViewBag.JobMatchList = "No Matches";
            }
            else
            {
                ViewBag.JobMatchList = "Matches(" + ObjJobVM.dtJobMatchList.Rows.Count + ")";
            }

            if (ObjJobVM.dtJobMatchListNJ.Rows.Count < 1)
            {
                ViewBag.JobMatchListNJ = "No NJ Matches";
            }
            else
            {
                ViewBag.JobMatchListNJ = "NJ Matches(" + ObjJobVM.dtJobMatchList.Rows.Count + ")";
            }

            if(ObjJobVM.ObjvwJobList.GenId > 0)
            {
                ViewBag.copygc = true;
            }
            else
            {
                ViewBag.copygc = false;
            }

            if (ObjJobVM.ObjvwJobList.OwnrId > 0)
            {
                ViewBag.copyowner = true;
            }
            else
            {
                ViewBag.copyowner = false;
            }

            if (ObjJobVM.ObjvwJobList.LenderId > 0)
            {
                ViewBag.copylender = true;
            }
            else
            {
                ViewBag.copylender = false;
            }
          
            return PartialView(GetVirtualPath("_LoadJob"), ObjJobVM);
        }
        //Read Edit Job more Info
        public ActionResult EditMoreJobInfo(string JobID)
        {
            EditJobVM objEditJob = new EditJobVM();
            objEditJob.obj_JobEdit = ADJob.ReadJob(Convert.ToInt32(JobID));

            return PartialView(GetVirtualPath("_EditMoreInfo"), objEditJob);
        }
        //Read Edit Job Info
        [HttpPost]
        public ActionResult EditJob(string JobID)
        {
            EditJobVM objEditJob = new EditJobVM();
            objEditJob.obj_JobEdit = ADJob.ReadJob(Convert.ToInt32(JobID));

            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            ADPortalUsers objADPortalUsers = new ADPortalUsers();
            objADPortalUsers = ADPortalUsers.GetUserForId(user.Id);
           
        
            if (objEditJob.obj_JobEdit.PrelimBox == true)
            {
                objEditJob.Editrbt =  "PrelimBox";
            }
            if (objEditJob.obj_JobEdit.VerifyJob == true)
            {
                objEditJob.Editrbt = "VerifyJob";
            }
            if (objEditJob.obj_JobEdit.TitleVerifiedBox == true)
            {
                objEditJob.Editrbt = "TitleVerifiedBox";
            }
            if (objEditJob.obj_JobEdit.SendAsIsBox == true)
            {
                objEditJob.Editrbt = "SendAsIsBox";
            }
            if (objEditJob.obj_JobEdit.VerifyOWOnly == true)
            {
                objEditJob.Editrbt = "VerifyOWOnly";
            }
            if (objEditJob.obj_JobEdit.VerifyOWOnlySend == true)
            {
                objEditJob.Editrbt = "VerifyOWOnlySend";
            }
            if (objEditJob.obj_JobEdit.VerifyOWOnlyTX == true)
            {
                objEditJob.Editrbt = "VerifyOWOnlyTX";
            }

            if (objEditJob.obj_JobEdit.JobState == "TX")
            {
                ViewBag.PrelimBox = false;
                ViewBag.VerifyJob = true;
                ViewBag.TitleVerifiedBox = true;
                ViewBag.SendAsIsBox = false;
                ViewBag.VerifyOWOnly = false;
                ViewBag.VerifyOWOnlySend = false;
                ViewBag.VerifyOWOnlyTX = true;
            }
            else
            {
                ViewBag.PrelimBox = true;
                ViewBag.VerifyJob = true;
                ViewBag.TitleVerifiedBox = true;
                ViewBag.SendAsIsBox = true;
                ViewBag.VerifyOWOnly = false;
                ViewBag.VerifyOWOnlySend = true;
                ViewBag.VerifyOWOnlyTX = true;
            }

            if (objADPortalUsers.RoleList == "STAFF")
            {

                ViewBag.PrelimBox = false;
                ViewBag.VerifyJob = false;
                ViewBag.TitleVerifiedBox = false;
                ViewBag.SendAsIsBox = false;
                ViewBag.VerifyOWOnly = false;
                ViewBag.VerifyOWOnlySend = false;
                ViewBag.VerifyOWOnlyTX = false;
            }
            else
            {
                ViewBag.PrelimBox = true;
                ViewBag.VerifyJob = true;
                ViewBag.TitleVerifiedBox = true;
                ViewBag.SendAsIsBox = true;
                ViewBag.VerifyOWOnly = true;
                ViewBag.VerifyOWOnlySend = true;
                ViewBag.VerifyOWOnlyTX = true;
            }

            return PartialView(GetVirtualPath("_EditJob"), objEditJob);
        }

        //Update Edit Job Info
        [HttpPost]
        public ActionResult SaveJobDetails(EditJobVM objUpdateJob)
        {
            //CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];

            if (JobInfo.LogJobChanges(objUpdateJob))
            {
                ADJob.UpdateJobEditView(objUpdateJob.obj_JobEdit,objUpdateJob);
             
            }
            JobVM objJobVM = new JobVM();
            objJobVM.ObjJobList = ADJob.ReadJob(Convert.ToInt32(objUpdateJob.obj_JobEdit.Id));
            objJobVM = JobInfo.LoadJobInfo(Convert.ToString(objUpdateJob.obj_JobEdit.Id));
            ViewBag.SelectedAlertList = Session["SelectedAlertList"];
            return PartialView(GetVirtualPath("_LoadJob"), objJobVM);
        }

        //Update Edit Job Info  
        [HttpPost]
        public ActionResult SaveEditCheckBoxes(EditJobVM objUpdateJob)
        {

            //CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
            if (JobInfo.LogJobChanges(objUpdateJob))
            {
                ADJob.UpdateEditCheckBoxes(objUpdateJob.obj_JobEdit);
            }


            //if (JobInfo.LogJobChanges(objUpdateJob, CU_Obj))
            //{
            //    ADJob.UpdateJobEditView(objUpdateJob.obj_JobEdit);
            //}
            JobVM objJobVM = new JobVM();
            objJobVM.ObjJobList = ADJob.ReadJob(Convert.ToInt32(objUpdateJob.obj_JobEdit.Id));
            objJobVM = JobInfo.LoadJobInfo(Convert.ToString(objUpdateJob.obj_JobEdit.Id));

            return PartialView(GetVirtualPath("_LoadJob"), objJobVM);
        }




        //Read Edit Job CheckBoxes
        [HttpPost]
        public ActionResult EditJobCheckboxs(string JobID)
        {
            EditJobVM objEditJob = new EditJobVM();
            objEditJob.obj_JobEdit = ADJob.ReadJob(Convert.ToInt32(JobID));

            return PartialView(GetVirtualPath("_EditJobCheckboxes"), objEditJob);
        }


        //Update Edit ClientBranch Info
        //[HttpPost]
        //public ActionResult SaveClientDetails(EditJobVM objUpdateJob)
        //{
        //    CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];



        //    CRFView.Adapters.Liens.Verifiers.WorkCard.ADClient.UpdateClientEditView(objUpdateJob.obj_ClientEdit);

        //    JobVM objJobVM = new JobVM();
        //    objJobVM.ObjClient = CRFView.Adapters.Liens.Verifiers.WorkCard.ADClient.ReadClientBranch(Convert.ToInt32(objUpdateJob.obj_ClientEdit.ClientId));
        //    //objJobVM.ObjClient = CRFView.Adapters.Liens.Verifiers.WorkCard.ADClient.ReadClientBranch(Convert.ToString(objUpdateJob.obj_ClientEdit.ClientId));
        //    objJobVM = JobInfo.LoadJobInfo(Convert.ToString(objUpdateJob.obj_JobEdit.Id));

        //    return PartialView(GetVirtualPath("_LoadJob"), objJobVM);
        //}



        // show Edit Branch modal with BranchList 
        [HttpPost]
        public ActionResult EditBranchInfo(string JobId,int ClientId,bool IsSelect)
        {

           // JobVM ObjJobVM;
            
            ObjJobVM = JobInfo.LoadJobInfo(JobId);

            //List<ADvwJobsList> list = ADClient
            IEnumerable<SelectListItem> ClientBranchList = JobInfo.GetBranchInfo(JobId);
            EditClientBranchVM objEditClientBranch = new EditClientBranchVM();
            objEditClientBranch.JobId = JobId;
            
            objEditClientBranch.IsJobView = ObjJobVM.ObjvwJobList.IsJobView;
            objEditClientBranch.ClientBranchList = ClientBranchList;
            objEditClientBranch.obj_ClientEdit = ADClient.ReadClientByClientId(ClientId);
            //objEditClientBranch.obj_ClientEdit = null;
            //objEditClientBranch.obj_ClientEdit = ADClient.GetClientDetails(Convert.ToInt32(ClientId));
            if (IsSelect == false)
            {
                objEditClientBranch.obj_ClientEdit.BranchNumber = ObjJobVM.ObjvwJobList.BranchNumber;
            }
            else
            {
                objEditClientBranch.obj_ClientEdit.BranchNumber = objEditClientBranch.obj_ClientEdit.BranchNumber;
            }
            return PartialView(GetVirtualPath("_BranchInfo"), objEditClientBranch);
        }

        //Read Branch Info by Branch name
        public ActionResult GetBranchInfo(string ClientId,string JobId)
        {
            //JobVM ObjJobVM;
            ObjJobVM = JobInfo.LoadJobInfo(JobId);
            EditClientBranchVM objEditClientBranch = new EditClientBranchVM();
            //int Id = objEditClientBranch.obj_ClientEdit.ClientId;
            objEditClientBranch.IsJobView = ObjJobVM.ObjvwJobList.IsJobView;
            objEditClientBranch.obj_ClientEdit = ADClient.GetClientDetails(Convert.ToInt32(ClientId));
            IEnumerable<SelectListItem> ClientBranchList = JobInfo.GetBranchInfo(JobId);
            objEditClientBranch.ClientBranchList = ClientBranchList;
           
                return PartialView(GetVirtualPath("_BranchInfo"), objEditClientBranch);
        }

        public ActionResult SaveEditBranchInfo(EditClientBranchVM objEditClientBranch)
        {
            //string BranchNumber = objEditClientBranch.obj_ClientEdit.BranchNumber;
            //string JobId = objEditClientBranch.JobId;
            //JobVM ObjJobVM;
            //ObjJobVM = JobInfo.LoadJobInfo(JobId);
            //objEditClientBranch.IsJobView = ObjJobVM.ObjvwJobList.IsJobView.ToString();
            int result = 0;
           
            result = JobInfo.UpdateBranchNumber(objEditClientBranch.JobId, objEditClientBranch.obj_ClientEdit.BranchNumber);
            ObjJobVM = JobInfo.LoadJobInfo(objEditClientBranch.JobId);
           
            ADClient.UpdateClientEditView(objEditClientBranch,ObjJobVM.ClientId);

            // objEditClientBranch.obj_ClientEdit = ADClient.ReadClientByClientId(ObjJobVM.ObjEditBranchVM.);


            return Json(result);
        }

        // Read Edit Branch Info
        //[HttpPost]
        //public ActionResult EditBranchInfo(string ClientId)
        //{

        //    EditClientBranchVM objEditClientBranch = new EditClientBranchVM();
         //objEditClientBranch.obj_ClientEdit = ADClient.ReadClientByClientId(Convert.ToInt32(ClientId));


        //    return PartialView(GetVirtualPath("_BranchInfo"), objEditClientBranch);
        //}




        //Show ClientLien Info
        [HttpPost]
        public ActionResult ShowClientLienInfo(string JobId,string ClientId)
        {


            ClientLienInfoVM obj = new ClientLienInfoVM();
            obj.obj_ClientLienInfo = ADClientLienInfo.ReadClientLienInfoByClientId(Convert.ToInt32(ClientId));
            return PartialView(GetVirtualPath("_ShowClientLienInfo"), obj);
        }
        [HttpPost]
        public ActionResult ShowCustomerInfo(string ClientId, string JobID)
        {
            string CustId = "";
            ADJobLegalParties obj_ADJobLegalParties = new ADJobLegalParties();
            CustomerEditVM objCustomerEditVM = new CustomerEditVM();
            //JobVM ObjJobVM;

            ObjJobVM = JobInfo.LoadJobInfo(JobID);
            objCustomerEditVM.IsJobView = ObjJobVM.ObjvwJobList.IsJobView;
            objCustomerEditVM.obj_JobEdit = ADJob.ReadJob(Convert.ToInt32(JobID));
            objCustomerEditVM.SameAsCust = objCustomerEditVM.obj_JobEdit.SameAsCust;
            if (objCustomerEditVM.IsPrimary == true)
            {
                CustId = objCustomerEditVM.obj_JobEdit.CustId.ToString();
            }
            if (objCustomerEditVM.IsPrimary == false)
            {
                obj_ADJobLegalParties = ADJobLegalParties.ReadJobLegalParties(objCustomerEditVM.obj_JobEdit.LenderId);
                CustId = obj_ADJobLegalParties.SecondaryId.ToString();
            }
            if(objCustomerEditVM.obj_JobEdit.LenderId > 0 && objCustomerEditVM.IsPrimary == false && objCustomerEditVM.SameAsCust == false)
            {
                objCustomerEditVM.DeleteButton = true;
            }


            Session["CustId"] = CustId;
            objCustomerEditVM.obj_ClientCustomer = ADClientCustomer.ReadClientCustomer(Convert.ToInt32(CustId));
            objCustomerEditVM.ClientId = ClientId;
            objCustomerEditVM.JobId = JobID;
            ViewBag.lblRecCount = "";
            objCustomerEditVM.obj_ClientCustomer.Telephone1 = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(objCustomerEditVM.obj_ClientCustomer.Telephone1);

            //ClientLienInfoVM obj = new ClientLienInfoVM();
            // obj.obj_ClientLienInfo = ADClientLienInfo.ReadClientLienInfoByClientId(Convert.ToInt32(ClientId));
            return PartialView(GetVirtualPath("_CustomerEdit"), objCustomerEditVM);
        }
        [HttpPost]
        public JsonResult ShowSearchCustomerList1(string prefix, string ClientId,string RdbBtnVal )
    {
            CustomerEditVM objCustomerEditVM = new CustomerEditVM();
            objCustomerEditVM.dtCustomeList = ADClientCustomer.GetCustomerListDt(prefix, ClientId, RdbBtnVal);
            //objCustomerEditVM.CustomerList = ADClientCustomer.GetCustomerLookUpList(objCustomerEditVM.dtCustomeList);

            List<string> getvalues = ADClientCustomer.GetCustomerLookUpList1(objCustomerEditVM.dtCustomeList);
            //return Json(objCustomerEditVM.CustomerList, JsonRequestBehavior.AllowGet);
            return Json(getvalues, JsonRequestBehavior.AllowGet);
            
        }
        public dynamic ShowSearchCustomerList(string searchInitial,string ClientId,string JobId, int LegalPartyId, bool IsPrimary,string RdbBtnVal)
        {
            CustomerEditVM objCustomerEditVM = new CustomerEditVM();
            ADJobLegalParties obj_ADJobLegalParties = new ADJobLegalParties();
            ObjJobVM = JobInfo.LoadJobInfo(JobId);
            int CUSTID = 0;
            if (IsPrimary == true)
            {
                CUSTID = ObjJobVM.ObjvwJobList.CustId;
            }
            if (IsPrimary == false)
            {
                obj_ADJobLegalParties = ADJobLegalParties.ReadJobLegalParties(Convert.ToInt32(LegalPartyId));
                CUSTID = obj_ADJobLegalParties.SecondaryId;
            }
            
            if(ObjJobVM.ObjvwJobList.IsJobView == true)
            {
                ViewBag.btnRefNum = false;
            }
            else
            {
                ViewBag.btnRefNum = true;
            }
            if(LegalPartyId > 0 && IsPrimary == false && ObjJobVM.ObjvwJobList.SameAsCust == false)
            {
                ViewBag.btnDelete = true;
            }
            else
            {
                ViewBag.btnDelete = false;
            }

            if (ObjJobVM.ObjvwJobList.SameAsCust == true)
            {
                ViewBag.CustEditFieldset = false;
            }
            else
            {
                ViewBag.CustEditFieldset = true;
            }


            objCustomerEditVM.dtCustomeList = ADClientCustomer.GetCustomerListDt(searchInitial, ClientId, RdbBtnVal);
            //objCustomerEditVM.CustomerList = ADClientCustomer.GetCustomerLookUpList(objCustomerEditVM.dtCustomeList);
            objCustomerEditVM.obj_ClientCustomer = ADClientCustomer.ReadClientCustomer(Convert.ToInt32(CUSTID));
            
            var s = objCustomerEditVM.obj_ClientCustomer.Website;
               //Regex r = new Regex(@"(https?://[^\s]+)");
            //string regex = @"((www\.|(http|https|ftp|news|file)+\:\/\/)[&#95;.a-z0-9-]+\.[a-z0-9\/&#95;:@=.+?,##%&~-]*[^.|\'|\# |!|\(|?|,| |>|<|;|\)])";
            //Regex r = new Regex(regex, RegexOptions.IgnoreCase); //objCustomerEditVM.obj_ClientCustomer.Website = r.Replace(s, "<a href=\"$1\">$1</a>").Replace("href=\"www", "href=\"http://www");
            //var test = s.Replace(" 'www' " , "" );
            //objCustomerEditVM.obj_ClientCustomer.Website = http://www
            //objCustomerEditVM.obj_ClientCustomer.Website  = r.Replace(s, "<a href=\"$1\">$1</a>").Replace("href=\"www", "href=\"http://www");
            //objCustomerEditVM.obj_ClientCustomer.Website = r.Replace(s, "<a href=\"$1\" title=\"Click to open in a new window or tab\" target=\"&#95;blank\">$1</a>").Replace("href=\"www", "href=\"http://www");
            //  objCustomerEditVM.obj_ClientCustomer.Website =  s.Replace(s, "<a href='" + s +"'>" + s +"</a>");
            // objCustomerEditVM.obj_ClientCustomer.Website = "http://www.crfsolutions.com";
           //string p = Process.Start("IExplore.exe", e.LinkText);
            objCustomerEditVM.ClientId = ClientId;
            objCustomerEditVM.JobId = JobId;
            objCustomerEditVM.IsPrimary = true;
            objCustomerEditVM.obj_ClientCustomer.Telephone1 = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(objCustomerEditVM.obj_ClientCustomer.Telephone1);
            objCustomerEditVM.obj_ClientCustomer.Fax = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(objCustomerEditVM.obj_ClientCustomer.Fax);

            Session["SelectCustId"] = CUSTID;
            //objCustomerEditVM.DeleteButton = false;
            ViewBag.lblRecCount = "Total  (" + objCustomerEditVM.CustomerList.Count + ") Records";
            //Session["IsCustId"] = objCustomerEditVM.;
            return PartialView(GetVirtualPath("_CustomerEdit"), objCustomerEditVM);
         }

        public ActionResult LoadCustomerInfo(string CustId, string JobID)
        {
            //string CustId = "";
            ADJobLegalParties obj_ADJobLegalParties = new ADJobLegalParties();
            CustomerEditVM objCustomerEditVM = new CustomerEditVM();
            //JobVM ObjJobVM;

            ObjJobVM = JobInfo.LoadJobInfo(JobID);
            objCustomerEditVM.IsJobView = ObjJobVM.ObjvwJobList.IsJobView;
            objCustomerEditVM.obj_JobEdit = ADJob.ReadJob(Convert.ToInt32(JobID));
            objCustomerEditVM.SameAsCust = objCustomerEditVM.obj_JobEdit.SameAsCust;
            objCustomerEditVM.obj_ClientCustomer = ADClientCustomer.ReadClientCustomer(Convert.ToInt32(CustId));
            objCustomerEditVM.ClientId = objCustomerEditVM.obj_JobEdit.ClientId.ToString();
            objCustomerEditVM.JobId = JobID;
            ViewBag.lblRecCount = "";
            ViewBag.copydata = "";
            Session["SelectCustId"] = CustId;
            
            //ClientLienInfoVM obj = new ClientLienInfoVM();
            // obj.obj_ClientLienInfo = ADClientLienInfo.ReadClientLienInfoByClientId(Convert.ToInt32(ClientId));
            return PartialView(GetVirtualPath("_CustomerEdit"), objCustomerEditVM);
        }

        //Add New Customer 
        public ActionResult NewCustomer(string ClientId,string CUSTID, string JobId,string RdbBtnVal ="")
        {
            CustomerEditVM objCustomerEditVM = new CustomerEditVM();
            string searchInitial = "";
            objCustomerEditVM.dtCustomeList = ADClientCustomer.GetCustomerListDt(searchInitial, ClientId, RdbBtnVal);
            objCustomerEditVM.CustomerList = ADClientCustomer.GetCustomerLookUpList(objCustomerEditVM.dtCustomeList);
            objCustomerEditVM.obj_ClientCustomer = ADClientCustomer.ReadClientCustomer(Convert.ToInt32(CUSTID));
            Session["SelectCustId"] = CUSTID;
            objCustomerEditVM.JobId = JobId;
            objCustomerEditVM.ClientId = ClientId;
            ViewBag.CustEditFieldset = true;

            return PartialView(GetVirtualPath("_CustomerEdit"), objCustomerEditVM);
        }


        //Save Customer Info
        public ActionResult SaveCustomerInfo(CustomerEditVM objCustomerEditVM)
        {
           
          
            int result = 0;
            int CustId= Convert.ToInt32(Session["SelectCustId"]);
            objCustomerEditVM.IsValidStateZip = CRF.BLL.CRFView.CRFView.ValidStateZip(objCustomerEditVM.obj_ClientCustomer.State,objCustomerEditVM.obj_ClientCustomer.PostalCode.Substring(0,5));
            objCustomerEditVM.ValidCityZip =  CRF.BLL.CRFView.CRFView.ValidCityZip(objCustomerEditVM.obj_ClientCustomer.City, objCustomerEditVM.obj_ClientCustomer.PostalCode.Substring(0, 5));


            if (objCustomerEditVM.obj_ClientCustomer.IsNonUSAddr == false && objCustomerEditVM.ValidCityZip == false)
            {
                result = 1;
                return Json(result);
            }

            if (objCustomerEditVM.obj_ClientCustomer.IsNonUSAddr == false && objCustomerEditVM.IsValidStateZip == false)
            {
                result = 0;
                return Json(result);
            }

            var regex = new Regex(@"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$");


            //var validateemail = new Regex(@"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z");
            if (objCustomerEditVM.obj_ClientCustomer.Email != null)
            {
               bool isValid = regex.IsMatch(objCustomerEditVM.obj_ClientCustomer.Email);
                if (isValid == false)
                {
                    //ModelState.AddModelError("Email", "Invalid Email.");
                    result = 2;
                    return Json(result);
                }
            }
            //objCustomerEditVM.obj_JobEdit = ADJob.ReadJob(Convert.ToInt32(objCustomerEditVM.JobId));
            //int CustId = Convert.ToInt32(Session["CustId"]);
            result = ADClientCustomer.SaveCustomer(objCustomerEditVM, CustId);

            if (result == 0)
            {
                return PartialView(GetVirtualPath("_CustomerEdit"), objCustomerEditVM);
            }
            return Json(result);
        }

        //Delete Customer Info
        public ActionResult DeleteCustomerInfo(CustomerEditVM objCustomerEditVM)
        {


            int result = 0;


            //objCustomerEditVM.obj_JobEdit = ADJob.ReadJob(Convert.ToInt32(objCustomerEditVM.JobId));
            int CustId = Convert.ToInt32(Session["SelectCustId"]);
            result = ADClientCustomer.DeleteCustomer(objCustomerEditVM, CustId);

            return Json(result);
        }


        //Show GC Modal
        public ActionResult ShowGCInfo(string ClientId, string JobID, string searchInitial, [Optional] string LegalPartyId, [Optional] bool IsPrimary, string RdbBtnVal)
        {

            GCEditVM objGCEditVM = new GCEditVM();
            ObjJobVM = JobInfo.LoadJobInfo(JobID);
            Session["NOCustCall"] = ObjJobVM.obj_ClientLienInfo.NOCustCall;
            ADJobLegalParties obj_ADJobLegalParties = new ADJobLegalParties();
            int GenId = 0;
            //int LegalPartyId = 0;
            //bool IsPrimary = true;
            if(IsPrimary == true)
            {

                GenId = ObjJobVM.ObjvwJobList.GenId;
               
            }

           
            if (IsPrimary == false)
            {
                obj_ADJobLegalParties = ADJobLegalParties.ReadJobLegalParties(Convert.ToInt32(LegalPartyId));
                GenId = obj_ADJobLegalParties.SecondaryId;
               
               // return PartialView(GetVirtualPath("_GCEdit"), objGCEditVM);
            }

            objGCEditVM.ClientId = ClientId;
            objGCEditVM.JobId = JobID;
            objGCEditVM.objADClientGeneralContractor = ADClientGeneralContractor.ReadClientGeneralContractor(GenId);
            objGCEditVM.dtGCList = ADClientGeneralContractor.GetGCListDt(searchInitial, ClientId, RdbBtnVal);
            objGCEditVM.GCList = ADClientGeneralContractor.GetGCLookUpList(objGCEditVM.dtGCList);


            

            if (GenId > 0 && ObjJobVM.ObjvwJobList.SameAsCust == false)
            {
                objGCEditVM.btnDelete = true;
            }

            else
            {
                objGCEditVM.btnDelete = false;
            }

            if (ObjJobVM.ObjvwJobList.JobState == "TX")
            {
                //objGCEditVM.MakeSameAsCustomerchkbox = false;
                ViewBag.MakeSameAsCustomerchkbox = false;
            }

            if (IsPrimary == false)
            {
                
                ViewBag.MakeSameAsCustomerchkbox = false;
                ViewBag.GCUnverifiedchkbox = false;
                return PartialView(GetVirtualPath("_GCEdit"), objGCEditVM);
            }
            else
            {
                ViewBag.GCUnverifiedchkbox = true;
            }

            ViewBag.MakeSameAsCustomerchkbox = true;
            if (ObjJobVM.ObjvwJobList.SameAsGC == true)
            {
                objGCEditVM.SameAsGC = true;
            }

            objGCEditVM.IsSameAsCustomerChecked = ObjJobVM.ObjvwJobList.SameAsCust;
            objGCEditVM.IsGCUnverified = ObjJobVM.ObjvwJobList.IsGCUnverified;
            objGCEditVM.IsPrimary = IsPrimary;
             //objGCEditVM.MakeSameAsCustomerchked = ObjJobVM.ObjvwJobList.SameAsCust;
             // objGCEditVM.IsGCUnverified = ObjJobVM.ObjvwJobList.IsGCUnverified;

            Session["GenId"] = GenId;
            return PartialView(GetVirtualPath("_GCEdit"), objGCEditVM);
        }

        //public ActionResult ShowSearchGCList(string searchInitial, string ClientId, string JobId)
        //{
        //    GCEditVM objGCEditVM = new GCEditVM();
        //    objGCEditVM.dtGCList = ADClientGeneralContractor.GetGCListDt(searchInitial, ClientId);
        //    objGCEditVM.GCList = ADClientGeneralContractor.GetGCLookUpList(objGCEditVM.dtGCList);
        //    //objGCEditVM.obj_ClientCustomer = ADClientCustomer.ReadClientCustomer(Convert.ToInt32(ClientId));
        //    objGCEditVM.obj_JobEdit = ADJob.ReadJob(Convert.ToInt32(JobId));
        //   // objGCEditVM.ClientId = ClientId;
        //    //objGCEditVM.IsPrimary = true;
        //    //objCustomerEditVM.DeleteButton = false;
        //    //ViewBag.lblRecCount = "Total  (" + objCustomerEditVM.CustomerList.Count + ") Records";
        //    //Session["IsCustId"] = objCustomerEditVM.;
        //    return PartialView(GetVirtualPath("_GCEdit"), objGCEditVM);
        //}

        public JsonResult ShowSearchGCList1(string prefix, string ClientId, string RdbBtnVal)
      {
            GCEditVM objGCEditVM = new GCEditVM();
            objGCEditVM.dtGCList = ADClientGeneralContractor.GetGCListDt(prefix, ClientId, RdbBtnVal);
            //objCustomerEditVM.CustomerList = ADClientCustomer.GetCustomerLookUpList(objCustomerEditVM.dtCustomeList);

            List<string> getvalues = ADClientGeneralContractor.GetGCLookUpList1(objGCEditVM.dtGCList);
            //return Json(objCustomerEditVM.CustomerList, JsonRequestBehavior.AllowGet);
            return Json(getvalues, JsonRequestBehavior.AllowGet);

        }
        public ActionResult LoadGCInfo(string GenId, string JobID, [Optional] bool IsPrimary)
        {
            GCEditVM objGCEditVM = new GCEditVM();

            objGCEditVM.objADClientGeneralContractor = ADClientGeneralContractor.ReadClientGeneralContractor(Convert.ToInt32(GenId));
            objGCEditVM.ClientId = objGCEditVM.objADClientGeneralContractor.ClientId.ToString();
            objGCEditVM.JobId = JobID;
            objGCEditVM.btnDelete = false;
            Session["GenId"] = GenId;
            objGCEditVM.IsPrimary = IsPrimary;

            return PartialView(GetVirtualPath("_GCEdit"), objGCEditVM);
        }

        public ActionResult NewGC(string GenId,string ClientId, string JobId, string RdbBtnVal = "")
        {
             GCEditVM objGCEditVM = new GCEditVM();
            string searchInitial = "";
            objGCEditVM.dtGCList = ADClientGeneralContractor.GetGCListDt(searchInitial, ClientId, RdbBtnVal);
            objGCEditVM.GCList = ADClientGeneralContractor.GetGCLookUpList(objGCEditVM.dtGCList);
            objGCEditVM.objADClientGeneralContractor = ADClientGeneralContractor.ReadClientGeneralContractor(Convert.ToInt32(GenId));

            Session["GenId"] = GenId;
            objGCEditVM.JobId = JobId;
            objGCEditVM.ClientId = ClientId;
            //ViewBag.CustEditFieldset = true;

            return PartialView(GetVirtualPath("_GCEdit"), objGCEditVM);
        }


        public ActionResult SameAsCustomerChecked(string ClientId, string JobId,bool IsPrimary,string RdbBtnVal = "")
        {
            GCEditVM objGCEditVM = new GCEditVM();
            string searchInitial = "";
            string ClientNoCustCall = Session["NOCustCall"].ToString();
            objGCEditVM.myClientNoCustCall = true; //Convert.ToBoolean(ClientNoCustCall);
            objGCEditVM.obj_JobEdit = ADJob.ReadJob(Convert.ToInt32(JobId));
            objGCEditVM.objADClientGeneralContractor = ADClientGeneralContractor.ReadClientGeneralContractor(Convert.ToInt32(0));
            objGCEditVM.obj_ClientCustomer = ADClientCustomer.ReadClientCustomer(objGCEditVM.obj_JobEdit.CustId);
            objGCEditVM.objADClientGeneralContractor.GeneralContractor = objGCEditVM.obj_ClientCustomer.ClientCustomer;
            objGCEditVM.objADClientGeneralContractor.ContactName = objGCEditVM.obj_ClientCustomer.ContactName;
            objGCEditVM.objADClientGeneralContractor.Telephone1 = objGCEditVM.obj_ClientCustomer.Telephone1;
            objGCEditVM.objADClientGeneralContractor.Fax = objGCEditVM.obj_ClientCustomer.Fax;
            objGCEditVM.objADClientGeneralContractor.AddressLine1 = objGCEditVM.obj_ClientCustomer.AddressLine1;
            objGCEditVM.objADClientGeneralContractor.AddressLine2 = objGCEditVM.obj_ClientCustomer.AddressLine2;
            objGCEditVM.objADClientGeneralContractor.City = objGCEditVM.obj_ClientCustomer.City;
            objGCEditVM.objADClientGeneralContractor.State = objGCEditVM.obj_ClientCustomer.State;
            objGCEditVM.objADClientGeneralContractor.PostalCode = objGCEditVM.obj_ClientCustomer.PostalCode;
            objGCEditVM.objADClientGeneralContractor.SpecialInstruction = objGCEditVM.obj_ClientCustomer.SpecialInstruction;
            objGCEditVM.objADClientGeneralContractor.Email = objGCEditVM.obj_ClientCustomer.Email;
            objGCEditVM.objADClientGeneralContractor.NoCallGC = objGCEditVM.obj_ClientCustomer.NoCallCust;
            objGCEditVM.objADClientGeneralContractor.NoEmail = objGCEditVM.obj_ClientCustomer.NoEmail;
            objGCEditVM.objADClientGeneralContractor.NoFax = objGCEditVM.obj_ClientCustomer.NoFax;
            objGCEditVM.objADClientGeneralContractor.GCLicNum = objGCEditVM.obj_ClientCustomer.Website;

            string GenId = Session["GenId"].ToString();
            Session["GenId"] = GenId;
            objGCEditVM.JobId = JobId;
            objGCEditVM.ClientId = ClientId;
            objGCEditVM.IsPrimary = IsPrimary;
            //ViewBag.CustEditFieldset = true;

            return PartialView(GetVirtualPath("_GCEdit"), objGCEditVM);
        }

        //Save GC Info
        public ActionResult SaveGCInfo(GCEditVM objGCEditVM,bool IsSameAsCustomerCheckedvisible)
        {


            int result = 0;
            int GenId = Convert.ToInt32(Session["GenId"]);

            objGCEditVM.IsValidStateZip = CRF.BLL.CRFView.CRFView.ValidStateZip(objGCEditVM.objADClientGeneralContractor.State,objGCEditVM.objADClientGeneralContractor.PostalCode.Substring(0, 5));
            objGCEditVM.ValidCityZip = CRF.BLL.CRFView.CRFView.ValidCityZip(objGCEditVM.objADClientGeneralContractor.City, objGCEditVM.objADClientGeneralContractor.PostalCode.Substring(0, 5));

            //objCustomerEditVM.obj_JobEdit = ADJob.ReadJob(Convert.ToInt32(objCustomerEditVM.JobId));
            //int CustId = Convert.ToInt32(Session["CustId"]);

            if (objGCEditVM.objADClientGeneralContractor.IsNonUSAddr == false && objGCEditVM.IsValidStateZip == false)
            {
                result = 0;
                return Json(0);
            }

            var regex = new Regex(@"^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$");
                          
                
            //var validateemail = new Regex(@"\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z");
            if (objGCEditVM.objADClientGeneralContractor.Email != null)
            {
                bool isValid = regex.IsMatch(objGCEditVM.objADClientGeneralContractor.Email);
                if (isValid == false)

                {
                    //ModelState.AddModelError("Email", "Invalid Email.");
                    result = 2;
                return Json(result);
            }
            }
            result = ADClientGeneralContractor.SaveGC(objGCEditVM, GenId, IsSameAsCustomerCheckedvisible);


            if (result == 0)
            {
                return PartialView(GetVirtualPath("_GCEdit"), objGCEditVM);
            }
            return Json(result);
        }

        // Check ToDelete Info
        public ActionResult CheckToDeleteInfo(string JobId,bool IsPrimary,string TypeCode)
        {
            bool result;
            int PrimaryFlag = 0;
            if (IsPrimary == true)
            {
                PrimaryFlag = 1;
            }
            else
            {
                PrimaryFlag = 0;
            }


            result = CRF.BLL.Queries.IsNoticePending(JobId, TypeCode, PrimaryFlag);

            return Json(result);
        }
        // Delete GC Info
        public ActionResult DeleteGCInfo(GCEditVM objGCEditVM)
        {
             

            int result = 0;


            //objCustomerEditVM.obj_JobEdit = ADJob.ReadJob(Convert.ToInt32(objCustomerEditVM.JobId));
            int GenId = Convert.ToInt32(Session["GenId"]);

            result = ADClientGeneralContractor.DeleteGC(objGCEditVM, GenId);

            return Json(result);
        }



        [HttpPost]
        public ActionResult ShowOwnerInfo(string searchInitial, string ClientId, string JobID, int LegalPartyId, bool IsPrimary, string RdbBtnVal)
        {
            OwnerEditVM objOwnerEditVM = new OwnerEditVM();
            //int LegalPartyId =0;
            int PrimaryOwnerId = 0;
            ObjJobVM = JobInfo.LoadJobInfo(JobID);
            //objOwnerEditVM.objJobEdit = ADJob.ReadJob(Convert.ToInt32(JobID));

            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            StateInfo mystate = new StateInfo();
            mystate = CRF.BLL.Providers.LiensDataEntry.GetStateInfoforState(user, ObjJobVM.ObjvwJobList.JobState);


            if (IsPrimary == true)
            {
                 LegalPartyId = ObjJobVM.ObjvwJobList.OwnrId;
                
            }
            
            else
            {
                 PrimaryOwnerId = ObjJobVM.ObjvwJobList.OwnrId;
            }
            if(LegalPartyId > 0)
            {
                ViewBag.btnOwDelete = true;

                if(IsPrimary == true)
                {
                    ViewBag.IschkChangeOWToLE = false;
                    ViewBag.IschkChangeToPrimary = false;
                    ViewBag.IschkChangeOWToDE = false;


                }
                else
                {
                    ViewBag.IschkChangeOWToLE = true;
                    ViewBag.IschkChangeToPrimary = true;
                    ViewBag.IschkChangeOWToDE = true;
                }
            }

            else
            {
                ViewBag.btnDelete = false;
                ViewBag.IschkChangeOWToLE = false;
                ViewBag.IschkChangeToPrimary = false;
                ViewBag.IschkChangeOWToDE = false;
            }
             Session["OwnrID"] = LegalPartyId;
            Session["PrimaryOwnerId"] = PrimaryOwnerId;
            objOwnerEditVM.ClientId = ClientId;
            objOwnerEditVM.JobId = JobID;
            objOwnerEditVM.objADJobLegalParties = ADJobLegalParties.ReadJobLegalParties(LegalPartyId);
            objOwnerEditVM.dtOWList = ADJobLegalParties.GetOWListDt(searchInitial, ClientId, RdbBtnVal);
            objOwnerEditVM.OWList = ADJobLegalParties.GetOWLookUpList(objOwnerEditVM.dtOWList);

            if(IsPrimary == false)
            {
                ViewBag.IschkMakeSameAsGc = false;
                return PartialView(GetVirtualPath("_OwnerEdit"), objOwnerEditVM);
            }

            ViewBag.IschkMakeSameAsGc = true;
            objOwnerEditVM.chkMakeSameAsGc = ObjJobVM.ObjvwJobList.SameAsGC;

            if(ObjJobVM.ObjvwJobList.JobState == "TX")
            {
                ViewBag.IschkMakeSameAsGc = false;
            }


            if(mystate.MLAgent == true)
            {
                ViewBag.IsMLAgent = true;
                
            }
            else
            {
                ViewBag.IsMLAgent = false;
            }

            Session["IsMLAgentValue"] = ViewBag.IsMLAgent;
            if (ObjJobVM.ObjvwJobList.SameAsGC == true)
            {

                ViewBag.btnDelete = false;
            }
            else
            {
                ViewBag.btnDelete = true;
                
            }
            Session["IsPrimary"] = IsPrimary;


            return PartialView(GetVirtualPath("_OwnerEdit"), objOwnerEditVM);
        }
        public JsonResult ShowSearchOwnerList(string prefix, string ClientId, string RdbBtnVal)
        {
            OwnerEditVM objOwnerEditVM = new OwnerEditVM();
            objOwnerEditVM.dtOWList = ADJobLegalParties.GetOWListDt(prefix, ClientId, RdbBtnVal);
            List<string> getvalues = ADJobLegalParties.GetOWLookUpList1(objOwnerEditVM.dtOWList);
            
            return Json(getvalues, JsonRequestBehavior.AllowGet);
            
        }
        public ActionResult LoadOWInfo(string OwnerId, string JobID)
        {
            OwnerEditVM objOwnerEditVM = new OwnerEditVM();
            objOwnerEditVM.objJobEdit = ADJob.ReadJob(Convert.ToInt32(JobID));

            objOwnerEditVM.objADJobLegalParties = ADJobLegalParties.ReadJobLegalParties(Convert.ToInt32(OwnerId));
            int LegalPartyId = Convert.ToInt32(Session["OwnrID"]);
            bool IsPrimary = Convert.ToBoolean(Session["IsPrimary"]);

            if (LegalPartyId > 0)
            {
                ViewBag.btnOwDelete = true;

                if (IsPrimary == true)
                {
                    ViewBag.IschkChangeOWToLE = false;
                    ViewBag.IschkChangeToPrimary = false;
                    ViewBag.IschkChangeOWToDE = false;


                }
                else
                {
                    ViewBag.IschkChangeOWToLE = true;
                    ViewBag.IschkChangeToPrimary = true;
                    ViewBag.IschkChangeOWToDE = true;
                }
            }

            else
            {
                ViewBag.btnOwDelete = false;
                ViewBag.IschkChangeOWToLE = false;
                ViewBag.IschkChangeToPrimary = false;
                ViewBag.IschkChangeOWToDE = false;
            }
            ViewBag.IsMLAgent = Session["IsMLAgentValue"];
            objOwnerEditVM.ClientId = objOwnerEditVM.objJobEdit.ClientId.ToString();
            objOwnerEditVM.JobId = JobID;
            string OwnrID = Session["OwnrID"].ToString();
            Session["OwnrID"] = OwnrID;
            ViewBag.btnOwDelete = false;
            return PartialView(GetVirtualPath("_OwnerEdit"), objOwnerEditVM);
        }
        //SameAsGc Checked
        public ActionResult MakeSameAsGcChecked(string ClientId, string JobId, bool IsPrimary, string RdbBtnVal = "")
        {
            OwnerEditVM objOwnerEditVM = new OwnerEditVM();
            string searchInitial = "";
            int LegalPartyId = Convert.ToInt32(Session["OwnrID"]);
            IsPrimary = Convert.ToBoolean(Session["IsPrimary"]);

            if (LegalPartyId > 0)
            {
                ViewBag.btnOwDelete = true;

                if (IsPrimary == true)
                {
                    ViewBag.IschkChangeOWToLE = false;
                    ViewBag.IschkChangeToPrimary = false;
                    ViewBag.IschkChangeOWToDE = false;


                }
                else
                {
                    ViewBag.IschkChangeOWToLE = true;
                    ViewBag.IschkChangeToPrimary = true;
                    ViewBag.IschkChangeOWToDE = true;
                }
            }

            else
            {
                ViewBag.btnOwDelete = false;
                ViewBag.IschkChangeOWToLE = false;
                ViewBag.IschkChangeToPrimary = false;
                ViewBag.IschkChangeOWToDE = false;
            }
            ViewBag.IsMLAgent = Session["IsMLAgentValue"];
            objOwnerEditVM.obj_JobEdit = ADJob.ReadJob(Convert.ToInt32(JobId));
            objOwnerEditVM.objADJobLegalParties = ADJobLegalParties.ReadJobLegalParties(Convert.ToInt32(0));
            objOwnerEditVM.objADClientGeneralContractor = ADClientGeneralContractor.ReadClientGeneralContractor(objOwnerEditVM.obj_JobEdit.GenId);
            objOwnerEditVM.objADJobLegalParties.AddressName = objOwnerEditVM.objADClientGeneralContractor.GeneralContractor;
            objOwnerEditVM.objADJobLegalParties.Telephone1 = objOwnerEditVM.objADClientGeneralContractor.Telephone1;
            objOwnerEditVM.objADJobLegalParties.Fax = objOwnerEditVM.objADClientGeneralContractor.Fax;
            objOwnerEditVM.objADJobLegalParties.AddressLine1 = objOwnerEditVM.objADClientGeneralContractor.AddressLine1;
            objOwnerEditVM.objADJobLegalParties.AddressLine2 = objOwnerEditVM.objADClientGeneralContractor.AddressLine2;
            objOwnerEditVM.objADJobLegalParties.City = objOwnerEditVM.objADClientGeneralContractor.City;
            objOwnerEditVM.objADJobLegalParties.State = objOwnerEditVM.objADClientGeneralContractor.State;
            objOwnerEditVM.objADJobLegalParties.PostalCode = objOwnerEditVM.objADClientGeneralContractor.PostalCode;

            string OwnrID = Session["OwnrID"].ToString();
            Session["OwnrID"] = OwnrID;
            objOwnerEditVM.JobId = JobId;
            objOwnerEditVM.ClientId = ClientId;
            ViewBag.IschkMakeSameAsGc = true;
            //ViewBag.CustEditFieldset = true;

            return PartialView(GetVirtualPath("_OwnerEdit"), objOwnerEditVM);
        }

        //Copy Job Address Checked
        public ActionResult CopyJobAddressChecked(string ClientId, string JobId, bool CopyJobAddressVal, string RdbBtnVal = "")
        {
            OwnerEditVM objOwnerEditVM = new OwnerEditVM();
            string searchInitial = "";
            int LegalPartyId = Convert.ToInt32(Session["OwnrID"]);
            bool IsPrimary = Convert.ToBoolean(Session["IsPrimary"]);

            if (LegalPartyId > 0)
            {
                ViewBag.btnOwDelete = true;

                if (IsPrimary == true)
                {
                    ViewBag.IschkChangeOWToLE = false;
                    ViewBag.IschkChangeToPrimary = false;
                    ViewBag.IschkChangeOWToDE = false;


                }
                else
                {
                    ViewBag.IschkChangeOWToLE = true;
                    ViewBag.IschkChangeToPrimary = true;
                    ViewBag.IschkChangeOWToDE = true;
                }
            }

            else
            {
                ViewBag.btnOwDelete = false;
                ViewBag.IschkChangeOWToLE = false;
                ViewBag.IschkChangeToPrimary = false;
                ViewBag.IschkChangeOWToDE = false;
            }
            ViewBag.IsMLAgent = Session["IsMLAgentValue"];
            objOwnerEditVM.obj_JobEdit = ADJob.ReadJob(Convert.ToInt32(JobId));
            objOwnerEditVM.objADJobLegalParties = ADJobLegalParties.ReadJobLegalParties(LegalPartyId);

            if (CopyJobAddressVal == true)
            {
                objOwnerEditVM.objADJobLegalParties.AddressName = objOwnerEditVM.obj_JobEdit.JobName;
                objOwnerEditVM.objADJobLegalParties.AddressLine1 = objOwnerEditVM.obj_JobEdit.JobAdd1;
                objOwnerEditVM.objADJobLegalParties.AddressLine2 = objOwnerEditVM.obj_JobEdit.JobAdd2;
                objOwnerEditVM.objADJobLegalParties.City = objOwnerEditVM.obj_JobEdit.JobCity;
                objOwnerEditVM.objADJobLegalParties.State = objOwnerEditVM.obj_JobEdit.JobState;
                objOwnerEditVM.objADJobLegalParties.PostalCode = objOwnerEditVM.obj_JobEdit.JobZip;
            }
            else
            {
                objOwnerEditVM.objADJobLegalParties.AddressName = objOwnerEditVM.objADJobLegalParties.AddressName;
                objOwnerEditVM.objADJobLegalParties.AddressLine1 = objOwnerEditVM.objADJobLegalParties.AddressLine1;
                objOwnerEditVM.objADJobLegalParties.AddressLine2 = objOwnerEditVM.objADJobLegalParties.AddressLine2;
                objOwnerEditVM.objADJobLegalParties.City = objOwnerEditVM.objADJobLegalParties.City;
                objOwnerEditVM.objADJobLegalParties.State = objOwnerEditVM.objADJobLegalParties.State;
                objOwnerEditVM.objADJobLegalParties.PostalCode = objOwnerEditVM.objADJobLegalParties.PostalCode;
            }
           

            string OwnrID = Session["OwnrID"].ToString();
            Session["OwnrID"] = OwnrID;
            objOwnerEditVM.JobId = JobId;
            objOwnerEditVM.ClientId = ClientId;
            ViewBag.IschkMakeSameAsGc = true;
            //ViewBag.CustEditFieldset = true;

            return PartialView(GetVirtualPath("_OwnerEdit"), objOwnerEditVM);
        }

        //Save Owner Info
        public ActionResult SaveOwnerInfo(OwnerEditVM objOwnerEditVM)
        {


            int result = 0;
            
            int OwnrID = Convert.ToInt32(Session["OwnrID"]);
            int PrimaryOwnerId = Convert.ToInt32(Session["PrimaryOwnerId"]);
            objOwnerEditVM.IsValidStateZip = CRF.BLL.CRFView.CRFView.ValidStateZip(objOwnerEditVM.objADJobLegalParties.State, objOwnerEditVM.objADJobLegalParties.PostalCode.Substring(0, 5));
            objOwnerEditVM.ValidCityZip = CRF.BLL.CRFView.CRFView.ValidCityZip(objOwnerEditVM.objADJobLegalParties.City, objOwnerEditVM.objADJobLegalParties.PostalCode.Substring(0, 5));


            if (objOwnerEditVM.objADJobLegalParties.IsNonUSAddr == false && objOwnerEditVM.IsValidStateZip == false)
            {
                result = 0;
                return Json(0);
            }

            if (objOwnerEditVM.objADJobLegalParties.IsNonUSAddr == false && objOwnerEditVM.ValidCityZip == false)
            {
                result = -1;
                return Json(result);
            }



            result = ADJobLegalParties.SaveOwner(objOwnerEditVM, OwnrID, PrimaryOwnerId);


            if (result == 0)
            {
                return PartialView(GetVirtualPath("_OwnerEdit"), objOwnerEditVM);
            }
            return Json(result);
        }



        // Delete Owner Info
        public ActionResult DeleteOwnerInfo(OwnerEditVM objOwnerEditVM)
        {


            int result = 0;
            int OwnrID = Convert.ToInt32(Session["OwnrID"]);

            //objCustomerEditVM.obj_JobEdit = ADJob.ReadJob(Convert.ToInt32(objCustomerEditVM.JobId));
           
            result = ADJobLegalParties.DeleteOwner(objOwnerEditVM, OwnrID);

            return Json(result);
        }

        //Show Lender Info
        public ActionResult ShowLenderInfo( string ClientId, string JobID, [Optional] int LegalPartyId, [Optional] bool IsPrimary,string RdbBtnVal ="")
        {
            OwnerEditVM objOwnerEditVM = new OwnerEditVM();

            ObjJobVM = JobInfo.LoadJobInfo(JobID);
           


            if (IsPrimary == true)
            {
                LegalPartyId = ObjJobVM.ObjvwJobList.LenderId;
                objOwnerEditVM.IschkSuretyBox = true;

            }
            else
            {
                objOwnerEditVM.IschkSuretyBox = false;
            }

            if(LegalPartyId > 0)
            {
                objOwnerEditVM.btnDelete = true;
                if (objOwnerEditVM.IsPrimary == true)
                {
                   
                    objOwnerEditVM.IschkChangeOWToLE = false;

                }
                else
                {
                    objOwnerEditVM.IschkChangeOWToLE = true;
                }
            }
            else
            {
                objOwnerEditVM.btnDelete = false;
                objOwnerEditVM.IschkChangeOWToLE = false;
            }
            

            Session["LegalPartyId"] = LegalPartyId;
            Session["LenderIsPrimary"] = IsPrimary;
            objOwnerEditVM.ClientId = ClientId;
            objOwnerEditVM.JobId = JobID;
            objOwnerEditVM.objADJobLegalParties = ADJobLegalParties.ReadJobLegalParties(LegalPartyId);

           // objOwnerEditVM.dtLenderList = ADJobLegalParties.GetLenderListDt(searchInitial, ClientId, RdbBtnVal);
            // objOwnerEditVM.LenderList = ADJobLegalParties.GetOWLookUpList(objOwnerEditVM.dtLenderList);

            return PartialView(GetVirtualPath("_LenderEdit"), objOwnerEditVM);
        }

        public JsonResult ShowSearchLenderList(string prefix, string ClientId, string RdbBtnVal)
        {
            OwnerEditVM objOwnerEditVM = new OwnerEditVM();

            objOwnerEditVM.dtLenderList = ADJobLegalParties.GetLenderListDt(prefix, ClientId, RdbBtnVal);
            List<string> getvalues = ADJobLegalParties.GetOWLookUpList1(objOwnerEditVM.dtLenderList);

           

            return Json(getvalues, JsonRequestBehavior.AllowGet);

        }

        public ActionResult LoadLenderInfo(string LenderId, string JobID)
        {
            OwnerEditVM objOwnerEditVM = new OwnerEditVM();
            int LegalPartyId = Convert.ToInt32(Session["LegalPartyId"]);
            bool IsPrimary = Convert.ToBoolean(Session["LenderIsPrimary"]);
            objOwnerEditVM.objJobEdit = ADJob.ReadJob(Convert.ToInt32(JobID));
            if (IsPrimary == true)
            {
                
                objOwnerEditVM.IschkSuretyBox = true;

            }
            else
            {
                objOwnerEditVM.IschkSuretyBox = false;
            }

            if (LegalPartyId > 0)
            {
                objOwnerEditVM.btnDelete = true;
                if (objOwnerEditVM.IsPrimary == true)
                {

                    objOwnerEditVM.IschkChangeOWToLE = false;

                }
                else
                {
                    objOwnerEditVM.IschkChangeOWToLE = true;
                }
            }
            else
            {
                objOwnerEditVM.btnDelete = false;
                objOwnerEditVM.IschkChangeOWToLE = false;
            }
            objOwnerEditVM.objADJobLegalParties = ADJobLegalParties.ReadJobLegalParties(Convert.ToInt32(LenderId));
            objOwnerEditVM.ClientId = objOwnerEditVM.objJobEdit.ClientId.ToString();
            objOwnerEditVM.JobId = JobID;
            //Session["GenId"] = GenId;
            return PartialView(GetVirtualPath("_LenderEdit"), objOwnerEditVM);
        }


        //New Lender Info
        public ActionResult NewLender(string ClientId, int LenderId, string RdbBtnVal = "")
        {
            OwnerEditVM objOwnerEditVM = new OwnerEditVM();
            string searchInitial = "";
            bool IsPrimary = false;
            objOwnerEditVM.objADJobLegalParties = ADJobLegalParties.ReadJobLegalParties(LenderId);

            objOwnerEditVM.dtLenderList = ADJobLegalParties.GetLenderListDt(searchInitial, ClientId,RdbBtnVal);
            objOwnerEditVM.LenderList = ADJobLegalParties.GetOWLookUpList(objOwnerEditVM.dtLenderList);
            Session["LegalPartyId"] = LenderId;
            // ViewBag.CustEditFieldset = true;

            return PartialView(GetVirtualPath("_LenderEdit"), objOwnerEditVM);
        }
        //Save Owner Info
        public ActionResult SaveLenderInfo(OwnerEditVM objOwnerEditVM)
        {


            int result = 0;

            int LenderId = Convert.ToInt32(Session["LegalPartyId"]);
            int PrimaryOwnerId = Convert.ToInt32(Session["PrimaryOwnerId"]);
            objOwnerEditVM.IsValidStateZip = CRF.BLL.CRFView.CRFView.ValidStateZip(objOwnerEditVM.objADJobLegalParties.State, objOwnerEditVM.objADJobLegalParties.PostalCode.Substring(0, 5));
            objOwnerEditVM.ValidCityZip = CRF.BLL.CRFView.CRFView.ValidCityZip(objOwnerEditVM.objADJobLegalParties.City, objOwnerEditVM.objADJobLegalParties.PostalCode.Substring(0, 5));



            if (objOwnerEditVM.objADJobLegalParties.IsNonUSAddr == false && objOwnerEditVM.IsValidStateZip == false)
            {
                result = 0;
                return Json(0);
            }
            
            if (objOwnerEditVM.objADJobLegalParties.IsNonUSAddr == false && objOwnerEditVM.ValidCityZip == false)
            {
                result = -1;
                return Json(result);
            }



            result = ADJobLegalParties.SaveLender(objOwnerEditVM, LenderId);


            if (result == 0)
            {
                return PartialView(GetVirtualPath("_LenderEdit"), objOwnerEditVM);
            }
            return Json(result);
        }

        // Delete Lender Info
       public ActionResult DeleteLenderInfo(OwnerEditVM objOwnerEditVM)
        {


            int result = 0;
            int LenderId = Convert.ToInt32(Session["LegalPartyId"]);
            //objCustomerEditVM.obj_JobEdit = ADJob.ReadJob(Convert.ToInt32(objCustomerEditVM.JobId));

            result = ADJobLegalParties.DeleteLender(objOwnerEditVM, LenderId);

            return Json(result);
        }

        //Show Designee
        public ActionResult ShowDesigneeInfo( string JobID, string ClientId,string searchInitial, [Optional] bool IsPrimary, [Optional] string LegalPartyId,string RdbBtnVal)
        {
           OwnerEditVM objOwnerEditVM = new OwnerEditVM();
            ObjJobVM = JobInfo.LoadJobInfo(JobID);
           string LegalPartyIds = LegalPartyId;
            if (IsPrimary == true)
            {
                LegalPartyIds = ObjJobVM.ObjvwJobList.DesigneeId.ToString();
            }
           objOwnerEditVM.objADJobLegalParties = ADJobLegalParties.ReadJobLegalPartiesByCondition(LegalPartyId, IsPrimary);
            
            if(Convert.ToInt32(LegalPartyIds) > 0)
            {
                objOwnerEditVM.btnDelete = true;
            }
            else
            {
                objOwnerEditVM.btnDelete = false;
            }
            objOwnerEditVM.ClientId = ClientId;
            objOwnerEditVM.JobId = JobID;
            Session["DesigneeID"] = LegalPartyIds;
            //Session["IsPrimary"] = IsPrimary;
            objOwnerEditVM.IsPrimaryDesignee = IsPrimary;
            objOwnerEditVM.dtDesigneeList = ADJobLegalParties.GetDesigneeListDt(searchInitial, ClientId, RdbBtnVal);
            objOwnerEditVM.DesigneeList = ADJobLegalParties.GetOWLookUpList(objOwnerEditVM.dtDesigneeList);

            return PartialView(GetVirtualPath("_DesigneeEdit"), objOwnerEditVM);
        }


        public JsonResult ShowSearchDesigneeList(string prefix, string ClientId, string RdbBtnVal)
        {
            OwnerEditVM objOwnerEditVM = new OwnerEditVM();
            objOwnerEditVM.dtDesigneeList = ADJobLegalParties.GetDesigneeListDt(prefix, ClientId, RdbBtnVal);
            List<string> getvalues = ADJobLegalParties.GetOWLookUpList1(objOwnerEditVM.dtDesigneeList);
            
            return Json(getvalues, JsonRequestBehavior.AllowGet);

        }
        //Show Designee Info
        public ActionResult LoadDesigneeInfo(string DesigneeId, string JobID)
        {
            OwnerEditVM objOwnerEditVM = new OwnerEditVM();
            objOwnerEditVM.objJobEdit = ADJob.ReadJob(Convert.ToInt32(JobID));

            objOwnerEditVM.objADJobLegalParties = ADJobLegalParties.ReadJobLegalParties(Convert.ToInt32(DesigneeId));
            objOwnerEditVM.ClientId = objOwnerEditVM.objJobEdit.ClientId.ToString();
            objOwnerEditVM.JobId = JobID;
            Session["NewDesigneeID"] = DesigneeId;
            return PartialView(GetVirtualPath("_DesigneeEdit"), objOwnerEditVM);
        }

        //New Designee Info
        public ActionResult NewDesignee(string ClientId, int DesigneeId, string RdbBtnVal)
        {
            OwnerEditVM objOwnerEditVM = new OwnerEditVM();
            string searchInitial = "";
             bool IsPrimary = false;
            objOwnerEditVM.dtDesigneeList = ADJobLegalParties.GetDesigneeListDt(searchInitial, ClientId, RdbBtnVal);
            objOwnerEditVM.DesigneeList = ADJobLegalParties.GetOWLookUpList(objOwnerEditVM.dtDesigneeList);
            objOwnerEditVM.objADJobLegalParties = ADJobLegalParties.ReadJobLegalPartiesByCondition(DesigneeId.ToString(), IsPrimary);
            Session["DesigneeID"] = DesigneeId;
            // ViewBag.CustEditFieldset = true;

            return PartialView(GetVirtualPath("_DesigneeEdit"), objOwnerEditVM);
        }

        //Save Designee Info
        public ActionResult SaveDesigneeInfo(OwnerEditVM objOwnerEditVM)
        {


            int result = 0;

            int NewDesigneeID = Convert.ToInt32(Session["NewDesigneeID"]);
            int DesigneeID = Convert.ToInt32(Session["DesigneeID"]);
            bool IsPrimary = objOwnerEditVM.IsPrimaryDesignee;
            objOwnerEditVM.IsValidStateZip = CRF.BLL.CRFView.CRFView.ValidStateZip(objOwnerEditVM.objADJobLegalParties.State, objOwnerEditVM.objADJobLegalParties.PostalCode.Substring(0, 5));
            objOwnerEditVM.ValidCityZip = CRF.BLL.CRFView.CRFView.ValidCityZip(objOwnerEditVM.objADJobLegalParties.City, objOwnerEditVM.objADJobLegalParties.PostalCode.Substring(0, 5));

            if (objOwnerEditVM.objADJobLegalParties.IsNonUSAddr == false && objOwnerEditVM.IsValidStateZip == false)
            {
                result = 0;
                return Json(result);
            }

            if (objOwnerEditVM.objADJobLegalParties.IsNonUSAddr == false && objOwnerEditVM.ValidCityZip == false)
            {
                result = -1;
                return Json(result);
            }

            result = ADJobLegalParties.SaveDesigee(objOwnerEditVM, NewDesigneeID, DesigneeID, IsPrimary);


            if (result == 0)
            {
                return PartialView(GetVirtualPath("_DesigneeEdit"), objOwnerEditVM);
            }
            return Json(result);
        }

        //Delete Designee Info
        public ActionResult DeleteDesigneeInfo(OwnerEditVM objOwnerEditVM)
        {


            int result = 0;
            int DesigneeID = Convert.ToInt32(Session["DesigneeID"]);

            //objCustomerEditVM.obj_JobEdit = ADJob.ReadJob(Convert.ToInt32(objCustomerEditVM.JobId));

            result = ADJobLegalParties.DeleteDesigee(objOwnerEditVM, DesigneeID);

            return Json(result);
        }

        public ActionResult AddNewNoteDetails(int JobId)
        {
            NoteVM objNote = new NoteVM();
            objNote.obj_noteJobId = JobId;
            int NoteId = 0;
            objNote.obj_note = ADJobHistory.ReadNote(NoteId);
            objNote.obj_note.DateCreated = DateTime.Now;
            objNote.obj_note.NoteTypeId = 1;

            if(objNote.obj_note.DateCreated < DateTime.Today)
            {
                ViewBag.btnOK = "HideOK";
                ViewBag.btnClear = "HideClear";
            }
            else
            {
                ViewBag.btnOK = "showOK";
                ViewBag.btnClear = "showClear";
            }
            
            return PartialView(GetVirtualPath("_AddNewNote"), objNote);
        }


        public ActionResult GetNoticeDetails(string JobId)
        {
            NoticeEditVM objNoticeEditVM = new NoticeEditVM();
            objNoticeEditVM.objLegalPartiesList = ADJobLegalParties.GetJobLegalPartiesList(JobId);
            //int result = ADvwJOBACTIONHISTORY.JobReviewed(JobId);
            ViewBag.checkAll = true;
            objNoticeEditVM.JobId = JobId;
            //ViewBag.SelectedAlertList = ObjJobVM.objADJobAlerts;
            return PartialView(GetVirtualPath("_NoticeEdit"), objNoticeEditVM);
        }
        public ActionResult Reviewed(int JobId)
        {
            NoteVM objNote = new NoteVM();
            objNote.obj_noteJobId = JobId;
            ADvwJOBACTIONHISTORY objADvwJOBACTIONHISTORY = new ADvwJOBACTIONHISTORY();

            int result = ADvwJOBACTIONHISTORY.JobReviewed(JobId);
            return PartialView(GetVirtualPath("_AddNewNote"), objNote);
        }

        //public ActionResult ShowNoteList(string JobId,string RadioButtonValue)
        //{
        //    JobVM ObjJobVM;
        //    ObjJobVM = JobInfo.LoadJobInfo(JobId);
        //    NoteVM obj = new NoteVM();
        //    //obj.obj_note = ADJobHistory.NoteList(ObjJobVM, RadioButtonValue);
        //   ObjJobVM.dtNotes  = ADJobHistory.NoteList(ObjJobVM, RadioButtonValue);
        //    //objNote.obj_noteJobId = JobId;
        //    ObjJobVM.ObjNotes = ADvwLienNotes.GetNotes(ObjJobVM.dtNotes);
        //    return PartialView(GetVirtualPath("_LoadJob"), ObjJobVM);
        //}



        //Save Note Details
        [HttpPost]
        public ActionResult SaveNote(NoteVM Obj_NoteVM)
        {

            int result = 0;
            try
            {
                if (Obj_NoteVM.obj_note.JobId == 0)
                {
                    Obj_NoteVM.obj_note.JobId = Obj_NoteVM.obj_noteJobId;
                }
                result = ADJobHistory.SaveADJobHistoryNote(Obj_NoteVM.obj_note);
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return Json(result);
        }

        //Get Note Details
        [HttpPost]
        public ActionResult GetNoteDetails(string Id)
        {
            NoteVM obj = new NoteVM();
            obj.obj_note = ADJobHistory.ReadNote(Convert.ToInt32(Id));
            if(obj.obj_note.DateCreated < DateTime.Today)
            {
                ViewBag.btnOK = "HideOK";
                
            }
            else
            {
                ViewBag.btnOK = "showOK";
                
            }
            return PartialView(GetVirtualPath("_EditNote"), obj);
        }

        //Delete Note Details
        [HttpPost]
        public ActionResult DeleteNote(string JobId)
        {
            bool result = ADJobHistory.DeleteJobHistoryNote(JobId);
            if (result)
            {
                return Json(result);
            }
            else
            {
                return Json(0);
            }
        }


        //Show Action Codes
        [HttpPost]
        public ActionResult ShowActionCode(string JobId, string StatusCodeId)
        {
            ActionHistoryVM objActionHistoryVM = new ActionHistoryVM();

            objActionHistoryVM.JobId = JobId;
            objActionHistoryVM.StatusCodeId = StatusCodeId;
            return PartialView(GetVirtualPath("_JobActionEdit"), objActionHistoryVM);
        }


        //Get Job Action History Codes List by Acction Type
        public ActionResult GetActionHistoryCodes(string ActionTypeValue)
        {
            List<CommonBindList> ActionHistoryCodesList = new List<CommonBindList>();
            try
            {
                ActionHistoryCodesList = CommonBindList.GetJobActionHistoryCodesByActionType(ActionTypeValue);
            }
            catch (Exception ex)
            {

            }
            return Json(ActionHistoryCodesList, JsonRequestBehavior.AllowGet);
        }


        //Detele ActionHistory Details 
        [HttpPost]
        public ActionResult DeleteActionHistoryDetails(int JobActionHistoryId)
        {

            int result = 0;
            result = ADvwJOBACTIONHISTORY.DeleteActionHistory(JobActionHistoryId);

            return Json(result);
        }

        //Add Action Code
        [HttpPost]
        public ActionResult AddActionCode(string ActionCodesId, string JobId, string StatusCodeId)
        {
            int result = -1;
            try
            {
                result = ADJob.ProcessActionCode(ActionCodesId, JobId, StatusCodeId);
            }
            catch (Exception ex)
            {
                result = -1;
            }

            return Json(result);
        }


        //Add New Document
        [HttpPost]
        public ActionResult AddDocument(string JobId)
        {
            DocumentVM objDocumentVM = new DocumentVM();
            objDocumentVM.JobId = JobId;
            return PartialView(GetVirtualPath("_AddNewDocument"), objDocumentVM);
        }

        //Save File Details
        [HttpPost]
        public ActionResult SaveDocument()
        {
            try
            {
                var file = Request.Files[0];
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/Output/UploadedFiles"), fileName);
                file.SaveAs(path);
                Session["path"] = path;
                return Json(true);
            }
            catch (Exception ex)
            {
                return Json(false);
            }
        }

        //Save Document Details
        [HttpPost]
        public ActionResult SaveDocumentInputs(DocumentVM Obj_DocumentVM)
        {
            int result = 0;
            try
            {
                string path = Convert.ToString(Session["path"]);

                if (!String.IsNullOrEmpty(path))
                {
                    byte[] bytes = System.IO.File.ReadAllBytes(path);
                    Obj_DocumentVM.ObjJobAttachments.DocumentImage = bytes;
                    Obj_DocumentVM.ObjJobAttachments.FileName = path.Substring(path.LastIndexOf("\\") + 1);
                }

                Obj_DocumentVM.ObjJobAttachments.JobId = Convert.ToInt32(Obj_DocumentVM.JobId);
                Obj_DocumentVM.ObjJobAttachments.DocumentType = Obj_DocumentVM.DocumentType;
                Obj_DocumentVM.ObjJobAttachments.BatchJobId = 0;
                Obj_DocumentVM.ObjJobAttachments.Id = Convert.ToInt32(Obj_DocumentVM.Id);

                result = ADJobAttachments.SaveJobAttachment(Obj_DocumentVM.ObjJobAttachments);
                Session["path"] = null;

                return Json(result);
            }
            catch
            {
                result = -1;
                return Json(result);
            }

        }


        //View Document
        [HttpPost]
        public string ViewDocument(string Id)
        {
            var Attachment = ADJobAttachments.ReadJobAttachment(Convert.ToInt32(Id));
            var path = Path.Combine(Server.MapPath("~/Output/DownloadableFiles"), Attachment.FileName);
            ADJobAttachments.SaveFileToBeViewed(Attachment.DocumentImage, path);
            byte[] fileBytes = Attachment.DocumentImage;
            string fileName = path.Substring(path.LastIndexOf("\\") + 1);
            //fileName = HttpUtility.UrlEncode(fileName);
            //fileName = HttpUtility.UrlPathEncode(fileName); //Remove space with % 20
            fileName = Uri.EscapeDataString(fileName); //Remove # with %23 , space with %20

            if (path != null && path != "" && path != "Error")
            {
                //string filepath = Path.Combine(Url.Content("~"), "Output/DownloadableFiles/", System.IO.Path.GetFileName(path));
                string filepath = Path.Combine(Url.Content("~"), "Output/DownloadableFiles/", fileName);

                return (filepath);
            }
            return "error";
        }

        //Get Document Details
        [HttpPost]
        public ActionResult GetDocumentDetails(string JobDocumentId, string JobId)
        {
            DocumentVM obj = new DocumentVM();
            obj.ObjJobAttachmentsEdit = ADJobAttachments.ReadJobAttachment(Convert.ToInt32(JobDocumentId));
            obj.Id = Convert.ToString(JobDocumentId);
            obj.JobId = Convert.ToString(obj.ObjJobAttachmentsEdit.JobId);
            obj.ddlDocumentType = obj.ObjJobAttachmentsEdit.DocumentType;

            return PartialView(GetVirtualPath("_EditDocument"), obj);
        }

        //Update Document Details
        [HttpPost]
        public ActionResult UpdateDocumentInputs(DocumentVM Obj_DocumentVM)
        {
            int result = 0;
            try
            {
                string path = Convert.ToString(Session["path"]);

                if (!String.IsNullOrEmpty(path))
                {
                    byte[] bytes = System.IO.File.ReadAllBytes(path);
                    Obj_DocumentVM.ObjJobAttachmentsEdit.DocumentImage = bytes;
                    Obj_DocumentVM.ObjJobAttachmentsEdit.FileName = path.Substring(path.LastIndexOf("\\") + 1);
                }

                Obj_DocumentVM.ObjJobAttachmentsEdit.JobId = Convert.ToInt32(Obj_DocumentVM.JobId);
                Obj_DocumentVM.ObjJobAttachmentsEdit.DocumentType = Obj_DocumentVM.DocumentType;
                Obj_DocumentVM.ObjJobAttachmentsEdit.BatchJobId = 0;
                Obj_DocumentVM.ObjJobAttachmentsEdit.Id = Convert.ToInt32(Obj_DocumentVM.Id);

                result = ADJobAttachments.SaveJobAttachment(Obj_DocumentVM.ObjJobAttachmentsEdit);
                Session["path"] = null;

                return Json(result);
            }
            catch
            {
                result = -1;
                return Json(result);
            }

        }



        //Delete Document
        [HttpPost]
        public ActionResult DeleteDocument(string Id)
        {
            var result = ADJobAttachments.DeleteDocument(Id);
            return Json(result);
        }




        //Read Edit More Info
        [HttpPost]
        public ActionResult RentalInfo(string JobId)
        {
            RentalInfoVM objRentalInfo = new RentalInfoVM();


            objRentalInfo.dtJobRentalList = ADJobRental.GetJobRentalDt(JobId);
            objRentalInfo.JobRentalList = ADJobRental.GetJobRentalList(objRentalInfo.dtJobRentalList);
            return PartialView(GetVirtualPath("_RentalInfo"), objRentalInfo);
        }

        //Read Edit More Info
        [HttpPost]
        public ActionResult EditMiscInfo(string JobId)
        {
            RentalInfoVM objRentalInfo = new RentalInfoVM();
            objRentalInfo.obj_JobEdit = ADJob.ReadJob(Convert.ToInt32(JobId));
            return PartialView(GetVirtualPath("_EditMiscInfo"), objRentalInfo);
        }



        [HttpPost]
        public ActionResult UpdateJobMiscInfo(RentalInfoVM objRentalInfo)
        {
            var result = ADJob.UpdateJobMiscInfo(objRentalInfo.obj_JobEdit);
            return Json(result);
        }

        //Read Edit More Info
        [HttpPost]
        public ActionResult EditOtherNotices(string JobId)
        {
            EditOtherNoticesVM objEditOtherNotices = new EditOtherNoticesVM();
            objEditOtherNotices.objJob = ADJob.ReadJob(Convert.ToInt32(JobId));

            if (objEditOtherNotices.objJob.LenderId > 0)
            {
                objEditOtherNotices.objJobLegalParties = ADJobLegalParties.ReadJobLegalParties(objEditOtherNotices.objJob.LenderId);

                if(DBNull.Value.Equals(objEditOtherNotices.objJobLegalParties.RefNum) == false)
                {
                    objEditOtherNotices.objJob.BondNum = objEditOtherNotices.objJobLegalParties.RefNum;
                }
                else
                {
                    objEditOtherNotices.objJob.BondNum = " ";
                }

            }

            

            return PartialView(GetVirtualPath("_EditOtherNotices"), objEditOtherNotices);
        }

        [HttpPost]
        public ActionResult SaveOtherNotices(EditOtherNoticesVM objEditOtherNoticesVM)
        {
           // objEditOtherNoticesVM.objJob = ADJob.ReadJob(Convert.ToInt32(objEditOtherNoticesVM.objJob.Id));
            int result = ADJob.SaveOtherNotices(objEditOtherNoticesVM);
            return Json(result);
        }
        [HttpPost]
        public ActionResult LoadEndDateOtherNotices(int Id,int JobId)
        {
            JobVM objJobVM = new JobVM();
            EndDateVM objEndDateVM = new EndDateVM();
            objEndDateVM.JobId = JobId.ToString();
            //objJobVM.ObjJobList = ADJob.ReadJob(Convert.ToInt32(objUpdateJob.obj_JobEdit.Id));
            objJobVM = JobInfo.LoadJobInfo(Convert.ToString(JobId));
            objEndDateVM.Id = Id.ToString();
            objEndDateVM.StateFormList = objJobVM.objADStateFormNotices;
            ViewBag.btnDelete = false;
            if (Id > 0)
            {
                objEndDateVM.objADJobOtherNoticeInternal = ADJobOtherNoticeInternal.ReadJobOtherNoticeInternal(Id);
                if (objEndDateVM.objADJobOtherNoticeInternal.FormId > 0)
                {
                    objEndDateVM.FormId = objEndDateVM.objADJobOtherNoticeInternal.FormId.ToString();
                    ViewBag.StateFormList = false;
                    ViewBag.RadioButton1 = true;
                }
                else
                {
                    ViewBag.StateFormList = true;
                    ViewBag.RadioButton2 = false;
                }
                if (objEndDateVM.objADJobOtherNoticeInternal.IsProcessed == false)
                {
                    ViewBag.btnDelete = true;
                }
                else
                {
                    ViewBag.disablepanel = true;
                }

                
            }
           
            //objEndDateVM.objADJobOtherNoticeInternal.TotalFees = EndDateVM.AddFees();
            return PartialView(GetVirtualPath("_EndDate"), objEndDateVM);

          
        }

        //save EndDateOtherNotices
       public ActionResult SaveEndDateInfo (EndDateVM objEndDateVM)
        {
            int result = ADJobOtherNoticeInternal.saveEndDateOtherNotices(objEndDateVM);


            return Json(result);
        }
        //Delete EndDateOtherNotices
        public ActionResult DeleteEndDateInfo(EndDateVM objEndDateVM)
        {
            int result = ADJobOtherNoticeInternal.DeleteEndDateOtherNotices(objEndDateVM);


            return Json(result);
        }

        public ActionResult GetPrelimNoticeDetails(string JobId,string JobState,string Zip)
        {

            NoticeEditVM objNoticeEditVM = new NoticeEditVM();
            bool IsBillableLegalDesc = (bool)Session["IsBillableLegalDesc"];
            
            
            objNoticeEditVM.IsValidStateZip = CRF.BLL.CRFView.CRFView.ValidStateZip(JobState, Zip);
            if(objNoticeEditVM.IsValidStateZip == false)
            {
                return Json(0);
            }
            if (IsBillableLegalDesc == true)
            {
                return Json(1);
            }
            //objNoticeEditVM.ob
            string JobType = (string)Session["JobType"];
            objNoticeEditVM = ADJobAlerts.GetAlerts(JobId, JobType);
            //objNoticeEditVM.objLegalPartiesList = ADJobLegalParties.GetJobLegalPartiesList(JobId);
            //int result = ADvwJOBACTIONHISTORY.JobReviewed(JobId);
            ViewBag.checkAll = objNoticeEditVM.CheckAll;
            ViewBag.AlertList = objNoticeEditVM.objJobAlertsList;
            objNoticeEditVM.LegalPartiesList = objNoticeEditVM.objLegalPartiesList;
            //ViewBag.SelectedAlertList = ObjJobVM.objADJobAlerts;
            objNoticeEditVM.JobId = JobId;
            if (objNoticeEditVM.dt1.Rows.Count > 0)
            {
                return PartialView(GetVirtualPath("_NoticeAlerts"), objNoticeEditVM);
            }
            else
            {

                return PartialView(GetVirtualPath("_NoticeEdit"), objNoticeEditVM);
            }
        }

        //Save PrelimNotice 
        // public ActionResult savePrelimNotice(List<ADJobLegalParties> list)
        [HttpPost]
        public ActionResult savePrelimNotice(IList<NoticeEditVM> noticeList,int JobId,bool RushBox, string RbtNoticeType,bool panelVal)
        {
            //int result = 0;
            int result = ADJobNoticePrelimRequest.savePrelimNotice(noticeList, JobId, RushBox, RbtNoticeType, panelVal);


            return Json(result);
        }
        //Delete PrelimNotice 
        [HttpPost]
        public ActionResult DeletePrelimNotices(string JobId)
        {
            bool result = ADJobNoticePrelimRequest.DeletePrelimNotice(JobId);
            if (result)
            {
                return Json(result);
            }
            else
            {
                return Json(0);
            }
        }
        //Delete Notice History
        public ActionResult FreeBillingUpdate(string JobNoticeHistoryId)
        {
            bool result = ADJobNoticeHistory.FreeBillingUpdate(JobNoticeHistoryId);
            if (result)
            {
                return Json(result);
            }
            else
            {
                return Json(0);
            }
        }

        //Get MailReturn details
        public ActionResult GetMailReturn(int JobNoticeHistoryId,string LegalPartyType, int JobId)
        {
            MailReturnVM objMailReturnVM = new MailReturnVM();
            List<CommonBindList> ActionHistoryCodesList = new List<CommonBindList>();
           objMailReturnVM.ResearchList =  CommonBindList.GetResearchType(LegalPartyType);
            objMailReturnVM.objADJobNoticeHistory = ADJobNoticeHistory.ReadJobNoticeHistory(JobNoticeHistoryId);

            objMailReturnVM.JobNoticeHistoryId = JobNoticeHistoryId;
            objMailReturnVM.JobId = JobId;
            objMailReturnVM.LegalPartyType = LegalPartyType;

            if (objMailReturnVM.objADJobNoticeHistory.MailStatus.Length > 0)
            {
                if(objMailReturnVM.objADJobNoticeHistory.MailStatus == "Returned")
                {
                    objMailReturnVM.optReturned = true;
                }
                else if(objMailReturnVM.objADJobNoticeHistory.MailStatus == "Refused")
                {
                    objMailReturnVM.optRefused = true;
                }
                else
                {
                    objMailReturnVM.optForwarded = true;
                }
                if (objMailReturnVM.optReturned == true)
                {
                    objMailReturnVM.visblBox = true;
                }
                else
                {
                    objMailReturnVM.visblBox = false;
                }

            }
            else
            {
                objMailReturnVM.optReturned = false;
                objMailReturnVM.optRefused = false;
                objMailReturnVM.optForwarded = false;
                objMailReturnVM.visblBox = false;
            }
            return PartialView(GetVirtualPath("_MailReturn"), objMailReturnVM);
        }


        //Save MailReturn details
        public ActionResult SaveMailReturnsDetails(MailReturnVM objMailReturnVM)
        {
            
            bool result = ADJobNoticeHistory.SaveMailReturns(objMailReturnVM);
            if (result)
            {
                return Json(result);
            }
            else
            {
                return Json(0);
            }
        }
        //Print Notice
        //public ActionResult GetPrintNotice(int JobNoticeHistoryId, int JobId)
        //{
        //    string result = null;
        //    try
        //    {

        //        string PdfUrl = null;

        //        PdfUrl = ADJobNoticeHistory.GetPrintNoticeCopy(JobNoticeHistoryId, JobId);
        //        if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
        //        {
        //            string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
        //            byte[] fileBytes = System.IO.File.ReadAllBytes(PdfUrl);
        //            //string path = Path.Combine(Server.MapPath("~/App_Data/DownloadableFiles"), fileName);
        //            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);

        //        }
        //        else
        //        {

        //            return Json(0);
        //        }

        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(0);
        //    }
        //}

        //Get SignitureCopy
        public ActionResult GetSignitureCopy(int JobNoticeHistoryId, string ClientCode)
        {
            try
            {

                string PdfUrl = null;

                PdfUrl = ADJobNoticeHistory.ViewSignitureCopy(JobNoticeHistoryId, ClientCode);
                if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
                {
                    string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                    byte[] fileBytes = System.IO.File.ReadAllBytes(PdfUrl);
                    //string path = Path.Combine(Server.MapPath("~/App_Data/DownloadableFiles"), fileName);
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);

                }
                else
                {

                    return Json(0);
                }
            }
            catch (Exception ex)
            {
                return Json(0);
            }
        }
        //Get POD Request
        public ActionResult GetPODRequest(int JobNoticeHistoryId, string LegalPartyType, int JobId)
        {
            string result = null;
            try
            {

                string PdfUrl = null;

                PdfUrl = ADJobNoticeHistory.GetPODCopy(JobNoticeHistoryId, LegalPartyType, JobId);
                if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
                {
                    string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                    byte[] fileBytes = System.IO.File.ReadAllBytes(PdfUrl);
                    //string path = Path.Combine(Server.MapPath("~/App_Data/DownloadableFiles"), fileName);
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);

                }
                else
                {

                    return Json(0);
                }

            }
            catch (Exception ex)
            {
                return Json(0);
            }
        }
        //Get Recension Letter
        public ActionResult GetRecensionLetter(int JobNoticeHistoryId, string LegalPartyType, int JobId)
        {
            string result = null;
            try
            {

                string PdfUrl = null;

                PdfUrl = ADJobNoticeHistory.GetRecendCopy(JobNoticeHistoryId, LegalPartyType, JobId);
                if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
                {
                    string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                    byte[] fileBytes = System.IO.File.ReadAllBytes(PdfUrl);
                    //string path = Path.Combine(Server.MapPath("~/App_Data/DownloadableFiles"), fileName);
                    return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);

                }
                else
                {

                    return Json(0);
                }

            }
            catch (Exception ex)
            {
                return Json(0);
            }
        }

        [HttpPost]
        public string SendFax(string TypeCode,int JobId)
        {
            

            string PdfUrl = ADJob.SendFax(TypeCode, JobId);
            if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
            {
                string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                return path;
            }
            return "error";
            
        }

        [HttpPost]
        public string PrintNotes(int JobId)
         {


            string PdfUrl = ADJobNoticeHistory.PrintNotes(JobId);
            if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
            {
                string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                return path;
            }
            return "error";

        }

        //Load Texas Request Details
        [HttpPost]
        public ActionResult ShowTexasRequestDetails(int Id)
        {
            try
            {
                TexasRequestVM objtexasRequestVM = new TexasRequestVM();

            objtexasRequestVM.OBJADJobTexasRequest = ADJobTexasRequest.ReadJobTexasRequest(Id);

            if (objtexasRequestVM.OBJADJobTexasRequest.IsTX60Day == true)
            {
                objtexasRequestVM.RdbBtn = "60Day";
            }
            if(objtexasRequestVM.OBJADJobTexasRequest.IsTX90Day == true)
            {
                objtexasRequestVM.RdbBtn = "90Day";
            }
                objtexasRequestVM.Id = objtexasRequestVM.OBJADJobTexasRequest.Id;
                objtexasRequestVM.JobId = objtexasRequestVM.OBJADJobTexasRequest.JobId;
                objtexasRequestVM.IsDelete = true;
                return PartialView(GetVirtualPath("_TexasReqEdit"), objtexasRequestVM);
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return Json(0);
            }
        }

        //Texas Request 
        [HttpPost]
        public ActionResult NewTexasRequest(int JobId)
        {

            TexasRequestVM objtexasRequestVM = new TexasRequestVM();
            objtexasRequestVM.JobId = JobId;
            objtexasRequestVM.IsDelete = false;
            objtexasRequestVM.Id = 0;
            return PartialView(GetVirtualPath("_TexasReqEdit"), objtexasRequestVM);
        }

        // Save Texas Request 
        public ActionResult SaveTexasRequestDetails(TexasRequestVM objtexasRequestVM)
        {
            try
            {

           
            int result;

            result = ADJobTexasRequest.SaveDetails(objtexasRequestVM);

            return Json(result);
            }
            catch(Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return Json(0);
            }
        }

        //Delete TexasRequest Details
        [HttpPost]
        public ActionResult DeleteTexasRequest(int Id,int JobId)
        {
            bool result = ADJobTexasRequest.DeleteTexasRequestDetails(Id,JobId);
            if (result)
            {
                return Json(result);
            }
            else
            {
                return Json(0);
            }
        }

        //Notice Request 
        [HttpPost]
        public ActionResult NewMonthlyRequest(int JobId)
        {

            ADJobVerifiedSentRequest objADJobVerifiedSentRequest = new ADJobVerifiedSentRequest();
            objADJobVerifiedSentRequest.JobId = JobId;
            objADJobVerifiedSentRequest.IsMnthlyReqDelete = false;
            objADJobVerifiedSentRequest.Id = 0;
            return PartialView(GetVirtualPath("_MonthlyReqEdit"), objADJobVerifiedSentRequest);
        }

        //Edit Notice Request 
        [HttpPost]
        public ActionResult EditMonthlyRequest(int MonthlyRequestId)
        {

            ADJobVerifiedSentRequest objADJobVerifiedSentRequest = new ADJobVerifiedSentRequest();

            objADJobVerifiedSentRequest = ADJobVerifiedSentRequest.ReadJobVerifiedSentRequest(MonthlyRequestId);


            objADJobVerifiedSentRequest.Id = MonthlyRequestId;
            objADJobVerifiedSentRequest.JobId = objADJobVerifiedSentRequest.JobId;
            objADJobVerifiedSentRequest.IsMnthlyReqDelete = true;
            return PartialView(GetVirtualPath("_MonthlyReqEdit"), objADJobVerifiedSentRequest);
      
    }
        // Save  Notice Request 
        public ActionResult SaveMonthlyRequestDetails(ADJobVerifiedSentRequest objADJobVerifiedSentRequest)
        {
            try
            {


                int result;

                result = ADJobVerifiedSentRequest.SaveDetails(objADJobVerifiedSentRequest);

                return Json(result);
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return Json(0);
            }
        }

        //Delete Notice Request 
        [HttpPost]
        public ActionResult DeleteMonthlyRequest(int MonthlyRequestId, int JobId)
        {
            bool result = ADJobVerifiedSentRequest.DeleteMonthlyRequestDetails(MonthlyRequestId, JobId);
            if (result)
            {
                return Json(result);
            }
            else
            {
                return Json(0);
            }
        }

        public ActionResult CopyToClipbord(string JobAdd1)
        {
            CRF.BLL.CRFView.CRFView.btnCopyToClipboard(JobAdd1);
            return Json(0);
        }
        
        [HttpPost]
        public ActionResult SendEmail(int JobId, string JobName, string ClientName, string PO, string ClientCode, int JobNo, string CustName, string CustNo, string RA, string clientid)
        {
            try
            {
                ClientVM clientVM = new ClientVM();
                string STypeCode = "CL";
                if (STypeCode == "cu")
                {
                    clientVM.Subject = "Job information request" + ClientName + " , " + JobName + ", CRF#: " + JobId + " , JOB#: " + JobNo + " ,PO#: " + PO;

                }
                else if (STypeCode == "gc")
                {
                    clientVM.Subject = "Job information request " + ClientName + " , " + JobName + " CRF#: " + JobId + " , JOB#: " + JobNo + " , SUBCONTRACTOR: " + CustName + " , PO#: " + PO;
                }
                else
                {
                    clientVM.Subject = ClientCode + " , " + JobName + " , " + " CRF#: " + JobId + " , JOB#: " + JobNo + " , CUSTNAME: " + CustName + " , CustRef#: " + CustNo + " , PO#: " + PO + " , RA#: " + RA;
                }
                List<ADJobAttachments> ListaDJobAttachments = new List<ADJobAttachments>();
                List<ReceiptientGridVM> ReceiptientGridList = new List<ReceiptientGridVM>();
                ObjJobVM = JobInfo.LoadJobInfo(JobId.ToString());

                //var ClientLienInfo = ADClientLienInfo.ReadClientLienInfoByClientId(int.Parse(clientid));
                var ClientLienInfo = ObjJobVM.obj_ClientLienInfo;
                if (ClientLienInfo.ContactEmail.Length > 0)
                {
                    ReceiptientGridList.Add(new ReceiptientGridVM { keyvalue = ClientLienInfo.ContactEmail, To = "Cl-Main", Name = ClientLienInfo.ContactName, Email = ClientLienInfo.ContactEmail });
                }

                if (ClientLienInfo.ContactEmail2.Length > 0)
                {
                    ReceiptientGridList.Add(new ReceiptientGridVM { keyvalue = ClientLienInfo.ContactEmail2, To = "Cl-Sec", Name = ClientLienInfo.ContactName2, Email = ClientLienInfo.ContactEmail2 });
                }
                clientVM.ListaDJobAttachments = ADJobAttachments.GetJobAttachmentsByJobId(JobId.ToString());
                foreach (var item in clientVM.ListaDJobAttachments)
                {
                    var path = Path.Combine(Server.MapPath("~/Output/UploadedFiles"), item.FileName);

                    item.FileName = path;
                }
                //var ClientCustomerInfo = ADClientCustomer.ReadClientCustomerByClientId(int.Parse(clientid));
                var ClientCustomerInfo = ObjJobVM.obj_ClientCustomer;
                if (ClientCustomerInfo.Email.Length > 0)
                {
                    ReceiptientGridList.Add(new ReceiptientGridVM { keyvalue = ClientCustomerInfo.Email, To = "CUST", Name = ClientCustomerInfo.ContactName, Email = ClientCustomerInfo.Email });
                }
                //var ClientGeneralContractorInfo = ADClientGeneralContractor.ReadClientGeneralContractorByClientId(int.Parse(clientid));
                var ClientGeneralContractorInfo = ObjJobVM.objADClientGeneralContractor;
                if (ClientGeneralContractorInfo.Email.Length > 0)
                {
                    ReceiptientGridList.Add(new ReceiptientGridVM { keyvalue = ClientGeneralContractorInfo.Email, To = "GC", Name = ClientGeneralContractorInfo.ContactName, Email = ClientGeneralContractorInfo.Email });
                }
                List<ClientContact> ClientContactList = ADClientContact.GetContactDetailsByClientId(Convert.ToInt32(ObjJobVM.ObjvwJobList.ClientId));
                foreach (var item in ClientContactList)
                {
                    ReceiptientGridList.Add(new ReceiptientGridVM { keyvalue = item.Email, To = "Contact", Name = item.ContactName, Email = item.Email });

                }
                clientVM.ReceiptientGridList = ReceiptientGridList;
                return PartialView(GetVirtualPath("_SendEmail"), clientVM);
            }
            catch (Exception ex)
            {
                return View("Index");
            }

        }
        [HttpPost]
        public ActionResult SendEmailInfo(string[] Email,string Subject, string Message, string[] AttachedFiles, bool isSend, int JobId)
        {
            try
            {
                string Files = AttachedFiles[0];
                string AllFilePaths = Files.Replace("\n", "").Replace("\r", "");
                AttachedFiles = AllFilePaths.Split(' ');
                CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                var myuser = ADPortalUsers.GetUserForId(CU_Obj.Id);
                //string SBODY = Message+ System.Environment.NewLine + System.Environment.NewLine + System.Environment.NewLine +
                //    myuser.SignatureLine1 + System.Environment.NewLine + myuser.SignatureLine2 + System.Environment.NewLine +
                //    myuser.SignatureLine3 + System.Environment.NewLine + myuser.SignatureLine4 +
                //    System.Environment.NewLine ;
                string Body = "";

                Body += "<p>" + Message + "</p>";
                Body += "<br />";
                Body += "<br />";
                Body += "<br />";
                Body += "<p style='margin-bottom:0px;'>" + myuser.SignatureLine1 + "</p>";
                Body += "<p style='margin-bottom:0px;'>" + myuser.SignatureLine2 + "</p>";
                Body += "<p style='margin-bottom:0px;'>" + myuser.SignatureLine3 + "</p>";
                Body += "<p style='margin-bottom:0px;'>" + myuser.SignatureLine4 + "</p>";


                bool isSent = false;

                if(Email == null)
                {
                    return Json(-1);
                }
               
                foreach (var item in Email)
                {

                    //string to = "onkar.kulkarni@rhealtech.com";
                    //string to = "testrhealsms@gmail.com";
                    
                    //string from = "testrhealsms@gmail.com";
                    string to = item;
                    string from = user.Email;


                    isSent = CRFView.Adapters.Utility.sendEmail(to, from, Subject, Body, AttachedFiles.ToList(), isSend);

                }


                ADJobHistory objUpdatedADJobHistory = new ADJobHistory();
                objUpdatedADJobHistory.JobId = JobId;
                objUpdatedADJobHistory.NoteTypeId = 1;
                var EmailSentTo = "";
                foreach (var email in Email)
                {
                    EmailSentTo += email + ";";
                }

                objUpdatedADJobHistory.Note = "EMAIL SENT TO: " + EmailSentTo;

                objUpdatedADJobHistory.Note += System.Environment.NewLine;

                objUpdatedADJobHistory.Note += "SUBJECT: " + Subject;


                objUpdatedADJobHistory.Note += System.Environment.NewLine;


                objUpdatedADJobHistory.Note += "MESSAGE: " + Message;

                objUpdatedADJobHistory.Note += System.Environment.NewLine;

                Body = " ";
                Body += " ";
                Body += System.Environment.NewLine;
                Body += System.Environment.NewLine;
                Body += System.Environment.NewLine;
                Body += " " + myuser.SignatureLine1 + " ";
                Body += " " + myuser.SignatureLine2 + " ";
                Body += " " + myuser.SignatureLine3 + " ";
                Body += " " + myuser.SignatureLine4 + " ";

                objUpdatedADJobHistory.Note += Body;

                int JobHistorySaved = ADJobHistory.SaveADJobHistoryNoteForSentEmail(objUpdatedADJobHistory);

                return Json(isSend);
            }
            catch(Exception ex)
            {
                return Json(-2);
            }
         
        }
        [HttpPost]
 public ActionResult AddAttachDoc(int Id)
        {
            try
            {
                var attachment = ADJobAttachments.ReadJobAttachment(Id);
                var path = Path.Combine(Server.MapPath("~/Output/DownloadableFiles"), attachment.FileName.Replace(" ", ""));
                ADDebtAccountAttachment.SaveFileToBeViewed(attachment.DocumentImage, path);
                return Json(path);
            }
            catch(Exception ex)
            {
                return Json("");
            }
          
        }
        [HttpPost]
      
        public ActionResult SaveMISCDocument()
        {
            try
            {
                var file = Request.Files[0];
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/Output/UploadedFiles"), fileName);
                file.SaveAs(path);
                Session["path"] = path;            
                return Json(path);
            }
            catch (Exception ex)
            {
                return Json(false);
            }
        }

        public ActionResult SetEmailPage(string TypeCode, int JobId)
        {
            ClientVM OBJClientVM = new ClientVM();
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            //ObjJobVM = JobInfo.LoadJobInfo(JobId.ToString());
            OBJClientVM = ADClientCustomer.EmailPageSetup(TypeCode, JobId, OBJClientVM);
           
            foreach (var item in OBJClientVM.ListaDJobAttachments)
            {
                var path = Path.Combine(Server.MapPath("~/Output/UploadedFiles"), item.FileName);

                item.FileName = path;
            }

            string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(OBJClientVM.path));
            if (fileName == "" || fileName == null)
            {

            }
            else
            {
                 OBJClientVM.path = Path.Combine(Server.MapPath("~/Output/Reports/"), fileName);
            }
           
            //OBJClientVM.path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
            OBJClientVM.JobId = JobId;

            //return Json(path);
            return PartialView(GetVirtualPath("_Email"), OBJClientVM);
        }

        [HttpPost]
        public ActionResult SendEmailT(string[] Email, string Subject, string Message, string[] AttachedFiles, bool isSend, int JobId)
        {
            try
            {
                string Files = AttachedFiles[0];
                string AllFilePaths = Files.Replace("\n", "").Replace("\r", "");
                AttachedFiles = AllFilePaths.Split(' ');
                CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                var myuser = ADPortalUsers.GetUserForId(CU_Obj.Id);
                
                string SBody = Message.Replace("\n", "<br/>");

                string Body = "";
                Body += "<p>" + SBody + "</p>";
                Body += "<br />";
                Body += "<br />";
                Body += "<br />";
                Body += "<p style='margin-bottom:0px;'>" + myuser.SignatureLine1 + "</p>";
                Body += "<p style='margin-bottom:0px;'>" + myuser.SignatureLine2 + "</p>";
                Body += "<p style='margin-bottom:0px;'>" + myuser.SignatureLine3 + "</p>";
                Body += "<p style='margin-bottom:0px;'>" + myuser.SignatureLine4 + "</p>";


                bool isSent = false;

                if (Email == null || Email.Length == 0)
                {
                    return Json(-1);
                }

                foreach (var item in Email)
                {

                    //string to = "onkar.kulkarni@rhealtech.com";
                    //string to = "testrhealhealsms@gmail.com";

                    //string from = "testrhealhealsms@gmail.com";
                    string to = item;
                   string from = user.Email;


                    isSent = CRFView.Adapters.Utility.sendEmail(to, from, Subject, Body, AttachedFiles.ToList(), isSend);

                }


                ADJobHistory objUpdatedADJobHistory = new ADJobHistory();
                objUpdatedADJobHistory.JobId = JobId;
                objUpdatedADJobHistory.NoteTypeId = 1;
                var EmailSentTo = "";
                foreach (var email in Email)
                {
                    EmailSentTo += email + ";";
                }

                objUpdatedADJobHistory.Note = "EMAIL SENT TO: " + EmailSentTo;

                objUpdatedADJobHistory.Note += System.Environment.NewLine;

                objUpdatedADJobHistory.Note += "SUBJECT: " + Subject;


                objUpdatedADJobHistory.Note += System.Environment.NewLine;


                objUpdatedADJobHistory.Note += "MESSAGE: " + Message;

                objUpdatedADJobHistory.Note += System.Environment.NewLine;

               

                int JobHistorySaved = ADJobHistory.SaveADJobHistoryNoteForSentEmail(objUpdatedADJobHistory);

                return Json(isSend);
            }
            catch (Exception ex)
            {
                return Json(-2);
            }

        }

        public ActionResult SendBranchEmail(int JobId)
        {
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            DataTable LienReportsInfo = ADVWLienReportsInfo.GetLienReportsInfoByJobId(JobId);
            ObjJobVM = JobInfo.LoadJobInfo(JobId.ToString());
            //var BranchInfo = JobInfo.GetBranchInfo(JobId.ToString());

            ClientVM OBJClientVM = new ClientVM();
            string STypeCode = "";
            string sBody = "";
            string s = "";
            //JobVM ObjJobVM;
            //ObjJobVM = JobInfo.LoadJobInfo(JobId.ToString());
            var Job = ObjJobVM.jobdt;
            var ClientLienInfo = ObjJobVM.dtClientInfo;
            if (ClientLienInfo.Rows.Count > 0)
            {
                foreach (DataRow dr in ClientLienInfo.Rows)
                {
                    OBJClientVM.To = dr["Email"].ToString();
                }
            }

            //clientVM.Subject = s;
            //clientVM.Message = sBody;
            //clientVM.ListaDJobAttachments = ADJobAttachments.GetJobAttachmentsByJobId(JobId.ToString());

            //clientVM.JobId = JobId;
            //clientVM = ADClientCustomer.EmailPageSetup(user, ObjJobVM.dtClientInfo, JobId);
            string TypeCode = "CL";
             OBJClientVM = ADClientCustomer.EmailPageSetup(TypeCode, JobId, OBJClientVM);

            //clientVM = PageSetup(JobId);

            return PartialView(GetVirtualPath("_BranchEmail"), OBJClientVM);
        }
        //[HttpPost]
        //public ActionResult SendBranchEmailInfo(string[] Email, string Subject, string Message, string[] AttachedFiles, bool isSend, int JobId)
        //{
        //    isSend = true;
        //    try
        //    {
        //        string Files = AttachedFiles[0];
        //        string AllFilePaths = Files.Replace("\n", "").Replace("\r", "");
        //        AttachedFiles = AllFilePaths.Split(' ');
        //        CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
        //        CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
        //        var myuser = ADPortalUsers.GetUserForId(CU_Obj.Id);
        //        //string SBODY = Message+ System.Environment.NewLine + System.Environment.NewLine + System.Environment.NewLine +
        //        //    myuser.SignatureLine1 + System.Environment.NewLine + myuser.SignatureLine2 + System.Environment.NewLine +
        //        //    myuser.SignatureLine3 + System.Environment.NewLine + myuser.SignatureLine4 +
        //        //    System.Environment.NewLine ;
        //        string Body = Message;




        //        bool isSent = false;

        //        if (Email == null)
        //        {
        //            return Json(-1);
        //        }

        //        foreach (var item in Email)
        //        {

        //            //string to = "onkar.kulkarni@rhealtech.com";
        //            string to = "testrhealhealsms@gmail.com";

        //            string from = "testrhealhealsms@gmail.com";
        //            //string to = item.ToString();
        //            //string from = user.Email;


        //            isSent = CRFView.Adapters.Utility.sendEmail(to, from, Subject, Body, AttachedFiles.ToList(), isSend);

        //        }


        //        ADJobHistory objUpdatedADJobHistory = new ADJobHistory();
        //        objUpdatedADJobHistory.JobId = (int)JobId;
        //        objUpdatedADJobHistory.NoteTypeId = 1;
        //        var EmailSentTo = "";
        //        foreach (var email in Email)
        //        {
        //            EmailSentTo += email + ";";
        //        }

        //        objUpdatedADJobHistory.Note = "EMAIL SENT TO" + EmailSentTo;

        //        objUpdatedADJobHistory.Note += System.Environment.NewLine;

        //        objUpdatedADJobHistory.Note += "SUBJECT " + Subject;


        //        objUpdatedADJobHistory.Note += System.Environment.NewLine;


        //        objUpdatedADJobHistory.Note += "MESSAGE:";

        //        objUpdatedADJobHistory.Note += System.Environment.NewLine;

        //        objUpdatedADJobHistory.Note += Body;

        //        int JobHistorySaved = ADJobHistory.SaveADJobHistoryNoteForSentEmail(objUpdatedADJobHistory);

        //        return Json(isSend);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(-2);
        //    }
        //    return Json(1);

        //}

        //[HttpPost]
        //public ActionResult SendEmailT(string[] Email, string Subject, string Message, string[] AttachedFiles, bool isSend, int JobId)

        //{

        //    return Json(true);
        //}

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/Verifiers/WorkCard/" + ViewName + ".cshtml";
            return path;

        }
    }
}
