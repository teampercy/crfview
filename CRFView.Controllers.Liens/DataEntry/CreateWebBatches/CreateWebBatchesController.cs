﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CRFView.Adapters;
using CRFView.Adapters.Liens.DataEntry.CreateWebBatches.ViewModels;
using System.Data;

namespace CRFView.Controllers.Liens.DataEntry.CreateWebBatches
{
    public class CreateWebBatchesController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            LienBatchWebPlacementsVM objLienBatchWebPlacementsVM = new LienBatchWebPlacementsVM();

             DataTable dtLienBatchJobForWebPlacements = ADBatchJob.GetLienBatchJobForWebPlacements();

            if (dtLienBatchJobForWebPlacements.Rows.Count < 1)
            {
                ViewBag.lblMessage = "No Web Placements to Be Batched";
                ViewBag.btnOK = "Hide OK";
            }
            else
            {
                ViewBag.lblMessage = "(" + dtLienBatchJobForWebPlacements.Rows.Count + ") Placements to Be Posted";
                ViewBag.lblMessage += Environment.NewLine;
                ViewBag.lblMessage += "      Press Ok to Post";
                ViewBag.btnOK = "Show OK";
            }

            return PartialView(GetVirtualPath("_LienBatchWebPlacements"), objLienBatchWebPlacementsVM);
        }
 
        //Print LienBatch WebPlacements
        [HttpPost]
        public string LienBatchWebPlacements(LienBatchWebPlacementsVM objLienBatchWebPlacementsVM)
        {
            ADBatchJob obj = new ADBatchJob();

            string PdfUrl = obj.PrintLienBatches(objLienBatchWebPlacementsVM);

            if (PdfUrl == "No WebPlacementError")
            {
                return "1";
            }
            else if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
            {
                string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                string path = System.IO.Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                return (path);
            }
            else
            {
                return "error";
            }
        }


        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/DataEntry/CreateWebBatches/" + ViewName + ".cshtml";
            return path;
        }
    }
}
