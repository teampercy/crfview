﻿using CRFView.Adapters;
using CRFView.Adapters.Liens.DataEntry.ImportJobView.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.DataEntry.ImportJobView
{
    public class ImportJobViewController : Controller
    {
        ImportJobViewVM objImportJobViewVM = new ImportJobViewVM();

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {

            return PartialView(GetVirtualPath("_ImportJobView"), objImportJobViewVM);
        }

        //Get ClientInterface ddl Details 
        //[HttpPost]
        public ActionResult GetClientInterfaceddlDetails(string FileName, string ClientInterfaceId)
        {
            objImportJobViewVM.objClientInterface = ADClientInterface.ReadClientInterface(Convert.ToInt32(ClientInterfaceId));
            objImportJobViewVM.FileName = FileName;
            objImportJobViewVM.ddlClientInterface = ClientInterfaceId;
            objImportJobViewVM.EDIInstruction = objImportJobViewVM.objClientInterface.EDIInfo;

            return PartialView(GetVirtualPath("_ImportJobView"), objImportJobViewVM);
        }

        //Save ImportJobView Document
        [HttpPost]
        public ActionResult SaveImportJobViewDocument()
        {
            try
            {
                var currDate = DateTime.Now.ToString("yyyyMMddHHmmss");
                var file = Request.Files[0];
                //var fileName = Path.GetFileName(file.FileName);
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var fileExt = Path.GetExtension(file.FileName);
                fileName = fileName + "_" + currDate + fileExt;
                var path = Path.Combine(Server.MapPath("~/Output/UploadedFiles"), fileName);
                var ReportPath = Path.Combine(Server.MapPath("~/Output/Reports"), fileName);
                file.SaveAs(path);
                file.SaveAs(ReportPath);
                Session["LienImportJobViewDocumentPath"] = ReportPath;
                return Json(true);
            }
            catch (Exception ex)
            {
                return Json(false);
            }
        }

        //Import JobView
        [HttpPost]
        public string ImportJobView(ImportJobViewVM objImportJobViewVM)
        {
            string result = null;
            try
            {
                objImportJobViewVM.FileName = Convert.ToString(Session["LienImportJobViewDocumentPath"]);
                if (!String.IsNullOrEmpty(objImportJobViewVM.FileName))
                {
                    result = ADClientInterface.LienImportJobView(objImportJobViewVM);

                    var path = Path.Combine(Server.MapPath("~/Output/Reports"), result);
                    string fileName = path.Substring(path.LastIndexOf("\\") + 1);
                    fileName = Uri.EscapeDataString(fileName); //Remove # with %23 , space with %20

                    if (result != null && result != "" && result != "error")
                    {
                        string filepath = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);

                        return (filepath);
                    }
                    else
                    {
                        return "error";
                    }
                }
                else
                {
                    return "error";
                }
            }
            catch (Exception ex)
            {
                return "error";
            }

        }


        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/DataEntry/ImportJobView/" + ViewName + ".cshtml";
            return path;
        }

    }
}
