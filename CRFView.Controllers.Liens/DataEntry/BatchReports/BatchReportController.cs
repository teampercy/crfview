﻿using CRFView.Adapters;
using CRFView.Adapters.Liens.DataEntry.BatchReport.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
 

namespace CRFView.Controllers.Configuration.Liens.DataEntry.BatchReports
{
    public class BatchReportController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            BatchReportOptionsVM objBatchReportOptionsVM = new BatchReportOptionsVM();

            return PartialView(GetVirtualPath("_BatchReportOptions"), objBatchReportOptionsVM);
        }

        //Print BatchReports
        [HttpPost]
        public string PrintBatchReportsOptions(BatchReportOptionsVM objBatchReportOptionsVM)
        {
            ADvwBatchDebtAccount obj = new ADvwBatchDebtAccount();

            string PdfUrl = obj.PrintLienBatchReport(objBatchReportOptionsVM);
            if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
            {
                string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                return (path);
            }

            return "error";
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/DataEntry/BatchReport/" + ViewName + ".cshtml";
            return path;
        }
    }
}
