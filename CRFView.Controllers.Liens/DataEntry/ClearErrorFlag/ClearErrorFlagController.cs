﻿using CRFView.Adapters;
using CRFView.Adapters.Liens.DataEntry.ClearErrorFlag.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.DataEntry.ClearErrorFlag
{
    public class ClearErrorFlagController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            ClearErrorFlagVM objClearErrorFlagVM = new ClearErrorFlagVM(); 
            
            return PartialView(GetVirtualPath("_ClearErrorFlag"), objClearErrorFlagVM);
        }

        //Get Address details for Individual Credit Report
        [HttpPost]
        public ActionResult GetBatchJobDetailsByBatchJobId(string BatchJobId)
        {
            ClearErrorFlagVM objClearErrorFlagVM = new ClearErrorFlagVM();

            objClearErrorFlagVM.ObjADvwBatchJobExport = ADvwBatchJobExport.ReadBatchJobByBatchJobId(BatchJobId);
            
            if(objClearErrorFlagVM.ObjADvwBatchJobExport.BatchId > 0)
            {
                objClearErrorFlagVM.ByBatchJobId = BatchJobId;
            }
            else
            {
                objClearErrorFlagVM.ByBatchJobId = "0";
            }

            return PartialView(GetVirtualPath("_ClearErrorFlag"), objClearErrorFlagVM);
        }


        //Update ClearErrorFlag Details
        [HttpPost]
        public ActionResult UpdateClearErrorFlag(ClearErrorFlagVM objClearErrorFlagVM)
        {
            int result = 0;
            try
            {
                objClearErrorFlagVM.ObjADvwBatchJobExport.Id =Convert.ToInt32(objClearErrorFlagVM.ByBatchJobId);
                objClearErrorFlagVM.ObjADvwBatchJobExport.ErrorDescription = "1";
                result = ADBatchJob.SaveBatchJob(objClearErrorFlagVM.ObjADvwBatchJobExport);
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return Json(result);
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/DataEntry/ClearErrorFlag/" + ViewName + ".cshtml";
            return path;
        }
    }
}
