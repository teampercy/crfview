﻿using CRFView.Adapters;
using CRFView.Adapters.Liens.DataEntry.DataEntry;
using CRFView.Adapters.Liens.DataEntry.DataEntry.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.DataEntry.DataEntry
{
    public class DataEntriesController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            FilterVM ObjFilterVM = new FilterVM();
            //ObjFilterVM.BatchIdList = CommonBindList.GetOpenLienBatchIdList(); // Load  BatchIdList ddl 
            
            Session["FiltersObj"] = ObjFilterVM;
            string sortBy = "BatchJobId DESC";
            List<ADvwBatchJobs> lst = ADvwBatchJobs.GetInitialList(ObjFilterVM, sortBy);
            BatchJobFilterListVM ObjVm = new BatchJobFilterListVM();
            ObjVm.ObjBatchJobs = lst;
            return PartialView(GetVirtualPath("Index"), ObjVm);
        }

        //Show filter
        [HttpGet]
        public ActionResult ShowFilterModal()
        {
            FilterVM ObjVm = new FilterVM();
            return PartialView(GetVirtualPath("_Filter"), ObjVm);
        }

        [HttpPost]
        public ActionResult GetFilterResult(FilterVM objFilter)
        {
            Session["FiltersObj"] = objFilter;
           
            string SortBy = "BatchJobId DESC";
            var s = objFilter.FilterObj.ddlBatchId;
            List<ADvwBatchJobs> lst = ADvwBatchJobs.GetFilteredList(objFilter, SortBy);
            BatchJobFilterListVM ObjVm = new BatchJobFilterListVM();
            ObjVm.ObjBatchJobs = lst;
            return PartialView(GetVirtualPath("_BatchJobsList"), ObjVm);
        }

        //Get Sorted Result
        [HttpPost]
        public ActionResult GetSortedResult(string SortByValue, string SortTypeValue)
        {

            string SortType = "ASC";
            string SortBy = "BatchJobId";
            if (SortTypeValue == "-1")
            {
                SortType = "ASC";
            }
            else if (SortTypeValue == "1")
            {
                SortType = "DESC";
            }

            if (SortByValue == "-1" || string.IsNullOrEmpty(SortByValue))
            {
                SortBy = "CustFax " + SortType;
            }
            else if (SortByValue == "1")
            {
                SortBy = "CustEmail " + SortType;
            }
            else if (SortByValue == "2")
            {
                SortBy = "GCFax " + SortType;
            }
            else if (SortByValue == "3")
            {
                SortBy = "GCEmail " + SortType;
            }
           
            

            FilterVM objFilter = (FilterVM)Session["FiltersObj"];

            List<ADvwBatchJobs> lst = ADvwBatchJobs.GetFilteredList(objFilter,SortBy);
           
            BatchJobFilterListVM ObjVm = new BatchJobFilterListVM();
            ObjVm.ObjBatchJobs = lst;

            Session["FilterList"] = ObjVm;
            return PartialView(GetVirtualPath("_BatchJobsList"), ObjVm);
        }

        //Load Batch job details
        public void LoadBatchJobDetails(string Id)
        {
            BatchJobVM obj;
            obj = BatchJobInfo.LoadInfo(Id);

            obj.Id = Id;
            EnforceRules(Id, obj);
        }

        //EnforceRules
        public void EnforceRules(string Id, BatchJobVM obj)
        {
            obj.objClientLienInfo = ADClientLienInfo.ReadClientLienInfoByClientId(obj.objBatchJob.ClientId);
            obj.objStateInfo = ADStateInfo.ReadStateInfoByJobState(obj.objBatchJob.JobState);
            obj.objCompanyInfo = ADCompanyInfo.ReadCompanyInfoById(1);

            obj.objClient = ADClient.ReadClientByClientId(obj.objBatchJob.ClientId);

            obj.objBatchJob.JobState = obj.objStateInfo.StateInitials;

            if (obj.objClientLienInfo.BranchBox == false)
            {
                ViewBag.BranchNum = "hide"; //BranchNum textbox is disabled 
            }
            else
            {
                ViewBag.BranchNum = "show"; //BranchNum textbox is enabled 
            }

            if (obj.objStateInfo.NOCBox == true)
            {
                ViewBag.FolioNum = "show"; //FolioNum textbox is enabled 
            }
            else
            {
                ViewBag.FolioNum = "hide"; //FolioNum textbox is disabled 
            }
             
            if (obj.objClient.IsRentalCo == true)
            {
                ViewBag.EquipRate = "show";  //EquipRate textbox is enabled 
                ViewBag.EquipRental = "show";  //EquipRental textbox is enabled 
                ViewBag.RANum = "show";  //RANum textbox is enabled 
            }
            else
            {
                ViewBag.EquipRate = "hide";  //EquipRate textbox is disabled 
                ViewBag.EquipRental = "hide";  //EquipRental textbox is disabled 
                ViewBag.RANum = "hide";  //RANum textbox is disabled 
            }

            if(obj.objBatchJob.PublicJob == false && obj.objBatchJob.ResidentialBox == false && obj.objBatchJob.FederalJob == false)
            {
                ViewBag.PrivateJob = "checked"; //PrivateJob radiobutton is checked 
            }
            else
            {
                ViewBag.PrivateJob = "unchecked"; //PrivateJob radiobutton is unchecked 
            }
            
            if(obj.objBatchJob.JobActionId == 0)
            {
                obj.objBatchJob.JobActionId = obj.objCompanyInfo.NBPrelimActionId;
            }

            if (obj.objStateInfo.MLAgent == true)
            {
                ViewBag.MLAgent = "show"; //MLAgent checkbox is enabled 
            }
            else
            {
                ViewBag.MLAgent = "hide"; //MLAgent checkbox is disabled   
            }

            if(obj.objClientLienInfo.IsNOCApproved == true && obj.objStateInfo.NoticeOfCompBox == true)
            {
                ViewBag.NOCBox = "show"; //NOCBox checkbox is enabled 
            }
            else
            {
                ViewBag.NOCBox = "hide"; //NOCBox checkbox is disabled   
            }

            if (obj.objBatchJob.JobActionId == obj.objCompanyInfo.NBNOCActionId)
            {
                ViewBag.NOCBox = "checked"; //NOCBox checkbox is checked 
            }
            else
            {
                ViewBag.NOCBox = "unchecked"; //NOCBox checkbox is unchecked 
            }

            if (obj.objBatchJob.JobActionId == obj.objCompanyInfo.NBPrelimActionId)
            {
                ViewBag.RushOrder = "show"; //RushOrder checkbox is enabled 
                ViewBag.RushOrderML = "hide"; //RushOrderML checkbox is disabled    
                ViewBag.RushOrderVerify = "hide"; //RushOrderVerify checkbox is disabled    
            }
            else if(obj.objBatchJob.JobActionId == obj.objCompanyInfo.NBVerifyOWOnlyActionId)
            {
                ViewBag.RushOrder = "hide"; //RushOrder checkbox is  disabled
                ViewBag.RushOrderML = "hide"; //RushOrderML checkbox is disabled    
                ViewBag.RushOrderVerify = "show"; //RushOrderVerify checkbox is enabled    
            }
            else if (obj.objBatchJob.JobActionId == obj.objCompanyInfo.NBVerifyOWOnlySendActionId)
            {
                ViewBag.RushOrder = "show"; //RushOrder checkbox is enabled 
                ViewBag.RushOrderML = "hide"; //RushOrderML checkbox is disabled    
                ViewBag.RushOrderVerify = "hide"; //RushOrderVerify checkbox is disabled    
            }
            else if(obj.objBatchJob.JobActionId == obj.objCompanyInfo.NBVerifyOWOnlyTXActionId)
            {
                ViewBag.RushOrder = "hide"; //RushOrder checkbox is  disabled
                ViewBag.RushOrderML = "hide"; //RushOrderML checkbox is disabled    
                ViewBag.RushOrderVerify = "show"; //RushOrderVerify checkbox is enabled    
            }
            else if(obj.objBatchJob.JobActionId == obj.objCompanyInfo.NBMechLienActionId)
            {
                ViewBag.RushOrder = "show"; //RushOrder checkbox is enabled 
                ViewBag.RushOrderML = "hide"; //RushOrderML checkbox is disabled    
                ViewBag.RushOrderVerify = "hide"; //RushOrderVerify checkbox is disabled    
            }
            else if(obj.objBatchJob.JobActionId == obj.objCompanyInfo.NBVerifyOnlyActionId)
            {
                ViewBag.RushOrder = "show"; //RushOrder checkbox is enabled 
                ViewBag.RushOrderML = "hide"; //RushOrderML checkbox is disabled    
                ViewBag.RushOrderVerify = "hide"; //RushOrderVerify checkbox is disabled    
            }
            else if(obj.objBatchJob.JobActionId == obj.objCompanyInfo.NBVerifyOnlyTXActionId)
            {
                ViewBag.RushOrder = "hide"; //RushOrder checkbox is  disabled
                ViewBag.RushOrderML = "hide"; //RushOrderML checkbox is disabled    
                ViewBag.RushOrderVerify = "show"; //RushOrderVerify checkbox is enabled    
            }
         else
            {
                ViewBag.RushOrder = "hide"; //RushOrder checkbox is  disabled
                ViewBag.RushOrderML = "hide"; //RushOrderML checkbox is disabled    
                ViewBag.RushOrderVerify = "hide"; //RushOrderVerify checkbox is disabled   
            }

            if(obj.objClientLienInfo.DataEntryFee == true)
            {
                ViewBag.DataEntryFee = "show"; //DataEntryFee checkbox is enabled    
                ViewBag.DataEntryFee = "checked"; //DataEntryFee checkbox is checked 
            }
            else
            {
                ViewBag.DataEntryFee = "hide"; //DataEntryFee checkbox is  disabled
                ViewBag.DataEntryFee = "unchecked"; //DataEntryFee checkbox is unchecked 
            }

            if(obj.objBatchJob.GreenCard == true)
            {
                if(obj.objStateInfo.ReturnReceiptPrivate == true && (obj.objBatchJob.PrivateJob == true || obj.objBatchJob.ResidentialBox == true))
                {
                    ViewBag.GreenCard = "checked"; //GreenCard checkbox is checked 
                }
                else if(obj.objStateInfo.ReturnReceiptPublic == true && (obj.objBatchJob.PublicJob == true))
                {
                    ViewBag.GreenCard = "checked"; //GreenCard checkbox is checked 
                }
            }
        }

        //Load Batch job details on view
        [HttpPost]
        public ActionResult LoadBatchJob(string Id)
        {
            string BatchId = "0";
            int result = 0;
            BatchJobVM obj = new BatchJobVM();

            if(Id == "0")
            {
                FilterVM ObjFilterVM = new FilterVM();
                var data = ObjFilterVM.BatchIdList.FirstOrDefault();
                if(data != null)
                {
                    BatchId = data.Value.ToString();
                }
                result = ADBatchJob.SaveNewLienBatchJobInfo(BatchId);
                if(result > 0)
                {
                    Id = Convert.ToString(result);
                }
            }

            obj = BatchJobInfo.LoadInfo(Id);
            obj.Id = Id;
           
            EnforceRules(Id, obj);

            return PartialView(GetVirtualPath("_LoadBatchJob"), obj);
        }

        //Create New Batch
        [HttpGet]
        public ActionResult CreateNewBatch()
        {
            int result = 0;
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            FilterVM ObjVm = new FilterVM();
            try
            {
                ObjVm.ObjADBatch.BatchType = "LIEN";
                ObjVm.ObjADBatch.BatchStatus = "OPEN";
                ObjVm.ObjADBatch.BatchDate = DateTime.Now;
                ObjVm.ObjADBatch.FileDate = DateTime.Now;
                ObjVm.ObjADBatch.SubmittedById = user.Id;
                ObjVm.ObjADBatch.SubmittedBy = user.UserName;

                result = ADBatch.SaveBatch(ObjVm.ObjADBatch);

                ObjVm.BatchIdList = CommonBindList.GetOpenLienBatchIdList(); // Load  BatchIdList ddl for updated record
            }
            catch (Exception ex)
            {
                result = 0;
            }

            return PartialView(GetVirtualPath("_Filter"), ObjVm);
        }

        //Release Batch
        [HttpGet]
        public ActionResult ReleaseBatch(string BatchId)
        {
            int result = 0;

            FilterVM ObjVm = new FilterVM();
            try
            {
                result = ADBatch.ReleaseLienBatch(BatchId);

                ObjVm.BatchIdList = CommonBindList.GetOpenLienBatchIdList(); // Load  BatchIdList ddl for updated record
            }
            catch (Exception ex)
            {
                result = 0;
            }

            return PartialView(GetVirtualPath("_Filter"), ObjVm);
        }

        //Delete Batch
        [HttpGet]
        public ActionResult DeleteBatch(string BatchId)
        {
            int result = 0;

            FilterVM ObjVm = new FilterVM();
            try
            {
                result = ADBatch.DeleteBatch(BatchId);

                ObjVm.BatchIdList = CommonBindList.GetOpenLienBatchIdList(); // Load  BatchIdList ddl for updated record
            }
            catch (Exception ex)
            {
                result = 0;
            }

            return PartialView(GetVirtualPath("_Filter"), ObjVm);
        }

        //Edit BatchJob
        [HttpPost]
        public ActionResult EditBatchJob(string Id)
        {
            LoadBatchJobDetails(Id);

            EditBatchJobVM objEditBatchJobVM = new EditBatchJobVM();
            objEditBatchJobVM.objBatchJobEdit = ADBatchJob.ReadBatchJob(Convert.ToInt32(Id));
            objEditBatchJobVM.ddlClientId = Convert.ToString(objEditBatchJobVM.objBatchJobEdit.ClientId);
            objEditBatchJobVM.ddlJobStateId = Convert.ToString(objEditBatchJobVM.objBatchJobEdit.JobState);
            objEditBatchJobVM.Id = Id;

            return PartialView(GetVirtualPath("_EditBatchJob"), objEditBatchJobVM);
        }

        //Save Batch Job Details
        [HttpPost]
        public ActionResult SaveBatchJobDetails(EditBatchJobVM objEditBatchJobVM)
        {
            int result = 0;
            try
            {
                objEditBatchJobVM.objBatchJobEdit.Id = Convert.ToInt32(objEditBatchJobVM.Id);
                objEditBatchJobVM.objBatchJobEdit.ClientId = Convert.ToInt32(objEditBatchJobVM.ddlClientId);
                objEditBatchJobVM.objBatchJobEdit.JobState = objEditBatchJobVM.ddlJobStateId;

                result = ADBatchJob.SaveLienBatchJob(objEditBatchJobVM.objBatchJobEdit);
            }
            catch (Exception ex)
            {
                result = 0;
            }

            return Json(result);
        }

        //Edit Customer Info
        [HttpPost]
        public ActionResult EditCustomerInfo(string Id)
        {
            LoadBatchJobDetails(Id);

            EditCustomerInfoVM objEditCustomerInfoVM = new EditCustomerInfoVM();
            objEditCustomerInfoVM.objBatchJobEdit = ADBatchJob.ReadBatchJob(Convert.ToInt32(Id));
            objEditCustomerInfoVM.Id = Id;

            return PartialView(GetVirtualPath("_EditCustomerInfo"), objEditCustomerInfoVM);
        }

        //Save  CustomerInfo Details
        [HttpPost]
        public ActionResult SaveCustomerInfo(EditCustomerInfoVM objEditCustomerInfoVM)
        {
            int result = 0;
            try
            {
                objEditCustomerInfoVM.objBatchJobEdit.Id = Convert.ToInt32(objEditCustomerInfoVM.Id);
                result = ADBatchJob.SaveLienCustomerInfo(objEditCustomerInfoVM.objBatchJobEdit);
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return Json(result);
        }

        //Edit Job Info
        [HttpPost]
        public ActionResult EditJobInfo(string Id)
        {
            LoadBatchJobDetails(Id);

            EditJobInfoVM objEditJobInfoVM = new EditJobInfoVM();
            objEditJobInfoVM.objBatchJobEdit = ADBatchJob.ReadBatchJob(Convert.ToInt32(Id));
            objEditJobInfoVM.Id = Id;
            objEditJobInfoVM.ddlJobActionId = Convert.ToString(objEditJobInfoVM.objBatchJobEdit.JobActionId);

            return PartialView(GetVirtualPath("_EditJobInfo"), objEditJobInfoVM);
        }

        //Save  JobInfo Details
        [HttpPost]
        public ActionResult SaveJobInfo(EditJobInfoVM objEditJobInfoVM)
        {
            int result = 0;
            try
            {
                objEditJobInfoVM.objBatchJobEdit.Id = Convert.ToInt32(objEditJobInfoVM.Id);
                objEditJobInfoVM.objBatchJobEdit.JobActionId = Convert.ToInt32(objEditJobInfoVM.ddlJobActionId);

                if (objEditJobInfoVM.RbtJobType == "Private")
                {
                    objEditJobInfoVM.objBatchJobEdit.PrivateJob = true;
                }
                else
                {
                    objEditJobInfoVM.objBatchJobEdit.PrivateJob = false;
                }

                if (objEditJobInfoVM.RbtJobType == "Public")
                {
                    objEditJobInfoVM.objBatchJobEdit.PublicJob = true;
                }
                else
                {
                    objEditJobInfoVM.objBatchJobEdit.PublicJob = false;
                }

                if (objEditJobInfoVM.RbtJobType == "Federal")
                {
                    objEditJobInfoVM.objBatchJobEdit.FederalJob = true;
                }
                else
                {
                    objEditJobInfoVM.objBatchJobEdit.FederalJob = false;
                }

                if (objEditJobInfoVM.RbtJobType == "Residential")
                {
                    objEditJobInfoVM.objBatchJobEdit.ResidentialBox = true;
                }
                else
                {
                    objEditJobInfoVM.objBatchJobEdit.ResidentialBox = false;
                }

                result = ADBatchJob.SaveLienJobInfo(objEditJobInfoVM.objBatchJobEdit);
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return Json(result);
        }

        //Edit More Job Info
        [HttpPost]
        public ActionResult EditMoreJobInfo(string Id)
        {
            LoadBatchJobDetails(Id);

            EditMoreJobInfoVM objEditMoreJobInfoVM = new EditMoreJobInfoVM();
            objEditMoreJobInfoVM.objBatchJobEdit = ADBatchJob.ReadBatchJob(Convert.ToInt32(Id));
            objEditMoreJobInfoVM.Id = Id;

            return PartialView(GetVirtualPath("_EditMoreJobInfo"), objEditMoreJobInfoVM);
        }

        //Save  MoreJobInfo Details
        [HttpPost]
        public ActionResult SaveMoreJobInfo(EditMoreJobInfoVM objEditMoreJobInfoVM)
        {
            int result = 0;
            try
            {
                objEditMoreJobInfoVM.objBatchJobEdit.Id = Convert.ToInt32(objEditMoreJobInfoVM.Id);

                result = ADBatchJob.SaveLienMoreJobInfo(objEditMoreJobInfoVM.objBatchJobEdit);
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return Json(result);
        }

        //Save for SameAsCust checkbox  Details
        [HttpPost]
        public ActionResult SaveGCSameAsCust(string Id)
        {
            int result = 0;
            try
            {
                result = ADBatchJob.SaveGCSameAsCust(Id);
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return Json(result);
        }


        //Edit GeneralContractor Info
        [HttpPost]
        public ActionResult EditGeneralContractorInfo(string Id)
        {
            LoadBatchJobDetails(Id);

            EditGeneralContractorInfoVM objEditGeneralContractorInfoVM = new EditGeneralContractorInfoVM();
            objEditGeneralContractorInfoVM.objBatchJobEdit = ADBatchJob.ReadBatchJob(Convert.ToInt32(Id));
            objEditGeneralContractorInfoVM.Id = Id;

            return PartialView(GetVirtualPath("_EditGeneralContractorInfo"), objEditGeneralContractorInfoVM);
        }

        //Save  GeneralContractor Details
        [HttpPost]
        public ActionResult SaveGeneralContractorInfo(EditGeneralContractorInfoVM objEditGeneralContractorInfoVM)
        {
            int result = 0;
            try
            {
                objEditGeneralContractorInfoVM.objBatchJobEdit.Id = Convert.ToInt32(objEditGeneralContractorInfoVM.Id);
                result = ADBatchJob.SaveGeneralContractorInfo(objEditGeneralContractorInfoVM.objBatchJobEdit);
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return Json(result);
        }

        //Save for SameAsGC checkbox  Details
        [HttpPost]
        public ActionResult SaveOwnerSameAsGC(string Id)
        {
            int result = 0;
            try
            {
                result = ADBatchJob.SaveOwnerSameAsGC(Id);
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return Json(result);
        }

        //Save for SameAsGC checkbox  Details
        [HttpPost]
        public ActionResult SaveOwnerchkCopyJobAddr(string Id)
        {
            int result = 0;
            try
            {
                result = ADBatchJob.SaveOwnerchkCopyJobAddr(Id);
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return Json(result);
        }


        //Save for MLAgent checkbox  Details
        [HttpPost]
        public ActionResult SaveOwnerMLAgent(string Id)
        {
            int result = 0;
            try
            {
                result = ADBatchJob.SaveOwnerMLAgent(Id);
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return Json(result);
        }

        //Edit Owner Info
        [HttpPost]
        public ActionResult EditOwnerInfo(string Id)
        {
            LoadBatchJobDetails(Id);

            EditOwnerInfoVM objEditOwnerInfoVM = new EditOwnerInfoVM();
            objEditOwnerInfoVM.objBatchJobEdit = ADBatchJob.ReadBatchJob(Convert.ToInt32(Id));
            objEditOwnerInfoVM.Id = Id;



            return PartialView(GetVirtualPath("_EditOwnerInfo"), objEditOwnerInfoVM);
        }

        //Save  Owner Details
        [HttpPost]
        public ActionResult SaveOwnerInfo(EditOwnerInfoVM objEditOwnerInfoVM)
        {
            int result = 0;
            try
            {
                objEditOwnerInfoVM.objBatchJobEdit.Id = Convert.ToInt32(objEditOwnerInfoVM.Id);
                result = ADBatchJob.SaveOwnerInfo(objEditOwnerInfoVM.objBatchJobEdit);
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return Json(result);
        }


        //Edit LenderSurety Info
        [HttpPost]
        public ActionResult EditLenderSuretyInfo(string Id)
        {
            LoadBatchJobDetails(Id);

            EditLenderSuretyInfoVM objEditLenderSuretyInfoVM = new EditLenderSuretyInfoVM();
            objEditLenderSuretyInfoVM.objBatchJobEdit = ADBatchJob.ReadBatchJob(Convert.ToInt32(Id));
            objEditLenderSuretyInfoVM.Id = Id;

            return PartialView(GetVirtualPath("_EditLenderSuretyInfo"), objEditLenderSuretyInfoVM);
        }

        //Save  Owner Details
        [HttpPost]
        public ActionResult SaveLenderSuretyInfo(EditLenderSuretyInfoVM objEditLenderSuretyInfoVM)
        {
            int result = 0;
            try
            {
                objEditLenderSuretyInfoVM.objBatchJobEdit.Id = Convert.ToInt32(objEditLenderSuretyInfoVM.Id);
                result = ADBatchJob.SaveLenderSuretyInfo(objEditLenderSuretyInfoVM.objBatchJobEdit);
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return Json(result);
        }
        
        //Edit LenderSurety Info
        [HttpPost]
        public ActionResult EditDesigneeInfo(string Id)
        {
            LoadBatchJobDetails(Id);

            EditDesigneeInfoVM objEditDesigneeInfoVM = new EditDesigneeInfoVM();
            objEditDesigneeInfoVM.objBatchJobEdit = ADBatchJob.ReadBatchJob(Convert.ToInt32(Id));
            objEditDesigneeInfoVM.Id = Id;

            return PartialView(GetVirtualPath("_EditDesigneeInfo"), objEditDesigneeInfoVM);
        }

        //Save  Designee Details
        [HttpPost]
        public ActionResult SaveDesigneeInfo(EditDesigneeInfoVM objEditDesigneeInfoVM)
        {
            int result = 0;
            try
            {
                objEditDesigneeInfoVM.objBatchJobEdit.Id = Convert.ToInt32(objEditDesigneeInfoVM.Id);
                result = ADBatchJob.SaveDesigneeInfo(objEditDesigneeInfoVM.objBatchJobEdit);
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return Json(result);
        }

        //AddNew Other Legal Details
        [HttpPost]
        public ActionResult AddNewOtherLegalDetails(string Id)
        {
            LoadBatchJobDetails(Id);

            AddNewOtherLegalDetailsVM objAddNewOtherLegalDetailsVM = new AddNewOtherLegalDetailsVM();
            objAddNewOtherLegalDetailsVM.objBatchJobId = Id;

            return PartialView(GetVirtualPath("_AddNewOtherLegalDetails"), objAddNewOtherLegalDetailsVM);
        }

        //Save  Owner Details
        [HttpPost]
        public ActionResult SaveOtherLegalDetails(AddNewOtherLegalDetailsVM objAddNewOtherLegalDetailsVM)
        {
            int result = 0;
            try
            {
                objAddNewOtherLegalDetailsVM.objBatchJobLegalParties.BatchJobId = Convert.ToInt32(objAddNewOtherLegalDetailsVM.objBatchJobId);
                result = ADBatchJobLegalParties.SaveLienBatchJobLegalParties(objAddNewOtherLegalDetailsVM.objBatchJobLegalParties);
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return Json(result);
        }

        //Edit LenderSurety Info
        [HttpPost]
        public ActionResult EditOtherLegalDetails(string Id)
        {
            LoadBatchJobDetails(Id);

            EditOtherLegalDetailsVM objEditOtherLegalDetailsVM = new EditOtherLegalDetailsVM();
            objEditOtherLegalDetailsVM.objBatchJobLegalPartiesEdit = ADBatchJobLegalParties.ReadBatchJobLegalParties(Convert.ToInt32(Id));
            objEditOtherLegalDetailsVM.Id = Id;
            objEditOtherLegalDetailsVM.objBatchJobId =Convert.ToString(objEditOtherLegalDetailsVM.objBatchJobLegalPartiesEdit.BatchJobId);

            return PartialView(GetVirtualPath("_EditOtherLegalDetails"), objEditOtherLegalDetailsVM);
        }

        //Update Other Legal Details
        [HttpPost]
        public ActionResult UpdateOtherLegalDetails(EditOtherLegalDetailsVM objEditOtherLegalDetailsVM)
        {
            int result = 0;
            try
            {
                objEditOtherLegalDetailsVM.objBatchJobLegalPartiesEdit.BatchJobId = Convert.ToInt32(objEditOtherLegalDetailsVM.objBatchJobId);
                objEditOtherLegalDetailsVM.objBatchJobLegalPartiesEdit.Id = Convert.ToInt32(objEditOtherLegalDetailsVM.Id);

                result = ADBatchJobLegalParties.SaveLienBatchJobLegalParties(objEditOtherLegalDetailsVM.objBatchJobLegalPartiesEdit);
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return Json(result);
        }


        //Delete Note Details
        [HttpPost]
        public ActionResult DeleteOtherLegalDetails(string BatchJobId,string Id)
        {
            bool result = ADBatchJobLegalParties.DeleteBatchJobLegalParties(Id);
            if (result)
            {
                return Json(result);
            }
            else
            {
                return Json(0);
            }
        }

        #region CityStateZip lookup for Job Info
        //Search CityStateZip
        [HttpPost]
        public ActionResult SearchCityStateZip(string Id, string City, string State, string Zip)
        {
            //LoadBatchJobDetails(Id);

            BatchJobVM objBatchJobVM = new BatchJobVM();
            objBatchJobVM.objBatchJob = new ADBatchJob();
            objBatchJobVM.objBatchJob = ADBatchJob.ReadBatchJob(Convert.ToInt32(Id));
            objBatchJobVM.Id = Id;
            objBatchJobVM.City = objBatchJobVM.objBatchJob.JobCity;
            objBatchJobVM.State = objBatchJobVM.objBatchJob.JobState;
            objBatchJobVM.Zip = objBatchJobVM.objBatchJob.JobZip;
            objBatchJobVM.objZipCodeDt = ADZipCode.GetZipCodeDtList(City, State, Zip);
            //if (objBatchJobVM.objZipCodeDt.Rows.Count > 0)
            //{
                objBatchJobVM.objZipCodeList = ADZipCode.GetZipCode(objBatchJobVM.objZipCodeDt);
           // }

            return PartialView(GetVirtualPath("_SearchCityStateZip"), objBatchJobVM);
        }

        //Select City/State/Zip Details
        [HttpPost]
        public ActionResult SelectCityStateZipDetails(string ZIPCodeId, string Id)
        {
            BatchJobVM objBatchJobVM = new BatchJobVM();
            objBatchJobVM.Id = Id;
            objBatchJobVM.objZipCode = new ADZipCode();
            objBatchJobVM.objZipCode = ADZipCode.ReadZipCode(Convert.ToInt32(ZIPCodeId));
 
            objBatchJobVM.City = objBatchJobVM.objZipCode.CityName;
            objBatchJobVM.State = objBatchJobVM.objZipCode.StateAbbr;
            objBatchJobVM.Zip = objBatchJobVM.objZipCode.ZIPCode;

            objBatchJobVM.objZipCodeDt = ADZipCode.GetZipCodeDtList(objBatchJobVM.City, objBatchJobVM.State, objBatchJobVM.Zip);
            if (objBatchJobVM.objZipCodeDt.Rows.Count > 0)
            {
                objBatchJobVM.objZipCodeList = ADZipCode.GetZipCode(objBatchJobVM.objZipCodeDt);
            }

            return PartialView(GetVirtualPath("_SearchCityStateZip"), objBatchJobVM);
        }

        //RefreshList CityStateZip
        [HttpPost]
        public ActionResult RefreshListCityStateZip(BatchJobVM objBatchJobVM)
        {
  
            objBatchJobVM.objBatchJob = new ADBatchJob();
 
            if(string.IsNullOrEmpty(objBatchJobVM.City))
            {
                objBatchJobVM.City = "";
            }
            if (string.IsNullOrEmpty(objBatchJobVM.State))
            {
                objBatchJobVM.State = "";
            }
            if (string.IsNullOrEmpty(objBatchJobVM.Zip))
            {
                objBatchJobVM.Zip = "";
            }

            objBatchJobVM.objZipCodeDt = ADZipCode.GetZipCodeDtList(objBatchJobVM.City, objBatchJobVM.State, objBatchJobVM.Zip);
            //if (objBatchJobVM.objZipCodeDt.Rows.Count > 0)
            //{
                objBatchJobVM.objZipCodeList = ADZipCode.GetZipCode(objBatchJobVM.objZipCodeDt);
            //}

            return PartialView(GetVirtualPath("_SearchCityStateZip"), objBatchJobVM);
        }

        //Save SearchCityStateZip
        [HttpPost]
        public ActionResult SaveSearchCityStateZip(BatchJobVM objBatchJobVM)
        {
            int result = 0;
            try
            {
                objBatchJobVM.objBatchJob = new ADBatchJob();
                objBatchJobVM.objBatchJob.Id = Convert.ToInt32(objBatchJobVM.Id);
                objBatchJobVM.objBatchJob.JobCity = objBatchJobVM.City;
                objBatchJobVM.objBatchJob.JobState = objBatchJobVM.State;
                objBatchJobVM.objBatchJob.JobZip = objBatchJobVM.Zip;

                result = ADBatchJob.SaveLienCityStateZipJobInfo(objBatchJobVM.objBatchJob);
            }
            catch (Exception ex)
            {
                result = 0;
            }

            return Json(result);
        }

        #endregion

        #region CityStateZip lookup for Owner Info
        //Search CityStateZip
        [HttpPost]
        public ActionResult SearchCityStateZipOwner(string Id, string City, string State, string Zip)
        {
            //LoadBatchJobDetails(Id);

            BatchJobVM objBatchJobVM = new BatchJobVM();
            objBatchJobVM.objBatchJob = new ADBatchJob();
            objBatchJobVM.objBatchJob = ADBatchJob.ReadBatchJob(Convert.ToInt32(Id));
            objBatchJobVM.Id = Id;
            objBatchJobVM.City = objBatchJobVM.objBatchJob.OwnrCity;
            objBatchJobVM.State = objBatchJobVM.objBatchJob.OwnrState;
            objBatchJobVM.Zip = objBatchJobVM.objBatchJob.OwnrZip;
            objBatchJobVM.objZipCodeDt = ADZipCode.GetZipCodeDtList(City, State, Zip);
            //if (objBatchJobVM.objZipCodeDt.Rows.Count > 0)
            //{
                objBatchJobVM.objZipCodeList = ADZipCode.GetZipCode(objBatchJobVM.objZipCodeDt);
            //}

            return PartialView(GetVirtualPath("_SearchCityStateZipOwner"), objBatchJobVM);
        }

        //Select City/State/Zip Details
        [HttpPost]
        public ActionResult SelectCityStateZipOwnerDetails(string ZIPCodeId, string Id)
        {
            BatchJobVM objBatchJobVM = new BatchJobVM();
            objBatchJobVM.Id = Id;
            objBatchJobVM.objZipCode = new ADZipCode();
            objBatchJobVM.objZipCode = ADZipCode.ReadZipCode(Convert.ToInt32(ZIPCodeId));

            objBatchJobVM.City = objBatchJobVM.objZipCode.CityName;
            objBatchJobVM.State = objBatchJobVM.objZipCode.StateAbbr;
            objBatchJobVM.Zip = objBatchJobVM.objZipCode.ZIPCode;

            objBatchJobVM.objZipCodeDt = ADZipCode.GetZipCodeDtList(objBatchJobVM.City, objBatchJobVM.State, objBatchJobVM.Zip);
            if (objBatchJobVM.objZipCodeDt.Rows.Count > 0)
            {
                objBatchJobVM.objZipCodeList = ADZipCode.GetZipCode(objBatchJobVM.objZipCodeDt);
            }

            return PartialView(GetVirtualPath("_SearchCityStateZipOwner"), objBatchJobVM);
        }

        //RefreshList CityStateZip
        [HttpPost]
        public ActionResult RefreshListCityStateZipOwner(BatchJobVM objBatchJobVM)
        {

            objBatchJobVM.objBatchJob = new ADBatchJob();

            if (string.IsNullOrEmpty(objBatchJobVM.City))
            {
                objBatchJobVM.City = "";
            }
            if (string.IsNullOrEmpty(objBatchJobVM.State))
            {
                objBatchJobVM.State = "";
            }
            if (string.IsNullOrEmpty(objBatchJobVM.Zip))
            {
                objBatchJobVM.Zip = "";
            }

            objBatchJobVM.objZipCodeDt = ADZipCode.GetZipCodeDtList(objBatchJobVM.City, objBatchJobVM.State, objBatchJobVM.Zip);
            //if (objBatchJobVM.objZipCodeDt.Rows.Count > 0)
            //{
            objBatchJobVM.objZipCodeList = ADZipCode.GetZipCode(objBatchJobVM.objZipCodeDt);
            //}

            return PartialView(GetVirtualPath("_SearchCityStateZipOwner"), objBatchJobVM);
        }

        //Save SearchCityStateZip
        [HttpPost]
        public ActionResult SaveSearchCityStateZipOwner(BatchJobVM objBatchJobVM)
        {
            int result = 0;
            try
            {
                objBatchJobVM.objBatchJob = new ADBatchJob();
                objBatchJobVM.objBatchJob.Id = Convert.ToInt32(objBatchJobVM.Id);
                objBatchJobVM.objBatchJob.OwnrCity = objBatchJobVM.City;
                objBatchJobVM.objBatchJob.OwnrState = objBatchJobVM.State;
                objBatchJobVM.objBatchJob.OwnrZip = objBatchJobVM.Zip;

                result = ADBatchJob.SaveCityStateZipOwnerInfo(objBatchJobVM.objBatchJob);
            }
            catch (Exception ex)
            {
                result = 0;
            }

            return Json(result);
        }

        #endregion

        #region CityStateZip lookup for Lender/Surety
        //Search CityStateZip
        [HttpPost]
        public ActionResult SearchCityStateZipLenderSurety(string Id, string City, string State, string Zip)
        {
            //LoadBatchJobDetails(Id);

            BatchJobVM objBatchJobVM = new BatchJobVM();
            objBatchJobVM.objBatchJob = new ADBatchJob();
            objBatchJobVM.objBatchJob = ADBatchJob.ReadBatchJob(Convert.ToInt32(Id));
            objBatchJobVM.Id = Id;
            objBatchJobVM.City = objBatchJobVM.objBatchJob.LenderCity;
            objBatchJobVM.State = objBatchJobVM.objBatchJob.LenderState;
            objBatchJobVM.Zip = objBatchJobVM.objBatchJob.LenderZip;
            objBatchJobVM.objZipCodeDt = ADZipCode.GetZipCodeDtList(City, State, Zip);
            //if (objBatchJobVM.objZipCodeDt.Rows.Count > 0)
            //{
            objBatchJobVM.objZipCodeList = ADZipCode.GetZipCode(objBatchJobVM.objZipCodeDt);
            //}

            return PartialView(GetVirtualPath("_SearchCityStateZipLenderSurety"), objBatchJobVM);
        }

        //Select City/State/Zip Details
        [HttpPost]
        public ActionResult SelectCityStateZipLenderSuretyDetails(string ZIPCodeId, string Id)
        {
            BatchJobVM objBatchJobVM = new BatchJobVM();
            objBatchJobVM.Id = Id;
            objBatchJobVM.objZipCode = new ADZipCode();
            objBatchJobVM.objZipCode = ADZipCode.ReadZipCode(Convert.ToInt32(ZIPCodeId));

            objBatchJobVM.City = objBatchJobVM.objZipCode.CityName;
            objBatchJobVM.State = objBatchJobVM.objZipCode.StateAbbr;
            objBatchJobVM.Zip = objBatchJobVM.objZipCode.ZIPCode;

            objBatchJobVM.objZipCodeDt = ADZipCode.GetZipCodeDtList(objBatchJobVM.City, objBatchJobVM.State, objBatchJobVM.Zip);
            if (objBatchJobVM.objZipCodeDt.Rows.Count > 0)
            {
                objBatchJobVM.objZipCodeList = ADZipCode.GetZipCode(objBatchJobVM.objZipCodeDt);
            }

            return PartialView(GetVirtualPath("_SearchCityStateZipLenderSurety"), objBatchJobVM);
        }

        //RefreshList CityStateZip
        [HttpPost]
        public ActionResult RefreshListCityStateZipLenderSurety(BatchJobVM objBatchJobVM)
        {

            objBatchJobVM.objBatchJob = new ADBatchJob();

            if (string.IsNullOrEmpty(objBatchJobVM.City))
            {
                objBatchJobVM.City = "";
            }
            if (string.IsNullOrEmpty(objBatchJobVM.State))
            {
                objBatchJobVM.State = "";
            }
            if (string.IsNullOrEmpty(objBatchJobVM.Zip))
            {
                objBatchJobVM.Zip = "";
            }

            objBatchJobVM.objZipCodeDt = ADZipCode.GetZipCodeDtList(objBatchJobVM.City, objBatchJobVM.State, objBatchJobVM.Zip);
            //if (objBatchJobVM.objZipCodeDt.Rows.Count > 0)
            //{
            objBatchJobVM.objZipCodeList = ADZipCode.GetZipCode(objBatchJobVM.objZipCodeDt);
            //}

            return PartialView(GetVirtualPath("_SearchCityStateZipLenderSurety"), objBatchJobVM);
        }

        //Save SearchCityStateZip
        [HttpPost]
        public ActionResult SaveSearchCityStateZipLenderSurety(BatchJobVM objBatchJobVM)
        {
            int result = 0;
            try
            {
                objBatchJobVM.objBatchJob = new ADBatchJob();
                objBatchJobVM.objBatchJob.Id = Convert.ToInt32(objBatchJobVM.Id);
                objBatchJobVM.objBatchJob.LenderCity = objBatchJobVM.City;
                objBatchJobVM.objBatchJob.LenderState = objBatchJobVM.State;
                objBatchJobVM.objBatchJob.LenderZip = objBatchJobVM.Zip;

                result = ADBatchJob.SaveCityStateZipLenderSuretyInfo(objBatchJobVM.objBatchJob);
            }
            catch (Exception ex)
            {
                result = 0;
            }

            return Json(result);
        }

        #endregion

        #region CityStateZip lookup for Designee
        //Search CityStateZip
        [HttpPost]
        public ActionResult SearchCityStateZipDesignee(string Id, string City, string State, string Zip)
        {
            //LoadBatchJobDetails(Id);

            BatchJobVM objBatchJobVM = new BatchJobVM();
            objBatchJobVM.objBatchJob = new ADBatchJob();
            objBatchJobVM.objBatchJob = ADBatchJob.ReadBatchJob(Convert.ToInt32(Id));
            objBatchJobVM.Id = Id;
            objBatchJobVM.City = objBatchJobVM.objBatchJob.DesigneeCity;
            objBatchJobVM.State = objBatchJobVM.objBatchJob.DesigneeState;
            objBatchJobVM.Zip = objBatchJobVM.objBatchJob.DesigneeZip;
            objBatchJobVM.objZipCodeDt = ADZipCode.GetZipCodeDtList(City, State, Zip);
            //if (objBatchJobVM.objZipCodeDt.Rows.Count > 0)
            //{
            objBatchJobVM.objZipCodeList = ADZipCode.GetZipCode(objBatchJobVM.objZipCodeDt);
            //}

            return PartialView(GetVirtualPath("_SearchCityStateZipDesignee"), objBatchJobVM);
        }

        //Select City/State/Zip Details
        [HttpPost]
        public ActionResult SelectCityStateZipDesigneeDetails(string ZIPCodeId, string Id)
        {
            BatchJobVM objBatchJobVM = new BatchJobVM();
            objBatchJobVM.Id = Id;
            objBatchJobVM.objZipCode = new ADZipCode();
            objBatchJobVM.objZipCode = ADZipCode.ReadZipCode(Convert.ToInt32(ZIPCodeId));

            objBatchJobVM.City = objBatchJobVM.objZipCode.CityName;
            objBatchJobVM.State = objBatchJobVM.objZipCode.StateAbbr;
            objBatchJobVM.Zip = objBatchJobVM.objZipCode.ZIPCode;

            objBatchJobVM.objZipCodeDt = ADZipCode.GetZipCodeDtList(objBatchJobVM.City, objBatchJobVM.State, objBatchJobVM.Zip);
            if (objBatchJobVM.objZipCodeDt.Rows.Count > 0)
            {
                objBatchJobVM.objZipCodeList = ADZipCode.GetZipCode(objBatchJobVM.objZipCodeDt);
            }

            return PartialView(GetVirtualPath("_SearchCityStateZipDesignee"), objBatchJobVM);
        }

        //RefreshList CityStateZip
        [HttpPost]
        public ActionResult RefreshListCityStateZipDesignee(BatchJobVM objBatchJobVM)
        {

            objBatchJobVM.objBatchJob = new ADBatchJob();

            if (string.IsNullOrEmpty(objBatchJobVM.City))
            {
                objBatchJobVM.City = "";
            }
            if (string.IsNullOrEmpty(objBatchJobVM.State))
            {
                objBatchJobVM.State = "";
            }
            if (string.IsNullOrEmpty(objBatchJobVM.Zip))
            {
                objBatchJobVM.Zip = "";
            }

            objBatchJobVM.objZipCodeDt = ADZipCode.GetZipCodeDtList(objBatchJobVM.City, objBatchJobVM.State, objBatchJobVM.Zip);
            //if (objBatchJobVM.objZipCodeDt.Rows.Count > 0)
            //{
            objBatchJobVM.objZipCodeList = ADZipCode.GetZipCode(objBatchJobVM.objZipCodeDt);
            //}

            return PartialView(GetVirtualPath("_SearchCityStateZipDesignee"), objBatchJobVM);
        }

        //Save SearchCityStateZip
        [HttpPost]
        public ActionResult SaveSearchCityStateZipDesignee(BatchJobVM objBatchJobVM)
        {
            int result = 0;
            try
            {
                objBatchJobVM.objBatchJob = new ADBatchJob();
                objBatchJobVM.objBatchJob.Id = Convert.ToInt32(objBatchJobVM.Id);
                objBatchJobVM.objBatchJob.DesigneeCity = objBatchJobVM.City;
                objBatchJobVM.objBatchJob.DesigneeState = objBatchJobVM.State;
                objBatchJobVM.objBatchJob.DesigneeZip = objBatchJobVM.Zip;

                result = ADBatchJob.SaveCityStateZipDesigneeInfo(objBatchJobVM.objBatchJob);
            }
            catch (Exception ex)
            {
                result = 0;
            }

            return Json(result);
        }

        #endregion

        #region CityStateZip lookup for OtherLegal
        //Search CityStateZip
        [HttpPost]
        public ActionResult SearchCityStateZipOtherLegal(string Id, string City, string State, string Zip)
        {
            //LoadBatchJobDetails(Id);

            BatchJobVM objBatchJobVM = new BatchJobVM();
            objBatchJobVM.objBatchJobLegalParties = new ADBatchJobLegalParties();
            objBatchJobVM.objBatchJobLegalParties = ADBatchJobLegalParties.ReadBatchJobLegalParties(Convert.ToInt32(Id));
            objBatchJobVM.Id = Id;
            objBatchJobVM.City = objBatchJobVM.objBatchJobLegalParties.City;
            objBatchJobVM.State = objBatchJobVM.objBatchJobLegalParties.State;
            objBatchJobVM.Zip = objBatchJobVM.objBatchJobLegalParties.PostalCode;
      
            objBatchJobVM.objZipCodeDt = ADZipCode.GetZipCodeDtList(City, State, Zip);
            //if (objBatchJobVM.objZipCodeDt.Rows.Count > 0)
            //{
            objBatchJobVM.objZipCodeList = ADZipCode.GetZipCode(objBatchJobVM.objZipCodeDt);
            //}

            return PartialView(GetVirtualPath("_SearchCityStateZipOtherLegal"), objBatchJobVM);
        }

        //Select City/State/Zip Details
        [HttpPost]
        public ActionResult SelectCityStateZipOtherLegalDetails(string ZIPCodeId, string Id)
        {
            BatchJobVM objBatchJobVM = new BatchJobVM();
            objBatchJobVM.Id = Id;
            objBatchJobVM.objZipCode = new ADZipCode();
            objBatchJobVM.objZipCode = ADZipCode.ReadZipCode(Convert.ToInt32(ZIPCodeId));

            objBatchJobVM.City = objBatchJobVM.objZipCode.CityName;
            objBatchJobVM.State = objBatchJobVM.objZipCode.StateAbbr;
            objBatchJobVM.Zip = objBatchJobVM.objZipCode.ZIPCode;

            objBatchJobVM.objZipCodeDt = ADZipCode.GetZipCodeDtList(objBatchJobVM.City, objBatchJobVM.State, objBatchJobVM.Zip);
            if (objBatchJobVM.objZipCodeDt.Rows.Count > 0)
            {
                objBatchJobVM.objZipCodeList = ADZipCode.GetZipCode(objBatchJobVM.objZipCodeDt);
            }

            return PartialView(GetVirtualPath("_SearchCityStateZipOtherLegal"), objBatchJobVM);
        }

        //RefreshList CityStateZip
        [HttpPost]
        public ActionResult RefreshListCityStateZipOtherLegal(BatchJobVM objBatchJobVM)
        {

            objBatchJobVM.objBatchJob = new ADBatchJob();

            if (string.IsNullOrEmpty(objBatchJobVM.City))
            {
                objBatchJobVM.City = "";
            }
            if (string.IsNullOrEmpty(objBatchJobVM.State))
            {
                objBatchJobVM.State = "";
            }
            if (string.IsNullOrEmpty(objBatchJobVM.Zip))
            {
                objBatchJobVM.Zip = "";
            }

            objBatchJobVM.objZipCodeDt = ADZipCode.GetZipCodeDtList(objBatchJobVM.City, objBatchJobVM.State, objBatchJobVM.Zip);
            //if (objBatchJobVM.objZipCodeDt.Rows.Count > 0)
            //{
            objBatchJobVM.objZipCodeList = ADZipCode.GetZipCode(objBatchJobVM.objZipCodeDt);
            //}

            return PartialView(GetVirtualPath("_SearchCityStateZipOtherLegal"), objBatchJobVM);
        }

        //Save SearchCityStateZip
        [HttpPost]
        public ActionResult SaveSearchCityStateZipOtherLegal(BatchJobVM objBatchJobVM)
        {
            int result = 0;
            try
            {
                objBatchJobVM.objBatchJobLegalParties = new ADBatchJobLegalParties();
                objBatchJobVM.objBatchJobLegalParties.Id = Convert.ToInt32(objBatchJobVM.Id);
                objBatchJobVM.objBatchJobLegalParties.City = objBatchJobVM.City;
                objBatchJobVM.objBatchJobLegalParties.State = objBatchJobVM.State;
                objBatchJobVM.objBatchJobLegalParties.PostalCode = objBatchJobVM.Zip;

                result = ADBatchJobLegalParties.SaveCityStateZipOtherLegalInfo(objBatchJobVM.objBatchJobLegalParties);
            }
            catch (Exception ex)
            {
                result = 0;
            }

            return Json(result);
        }

        #endregion


        //Edit Legal Info
        [HttpPost]
        public ActionResult EditLegalInfo(string Id)
        {
            LoadBatchJobDetails(Id);

            EditLegalInfoVM objEditLegalInfoVM = new EditLegalInfoVM();
            objEditLegalInfoVM.objBatchJobEdit = ADBatchJob.ReadBatchJob(Convert.ToInt32(Id));
            objEditLegalInfoVM.Id = Id;

            return PartialView(GetVirtualPath("_EditLegalInfo"), objEditLegalInfoVM);
        }

        //Save  Legal Info Details
        [HttpPost]
        public ActionResult SaveLegalInfo(EditLegalInfoVM objEditLegalInfoVM)
        {
            int result = 0;
            try
            {
                objEditLegalInfoVM.objBatchJobEdit.Id = Convert.ToInt32(objEditLegalInfoVM.Id);
                result = ADBatchJob.SaveLegalInfo(objEditLegalInfoVM.objBatchJobEdit);
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return Json(result);
        }

        //Edit Legal Info
        [HttpPost]
        public ActionResult EditNotesInfo(string Id)
        {
            LoadBatchJobDetails(Id);

            EditNotesInfoVM objEditNotesInfoVM = new EditNotesInfoVM();
            objEditNotesInfoVM.objBatchJobEdit = ADBatchJob.ReadBatchJob(Convert.ToInt32(Id));
            objEditNotesInfoVM.Id = Id;

            return PartialView(GetVirtualPath("_EditNotesInfo"), objEditNotesInfoVM);
        }

        //Save  Notes  Details
        [HttpPost]
        public ActionResult SaveNotesInfo(EditNotesInfoVM objEditNotesInfoVM)
        {
            int result = 0;
            try
            {
                objEditNotesInfoVM.objBatchJobEdit.Id = Convert.ToInt32(objEditNotesInfoVM.Id);
                result = ADBatchJob.SaveNotesInfo(objEditNotesInfoVM.objBatchJobEdit);
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return Json(result);
        }


        //Add New Document
        [HttpPost]
        public ActionResult AddDocument(string Id)
        {
            JobAttachmentsVM objJobAttachmentsVM = new JobAttachmentsVM();
            objJobAttachmentsVM.BatchJobId = Id;
            return PartialView(GetVirtualPath("_AddNewDocument"), objJobAttachmentsVM);
        }

        //Save File Details
        [HttpPost]
        public ActionResult SaveDocument()
        {
            try
            {
                var file = Request.Files[0];
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/Output/UploadedFiles"), fileName);
                file.SaveAs(path);
                Session["path"] = path;
                return Json(true);
            }
            catch (Exception ex)
            {
                return Json(false);
            }
        }

        //Save Document Details
        [HttpPost]
        public ActionResult SaveDocumentInputs(JobAttachmentsVM objJobAttachmentsVM)
        {
            int result = 0;
            try
            {
                string path = Convert.ToString(Session["path"]);

                if (!String.IsNullOrEmpty(path))
                {
                    byte[] bytes = System.IO.File.ReadAllBytes(path);
                    objJobAttachmentsVM.objJobAttachments.DocumentImage = bytes;
                    objJobAttachmentsVM.objJobAttachments.FileName = path.Substring(path.LastIndexOf("\\") + 1);
                }

                objJobAttachmentsVM.objJobAttachments.BatchJobId = Convert.ToInt32(objJobAttachmentsVM.BatchJobId);
                objJobAttachmentsVM.objJobAttachments.DocumentType = objJobAttachmentsVM.DocumentType;
                objJobAttachmentsVM.objJobAttachments.Id = Convert.ToInt32(objJobAttachmentsVM.Id);

                result = ADJobAttachments.SaveJobAttachment(objJobAttachmentsVM.objJobAttachments);
                Session["path"] = null;

                return Json(result);
            }
            catch
            {
                result = -1;
                return Json(result);
            }

        }

        //Update Document Details
        [HttpPost]
        public ActionResult UpdateDocumentInputs(JobAttachmentsVM objJobAttachmentsVM)
        {
            int result = 0;
            try
            {
                string path = Convert.ToString(Session["path"]);

                if (!String.IsNullOrEmpty(path))
                {
                    byte[] bytes = System.IO.File.ReadAllBytes(path);
                    objJobAttachmentsVM.objJobAttachmentsEdit.DocumentImage = bytes;
                    objJobAttachmentsVM.objJobAttachmentsEdit.FileName = path.Substring(path.LastIndexOf("\\") + 1);
                }

                objJobAttachmentsVM.objJobAttachmentsEdit.BatchJobId = Convert.ToInt32(objJobAttachmentsVM.BatchJobId);
                objJobAttachmentsVM.objJobAttachmentsEdit.DocumentType = objJobAttachmentsVM.DocumentType;
                objJobAttachmentsVM.objJobAttachmentsEdit.Id = Convert.ToInt32(objJobAttachmentsVM.Id);

                result = ADJobAttachments.SaveJobAttachment(objJobAttachmentsVM.objJobAttachmentsEdit);
                Session["path"] = null;

                return Json(result);
            }
            catch
            {
                result = -1;
                return Json(result);
            }

        }

        //Get Document Details
        [HttpPost]
        public ActionResult GetDocumentDetails(string Id, string BatchJobId)
        {
            JobAttachmentsVM obj = new JobAttachmentsVM();
            obj.objJobAttachmentsEdit = ADJobAttachments.ReadJobAttachment(Convert.ToInt32(Id));
            obj.Id = Convert.ToString(Id);
            obj.BatchJobId = Convert.ToString(obj.objJobAttachmentsEdit.BatchJobId);
            obj.ddlDocumentType = obj.objJobAttachmentsEdit.DocumentType;
            return PartialView(GetVirtualPath("_EditDocument"), obj);
        }

        //Delete Document
        [HttpPost]
        public ActionResult DeleteDocument(string Id)
        {
            var result = ADJobAttachments.DeleteDocument(Id);
            return Json(result);
        }

        //View Document
        [HttpPost]
        public string ViewDocument(string Id)
        {
            var Attachment = ADJobAttachments.ReadJobAttachment(Convert.ToInt32(Id));
            var path = Path.Combine(Server.MapPath("~/Output/DownloadableFiles"), Attachment.FileName);
            ADJobAttachments.SaveFileToBeViewed(Attachment.DocumentImage, path);
            byte[] fileBytes = Attachment.DocumentImage;
            string fileName = path.Substring(path.LastIndexOf("\\") + 1);
            //fileName = HttpUtility.UrlEncode(fileName);
            //fileName = HttpUtility.UrlPathEncode(fileName); //Remove space with % 20
            fileName = Uri.EscapeDataString(fileName); //Remove # with %23 , space with %20

            if (path != null && path != "" && path != "Error")
            {
                //string filepath = Path.Combine(Url.Content("~"), "Output/DownloadableFiles/", System.IO.Path.GetFileName(path));
                string filepath = Path.Combine(Url.Content("~"), "Output/DownloadableFiles/", fileName);

                return (filepath);
            }
            return "error";
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/DataEntry/DataEntry/" + ViewName + ".cshtml";
            return path;
        }
    }
}
