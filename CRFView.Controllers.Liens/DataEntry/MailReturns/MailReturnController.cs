﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CRFView.Adapters.Liens.DataEntry.MailReturns.ViewModels;
using CRFView.Adapters;

namespace CRFView.Controllers.Liens.DataEntry.MailReturns
{
    public class MailReturnController : Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            MailReturnsVM objMailReturnsVM = new MailReturnsVM();

            objMailReturnsVM.dtBatchMailReturnList = ADBatchMailReturn.GetBatchMailReturnDetailDt();

            objMailReturnsVM.BatchMailReturnList = ADBatchMailReturn.GetBatchMailReturnDetailList(objMailReturnsVM.dtBatchMailReturnList);

            ViewBag.lblRecCount = "Total  (" + objMailReturnsVM.BatchMailReturnList.Count + ") Mail Returns";

            return PartialView(GetVirtualPath("_MailReturns"), objMailReturnsVM);
        }

        //Save MailReturn
        [HttpPost]
        public ActionResult SaveMailReturn(MailReturnsVM objMailReturnsVM)
        {
            int result = 0;

            string sCert = objMailReturnsVM.CertNum;
            if (sCert.Length == 22)
            {
                sCert = sCert.Substring(1, 4) + " " + sCert.Substring(5, 4) + " " + sCert.Substring(9, 4) + " " + sCert.Substring(13, 4) + " " + sCert.Substring(17, 4) + " " + sCert.Substring(21, 2);
            }
            else if (sCert.Length == 20)
            {
                sCert = sCert.Substring(1, 4) + " " + sCert.Substring(5, 4) + " " + sCert.Substring(9, 4) + " " + sCert.Substring(13, 4) + " " + sCert.Substring(17, 4);
            }

            objMailReturnsVM.dtGetJobNoticeSentDetails = ADBatchMailReturn.GetJobNoticeSentDetails(sCert);
            if (objMailReturnsVM.dtGetJobNoticeSentDetails.Rows.Count > 0)
            {
                ADBatchMailReturn objADBatchMailReturn = new ADBatchMailReturn();
                objADBatchMailReturn.BatchId = -1;
                objADBatchMailReturn.CertNum = objMailReturnsVM.CertNum;
                objADBatchMailReturn.JobId = Convert.ToInt32(objMailReturnsVM.dtGetJobNoticeSentDetails.Rows[0]["JobId"]);
                objADBatchMailReturn.JobNoticeHistoryId = Convert.ToInt32(objMailReturnsVM.dtGetJobNoticeSentDetails.Rows[0]["NoticeHistoryId"]);
                objADBatchMailReturn.MailStatus = objMailReturnsVM.RbtActionChangeOption;
                objADBatchMailReturn.LegalPartyType = Convert.ToString(objMailReturnsVM.dtGetJobNoticeSentDetails.Rows[0]["TypeCode"]);
          
                result = ADBatchMailReturn.SaveBatchMailReturn(objADBatchMailReturn);
            }
            else
            {
                result = 0;
            }

            return Json(result);
        }

        //Save MailReturn
        [HttpPost]
        public ActionResult PostBatchMailReturns(MailReturnsVM objMailReturnsVM)
        {
            int result = 0;
             
            result = ADBatchMailReturn.PostBatchMailReturns(objMailReturnsVM);

            return Json(result);
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/DataEntry/MailReturns/" + ViewName + ".cshtml";
            return path;
        }
    }
}
