﻿using CRFView.Adapters;
using CRFView.Adapters.Liens.DataEntry.PostBatch.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace CRFView.Controllers.Liens.DataEntry.PostBatch
{
    public class PostBatchController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            PostBatchVM objPostBatchVM = new PostBatchVM();

            DataTable dtLienBatchForPostBatches = ADBatch.GetLienBatchForBatchPosts();

            if (dtLienBatchForPostBatches.Rows.Count < 1)
            {
                ViewBag.lblMessage = "No Batches to be Posted";
                ViewBag.btnOK = "Hide OK";
            }
            else
            {
                ViewBag.lblMessage = "(" + dtLienBatchForPostBatches.Rows.Count + ") Internal Batches to Be Posted";
                ViewBag.lblMessage += Environment.NewLine;
                ViewBag.lblMessage += "      Press Ok to Post";
                ViewBag.btnOK = "Show OK";
            }

            return PartialView(GetVirtualPath("_PostBatch"), objPostBatchVM);
        }

        
        //Batch Posted
        //[HttpPost]
        //public ActionResult BatchPosted(PostBatchVM objPostBatchVM)
        //{
        //    int result = 0;
        //    try
        //    {
        //        result = ADBatch.PostLienBatches();
        //    }
        //    catch (Exception ex)
        //    {
        //        result = 0;
        //    }
        //    return Json(result);
        //}


        //Print BatchPostedReport
        [HttpPost]
        public string PrintBatchPostedReport(PostBatchVM objPostBatchVM)
        {
            ADBatch obj = new ADBatch();

            string PdfUrl = obj.PostLienBatches(objPostBatchVM);
            if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
            {
                string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                return (path);
            }

            return "error";
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/DataEntry/PostBatch/" + ViewName + ".cshtml";
            return path;
        }
    }
}
