﻿using CRFView.Adapters;
using CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.DataEntry.EmailFaxUpdates
{
   public class EmailFaxUpdatesController: Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            JobListVM ObjVm = new JobListVM();
            string sortBy = "JobId ASC";
            List<ADvwJobsList> list = ADvwJobsList.GetJobList(sortBy);
           
            ObjVm.ObjJobList = list;
            return PartialView(GetVirtualPath("Index"), ObjVm);
        }

        //Get Sorted Result
        [HttpPost]
        public ActionResult GetSortedResult(string SortByValue, string SortTypeValue)
        {

            string SortType = "ASC";
            string SortBy = "JobId";
            if (SortTypeValue == "-1")
            {
                SortType = "ASC";
            }
            else if (SortTypeValue == "1")
            {
                SortType = "DESC";
            }

            if (SortByValue == "-1" || string.IsNullOrEmpty(SortByValue))
            {
                SortBy = "CustFax " + SortType;
            }
            else if (SortByValue == "1")
            {
                SortBy = "CustEmail " + SortType;
            }
            else if (SortByValue == "2")
            {
                SortBy = "GCFax " + SortType;
            }
            else if (SortByValue == "3")
            {
                SortBy = "GCEmail " + SortType;
            }
            else if (SortByValue == "4")
            {
                SortBy = "GCPhoNo " + SortType;
            }



            List<ADvwJobsList> list = ADvwJobsList.GetJobList(SortBy);
            JobListVM ObjVm = new JobListVM();
            ObjVm.ObjJobList = list;

            //Session["FilterList"] = ObjVm;
            return PartialView(GetVirtualPath("_ActiveJobsList"), ObjVm);
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/DataEntry/EmailFaxUpdates/" + ViewName + ".cshtml";
            return path;

        }
    }
}
