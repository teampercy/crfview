﻿using CRFView.Adapters;
using CRFView.Adapters.Liens.JVTNNoticeRequest.NoticeRequest.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Liens.JVTNNoticeRequest.NoticeRequest
{
    public class JVTNNoticeRequestController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            JVTNNoticeRequestVM objJVTNNoticeRequestVM = new JVTNNoticeRequestVM();
            
                ViewBag.lblMessage = "Press OK to Start TN Auto Notice Request Process.";
                ViewBag.lblMessage += Environment.NewLine; 
                ViewBag.btnOK = "Show OK";
            
            return PartialView(GetVirtualPath("_JVTNNoticeRequest"), objJVTNNoticeRequestVM);
        }

        //JV TN Notice Request Process
        [HttpPost]
        public ActionResult JVTNNoticeRequestProcess(JVTNNoticeRequestVM objJVTNNoticeRequestVM)
        {
            int result = 0;
            try
            {
                result = ADJob.ProcessJVTNNoticeRequest();
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return Json(result);
        }


        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewLiens/JVTNNoticeRequest/NoticeRequest/" + ViewName + ".cshtml";
            return path;
        }
 
    }
}
