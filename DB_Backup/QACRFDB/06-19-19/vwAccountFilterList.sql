
CREATE VIEW [dbo].[vwAccountFilterList]
AS
SELECT        dbo.DebtAccount.DebtAccountId, dbo.Client.ClientCode, dbo.DebtAccount.ClientId, dbo.DebtAccount.LocationId, dbo.DebtAccount.CollectionStatusId, dbo.DebtAccount.Location, dbo.DebtAccount.CollectionStatus, 
                         dbo.DebtAccount.TempOwnerId, dbo.DebtAccount.TempOwner, dbo.DebtAccount.Priority, dbo.DebtAccount.AccountNo, dbo.DebtAccount.ReferalDate, dbo.DebtAccount.NextContactDate, 
                         dbo.DebtAccount.NextStatusDate, dbo.DebtAccount.CloseDate, dbo.DebtAccount.TotBalAmt, dbo.DebtAddress.IsMainAddress, dbo.DebtAddress.ContactName, dbo.DebtAddress.AddressLine1, 
                         dbo.DebtAddress.City, dbo.DebtAddress.State, dbo.DebtAddress.PostalCode, dbo.DebtAddress.PhoneNo, dbo.DebtAddress.Fax, dbo.DebtAccountLegalInfo.AttyId, dbo.DebtAccountSmallClaims.TrialDate, 
                         dbo.DebtAccountSmallClaims.JudgmentDate, dbo.DebtAccountSmallClaims.CourtId, dbo.DebtAccountSmallClaims.ProcessServerId, dbo.DebtAccountSmallClaims.CaseNum, 
                         dbo.DebtAccountSmallClaims.SCReviewDate, dbo.DebtAccountMisc.DebtorBKName, dbo.DebtAccountMisc.BKCaseNo, dbo.DebtAccountSmallClaims.EntityName1, dbo.Debt.DebtorName, dbo.Debt.TaxId, 
                         dbo.DebtAccountLegalInfo.FwdJudgmentDate, dbo.DebtAccount.DebtId, dbo.DebtAddress.SSN, dbo.Debt.MatchingKey, dbo.DebtAddress.AddressKey, dbo.DebtAccount.BranchNum, dbo.DebtAccount.FileNumber, 
                         dbo.CollectionStatusGroup.StatusGroupDescription, dbo.CollectionStatusGroup.Id AS StatusGroupId, dbo.DebtAccountLegalInfo.AttyRef, dbo.DebtAccountLegalInfo.CourtMemo, 
                         dbo.DebtAccountLegalInfo.FwdCourtDate,
						 dbo.DebtAccount.LastPaymentDate,
						 dbo.DebtAccount.TotAsgAmt,
						 dbo.DebtAccount.TotCollFeeAmt,
						 dbo.DebtAccount.TotPmtAmt,
						 dbo.DebtAccount.TotIntAmt
FROM            dbo.CollectionStatusGroup RIGHT OUTER JOIN
                         dbo.CollectionStatus ON dbo.CollectionStatusGroup.Id = dbo.CollectionStatus.StatusGroup RIGHT OUTER JOIN
                         dbo.Client INNER JOIN
                         dbo.DebtAccount ON dbo.Client.ClientId = dbo.DebtAccount.ClientId INNER JOIN
                         dbo.Debt ON dbo.DebtAccount.DebtId = dbo.Debt.DebtId INNER JOIN
                         dbo.DebtAddress ON dbo.Debt.DebtId = dbo.DebtAddress.DebtId ON dbo.CollectionStatus.CollectionStatusId = dbo.DebtAccount.CollectionStatusId LEFT OUTER JOIN
                         dbo.DebtAccountLegalInfo ON dbo.DebtAccount.DebtAccountId = dbo.DebtAccountLegalInfo.DebtAccountId LEFT OUTER JOIN
                         dbo.DebtAccountMisc ON dbo.DebtAccount.DebtAccountId = dbo.DebtAccountMisc.DebtAccountId LEFT OUTER JOIN
                         dbo.DebtAccountSmallClaims ON dbo.DebtAccount.DebtAccountId = dbo.DebtAccountSmallClaims.DebtAccountId
WHERE        (dbo.DebtAddress.IsMainAddress = 1)