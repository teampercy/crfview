﻿using CRF.BLL.CRFDB.TABLES;
using CRFView.Adapters;
using CRFView.Adapters.Clients.Client_Management;
using CRFView.Adapters.Collections.Collectors.WorkCard;
using CRFView.Adapters.Collections.Collectors.WorkCard.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
 
namespace CRFView.Controllers.Collections.Collectors.WorkCard
{
    public class WorkCardController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            Session["FilterList"] = null;

            if (Session["Filters"] != null)
                Session["Filters"] = null;
            //List<ADvwAccountFilterList> lst = ADvwAccountFilterList.GetInitialList();
            string SortBy = "DebtAccountId DESC";

            List<ADvwAccountFilterList> lst = ADvwAccountFilterList.GetAccountList(null, SortBy);
            AccountFilterListVM ObjVm = new AccountFilterListVM();
            ObjVm.ObjAccounts = lst;
            return PartialView(GetVirtualPath("Index"), ObjVm);
            //return View(GetVirtualPath("Index"), ObjVm);
        }

        public ActionResult BackTolist()
        {
            string SortBy = "DebtAccountId DESC";
            AccountFilterListVM ObjVm = new AccountFilterListVM();
            if (Session["FilterList"] != null)
            {
      
                ObjVm = (AccountFilterListVM)Session["FilterList"];
            }
            else
            {
                List<ADvwAccountFilterList> lst = ADvwAccountFilterList.GetAccountList(null, SortBy);

                ObjVm.ObjAccounts = lst;

            }

            return PartialView(GetVirtualPath("Index"), ObjVm);
        }

        [HttpGet]
        public ActionResult ShowFilterModal()
        {
            FilterVM ObjVm = new FilterVM();

            ObjVm.FilterObj = new  Adapters.Collections.Collectors.WorkCard.Filter();

            if (Session["LocationId"] != null)
            {
                ObjVm.FilterObj.cboDesk = Convert.ToString(Session["LocationId"]);
            }
            if (Session["TempDeskId"] != null)
            {
                ObjVm.FilterObj.cboToDesk = Convert.ToString(Session["TempDeskId"]);
            }
          
            DateTime currentDate = DateTime.Now;

            if (Session["NextContactDateFrom"] != null)
            {
                ObjVm.FilterObj.NextContactDateFrom = Convert.ToDateTime(Session["NextContactDateFrom"]);
            }
            else
            {
                ObjVm.FilterObj.NextContactDateFrom = Convert.ToDateTime(currentDate.AddDays(-60));
            }

            if (Session["NextContactDateTo"] != null)
            {
                ObjVm.FilterObj.NextContactDateTo = Convert.ToDateTime(Session["NextContactDateTo"]);

            }
            else
            {
                ObjVm.FilterObj.NextContactDateTo = Convert.ToDateTime(currentDate);
            }
            if (Session["ReferalDateFrom"] != null)
            {
                ObjVm.FilterObj.ReferalDateFrom = Convert.ToDateTime(Session["ReferalDateFrom"]);
            }
            else
            {
                ObjVm.FilterObj.ReferalDateFrom = Convert.ToDateTime(currentDate);
            }
            if (Session["ReferalDateTo"] != null)
            {
                ObjVm.FilterObj.ReferalDateTo = Convert.ToDateTime(Session["ReferalDateTo"]);
            }
            else
            {
                ObjVm.FilterObj.ReferalDateTo = Convert.ToDateTime(currentDate);
            }
            if (Session["FWDJudgmentDateFrom"] != null)
            {
                ObjVm.FilterObj.FWDJudgmentDateFrom = Convert.ToDateTime(Session["FWDJudgmentDateFrom"]);
            }
            else
            {
                ObjVm.FilterObj.FWDJudgmentDateFrom = Convert.ToDateTime(currentDate);
            }
            if (Session["FWDJudgmentDateTo"] != null)
            {
                ObjVm.FilterObj.FWDJudgmentDateTo = Convert.ToDateTime(Session["FWDJudgmentDateTo"]);
            }
            else
            {
                ObjVm.FilterObj.FWDJudgmentDateTo = Convert.ToDateTime(currentDate.AddDays(30));
            }
            if (Session["SCReviewDateFrom"] != null)
            {
                ObjVm.FilterObj.SCReviewDateFrom = Convert.ToDateTime(Session["SCReviewDateFrom"]);
            }
            else
            {
                ObjVm.FilterObj.SCReviewDateFrom = Convert.ToDateTime(currentDate.AddDays(-60));
            }
            if (Session["SCReviewDateTo"] != null)
            {
                ObjVm.FilterObj.SCReviewDateTo = Convert.ToDateTime(Session["SCReviewDateTo"]);
            }
            else
            {
                ObjVm.FilterObj.SCReviewDateTo = Convert.ToDateTime(currentDate);
            }
            if (Session["SCJudgmentDateFrom"] != null)
            {
                ObjVm.FilterObj.SCJudgmentDateFrom = Convert.ToDateTime(Session["SCJudgmentDateFrom"]);
            }
            else
            {
                ObjVm.FilterObj.SCJudgmentDateFrom = Convert.ToDateTime(currentDate);

            }
            if (Session["SCJudgmentDateTo"] != null)
            {
                ObjVm.FilterObj.SCJudgmentDateTo = Convert.ToDateTime(Session["SCJudgmentDateTo"]);
            }
            else
            {
                ObjVm.FilterObj.SCJudgmentDateTo = Convert.ToDateTime(currentDate.AddDays(30));
            }

            if (Session["TrialDateFrom"] != null)
            {
                ObjVm.FilterObj.NextTrialDateFrom = Convert.ToDateTime(Session["TrialDateFrom"]);
            }
            else
            {
                ObjVm.FilterObj.NextTrialDateFrom = Convert.ToDateTime(currentDate);
            }
            if (Session["TrialDateTo"] != null)
            {
                ObjVm.FilterObj.NextTrialDateTo = Convert.ToDateTime(Session["TrialDateTo"]);
            }
            else
            {
                ObjVm.FilterObj.NextTrialDateTo = Convert.ToDateTime(currentDate);
            }
            if (Session["FWDCourtDateFrom"] != null)
            {
                ObjVm.FilterObj.FWDCourtDateFrom = Convert.ToDateTime(Session["FWDCourtDateFrom"]);
            }
            else
            {
                ObjVm.FilterObj.FWDCourtDateFrom = Convert.ToDateTime(currentDate);
            }
            if (Session["FWDCourtDateTo"] != null)
            {
                ObjVm.FilterObj.FWDCourtDateTo = Convert.ToDateTime(Session["FWDCourtDateTo"]);
            }
            else
            {
                ObjVm.FilterObj.FWDCourtDateTo = Convert.ToDateTime(currentDate);
            }

            return PartialView(GetVirtualPath("_FilterNew"), ObjVm);
        }

        [HttpPost]
        public ActionResult GetFilterResult(Adapters.Collections.Collectors.WorkCard.Filter FilterObj)
        {
            Session["Filters"] = FilterObj;
            string SortBy = "DebtAccountId DESC";
            //List<ADvwAccountFilterList> lst = ADvwAccountFilterList.GetFilteredList(FilterObj);
            List<ADvwAccountFilterList> lst = ADvwAccountFilterList.GetAccountList(FilterObj, SortBy);

            AccountFilterListVM ObjVm = new AccountFilterListVM();
            ObjVm.ObjAccounts = lst;
           
            FilterVM objFilterVM = new FilterVM();
            ObjVm.listCount = lst.Count();

            Session["FilterList"] = ObjVm;

            Session["LocationId"] = FilterObj.cboDesk;
            Session["TempDeskId"] = FilterObj.cboToDesk;
            Session["NextContactDateFrom"] = FilterObj.NextContactDateFrom;
            Session["NextContactDateTo"] = FilterObj.NextContactDateTo;
            Session["ReferalDateFrom"] = FilterObj.ReferalDateFrom;
            Session["ReferalDateTo"] = FilterObj.ReferalDateTo;
            Session["FWDJudgmentDateFrom"] = FilterObj.FWDJudgmentDateFrom;
            Session["FWDJudgmentDateTo"] = FilterObj.FWDJudgmentDateTo;
            Session["SCReviewDateFrom"] = FilterObj.SCReviewDateFrom;
            Session["SCReviewDateTo"] = FilterObj.SCReviewDateTo;
            Session["SCJudgmentDateFrom"] = FilterObj.SCJudgmentDateFrom;
            Session["SCJudgmentDateTo"] = FilterObj.SCJudgmentDateTo;
            Session["TrialDateFrom"] = FilterObj.NextTrialDateFrom;
            Session["TrialDateTo"] = FilterObj.NextTrialDateTo;
            Session["FWDCourtDateFrom"] = FilterObj.FWDCourtDateFrom;
            Session["FWDCourtDateTo"] = FilterObj.FWDCourtDateTo;

            return PartialView(GetVirtualPath("_AccountListNew"), ObjVm);
        }


        [HttpPost]
        public ActionResult Refresh()
        {
            string SortBy = "DebtAccountId DESC";
             List<ADvwAccountFilterList> lst = ADvwAccountFilterList.GetAccountList(null, SortBy);
            AccountFilterListVM ObjVm = new AccountFilterListVM();
            ObjVm.ObjAccounts = lst;
            return PartialView(GetVirtualPath("_AccountListNew"), ObjVm);
        }

        [HttpPost]
        public ActionResult LoadDebtAccount(string DebtAccountId)
        {

            DebtAccountVM obj;

            //Session["NextValue"] = NextValue;
            //Session["PreviousValue"] = PreviousValue; 

            obj = AccountInfo.LoadInfo(DebtAccountId);

            if (obj == null)
            {
                return null;
            }

            obj.obj_DebtAccount.ClientFax = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(obj.obj_DebtAccount.ClientFax);
            obj.obj_DebtAccount.ClientPhone = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(obj.obj_DebtAccount.ClientPhone);

            List<ADvwAccountFilterList> lst = ADvwAccountFilterList.GetMatchedList(DebtAccountId);

            


            if (lst.Count < 1)
            {
                ViewBag.btnMatch = "No Matches"; //btnMatch button is disabled 
            }
            else
            {
                ViewBag.btnMatch = "Matches (" + lst.Count + ")"; //btnMatch button is enabled 
            }

            //Credit Report
            if (obj.obj_DebtAccount.IsNoCBR == true)
            {
                ViewBag.btnAddIndividual = "Disabled Add Individual"; // btnAddIndividual button is disabled
                ViewBag.btnAddCommercial = "Disabled Add Commercial"; // btnAddCommercial button is disabled
            }
            else
            {
                ViewBag.btnAddIndividual = "Enable Add Individual"; // btnAddIndividual button is enabled
                ViewBag.btnAddCommercial = "Enable Add Commercial"; // btnAddCommercial button is enabled
            }

            DataTable dtCommercialCreditReportList = ADvwAccountFilterList.GetCommercialCreditReportList(DebtAccountId);
            if (obj.obj_DebtAccount.IsNoCBR == false)
            {
                if (dtCommercialCreditReportList.Rows.Count > 0)
                {
                    ViewBag.btnAddCommercial = "Disabled Add Commercial"; // btnAddCommercial button is disabled
                }
                else
                {
                    ViewBag.btnAddCommercial = "Enable Add Commercial"; // btnAddCommercial button is enabled
                }
            }


            //Misc Info
            DataTable dtDebtAccountMiscList = ADDebtAccountMisc.GetDebtAccountMiscList(DebtAccountId);
            if (dtDebtAccountMiscList.Rows.Count > 0)
            {
                ViewBag.btnNewMiscInfo = "Hide Add MiscInfo"; // btnNewMiscInfo button is Hide
                ViewBag.divMiscInfoDetails = "Show Div MiscInfoDetails"; // divMiscInfoDetails div is Show
            }
            else
            {
                ViewBag.btnNewMiscInfo = "Show Add MiscInfo"; // btnNewMiscInfo button is Show
                ViewBag.divMiscInfoDetails = "Hide Div MiscInfoDetails"; // divMiscInfoDetails div is Hide
            }

            //Forwarding
            DataTable dtDebtAccountLegalInfoList = ADDebtAccountLegalInfo.GetDebtAccountLegalInfoList(DebtAccountId);
            if (dtDebtAccountLegalInfoList.Rows.Count > 0)
            {
                ViewBag.btnNewLegalInfo = "Hide Add LegalInfo"; // btnNewLegalInfo button is Hide
                ViewBag.divLegalInfoDetails = "Show Div LegalInfoDetails"; // divLegalInfoDetails div is Show
            }
            else
            {
                ViewBag.btnNewLegalInfo = "Show Add SmallClaims"; // btnNewLegalInfo button is Show
                ViewBag.divLegalInfoDetails = "Hide Div LegalInfoDetails"; // divLegalInfoDetails div is Hide 
            }

            //Small Claims
            DataTable dtDebtAccountSmallClaimsList = ADDebtAccountSmallClaims.GetDebtAccountSmallClaimsList(DebtAccountId);
            if (dtDebtAccountSmallClaimsList.Rows.Count > 0)
            {
                ViewBag.btnNewSmallClaims = "Hide Add SmallClaims"; // btnNewSmallClaims button is Hide
                ViewBag.divSmallClaimsDetails = "Show Div SmallClaimsDetails"; // divSmallClaimsDetails div is Show

                //If Common.Mapper.GetDate(Me.JudgmentDate.Value) > "01/01/1980" Or Me.JudgmentAmt.Value > 0 Then
                //    CalculateSCJudgmentTotals()
                //    Me.SuitAmt.Value = Me.SCTotalDue.Value
                //Else
                //    Me.Interest.Value = 0
                //    Me.SCTotalDue.Value = 0
                //End If

                if (obj.objDebtAccountSmallClaims.JudgmentDate > DateTime.MinValue || obj.objDebtAccountSmallClaims.JudgmentAmt > 0)
                {
                    CalculateSCJudgmentTotals(obj.objDebtAccountSmallClaims);
                }

                objSmallClaimsVM.ObjDebtAccountSCCostsSpentDt = ADDebtAccountSmallClaims.GetDebtAccountSCCostsSpentDt(DebtAccountId);

                if (objSmallClaimsVM.ObjDebtAccountSCCostsSpentDt.Rows.Count > 0)
                {
                    obj.objDebtAccountSmallClaims.CostsSpent = Convert.ToDecimal(objSmallClaimsVM.ObjDebtAccountSCCostsSpentDt.Rows[0]["CostsSpent"]);
                }
                else
                {
                    obj.objDebtAccountSmallClaims.CostsSpent = 0;
                }

            }
            else
            {
                ViewBag.btnNewSmallClaims = "Show Add SmallClaimsDetails"; // btnNewSmallClaims button is Show
                ViewBag.divSmallClaimsDetails = "Hide Div SmallClaimsDetails"; // divSmallClaimsDetails div is Hide 
            }

            //obj.NxtValue = NextValue;
            //obj.PreValue = PreviousValue;

            return PartialView(GetVirtualPath("_LoadAccount"), obj);
        }

        [HttpPost]
        public ActionResult LoadDebtAccountByOrder(string Id)
        {
            //string Id = " ";
            DebtAccountVM obj; 
           // DataTable dt = ADvwAccountFilterList.GetMatchedListByOrder(DebtAccountId);
            //if(dt == null)
            //{
            //    return PartialView(GetVirtualPath("_LoadAccount"), "hi");
            //}

           // Id = ((dt.Rows[0]["DebtAccountId"] != DBNull.Value ? dt.Rows[0]["DebtAccountId"].ToString() : "")); 
            obj = AccountInfo.LoadInfo(Id);
            List<ADvwAccountFilterList> lst = ADvwAccountFilterList.GetMatchedList(Id);

            


            if (lst.Count < 1)
            {
                ViewBag.btnMatch = "No Matches"; //btnMatch button is disabled 
            }
            else
            {
                ViewBag.btnMatch = "Matches (" + lst.Count + ")"; //btnMatch button is enabled 
            }

            //Credit Report
            if (obj.obj_DebtAccount.IsNoCBR == true)
            {
                ViewBag.btnAddIndividual = "Disabled Add Individual"; // btnAddIndividual button is disabled
                ViewBag.btnAddCommercial = "Disabled Add Commercial"; // btnAddCommercial button is disabled
            }
            else
            {
                ViewBag.btnAddIndividual = "Enable Add Individual"; // btnAddIndividual button is enabled
                ViewBag.btnAddCommercial = "Enable Add Commercial"; // btnAddCommercial button is enabled
            }

            DataTable dtCommercialCreditReportList = ADvwAccountFilterList.GetCommercialCreditReportList(Id);
            if (obj.obj_DebtAccount.IsNoCBR == false)
            {
                if (dtCommercialCreditReportList.Rows.Count > 0)
                {
                    ViewBag.btnAddCommercial = "Disabled Add Commercial"; // btnAddCommercial button is disabled
                }
                else
                {
                    ViewBag.btnAddCommercial = "Enable Add Commercial"; // btnAddCommercial button is enabled
                }
            }


            //Misc Info
            DataTable dtDebtAccountMiscList = ADDebtAccountMisc.GetDebtAccountMiscList(Id);
            if (dtDebtAccountMiscList.Rows.Count > 0)
            {
                ViewBag.btnNewMiscInfo = "Hide Add MiscInfo"; // btnNewMiscInfo button is Hide
                ViewBag.divMiscInfoDetails = "Show Div MiscInfoDetails"; // divMiscInfoDetails div is Show
            }
            else
            {
                ViewBag.btnNewMiscInfo = "Show Add MiscInfo"; // btnNewMiscInfo button is Show
                ViewBag.divMiscInfoDetails = "Hide Div MiscInfoDetails"; // divMiscInfoDetails div is Hide
            }

            //Forwarding
            DataTable dtDebtAccountLegalInfoList = ADDebtAccountLegalInfo.GetDebtAccountLegalInfoList(Id);
            if (dtDebtAccountLegalInfoList.Rows.Count > 0)
            {
                ViewBag.btnNewLegalInfo = "Hide Add LegalInfo"; // btnNewLegalInfo button is Hide
                ViewBag.divLegalInfoDetails = "Show Div LegalInfoDetails"; // divLegalInfoDetails div is Show
            }
            else
            {
                ViewBag.btnNewLegalInfo = "Show Add SmallClaims"; // btnNewLegalInfo button is Show
                ViewBag.divLegalInfoDetails = "Hide Div LegalInfoDetails"; // divLegalInfoDetails div is Hide 
            }

            //Small Claims
            DataTable dtDebtAccountSmallClaimsList = ADDebtAccountSmallClaims.GetDebtAccountSmallClaimsList(Id);
            if (dtDebtAccountSmallClaimsList.Rows.Count > 0)
            {
                ViewBag.btnNewSmallClaims = "Hide Add SmallClaims"; // btnNewSmallClaims button is Hide
                ViewBag.divSmallClaimsDetails = "Show Div SmallClaimsDetails"; // divSmallClaimsDetails div is Show

                //If Common.Mapper.GetDate(Me.JudgmentDate.Value) > "01/01/1980" Or Me.JudgmentAmt.Value > 0 Then
                //    CalculateSCJudgmentTotals()
                //    Me.SuitAmt.Value = Me.SCTotalDue.Value
                //Else
                //    Me.Interest.Value = 0
                //    Me.SCTotalDue.Value = 0
                //End If

                if (obj.objDebtAccountSmallClaims.JudgmentDate > DateTime.MinValue || obj.objDebtAccountSmallClaims.JudgmentAmt > 0)
                {
                    CalculateSCJudgmentTotals(obj.objDebtAccountSmallClaims);
                }

                objSmallClaimsVM.ObjDebtAccountSCCostsSpentDt = ADDebtAccountSmallClaims.GetDebtAccountSCCostsSpentDt(Id);

                if (objSmallClaimsVM.ObjDebtAccountSCCostsSpentDt.Rows.Count > 0)
                {
                    obj.objDebtAccountSmallClaims.CostsSpent = Convert.ToDecimal(objSmallClaimsVM.ObjDebtAccountSCCostsSpentDt.Rows[0]["CostsSpent"]);
                }
                else
                {
                    obj.objDebtAccountSmallClaims.CostsSpent = 0;
                }

            }
            else
            {
                ViewBag.btnNewSmallClaims = "Show Add SmallClaimsDetails"; // btnNewSmallClaims button is Show
                ViewBag.divSmallClaimsDetails = "Hide Div SmallClaimsDetails"; // divSmallClaimsDetails div is Hide 
            }

            return PartialView(GetVirtualPath("_LoadAccount"), obj);
        }

        [HttpPost]
        public ActionResult GetAddressDetails(string DebtAddressId)
        {
            AddressVM obj = new AddressVM();
            obj.ObjAddress = ADDebtAddress.ReadAddress(Convert.ToInt32(DebtAddressId));
            obj.obj_addressId = Convert.ToInt32(DebtAddressId);
            obj.obj_addressDebtId = obj.ObjAddress.DebtId;
            obj.obj_addressTypeId = obj.ObjAddress.DebtAddressTypeId;
            return PartialView(GetVirtualPath("_EditAddress"), obj);
        }

        [HttpPost]
        public ActionResult AddAddress(string DebtAccountId)
        {
            AddressVM objAddressVM = new AddressVM();
            objAddressVM.ObjAddress = ADDebtAddress.ReadAddressByDebtAccountId(DebtAccountId);

            if (objAddressVM.ObjAddress == null)
            {
                objAddressVM.ObjAddress.DebtAddressTypeId = 1;
            }
            else
            {
                objAddressVM.ObjAddress.DebtAddressTypeId += 1;
                ClearAddressDetails(objAddressVM.ObjAddress);
            }
            objAddressVM.obj_addressDebtId = Convert.ToInt32(DebtAccountId);
            objAddressVM.obj_addressTypeId = objAddressVM.ObjAddress.DebtAddressTypeId;
            return PartialView(GetVirtualPath("_AddNewAddress"), objAddressVM);
        }

        public void ClearAddressDetails(ADDebtAddress objAddress)
        {
            objAddress.ContactName = string.Empty;
            objAddress.AddressLine1 = string.Empty;
            objAddress.AddressLine2 = string.Empty;
            objAddress.City = string.Empty;
            objAddress.State = string.Empty;
            objAddress.PostalCode = string.Empty;
            objAddress.PhoneNo = string.Empty;
            objAddress.PhoneNoExt = string.Empty;
            objAddress.PhoneNo2 = string.Empty;
            objAddress.Fax = string.Empty;
            objAddress.CellPhone = string.Empty;
            objAddress.Email = string.Empty;
            objAddress.SSN = string.Empty;
            objAddress.Note = string.Empty;
        }

        //Save Address Details
        [HttpPost]
        public ActionResult SaveAddress(AddressVM Obj_AddressVM)
        {
            int result = 0;

            if (Obj_AddressVM.ObjAddress.DebtId == 0)
            {
                Obj_AddressVM.ObjAddress.DebtId = Obj_AddressVM.obj_addressDebtId;
            }
            if (Obj_AddressVM.ObjAddress.DebtAddressTypeId == 0)
            {
                Obj_AddressVM.ObjAddress.DebtAddressTypeId = Obj_AddressVM.obj_addressTypeId;
            }
            if (Obj_AddressVM.ObjAddress.DebtAddressId == 0)
            {
                Obj_AddressVM.ObjAddress.DebtAddressId = Obj_AddressVM.obj_addressId;
            }
            result = ADDebtAddress.SaveDebtAccountAddress(Obj_AddressVM.ObjAddress);

            return Json(result);
        }

        //Get Note Details
        [HttpPost]
        public ActionResult GetNoteDetails(string Id)
        {
            NoteVM obj = new NoteVM();
            obj.obj_note = ADDebtAccountNote.ReadNote(Convert.ToInt32(Id));
            return PartialView(GetVirtualPath("_EditNote"), obj);
        }

        [HttpPost]
        public ActionResult AddNewNoteDetails(int DebtAccountId)
        {
            NoteVM obj = new NoteVM();
            //obj.obj_note.DebtAccountNoteId =  -1;
            obj.obj_noteDebtAccountId = DebtAccountId;
            return PartialView(GetVirtualPath("_AddNewNote"), obj);
        }

        //Save Note Details
        [HttpPost]
        public ActionResult SaveNote(NoteVM Obj_NoteVM)
        {

            int result = 0;
            try
            {
                if (Obj_NoteVM.obj_note.DebtAccountId == 0)
                {
                    Obj_NoteVM.obj_note.DebtAccountId = Obj_NoteVM.obj_noteDebtAccountId;
                }
                result = ADDebtAccountNote.SaveDebtAccountNote(Obj_NoteVM.obj_note);
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return Json(result);
        }

        //Delete Note Details
        [HttpPost]
        public ActionResult DeleteNote(string DebtAccountNoteId)
        {
            bool result = ADDebtAccountNote.DeleteDebtAccountNote(DebtAccountNoteId);
            if (result)
            {
                return Json(result);
            }
            else
            {
                return Json(0);
            }
        }

        //Read Edit Account Info
        [HttpPost]
        public ActionResult EditAccount(string DebtAccountId)
        {
            EditDebtAccountVM objDebtAccount = new EditDebtAccountVM();
            objDebtAccount.obj_DebtAccountEdit = ADDebtAccount.ReadDebtAccount(Convert.ToInt32(DebtAccountId));
            objDebtAccount.obj_DebtEdit = ADDebt.ReadDebt(objDebtAccount.obj_DebtAccountEdit.DebtId);
            objDebtAccount.obj_DebtAddressEdit = ADDebtAddress.ReadAddressByDebtAccountId(DebtAccountId);
            return PartialView(GetVirtualPath("_EditAccount"), objDebtAccount);
        }

        //Update Edit Account Info
        [HttpPost]
        public ActionResult SaveAccountDetails(EditDebtAccountVM objUpdateDebtAccount)
        {

            CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
            if (AccountInfo.LogAccountChanges(objUpdateDebtAccount, CU_Obj))
            {
                ADDebtAccount.UpdateDebtAccountEditView(objUpdateDebtAccount.obj_DebtAccountEdit);
                ADDebt.UpdateDebtEditView(objUpdateDebtAccount.obj_DebtEdit, CU_Obj);
                ADDebtAddress.UpdateAddressEditView(objUpdateDebtAccount.obj_DebtAddressEdit);
            }
            DebtAccountVM obj = new DebtAccountVM();
            obj.obj_ADDebtAccount = ADDebtAccount.ReadDebtAccount(Convert.ToInt32(objUpdateDebtAccount.obj_DebtAccountEdit.DebtAccountId));
            obj = AccountInfo.LoadInfo(Convert.ToString(objUpdateDebtAccount.obj_DebtAccountEdit.DebtAccountId));

            return PartialView(GetVirtualPath("_LoadAccount"), obj);
        }

        //Get Sorted Result
        [HttpPost]
        public ActionResult GetSortedResult(string SortByValue, string SortTypeValue)
        {

            string SortType = "ASC";
            string SortBy = "DebtAccountId";
            if (SortTypeValue == "-1")
            {
                SortType = "ASC";
            }
            else if (SortTypeValue == "1")
            {
                SortType = "DESC";
            }

            if (SortByValue == "-1" || string.IsNullOrEmpty(SortByValue))
            {
                SortBy = "DebtAccountId " + SortType;
            }
            else if (SortByValue == "1")
            {
                SortBy = "DebtorName " + SortType;
            }
            else if (SortByValue == "2")
            {
                SortBy = "Contact " + SortType;
            }
            else if (SortByValue == "3")
            {
                SortBy = "DebtorAddress " + SortType;
            }
            else if (SortByValue == "4")
            {
                SortBy = "DebtorRef " + SortType;
            }
            else if (SortByValue == "5")
            {
                SortBy = "StatusCode " + SortType;
            }
            else if (SortByValue == "6")
            {
                SortBy = "Desk " + SortType;
            }
            else if (SortByValue == "7")
            {
                SortBy = "ToDesk " + SortType;
            }
            else if (SortByValue == "8")
            {
                SortBy = "DateAssigned " + SortType;
            }
            else if (SortByValue == "9")
            {
                SortBy = "ReviewDate " + SortType;
            }
            else if (SortByValue == "10")
            {
                SortBy = "LastPayment " + SortType;
            }
            else if (SortByValue == "11")
            {
                SortBy = "CloseDate " + SortType;
            }
            else if (SortByValue == "12")
            {
                SortBy = "AssignmentAmt " + SortType;
            }
            else if (SortByValue == "13")
            {
                SortBy = "CollectFees " + SortType;
            }
            else if (SortByValue == "14")
            {
                SortBy = "Interest " + SortType;
            }
            else if (SortByValue == "15")
            {
                SortBy = "Payments " + SortType;
            }
            else if (SortByValue == "16")
            {
                SortBy = "TotalOwned " + SortType;
            }
            else if (SortByValue == "17")
            {
                SortBy = "SCRevDate " + SortType;
            }
            else if (SortByValue == "18")
            {
                SortBy = "SCTrialDate " + SortType;
            }
            else if (SortByValue == "19")
            {
                SortBy = "SCJudgeDate " + SortType;
            }
            else if (SortByValue == "20")
            {
                SortBy = "FWDCourtDate " + SortType;
            }
            else if (SortByValue == "21")
            {
                SortBy = "FWDJudgeDate " + SortType;
            }
            else if (SortByValue == "22")
            {
                SortBy = "State " + SortType;
            }
            else if (SortByValue == "23")
            {
                SortBy = "ClientCode " + SortType;
            }

            //List<ADvwAccountFilterList> lst = ADvwAccountFilterList.GetSortedList(SortBy);
            //List<ADvwAccountFilterList> Filterlst = ADvwAccountFilterList.GetFilteredList(Filte);
            //AccountFilterListVM ObjVm = new AccountFilterListVM();
            //ObjVm.ObjAccounts = lst;
            //return PartialView(GetVirtualPath("_AccountListNew"), ObjVm);

            Adapters.Collections.Collectors.WorkCard.Filter objFilter = (Adapters.Collections.Collectors.WorkCard.Filter)Session["Filters"];
            
            List<ADvwAccountFilterList> lst = ADvwAccountFilterList.GetAccountList(objFilter, SortBy);
            AccountFilterListVM ObjVm = new AccountFilterListVM();
            ObjVm.ObjAccounts = lst;

            Session["FilterList"] = ObjVm;
            return PartialView(GetVirtualPath("_AccountListNew"), ObjVm);
        }

        [HttpPost]
        public ActionResult ShowPaymentCalculator(string DebtAccountId, string TotalDue)
        {
            PaymentCalculatorVM obj = new PaymentCalculatorVM();
            obj.FrequencyList = CommonBindList.GetFrequency();
            obj.TotalDue = Convert.ToDouble(TotalDue);
            obj.TotalDueP = Convert.ToDouble(TotalDue);
            obj.DebtAccountId = DebtAccountId;
            return PartialView(GetVirtualPath("_PaymentCalculator"), obj);
        }

        [HttpPost]
        public ActionResult CalculateSchedule(string DebtAccountId, double PaymentAmt, string Frequency, DateTime FirstPayDate, decimal TotalDue, bool IsNoInterest)
        {
            PaymentCalculatorVM obj = ADDebtAccount.GetPromiseByTimeFrame(DebtAccountId, PaymentAmt, Frequency, FirstPayDate, TotalDue, IsNoInterest);
            //obj.LastPmtDateString = obj.LastPmtDate.ToShortDateString();
            obj.LastPmtDateString = obj.LastPmtDate.ToString("MM/dd/yyyy");
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult CalculatePaymentAmount(string DebtAccountId, string FrequencyP, DateTime FirstPayDate, DateTime LastPayDate, decimal TotalDueP, bool IsNoInterest)
        {
            PaymentCalculatorVM obj = ADDebtAccount.CalculatePayment(DebtAccountId, FrequencyP, FirstPayDate, LastPayDate, TotalDueP, IsNoInterest);
            //obj.LastPmtDatePString = obj.LastPmtDateP.ToShortDateString();
            obj.LastPmtDatePString = obj.LastPmtDateP.ToString("MM/dd/yyyy");
            return Json(obj, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult ShowDBFaxAccountInfo(string DebtAccountId)
        {
            FaxAccountInfoVM obj = new FaxAccountInfoVM();
            obj.obj_Debt = ADDebt.ReadDebt(Convert.ToInt32(DebtAccountId));
            obj.DebtAccountId = Convert.ToInt32(DebtAccountId);
            obj.DBFaxContact = obj.obj_Debt.DebtorName;
            return PartialView(GetVirtualPath("_DBFaxAccountInfo"), obj);
        }

        [HttpPost]
        public ActionResult ShowDBFaxFromName(string DebtAccountId)
        {
            FaxFromNameVM obj = new FaxFromNameVM();
            obj.obj_DebtAccount = ADDebtAccount.ReadDebtAccount(Convert.ToInt32(DebtAccountId));
            obj.obj_Location = ADLocation.ReadLocation(obj.obj_DebtAccount.LocationId);
            obj.LocationDescr = obj.obj_Location.LocationDescr;
            return PartialView(GetVirtualPath("_DBFaxFromName"), obj);
        }

        [HttpPost]
        public ActionResult ShowCLFaxAccountInfo(string DebtAccountId)
        {
            FaxAccountInfoVM obj = new FaxAccountInfoVM();
            obj.obj_DebtAccount = ADDebtAccount.ReadDebtAccount(Convert.ToInt32(DebtAccountId));
            obj.obj_Client = ADClient.GetClientDetails(Convert.ToInt32(obj.obj_DebtAccount.ClientId));
            obj.DebtAccountId = Convert.ToInt32(DebtAccountId);
            obj.CLFaxContact = obj.obj_Client.ContactName;
            return PartialView(GetVirtualPath("_CLFaxAccountInfo"), obj);
        }

        [HttpPost]
        public ActionResult ShowCLFaxFromName(string DebtAccountId)
        {
            FaxFromNameVM obj = new FaxFromNameVM();
            obj.obj_DebtAccount = ADDebtAccount.ReadDebtAccount(Convert.ToInt32(DebtAccountId));
            obj.obj_Location = ADLocation.ReadLocation(obj.obj_DebtAccount.LocationId);
            obj.LocationDescr = obj.obj_Location.LocationDescr;
            return PartialView(GetVirtualPath("_CLFaxFromName"), obj);
        }

        //Print Fax
        [HttpPost]
        public string FaxPrint(string DebtAccountId, string FaxCode)
        {
            FaxFromNameVM obj = new FaxFromNameVM();
            obj.obj_DebtAccount = ADDebtAccount.ReadDebtAccount(Convert.ToInt32(DebtAccountId));
            obj.obj_Debt = ADDebt.ReadDebt(Convert.ToInt32(DebtAccountId));
            obj.obj_Client = ADClient.GetClientDetails(Convert.ToInt32(obj.obj_DebtAccount.ClientId));

            switch (FaxCode)
            {
                case "CL":
                    obj.FaxContactName = obj.obj_Client.ContactName;
                    break;

                case "DB":
                    obj.FaxContactName = obj.obj_Debt.DebtorName;
                    break;
            }

            obj.obj_Location = ADLocation.ReadLocation(obj.obj_DebtAccount.LocationId);
            obj.LocationDescr = obj.obj_Location.LocationDescr;

            string PdfUrl = obj.PrintFax(DebtAccountId, FaxCode, obj.FaxContactName, obj.LocationDescr);
            //PdfUrl = "D:/Projects/CRFS/CRFView/CRFView/CRFView/Output/Reports/LauraChung-1788-CollectionPaySchedule-20190713021618.PDF";

            if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
            {
                string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                return (path);
            }
            return "error";
        }

        //Show Client Info
        [HttpPost]
        public ActionResult ShowClientInfo(string ClientId)
        {
            ClientInfoVM obj = new ClientInfoVM();
            obj.obj_ClientCollectionInfo = ADClientCollectionInfo.ReadCollectionInfo(ClientId);
            return PartialView(GetVirtualPath("_ShowClientInfo"), obj);
        }

        //Read Edit More Info
        [HttpPost]
        public ActionResult EditMoreInfo(string DebtAccountId)
        {
            EditMoreInfoVM objMoreInfo = new EditMoreInfoVM();
            objMoreInfo.obj_DebtAccountMoreInfoEdit = ADDebtAccount.ReadDebtAccount(Convert.ToInt32(DebtAccountId));
            return PartialView(GetVirtualPath("_EditMoreInfo"), objMoreInfo);
        }

        //Update Edit More Info
        [HttpPost]
        public ActionResult SaveMoreInfoDetails(EditMoreInfoVM objUpdateDebtAccountMoreInfo)
        {
            if(string.IsNullOrEmpty(objUpdateDebtAccountMoreInfo.obj_DebtAccountMoreInfoEdit.Priority))
            {
                objUpdateDebtAccountMoreInfo.obj_DebtAccountMoreInfoEdit.Priority = "";
            }

            ADDebtAccount.UpdateDebtAccountMoreInfoEditView(objUpdateDebtAccountMoreInfo.obj_DebtAccountMoreInfoEdit);
            DebtAccountVM obj = new DebtAccountVM();
            obj.obj_ADDebtAccount = ADDebtAccount.ReadDebtAccount(Convert.ToInt32(objUpdateDebtAccountMoreInfo.obj_DebtAccountMoreInfoEdit.DebtAccountId));
            obj = AccountInfo.LoadInfo(Convert.ToString(objUpdateDebtAccountMoreInfo.obj_DebtAccountMoreInfoEdit.DebtAccountId));

            return PartialView(GetVirtualPath("_LoadAccount"), obj);
        }

        //Print  Payment Schedule
        [HttpPost]
        public string PrintPaymentSchedule(string DebtAccountId, double PaymentAmt, string Frequency, DateTime FirstPayDate, decimal TotalDue, bool IsNoInterest)
        {
            ADDebtAccount obj = new ADDebtAccount();
            string PdfUrl = obj.PrintPaymentSchedule(DebtAccountId, PaymentAmt, Frequency, FirstPayDate, TotalDue, IsNoInterest);
            if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
            {
                string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                return (path);
            }

            return "error";
        }

        //Print Payment Schedule Amount
        [HttpPost]
        public string PrintPaymentScheduleAmount(string DebtAccountId, string Frequency, DateTime FirstPayDate, DateTime LastPayDate, decimal TotalDue, bool IsNoInterest)
        {
            ADDebtAccount obj = new ADDebtAccount();
            string PdfUrl = obj.PrintPaymentScheduleAmount(DebtAccountId, Frequency, FirstPayDate, LastPayDate, TotalDue, IsNoInterest);
            if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
            {
                string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                return (path);
            }

            return "error";
        }

        //Get Match List
        [HttpPost]
        public ActionResult GetMatchList(string AccountId)
        {
            List<ADvwAccountFilterList> lst = ADvwAccountFilterList.GetMatchedList(AccountId);
            AccountFilterListVM ObjVm = new AccountFilterListVM();
            ObjVm.ObjAccounts = lst;
            return PartialView(GetVirtualPath("_AccountListNew"), ObjVm);
        }

        //Get Credit Card Auth Report
        [HttpPost]
        public string CreditCardAuthReport(string DebtAccountId)
        {
            ADDebtAccount obj = new ADDebtAccount();
            string PdfUrl = obj.PrintCreditCardAuthReport(DebtAccountId);
            if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
            {
                string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                return (path);
            }

            return "error";
        }

        //Show Action Codes
        [HttpPost]
        public ActionResult ShowActionCode(string DebtAccountId, string CollectionStatusId)
        {
            ActionHistoryVM objActionHistoryVM = new ActionHistoryVM();
            //ViewBag.DebtAccountId = DebtAccountId;
            //ViewBag.CollectionStatusId = CollectionStatusId;
            objActionHistoryVM.DebtAccountId = DebtAccountId;
            objActionHistoryVM.CollectionStatusId = CollectionStatusId;
            return PartialView(GetVirtualPath("_ActionCode"), objActionHistoryVM);
        }

        //Add Action Code
        [HttpPost]
        public ActionResult AddActionCode(string ActionCodesId, string DebtAccountId, string CollectionStatusId)
        {
            int result = -1;
            try
            {
                result = ADDebtAccount.ProcessActionCode(ActionCodesId, DebtAccountId, CollectionStatusId);
            }
            catch (Exception ex)
            {
                result = -1;
            }

            return Json(result);
        }

        //Get Action History Codes List by Acction Type
        public ActionResult GetActionHistoryCodes(string ActionTypeValue)
        {
            List<CommonBindList> ActionHistoryCodesList = new List<CommonBindList>();
            try
            {
                ActionHistoryCodesList = CommonBindList.GetActionHistoryCodesByActionType(ActionTypeValue);
            }
            catch (Exception ex)
            {

            }
            return Json(ActionHistoryCodesList, JsonRequestBehavior.AllowGet);
        }

        //Get Address details for Individual Credit Report
        [HttpPost]
        public ActionResult GetAddressDetailsByDebtAddressId(string DebtAddressId, string DebtAccountId)
        {
            DebtAccountVM objDebtAccountVM = AccountInfo.LoadInfo(DebtAccountId);

            CreditReportVM objCreditReportVM = new CreditReportVM(DebtAccountId);
            objCreditReportVM.ObjAddress = ADDebtAddress.ReadAddress(Convert.ToInt32(DebtAddressId));
            objCreditReportVM.DebtAccountId = DebtAccountId;
            objCreditReportVM.DebtAddressId = DebtAddressId;
            objCreditReportVM.ddlSelectNames = DebtAddressId;

            //objCreditReportVM.ObjDebtAccountIndividualCreditReport = new ADDebtAccountCreditReport();

            objCreditReportVM.ObjDebtAccountIndividualCreditReport.DebtAccountId = Convert.ToInt32(DebtAccountId);
            objCreditReportVM.ObjDebtAccountIndividualCreditReport.AddressLine1 = objCreditReportVM.ObjAddress.AddressLine1;
            objCreditReportVM.ObjDebtAccountIndividualCreditReport.AddressLine2 = objCreditReportVM.ObjAddress.AddressLine2;
            objCreditReportVM.ObjDebtAccountIndividualCreditReport.City = objCreditReportVM.ObjAddress.City;
            objCreditReportVM.ObjDebtAccountIndividualCreditReport.State = objCreditReportVM.ObjAddress.State;
            objCreditReportVM.ObjDebtAccountIndividualCreditReport.Zip = objCreditReportVM.ObjAddress.PostalCode;
            objCreditReportVM.ObjDebtAccountIndividualCreditReport.SSN = objCreditReportVM.ObjAddress.SSN;
            objCreditReportVM.DebtAddressTypeId = Convert.ToString(objCreditReportVM.ObjAddress.DebtAddressTypeId);
            objCreditReportVM.ObjDebtAccountIndividualCreditReport.DateOpened = objDebtAccountVM.obj_DebtAccount.ReferalDate;
            objCreditReportVM.ObjDebtAccountIndividualCreditReport.CurrentBalance = objDebtAccountVM.obj_DebtAccount.TotBalAmt;
            objCreditReportVM.ObjDebtAccountIndividualCreditReport.PastDueAmt = objDebtAccountVM.obj_DebtAccount.TotBalAmt;
            objCreditReportVM.ObjDebtAccountIndividualCreditReport.HighestCreditAmt = objDebtAccountVM.obj_DebtAccount.TotAsgAmt;
            objCreditReportVM.ObjDebtAccountIndividualCreditReport.LastPayDate = objDebtAccountVM.obj_DebtAccount.LastTrustDate;



            if (objDebtAccountVM.obj_DebtAccount.LastServiceDate > DateTime.MinValue)
            {
                objCreditReportVM.ObjDebtAccountIndividualCreditReport.BillingDate = objDebtAccountVM.obj_DebtAccount.LastServiceDate;
            }
            else
            {
                objCreditReportVM.ObjDebtAccountIndividualCreditReport.BillingDate = objDebtAccountVM.obj_DebtAccount.LastPaymentDate;
            }

            if (objCreditReportVM.ObjDebtAccountIndividualCreditReport.CurrentBalance == 0)
            {
                objCreditReportVM.ObjDebtAccountIndividualCreditReport.AccountStatus = "1";
            }
            else
            {
                objCreditReportVM.ObjDebtAccountIndividualCreditReport.AccountStatus = "2";
            }

            objCreditReportVM.ObjDebtAccountIndividualCreditReport.OriginalCreditor = objDebtAccountVM.obj_DebtAccount.ClientName;

            return PartialView(GetVirtualPath("_AddNewIndividual"), objCreditReportVM);
        }

        //Add New Individual Details
        [HttpPost]
        public ActionResult AddNewIndividual(string DebtAccountId)
        {
            CreditReportVM objCreditReportVM = new CreditReportVM(DebtAccountId);
            objCreditReportVM.DebtAccountId = DebtAccountId;
            //objCreditReportVM.ObjDebtAccountIndividualCreditReport = new ADDebtAccountCreditReport();
            //objCreditReportVM.SelectNamesList = CommonBindList.GetSelectNames(DebtAccountId);

            return PartialView(GetVirtualPath("_AddNewIndividual"), objCreditReportVM);
        }

        //Save Individual Details
        [HttpPost]
        public ActionResult SaveIndividual(CreditReportVM Obj_CreditReportVM)
        {

            int result = 0;
            DebtAccountVM objDebtAccountVM = AccountInfo.LoadInfo(Obj_CreditReportVM.DebtAccountId);

            Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport.DateOpened = objDebtAccountVM.obj_DebtAccount.ReferalDate;
            Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport.CurrentBalance = objDebtAccountVM.obj_DebtAccount.TotBalAmt;
            Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport.PastDueAmt = objDebtAccountVM.obj_DebtAccount.TotBalAmt;
            Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport.HighestCreditAmt = objDebtAccountVM.obj_DebtAccount.TotAsgAmt;
            Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport.LastPayDate = objDebtAccountVM.obj_DebtAccount.LastTrustDate;

            if (objDebtAccountVM.obj_DebtAccount.LastServiceDate > DateTime.MinValue)
            {
                Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport.BillingDate = objDebtAccountVM.obj_DebtAccount.LastServiceDate;
            }
            else
            {
                Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport.BillingDate = objDebtAccountVM.obj_DebtAccount.LastPaymentDate;
            }

            Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport.Id = Convert.ToInt32(Obj_CreditReportVM.Id);
            Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport.DebtAccountId = Convert.ToInt32(Obj_CreditReportVM.DebtAccountId);
            Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport.AddressId = Convert.ToInt32(Obj_CreditReportVM.ddlSelectNames);
            Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport.ECOACode = "1";
            Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport.PaymentRating = "G";
            string[] AccountStatus = Obj_CreditReportVM.AccountStatus.Split('-');
            Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport.AccountStatus = AccountStatus[0].Trim();

            Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport.DebtAddressTypeId = Convert.ToInt32(Obj_CreditReportVM.DebtAddressTypeId);
            if (string.IsNullOrEmpty(Obj_CreditReportVM.ddlAccountStatus))
            {
                Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport.AccountStatusId = 1;
            }
            else
            {
                Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport.AccountStatusId = Convert.ToInt32(Obj_CreditReportVM.ddlAccountStatus);
            }

            if (Obj_CreditReportVM.chkDispute == true)
            {
                if (Obj_CreditReportVM.RbtDisputeCode == "1")
                {
                    Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport.ComplianceCode = "XB";
                }
                else
                {
                    Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport.ComplianceCode = "XH";
                }
            }

            Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport.FirstDelinqDate = Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport.BillingDate;


            if (Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport.AccountStatusId == 1)
            {
                Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport.CloseDate = Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport.LastPayDate;
            }
            else
            {
                Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport.CloseDate = null;

            }

            Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport.ReportType = "I";

            result = ADDebtAccountCreditReport.SaveDebtAccountCreditReportIndividual(Obj_CreditReportVM.ObjDebtAccountIndividualCreditReport);

            return Json(result);
        }

        //Get Individual CreditReport Details
        [HttpPost]
        public ActionResult GetIndividualCreditReportDetails(string Id, string DebtAccountId)
        {
            CreditReportVM obj = new CreditReportVM(DebtAccountId);
            obj.DebtAccountId = DebtAccountId;
            obj.ObjDebtAccountIndividualCreditReport = ADDebtAccountCreditReport.ReadDebtAccountCreditReport(Convert.ToInt32(Id));
            obj.ddlSelectNames = Convert.ToString(obj.ObjDebtAccountIndividualCreditReport.AddressId);
            obj.Id = Id;
            obj.ddlAccountStatus = Convert.ToString(obj.ObjDebtAccountIndividualCreditReport.AccountStatusId);

            if (obj.ObjDebtAccountIndividualCreditReport.ComplianceCode == "XB")
            {
                obj.chkDispute = true;
                obj.RbtDisputeCode = "1";
            }
            else if (obj.ObjDebtAccountIndividualCreditReport.ComplianceCode == "XH")
            {
                obj.chkDispute = true;
                obj.RbtDisputeCode = "2";
            }

            return PartialView(GetVirtualPath("_EditIndividual"), obj);
        }

        //Delete Individual
        [HttpPost]
        public ActionResult DeleteIndividual(string Id)
        {
            var result = ADDebtAccountCreditReport.DeleteCreditReportIndividual(Id);
            return Json(result);
        }

        //Add New Commercial Details
        [HttpPost]
        public ActionResult AddNewCommercial(string DebtAccountId)
        {
            DebtAccountVM objDebtAccountVM = AccountInfo.LoadInfo(DebtAccountId);

            CreditReportVM objCreditReportVM = new CreditReportVM(DebtAccountId);
            objCreditReportVM.DebtAccountId = DebtAccountId;

            //objCreditReportVM.ObjDebtAccountCommercialCreditReport = new ADDebtAccountCreditReport();

            objCreditReportVM.ObjDebtAccountCommercialCreditReport.DebtAccountId = Convert.ToInt32(DebtAccountId);
            objCreditReportVM.ObjDebtAccountCommercialCreditReport.AddressId = Convert.ToInt32(objDebtAccountVM.obj_DebtAccount.DebtAddressId);
            objCreditReportVM.ObjDebtAccountCommercialCreditReport.CosignerName = objDebtAccountVM.obj_DebtAccount.DebtorName;
            objCreditReportVM.ObjDebtAccountCommercialCreditReport.CosignerAddressLine1 = objDebtAccountVM.obj_DebtAccount.AddressLine1;
            objCreditReportVM.ObjDebtAccountCommercialCreditReport.CosignerAddressLine2 = objDebtAccountVM.obj_DebtAccount.AddressLine2;
            objCreditReportVM.ObjDebtAccountCommercialCreditReport.CosignerCity = objDebtAccountVM.obj_DebtAccount.City;
            objCreditReportVM.ObjDebtAccountCommercialCreditReport.CosignerState = objDebtAccountVM.obj_DebtAccount.State;
            objCreditReportVM.ObjDebtAccountCommercialCreditReport.CosignerZip = objDebtAccountVM.obj_DebtAccount.PostalCode;
            objCreditReportVM.ObjDebtAccountCommercialCreditReport.CosignerSSN = objDebtAccountVM.obj_DebtAccount.TaxId;
            objCreditReportVM.DebtAddressTypeId = Convert.ToString(objDebtAccountVM.obj_DebtAccount.DebtAddressTypeId);
            objCreditReportVM.ObjDebtAccountCommercialCreditReport.BirthDate = objDebtAccountVM.obj_DebtAccount.BirthDate;
            objCreditReportVM.ObjDebtAccountCommercialCreditReport.DateOpened = objDebtAccountVM.obj_DebtAccount.ReferalDate;
            objCreditReportVM.ObjDebtAccountCommercialCreditReport.CurrentBalance = objDebtAccountVM.obj_DebtAccount.TotBalAmt;

            objCreditReportVM.ObjDebtAccountCommercialCreditReport.PastDueAmt = objDebtAccountVM.obj_DebtAccount.TotBalAmt;
            objCreditReportVM.ObjDebtAccountCommercialCreditReport.HighestCreditAmt = objDebtAccountVM.obj_DebtAccount.TotAsgAmt;
            objCreditReportVM.ObjDebtAccountCommercialCreditReport.LastPayDate = objDebtAccountVM.obj_DebtAccount.LastTrustDate;

            if (objDebtAccountVM.obj_DebtAccount.LastServiceDate > DateTime.MinValue)
            {
                objCreditReportVM.ObjDebtAccountCommercialCreditReport.BillingDate = objDebtAccountVM.obj_DebtAccount.LastServiceDate;
            }
            else
            {
                objCreditReportVM.ObjDebtAccountCommercialCreditReport.BillingDate = objDebtAccountVM.obj_DebtAccount.LastPaymentDate;
            }

            if (objCreditReportVM.ObjDebtAccountCommercialCreditReport.CurrentBalance == 0)
            {
                objCreditReportVM.ObjDebtAccountCommercialCreditReport.AccountStatus = "1";
            }
            else
            {
                objCreditReportVM.ObjDebtAccountCommercialCreditReport.AccountStatus = "2";
            }

            objCreditReportVM.ObjDebtAccountCommercialCreditReport.OriginalCreditor = objDebtAccountVM.obj_DebtAccount.ClientName;

            return PartialView(GetVirtualPath("_AddNewCommercial"), objCreditReportVM);
        }

        //Save Commercial Details
        [HttpPost]
        public ActionResult SaveCommercial(CreditReportVM Obj_CreditReportVM)
        {
            int result = 0;
            DebtAccountVM objDebtAccountVM = AccountInfo.LoadInfo(Obj_CreditReportVM.DebtAccountId);

            Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.DateOpened = objDebtAccountVM.obj_DebtAccount.ReferalDate;
            Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.CurrentBalance = objDebtAccountVM.obj_DebtAccount.TotBalAmt;
            Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.PastDueAmt = objDebtAccountVM.obj_DebtAccount.TotBalAmt;
            Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.HighestCreditAmt = objDebtAccountVM.obj_DebtAccount.TotAsgAmt;
            Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.LastPayDate = objDebtAccountVM.obj_DebtAccount.LastTrustDate;

            if (objDebtAccountVM.obj_DebtAccount.LastServiceDate > DateTime.MinValue)
            {
                Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.BillingDate = objDebtAccountVM.obj_DebtAccount.LastServiceDate;
            }
            else
            {
                Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.BillingDate = objDebtAccountVM.obj_DebtAccount.LastPaymentDate;
            }

            Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.Id = Convert.ToInt32(Obj_CreditReportVM.Id);
            Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.DebtAccountId = Convert.ToInt32(Obj_CreditReportVM.DebtAccountId);
            Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.ECOACode = "2";
            Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.CosignerECOACode = "W";
            Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.PaymentRating = "";

            string[] AccountStatus = Obj_CreditReportVM.AccountStatus.Split('-');
            Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.AccountStatus = AccountStatus[0].Trim();

            Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.DebtAddressTypeId = Convert.ToInt32(Obj_CreditReportVM.DebtAddressTypeId);
            if (string.IsNullOrEmpty(Obj_CreditReportVM.ddlAccountStatus))
            {
                Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.AccountStatusId = 1;
            }
            else
            {
                Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.AccountStatusId = Convert.ToInt32(Obj_CreditReportVM.ddlAccountStatus);
            }

            if (Obj_CreditReportVM.chkDispute == true)
            {
                if (Obj_CreditReportVM.RbtDisputeCode == "1")
                {
                    Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.ComplianceCode = "XB";
                }
                else
                {
                    Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.ComplianceCode = "XH";
                }
            }

            Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.FirstDelinqDate = Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.BillingDate;

            if (Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.AccountStatusId == 1)
            {
                Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.CloseDate = Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.LastPayDate;
            }
            else
            {
                Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.CloseDate = null;

            }

            Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.LastName = "None";
            Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.FirstName = "None";
            Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.MiddleName = "None";

            Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport.ReportType = "C";

            result = ADDebtAccountCreditReport.SaveDebtAccountCreditReportCommercial(Obj_CreditReportVM.ObjDebtAccountCommercialCreditReport);

            return Json(result);
        }

        //Get Commercial CreditReport Details
        [HttpPost]
        public ActionResult GetCommercialCreditReportDetails(string Id, string DebtAccountId)
        {
            CreditReportVM obj = new CreditReportVM(DebtAccountId);
            obj.DebtAccountId = DebtAccountId;
            obj.ObjDebtAccountCommercialCreditReport = ADDebtAccountCreditReport.ReadDebtAccountCreditReport(Convert.ToInt32(Id));

            obj.Id = Id;
            obj.ddlAccountStatus = Convert.ToString(obj.ObjDebtAccountCommercialCreditReport.AccountStatusId);

            if (obj.ObjDebtAccountCommercialCreditReport.ComplianceCode == "XB")
            {
                obj.chkDispute = true;
                obj.RbtDisputeCode = "1";
            }
            else if (obj.ObjDebtAccountCommercialCreditReport.ComplianceCode == "XH")
            {
                obj.chkDispute = true;
                obj.RbtDisputeCode = "2";
            }

            return PartialView(GetVirtualPath("_EditCommercial"), obj);
        }

        //Delete Commercial
        [HttpPost]
        public ActionResult DeleteCommercial(string Id)
        {
            var result = ADDebtAccountCreditReport.DeleteCreditReportCommercial(Id);
            return Json(result);
        }

        //Add New Document
        [HttpPost]
        public ActionResult AddDocument(string DebtAccountId)
        {
            DocumentVM objDocumentVM = new DocumentVM();
            objDocumentVM.DebtAccountId = DebtAccountId;
            return PartialView(GetVirtualPath("_AddNewDocument"), objDocumentVM);
        }

        //Save File Details
        [HttpPost]
        public ActionResult SaveDocument()
        {
            try
            {
                var file = Request.Files[0];
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/Output/UploadedFiles"), fileName);
                file.SaveAs(path);
                Session["path"] = path;
                return Json(true);
            }
            catch (Exception ex)
            {
                return Json(false);
            }
        }

        //Save Document Details
        [HttpPost]
        public ActionResult SaveDocumentInputs(DocumentVM Obj_DocumentVM)
        {
            int result = 0;
            try
            {
                string path = Convert.ToString(Session["path"]);

                if (!String.IsNullOrEmpty(path))
                {
                    byte[] bytes = System.IO.File.ReadAllBytes(path);
                    Obj_DocumentVM.ObjDebtAccountAttachment.DocumentImage = bytes;
                    Obj_DocumentVM.ObjDebtAccountAttachment.FileName = path.Substring(path.LastIndexOf("\\") + 1);
                }

                Obj_DocumentVM.ObjDebtAccountAttachment.DebtAccountId = Convert.ToInt32(Obj_DocumentVM.DebtAccountId);
                Obj_DocumentVM.ObjDebtAccountAttachment.DocumentType = Obj_DocumentVM.DocumentType;
                Obj_DocumentVM.ObjDebtAccountAttachment.BatchDebtAccountId = 0;
                Obj_DocumentVM.ObjDebtAccountAttachment.Id = Convert.ToInt32(Obj_DocumentVM.Id);

                result = ADDebtAccountAttachment.SaveDebtAccountAttachment(Obj_DocumentVM.ObjDebtAccountAttachment);
                Session["path"] = null;

                return Json(result);
            }
            catch
            {
                result = -1;
                return Json(result);
            }

        }

        //Update Document Details
        [HttpPost]
        public ActionResult UpdateDocumentInputs(DocumentVM Obj_DocumentVM)
        {
            int result = 0;
            try
            {
                string path = Convert.ToString(Session["path"]);

                if (!String.IsNullOrEmpty(path))
                {
                    byte[] bytes = System.IO.File.ReadAllBytes(path);
                    Obj_DocumentVM.ObjDebtAccountAttachmentEdit.DocumentImage = bytes;
                    Obj_DocumentVM.ObjDebtAccountAttachmentEdit.FileName = path.Substring(path.LastIndexOf("\\") + 1);
                }

                Obj_DocumentVM.ObjDebtAccountAttachmentEdit.DebtAccountId = Convert.ToInt32(Obj_DocumentVM.DebtAccountId);
                Obj_DocumentVM.ObjDebtAccountAttachmentEdit.DocumentType = Obj_DocumentVM.DocumentType;
                Obj_DocumentVM.ObjDebtAccountAttachmentEdit.BatchDebtAccountId = 0;
                Obj_DocumentVM.ObjDebtAccountAttachmentEdit.Id = Convert.ToInt32(Obj_DocumentVM.Id);

                result = ADDebtAccountAttachment.SaveDebtAccountAttachment(Obj_DocumentVM.ObjDebtAccountAttachmentEdit);
                Session["path"] = null;

                return Json(result);
            }
            catch
            {
                result = -1;
                return Json(result);
            }

        }

        //Get Document Details
        [HttpPost]
        public ActionResult GetDocumentDetails(string DebtDocumentId, string DebtAccountId)
        {
            DocumentVM obj = new DocumentVM();
            obj.ObjDebtAccountAttachmentEdit = ADDebtAccountAttachment.ReadDebtAccountAttachment(Convert.ToInt32(DebtDocumentId));
            obj.Id = Convert.ToString(DebtDocumentId);
            obj.DebtAccountId = Convert.ToString(obj.ObjDebtAccountAttachmentEdit.DebtAccountId);
            obj.ddlDocumentType = obj.ObjDebtAccountAttachmentEdit.DocumentType;
            return PartialView(GetVirtualPath("_EditDocument"), obj);
        }

        //Delete Document
        [HttpPost]
        public ActionResult DeleteDocument(string Id)
        {
            var result = ADDebtAccountAttachment.DeleteDocument(Id);
            return Json(result);
        }

        //View Document
        [HttpPost]
        public string ViewDocument(string Id)
        {
            var Attachment = ADDebtAccountAttachment.ReadDebtAccountAttachment(Convert.ToInt32(Id));
            var path = Path.Combine(Server.MapPath("~/Output/DownloadableFiles"), Attachment.FileName);
            ADDebtAccountAttachment.SaveFileToBeViewed(Attachment.DocumentImage, path);
            byte[] fileBytes = Attachment.DocumentImage;
            string fileName = path.Substring(path.LastIndexOf("\\") + 1);
            //fileName = HttpUtility.UrlEncode(fileName);
            //fileName = HttpUtility.UrlPathEncode(fileName); //Remove space with % 20
            fileName = Uri.EscapeDataString(fileName); //Remove # with %23 , space with %20

            if (path != null && path != "" && path != "Error")
            {
                //string filepath = Path.Combine(Url.Content("~"), "Output/DownloadableFiles/", System.IO.Path.GetFileName(path));
               string filepath = Path.Combine(Url.Content("~"), "Output/DownloadableFiles/", fileName);

                return (filepath);
            }
            return "error";
        }

        //Add Edit Misc Info
        [HttpPost]
        public ActionResult AddEditMiscInfo(string DebtAccountId, string PKID, string State)
        {
            MiscInfoVM objMiscInfoVM = new MiscInfoVM();
            objMiscInfoVM.DebtAccountId = DebtAccountId;
            objMiscInfoVM.PKID = PKID;
            objMiscInfoVM.State = State;
            objMiscInfoVM.objMiscEdit = ADDebtAccountMisc.ReadMiscInfoByDebtAccountId(DebtAccountId);

            return PartialView(GetVirtualPath("_AddEditMiscInfo"), objMiscInfoVM);
        }

        //Save Misc Info Details
        [HttpPost]
        public ActionResult SaveMiscInfo(MiscInfoVM Obj_MiscInfoVM)
        {
            int result = 0;
            try
            {
                Obj_MiscInfoVM.objMiscEdit.DebtAccountId = Convert.ToInt32(Obj_MiscInfoVM.DebtAccountId);
                Obj_MiscInfoVM.objMiscEdit.PKID = Convert.ToInt32(Obj_MiscInfoVM.PKID);
                result = ADDebtAccountMisc.SaveDebtAccountMisc(Obj_MiscInfoVM.objMiscEdit);
            }
            catch (Exception ex)
            {
                result = 0;
            }

            return Json(result);
        }

        LegalInfoVM objLegalInfoVM = new LegalInfoVM();
        //Add Edit Legal Info
        [HttpPost]
        public ActionResult AddEditLegalInfo(string DebtAccountId, string PKID, string LawPKId)
        {
            //LegalInfoVM objLegalInfoVM = new LegalInfoVM();
            objLegalInfoVM.DebtAccountId = DebtAccountId;
            objLegalInfoVM.PKID = PKID;
            objLegalInfoVM.LawPKId = LawPKId;
            objLegalInfoVM.objLegalInfoEdit = ADDebtAccountLegalInfo.ReadLegalInfoByDebtAccountId(DebtAccountId);

            if (objLegalInfoVM.objLegalInfoEdit.SuitFiledDate == DateTime.MinValue)
            {
                objLegalInfoVM.chkSuitFiled = false;
            }
            else
            {
                objLegalInfoVM.chkSuitFiled = true;
            }

            if (objLegalInfoVM.objLegalInfoEdit.FwdJudgmentDate == DateTime.MinValue)
            {
                objLegalInfoVM.chkJudgmentDate = false;
            }
            else
            {
                objLegalInfoVM.chkJudgmentDate = true;
            }

            if (objLegalInfoVM.objLegalInfoEdit.AttyId > 0)
            {
                objLegalInfoVM.ddlAttyNo = Convert.ToString(objLegalInfoVM.objLegalInfoEdit.AttyId);
            }
            
            if (objLegalInfoVM.objLegalInfoEdit.LawListId > 0)
            {
                objLegalInfoVM.ddlLawListCode = Convert.ToString(objLegalInfoVM.objLegalInfoEdit.LawListId);
            }

            objLegalInfoVM.objAttorneyInfoEdit = ADAttorneyInfo.ReadAttorneyInfo(objLegalInfoVM.objLegalInfoEdit.AttyId);
            objLegalInfoVM.objLawListInfoEdit = ADLawListInfo.ReadLawLists(objLegalInfoVM.objLegalInfoEdit.LawListId);


            DebtAccountVM objDebtAccountVM = AccountInfo.LoadInfo(objLegalInfoVM.DebtAccountId);
            if (objLegalInfoVM.objLegalInfoEdit.LegalAssignAmt == 0)
            {
                objLegalInfoVM.objLegalInfoEdit.LegalAssignAmt = objDebtAccountVM.obj_DebtAccount.TotAsgAmt
                                                                                              + objDebtAccountVM.obj_DebtAccount.TotAdjAmt
                                                                                              - objDebtAccountVM.obj_DebtAccount.TotPmtAmt;

                objLegalInfoVM.LegalAssignAmt = objLegalInfoVM.objLegalInfoEdit.LegalAssignAmt;
            }

            objLegalInfoVM.TotalCosts = objLegalInfoVM.objLegalInfoEdit.CourtCostAmt + objLegalInfoVM.objLegalInfoEdit.SuitFeeAmt;

            return PartialView(GetVirtualPath("_AddEditLegalInfo"), objLegalInfoVM);
        }

        //Get ddl details for Forwarding
        //[HttpPost]
        public ActionResult GetddlDetails(string DebtAccountId, string PKID, string AttyId, string AttyNo, string LawPKId, string LawListCode, string LegalAssignAmt, decimal CourtCostAmt, decimal SuitFeeAmt, decimal TotalCosts)
        {
            objLegalInfoVM.ddlAttyNo = AttyId;
            objLegalInfoVM.DebtAccountId = DebtAccountId;
            objLegalInfoVM.PKID = PKID;
            objLegalInfoVM.AttyNo = AttyNo;

            objLegalInfoVM.objAttorneyInfoEdit = ADAttorneyInfo.ReadAttorneyInfo(Convert.ToInt32(AttyId));

            //if(!string.IsNullOrEmpty(Convert.ToString(objLegalInfoVM.objAttorneyInfoEdit.LawListId)))
            //{
            //    objLegalInfoVM.ddlLawListCode = Convert.ToString(objLegalInfoVM.objAttorneyInfoEdit.LawListId);
            //}
            //else
            //{
            //    objLegalInfoVM.ddlLawListCode = LawPKId;
            //}

            if (objLegalInfoVM.objAttorneyInfoEdit.LawListId > 0)
            {
                objLegalInfoVM.ddlLawListCode = Convert.ToString(objLegalInfoVM.objAttorneyInfoEdit.LawListId);
            }
            else
            {
                objLegalInfoVM.ddlLawListCode = LawPKId;
            }


            objLegalInfoVM.objLawListInfoEdit = ADLawListInfo.ReadLawLists(Convert.ToInt32(objLegalInfoVM.ddlLawListCode));
            if (!string.IsNullOrEmpty(objLegalInfoVM.objLawListInfoEdit.LawListCode))
            {
                objLegalInfoVM.LawListCode = objLegalInfoVM.objLawListInfoEdit.LawListCode;
            }
            else
            {
                objLegalInfoVM.LawListCode = LawListCode;
            }

            objLegalInfoVM.objLegalInfoEdit = ADDebtAccountLegalInfo.ReadLegalInfoByDebtAccountId(DebtAccountId);

            if (objLegalInfoVM.objLegalInfoEdit.SuitFiledDate == DateTime.MinValue)
            {
                objLegalInfoVM.chkSuitFiled = false;
            }
            else
            {
                objLegalInfoVM.chkSuitFiled = true;
            }

            if (objLegalInfoVM.objLegalInfoEdit.FwdJudgmentDate == DateTime.MinValue)
            {
                objLegalInfoVM.chkJudgmentDate = false;
            }
            else
            {
                objLegalInfoVM.chkJudgmentDate = true;
            }

            if (objLegalInfoVM.objLegalInfoEdit.LegalAssignAmt == 0)
            {
                objLegalInfoVM.objLegalInfoEdit.LegalAssignAmt = Convert.ToDecimal(LegalAssignAmt);
                objLegalInfoVM.LegalAssignAmt = Convert.ToDecimal(LegalAssignAmt);
            }

            objLegalInfoVM.objLegalInfoEdit.AttyMemo = objLegalInfoVM.objAttorneyInfoEdit.AttyNote;

            objLegalInfoVM.objLegalInfoEdit.CourtCostAmt = CourtCostAmt;
            objLegalInfoVM.objLegalInfoEdit.SuitFeeAmt = SuitFeeAmt;
            objLegalInfoVM.TotalCosts = TotalCosts;

            return PartialView(GetVirtualPath("_AddEditLegalInfo"), objLegalInfoVM);
        }

        //Save Legal Info Details
        [HttpPost]
        public ActionResult SaveLegalInfo(LegalInfoVM Obj_LegalInfoVM)
        {
            int result = 0;
            try
            {
                Obj_LegalInfoVM.objLegalInfoEdit.DebtAccountId = Convert.ToInt32(Obj_LegalInfoVM.DebtAccountId);
                Obj_LegalInfoVM.objLegalInfoEdit.PKID = Convert.ToInt32(Obj_LegalInfoVM.PKID);
                Obj_LegalInfoVM.objLegalInfoEdit.AttyId = Convert.ToInt32(Obj_LegalInfoVM.ddlAttyNo);

                if (!string.IsNullOrEmpty(Obj_LegalInfoVM.AttyNo))
                {
                    string[] AttyNo = Obj_LegalInfoVM.AttyNo.Split('-');
                    Obj_LegalInfoVM.objLegalInfoEdit.AttyNO = AttyNo[0].Trim();
                }

                Obj_LegalInfoVM.objLegalInfoEdit.AttyMemo = Obj_LegalInfoVM.objAttorneyInfoEdit.AttyNote;

                Obj_LegalInfoVM.objLegalInfoEdit.LawListId = Convert.ToInt32(Obj_LegalInfoVM.ddlLawListCode);
                Obj_LegalInfoVM.objLegalInfoEdit.LawListCode = Obj_LegalInfoVM.LawListCode;

                if (Obj_LegalInfoVM.LawListCode == "--Select--")
                {
                    Obj_LegalInfoVM.objLegalInfoEdit.LawListCode = null;
                }

                if (Obj_LegalInfoVM.objLegalInfoEdit.LawListId > 0)
                {
                    objLegalInfoVM.objLawListInfoEdit = ADLawListInfo.ReadLawLists(Convert.ToInt32(Obj_LegalInfoVM.objLegalInfoEdit.LawListId));
                    Obj_LegalInfoVM.objLegalInfoEdit.LawListCode = objLegalInfoVM.objLawListInfoEdit.LawListCode;
                }

                if (Obj_LegalInfoVM.chkSuitFiled == false)
                {
                    Obj_LegalInfoVM.objLegalInfoEdit.SuitFiledDate = DateTime.MinValue;
                }

                if (Obj_LegalInfoVM.chkJudgmentDate == false)
                {
                    Obj_LegalInfoVM.objLegalInfoEdit.FwdJudgmentDate = DateTime.MinValue;
                }

                result = ADDebtAccountLegalInfo.SaveDebtAccountLegalInfo(Obj_LegalInfoVM.objLegalInfoEdit);
            }
            catch (Exception ex)
            {
                result = 0;
            }

            return Json(result);
        }

        //Add Edit Legal Info
        [HttpPost]
        public ActionResult AddEditCostsSpent(string DebtAccountId)
        {
            CostsSpentVM objCostsSpentVM = new CostsSpentVM(Convert.ToInt32(DebtAccountId));
            objCostsSpentVM.DebtAccountId = DebtAccountId;
            objCostsSpentVM.ObjDebtAccountLegalCostATTCDt = ADDebtAccountLegalCost.GetDebtAccountLegalCostATTCDtList(DebtAccountId);
            objCostsSpentVM.ObjDebtAccountLegalCostATTADt = ADDebtAccountLegalCost.GetDebtAccountLegalCostATTADtList(DebtAccountId);

            // Declare an object variable.
            object sumCLCC = null, sumCLSF = null, sumAGCC = null, sumAGSF = null;
            decimal sumTOTCL, sumTOTAG, sumTOTCOST;
            if (objCostsSpentVM.ObjDebtAccountLegalCostATTCDt.Rows.Count > 0)
            {
                sumCLCC = objCostsSpentVM.ObjDebtAccountLegalCostATTCDt.Compute("Sum(CourtCostAmt)", string.Empty);
                sumCLSF = objCostsSpentVM.ObjDebtAccountLegalCostATTCDt.Compute("Sum(SuitFeeAmt)", string.Empty);
            }
            if (objCostsSpentVM.ObjDebtAccountLegalCostATTADt.Rows.Count > 0)
            {
                sumAGCC = objCostsSpentVM.ObjDebtAccountLegalCostATTADt.Compute("Sum(CourtCostAmt)", string.Empty);
                sumAGSF = objCostsSpentVM.ObjDebtAccountLegalCostATTADt.Compute("Sum(SuitFeeAmt)", string.Empty);
            }

            objCostsSpentVM.CLCC = Convert.ToDecimal(sumCLCC);
            objCostsSpentVM.CLSF = Convert.ToDecimal(sumCLSF);

            objCostsSpentVM.AGCC = Convert.ToDecimal(sumAGCC);
            objCostsSpentVM.AGSF = Convert.ToDecimal(sumAGSF);

            sumTOTCL = Convert.ToDecimal(sumCLCC) + Convert.ToDecimal(sumCLSF);
            sumTOTAG = Convert.ToDecimal(sumAGCC) + Convert.ToDecimal(sumAGSF);

            sumTOTCOST = Convert.ToDecimal(sumCLCC) + Convert.ToDecimal(sumCLSF) + Convert.ToDecimal(sumAGCC) + Convert.ToDecimal(sumAGSF);

            objCostsSpentVM.TOTCL = sumTOTCL;
            objCostsSpentVM.TOTAG = sumTOTAG;
            objCostsSpentVM.TOTCOST = sumTOTCOST;

            return PartialView(GetVirtualPath("_AddEditCostsSpent"), objCostsSpentVM);
        }

        //Add new legal cost details
        [HttpPost]
        public ActionResult AddLegalCost(string DebtAccountId)
        {
            LegalCostVM obj = new LegalCostVM();
            obj.DebtAccountId = DebtAccountId;

            return PartialView(GetVirtualPath("_AddLegalCost"), obj);
        }

        //Save DebtAccount Legal Cost Details
        [HttpPost]
        public ActionResult SaveLegalCost(LegalCostVM Obj_LegalCostVM)
        {
            int result = 0;
            Obj_LegalCostVM.objDebtAccountLegalCost.DebtAccountId = Convert.ToInt32(Obj_LegalCostVM.DebtAccountId);
            Obj_LegalCostVM.objDebtAccountLegalCost.Id = Convert.ToInt32(Obj_LegalCostVM.Id);
            result = ADDebtAccountLegalCost.SaveDebtAccountLegalCost(Obj_LegalCostVM.objDebtAccountLegalCost);

            return Json(result);
        }

        //Update DebtAccount Legal Cost Details
        [HttpPost]
        public ActionResult UpdateLegalCost(LegalCostVM Obj_LegalCostVM)
        {
            int result = 0;
            Obj_LegalCostVM.objDebtAccountLegalCostEdit.DebtAccountId = Convert.ToInt32(Obj_LegalCostVM.DebtAccountId);
            Obj_LegalCostVM.objDebtAccountLegalCostEdit.Id = Convert.ToInt32(Obj_LegalCostVM.Id);
            result = ADDebtAccountLegalCost.SaveDebtAccountLegalCost(Obj_LegalCostVM.objDebtAccountLegalCostEdit);

            return Json(result);
        }

        //Get DebtAccount Legal Cost Details for Edit/Update
        [HttpPost]
        public ActionResult GetDebtAccountLegalCostDetails(string Id)
        {
            LegalCostVM obj = new LegalCostVM();
            obj.objDebtAccountLegalCostEdit = ADDebtAccountLegalCost.ReadDebtAccountLegalCost(Convert.ToInt32(Id));
            obj.DebtAccountId = Convert.ToString(obj.objDebtAccountLegalCostEdit.DebtAccountId);
            obj.Id = Id;
            return PartialView(GetVirtualPath("_EditLegalCost"), obj);
        }

        //Delete LegalCost
        [HttpPost]
        public ActionResult DeleteLegalCost(string Id)
        {
            var result = ADDebtAccountLegalCost.DeleteDebtAccountLegalCost(Id);
            return Json(result);
        }


        SmallClaimsVM objSmallClaimsVM = new SmallClaimsVM();
        //Add  Small Claims
        [HttpPost]
        public ActionResult AddSmallClaims(string DebtAccountId, string State, string DebtId)
        {
            //SmallClaimsVM objSmallClaimsVM = new SmallClaimsVM();
            objSmallClaimsVM.DebtAccountId = DebtAccountId;
            objSmallClaimsVM.State = State;
            objSmallClaimsVM.DebtId = DebtId;
            objSmallClaimsVM.CourtIdList = CommonBindList.GetCourtIdList(State);
            objSmallClaimsVM.ProcessServerIdList = CommonBindList.GetProcessServerIdList();

            return PartialView(GetVirtualPath("_AddSmallClaims"), objSmallClaimsVM);
        }

        //Save DebtAccount Small Claims Details
        [HttpPost]
        public ActionResult SaveSmallClaims(SmallClaimsVM Obj_SmallClaimsVM)
        {
            int result = 0;
            Obj_SmallClaimsVM.ObjDebtAccountSmallClaims.DebtAccountId = Convert.ToInt32(Obj_SmallClaimsVM.DebtAccountId);

            if (Convert.ToInt32(Obj_SmallClaimsVM.CourtId) > 0)
            {
                Obj_SmallClaimsVM.ObjCourtHouse = ADCourtHouse.ReadCourtHouse(Convert.ToInt32(Obj_SmallClaimsVM.CourtId));
                Obj_SmallClaimsVM.ObjDebtAccountSmallClaims.VenueNum = Obj_SmallClaimsVM.ObjCourtHouse.CourtId;
                Obj_SmallClaimsVM.ObjDebtAccountSmallClaims.CourtId = Convert.ToInt32(Obj_SmallClaimsVM.CourtId);
            }

            if (Convert.ToInt32(Obj_SmallClaimsVM.ProcessServerId) > 0)
            {
                Obj_SmallClaimsVM.ObjProcessServers = ADProcessServer.ReadProcessServers(Convert.ToInt32(Obj_SmallClaimsVM.ProcessServerId));
                Obj_SmallClaimsVM.ObjDebtAccountSmallClaims.ProcessServerNum = Obj_SmallClaimsVM.ObjProcessServers.ProcessServerNo;
                Obj_SmallClaimsVM.ObjDebtAccountSmallClaims.ProcessServerId = Convert.ToInt32(Obj_SmallClaimsVM.ProcessServerId);
            }

            Obj_SmallClaimsVM.ObjDebtAddressDt = ADDebtAddress.GetDebtAccountAddressDtList(Obj_SmallClaimsVM.DebtId);

            if (Obj_SmallClaimsVM.ObjDebtAddressDt.Rows.Count > 0)
            {
                foreach (DataRow row in Obj_SmallClaimsVM.ObjDebtAddressDt.Rows)
                {
                    if (Convert.ToInt32(row["DebtAddressTypeId"]) == 1)
                    {
                        Obj_SmallClaimsVM.ObjDebtAccountSmallClaims.ServAdd1 = Convert.ToString(row["AddressLine1"]);
                        Obj_SmallClaimsVM.ObjDebtAccountSmallClaims.ServCity1 = Convert.ToString(row["City"]);
                        Obj_SmallClaimsVM.ObjDebtAccountSmallClaims.ServSt1 = Convert.ToString(row["State"]);
                        Obj_SmallClaimsVM.ObjDebtAccountSmallClaims.ServZip1 = Convert.ToString(row["PostalCode"]);
                    }
                    if (Convert.ToInt32(row["DebtAddressTypeId"]) == 2)
                    {
                        Obj_SmallClaimsVM.ObjDebtAccountSmallClaims.ServAdd2 = Convert.ToString(row["AddressLine1"]);
                        Obj_SmallClaimsVM.ObjDebtAccountSmallClaims.ServCity2 = Convert.ToString(row["City"]);
                        Obj_SmallClaimsVM.ObjDebtAccountSmallClaims.ServSt2 = Convert.ToString(row["State"]);
                        Obj_SmallClaimsVM.ObjDebtAccountSmallClaims.ServZip2 = Convert.ToString(row["PostalCode"]);
                    }
                    if (Convert.ToInt32(row["DebtAddressTypeId"]) == 3)
                    {
                        Obj_SmallClaimsVM.ObjDebtAccountSmallClaims.ServAdd3 = Convert.ToString(row["AddressLine1"]);
                        Obj_SmallClaimsVM.ObjDebtAccountSmallClaims.ServCity3 = Convert.ToString(row["City"]);
                        Obj_SmallClaimsVM.ObjDebtAccountSmallClaims.ServSt3 = Convert.ToString(row["State"]);
                        Obj_SmallClaimsVM.ObjDebtAccountSmallClaims.ServZip3 = Convert.ToString(row["PostalCode"]);
                    }
                }
            }


            Obj_SmallClaimsVM.ObjDebtAccountSmallClaims.ClaimDescription = "Goods, Wares, Merchandise, Services Rendered and any applicable interest";

            result = ADDebtAccountSmallClaims.SaveDebtAccountSmallClaims_AccountInfo(Obj_SmallClaimsVM.ObjDebtAccountSmallClaims);

            return Json(result);
        }

        //Edit SmallClaims AccountInfo
        [HttpPost]
        public ActionResult EditSmallClaimsAccountInfo(string DebtAccountId, string PKID, string State)
        {
            //SmallClaimsVM objSmallClaimsVM = new SmallClaimsVM();
            objSmallClaimsVM.DebtAccountId = DebtAccountId;
            objSmallClaimsVM.PKID = PKID;
            objSmallClaimsVM.State = State;
            objSmallClaimsVM.CourtIdList = CommonBindList.GetCourtIdList(State);
            objSmallClaimsVM.ProcessServerIdList = CommonBindList.GetProcessServerIdList();

            objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit = ADDebtAccountSmallClaims.ReadDebtAccountSmallClaims(Convert.ToInt32(PKID));

            objSmallClaimsVM.ddlCourtId = Convert.ToString(objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.CourtId);
            objSmallClaimsVM.ObjCourtHouseEdit = ADCourtHouse.ReadCourtHouse(Convert.ToInt32(objSmallClaimsVM.ddlCourtId));

            objSmallClaimsVM.ddlProcessServerId = Convert.ToString(objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.ProcessServerId);
            objSmallClaimsVM.ObjProcessServersEdit = ADProcessServer.ReadProcessServers(Convert.ToInt32(objSmallClaimsVM.ddlProcessServerId));

            if (objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.TrialDate == DateTime.MinValue)
            {
                objSmallClaimsVM.chkTrialDate = false;
            }
            else
            {
                objSmallClaimsVM.chkTrialDate = true;
            }

            if (objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.SCSuitFiledDate == DateTime.MinValue)
            {
                objSmallClaimsVM.chkSuitFiled = false;
            }
            else
            {
                objSmallClaimsVM.chkSuitFiled = true;
            }

            var TrialTime = objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.TrialTime;
            if (!string.IsNullOrEmpty(TrialTime))
            {
                DateTime dtTrialTime = new DateTime(); // Creating the object of DateTime;
                dtTrialTime = Convert.ToDateTime(TrialTime); // now converting our temporary string into DateTime;              
                objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.TrialTime = dtTrialTime.ToString("HH:mm"); // 24 hrour format
            }
            else
            {
                objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.TrialTime = null;
            }

            return PartialView(GetVirtualPath("_EditSmallClaimsAccountInfo"), objSmallClaimsVM);
        }


        //Get ddl details for Samll Claims
        //[HttpPost]
        public ActionResult GetSCddlDetails(string DebtAccountId, string PKID, string ddlCourtId, string ddlProcessServerId, string State, string CaseNum, string ProcessServerRefNum, bool chkTrialDate, DateTime TrialDate, string TrialTime, bool chkSuitFiled, DateTime SCSuitFiledDate)
        {
            objSmallClaimsVM.ddlCourtId = ddlCourtId;
            objSmallClaimsVM.ddlProcessServerId = ddlProcessServerId;
            objSmallClaimsVM.State = State;
            objSmallClaimsVM.CourtIdList = CommonBindList.GetCourtIdList(State);
            objSmallClaimsVM.ProcessServerIdList = CommonBindList.GetProcessServerIdList();
            objSmallClaimsVM.DebtAccountId = DebtAccountId;
            objSmallClaimsVM.PKID = PKID;

            objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit = ADDebtAccountSmallClaims.ReadDebtAccountSmallClaims(Convert.ToInt32(PKID));
            objSmallClaimsVM.ObjCourtHouseEdit = ADCourtHouse.ReadCourtHouse(Convert.ToInt32(ddlCourtId));
            objSmallClaimsVM.ObjProcessServersEdit = ADProcessServer.ReadProcessServers(Convert.ToInt32(ddlProcessServerId));

            //if (objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.SCSuitFiledDate == DateTime.MinValue)
            //{
            //    objSmallClaimsVM.chkSuitFiled = false;
            //}
            //else
            //{
            //    objSmallClaimsVM.chkSuitFiled = true;
            //}

            //if (objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.TrialDate == DateTime.MinValue)
            //{
            //    objSmallClaimsVM.chkTrialDate = false;
            //}
            //else
            //{
            //    objSmallClaimsVM.chkTrialDate = true;
            //}

            objSmallClaimsVM.chkSuitFiled = chkSuitFiled;
            objSmallClaimsVM.chkTrialDate = chkTrialDate;

            objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.CaseNum = CaseNum;
            objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.ProcessServerRefNum = ProcessServerRefNum;

            objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.TrialDate = TrialDate;
            objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.TrialTime = TrialTime;

            objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.SCSuitFiledDate = SCSuitFiledDate;

            return PartialView(GetVirtualPath("_EditSmallClaimsAccountInfo"), objSmallClaimsVM);
        }

        //Save DebtAccount Small Claims Account Info Details
        [HttpPost]
        public ActionResult SaveSmallClaimsAccountInfo(SmallClaimsVM Obj_SmallClaimsVM)
        {
            int result = 0;
            try
            {
                Obj_SmallClaimsVM.ObjDebtAccountSmallClaimsEdit.DebtAccountId = Convert.ToInt32(Obj_SmallClaimsVM.DebtAccountId);
                Obj_SmallClaimsVM.ObjDebtAccountSmallClaimsEdit.PKID = Convert.ToInt32(Obj_SmallClaimsVM.PKID);

                if(Obj_SmallClaimsVM.chkTrialDate == false)
                {
                    Obj_SmallClaimsVM.ObjDebtAccountSmallClaimsEdit.TrialDate = DateTime.MinValue;
                    Obj_SmallClaimsVM.ObjDebtAccountSmallClaimsEdit.TrialTime = null;
                }

                //Adding AM/PM to the Trial Time
                var TrialTime = Obj_SmallClaimsVM.ObjDebtAccountSmallClaimsEdit.TrialTime;
                if (!string.IsNullOrEmpty(TrialTime))
                {
                    DateTime dtTrialTime = new DateTime(); // Creating the object of DateTime;
                    dtTrialTime = Convert.ToDateTime(TrialTime); // now converting our temporary string into DateTime;              
                    Obj_SmallClaimsVM.ObjDebtAccountSmallClaimsEdit.TrialTime = dtTrialTime.ToString("hh:mm tt"); //12 hour format
                }

                if (Obj_SmallClaimsVM.chkSuitFiled == false)
                {
                    Obj_SmallClaimsVM.ObjDebtAccountSmallClaimsEdit.SCSuitFiledDate = DateTime.MinValue;
                }

                Obj_SmallClaimsVM.ObjDebtAccountSmallClaimsEdit.CourtId =Convert.ToInt32(Obj_SmallClaimsVM.CourtId);
                Obj_SmallClaimsVM.ObjDebtAccountSmallClaimsEdit.ProcessServerId = Convert.ToInt32(Obj_SmallClaimsVM.ProcessServerId);

                result = ADDebtAccountSmallClaims.SaveDebtAccountSmallClaims_AccountInfo(Obj_SmallClaimsVM.ObjDebtAccountSmallClaimsEdit);
            }
            catch (Exception ex)
            {
                result = 0;
            }

            return Json(result);
        }

        //Edit SmallClaims AdditionalInfo
        [HttpPost]
        public ActionResult EditSmallClaimsAdditionalInfo(string DebtAccountId, string PKID)
        {
            objSmallClaimsVM.DebtAccountId = DebtAccountId;
            objSmallClaimsVM.PKID = PKID;
            DebtAccountVM obj;
            obj = AccountInfo.LoadInfo(DebtAccountId);
            objSmallClaimsVM.ClientCode = obj.obj_DebtAccount.ClientCode;
            objSmallClaimsVM.ClientName = obj.obj_DebtAccount.ClientName;
            objSmallClaimsVM.ReferalDate = obj.obj_DebtAccount.ReferalDate;
            objSmallClaimsVM.CollectionStatus = obj.obj_DebtAccount.CollectionStatus;
            objSmallClaimsVM.DebtorName = obj.obj_DebtAccount.DebtorName;
            objSmallClaimsVM.AccountNo = obj.obj_DebtAccount.AccountNo;
            objSmallClaimsVM.Location = obj.obj_DebtAccount.Location;

            objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit = ADDebtAccountSmallClaims.ReadDebtAccountSmallClaims(Convert.ToInt32(PKID));
            objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.PKID = Convert.ToInt32(objSmallClaimsVM.PKID);

            return PartialView(GetVirtualPath("_EditSmallClaimsAdditionalInfo"), objSmallClaimsVM);
        }

        //Save DebtAccount Small Claims SmallClaims Additional Info Details
        [HttpPost]
        public ActionResult SaveSmallClaimsAdditionalInfo(SmallClaimsVM Obj_SmallClaimsVM)
        {
            int result = 0;
            try
            {
                Obj_SmallClaimsVM.ObjDebtAccountSmallClaimsEdit.DebtAccountId = Convert.ToInt32(Obj_SmallClaimsVM.DebtAccountId);
                Obj_SmallClaimsVM.ObjDebtAccountSmallClaimsEdit.PKID = Convert.ToInt32(Obj_SmallClaimsVM.PKID);

                result = ADDebtAccountSmallClaims.SaveDebtAccountSmallClaims_AdditionalInfo(Obj_SmallClaimsVM.ObjDebtAccountSmallClaimsEdit);
            }
            catch (Exception ex)
            {
                result = 0;
            }

            return Json(result);
        }

        //Edit SmallClaims Costs Spent Info
        [HttpPost]
        public ActionResult EditCostsSpentInfo(string DebtAccountId, string PKID)
        {
            SmallClaimsVM objSmallClaimsVM = new SmallClaimsVM(Convert.ToInt32(DebtAccountId));
            objSmallClaimsVM.DebtAccountId = DebtAccountId;
            objSmallClaimsVM.PKID = PKID;
            objSmallClaimsVM.ObjDebtAccountSCCostCRTADt = ADDebtAccountLegalCost.GetDebtAccountSCCRTADtList(DebtAccountId);
            objSmallClaimsVM.ObjDebtAccountSCCostCRTCDt = ADDebtAccountLegalCost.GetDebtAccountSCCRTCDtList(DebtAccountId);

            // Declare an object variable.
            object sumCLFilingFee = null, sumCLServiceFeeAmt = null, sumCLWritFeeAmt = null, sumCLLevyFeeAmt = null, sumCLAbstractFeeAmt = null, sumCLRecordingFeeAmt = null, sumCLCostExpAmt = null;
            object sumAGFilingFee = null, sumAGServiceFeeAmt = null, sumAGWritFeeAmt = null, sumAGLevyFeeAmt = null, sumAGAbstractFeeAmt = null, sumAGRecordingFeeAmt = null, sumAGCostExpAmt = null;
            decimal sumTOTEXP;
            if (objSmallClaimsVM.ObjDebtAccountSCCostCRTCDt.Rows.Count > 0)
            {
                sumCLFilingFee = objSmallClaimsVM.ObjDebtAccountSCCostCRTCDt.Compute("Sum(FilingFee)", string.Empty);
                sumCLServiceFeeAmt = objSmallClaimsVM.ObjDebtAccountSCCostCRTCDt.Compute("Sum(ServiceFeeAmt)", string.Empty);
                sumCLWritFeeAmt = objSmallClaimsVM.ObjDebtAccountSCCostCRTCDt.Compute("Sum(WritFeeAmt)", string.Empty);
                sumCLLevyFeeAmt = objSmallClaimsVM.ObjDebtAccountSCCostCRTCDt.Compute("Sum(LevyFeeAmt)", string.Empty);
                sumCLAbstractFeeAmt = objSmallClaimsVM.ObjDebtAccountSCCostCRTCDt.Compute("Sum(AbstractFeeAmt)", string.Empty);
                sumCLRecordingFeeAmt = objSmallClaimsVM.ObjDebtAccountSCCostCRTCDt.Compute("Sum(RecordingFeeAmt)", string.Empty);
                sumCLCostExpAmt = objSmallClaimsVM.ObjDebtAccountSCCostCRTCDt.Compute("Sum(CostExpAmt)", string.Empty);
            }

            if (objSmallClaimsVM.ObjDebtAccountSCCostCRTADt.Rows.Count > 0)
            {
                sumAGFilingFee = objSmallClaimsVM.ObjDebtAccountSCCostCRTADt.Compute("Sum(FilingFee)", string.Empty);
                sumAGServiceFeeAmt = objSmallClaimsVM.ObjDebtAccountSCCostCRTADt.Compute("Sum(ServiceFeeAmt)", string.Empty);
                sumAGWritFeeAmt = objSmallClaimsVM.ObjDebtAccountSCCostCRTADt.Compute("Sum(WritFeeAmt)", string.Empty);
                sumAGLevyFeeAmt = objSmallClaimsVM.ObjDebtAccountSCCostCRTADt.Compute("Sum(LevyFeeAmt)", string.Empty);
                sumAGAbstractFeeAmt = objSmallClaimsVM.ObjDebtAccountSCCostCRTADt.Compute("Sum(AbstractFeeAmt)", string.Empty);
                sumAGRecordingFeeAmt = objSmallClaimsVM.ObjDebtAccountSCCostCRTADt.Compute("Sum(RecordingFeeAmt)", string.Empty);
                sumAGCostExpAmt = objSmallClaimsVM.ObjDebtAccountSCCostCRTADt.Compute("Sum(CostExpAmt)", string.Empty);
            }

            objSmallClaimsVM.CLFilingFee = Convert.ToDecimal(sumCLFilingFee);
            objSmallClaimsVM.CLServiceFeeAmt = Convert.ToDecimal(sumCLServiceFeeAmt);
            objSmallClaimsVM.CLWritFeeAmt = Convert.ToDecimal(sumCLWritFeeAmt);
            objSmallClaimsVM.CLLevyFeeAmt = Convert.ToDecimal(sumCLLevyFeeAmt);
            objSmallClaimsVM.CLAbstractFeeAmt = Convert.ToDecimal(sumCLAbstractFeeAmt);
            objSmallClaimsVM.CLRecordingFeeAmt = Convert.ToDecimal(sumCLRecordingFeeAmt);
            objSmallClaimsVM.CLCostExpAmt = Convert.ToDecimal(sumCLCostExpAmt);

            objSmallClaimsVM.AGFilingFee = Convert.ToDecimal(sumAGFilingFee);
            objSmallClaimsVM.AGServiceFeeAmt = Convert.ToDecimal(sumAGServiceFeeAmt);
            objSmallClaimsVM.AGWritFeeAmt = Convert.ToDecimal(sumAGWritFeeAmt);
            objSmallClaimsVM.AGLevyFeeAmt = Convert.ToDecimal(sumAGLevyFeeAmt);
            objSmallClaimsVM.AGAbstractFeeAmt = Convert.ToDecimal(sumAGAbstractFeeAmt);
            objSmallClaimsVM.AGRecordingFeeAmt = Convert.ToDecimal(sumAGRecordingFeeAmt);
            objSmallClaimsVM.AGCostExpAmt = Convert.ToDecimal(sumAGCostExpAmt);

            sumTOTEXP = Convert.ToDecimal(sumCLCostExpAmt) + Convert.ToDecimal(sumAGCostExpAmt);

            objSmallClaimsVM.TOTEXP = sumTOTEXP;

            return PartialView(GetVirtualPath("_EditSmallClaimsCostSpentInfo"), objSmallClaimsVM);
        }

        //Add new Small Claims cost spent details
        [HttpPost]
        public ActionResult AddSCCostSpent(string DebtAccountId)
        {
            LegalCostVM obj = new LegalCostVM();
            obj.DebtAccountId = DebtAccountId;

            return PartialView(GetVirtualPath("_AddSCCostSpent"), obj);
        }

        //Save DebtAccount SC Legal Cost Details
        [HttpPost]
        public ActionResult SaveSCLegalCost(LegalCostVM Obj_LegalCostVM)
        {
            int result = 0;
            Obj_LegalCostVM.objDebtAccountLegalCost.DebtAccountId = Convert.ToInt32(Obj_LegalCostVM.DebtAccountId);
            Obj_LegalCostVM.objDebtAccountLegalCost.Id = Convert.ToInt32(Obj_LegalCostVM.Id);
            result = ADDebtAccountLegalCost.SaveDebtAccountLegalCost(Obj_LegalCostVM.objDebtAccountLegalCost);

            return Json(result);
        }

        //Get DebtAccount SC Legal Cost Details for Edit/Update
        [HttpPost]
        public ActionResult GetDebtAccountSCLegalCostDetails(string Id)
        {
            LegalCostVM obj = new LegalCostVM();
            obj.objDebtAccountLegalCostEdit = ADDebtAccountLegalCost.ReadDebtAccountLegalCost(Convert.ToInt32(Id));
            obj.DebtAccountId = Convert.ToString(obj.objDebtAccountLegalCostEdit.DebtAccountId);
            obj.Id = Id;
            return PartialView(GetVirtualPath("_EditSCCostSpent"), obj);
        }

        //Update DebtAccount SC Legal Cost Details
        [HttpPost]
        public ActionResult UpdateSCLegalCost(LegalCostVM Obj_LegalCostVM)
        {
            int result = 0;
            Obj_LegalCostVM.objDebtAccountLegalCostEdit.DebtAccountId = Convert.ToInt32(Obj_LegalCostVM.DebtAccountId);
            Obj_LegalCostVM.objDebtAccountLegalCostEdit.Id = Convert.ToInt32(Obj_LegalCostVM.Id);
            result = ADDebtAccountLegalCost.SaveDebtAccountLegalCost(Obj_LegalCostVM.objDebtAccountLegalCostEdit);

            return Json(result);
        }

        //Delete SC LegalCost
        [HttpPost]
        public ActionResult DeleteSCLegalCost(string Id)
        {
            var result = ADDebtAccountLegalCost.DeleteDebtAccountLegalCost(Id);
            return Json(result);
        }

        //Save DebtAccount Small Claims Court Cost Info Details
        [HttpPost]
        public ActionResult SaveSmallClaimsCostSpentInfo(SmallClaimsVM Obj_SmallClaimsVM)
        {
            int result = 0;
            try
            {
                Obj_SmallClaimsVM.ObjDebtAccountSmallClaimsEdit = new ADDebtAccountSmallClaims();
                Obj_SmallClaimsVM.ObjDebtAccountSmallClaimsEdit.DebtAccountId = Convert.ToInt32(Obj_SmallClaimsVM.DebtAccountId);
                Obj_SmallClaimsVM.ObjDebtAccountSmallClaimsEdit.PKID = Convert.ToInt32(Obj_SmallClaimsVM.PKID);
                Obj_SmallClaimsVM.ObjDebtAccountSmallClaimsEdit.CostsSpent = Obj_SmallClaimsVM.TOTEXP;

                result = ADDebtAccountSmallClaims.SaveDebtAccountSmallClaims_CourtCostInfo(Obj_SmallClaimsVM.ObjDebtAccountSmallClaimsEdit);
            }
            catch (Exception ex)
            {
                result = 0;
            }

            return Json(result);
        }

        //Save DebtAccount Small Claims  SCReviewDate
        [HttpPost]
        public ActionResult UpdateSCReviewDate(string DebtAccountId, string PKID, string NewSCReviewDate)
        {
            int result = 0;
            try
            {
                DebtAccountVM objDebtAccountVM = new DebtAccountVM();
                objDebtAccountVM.objDebtAccountSmallClaims = ADDebtAccountSmallClaims.ReadDebtAccountSmallClaims(Convert.ToInt32(PKID));
                objDebtAccountVM.objDebtAccountSmallClaims.SCReviewDate = Convert.ToDateTime(NewSCReviewDate);

                result = ADDebtAccountSmallClaims.SaveDebtAccountSmallClaims_AccountAction(objDebtAccountVM.objDebtAccountSmallClaims);
            }
            catch (Exception ex)
            {
                result = 0;
            }

            return Json(result);
        }

        //Save DebtAccount Small Claims  ReviewDate(NextContactDate)
        [HttpPost]
        public ActionResult UpdateReviewDate(string DebtAccountId, string PKID, string NewReviewDate)
        {
            int result = 0;
            try
            {
                DebtAccountVM objDebtAccountVM = new DebtAccountVM();
                objDebtAccountVM.obj_ADDebtAccount = ADDebtAccount.ReadDebtAccount(Convert.ToInt32(DebtAccountId));
                objDebtAccountVM.obj_ADDebtAccount.NextContactDate = Convert.ToDateTime(NewReviewDate);

                result = ADDebtAccount.SaveDebtAccount(objDebtAccountVM.obj_ADDebtAccount);
            }
            catch (Exception ex)
            {
                result = 0;
            }

            return Json(result);
        }

        //Edit SmallClaims PostJudgementInfo
        [HttpPost]
        public ActionResult EditSmallClaimsPostJudgementInfo(string DebtAccountId, string PKID)
        {
            objSmallClaimsVM.DebtAccountId = DebtAccountId;
            objSmallClaimsVM.PKID = PKID;

            objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit = ADDebtAccountSmallClaims.ReadDebtAccountSmallClaims(Convert.ToInt32(PKID));

            DateTime JudgmentDate = objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.JudgmentDate;
            decimal SubTotal2 = objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.SubTotal2;

            DateTime Today = DateTime.Now;
            int daysDiff = ((TimeSpan)(Today - JudgmentDate)).Days;

            double Interest = (Convert.ToDouble(SubTotal2) * 0.1 / 365) * (daysDiff);
            Interest = Math.Ceiling(Interest * 100) / 100;
            objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.Interest = Convert.ToDecimal(Interest);

            double SCTotalDue = (Convert.ToDouble(SubTotal2) * 0.1 / 365) * (daysDiff) + Convert.ToDouble(SubTotal2);
            SCTotalDue = Math.Ceiling(SCTotalDue * 100) / 100;
            objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.SCTotalDue = Convert.ToDecimal(SCTotalDue);

            if (objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.JudgmentDate == DateTime.MinValue)
            {
                objSmallClaimsVM.chkJudgmentDate = false;
            }
            else
            {
                objSmallClaimsVM.chkJudgmentDate = true;
            }

            return PartialView(GetVirtualPath("_EditSmallClaimsPostJudgementInfo"), objSmallClaimsVM);
        }

        //Edit SmallClaims PostJudgementInfo txt_leave
        public ActionResult EditSmallClaimsPostJudgementInfotxt_Leave(string DebtAccountId, string PKID, DateTime JudgmentDate, decimal JudgmentAmt, decimal Costs, bool chkJudgmentDate)
        {
            objSmallClaimsVM.DebtAccountId = DebtAccountId;
            objSmallClaimsVM.PKID = PKID;
            objSmallClaimsVM.chkJudgmentDate = chkJudgmentDate;

            objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit = ADDebtAccountSmallClaims.ReadDebtAccountSmallClaims(Convert.ToInt32(PKID));

            if (objSmallClaimsVM.chkJudgmentDate == false)
            {
                objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.JudgmentDate = DateTime.MinValue;
            }
            else
            {
                objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.JudgmentDate = JudgmentDate;
            }

            objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.JudgmentAmt = JudgmentAmt;
            objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.Costs = Costs;

            decimal SubTotal1 = JudgmentAmt + Costs;
            objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.SubTotal1 = SubTotal1;

            objSmallClaimsVM.ObjDebtAccountSCPostPmtsDt = ADDebtAccountSmallClaims.GetDebtAccountSCPostPmtsDt(DebtAccountId, JudgmentDate);

            if (objSmallClaimsVM.ObjDebtAccountSCPostPmtsDt.Rows.Count > 0)
            {
                objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.Credits = Convert.ToDecimal(objSmallClaimsVM.ObjDebtAccountSCPostPmtsDt.Rows[0]["Credit"]);
            }
            else
            {
                objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.Credits = 0;
            }

            objSmallClaimsVM.ObjDebtAccountSCPostCostsDt = ADDebtAccountSmallClaims.GetDebtAccountSCPostCostsDt(DebtAccountId, JudgmentDate);

            if (objSmallClaimsVM.ObjDebtAccountSCPostCostsDt.Rows.Count > 0)
            {
                objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.PostCosts = Convert.ToDecimal(objSmallClaimsVM.ObjDebtAccountSCPostCostsDt.Rows[0]["PostCosts"]);
            }
            else
            {
                objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.PostCosts = 0;
            }

            objSmallClaimsVM.ObjDebtAccountSCRecCostsDt = ADDebtAccountSmallClaims.GetDebtAccountSCRecCostsDt(DebtAccountId);

            if (objSmallClaimsVM.ObjDebtAccountSCRecCostsDt.Rows.Count > 0)
            {
                objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.RecCosts = Convert.ToDecimal(objSmallClaimsVM.ObjDebtAccountSCRecCostsDt.Rows[0]["RecCosts"]);
            }
            else
            {
                objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.RecCosts = 0;
            }

            decimal SubTotal2 = objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.JudgmentAmt +
                                             objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.PostCosts -
                                             objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.Credits +
                                             objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.Costs -
                                             objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.RecCosts;

            objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.SubTotal2 = SubTotal2;

            double DailyInt = Convert.ToDouble(SubTotal2) * 0.1 / 365;
            DailyInt = Math.Ceiling(DailyInt * 100) / 100;
            objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.DailyInt = Convert.ToDecimal(DailyInt);

            DateTime Today = DateTime.Now;
            int daysDiff = ((TimeSpan)(Today - JudgmentDate)).Days;

            double Interest = (Convert.ToDouble(SubTotal2) * 0.1 / 365) * (daysDiff);
            Interest = Math.Ceiling(Interest * 100) / 100;
            objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.Interest = Convert.ToDecimal(Interest);

            double SCTotalDue = (Convert.ToDouble(SubTotal2) * 0.1 / 365) * (daysDiff) + Convert.ToDouble(SubTotal2);
            SCTotalDue = Math.Ceiling(SCTotalDue * 100) / 100;
            objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit.SCTotalDue = Convert.ToDecimal(SCTotalDue);

            return PartialView(GetVirtualPath("_EditSmallClaimsPostJudgementInfo"), objSmallClaimsVM);
        }


        //Save DebtAccount Small Claims Post Judgement Info Details
        [HttpPost]
        public ActionResult SaveSmallClaimsPostJudgementInfo(SmallClaimsVM Obj_SmallClaimsVM)
        {
            int result = 0;
            try
            {
                Obj_SmallClaimsVM.ObjDebtAccountSmallClaimsEdit.DebtAccountId = Convert.ToInt32(Obj_SmallClaimsVM.DebtAccountId);
                Obj_SmallClaimsVM.ObjDebtAccountSmallClaimsEdit.PKID = Convert.ToInt32(Obj_SmallClaimsVM.PKID);

                if (Obj_SmallClaimsVM.chkJudgmentDate == false)
                {
                    Obj_SmallClaimsVM.ObjDebtAccountSmallClaimsEdit.JudgmentDate = DateTime.MinValue;
                }


                result = ADDebtAccountSmallClaims.SaveDebtAccountSmallClaims_PostJudgementInfo(Obj_SmallClaimsVM.ObjDebtAccountSmallClaimsEdit);
            }
            catch (Exception ex)
            {
                result = 0;
            }

            return Json(result);
        }

        //Calculate Small Claims Judgment Totals
        private void CalculateSCJudgmentTotals(ADDebtAccountSmallClaims objADDebtAccountSmallClaims)
        {
            string DebtAccountId = Convert.ToString(objADDebtAccountSmallClaims.DebtAccountId);
            string PKID = Convert.ToString(objADDebtAccountSmallClaims.PKID);
            DateTime JudgmentDate = objADDebtAccountSmallClaims.JudgmentDate;

            decimal SubTotal1 = objADDebtAccountSmallClaims.JudgmentAmt + objADDebtAccountSmallClaims.Costs;
            objADDebtAccountSmallClaims.SubTotal1 = SubTotal1;

            objSmallClaimsVM.ObjDebtAccountSCPostPmtsDt = ADDebtAccountSmallClaims.GetDebtAccountSCPostPmtsDt(DebtAccountId, JudgmentDate);

            if (objSmallClaimsVM.ObjDebtAccountSCPostPmtsDt.Rows.Count > 0)
            {
                objADDebtAccountSmallClaims.Credits = Convert.ToDecimal(objSmallClaimsVM.ObjDebtAccountSCPostPmtsDt.Rows[0]["Credit"]);
            }
            else
            {
                objADDebtAccountSmallClaims.Credits = 0;
            }

            objSmallClaimsVM.ObjDebtAccountSCPostCostsDt = ADDebtAccountSmallClaims.GetDebtAccountSCPostCostsDt(DebtAccountId, JudgmentDate);

            if (objSmallClaimsVM.ObjDebtAccountSCPostCostsDt.Rows.Count > 0)
            {
                objADDebtAccountSmallClaims.PostCosts = Convert.ToDecimal(objSmallClaimsVM.ObjDebtAccountSCPostCostsDt.Rows[0]["PostCosts"]);
            }
            else
            {
                objADDebtAccountSmallClaims.PostCosts = 0;
            }

            objSmallClaimsVM.ObjDebtAccountSCRecCostsDt = ADDebtAccountSmallClaims.GetDebtAccountSCRecCostsDt(DebtAccountId);

            if (objSmallClaimsVM.ObjDebtAccountSCRecCostsDt.Rows.Count > 0)
            {
                objADDebtAccountSmallClaims.RecCosts = Convert.ToDecimal(objSmallClaimsVM.ObjDebtAccountSCRecCostsDt.Rows[0]["RecCosts"]);
            }
            else
            {
                objADDebtAccountSmallClaims.RecCosts = 0;
            }

            decimal SubTotal2 = objADDebtAccountSmallClaims.JudgmentAmt +
                                            objADDebtAccountSmallClaims.PostCosts -
                                             objADDebtAccountSmallClaims.Credits +
                                           objADDebtAccountSmallClaims.Costs -
                                            objADDebtAccountSmallClaims.RecCosts;

            objADDebtAccountSmallClaims.SubTotal2 = SubTotal2;


            DateTime Today = DateTime.Now;
            int daysDiff = ((TimeSpan)(Today - JudgmentDate)).Days;

            if (objADDebtAccountSmallClaims.JudgmentDate > DateTime.MinValue)
            { 
            if ((Convert.ToDouble(SubTotal2) * 0.1 / 365) < 0.01)
            {
                objADDebtAccountSmallClaims.DailyInt = 0;
                objADDebtAccountSmallClaims.Interest = 0;
                objADDebtAccountSmallClaims.SCTotalDue = 0;
            }
            else
            {
                double DailyInt = Convert.ToDouble(SubTotal2) * 0.1 / 365;
                DailyInt = Math.Ceiling(DailyInt * 100) / 100;
                objADDebtAccountSmallClaims.DailyInt = Convert.ToDecimal(DailyInt);

                double Interest = (Convert.ToDouble(SubTotal2) * 0.1 / 365) * (daysDiff);
                Interest = Math.Ceiling(Interest * 100) / 100;
                objADDebtAccountSmallClaims.Interest = Convert.ToDecimal(Interest);

                double SCTotalDue = (Convert.ToDouble(SubTotal2) * 0.1 / 365) * (daysDiff) + Convert.ToDouble(SubTotal2);
                SCTotalDue = Math.Ceiling(SCTotalDue * 100) / 100;
                objADDebtAccountSmallClaims.SCTotalDue = Convert.ToDecimal(SCTotalDue);
            }

            objSmallClaimsVM.ObjDebtAccountSCCostsSpentDt = ADDebtAccountSmallClaims.GetDebtAccountSCCostsSpentDt(DebtAccountId);

            if (objSmallClaimsVM.ObjDebtAccountSCCostsSpentDt.Rows.Count > 0)
            {
                objADDebtAccountSmallClaims.CostsSpent = Convert.ToDecimal(objSmallClaimsVM.ObjDebtAccountSCCostsSpentDt.Rows[0]["CostsSpent"]);
            }
            else
            {
                objADDebtAccountSmallClaims.CostsSpent = 0;
            }
        }
            if (objADDebtAccountSmallClaims.JudgmentAmt > 0)
            {
                objADDebtAccountSmallClaims.SuitAmt = objADDebtAccountSmallClaims.SCTotalDue;
            }
        }

        //Edit SmallClaims Costs Spent Info
        [HttpPost]
        public ActionResult EditLetterMergeInfo(string DebtAccountId, string PKID)
        {
            objSmallClaimsVM.DebtAccountId = DebtAccountId;
            objSmallClaimsVM.PKID = PKID;

            objSmallClaimsVM.ObjDebtAccountSmallClaimsEdit = ADDebtAccountSmallClaims.ReadDebtAccountSmallClaims(Convert.ToInt32(PKID));

            return PartialView(GetVirtualPath("_EditSmallClaimsLetterMergeInfo"), objSmallClaimsVM);
        }


        //Save DebtAccount Small Claims Post Judgement Info Details
        [HttpPost]
        public ActionResult SaveSCLetterMerge(SmallClaimsVM Obj_SmallClaimsVM)
        {
            int result = 0;
            try
            {
                Obj_SmallClaimsVM.ObjDebtAccountSmallClaimsEdit.DebtAccountId = Convert.ToInt32(Obj_SmallClaimsVM.DebtAccountId);
                Obj_SmallClaimsVM.ObjDebtAccountSmallClaimsEdit.PKID = Convert.ToInt32(Obj_SmallClaimsVM.PKID);

                result = ADDebtAccountSmallClaims.SaveDebtAccountSmallClaims_LetterMergeInfo(Obj_SmallClaimsVM.ObjDebtAccountSmallClaimsEdit);
            }
            catch (Exception ex)
            {
                result = 0;
            }

            return Json(result);
        }

        //Show Small Claims Action Codes
        [HttpPost]
        public ActionResult AddSmallClaimsAction(string DebtAccountId, string CollectionStatusId)
        {
            ActionHistoryVM objActionHistoryVM = new ActionHistoryVM();
            objActionHistoryVM.DebtAccountId = DebtAccountId;
            objActionHistoryVM.CollectionStatusId = CollectionStatusId;
            return PartialView(GetVirtualPath("_AddSmallClaimsActionCode"), objActionHistoryVM);
        }

        //Add New Request Letter Details
        [HttpPost]
        public ActionResult AddRequestLetter(string DebtAccountId, string DebtId)
        {
            LetterVM objLetterVM = new LetterVM(Convert.ToInt32(DebtId));
            objLetterVM.DebtAccountId = DebtAccountId;
            objLetterVM.DebtId = DebtId;
            objLegalInfoVM.PKID = "0";

            return PartialView(GetVirtualPath("_AddRequestLetter"), objLetterVM);
        }

        //Get Letter Type List by LetterType
        public ActionResult GetLetterType(string LetterTypeValue)
        {
            List<CommonBindList> LetterTypeList = new List<CommonBindList>();
            try
            {
                LetterTypeList = CommonBindList.GetLetterTypeList(LetterTypeValue);
            }
            catch (Exception ex)
            {

            }
            return Json(LetterTypeList, JsonRequestBehavior.AllowGet);
        }

        //Save DebtAccount Letter Request Details
        [HttpPost]
        public ActionResult SaveLetterRequest(LetterVM Obj_LetterVM)
        {
            int result = 0;
            try
            {
                Obj_LetterVM.ObjDebtAccount = ADDebtAccount.ReadDebtAccount(Convert.ToInt32(Obj_LetterVM.DebtAccountId));
                Obj_LetterVM.ObjDebtAccountLetters = new ADDebtAccountLetters();
                Obj_LetterVM.ObjDebtAccountLetters.DebtAccountId = Convert.ToInt32(Obj_LetterVM.DebtAccountId);

                Obj_LetterVM.ObjDebtAccountLetters.LetterId = Convert.ToInt32(Obj_LetterVM.ddlLetterTypeId);

                string[] LetterType = Obj_LetterVM.LetterType.Split('-');
                Obj_LetterVM.ObjDebtAccountLetters.LetterCode = LetterType[0].Trim();

                Obj_LetterVM.ObjDebtAccountLetters.ContractId = Obj_LetterVM.ObjDebtAccount.ContractId;

                Obj_LetterVM.ObjLetters = ADLetters.ReadCollectionLetters(Convert.ToInt32(Obj_LetterVM.ddlLetterTypeId));
                
                ADCompanyInfo objCompanyInfo = new ADCompanyInfo();
                Obj_LetterVM.ObjCompanyInfo = objCompanyInfo.ReadCompanyInfo(1);

                //SC Invoice generation
                if (Obj_LetterVM.ObjLetters.GenerateInvoiceNum == true)
                {
                    Obj_LetterVM.ObjDebtAccountLetters.IsSCInvoiceLetter = true;
                    Obj_LetterVM.ObjDebtAccountLetters.SCInvoiceNum = Obj_LetterVM.ObjCompanyInfo.MiscInvoiceNum + 1;
                    Obj_LetterVM.ObjCompanyInfo.MiscInvoiceNum = Obj_LetterVM.ObjDebtAccountLetters.SCInvoiceNum;
                    objCompanyInfo.SaveCompanyInfo(Obj_LetterVM.ObjCompanyInfo);
                }

                if (Obj_LetterVM.RbtLetterType == "D")
                {
                    //Debtor Letter
                    string[] selectedDebtAddress = Obj_LetterVM.selectedDebtAddress.Split(',');
                    foreach(string DebtAddressId in selectedDebtAddress)
                    {
                        Obj_LetterVM.ObjDebtAddress = ADDebtAddress.ReadAddress(Convert.ToInt32(DebtAddressId));
                        Obj_LetterVM.ObjDebtAccountLetters.AddressId =Convert.ToInt32(DebtAddressId);
                        Obj_LetterVM.ObjDebtAccountLetters.AddressTypeId = Obj_LetterVM.ObjDebtAddress.DebtAddressTypeId;
                        result = ADDebtAccountLetters.SaveDebtAccountLetters(Obj_LetterVM.ObjDebtAccountLetters);
                    }
                }
                else
                {
                    //ClientLetters
                    Obj_LetterVM.ObjDebtAccountLetters.AddressId = 0;
                    Obj_LetterVM.ObjDebtAccountLetters.AddressTypeId = 0;
                    result = ADDebtAccountLetters.SaveDebtAccountLetters(Obj_LetterVM.ObjDebtAccountLetters);
                }

               // result = ADDebtAccountLetters.SaveDebtAccountLetters(Obj_LetterVM.ObjDebtAccountLetters);
            }
            catch (Exception ex)
            {
                result = 0;
            }

            return Json(result);
        }

        //Delete DebtAccountLetters Details
        [HttpPost]
        public ActionResult DeleteDebtAccountLetter(string DebtAccountLetterId)
        {
            bool result = ADDebtAccountLetters.DeleteDebtAccountLetters(DebtAccountLetterId);
            if (result)
            {
                return Json(result);
            }
            else
            {
                return Json(0);
            }
        }

        //Add New Request Letter Details
        [HttpPost]
        public ActionResult AddNewStatusReport(string DebtAccountId, string DebtId)
        {
            LetterVM objLetterVM = new LetterVM(Convert.ToInt32(DebtId));
            objLetterVM.DebtAccountId = DebtAccountId;
            objLetterVM.DebtId = DebtId;
            objLegalInfoVM.PKID = "0";
            objLetterVM.ddlStatusReportLetterTypeId = "-1";
            //objLetterVM.CustomText = "";

            return PartialView(GetVirtualPath("_AddNewStatusReport"), objLetterVM);
        }

        //Get StatusReportLetterType List by StatusReportLetterType
        public ActionResult GetStatusReportLetterType(string StatusReportLetterTypeValue)
        {
            List<CommonBindList> LetterTypeList = new List<CommonBindList>();
            try
            {
                LetterTypeList = CommonBindList.GetStatusReportLetterTypeList(StatusReportLetterTypeValue);
            }
            catch (Exception ex)
            {

            }
            return Json(LetterTypeList, JsonRequestBehavior.AllowGet);
        }

        //Get GetCollectionCustomText details 
        [HttpPost]
        public ActionResult GetCollectionCustomText(string StatusReportLetterTypeId, string DebtId, string DebtAccountId, string StatusReportLetterType)
        {
            LetterVM objLetterVM = new LetterVM(Convert.ToInt32(DebtId));
            objLetterVM.DebtId = DebtId;
            objLetterVM.DebtAccountId = DebtAccountId;
            objLetterVM.PKID = "0";

            objLetterVM.ddlStatusReportLetterTypeId = StatusReportLetterTypeId;
            objLetterVM.RbtStatusReportLetterType = StatusReportLetterType;
            //objLetterVM.ObjDebtAccountLetters_StatusReport = new ADDebtAccountLetters();
            ADCustomText objADCustomText = new ADCustomText();
            objLetterVM.ObjCollectionCustomText = objADCustomText.ReadCustomText(Convert.ToInt32(StatusReportLetterTypeId));
            objLetterVM.CustomText = objLetterVM.ObjCollectionCustomText.CustomText;
 

            return PartialView(GetVirtualPath("_AddNewStatusReport"), objLetterVM);
        }

        //Get GetCollectionCustomTextEdit details 
        [HttpPost]
        public ActionResult GetCollectionCustomTextEdit(string StatusReportLetterTypeId, string DebtId, string DebtAccountId, string StatusReportLetterType,string PKID)
        {
             LetterVM objLetterVM = new LetterVM(Convert.ToInt32(DebtId));
            objLetterVM.DebtId = DebtId;
            objLetterVM.DebtAccountId = DebtAccountId;
            objLetterVM.PKID = PKID;

            objLetterVM.ddlStatusReportLetterTypeId = StatusReportLetterTypeId;
            objLetterVM.RbtStatusReportLetterType = StatusReportLetterType;
            //objLetterVM.ObjDebtAccountLetters_StatusReport = new ADDebtAccountLetters();
            ADCustomText objADCustomText = new ADCustomText();
            objLetterVM.ObjCollectionCustomText = objADCustomText.ReadCustomText(Convert.ToInt32(StatusReportLetterTypeId));
            objLetterVM.CustomText = objLetterVM.ObjCollectionCustomText.CustomText;

            return PartialView(GetVirtualPath("_EditStatusReport"), objLetterVM);
        }


        //Save DebtAccount New Status Report Details
        [HttpPost]
        public ActionResult SaveStatusReportLetterRequest(LetterVM Obj_LetterVM)
        {
            int result = 0;
            try
            {
                Obj_LetterVM.ObjDebtAccount = ADDebtAccount.ReadDebtAccount(Convert.ToInt32(Obj_LetterVM.DebtAccountId));
                Obj_LetterVM.ObjDebtAccountLetters_StatusReport = new ADDebtAccountLetters();
                Obj_LetterVM.ObjDebtAccountLetters_StatusReport.DebtAccountId = Convert.ToInt32(Obj_LetterVM.DebtAccountId);
                Obj_LetterVM.ObjDebtAccountLettersDt = ADDebtAccountLetters.GetDebtAccountLettersDtList(Obj_LetterVM.RbtStatusReportLetterType);
                Obj_LetterVM.ObjDebtAccountLetters_StatusReport.PKID =Convert.ToInt32(Obj_LetterVM.PKID);

                ADClientCollectionInfo Obj_ADCCI = new ADClientCollectionInfo();
                Obj_LetterVM.ObjClientCollectionInfo = Obj_ADCCI.LoadCollectionInfo(Obj_LetterVM.ObjDebtAccount.ClientId);

                string CustomText = Obj_LetterVM.CustomText;
                if (!string.IsNullOrEmpty(CustomText))
                {
                    CustomText = CustomText.Trim();
                }

                if (Obj_LetterVM.ObjDebtAccountLettersDt.Rows.Count > 0)
                {
                    Obj_LetterVM.ObjDebtAccountLetters_StatusReport.LetterId = Convert.ToInt32(Obj_LetterVM.ObjDebtAccountLettersDt.Rows[0]["PKID"]);
                    Obj_LetterVM.ObjDebtAccountLetters_StatusReport.LetterCode = Convert.ToString(Obj_LetterVM.ObjDebtAccountLettersDt.Rows[0]["LetterCode"]);

                    DateTime Today = DateTime.Now;
                    int ReviewDays = Convert.ToInt32(Obj_LetterVM.ObjDebtAccountLettersDt.Rows[0]["ReviewDays"]);
                    int StatusDays = Convert.ToInt32(Obj_LetterVM.ObjDebtAccountLettersDt.Rows[0]["StatusDays"]);
                    bool IsStatusGroup, IsStatusRpt;
                    IsStatusGroup = Obj_LetterVM.ObjClientCollectionInfo.IsStatusGroup;
                    IsStatusRpt = Obj_LetterVM.ObjClientCollectionInfo.IsStatusRpt;

                    if (ReviewDays > 0)
                    {
                        Obj_LetterVM.ObjDebtAccount.NextContactDate = Today.AddDays(ReviewDays);
                    }

                    if (StatusDays > 0 && (IsStatusGroup == true || IsStatusRpt || true))
                    {
                        Obj_LetterVM.ObjDebtAccount.NextStatusDate = Today.AddDays(StatusDays);
                    }
                    result = ADDebtAccount.SaveDebtAccount_Letters(Obj_LetterVM.ObjDebtAccount);

                    //Add custom text to DebtAccountNote
                    if (IsStatusGroup == true)
                    {
                        Obj_LetterVM.ObjDebtAccountNote.DebtAccountId = Convert.ToInt32(Obj_LetterVM.DebtAccountId);
                        Obj_LetterVM.ObjDebtAccountNote.Note = "STATUS NOTE: " + CustomText;
                        Obj_LetterVM.ObjDebtAccountNote.NoteTypeId = 6;
                        result = ADDebtAccountNote.SaveDebtAccountNote(Obj_LetterVM.ObjDebtAccountNote);
                    }
                }

                Obj_LetterVM.ObjDebtAccountLetters_StatusReport.ContractId = Obj_LetterVM.ObjDebtAccount.ContractId;
                Obj_LetterVM.ObjDebtAccountLetters_StatusReport.CustomTextId = Convert.ToInt32(Obj_LetterVM.ddlStatusReportLetterTypeId);
                Obj_LetterVM.ObjDebtAccountLetters_StatusReport.CustomText = CustomText;


                string[] StatusReportLetterType = Obj_LetterVM.StatusReportLetterType.Split('-');
                Obj_LetterVM.ObjDebtAccountLetters_StatusReport.CustomLetterCode = StatusReportLetterType[0].Trim();

                Obj_LetterVM.ObjDebtAccountLetters_StatusReport.CustomLetterType = Obj_LetterVM.RbtStatusReportLetterType;

                if (Obj_LetterVM.chkDesk == true)
                {
                    Obj_LetterVM.ObjDebtAccountLetters_StatusReport.LocationId = Convert.ToInt32(Obj_LetterVM.ddlLocationId);
                }


                if (Obj_LetterVM.RbtStatusReportLetterType == "D")
                {
                    //Debtor Letter
                    string[] selectedDebtAddress = Obj_LetterVM.selectedDebtAddress.Split(',');
                    foreach (string DebtAddressId in selectedDebtAddress)
                    {
                        Obj_LetterVM.ObjDebtAddress = ADDebtAddress.ReadAddress(Convert.ToInt32(DebtAddressId));
                        Obj_LetterVM.ObjDebtAccountLetters_StatusReport.AddressId = Convert.ToInt32(DebtAddressId);
                        Obj_LetterVM.ObjDebtAccountLetters_StatusReport.AddressTypeId = Obj_LetterVM.ObjDebtAddress.DebtAddressTypeId;
                        result = ADDebtAccountLetters.SaveDebtAccountLetters_StatusReport(Obj_LetterVM.ObjDebtAccountLetters_StatusReport);
                    }
                }
                else
                {
                    //ClientLetters
                    Obj_LetterVM.ObjDebtAccountLetters_StatusReport.AddressId = 0;
                    Obj_LetterVM.ObjDebtAccountLetters_StatusReport.AddressTypeId = 0;
                    result = ADDebtAccountLetters.SaveDebtAccountLetters_StatusReport(Obj_LetterVM.ObjDebtAccountLetters_StatusReport);
                }
            }
            catch (Exception ex)
            {
                result = 0;
            }

            return Json(result);
        }


        //Get Status Report Details
        [HttpPost]
        public ActionResult GetStatusReportDetails(string PKID, string DebtAccountId, string DebtId)
        {
            LetterVM objLetterVM = new LetterVM(Convert.ToInt32(DebtId));
            objLetterVM.DebtAccountId = DebtAccountId;
            objLetterVM.DebtId = DebtId;
            objLetterVM.PKID = PKID;
            objLetterVM.ObjDebtAccountLetters = ADDebtAccountLetters.ReadDebtAccountLetters(Convert.ToInt32(PKID));

            objLetterVM.ObjDebtAccountLetters.PKID =Convert.ToInt32(PKID);

            objLetterVM.ObjLetters = ADLetters.ReadCollectionLetters(objLetterVM.ObjDebtAccountLetters.LetterId);

            objLetterVM.RbtStatusReportLetterType = objLetterVM.ObjLetters.LetterType;
            objLetterVM.ddlStatusReportLetterTypeId = Convert.ToString(objLetterVM.ObjDebtAccountLetters.CustomTextId);

            objLetterVM.CustomText = objLetterVM.ObjDebtAccountLetters.CustomText;

            if (objLetterVM.ObjDebtAccountLetters.LocationId <= 0)
            {
                objLetterVM.chkDesk = false;
            }
            else
            {
                objLetterVM.chkDesk = true;
                objLetterVM.ddlLocationId= Convert.ToString(objLetterVM.ObjDebtAccountLetters.LocationId);
            }
            
            return PartialView(GetVirtualPath("_EditStatusReport"), objLetterVM);
        }

        //Print DebtAccount Letter
        [HttpPost]
        public string PrintDebtAccountLetter(int DebtAccountId, int PKID, int AddressId, int LocationId)
        {
            LetterVM objLetterVM = new LetterVM();
            objLetterVM.PKID = Convert.ToString(PKID); 
            objLegalInfoVM.DebtAccountId = Convert.ToString(DebtAccountId);

            string PdfUrl = objLetterVM.PrintLetter(DebtAccountId, PKID, AddressId, LocationId);
            //PdfUrl = "D:/Projects/CRFS/CRFView/CRFView/CRFView/Output/Reports/LauraChung-1788-CollectionPaySchedule-20190713021618.PDF";

            if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
            {

                objLetterVM.ObjDebtAccountLetters_StatusReport = new ADDebtAccountLetters();
                objLetterVM.ObjDebtAccountLetters_StatusReport.DebtAccountId = DebtAccountId;
                objLetterVM.ObjDebtAccountLetters_StatusReport.PKID = PKID;
                objLetterVM.ObjDebtAccountLetters_StatusReport.DatePrinted = DateTime.Now;
               ADDebtAccountLetters.SaveDebtAccountLetters_StatusReport(objLetterVM.ObjDebtAccountLetters_StatusReport);

                string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl)); 
                string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                return (path);
            }
            return "error";
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewCollections/Collectors/WorkCard/" + ViewName + ".cshtml";
            return path;
        }
        public ActionResult LoadSendDBTEmail(string DebtAccountId,string DebtClientId)
        {
            DebtAccountVM obj;

            //Session["NextValue"] = NextValue;
            //Session["PreviousValue"] = PreviousValue; 
            try
            {
                obj = AccountInfo.LoadInfo(DebtAccountId);
                CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels.ClientVM Obj_ClientVM = new CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels.ClientVM();
               var ClientCollectionInfoList = obj.dtCollectionInfo;

                List<CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels.ReceiptientGridVM> ReceiptientGridList = new List<CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels.ReceiptientGridVM>();
            
               foreach(DataRow row in obj.dtCollectionInfo.Rows)
                {

                    if (row["ContactEmail"].ToString() != "")
                    {
                        ReceiptientGridList.Add(new CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels.ReceiptientGridVM { keyvalue = row["ContactEmail"].ToString(), To = "Cl-Main", Name = row["ContactName"].ToString(), Email = row["ContactEmail"].ToString() });

                    }
                    if (row["ContactEmail2"].ToString().Length > 0)
                    {
                        ReceiptientGridList.Add(new CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels.ReceiptientGridVM { keyvalue = row["ContactEmail2"].ToString(), To = "Cl-Sec", Name = row["ContactName2"].ToString(), Email = row["ContactEmail2"].ToString() });
                    }
                    if (row["ContactCCEmail"].ToString().Length > 0)
                    {
                        ReceiptientGridList.Add(new CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels.ReceiptientGridVM { keyvalue = row["ContactCCEmail"].ToString(), To = "Cl-CC", Name = row["ContactCCEmail"].ToString(), Email = row["ContactCCEmail"].ToString() });

                    }
                }
                Obj_ClientVM.ReceiptientGridList = ReceiptientGridList;

             List <ClientContact> ClientContactList = ADClientContact.GetContactDetailsByClientId(Convert.ToInt32(DebtClientId));
                foreach (var item in ClientContactList)
                {
                    ReceiptientGridList.Add(new CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels.ReceiptientGridVM { keyvalue = item.Email, To = "Contact", Name = item.ContactName, Email = item.Email });

                }
                string Text = "Send Email";
                string s = obj.obj_DebtAccount.DebtorName + ", CRFS#:" + obj.obj_DebtAccount.DebtAccountId + ", Acct#:" + obj.obj_DebtAccount.AccountNo + ", Remaining Bal: $" + obj.obj_DebtAccount.TotBalAmt;
                Obj_ClientVM.Subject = s;

                //CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels.ClientVM clientVM = new CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels.ClientVM();
                Obj_ClientVM.DebtAccountId = DebtAccountId;
               Obj_ClientVM.ListADDebtAccountAttachment = ADDebtAccountAttachment.GetDebtAccountAttachments(DebtAccountId);
               
                return PartialView(GetVirtualPath("_SendDBTEmail"),Obj_ClientVM);

            }
            catch(Exception ex)
            {

            }
            return PartialView(GetVirtualPath("_SendDBTEmail"));
        }
        [HttpPost]
        public ActionResult AddAttachDoc(int Id)
        {
            try
            {
                var attachment = ADDebtAccountAttachment.ReadDebtAccountAttachment(Id);
                var path = Path.Combine(Server.MapPath("~/Output/DownloadableFiles"), attachment.FileName.Replace(" ", ""));
                ADDebtAccountAttachment.SaveFileToBeViewed(attachment.DocumentImage, path);
                return Json(path);
            }
            catch (Exception ex)
            {
                return Json("");
            }

        }
        [HttpPost]
        public ActionResult SaveMISCDocument()
        {
            try
            {
                var file = Request.Files[0];
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/Output/UploadedFiles"), fileName);
                file.SaveAs(path);
                Session["path"] = path;
                return Json(path);
            }
            catch (Exception ex)
            {
                return Json(false);
            }
        }
        [HttpPost]
        public ActionResult SendEmailInfo(string[] Email, string Subject, string Message, string[] AttachedFiles, bool isSend, int DebtAccountId)
        {
            try
            {
                string Files = AttachedFiles[0];
                string AllFilePaths = Files.Replace("\n", "").Replace("\r", "");
                AttachedFiles = AllFilePaths.Split(' ');
                CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                var myuser = ADPortalUsers.GetUserForId(CU_Obj.Id);
                //string SBODY = Message+ System.Environment.NewLine + System.Environment.NewLine + System.Environment.NewLine +
                //    myuser.SignatureLine1 + System.Environment.NewLine + myuser.SignatureLine2 + System.Environment.NewLine +
                //    myuser.SignatureLine3 + System.Environment.NewLine + myuser.SignatureLine4 +
                //    System.Environment.NewLine ;

                string Body = "";
                var msg  = Message.Split('\n');
              
                foreach (var item in msg)
                {
                    Body += "<p>";
                    if (item == "")
                    {
                        Body += "<br />";
                    }
                    Body += item + "</p>";
                }

                //Body +=  "</p>";
                Body += "<br />";
                Body += "<br />";
                Body += "<br />";
                Body += "<p style='margin-bottom:0px;'>" + myuser.SignatureLine1 + "</p>";
                Body += "<p style='margin-bottom:0px;'>" + myuser.SignatureLine2 + "</p>";
                Body += "<p style='margin-bottom:0px;'>" + myuser.SignatureLine3 + "</p>";
                Body += "<p style='margin-bottom:0px;'>" + myuser.SignatureLine4 + "</p>";


                bool isSent = false;

                if (Email == null)
                {
                    return Json(-1);
                }

                foreach (var item in Email)
                {

                    //string to = "onkar.kulkarni@rhealtech.com";
                    //string to = "testrhealsms@gmail.com";

                    //string from = "testrhealsms@gmail.com";
                    string to = item.ToString();
                    string from = user.Email;


                    isSent = CRFView.Adapters.Utility.sendEmail(to, from, Subject, Body, AttachedFiles.ToList(), isSend);

                }


                ADDebtAccountNote objADDebtAccountNote = new ADDebtAccountNote();
                objADDebtAccountNote.DebtAccountId = DebtAccountId;
                objADDebtAccountNote.NoteTypeId = 1;
                var emailsentto = "";
                foreach (var email in Email)
                {
                    emailsentto += email + ";";
                }
                objADDebtAccountNote.Note = "EMAIL SENT TO: " + emailsentto;
                objADDebtAccountNote.Note += System.Environment.NewLine;
                objADDebtAccountNote.Note += "SUBJECT: " + Subject;
                objADDebtAccountNote.Note += System.Environment.NewLine;
                objADDebtAccountNote.Note += "MESSAGE: ";
                objADDebtAccountNote.Note += System.Environment.NewLine;
          
                objADDebtAccountNote.Note += Message;
                int debtacchistorysaved = ADDebtAccountNote.SaveDebtAccountNote(objADDebtAccountNote);
                if (isSend == true)
                {
                    return Json(isSent);
                }
                else
                {
                    return Json(false);
                }
            }
            catch (Exception ex)
            {
                return Json(-2);
            }
        }
public ActionResult LoadSendDBTAccountEmail(string DebtAccountId, string DebtClientId,string Email)
        {
            DebtAccountVM Obj_DebtAccount = new DebtAccountVM();
          Obj_DebtAccount=  AccountInfo.LoadInfo(DebtAccountId);
            CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels.ClientVM Obj_ClientVM = new CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels.ClientVM();
            Obj_ClientVM.To = Email;
            Obj_ClientVM.Subject = "Regarding :" + Obj_DebtAccount.obj_DebtAccount.ClientName + ", CRF#:" + Obj_DebtAccount.obj_DebtAccount.DebtAccountId + ", Ref#: " + Obj_DebtAccount.obj_DebtAccount.AccountNo;
            Obj_ClientVM.ListADDebtAccountAttachment = ADDebtAccountAttachment.GetDebtAccountAttachments(DebtAccountId);
            Obj_ClientVM.DebtAccountId = DebtAccountId;
            return PartialView(GetVirtualPath("_SendDBTAccountEmail"),Obj_ClientVM);

        }
        public ActionResult SendDBTAccountEmail(string DebtAccountId,string[] To,string Subject,string Message,string[] AttachedFiles,bool isSend)
        {
            try
            {
                string Files = AttachedFiles[0];
                string AllFilePaths = Files.Replace("\n", "").Replace("\r", "");
                AttachedFiles = AllFilePaths.Split(' ');
                if (To == null)
                {
                    return Json(-1);
                }
                //To = "testrhealsms@gmail.com";
                string SBODY = "";
                CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                var myuser = ADPortalUsers.GetUserForId(CU_Obj.Id);
                SBODY += "<br />";
                SBODY += "<br />";
                SBODY += "<br />";
                SBODY += myuser.SignatureLine1 + "<br />";
                SBODY += myuser.SignatureLine2 + "<br />";
                SBODY += myuser.SignatureLine3 + "<br />";
                SBODY += myuser.SignatureLine4 + "<br />" +Message;
                //string from = "testrhealsms@gmail.com";
                string from = user.Email;

                bool isSent=false;
                foreach (var email in To)
                {
                   isSent = CRFView.Adapters.Utility.sendEmail(email, from, Subject, SBODY, AttachedFiles.ToList(), isSend);

                }

                ADDebtAccountNote objADDebtAccountNote = new ADDebtAccountNote();
                objADDebtAccountNote.DebtAccountId = Convert.ToInt32(DebtAccountId);
                objADDebtAccountNote.NoteTypeId = 1;
                var emailsentto = "";
                foreach (var email in To)
                {
                    emailsentto += email + ";";
                }

                objADDebtAccountNote.Note = "EMAIL SENT TO: " + emailsentto;

                objADDebtAccountNote.Note += System.Environment.NewLine;

                objADDebtAccountNote.Note += "SUBJECT: " + Subject;


                objADDebtAccountNote.Note += System.Environment.NewLine;


                objADDebtAccountNote.Note += "MESSAGE: ";

                objADDebtAccountNote.Note += System.Environment.NewLine;
               

                objADDebtAccountNote.Note += Message;

                int debtacchistorysaved = ADDebtAccountNote.SaveDebtAccountNote(objADDebtAccountNote);
                if (isSent == true)
                {
                    return Json(true);
                }
                else
                {
                    return Json(false);
                }

            }
            catch (Exception ex)
            {
                return Json(-2);
            }
         
        }
    }
}
