﻿using CRFView.Adapters;
using CRFView.Adapters.Collections.DayEnd.PrintLetters.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Collections.DayEnd.PrintLetters
{
    public class PrintLettersController : Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            PrintLettersVM objPrintLettersVM = new PrintLettersVM();
            
            return PartialView(GetVirtualPath("_PrintLetters"), objPrintLettersVM);
        }


        //Print Letters 
        [HttpPost]
        public string LettersPrint(PrintLettersVM objPrintLettersVM)
        {
            try
            {
                if(objPrintLettersVM.RbtClientFilter == "All Clients")
                {
                    objPrintLettersVM.ClientId = "0";
                }
                else
                {
                    string[] UniqueIdDetails = objPrintLettersVM.ddlClientId.Split('-');
                    objPrintLettersVM.ClientId = UniqueIdDetails[0].Trim();
                }

                string PdfUrl = ADDebtAccountLetters.PrintLetters(objPrintLettersVM);
                if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
                {
                    string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                    string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                    return (path);
                }
            }
            catch (Exception ex)
            {
                return "error";
            }
            return "error";
        }


        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewCollections/DayEnd/PrintLetters/" + ViewName + ".cshtml";
            return path;
        }
    }
}
