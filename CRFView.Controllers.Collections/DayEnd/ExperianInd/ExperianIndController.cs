﻿using CRFView.Adapters;
using CRFView.Adapters.Collections.DayEnd.ExperianInd.ViewModels;
using System;
using System.IO;
using System.Web.Mvc;

namespace CRFView.Controllers.Collections.DayEnd.ExperianInd
{
    public class ExperianIndController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            ExperianIndVM objExperianIndVM = new ExperianIndVM();
         
           ViewBag.lblMessage = "Press OK To Extract (Individual) Credit Report File";

            return PartialView(GetVirtualPath("_ExperianInd"), objExperianIndVM);
        }


        ////Extract Individual CreditReport File
        [HttpPost]
        public string ExtractIndividualCreditReportFile(ExperianIndVM objExperianIndVM)
        {
            try
            {
                string PdfUrl = ADDebtAccountCreditReport.ExtractIndividualCreditReport();
                if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
                {
                    string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                    string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                    return (path);
                }
            }
            catch (Exception ex)
            {
                return "error";
            }
            return "error";
        }




        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewCollections/DayEnd/ExperianInd/" + ViewName + ".cshtml";
            return path;
        }
    }
}
