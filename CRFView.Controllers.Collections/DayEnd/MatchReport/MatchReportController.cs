﻿using CRFView.Adapters;
using CRFView.Adapters.Collections.DayEnd.MatchReport.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Collections.DayEnd.MatchReport
{
    public class MatchReportController : Controller
    {


        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            MatchReportVM objMatchReportVM = new MatchReportVM();

            return PartialView(GetVirtualPath("_PrintMatchReport"), objMatchReportVM);
        }


        //Print Match Report
        [HttpPost]
        public string MatchReportPrint(MatchReportVM objMatchReportVM)
        {
            try
            {
                string PdfUrl = ADvwAccountFilterList.PrintMatchReport(objMatchReportVM);
                if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
                {
                    string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                    string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                    return (path);
                }
            }
            catch (Exception ex)
            {
                return "error";
            }
            return "error";
        }


        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewCollections/DayEnd/MatchReport/" + ViewName + ".cshtml";
            return path;
        }
    }
}
