﻿using CRFView.Adapters;
using CRFView.Adapters.Collections.DayEnd.Acks.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Collections.DayEnd.Acks
{
    public class AcksController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            AcksVM objAcksVM = new AcksVM();

            return PartialView(GetVirtualPath("_PrintAcks"), objAcksVM);
        }


        //Print Acks
        [HttpPost]
        public string AcksPrint(AcksVM objAcksVM)
        {
            try
            {
                if (objAcksVM.RbtClientFilter == "All Clients")
                {
                    objAcksVM.ClientId = "0";
                }
                else
                {
                    string[] UniqueIdDetails = objAcksVM.ddlClientId.Split('-');
                    objAcksVM.ClientId = UniqueIdDetails[0].Trim();
                }

                string PdfUrl = ADvwAccountList.PrintAcks(objAcksVM);
                if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
                {
                    string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                    string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                    return (path);
                }

            }
            catch (Exception ex)
            {
                return "error";
            }
            return "error";
        }


        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewCollections/DayEnd/Acks/" + ViewName + ".cshtml";
            return path;
        }
    }
}
