﻿using CRFView.Adapters;
using CRFView.Adapters.Collections.DayEnd.ExperianCom.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Collections.DayEnd.ExperianCom
{
    public class ExperianComController : Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            ExperianComVM objExperianComVM = new ExperianComVM();

            ViewBag.lblMessage = "Press OK To Extract (Commercial) Credit Report File";

            return PartialView(GetVirtualPath("_ExperianCom"), objExperianComVM);
        }


        ////Extract Commercial CreditReport File
        [HttpPost]
        public string ExtractCommercialCreditReportFile(ExperianComVM objExperianComVM)
        {
            try
            {
                string PdfUrl = ADDebtAccountCreditReport.ExtractCommercialCreditReport();
                if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
                {
                    string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                    string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                    return (path);
                }
            }
            catch (Exception ex)
            {
                return "error";
            }
            return "error";
        }




        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewCollections/DayEnd/ExperianCom/" + ViewName + ".cshtml";
            return path;
        }
    }
}
