﻿using CRF.BLL.CRFDB.TABLES;
using CRFView.Adapters;
using CRFView.Adapters.Collections.DataEntry.DataEntry.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Collections.DataEntry.DataEntry
{
   public class DataEntryController : Controller
    {
       
 
       [HttpPost]
        public ActionResult Index()
        {
            FilterVM ObjFilterVM = new FilterVM();
            string batchId;
            if (ObjFilterVM.BatchIdList.Count() > 0)
            {
                 batchId = ObjFilterVM.BatchIdList.First().Value;
            }

            else
            {
                 batchId = 0.ToString();
            }
            List<ADvwBatchDebtAccountFilterList> lst = ADvwBatchDebtAccountFilterList.GetInitialList(batchId);
            BatchDebtAccountFilterListVM ObjVm = new BatchDebtAccountFilterListVM();
            ObjVm.ObjBatchDebtAccount = lst;
            return PartialView(GetVirtualPath("Index"), ObjVm);
        }

        //Show filter
        [HttpGet]
        public ActionResult ShowFilterModal()
        {
            FilterVM ObjVm = new FilterVM();
            return PartialView(GetVirtualPath("_Filter"), ObjVm);
        }

        //Get Filter Result
        [HttpPost]
        public ActionResult GetFilterResult(Adapters.Collections.DataEntry.DataEntry.Filter FilterObj)
        {
            List<ADvwBatchDebtAccountFilterList> lst = ADvwBatchDebtAccountFilterList.GetFilteredList(FilterObj);
            BatchDebtAccountFilterListVM ObjVm = new BatchDebtAccountFilterListVM();
            ObjVm.ObjBatchDebtAccount = lst;
            return PartialView(GetVirtualPath("_BatchDebtAccountList"), ObjVm);
        }

        //  LoadDebtAccountListByBatchId
        [HttpPost]
        public ActionResult LoadDebtAccountListByBatchId(string BatchId)
        {
            List<ADvwBatchDebtAccountFilterList> lst = ADvwBatchDebtAccountFilterList.LoadDebtAccountListByBatchId(BatchId);
            BatchDebtAccountFilterListVM ObjVm = new BatchDebtAccountFilterListVM();
            ObjVm.ObjBatchDebtAccount = lst;
            return PartialView(GetVirtualPath("_BatchDebtAccountList"), ObjVm);
        }

        //Create New Batch
        [HttpGet]
        public ActionResult CreateNewBatch()
        {
            int result = 0;
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            FilterVM ObjVm = new FilterVM();
            try
            {           
                ObjVm.ObjADBatch.BatchType = "COLL";
                ObjVm.ObjADBatch.BatchStatus = "OPEN";
                ObjVm.ObjADBatch.BatchDate = DateTime.Now;
                ObjVm.ObjADBatch.FileDate = DateTime.Now;
                ObjVm.ObjADBatch.SubmittedById = user.Id;
                ObjVm.ObjADBatch.SubmittedBy = user.UserName;

                result = ADBatch.SaveBatch(ObjVm.ObjADBatch);

                ObjVm.BatchIdList = CommonBindList.GetBatchIdList(); // Load  BatchIdList ddl for updated record
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                result = 0;
            }
 
            return PartialView(GetVirtualPath("_Filter"), ObjVm);
        }

        //Release Batch
        [HttpGet]
        public ActionResult ReleaseBatch(string BatchId)
        {
            int result = 0;
          
            FilterVM ObjVm = new FilterVM();
            try
            {
                result = ADBatch.ReleaseBatch(BatchId);

                ObjVm.BatchIdList = CommonBindList.GetBatchIdList(); // Load  BatchIdList ddl for updated record
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                result = 0;
            }

            return PartialView(GetVirtualPath("_Filter"), ObjVm);
        }

        //Delete Batch
        [HttpGet]
        public ActionResult DeleteBatch(string BatchId)
        {
            int result = 0;

            FilterVM ObjVm = new FilterVM();
            try
            {
                result = ADBatch.DeleteBatch(BatchId);

                ObjVm.BatchIdList = CommonBindList.GetBatchIdList(); // Load  BatchIdList ddl for updated record
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                result = 0;
            }

            return PartialView(GetVirtualPath("_Filter"), ObjVm);
        }

        //Show AddBatchDebtAccountModal
        [HttpGet]
        public ActionResult ShowAddBatchDebtAccountModal()
        {
            int result = 0;
            FilterVM ObjFilterVM = new FilterVM();
            BatchDebtAccountVM ObjBatchDebtAccountVM = new BatchDebtAccountVM();
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            
            string batchId;
            if (ObjFilterVM.BatchIdList.Count() > 0)
            {
                batchId = ObjFilterVM.BatchIdList.First().Value;
            }

            else
            {
                //batchId = 0.ToString();
                ObjFilterVM.ObjADBatch.BatchType = "COLL";
                ObjFilterVM.ObjADBatch.BatchStatus = "OPEN";
                ObjFilterVM.ObjADBatch.BatchDate = DateTime.Now;
                ObjFilterVM.ObjADBatch.FileDate = DateTime.Now;
                ObjFilterVM.ObjADBatch.SubmittedById = user.Id;
                ObjFilterVM.ObjADBatch.SubmittedBy = user.UserName;

                result = ADBatch.SaveBatch(ObjFilterVM.ObjADBatch);

                ObjBatchDebtAccountVM.BatchIdList = CommonBindList.GetBatchIdList();
            }
            //ObjBatchDebtAccountVM.obj_DebtAccountAttachment = ADDebtAccountAttachment.GetBatchDebtAccountAttachments(0.ToString());
            ObjBatchDebtAccountVM.BatchDebtAccountId = 0.ToString();
            return PartialView(GetVirtualPath("_AddBatchDebtAccountInfo"), ObjBatchDebtAccountVM);
        }

        //Get  ServiceCodesDetails of client
        public ActionResult GetServiceCodesddlDetails(BatchDebtAccountVM ObjBatchDebtAccountVM)
        {
            ObjBatchDebtAccountVM.ddlServiceCodes = ObjBatchDebtAccountVM.UniqueId;
            string UniqueId = ObjBatchDebtAccountVM.UniqueId;

            string[] UniqueIdDetails = UniqueId.Split('-');
            ObjBatchDebtAccountVM.ClientId = UniqueIdDetails[0].Trim();
            ObjBatchDebtAccountVM.ContractId = UniqueIdDetails[1].Trim();

            ObjBatchDebtAccountVM.obj_Client = ADClient.GetClientDetails(Convert.ToInt32(ObjBatchDebtAccountVM.ClientId));
            ObjBatchDebtAccountVM.lblClientInfo = ObjBatchDebtAccountVM.obj_Client.ContactName + " - " + ObjBatchDebtAccountVM.obj_Client.AddressLine1 + " , " + ObjBatchDebtAccountVM.obj_Client.City + " , " + ObjBatchDebtAccountVM.obj_Client.State + " " + ObjBatchDebtAccountVM.obj_Client.PostalCode;

            ObjBatchDebtAccountVM.obj_ClientCollectionInfo = ADClientCollectionInfo.ReadCollectionInfo(ObjBatchDebtAccountVM.ClientId);
            ObjBatchDebtAccountVM.lblMiscVar = ObjBatchDebtAccountVM.obj_ClientCollectionInfo.NBVar;
            ObjBatchDebtAccountVM.BranchBox = ObjBatchDebtAccountVM.obj_ClientCollectionInfo.BranchBox;

            ObjBatchDebtAccountVM.obj_ClientContract = ADClientContract.ReadClientContractType(Convert.ToInt32(ObjBatchDebtAccountVM.ContractId));
            ObjBatchDebtAccountVM.IsIntlClient = ObjBatchDebtAccountVM.obj_ClientContract.IsIntlClient;
            ObjBatchDebtAccountVM.IsTenDayContactPlan = ObjBatchDebtAccountVM.obj_ClientContract.IsTenDayContactPlan;

            if(ObjBatchDebtAccountVM.IsIntlClient == true)
            {
                ViewBag.divExchRate = "Show divExchRate";
            }

            if (ObjBatchDebtAccountVM.IsTenDayContactPlan == false)
            {
                ViewBag.divTenDayContactPlan = "Hide divTenDayContactPlan";
            } 

            return PartialView(GetVirtualPath("_AddBatchDebtAccountInfo"), ObjBatchDebtAccountVM);
        }

        //Save BatchDebtAccount Details
        [HttpPost]
        public ActionResult SaveBatchDebtAccountInfo(BatchDebtAccountVM Obj_BatchDebtAccountVM)
        {
            int result = 0;
            try
            {
                BatchDebtAccountVM ObjBatchDebtAccountVM = new BatchDebtAccountVM();
                string UniqueId = Obj_BatchDebtAccountVM.ddlServiceCodes;
                string[] UniqueIdDetails = UniqueId.Split('-');
                Obj_BatchDebtAccountVM.ClientId = UniqueIdDetails[0].Trim();
                Obj_BatchDebtAccountVM.ContractId = UniqueIdDetails[1].Trim();
               

                Obj_BatchDebtAccountVM.obj_ClientCollectionInfo = ADClientCollectionInfo.ReadCollectionInfo(Obj_BatchDebtAccountVM.ClientId);
                Obj_BatchDebtAccountVM.obj_BatchDebtAccount.ClientId =Convert.ToInt32(Obj_BatchDebtAccountVM.ClientId);
                Obj_BatchDebtAccountVM.obj_BatchDebtAccount.ContractId =Convert.ToInt32(Obj_BatchDebtAccountVM.ContractId);
                Obj_BatchDebtAccountVM.obj_BatchDebtAccount.BatchId = Convert.ToInt32(Obj_BatchDebtAccountVM.ddlBatchId);
                Obj_BatchDebtAccountVM.obj_BatchDebtAccount.LocationId = -1;
                Obj_BatchDebtAccountVM.obj_BatchDebtAccount.CollectionStatusId = -1;
                Obj_BatchDebtAccountVM.obj_BatchDebtAccount.CommRateId = -1;

                Obj_BatchDebtAccountVM.obj_ClientContract = ADClientContract.ReadClientContractType(Convert.ToInt32(ObjBatchDebtAccountVM.ContractId));
                Obj_BatchDebtAccountVM.obj_BatchDebtAccount.ExchangeRate = ObjBatchDebtAccountVM.obj_BatchDebtAccount.ExchangeRate;
                Obj_BatchDebtAccountVM.obj_BatchDebtAccount.BatchDebtAccountId = Convert.ToInt32(Obj_BatchDebtAccountVM.BatchDebtAccountId);

                if (Obj_BatchDebtAccountVM.obj_ClientContract.IsIntlClient == false || ObjBatchDebtAccountVM.obj_BatchDebtAccount.ExchangeRate == 0)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.ExchangeRate = 1;
                }

                Obj_BatchDebtAccountVM.obj_BatchDebtAccount.CommRateId = 0;
                if (Obj_BatchDebtAccountVM.chkRate == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.CommRateId = Convert.ToInt32(Obj_BatchDebtAccountVM.ddlCommRateId);
                }
        
     
                if (Obj_BatchDebtAccountVM.chkEstAudit == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsEstAudit = true;
                }
                else
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsEstAudit = false;
                }

                if (Obj_BatchDebtAccountVM.chkRapidRebate == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsRapidRebate = true;
                }
                else
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsRapidRebate = false;
                }

                if (Obj_BatchDebtAccountVM.chkInternational == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsIntlAcct = true;
                }
                else
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsIntlAcct = false;
                }

                if (Obj_BatchDebtAccountVM.chkSkip == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsSkip = true;
                }
                else
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsSkip = false;
                }

                if (Obj_BatchDebtAccountVM.chkJudgment == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsJudgmentAcct = true;
                }
                else
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsJudgmentAcct = false;
                }

                if (Obj_BatchDebtAccountVM.chkSecondary == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsSecondary = true;
                }
                else
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsSecondary = false;
                }

                if (Obj_BatchDebtAccountVM.chkNoCollectionFee == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsNoCollectFee = true;
                }
                else
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsNoCollectFee = false;
                }

                if (Obj_BatchDebtAccountVM.chkTenDayContactPlan == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsTenDayContactPlan = true;
                }
                else
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsTenDayContactPlan = false;
                }
                
                result = ADBatchDebtAccount.SaveBatchDebtAccount(Obj_BatchDebtAccountVM.obj_BatchDebtAccount);
                //ObjBatchDebtAccountVM.BatchDebtAccountIds = ADBatchDebtAccount.GetLastBatchDebtAccountId(Obj_BatchDebtAccountVM.obj_BatchDebtAccount);
                //Session[" ObjBatchDebtAccountVM.BatchDebtAccountIds"] = ObjBatchDebtAccountVM.BatchDebtAccountIds;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                result = 0;
            }

            return Json(result);
        }

        public BatchDebtAccount SaveBatchDebtAccountInfo1(BatchDebtAccountVM Obj_BatchDebtAccountVM)
        {
            int result = 0;
            BatchDebtAccountVM ObjBatchDebtAccountVM = new BatchDebtAccountVM();
            BatchDebtAccount objBatchDebtAccount = new BatchDebtAccount();
            try
            {
                string UniqueId = Obj_BatchDebtAccountVM.ddlServiceCodes;
                string[] UniqueIdDetails = UniqueId.Split('-');
                Obj_BatchDebtAccountVM.ClientId = UniqueIdDetails[0].Trim();
                Obj_BatchDebtAccountVM.ContractId = UniqueIdDetails[1].Trim();


                Obj_BatchDebtAccountVM.obj_ClientCollectionInfo = ADClientCollectionInfo.ReadCollectionInfo(Obj_BatchDebtAccountVM.ClientId);
                Obj_BatchDebtAccountVM.obj_BatchDebtAccount.ClientId = Convert.ToInt32(Obj_BatchDebtAccountVM.ClientId);
                Obj_BatchDebtAccountVM.obj_BatchDebtAccount.ContractId = Convert.ToInt32(Obj_BatchDebtAccountVM.ContractId);
                Obj_BatchDebtAccountVM.obj_BatchDebtAccount.BatchId = Convert.ToInt32(Obj_BatchDebtAccountVM.ddlBatchId);
                Obj_BatchDebtAccountVM.obj_BatchDebtAccount.LocationId = -1;
                Obj_BatchDebtAccountVM.obj_BatchDebtAccount.CollectionStatusId = -1;
                Obj_BatchDebtAccountVM.obj_BatchDebtAccount.CommRateId = -1;

                Obj_BatchDebtAccountVM.obj_ClientContract = ADClientContract.ReadClientContractType(Convert.ToInt32(ObjBatchDebtAccountVM.ContractId));
                Obj_BatchDebtAccountVM.obj_BatchDebtAccount.ExchangeRate = ObjBatchDebtAccountVM.obj_BatchDebtAccount.ExchangeRate;
                Obj_BatchDebtAccountVM.obj_BatchDebtAccount.BatchDebtAccountId = Convert.ToInt32(Obj_BatchDebtAccountVM.BatchDebtAccountId);
                if (Obj_BatchDebtAccountVM.obj_ClientContract.IsIntlClient == false || ObjBatchDebtAccountVM.obj_BatchDebtAccount.ExchangeRate == 0)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.ExchangeRate = 1;
                }

                Obj_BatchDebtAccountVM.obj_BatchDebtAccount.CommRateId = 0;
                if (Obj_BatchDebtAccountVM.chkRate == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.CommRateId = Convert.ToInt32(Obj_BatchDebtAccountVM.ddlCommRateId);
                }


                if (Obj_BatchDebtAccountVM.chkEstAudit == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsEstAudit = true;
                }
                else
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsEstAudit = false;
                }

                if (Obj_BatchDebtAccountVM.chkRapidRebate == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsRapidRebate = true;
                }
                else
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsRapidRebate = false;
                }

                if (Obj_BatchDebtAccountVM.chkInternational == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsIntlAcct = true;
                }
                else
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsIntlAcct = false;
                }

                if (Obj_BatchDebtAccountVM.chkSkip == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsSkip = true;
                }
                else
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsSkip = false;
                }

                if (Obj_BatchDebtAccountVM.chkJudgment == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsJudgmentAcct = true;
                }
                else
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsJudgmentAcct = false;
                }

                if (Obj_BatchDebtAccountVM.chkSecondary == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsSecondary = true;
                }
                else
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsSecondary = false;
                }

                if (Obj_BatchDebtAccountVM.chkNoCollectionFee == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsNoCollectFee = true;
                }
                else
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsNoCollectFee = false;
                }

                if (Obj_BatchDebtAccountVM.chkTenDayContactPlan == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsTenDayContactPlan = true;
                }
                else
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccount.IsTenDayContactPlan = false;
                }

                //ADBatchDebtAccount.SaveBatchDebtAccount(Obj_BatchDebtAccountVM.obj_BatchDebtAccount, 0);
                 objBatchDebtAccount = ADBatchDebtAccount.SaveBatchDebtAccount(Obj_BatchDebtAccountVM.obj_BatchDebtAccount,0);
                //ObjBatchDebtAccountVM.BatchDebtAccountIds = ADBatchDebtAccount.GetLastBatchDebtAccountId(Obj_BatchDebtAccountVM.obj_BatchDebtAccount);
                //Session[" ObjBatchDebtAccountVM.BatchDebtAccountIds"] = ObjBatchDebtAccountVM.BatchDebtAccountIds;
                //Session[" ObjBatchDebtAccountVM.BatchDebtAccountIds"] = ObjBatchDebtAccountVM.BatchDebtAccountIds;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                result = 0;
            }

            return objBatchDebtAccount;
        }

        //Get  ServiceCodesDetails of client
        public ActionResult GetServiceCodesEditddlDetails(BatchDebtAccountVM ObjBatchDebtAccountVM)
        {
          
            string UniqueId = ObjBatchDebtAccountVM.ddlServiceCodes;
            string[] UniqueIdDetails = UniqueId.Split('-');
            ObjBatchDebtAccountVM.ClientId = UniqueIdDetails[0].Trim();
            ObjBatchDebtAccountVM.ContractId = UniqueIdDetails[1].Trim();

            ObjBatchDebtAccountVM.obj_Client = ADClient.GetClientDetails(Convert.ToInt32(ObjBatchDebtAccountVM.ClientId));
            ObjBatchDebtAccountVM.lblClientInfo = ObjBatchDebtAccountVM.obj_Client.ContactName + " - " + ObjBatchDebtAccountVM.obj_Client.AddressLine1 + " , " + ObjBatchDebtAccountVM.obj_Client.City + " , " + ObjBatchDebtAccountVM.obj_Client.State + " " + ObjBatchDebtAccountVM.obj_Client.PostalCode;

            ObjBatchDebtAccountVM.obj_ClientCollectionInfo = ADClientCollectionInfo.ReadCollectionInfo(ObjBatchDebtAccountVM.ClientId);
            ObjBatchDebtAccountVM.lblMiscVar = ObjBatchDebtAccountVM.obj_ClientCollectionInfo.NBVar;
            ObjBatchDebtAccountVM.BranchBox = ObjBatchDebtAccountVM.obj_ClientCollectionInfo.BranchBox;

            ObjBatchDebtAccountVM.obj_ClientContract = ADClientContract.ReadClientContractType(Convert.ToInt32(ObjBatchDebtAccountVM.ContractId));
            ObjBatchDebtAccountVM.IsIntlClient = ObjBatchDebtAccountVM.obj_ClientContract.IsIntlClient;
            ObjBatchDebtAccountVM.IsTenDayContactPlan = ObjBatchDebtAccountVM.obj_ClientContract.IsTenDayContactPlan;

            if (ObjBatchDebtAccountVM.IsIntlClient == true)
            {
                ViewBag.divExchRate = "Show divExchRate";
            }

            if (ObjBatchDebtAccountVM.IsTenDayContactPlan == false)
            {
                ViewBag.divTenDayContactPlan = "Hide divTenDayContactPlan";
            }

            ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.BatchId =Convert.ToInt32(ObjBatchDebtAccountVM.BatchId);

            ObjBatchDebtAccountVM.obj_DebtAccountAttachment = ADDebtAccountAttachment.GetBatchDebtAccountAttachments(ObjBatchDebtAccountVM.BatchDebtAccountId);

            return PartialView(GetVirtualPath("_EditBatchDebtAccountInfo"), ObjBatchDebtAccountVM);
        }

        //Get BatchDebtAccount Details
        [HttpPost]
        public ActionResult GetBatchDebtAccountDetails(string BatchDebtAccountId,bool IsEdit)
        {
            BatchDebtAccountVM ObjBatchDebtAccountVM = new BatchDebtAccountVM();
  
            Session["IsEdit"] = IsEdit;
            if (IsEdit == false)
            {
                ObjBatchDebtAccountVM.obj_BatchDebtAccount = ADBatchDebtAccount.ReadBatchDebtAccounts(Convert.ToInt32(BatchDebtAccountId));
                ObjBatchDebtAccountVM.obj_BatchDebtAccount.PhoneNo = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(ObjBatchDebtAccountVM.obj_BatchDebtAccount.PhoneNo);
                ObjBatchDebtAccountVM.obj_BatchDebtAccount.PhoneNo2 = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(ObjBatchDebtAccountVM.obj_BatchDebtAccount.PhoneNo2);
                ObjBatchDebtAccountVM.obj_BatchDebtAccount.Cell = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(ObjBatchDebtAccountVM.obj_BatchDebtAccount.Cell);
                ObjBatchDebtAccountVM.obj_BatchDebtAccount.PhoneNo_2 = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(ObjBatchDebtAccountVM.obj_BatchDebtAccount.PhoneNo);
                ObjBatchDebtAccountVM.obj_BatchDebtAccount.Cell_2 = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(ObjBatchDebtAccountVM.obj_BatchDebtAccount.PhoneNo2);
                ObjBatchDebtAccountVM.obj_BatchDebtAccount.PhoneNo_3 = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(ObjBatchDebtAccountVM.obj_BatchDebtAccount.Cell);
                ObjBatchDebtAccountVM.obj_BatchDebtAccount.Cell_3 = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(ObjBatchDebtAccountVM.obj_BatchDebtAccount.PhoneNo);
                ObjBatchDebtAccountVM.obj_BatchDebtAccount.Fax = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(ObjBatchDebtAccountVM.obj_BatchDebtAccount.Fax);
                ObjBatchDebtAccountVM.obj_BatchDebtAccount.TotAsgAmt = ObjBatchDebtAccountVM.obj_BatchDebtAccount.TotAsgAmt;


                ObjBatchDebtAccountVM.BatchDebtAccountId = BatchDebtAccountId;
                ObjBatchDebtAccountVM.BatchId = Convert.ToString(ObjBatchDebtAccountVM.obj_BatchDebtAccount.BatchId);

                ObjBatchDebtAccountVM.ddlServiceCodes = ObjBatchDebtAccountVM.obj_BatchDebtAccount.ClientId + "-" + ObjBatchDebtAccountVM.obj_BatchDebtAccount.ContractId;

                ObjBatchDebtAccountVM.obj_Client = ADClient.GetClientDetails(ObjBatchDebtAccountVM.obj_BatchDebtAccount.ClientId);
                ObjBatchDebtAccountVM.lblClientInfo = ObjBatchDebtAccountVM.obj_Client.ContactName + " - " + ObjBatchDebtAccountVM.obj_Client.AddressLine1 + " , " + ObjBatchDebtAccountVM.obj_Client.City + " , " + ObjBatchDebtAccountVM.obj_Client.State + " " + ObjBatchDebtAccountVM.obj_Client.PostalCode;

                ObjBatchDebtAccountVM.obj_ClientCollectionInfo = ADClientCollectionInfo.ReadCollectionInfo(Convert.ToString(ObjBatchDebtAccountVM.obj_BatchDebtAccount.ClientId));
                ObjBatchDebtAccountVM.lblMiscVar = ObjBatchDebtAccountVM.obj_ClientCollectionInfo.NBVar;
                ObjBatchDebtAccountVM.BranchBox = ObjBatchDebtAccountVM.obj_ClientCollectionInfo.BranchBox;

                ObjBatchDebtAccountVM.ddlCommRateId = Convert.ToString(ObjBatchDebtAccountVM.obj_BatchDebtAccount.CommRateId);

                if (ObjBatchDebtAccountVM.obj_BatchDebtAccount.CommRateId == 0)
                {
                    ObjBatchDebtAccountVM.chkRate = false;
                }
                else
                {
                    ObjBatchDebtAccountVM.chkRate = true;
                }

                if (ObjBatchDebtAccountVM.obj_BatchDebtAccount.IsEstAudit == true)
                {
                    ObjBatchDebtAccountVM.chkEstAudit = true;
                }
                else
                {
                    ObjBatchDebtAccountVM.chkEstAudit = false;
                }

                if (ObjBatchDebtAccountVM.obj_BatchDebtAccount.IsRapidRebate == true)
                {
                    ObjBatchDebtAccountVM.chkRapidRebate = true;
                }
                else
                {
                    ObjBatchDebtAccountVM.chkRapidRebate = false;
                }

                if (ObjBatchDebtAccountVM.obj_BatchDebtAccount.IsIntlAcct == true)
                {
                    ObjBatchDebtAccountVM.chkInternational = true;
                }
                else
                {
                    ObjBatchDebtAccountVM.chkInternational = false;
                }

                if (ObjBatchDebtAccountVM.obj_BatchDebtAccount.IsSkip == true)
                {
                    ObjBatchDebtAccountVM.chkSkip = true;
                }
                else
                {
                    ObjBatchDebtAccountVM.chkSkip = false;
                }

                if (ObjBatchDebtAccountVM.obj_BatchDebtAccount.IsJudgmentAcct == true)
                {
                    ObjBatchDebtAccountVM.chkJudgment = true;
                }
                else
                {
                    ObjBatchDebtAccountVM.chkJudgment = false;
                }

                if (ObjBatchDebtAccountVM.obj_BatchDebtAccount.IsSecondary == true)
                {
                    ObjBatchDebtAccountVM.chkSecondary = true;
                }
                else
                {
                    ObjBatchDebtAccountVM.chkSecondary = false;
                }

                if (ObjBatchDebtAccountVM.obj_BatchDebtAccount.IsNoCollectFee == true)
                {
                    ObjBatchDebtAccountVM.chkNoCollectionFee = true;
                }
                else
                {
                    ObjBatchDebtAccountVM.chkNoCollectionFee = false;
                }

                if (ObjBatchDebtAccountVM.obj_BatchDebtAccount.IsTenDayContactPlan == true)
                {
                    ObjBatchDebtAccountVM.chkTenDayContactPlan = true;
                }
                else
                {
                    ObjBatchDebtAccountVM.chkTenDayContactPlan = false;
                }

                ObjBatchDebtAccountVM.obj_DebtAccountAttachment = ADDebtAccountAttachment.GetBatchDebtAccountAttachments(BatchDebtAccountId);
                return PartialView(GetVirtualPath("_AddBatchDebtAccountInfo"), ObjBatchDebtAccountVM);
            }
            else
            {
                ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit = ADBatchDebtAccount.ReadBatchDebtAccounts(Convert.ToInt32(BatchDebtAccountId));
                ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.PhoneNo = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.PhoneNo);
                ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.PhoneNo2 = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.PhoneNo2);
                ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.Cell = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.Cell);
                ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.PhoneNo_2 = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.PhoneNo);
                ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.Cell_2 = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.PhoneNo2);
                ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.PhoneNo_3 = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.Cell);
                ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.Cell_3 = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.PhoneNo);
                ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.Fax = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.Fax);

                ObjBatchDebtAccountVM.BatchDebtAccountId = BatchDebtAccountId;
                ObjBatchDebtAccountVM.BatchId = Convert.ToString(ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.BatchId);

            ObjBatchDebtAccountVM.ddlServiceCodes = ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.ClientId + "-" + ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.ContractId;

            ObjBatchDebtAccountVM.obj_Client = ADClient.GetClientDetails(ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.ClientId);
            ObjBatchDebtAccountVM.lblClientInfo = ObjBatchDebtAccountVM.obj_Client.ContactName + " - " + ObjBatchDebtAccountVM.obj_Client.AddressLine1 + " , " + ObjBatchDebtAccountVM.obj_Client.City + " , " + ObjBatchDebtAccountVM.obj_Client.State + " " + ObjBatchDebtAccountVM.obj_Client.PostalCode;

            ObjBatchDebtAccountVM.obj_ClientCollectionInfo = ADClientCollectionInfo.ReadCollectionInfo(Convert.ToString(ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.ClientId));
            ObjBatchDebtAccountVM.lblMiscVar = ObjBatchDebtAccountVM.obj_ClientCollectionInfo.NBVar;
            ObjBatchDebtAccountVM.BranchBox = ObjBatchDebtAccountVM.obj_ClientCollectionInfo.BranchBox;

            ObjBatchDebtAccountVM.ddlCommRateId =Convert.ToString(ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.CommRateId);

            if (ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.CommRateId == 0)
            {
                ObjBatchDebtAccountVM.chkRate = false;
            }
            else
            {
                ObjBatchDebtAccountVM.chkRate = true;
            }

            if (ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.IsEstAudit  == true)
            {
                ObjBatchDebtAccountVM.chkEstAudit = true;
            }
            else
            {
                ObjBatchDebtAccountVM.chkEstAudit = false;
            }

            if (ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.IsRapidRebate == true)
            {
                ObjBatchDebtAccountVM.chkRapidRebate = true;
            }
            else
            {
                ObjBatchDebtAccountVM.chkRapidRebate = false;
            }

            if (ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.IsIntlAcct == true)
            {
                ObjBatchDebtAccountVM.chkInternational= true;
            }
            else
            {
                ObjBatchDebtAccountVM.chkInternational = false;
            }

            if (ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.IsSkip == true)
            {
                ObjBatchDebtAccountVM.chkSkip = true;
            }
            else
            {
                ObjBatchDebtAccountVM.chkSkip = false;
            }

            if (ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.IsJudgmentAcct == true)
            {
                ObjBatchDebtAccountVM.chkJudgment = true;
            }
            else
            {
                ObjBatchDebtAccountVM.chkJudgment = false;
            }

            if (ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.IsSecondary == true)
            {
                ObjBatchDebtAccountVM.chkSecondary = true;
            }
            else
            {
                ObjBatchDebtAccountVM.chkSecondary = false;
            }

            if (ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.IsNoCollectFee == true)
            {
                ObjBatchDebtAccountVM.chkNoCollectionFee = true;
            }
            else
            {
                ObjBatchDebtAccountVM.chkNoCollectionFee = false;
            }

            if (ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.IsTenDayContactPlan == true)
            {
                ObjBatchDebtAccountVM.chkTenDayContactPlan = true;
            }
            else
            {
                ObjBatchDebtAccountVM.chkTenDayContactPlan = false;
            }

                ObjBatchDebtAccountVM.obj_DebtAccountAttachment = ADDebtAccountAttachment.GetBatchDebtAccountAttachments(BatchDebtAccountId);
                return PartialView(GetVirtualPath("_EditBatchDebtAccountInfo"), ObjBatchDebtAccountVM);
            }
            
        }

        //Update BatchDebtAccount Details
        [HttpPost]
        public ActionResult UpdateBatchDebtAccountInfo(BatchDebtAccountVM Obj_BatchDebtAccountVM)
        {
            int result = 0;
            try
            {
                BatchDebtAccountVM ObjBatchDebtAccountVM = new BatchDebtAccountVM();
                //Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit = ADBatchDebtAccount.ReadBatchDebtAccounts(Convert.ToInt32(Obj_BatchDebtAccountVM.BatchDebtAccountId));
                Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit.BatchDebtAccountId= Convert.ToInt32(Obj_BatchDebtAccountVM.BatchDebtAccountId);

                string UniqueId = Obj_BatchDebtAccountVM.ddlServiceCodes;
                string[] UniqueIdDetails = UniqueId.Split('-');
                Obj_BatchDebtAccountVM.ClientId = UniqueIdDetails[0].Trim();
                Obj_BatchDebtAccountVM.ContractId = UniqueIdDetails[1].Trim();
                Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit.ClientId = Convert.ToInt32(Obj_BatchDebtAccountVM.ClientId);
                Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit.ContractId = Convert.ToInt32(Obj_BatchDebtAccountVM.ContractId);

                Obj_BatchDebtAccountVM.obj_ClientContract = ADClientContract.ReadClientContractType(Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit.ContractId);
                //Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit.ExchangeRate = ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.ExchangeRate;
                if (Obj_BatchDebtAccountVM.obj_ClientContract.IsIntlClient == false || ObjBatchDebtAccountVM.obj_BatchDebtAccountEdit.ExchangeRate == 0)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit.ExchangeRate = 1;
                }

                Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit.CommRateId = 0;
                if (Obj_BatchDebtAccountVM.chkRate == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit.CommRateId = Convert.ToInt32(Obj_BatchDebtAccountVM.ddlCommRateId);
                }

                if (Obj_BatchDebtAccountVM.chkEstAudit == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit.IsEstAudit = true;
                }
                else
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit.IsEstAudit = false;
                }

                if (Obj_BatchDebtAccountVM.chkRapidRebate == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit.IsRapidRebate = true;
                }
                else
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit.IsRapidRebate = false;
                }

                if (Obj_BatchDebtAccountVM.chkInternational == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit.IsIntlAcct = true;
                }
                else
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit.IsIntlAcct = false;
                }

                if (Obj_BatchDebtAccountVM.chkSkip == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit.IsSkip = true;
                }
                else
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit.IsSkip = false;
                }

                if (Obj_BatchDebtAccountVM.chkJudgment == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit.IsJudgmentAcct = true;
                }
                else
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit.IsJudgmentAcct = false;
                }

                if (Obj_BatchDebtAccountVM.chkSecondary == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit.IsSecondary = true;
                }
                else
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit.IsSecondary = false;
                }

                if (Obj_BatchDebtAccountVM.chkNoCollectionFee == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit.IsNoCollectFee = true;
                }
                else
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit.IsNoCollectFee = false;
                }

                if (Obj_BatchDebtAccountVM.chkTenDayContactPlan == true)
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit.IsTenDayContactPlan = true;
                }
                else
                {
                    Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit.IsTenDayContactPlan = false;
                }

                result = ADBatchDebtAccount.SaveBatchDebtAccount(Obj_BatchDebtAccountVM.obj_BatchDebtAccountEdit);

            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                result = 0;
            }

            return Json(result);
        }


        //Add New Document
        [HttpPost]
        public ActionResult AddDocuments(BatchDebtAccountVM Obj_BatchDebtAccountVM,string BatchDebtAccountId)
        {
            DocumentVM objDocumentVM = new DocumentVM();
            BatchDebtAccount objBatchDebtAccount = new BatchDebtAccount();
            objBatchDebtAccount = SaveBatchDebtAccountInfo1(Obj_BatchDebtAccountVM);


            objDocumentVM.BatchDebtAccountId = objBatchDebtAccount.BatchDebtAccountId.ToString();
          
            //objDocumentVM.BatchDebtAccountId = 185776.ToString();
            Session["BatchDebtAccId"] = objDocumentVM.BatchDebtAccountId;
            //BatchDebtAccountVM ObjBatchDebtAccountVM = new BatchDebtAccountVM();
            objDocumentVM.IsEdit = false;


            return PartialView(GetVirtualPath("_AddNewDocument"), objDocumentVM);
        }

        public ActionResult AddDocument(string BatchDebtAccountId)
        {
            
            DocumentVM objDocumentVM = new DocumentVM();

            objDocumentVM.BatchDebtAccountId = BatchDebtAccountId;
            BatchDebtAccountVM ObjBatchDebtAccountVM = new BatchDebtAccountVM();
            Session["BatchDebtAccId"] = objDocumentVM.BatchDebtAccountId;
            objDocumentVM.IsEdit = true;
            return PartialView(GetVirtualPath("_AddNewDocument"), objDocumentVM);
        }

        //Save File Details
        [HttpPost]
        public ActionResult SaveDocument()
        {
            try
            {
                var file = Request.Files[0];
                var fileName = Path.GetFileName(file.FileName);
                var path = Path.Combine(Server.MapPath("~/Output/UploadedFiles"), fileName);
                file.SaveAs(path);
                Session["path"] = path;
                return Json(true);
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return Json(false);
            }
        }

        //Save Document Details
        [HttpPost]
        public ActionResult SaveDocumentInputs(DocumentVM Obj_DocumentVM)
        {
            int result = 0;
            try
            {
                string path = Convert.ToString(Session["path"]);

                if (!String.IsNullOrEmpty(path))
                {
                    byte[] bytes = System.IO.File.ReadAllBytes(path);
                    Obj_DocumentVM.ObjDebtAccountAttachment.DocumentImage = bytes;
                    Obj_DocumentVM.ObjDebtAccountAttachment.FileName = path.Substring(path.LastIndexOf("\\") + 1);
                }
                var BatchDebtAccountId = Session["BatchDebtAccId"];
                Obj_DocumentVM.ObjDebtAccountAttachment.BatchDebtAccountId = Convert.ToInt32(BatchDebtAccountId);
                Obj_DocumentVM.ObjDebtAccountAttachment.DocumentType = Obj_DocumentVM.ddlDocumentType;
                Obj_DocumentVM.ObjDebtAccountAttachment.DebtAccountId = 0;
                Obj_DocumentVM.ObjDebtAccountAttachment.Id = Convert.ToInt32(Obj_DocumentVM.Id);

                result = ADDebtAccountAttachment.SaveDebtAccountAttachment(Obj_DocumentVM.ObjDebtAccountAttachment);
                Session["path"] = null;

                return Json(result);
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                result = -1;
                return Json(result);
            }

        }

        //Update Document Details
        [HttpPost]
        public ActionResult UpdateDocumentInputs(DocumentVM Obj_DocumentVM)
        {
            int result = 0;
            try
            {
                string path = Convert.ToString(Session["path"]);

                if (!String.IsNullOrEmpty(path))
                {
                    byte[] bytes = System.IO.File.ReadAllBytes(path);
                    Obj_DocumentVM.ObjDebtAccountAttachmentEdit.DocumentImage = bytes;
                    Obj_DocumentVM.ObjDebtAccountAttachmentEdit.FileName = path.Substring(path.LastIndexOf("\\") + 1);
                }

                Obj_DocumentVM.ObjDebtAccountAttachmentEdit.BatchDebtAccountId = Convert.ToInt32(Obj_DocumentVM.BatchDebtAccountId);
                Obj_DocumentVM.ObjDebtAccountAttachmentEdit.DocumentType = Obj_DocumentVM.ddlDocumentType;
                Obj_DocumentVM.ObjDebtAccountAttachmentEdit.DebtAccountId = 0;
                Obj_DocumentVM.ObjDebtAccountAttachmentEdit.Id = Convert.ToInt32(Obj_DocumentVM.Id);

                result = ADDebtAccountAttachment.SaveDebtAccountAttachment(Obj_DocumentVM.ObjDebtAccountAttachmentEdit);
                Session["path"] = null;

                return Json(result);
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                result = -1;
                return Json(result);
            }

        }

        //Get Document Details
        [HttpPost]
        public ActionResult GetDocumentDetails(string DebtDocumentId, string BatchDebtAccountId)
        {
            DocumentVM obj = new DocumentVM();
            obj.ObjDebtAccountAttachmentEdit = ADDebtAccountAttachment.ReadDebtAccountAttachment(Convert.ToInt32(DebtDocumentId));
            obj.Id = Convert.ToString(obj.ObjDebtAccountAttachmentEdit.Id);
            obj.BatchDebtAccountId = Convert.ToString(obj.ObjDebtAccountAttachmentEdit.BatchDebtAccountId);
            obj.ddlDocumentType = obj.ObjDebtAccountAttachmentEdit.DocumentType;
            obj.IsEdit = Convert.ToBoolean(Session["IsEdit"]);
            return PartialView(GetVirtualPath("_EditDocument"), obj); 
        }

        //Delete Document
        [HttpPost]
        public ActionResult DeleteDocument(string Id)
        {
            var result = ADDebtAccountAttachment.DeleteDocument(Id);
            return Json(result);
        }

        //View Document
        [HttpPost]
        public string ViewDocument(string Id)
        {
            var Attachment = ADDebtAccountAttachment.ReadDebtAccountAttachment(Convert.ToInt32(Id));
            var path = Path.Combine(Server.MapPath("~/Output/DownloadableFiles"), Attachment.FileName);
            ADDebtAccountAttachment.SaveFileToBeViewed(Attachment.DocumentImage, path);
            byte[] fileBytes = Attachment.DocumentImage;
            string fileName = path.Substring(path.LastIndexOf("\\") + 1);
            //fileName = HttpUtility.UrlEncode(fileName);
            //fileName = HttpUtility.UrlPathEncode(fileName); //Remove space with % 20
            fileName = Uri.EscapeDataString(fileName); //Remove # with %23 , space with %20

            if (path != null && path != "" && path != "Error")
            {
                //string filepath = Path.Combine(Url.Content("~"), "Output/DownloadableFiles/", System.IO.Path.GetFileName(path));
                string filepath = Path.Combine(Url.Content("~"), "Output/DownloadableFiles/", fileName);

                return (filepath);
            }
            return "error";
        }

        //Delete 
        [HttpPost]
        public ActionResult DeleteBatchDebtAccount(int BatchDebtAccountId)
        {
            int result = 0;
            FilterVM ObjFilterVM = new FilterVM();
            string batchId ="";
            if (ObjFilterVM.BatchIdList.Count() > 0)
            {
                batchId = ObjFilterVM.BatchIdList.First().Value;
            }
            result = ADBatchDebtAccount.DeleteBatchDebtAccount(BatchDebtAccountId, batchId);
            return Json(result);
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewCollections/DataEntry/DataEntry/" + ViewName + ".cshtml";
            return path;
        }
    }
}
