﻿using CRFView.Adapters;
using CRFView.Adapters.Collections.DataEntry.PostBatches.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Collections.DataEntry.PostBatches
{
    public class PostBatchesController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            PostBatchesVM objPostBatchesVM = new PostBatchesVM();

            DataTable dtBatchForPostBatches = ADBatch.GetBatchForBatchPosts();
            
            if (dtBatchForPostBatches.Rows.Count < 1)
            {
                ViewBag.lblMessage = "No Batches to be Posted";
                ViewBag.btnOK = "Hide OK";
            }
            else
            {
                ViewBag.lblMessage = "(" + dtBatchForPostBatches.Rows.Count + ") Internal Batches to Be Posted";
                ViewBag.lblMessage += Environment.NewLine;
                ViewBag.lblMessage += "      Press Ok to Post";
                ViewBag.btnOK = "Show OK";
            }

            return PartialView(GetVirtualPath("_PostBatches"), objPostBatchesVM);
        }


        //Batch Posted
        [HttpPost]
        public ActionResult BatchPosted(PostBatchesVM objPostBatchesVM)
        {
            int result = 0;
            try
            {
                result = ADBatch.PostBatches();
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return Json(result);
        }


        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewCollections/DataEntry/PostBatches/" + ViewName + ".cshtml";
            return path;
        }
    }
}
