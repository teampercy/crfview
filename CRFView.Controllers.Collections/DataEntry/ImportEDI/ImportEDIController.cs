﻿using CRFView.Adapters;
using CRFView.Adapters.Collections.DataEntry.ImportEDI.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Collections.DataEntry.ImportEDI
{
    public class ImportEDIController : Controller
    {
        ImportEDIVM objImportEDIVM = new ImportEDIVM();

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
           
            return PartialView(GetVirtualPath("_ImportEDI"), objImportEDIVM);
        }

        //Get ClientInterface ddl Details 
        //[HttpPost]
        public ActionResult GetClientInterfaceddlDetails(string FileName, string ClientBatch, string ClientInterfaceId)
        {
            objImportEDIVM.objClientInterface = ADClientInterface.ReadClientInterface(Convert.ToInt32(ClientInterfaceId));
            objImportEDIVM.FileName = FileName;
            objImportEDIVM.ddlClientInterface = ClientInterfaceId;
            objImportEDIVM.ClientBatch = ClientBatch;
            objImportEDIVM.EDIInstruction = objImportEDIVM.objClientInterface.EDIInfo;

            return PartialView(GetVirtualPath("_ImportEDI"), objImportEDIVM);
        }

        //Save ImportEDI Document
        [HttpPost]
        public ActionResult SaveImportEDIDocument()
        {
            try
            {
                var currDate = DateTime.Now.ToString("yyyyMMddHHmmss");
                var file = Request.Files[0];
                //var fileName = Path.GetFileName(file.FileName);
                var fileName = Path.GetFileNameWithoutExtension(file.FileName);
                var fileExt = Path.GetExtension(file.FileName);
                fileName = fileName + "_" + currDate + fileExt;
                var path = Path.Combine(Server.MapPath("~/Output/UploadedFiles"), fileName );
                file.SaveAs(path);
                Session["ImportEDIDocumentPath"] = path;
                return Json(true);
            }
            catch (Exception ex)
            {
                return Json(false);
            }
        }

        //Import EDI
        [HttpPost]
        public string ImportEDI(ImportEDIVM objImportEDIVM)
        {
            string result = null;
            try
            {
                objImportEDIVM.FileName = Convert.ToString(Session["ImportEDIDocumentPath"]);
                if (!String.IsNullOrEmpty(objImportEDIVM.FileName))
                {
                    result = ADClientInterface.ImportEDI(objImportEDIVM);
                    //ADClientInterface.ImportEDI(objImportEDIVM);
                    var path = Path.Combine(Server.MapPath("~/Output/Reports"), result);
                    string fileName = path.Substring(path.LastIndexOf("\\") + 1);
                    fileName = Uri.EscapeDataString(fileName); //Remove # with %23 , space with %20

                    if (result != null && result != "" && result != "error")
                    {
                        string filepath = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);

                        return (filepath);
                    }
                    else
                    {
                        return "error";
                    }
                }
                else
                {
                    return "error";
                }
            }
            catch (Exception ex)
            {
                return "error";
            }
           
        }


        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewCollections/DataEntry/ImportEDI/" + ViewName + ".cshtml";
            return path;
        }

    }
}
