﻿using CRFView.Adapters;
using CRFView.Adapters.Collections.DataEntry.CreateWebBatch.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Collections.DataEntry.CreateWebBatch
{
    public class CreateWebBatchController : Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            BatchWebPlacementsVM objBatchWebPlacementsVM = new BatchWebPlacementsVM();

            DataTable dtBatchDebtAccountForWebPlacements = ADBatchDebtAccount.GetBatchDebtAccountForWebPlacements();

            if (dtBatchDebtAccountForWebPlacements.Rows.Count < 1)
            {
                ViewBag.lblMessage = "No Web Placements to Be Batched";
                ViewBag.btnOK = "Hide OK";
            }
            else
            {
                ViewBag.lblMessage = "(" + dtBatchDebtAccountForWebPlacements.Rows.Count + ") Placements to Be Posted";
                ViewBag.lblMessage += Environment.NewLine;
                ViewBag.lblMessage += "      Press Ok to Post";
                ViewBag.btnOK = "Show OK";
            }

            return PartialView(GetVirtualPath("_BatchWebPlacements"), objBatchWebPlacementsVM);
        }


        //Batch WebPlacements
        [HttpPost]
        public ActionResult BatchWebPlacements(BatchWebPlacementsVM objBatchWebPlacementsVM)
        {
            int result = 0;
            try
            {
                result = ADBatchDebtAccount.CreateBatchWebPlacements();
            }
            catch (Exception ex)
            {
                result = 0;
            }
            return Json(result);
        }


        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewCollections/DataEntry/CreateWebBatch/" + ViewName + ".cshtml";
            return path;
        }
    }
}
