﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CRFView.Adapters;
using CRFView.Adapters.Collections.DataEntry.BatchReports.ViewModels;
using System.IO;

namespace CRFView.Controllers.Collections.DataEntry.BatchReports
{
    public class BatchReportsController : Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]    
        public ActionResult ShowModal()
        {
            BatchReportOptionsVM objBatchReportOptionsVM = new BatchReportOptionsVM();
         
            return PartialView(GetVirtualPath("_BatchReportOptions"), objBatchReportOptionsVM);
        }

        //Print BatchReports
        [HttpPost]
        public string PrintBatchReportsOptions(BatchReportOptionsVM objBatchReportOptionsVM)
        {
            ADvwBatchDebtAccount obj = new ADvwBatchDebtAccount();
      
            string PdfUrl = obj.PrintBatchReport(objBatchReportOptionsVM);
            if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
            {
                string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                return (path);
            }

            return "error";
        }
         
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewCollections/DataEntry/BatchReports/" + ViewName + ".cshtml";
            return path;
        }

    }
}
