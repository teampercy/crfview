﻿using CRFView.Adapters;
using CRFView.Adapters.Collections.Management.Reports.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Collections.Management.Reports
{
    public class ReportsController : Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            ReportsVM objReportsVM = new ReportsVM();

            return PartialView(GetVirtualPath("_ManagementReports"), objReportsVM);
        }


        //Print ManagementReports 
        [HttpPost]
        public string ManagementReportsPrint(ReportsVM objReportsVM)
        {
            try
            {
                string PdfUrl = null;
                if (objReportsVM.RbtPrintOption == "Send to CSV")
                {
                    PdfUrl = ADManagementReports.PrintCSVManagementReports(objReportsVM);
                }
                else
                {
                    PdfUrl = ADManagementReports.PrintPDFManagementReports(objReportsVM);
                }
      
                if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
                {
                    string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                    string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                    return (path);
                }
            }
            catch (Exception ex)
            {
                return "error";
            }
            return "error";
        }


        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewCollections/Management/Reports/" + ViewName + ".cshtml";
            return path;
        }

    }
}
