﻿using CRFView.Adapters;
using CRFView.Adapters.Collections.Management.ChangeAccountInfo.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Collections.Management.ChangeAccountInfo
{
    public class ChangeAccountInfoController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            ChangeAccountInfoVM objChangeAccountInfoVM = new ChangeAccountInfoVM();

            return PartialView(GetVirtualPath("_ChangeAccountInfo"), objChangeAccountInfoVM);
        }

        //Get DebtAccountDetails By DebtAccountId
        [HttpPost]
        public ActionResult GetDebtAccountDetailsByDebtAccountId(string DebtAccountId)
        {
            ChangeAccountInfoVM objChangeAccountInfoVM = new ChangeAccountInfoVM();
            objChangeAccountInfoVM.DebtAccountId = DebtAccountId;

            DataTable dtDebtAccountInfoList = ADvwAccountList.GetDebtAccountInfoList(DebtAccountId);

            if (dtDebtAccountInfoList.Rows.Count > 0)
            {
                objChangeAccountInfoVM.Message = "Success";
                objChangeAccountInfoVM.objDebtAccount.ClientCode = ((dtDebtAccountInfoList.Rows[0]["ClientCode"] != DBNull.Value ? dtDebtAccountInfoList.Rows[0]["ClientCode"].ToString() : ""));
                objChangeAccountInfoVM.objDebtAccount.ClientName = ((dtDebtAccountInfoList.Rows[0]["ClientName"] != DBNull.Value ? dtDebtAccountInfoList.Rows[0]["ClientName"].ToString() : ""));
                objChangeAccountInfoVM.objDebtAccount.DebtAccountId = ((dtDebtAccountInfoList.Rows[0]["DebtAccountId"] != DBNull.Value ? Convert.ToInt32(dtDebtAccountInfoList.Rows[0]["DebtAccountId"]) : 0));
                objChangeAccountInfoVM.objDebtAccount.ReferalDate = ((dtDebtAccountInfoList.Rows[0]["ReferalDate"] != DBNull.Value ? Convert.ToDateTime(dtDebtAccountInfoList.Rows[0]["ReferalDate"]) : DateTime.MinValue));
                objChangeAccountInfoVM.objDebtAccount.AccountNo = ((dtDebtAccountInfoList.Rows[0]["AccountNo"] != DBNull.Value ? dtDebtAccountInfoList.Rows[0]["AccountNo"].ToString() : ""));
                objChangeAccountInfoVM.objDebtAccount.LastServiceDate = ((dtDebtAccountInfoList.Rows[0]["LastServiceDate"] != DBNull.Value ? Convert.ToDateTime(dtDebtAccountInfoList.Rows[0]["LastServiceDate"]) : DateTime.MinValue));
                objChangeAccountInfoVM.objDebtAccount.DebtorName = ((dtDebtAccountInfoList.Rows[0]["DebtorName"] != DBNull.Value ? dtDebtAccountInfoList.Rows[0]["DebtorName"].ToString() : ""));
                objChangeAccountInfoVM.objDebtAccount.LastPaymentDate = ((dtDebtAccountInfoList.Rows[0]["LastPaymentDate"] != DBNull.Value ? Convert.ToDateTime(dtDebtAccountInfoList.Rows[0]["LastPaymentDate"]) : DateTime.MinValue));
                objChangeAccountInfoVM.objDebtAccount.ContactName = ((dtDebtAccountInfoList.Rows[0]["ContactName"] != DBNull.Value ? dtDebtAccountInfoList.Rows[0]["ContactName"].ToString() : ""));
                objChangeAccountInfoVM.objDebtAccount.TotAsgAmt = ((dtDebtAccountInfoList.Rows[0]["TotAsgAmt"] != DBNull.Value ? Convert.ToDecimal(dtDebtAccountInfoList.Rows[0]["TotAsgAmt"]) : 0));
                objChangeAccountInfoVM.objDebtAccount.AddressLine1 = ((dtDebtAccountInfoList.Rows[0]["AddressLine1"] != DBNull.Value ? dtDebtAccountInfoList.Rows[0]["AddressLine1"].ToString() : ""));
                objChangeAccountInfoVM.objDebtAccount.TotPmtAmt = ((dtDebtAccountInfoList.Rows[0]["TotPmtAmt"] != DBNull.Value ? Convert.ToDecimal(dtDebtAccountInfoList.Rows[0]["TotPmtAmt"]) : 0));
                objChangeAccountInfoVM.objDebtAccount.City = ((dtDebtAccountInfoList.Rows[0]["City"] != DBNull.Value ? dtDebtAccountInfoList.Rows[0]["City"].ToString() : ""));
                objChangeAccountInfoVM.objDebtAccount.State = ((dtDebtAccountInfoList.Rows[0]["State"] != DBNull.Value ? dtDebtAccountInfoList.Rows[0]["State"].ToString() : ""));
                objChangeAccountInfoVM.objDebtAccount.PostalCode = ((dtDebtAccountInfoList.Rows[0]["PostalCode"] != DBNull.Value ? dtDebtAccountInfoList.Rows[0]["PostalCode"].ToString() : ""));
                objChangeAccountInfoVM.objDebtAccount.CommRateId =  ((dtDebtAccountInfoList.Rows[0]["CommRateId"] != DBNull.Value ? Convert.ToInt32(dtDebtAccountInfoList.Rows[0]["CommRateId"]) : 0));
                objChangeAccountInfoVM.objDebtAccount.IsNoCollectFee = ((dtDebtAccountInfoList.Rows[0]["IsNoCollectFee"] != DBNull.Value ? Convert.ToBoolean(dtDebtAccountInfoList.Rows[0]["IsNoCollectFee"]) : false));
                objChangeAccountInfoVM.objDebtAccount.ClientId = ((dtDebtAccountInfoList.Rows[0]["ClientId"] != DBNull.Value ? Convert.ToInt32(dtDebtAccountInfoList.Rows[0]["ClientId"]) : 0));

                int CommRateId = objChangeAccountInfoVM.objDebtAccount.CommRateId;

                if (CommRateId > 0)
                {
                    objChangeAccountInfoVM.objCollectionRate = ADCollectionRate.ReadCollectionRate(Convert.ToString(CommRateId));
                    objChangeAccountInfoVM.CollectionRateCode = objChangeAccountInfoVM.objCollectionRate.CollectionRateCode;
                }

                if (objChangeAccountInfoVM.objDebtAccount.IsNoCollectFee == true)
                {
                    objChangeAccountInfoVM.chkNoCollectFee = true;
                }
                else
                {
                    objChangeAccountInfoVM.chkNoCollectFee = false;
                }

                int ClientId = objChangeAccountInfoVM.objDebtAccount.ClientId;

                if(ClientId >0)
                {
                    objChangeAccountInfoVM.ddlNewClientId =Convert.ToString(ClientId);
                }

                if (objChangeAccountInfoVM.objDebtAccount.IsTenDayContactPlan == true)
                {
                    objChangeAccountInfoVM.chkTenDayContactPlan = true;
                }
                else
                {
                    objChangeAccountInfoVM.chkTenDayContactPlan = false;
                }

            }
            else
            {
                objChangeAccountInfoVM.Message = "Requested Debtor is not found";
            }

            return PartialView(GetVirtualPath("_ChangeAccountInfo"), objChangeAccountInfoVM);
        }

        //Update DebtAccount Info
        [HttpPost]
        public ActionResult UpdateDebtAccountInfo(ChangeAccountInfoVM objChangeAccountInfoVM)
        {
            int result = 0;
            string DebtAccountId = objChangeAccountInfoVM.DebtAccountId;
            try
            {
                if(objChangeAccountInfoVM.chkAssignAmt == true)
                {
                    objChangeAccountInfoVM.objDebtAccountLedger = ADDebtAccountLedger.ReadDebtAccountLedgerByDebtAccountId(DebtAccountId);
                    objChangeAccountInfoVM.objDebtAccountLedger.TrxAmt = Convert.ToDecimal(objChangeAccountInfoVM.NewAssignAmt);
                    objChangeAccountInfoVM.objDebtAccountLedger.AsgAmt = Convert.ToDecimal(objChangeAccountInfoVM.NewAssignAmt);
                    objChangeAccountInfoVM.objDebtAccountLedger.DebtAccountLedgerId = objChangeAccountInfoVM.objDebtAccountLedger.DebtAccountLedgerId;
                    result = ADDebtAccountLedger.SaveDebtAccountLedger(objChangeAccountInfoVM.objDebtAccountLedger);
                }

                objChangeAccountInfoVM.objDebtAccnt = ADDebtAccount.ReadDebtAccount(Convert.ToInt32(DebtAccountId));
             
                if (objChangeAccountInfoVM.chkAccountNo == true)
                {
                    objChangeAccountInfoVM.objDebtAccnt.AccountNo = objChangeAccountInfoVM.NewAccountNo;
                }

                if (objChangeAccountInfoVM.chkAssignDate == true)
                {
                    objChangeAccountInfoVM.objDebtAccnt.ReferalDate = objChangeAccountInfoVM.NewAssignDate;
                }

                if (objChangeAccountInfoVM.chkLastChgDate == true)
                {
                    objChangeAccountInfoVM.objDebtAccnt.LastServiceDate = objChangeAccountInfoVM.NewLastChgDate;
                }

                if (objChangeAccountInfoVM.chkLastPayDate == true)
                {
                    objChangeAccountInfoVM.objDebtAccnt.LastPaymentDate = objChangeAccountInfoVM.NewLastPayDate;
                }

                if (objChangeAccountInfoVM.chkSpecialRate == true)
                {
                    objChangeAccountInfoVM.objDebtAccnt.CommRateId =Convert.ToInt32(objChangeAccountInfoVM.ddlNewCommRateId);
                }

                if (objChangeAccountInfoVM.chkIsNoCollectFee == true)
                {
                    objChangeAccountInfoVM.objDebtAccnt.IsNoCollectFee = true;
                }
                

                if (objChangeAccountInfoVM.chkAssignAmt == true)
                {
                    objChangeAccountInfoVM.objDebtAccnt.IsRecalcTotals = true;
                }

                if (objChangeAccountInfoVM.chkIsTenDayContactPlan == true)
                {
                    objChangeAccountInfoVM.objDebtAccnt.IsTenDayContactPlan = true;
                }

                result = ADDebtAccount.SaveDebtAccount_ChangeDebtorAccountInfo(objChangeAccountInfoVM.objDebtAccnt);

                objChangeAccountInfoVM.objDebtAccnt = ADDebtAccount.ReadDebtAccount(Convert.ToInt32(DebtAccountId));

                int DebtId = objChangeAccountInfoVM.objDebtAccnt.DebtId;
                if (objChangeAccountInfoVM.chkDebtorNameTo == true)
                {
                    objChangeAccountInfoVM.objDebt = ADDebt.ReadDebt(DebtId);
                    objChangeAccountInfoVM.objDebt.DebtorName = objChangeAccountInfoVM.NewDebtorName;
                    result = ADDebt.SaveDebt_ChangeDebtorAccountInfo(objChangeAccountInfoVM.objDebt);
                }

                if (objChangeAccountInfoVM.chkClientNumber == true)
                {
                    string ClientId = objChangeAccountInfoVM.ddlNewClientId;
                    result = ADDebtAccount.SaveDebt_ChangeDebtorAccountInfo_ClientInfo(DebtAccountId, ClientId);
                }

                return Json(result);
            }
            catch
            {
                result = -1;
                return Json(result);
            }

        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewCollections/Management/ChangeAccountInfo/" + ViewName + ".cshtml";
            return path;
        }


    }
}
