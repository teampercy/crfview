﻿using CRFView.Adapters;
using CRFView.Adapters.Collections.Management.DeskAssignment.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Collections.Management.DeskAssignment
{
    public class DeskAssignmentController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            DeskAssignmentVM objDeskAssignmentVM = new DeskAssignmentVM();

            ViewBag.lblRecCount = "";

            return PartialView(GetVirtualPath("_DeskAssignment"), objDeskAssignmentVM);
        }


        //Get DeskAssignment List
        [HttpPost]
        public ActionResult GetDeskAssignmentList(DeskAssignmentVM objDeskAssignmentVM)
        {
            objDeskAssignmentVM.dtDeskAssignmentList = ADvwAccountList.GetAccountDetailDt(objDeskAssignmentVM);

            objDeskAssignmentVM.DeskAssignmentList = ADvwAccountList.GetAccountDetailList(objDeskAssignmentVM.dtDeskAssignmentList);

            ViewBag.lblRecCount = "Total  (" + objDeskAssignmentVM.DeskAssignmentList.Count + ") Accounts Selected";

            return PartialView(GetVirtualPath("_DeskAssignment"), objDeskAssignmentVM);
        }

        //Get DeskAssignment Details for Edit/Update
        [HttpPost]
        public ActionResult GetDeskAssignmentDetails(string DebtAccountId)
        {
            DeskAssignmentVM objDeskAssignmentVM = new DeskAssignmentVM();
            objDeskAssignmentVM.objDebtAccount = ADDebtAccount.ReadDebtAccount(Convert.ToInt32(DebtAccountId));
            objDeskAssignmentVM.DebtAccountId = DebtAccountId;
            objDeskAssignmentVM.ddlNewDeskId =Convert.ToString(objDeskAssignmentVM.objDebtAccount.LocationId);
            objDeskAssignmentVM.ddlNewActionId = Convert.ToString(objDeskAssignmentVM.objDebtAccount.CollectionStatusId);
             
            return PartialView(GetVirtualPath("_EditDeskAssignment"), objDeskAssignmentVM);
        }


        //Update DeskAssignment Details
        [HttpPost]
        public ActionResult UpdateDeskAssignment(DeskAssignmentVM objDeskAssignmentVM)
        {
            int result = 1;

            if(objDeskAssignmentVM.RbtNewOption == "New Desk" && Convert.ToInt32(objDeskAssignmentVM.ddlNewDeskId) > 0)
            {
                objDeskAssignmentVM.objDebtAccount.DebtAccountId =Convert.ToInt32(objDeskAssignmentVM.DebtAccountId);
                objDeskAssignmentVM.objDebtAccount.LocationId = Convert.ToInt32(objDeskAssignmentVM.ddlNewDeskId);
               result = ADDebtAccount.SaveDebtAccount_DeskAssignment_Desk(objDeskAssignmentVM.objDebtAccount);
            }
            else if(objDeskAssignmentVM.RbtNewOption == "New Action" && Convert.ToInt32(objDeskAssignmentVM.ddlNewActionId) > 0)
            {
                objDeskAssignmentVM.objDebtAccount.DebtAccountId = Convert.ToInt32(objDeskAssignmentVM.DebtAccountId);
                objDeskAssignmentVM.objDebtAccount.CollectionStatusId = Convert.ToInt32(objDeskAssignmentVM.ddlNewActionId);
                result = ADDebtAccount.SaveDebtAccount_DeskAssignment_Action(objDeskAssignmentVM.objDebtAccount);
            }

            //Obj_LegalCostVM.objDebtAccountLegalCostEdit.DebtAccountId = Convert.ToInt32(Obj_LegalCostVM.DebtAccountId);
            //Obj_LegalCostVM.objDebtAccountLegalCostEdit.Id = Convert.ToInt32(Obj_LegalCostVM.Id);
            //result = ADDebtAccountLegalCost.SaveDebtAccountLegalCost(Obj_LegalCostVM.objDebtAccountLegalCostEdit);

            return Json(result);
        }

        //Load DeskAssignment Modal
        [HttpPost]
        public ActionResult DeskAssignmentLoad(DeskAssignmentVM objDeskAssignmentVM)
        {

            ViewBag.lblRecCount = "";
            return PartialView(GetVirtualPath("_DeskAssignment"), objDeskAssignmentVM);
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewCollections/Management/DeskAssignment/" + ViewName + ".cshtml";
            return path;
        }
    }
}
