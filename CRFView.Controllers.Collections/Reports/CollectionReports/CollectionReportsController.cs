﻿using CRFView.Adapters;
using CRFView.Adapters.Collections.Reports.CollectionReports.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Collections.Reports.CollectionReports
{
    public class CollectionReportsController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        //Show Modal
        [HttpPost]
        public ActionResult ShowModal()
        {
            CollectionReportsVM objCollectionReportsVM = new CollectionReportsVM();

            return PartialView(GetVirtualPath("_CollectionReports"), objCollectionReportsVM);
        }

        //Get Action History Codes List by Acction Type
        public ActionResult GetReportIdList(string ReportCategoryValue)
        {
            List<CommonBindList> ReportIdList = new List<CommonBindList>();
            try
            {
                ReportIdList = CommonBindList.GetReportIdListByCategoryType(ReportCategoryValue);
            }
            catch (Exception ex)
            {

            }
            return Json(ReportIdList, JsonRequestBehavior.AllowGet);
        }

        //Print CollectionReports 
        [HttpPost]
        public string CollectionReportsPrint(CollectionReportsVM objCollectionReportsVM)
        {
            try
            {
                string PdfUrl = null;
              
                PdfUrl = ADCollectionReports.PrintCollectionReports(objCollectionReportsVM);
              
                if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
                {
                    string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                    string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                    return (path);
                }
            }
            catch (Exception ex)
            {
                return "error";
            }
            return "error";
        }


        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewCollections/Reports/CollectionReports/" + ViewName + ".cshtml";
            return path;
        }
    }
}
