﻿using CRFView.Adapters;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices.LienInvoicesVM;
using CRFView.Adapters.Clients.Client_Management;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class LienInvoicesController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            LienInvoicesListVM obj = new LienInvoicesListVM();
            obj.LienInvoices = ADVWCLIENTFINDOCEMAIL.ListADVWCLIENTFINDOCEMAIL();
            return PartialView(GetVirtualPath("Index"), obj);
        }

        [HttpPost]
        public ActionResult Search(FormCollection fc)
        {
            string RadioSelection = fc["RadioSelection"];
            string FromDate = fc["FromDate"];
            string ToDate = fc["ToDate"];
            string TxtSearch = fc["TxtSearch"];
            LienInvoicesListVM obj = new LienInvoicesListVM();
            obj.LienInvoices = ADVWCLIENTFINDOCEMAIL.FilterListADVWCLIENTFINDOCEMAIL(RadioSelection, FromDate,
                                                                                      ToDate, TxtSearch);
            return PartialView(GetVirtualPath("_LienInvoicesList"), obj);
        }


        [HttpPost]
        public ActionResult ShowDetails(string ClientFinDocId, string HistoryId)
        {
            LienInvoiceDetailsVM obj = new LienInvoiceDetailsVM();
            obj = ADVWCLIENTFINDOCEMAIL.GetDetails(ClientFinDocId, HistoryId);
            obj.ObjClientLienInfo.InvoicePhoneNo = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(obj.ObjClientLienInfo.InvoicePhoneNo);
            obj.ObjClientLienInfo.InvoiceFax = CRF.BLL.CRFView.CRFView.GetFormattedPhoneNumber(obj.ObjClientLienInfo.InvoiceFax);
            obj.HistoryId = HistoryId;

            return PartialView(GetVirtualPath("_LienInvoiceDetails"), obj);
        }

        [HttpPost]
        public ActionResult FlagAsPaid(string Id)
        {
            bool result = ADClientFinDoc.UpdatePaid(Id);
            return Json(result);
        }

        [HttpPost]
        public ActionResult ResendEmail(string Id,string PKID,string InvoiceEmail)
        {
            bool result;
            result = ADClientFinDoc.ResendEmailUpdate(Id,PKID,InvoiceEmail);
            result = ADReport_History.UpdateStatus(PKID);
            return Json(result);
        }

        [HttpPost]
        public ActionResult FlagUnpaid(string Id)
        {
            bool result = false;
            result = ADClientFinDoc.FlagUnpaid(Id);
            return Json(result);
        }

        [HttpPost]
        public ActionResult FlagViewed(string PKID)
        {
            bool result = false;
            result = ADReport_History.UpdateViewed(PKID);
            return Json(result);
        }

        [HttpPost]
        public ActionResult LienInvoiceSave(string Id,bool ChkNewReview,bool ChkNewEmail,
                                            bool ChkDeleteReview, string TxtNewReviewDate , string TxtNewEmail)
        {
            bool result = ADClientFinDoc.UpdateClientFinDoc( Id,  ChkNewReview,  ChkNewEmail,
                                             ChkDeleteReview,  TxtNewReviewDate,  TxtNewEmail);
            return Json(result);
        }

        [HttpPost]
        public ActionResult ShowAddNote(string ClientId)
        {
            NoteVM Obj_NoteVM = new NoteVM();
            Obj_NoteVM.Obj_Note.ClientId = Convert.ToInt32(ClientId);
            return PartialView(GetVirtualPath("_Note"), Obj_NoteVM);
        }

        [HttpPost]
        public ActionResult SaveNote(NoteVM Obj_NoteVM)
        {
            CurrentUser CU_Obj = (CurrentUser)Session["LoggedInUser"];
            bool result = ADIssues.SaveNoteClientFollowup(Obj_NoteVM.Obj_Note,CU_Obj);
            return Json(result);
        }

        public ActionResult ViewAttachment(string PKId)
        {
            var Attachment = ADreport_historyattachment.ReadReport(PKId); 
            var path = Path.Combine(Server.MapPath("~/App_Data/DownloadableFiles"), Attachment.url);
            ADreport_historyattachment.SaveFileToBeViewed(Attachment.image, path);
            byte[] fileBytes = Attachment.image;
            string fileName = path.Substring(path.LastIndexOf("\\") + 1);
            return File(fileBytes, System.Net.Mime.MediaTypeNames.Application.Octet, fileName);
        }

        [HttpPost]
        public ActionResult ShowPaymentORAdjust(string Id, string HistoryId)
        {
            LienInvoiceDetailsVM obj = new LienInvoiceDetailsVM();
            obj = ADVWCLIENTFINDOCEMAIL.GetDetails(Id, HistoryId);
            obj.objADClientFinDocLedger = ADClientFinDocLedger.ReadClientFinDocLedger(0);
            obj.LienBalAmt = obj.ObjClientFinDoc.DocumentAmt.ToString();
            obj.LienRcvAmt = obj.objADClientFinDocLedger.RcvAmt.ToString();
            obj.LienAdjAmt = obj.objADClientFinDocLedger.AdjAmt.ToString();
            obj.ClientFinDocId = obj.ObjClientFinDoc.Id;
            obj.ClntFinId = 0;
            obj.HistoryId = HistoryId;


            return PartialView(GetVirtualPath("_PaymentAndAdjustmentDetails"), obj);
        }

        
        public ActionResult PaymentORAdjustmentDetails(string Id, string HistoryId)
        {
            LienInvoiceDetailsVM objLienInvoiceDetailsVM = new LienInvoiceDetailsVM();
            //obj = ADVWCLIENTFINDOCEMAIL.GetDetails(Id, "0");
            objLienInvoiceDetailsVM.objADClientFinDocLedger = ADClientFinDocLedger.ReadClientFinDocLedger(Convert.ToInt32(Id));
            objLienInvoiceDetailsVM.LienBalAmt = objLienInvoiceDetailsVM.objADClientFinDocLedger.BalAmt.ToString();
            objLienInvoiceDetailsVM.LienRcvAmt = objLienInvoiceDetailsVM.objADClientFinDocLedger.RcvAmt.ToString();
            objLienInvoiceDetailsVM.LienAdjAmt = objLienInvoiceDetailsVM.objADClientFinDocLedger.AdjAmt.ToString();
            objLienInvoiceDetailsVM.ClntFinId = objLienInvoiceDetailsVM.objADClientFinDocLedger.Id;
            objLienInvoiceDetailsVM.ClientFinDocId = objLienInvoiceDetailsVM.objADClientFinDocLedger.ClientFinDocId;
            objLienInvoiceDetailsVM.HistoryId = HistoryId;
            if (objLienInvoiceDetailsVM.objADClientFinDocLedger.IsAdjustment == true)
            {
                objLienInvoiceDetailsVM.RdbBtn = "Adjustment";
            }
            if (objLienInvoiceDetailsVM.objADClientFinDocLedger.IsCheck == true)
            {
                objLienInvoiceDetailsVM.PTypeRdbBtn = "Check";
            }
            if (objLienInvoiceDetailsVM.objADClientFinDocLedger.IsCreditCard == true)
            {
                objLienInvoiceDetailsVM.PTypeRdbBtn = "CreditCard";
            }
            if (objLienInvoiceDetailsVM.objADClientFinDocLedger.IsACH == true)
            {
                objLienInvoiceDetailsVM.PTypeRdbBtn = "ACH";
            }

            return PartialView(GetVirtualPath("_EditPaymentAndAdjustmentDetails"), objLienInvoiceDetailsVM);
        }


        [HttpPost]
        public ActionResult SavePaymentORAdjustmentDetails(LienInvoiceDetailsVM objLienInvoiceDetailsVM)
        {
            
            bool result = ADClientFinDocLedger.SavePaymentORAdjustment(objLienInvoiceDetailsVM);
            return Json(result);
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewAccounting/ClientFollowUp/LienInvoices/" + ViewName + ".cshtml";
            return path;
        }
    }
}
