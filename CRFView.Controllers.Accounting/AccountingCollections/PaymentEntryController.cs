﻿//using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.COMMON;
using CRF.BLL.CRFDB.TABLES;
using CRFView.Adapters;
using CRFView.Adapters.Accounting.AccountingCollections.PaymentEntry;
using CRFView.Adapters.Accounting.AccountingCollections.PaymentEntry.PaymentEntryVM;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices.CollectionInvoicesVM;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices.LienInvoicesVM;
using CRFView.Adapters.Clients.Client_Management;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRFView.Controllers.Accounting.AccountingCollections 
{
    public class PaymentEntryController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        [HttpPost]
        public  ActionResult CheckAccount(string AccountId)
        {
            bool result = AdAccountLedger.CheckAccountExists(AccountId);
            return Json(result);
        }

        [HttpPost]
        public ActionResult Search(string AccountId)
        {
            var obj = AdAccountLedger.GetAccountDetail(AccountId);
            return PartialView(GetVirtualPath("_AccountDetails"), obj);
        }

        [HttpPost]
        public ActionResult LoadNewAdjustment(string FileNumber, string TotBalAmt)
        {
            NewAdjustmentVM ObjNewAdjustmentVM = new NewAdjustmentVM();
            AdDedtAccountLedger objAdDedtAccountLedger = new AdDedtAccountLedger(); 

            objAdDedtAccountLedger.DebtAccountId = Convert.ToInt32(FileNumber);
            objAdDedtAccountLedger.LedgerType = "BADJ";
            objAdDedtAccountLedger.ReceiptDate = DateTime.Today;

            ObjNewAdjustmentVM = AdDedtAccountLedger.LoadNewAdjustmentDetails(FileNumber, objAdDedtAccountLedger, TotBalAmt);



            return PartialView(GetVirtualPath("_NewAdjustment"), ObjNewAdjustmentVM);
        }


        [HttpPost]
        public ActionResult LoadNewPayment(string FileNumber,string location, int DebtAccountLedgerId, bool IsEdit)
        {
            NewPaymentVM ObjNewPaymentVM = new NewPaymentVM();
            


          //  ConfigFile myconfig = new ConfigFile();
            string mylastledgertype = "";
          //  mylastledgertype = myconfig.GetValue("LASTLEDGERTYPE");

            DebtAccountLedger myledger = new DebtAccountLedger();
            myledger.DebtAccountId = Convert.ToInt32(FileNumber);
            myledger.LedgerType = mylastledgertype;
            //myledger.ReceiptDate = Today
            //myledger.DeskNum = Me.Location.Value

            ObjNewPaymentVM = AdDedtAccountLedger.LoadNewPayment(FileNumber, location, DebtAccountLedgerId, IsEdit);
            ObjNewPaymentVM.EditNewPayment = false;
            ObjNewPaymentVM.DebtAccountId = Convert.ToInt32(FileNumber);
            ObjNewPaymentVM.Id = ObjNewPaymentVM.Id;
            ObjNewPaymentVM.Location = location;
            ObjNewPaymentVM.DebtAccountLedgerId = DebtAccountLedgerId;
            ObjNewPaymentVM.IsAutodistribute = false;

            return PartialView(GetVirtualPath("_NewPayment"), ObjNewPaymentVM);
        }

        [HttpPost]
        public ActionResult EditDetails(string FileNumber, string location, int DebtAccountLedgerId, bool IsEdit)
        {
            NewPaymentVM ObjNewPaymentVM = new NewPaymentVM();
            int result = 0;
            AdDedtAccountLedger objDebtAccountLedger = new AdDedtAccountLedger();
            ADDebtAccount objADDebtAccount = new ADDebtAccount();
            NewAdjustmentVM ObjNewAdjustmentVM = new NewAdjustmentVM();
            //AdDedtAccountLedger objAdDedtAccountLedger = new AdDedtAccountLedger();
            objDebtAccountLedger = AdDedtAccountLedger.ReadDedtAccountLedger(DebtAccountLedgerId);
            objADDebtAccount = ADDebtAccount.ReadDebtAccount(Convert.ToInt32(FileNumber));

            

            if (objDebtAccountLedger.IsRemitted == true)
            {
                result = 0;
                return Json(result);
            }

           
             switch (objDebtAccountLedger.LedgerType)
            {
                case "PMTA":case "PMTC":
                    ObjNewPaymentVM = AdDedtAccountLedger.LoadNewPayment(FileNumber, location, DebtAccountLedgerId, IsEdit);
                    ObjNewPaymentVM.EditNewPayment = true;
                    ObjNewPaymentVM.DebtAccountId = Convert.ToInt32(FileNumber);
                    ObjNewPaymentVM.Id = ObjNewPaymentVM.Id;
                    ObjNewPaymentVM.DebtAccountLedgerId = DebtAccountLedgerId;
                    ObjNewPaymentVM.Location = location;
                    ObjNewPaymentVM.IsAutodistribute = true;
                    return PartialView(GetVirtualPath("_NewPayment"), ObjNewPaymentVM);
                    break;

                case "BADJ":
                    ObjNewAdjustmentVM = AdDedtAccountLedger.LoadNewAdjustmentDetails(FileNumber, objDebtAccountLedger, objADDebtAccount.TotBalAmt.ToString());
                    ObjNewAdjustmentVM.EditNewAdjustment = true;
                    ObjNewAdjustmentVM.DebtAccountLedgerId = DebtAccountLedgerId;
                    return PartialView(GetVirtualPath("_NewAdjustment"), ObjNewAdjustmentVM);
                    break;


            }
            result = -1;
            return Json(result);
           
           
        }
        //AutoDistribute
        [HttpPost]
        public ActionResult AutoDistribute(NewPaymentVM ObjNewPaymentVM,int DebtAccountId,decimal TrxAmt,string location,int DebtAccountLedgerId,bool IsAutodistribute)
        {
            

            ObjNewPaymentVM = AdDedtAccountLedger.AutoDistribution(ObjNewPaymentVM,DebtAccountId, TrxAmt, location, DebtAccountLedgerId, IsAutodistribute);
            //ObjNewPaymentVM.EditNewPayment = IsAutodistribute;
            ObjNewPaymentVM.DebtAccountId = Convert.ToInt32(DebtAccountId);
            ObjNewPaymentVM.Id = ObjNewPaymentVM.Id;
            ObjNewPaymentVM.DebtAccountLedgerId = DebtAccountLedgerId;
            ObjNewPaymentVM.Location = location;
            ObjNewPaymentVM.IsAutodistribute = IsAutodistribute;
            return PartialView(GetVirtualPath("_NewPayment"), ObjNewPaymentVM);
        }


        public ActionResult SaveNewAdjustment(NewAdjustmentVM ObjNewAdjustmentVM)
        {
            int result = 0;

           // result = AdDedtAccountLedger.SaveNewAdjustment(ObjNewAdjustmentVM);
            result =  ADDebtAccount.SaveNewAdjustment(ObjNewAdjustmentVM);
            return Json(result);

        }

        public ActionResult SaveNewPayment(NewPaymentVM ObjNewPaymentVM)
        {
            int result = 0;

            //result =  ADDebtAccount.TotalDistributions(ObjNewPaymentVM);
            result = ADDebtAccount.SaveNewPaymentDetails(ObjNewPaymentVM);

           
            return Json(result);

        }




        [HttpPost]
        public ActionResult GetLedger(string AccountId)
        {
            var obj = AdAccountLedger.GetLedgerDetail(AccountId);
            //obj.DebtAccountId = Convert.ToInt32(AccountId);
            return PartialView(GetVirtualPath("_LedgerDetails"), obj);

        }
        //Delete Adjustment
        [HttpPost]
        public ActionResult DeleteAdjustment (int DebtAccountLedgerId)
        {
            int result = 0;
            result = AdDedtAccountLedger.DeleteAdjustment(DebtAccountLedgerId);
            return Json(result);
        }

        //NSF Functionality
        [HttpPost]
        public ActionResult NsfORReverse(int DebtAccountLedgerId, bool IsNSF, bool IsReverse)
        {
            int result = 0;
            AdDedtAccountLedger objDebtAccountLedger = new AdDedtAccountLedger();
            objDebtAccountLedger = AdDedtAccountLedger.ReadDedtAccountLedger(DebtAccountLedgerId);
            if (objDebtAccountLedger.LedgerType == "ASG")
            {
                result = 0;
                return Json(result);
            }
            result = AdDedtAccountLedger.NsfORReverse(objDebtAccountLedger, IsNSF, IsReverse);
            return Json(result);
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewAccounting/AccountingCollections/PaymentEntry/" + ViewName + ".cshtml";
            return path;
        }
    }
}
