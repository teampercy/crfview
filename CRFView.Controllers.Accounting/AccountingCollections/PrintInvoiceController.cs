﻿using CRFView.Adapters.Accounting.AccountingCollections.PrintInvoice.ViewModels;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Accounting.AccountingCollections.PrintInvoice
{
    public class PrintInvoiceController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return View(GetVirtualPath("Index"));
        }

        [HttpPost]
        public ActionResult ShowModal()
        {
            PrintInvoiceVM objPrintInvoiceVM = new PrintInvoiceVM();


            return PartialView(GetVirtualPath("_PrintInvoice"), objPrintInvoiceVM);

        }

        [HttpPost]
        public string PrintTrustInvoice(PrintInvoiceVM objPrintInvoiceVM)
        {

            try
            {
                string PdfUrl = null;

                PdfUrl = ADVWCLIENTCOLLFINDOCEMAIL.PrintInvoice(objPrintInvoiceVM);
                if(PdfUrl == "1")
                {
                    return "1";
                }

                //if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
                //{
                //    string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                //    string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                //    return (path);
                //}
            }
            catch (Exception ex)
            {
                return "error";
            }
            return "error";
        }

        //public ActionResult JVTXLettersPrint(PrintInvoiceVM objPrintInvoiceVM)
        //{
        //    int result = 0;
        //    try
        //    {
        //        result = ADVWCLIENTCOLLFINDOCEMAIL.PrintInvoice(objPrintInvoiceVM);
        //    }
        //    catch (Exception ex)
        //    {
        //        result = -1;
        //    }
        //    return Json(result);
        //}

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewAccounting/AccountingCollections/PrintInvoice/" + ViewName + ".cshtml";
            return path;
        }
    }
}
