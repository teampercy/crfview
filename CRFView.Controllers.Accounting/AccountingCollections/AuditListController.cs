﻿//using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters;
using CRFView.Adapters.Accounting.AccountingCollections.AuditList.AuditListVM;
using CRFView.Adapters.Accounting.AccountingCollections.PaymentEntry;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices.CollectionInvoicesVM;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices.LienInvoicesVM;
using CRFView.Adapters.Clients.Client_Management;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using CRFView.Adapters.Collections.Collectors.WorkCard;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CRFView.Controllers.Accounting.AccountingCollections
{
    public class AuditListController : Controller
    {
        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        [HttpPost]
        public ActionResult ShowModal()
        {
            AuditOptionsVM objAuditOptionsVM = new AuditOptionsVM();
            objAuditOptionsVM.States = CommonBindList.GetStates();
            objAuditOptionsVM.ServiceCodes = CommonBindList.GetCollectionServiceCodes();
            objAuditOptionsVM.InvoiceCycle = CommonBindList.GetInvoiceCycleList().ToString();
            //objAuditOptionsVM.FinDocCycle = CommonBindList.GetFinDocCycleList();
            return PartialView(GetVirtualPath("_AuditListOptions"), objAuditOptionsVM);
        }

        [HttpPost]
        public string ProcessAuditList(AuditOptionsVM objAuditOptionsVM)
        {
            try
            {
                string PdfUrl = null;

                PdfUrl = ADvwAccountLedgerSales.PrintAuditListReport(objAuditOptionsVM);

                if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
                {
                    string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                    string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                    return (path);
                }
            }
            catch (Exception ex)
            {
                return "error";
            }
            return "error";
            //return Json(true);
        }
        //[HttpPost]
        //public string InvoiceRecapDetails(InvoiceRecapsVM objInvoiceRecapsVM)
        //{

        //}
       
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewAccounting/AccountingCollections/AuditList/" + ViewName + ".cshtml";
            return path;
        }
    }
}
