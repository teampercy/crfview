﻿using CRFView.Adapters;
using CRFView.Adapters.Accounting.AccountingCollections.DeleteInvoiceRuns.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Accounting.AccountingCollections
{
    public class DeleteInvoiceRunController : Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        public ActionResult ShowModal()
        {
            AccountingDeleteInvoiceRunVM objAccountingDeleteInvoiceRunVM = new AccountingDeleteInvoiceRunVM();
            return PartialView(GetVirtualPath("_DeleteInvoiceRun"), objAccountingDeleteInvoiceRunVM);
        }


        public ActionResult OkDeleteInvoiceRunDetails(AccountingDeleteInvoiceRunVM ObjAccountingDeleteInvoiceRunVM)
        {
            int result = 0;

            result = ADClientCollFinDocItem.OkDeleteInvoiceRuns(ObjAccountingDeleteInvoiceRunVM);

            return Json(result);

        }





        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewAccounting/AccountingCollections/DeleteInvoiceRuns/" + ViewName + ".cshtml";
            return path;
        }

    }
}
