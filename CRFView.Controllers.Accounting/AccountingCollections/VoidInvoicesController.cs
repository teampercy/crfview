﻿using CRFView.Adapters;
using CRFView.Adapters.Accounting.AccountingCollections.VoidInvoices.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Accounting.AccountingCollections
{
    public class VoidInvoicesController : Controller
    {

        public ActionResult Index()
        {
            return View(GetVirtualPath("Index"));
        }

        public ActionResult ShowModal()
        {
            AccountingVoidInvoicesVM ObjAccountingVoidInvoicesVM = new AccountingVoidInvoicesVM();
            return PartialView(GetVirtualPath("_VoidInvoice"), ObjAccountingVoidInvoicesVM);
        }

        public ActionResult VoidInvoicesDetails(AccountingVoidInvoicesVM ObjAccountingVoidInvoicesVM)
        {
            int result = 0;

            result = ADClientCollFinDocItem.SaveVoidInvoices(ObjAccountingVoidInvoicesVM);

            return Json(result);

        }


        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewAccounting/AccountingCollections/VoidInvoices/" + ViewName + ".cshtml";
            return path;
        }


    }
}
