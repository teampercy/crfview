﻿using CRFView.Adapters.Accounting.AccountingCollections.InvoiceRecap;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Accounting.AccountingCollections
{
    public class InvoiceRecapController : Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        public ActionResult ShowModal()
        {
            AccountingInvoiceRecapsVM objAccountingInvoiceRecapsVM = new AccountingInvoiceRecapsVM();
            return PartialView(GetVirtualPath("_InvoiceRecaps"), objAccountingInvoiceRecapsVM);
        }


        [HttpPost]
        public string InvoiceRecapPrintDetails(AccountingInvoiceRecapsVM objAccountingInvoiceRecapsVM)
        {
            try
            {
                string PdfUrl = null;

                PdfUrl = ADClientCollFinDoc.SaveInvoiceRecapsPrint(objAccountingInvoiceRecapsVM);

                if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
                {
                    string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                    string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                    return (path);
                }
            }
            catch (Exception ex)
            {
                return "error";
            }
            return "error";
        }



        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewAccounting/AccountingCollections/InvoiceRecap/" + ViewName + ".cshtml";
            return path;
        }

    }
}
