﻿using CRFView.Adapters.Accounting.AccountingCollections.SmanCommissions.ViewModels;
using CRFView.Adapters.Collections.Collectors.WorkCard;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Accounting.AccountingCollections
{
    public class SmanCommissionsController : Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            return PartialView(GetVirtualPath("Index"));
        }

        public ActionResult ShowModal()
        {
            AccountingSmanCommissionsVM objAccountingSmanCommissionsVM = new AccountingSmanCommissionsVM();
            return PartialView(GetVirtualPath("_SmanCommissions"), objAccountingSmanCommissionsVM);
        }

        [HttpPost]
        public string PrintSmanCommissionsDetails(AccountingSmanCommissionsVM objAccountingSmanCommissionsVM)
        {
            try
            {
                string PdfUrl = null;

                PdfUrl = ADvwAccountLedgerSales.PrintSmanCommissionsReport(objAccountingSmanCommissionsVM);

                if (PdfUrl == "True")
                {
                    return "CSVFilesuccess";
                }

                if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
                {
                    string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                    string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                    return (path);
                }
            }
            catch (Exception ex)
            {
                return "error";
            }
            return "error";
        }





        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewAccounting/AccountingCollections/SmanCommissions/" + ViewName + ".cshtml";
            return path;
        }

    }
}
