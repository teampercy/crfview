﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRFView.Adapters.Accounting.AccountingCollections.CreateInvoices;
using System.Web.Mvc;
using CRFView.Adapters.Accounting.AccountingCollections.CreateInvoices.ViewModels;

namespace CRFView.Controllers.Accounting.AccountingCollections
{
    public class CreateInvoicesController:Controller
    {
        public ActionResult Index()
        {
            return View(GetVirtualPath("Index"));
        }

        public ActionResult ShowModal()
        {
            AccountingCreateInvoicesVM objAccountingCreateInvoicesVM = new AccountingCreateInvoicesVM();
            //AdAccountingCreateInvoices objAdAccountingCreateInvoices = new AdAccountingCreateInvoices();
            return PartialView(GetVirtualPath("_CreateInvoices"), objAccountingCreateInvoicesVM);
        }


        public ActionResult CreateInvoice(AccountingCreateInvoicesVM objAccountingCreateInvoicesVM)
        {
            int result = 0;


            result = AdAccountingCreateInvoices.CreateNewINvoice(objAccountingCreateInvoicesVM);

            return Json(result);

        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewAccounting/AccountingCollections/CreateInvoices/" + ViewName + ".cshtml";
            return path;
        }
    }
}
