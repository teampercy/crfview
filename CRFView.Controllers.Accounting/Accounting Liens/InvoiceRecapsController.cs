﻿using CRFView.Adapters.Accounting.AccountingLiens.InvoiceRecaps.ViewModels;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Accounting.Accounting_Liens
{
    public class InvoiceRecapsController : Controller
    {

        public ActionResult Index()
        {
            return View(GetVirtualPath("Index"));
        }

        public ActionResult ShowModal()
        {

            InvoiceRecapsVM objInvoiceRecapsVM = new InvoiceRecapsVM();
            return PartialView(GetVirtualPath("_InvoiceRecap"), objInvoiceRecapsVM);
        }


        [HttpPost]
        public string InvoiceRecapDetails(InvoiceRecapsVM objInvoiceRecapsVM)
        {
            try
            {
                string PdfUrl = null;

                PdfUrl = ADClientFinDoc.SaveInvoiceRecap(objInvoiceRecapsVM);

                if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
                {
                    string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                    string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                    return (path);
                }
            }
            catch (Exception ex)
            {
                return "error";
            }
            return "error";
        }




        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewAccounting/AccountingLiens/InvoiceRecaps/" + ViewName + ".cshtml";
            return path;
        }


    }
}
