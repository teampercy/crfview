﻿using CRFView.Adapters.Accounting.AccountingLiens.PrintInvoice.ViewModels;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Accounting.Accounting_Liens
{
    public class PrintInvoicesController : Controller
    {

        public ActionResult Index()
         {
            return View(GetVirtualPath("Index"));
        }

        public ActionResult ShowModal()
        {
            PrintInvoicesVM objPrintInvoicesVM = new PrintInvoicesVM();
            return PartialView(GetVirtualPath("_PrintInvoice"), objPrintInvoicesVM);
        }

        [HttpPost]
        public string PrintInvoiceDetails(PrintInvoicesVM objPrintInvoicesVM )
        {
            //string PdfUrl = null;

            //PdfUrl = ADClientFinDoc.PrintInvoicesDetails(objPrintInvoicesVM);
            //string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
            try
            {
                string PdfUrl = null;

                PdfUrl = ADClientFinDoc.PrintInvoicesDetails(objPrintInvoicesVM);
                if (PdfUrl == "1")
                {
                    return "1";
                }
            //    if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
            //    {
            //        string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
            //        string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
            //        return (path);
            //    }
            }
            catch (Exception ex)
            {
                return "error";
            }
           // DownloadReport(MYREPORTPATH);
            return "error";
            //return File(fileName, "application/pdf", Server.UrlEncode(fileName));
        }

        //public string DownloadReport(string path)
        //{
        //    Response.AppendHeader("Content-Disposition", "attachment; filename=" + "abc.pdf"); Response.ContentType = "application/octet-stream";
        //    //string ShareName = System.Configuration.ConfigurationManager.AppSettings.Get("ServerLocationDocumentsFolder");
        //    //  NetworkCredential nc = new NetworkCredential(CommonRoutines.decryptString(ConfigurationSettingsReader.ServerLocationUsername), CommonRoutines.decryptString(ConfigurationSettingsReader.ServerLocationPassword));
        //    //  NetworkConnection ncn = new NetworkConnection(ShareName, nc);
        //    using (System.IO.FileStream fs = new System.IO.FileStream(path, System.IO.FileMode.Open, System.IO.FileAccess.Read))
        //    {
        //        byte[] currentFileBytes = new byte[fs.Length];
        //        fs.Read(currentFileBytes, 0, currentFileBytes.Length);
        //        fs.Close();
        //        Response.BinaryWrite(currentFileBytes);
        //        currentFileBytes = null;
        //        Response.Flush();
        //    }
        //    Response.End();
        //    return "";
        //}

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewAccounting/AccountingLiens/PrintInvoice/" + ViewName + ".cshtml";
            return path;
        }

    }

}
