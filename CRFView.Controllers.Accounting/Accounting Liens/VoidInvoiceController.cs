﻿using CRFView.Adapters.Accounting.AccountingLiens.ViewModels;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Accounting.Accounting_Liens
{
    public class VoidInvoiceController : Controller
    {

        public ActionResult Index()
        {
            return View(GetVirtualPath("Index"));
        }

        public ActionResult ShowModal()
        {
            VoidInvoiceVM objVoidInvoiceVM = new VoidInvoiceVM();

            return PartialView(GetVirtualPath("_VoidInvoice"), objVoidInvoiceVM);
        }

        public ActionResult VoidInvoiceDetails(VoidInvoiceVM objVoidInvoiceVM)
        {
            int result = 0;

            result = ADClientFinDoc.SaveVoidInvoice(objVoidInvoiceVM);

            return Json(result);

        }


        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewAccounting/AccountingLiens/VoidInvoice/" + ViewName + ".cshtml";
            return path;
        }

    }
}
