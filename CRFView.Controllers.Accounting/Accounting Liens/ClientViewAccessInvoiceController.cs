﻿using CRFView.Adapters;
using CRFView.Adapters.Accounting.AccountingLiens.ClientViewAccessInvoice.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Accounting.Accounting_Liens
{
    public class ClientViewAccessInvoiceController : Controller
    {

        [HttpPost]
        public ActionResult Index()
        {
            return View(GetVirtualPath("Index"));
        }

        [HttpPost]
        public ActionResult ShowModal()
        {
            ClientViewAccessInvoiceVM objClientViewAccessInvoiceVM = new ClientViewAccessInvoiceVM();


            return PartialView(GetVirtualPath("_ClientViewAccessInvoice"), objClientViewAccessInvoiceVM);

        }

        [HttpPost]
        public string ClientViewAccessInvoice (ClientViewAccessInvoiceVM  objClientViewAccessInvoiceVM)
        {

            try
            {
                string PdfUrl = null;

                PdfUrl = ADvmJobInfo.ClientViewAccessInvoice(objClientViewAccessInvoiceVM);


                if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
                {
                    string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                   string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                    return (path);
                }
            }
            catch (Exception ex)
            {
                return "error";
            }
            return "error";
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewAccounting/AccountingLiens/ClientViewAccessInvoice/" + ViewName + ".cshtml";
            return path;
        }
    }
}
