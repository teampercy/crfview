﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRFView.Adapters.Accounting.AccountingCollections.CreateInvoices;
using System.Web.Mvc;
using CRFView.Adapters.Accounting.AccountingCollections.CreateInvoices.ViewModels;

namespace CRFView.Controllers.Accounting.Accounting_Liens
{
    public class CreateInvoiceController:Controller
    {
        public ActionResult Index()
        {
            return View(GetVirtualPath("Index"));
        }

        public ActionResult ShowModal()
        {
            AccountingCreateInvoicesVM objAccountingCreateInvoicesVM = new AccountingCreateInvoicesVM();

            return PartialView(GetVirtualPath("_CreateInvoice"), objAccountingCreateInvoicesVM);
        }

        public ActionResult CreateInvoice(AccountingCreateInvoicesVM objAccountingCreateInvoicesVM)
        {
            int result = 0;

           
            result = AdAccountingCreateInvoices.SaveINvoice(objAccountingCreateInvoicesVM);

            return Json(result);
           
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewAccounting/AccountingLiens/CreateInvoice/" + ViewName + ".cshtml";
            return path;
        }
    }
}
