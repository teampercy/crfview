﻿using CRFView.Adapters.Accounting.AccountingLiens.SmanCommission.ViewModels;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Accounting.Accounting_Liens
{
    public class SmanCommissionController : Controller
    {
        
        public ActionResult Index()
        {
            return View(GetVirtualPath("Index"));
        }

        public ActionResult ShowModal()
        {
            SmanCommissionVM objSmanCommissionVM = new SmanCommissionVM();
            return PartialView(GetVirtualPath("_SmanCommission"), objSmanCommissionVM);
        }


        [HttpPost]
        public string SmanCommissionDetails(SmanCommissionVM objSmanCommissionVM)
        {
            try
            {
                string PdfUrl = null;

                PdfUrl = ADClientFinDoc.PrintSmanCommission(objSmanCommissionVM);

                if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
                {
                    string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                    string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                    return (path);
                }
            }
            catch (Exception ex)
            {
                return "error";
            }
            return "error";
        }


        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewAccounting/AccountingLiens/SmanCommission/" + ViewName + ".cshtml";
            return path;
        }



    }
}
