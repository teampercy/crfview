﻿using CRFView.Adapters.Accounting.AccountingLiens.DeleteInvoiceRun.ViewModels;
using CRFView.Adapters.Accounting.AccountingCollections.CreateInvoices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Accounting.AccountingLiens.DeleteInvoiceRun
{
    public class DeleteInvoiceController : Controller
    {
        public ActionResult Index()
        {
            return View(GetVirtualPath("Index"));
        }

        public ActionResult ShowModal()
        {
            AccountingDeleteInvoicesVM objAccountingCreateInvoicesVM = new AccountingDeleteInvoicesVM();

            return PartialView(GetVirtualPath("_DeleteInvoice"), objAccountingCreateInvoicesVM);
        }

        public ActionResult DeleteInvoices(AccountingDeleteInvoicesVM objAccountingDeleteInvoicesVM)
        {
            int result = 0;


            result = AdAccountingCreateInvoices.DeleteINvoice(objAccountingDeleteInvoicesVM);

            return Json(result);

        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewAccounting/AccountingLiens/DeleteInvoiceRun/" + ViewName + ".cshtml";
            return path;
        }
    }
}
