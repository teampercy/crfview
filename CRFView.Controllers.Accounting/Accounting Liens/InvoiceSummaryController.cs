﻿using CRFView.Adapters.Accounting.AccountingLiens.InvoiceSummary.ViewModels;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Accounting.Accounting_Liens
{
    public class InvoiceSummaryController : Controller
    {

        public ActionResult Index()
        {
            return View(GetVirtualPath("Index"));
        }

        public ActionResult ShowModal()
        {
            InvoiceSummaryVM objInvoiceSummaryVM = new InvoiceSummaryVM();
            return PartialView(GetVirtualPath("_InvoiceSummary"), objInvoiceSummaryVM);
        }


     
        [HttpPost]
        public string InvoiceSummaryDetails(InvoiceSummaryVM objInvoiceSummaryVM)
        {
            try
            {
                string PdfUrl = null;

                PdfUrl = ADClientFinDoc.SaveInvoiceSummary(objInvoiceSummaryVM);

                if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
                {
                    string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                    string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                    return (path);
                }
            }
            catch (Exception ex)
            {
                return "error";
            }
            return "error";
        }



        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewAccounting/AccountingLiens/InvoiceSummary/" + ViewName + ".cshtml";
            return path;
        }

    }
}
