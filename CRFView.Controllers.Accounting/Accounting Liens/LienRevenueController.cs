﻿using CRFView.Adapters.Accounting.AccountingLiens.LienRevenue.ViewModels;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Controllers.Accounting.Accounting_Liens.LienRevenue
{
     public class LienRevenueController: Controller
    {
       

        [HttpPost]
        public ActionResult Index()
        {
            return View(GetVirtualPath("Index"));
        }

        [HttpPost]
        public ActionResult ShowModal()
        {
            LienRevenueVM objLienRevenueVM = new LienRevenueVM();


            return PartialView(GetVirtualPath("_LienRevenue"), objLienRevenueVM);

        }

        //Print CollectionReports 
        [HttpPost]
        public string LienRevenuePrint(LienRevenueVM objLienRevenueVM)
        {
           
            try
            {
                string PdfUrl = null;

                PdfUrl = ADClientFinDoc.PrintLienRevenue(objLienRevenueVM);


             if (PdfUrl != null && PdfUrl != "" && PdfUrl != "Error")
            {
                string fileName = Uri.EscapeDataString(System.IO.Path.GetFileName(PdfUrl));
                string path = Path.Combine(Url.Content("~"), "Output/Reports/", fileName);
                return (path);
            }
             }
            catch (Exception ex)
            {
                return "error";
            }
            return "error";
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewAccounting/AccountingLiens/LienRevenue/" + ViewName + ".cshtml";
            return path;
        }
    }
}
