﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using System.Data;
using HDS.DAL.COMMON;
using CRF.BLL.Providers;
using System.Web.Mvc;

namespace CRFView.Adapters
{
    public class CommonBindList
    {
        #region Properties

        public int Id { get; set; }
        public string Name { get; set; }
        public string Code { get; set; }
        public string ClientCode { get; set; }
        public string StateInitials { get; set; }
        public string DESKNUM { get; set; }

        public bool Selected { get; set; }
        public static int Jvalue { get; set; }

        #endregion

        #region Functions

        public static List<CommonBindList> GetClientList()
        {
            try
            {
                List<CommonBindList> IUList = new List<CommonBindList>();
                DataView ClientDataView = CRF.BLL.Queries.GetAllClients();
                DataTable dt = ClientDataView.ToTable();

                IUList = (from DataRow dr in dt.Rows
                          select new CommonBindList()
                          {
                              Id = Convert.ToInt16(dr["ClientId"]),
                              Name = Convert.ToString(dr["ClientName"]),
                              Code = Convert.ToString(dr["Clientcode"])
                          }).ToList();

                return IUList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetClientContract(int ClientId)
        {
            try
            {
                TableView tblClientContract = CRF.BLL.Queries.GetClientContractList(ClientId);
                DataTable dt = tblClientContract.ToTable();
                List<CommonBindList> ListClientContract = new List<CommonBindList>();
                if (tblClientContract.Count > 0)
                {
                    ListClientContract = (from DataRow dr in dt.Rows
                                          select new CommonBindList()
                                          {
                                              Id = Convert.ToInt32(dr["Id"]),
                                              Name = Convert.ToString(dr["Title"])
                                          }).ToList();
                }
                return ListClientContract;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetClientInterface()
        {

            TableView tblClientInterface = CRF.BLL.Queries.GetClientInterfaceList();
            DataTable dt = tblClientInterface.ToTable();
            List<CommonBindList> ListClientInterface = new List<CommonBindList>();
            if (tblClientInterface.Count > 0)
            {
                ListClientInterface = (from DataRow dr in dt.Rows
                                       select new CommonBindList()
                                       {
                                           Id = Convert.ToInt32(dr["Id"]),
                                           Name = Convert.ToString(dr["InterfaceName"])

                                       }).ToList();
            }
            return ListClientInterface;
        }

        public static List<CommonBindList> GetClientViewGroups()
        {
            try
            {
                List<CommonBindList> IUList = new List<CommonBindList>();
                TableView ClientViewGroupTV = CRF.BLL.Queries.ClientViewGroups();
                DataTable dt = ClientViewGroupTV.ToTable();

                IUList = (from DataRow dr in dt.Rows
                          select new CommonBindList()
                          {
                              Id = Convert.ToInt16(dr["Id"]),
                              Name = Convert.ToString(dr["ClientViewGroupName"])
                          }).ToList();

                return IUList;
            }

            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }

        }

        public static List<CommonBindList> GetSelectedClientList(string ClientCodeList)
        {
            TableView EntityList = CRF.BLL.Queries.GetSelectedClientList(ClientCodeList);
            DataTable dt = EntityList.ToTable();
            List<CommonBindList> CList = new List<CommonBindList>();
            CList = (from DataRow dr in dt.Rows
                     select new CommonBindList()
                     {
                         Code = Convert.ToString(dr["Clientcode"]),
                         Name = Convert.ToString(dr["Clientname"])

                     }).ToList();

            return CList;

        }

        public static List<CommonBindList> GetTaskNamesList()
        {
            try
            {

                TableView tblTask = CRF.BLL.Queries.GetTaskList();
                DataTable dt = tblTask.ToTable();
                List<CommonBindList> Listtasks = new List<CommonBindList>();
                if (tblTask.Count > 0)
                {
                    Listtasks = (from DataRow dr in dt.Rows
                                 select new CommonBindList()
                                 {
                                     Id = Convert.ToInt32(dr["PKID"]),
                                     Name = Convert.ToString(dr["taskname"])
                                 }).ToList();
                }
                return Listtasks;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetInternalUsersList()
        {
            try
            {
                List<CommonBindList> InternalUserList = new List<CommonBindList>();
                DataView InternalUserDataView = CRF.BLL.Queries.GetInternalUsers();
                DataTable dt = InternalUserDataView.ToTable();

                InternalUserList = (from DataRow dr in dt.Rows
                                    select new CommonBindList()
                                    {
                                        Id = Convert.ToInt16(dr["ID"]),
                                        Name = Convert.ToString(dr["UserName"])
                                    }).ToList();

                return InternalUserList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetClientUsersList()
        {
            try
            {
                List<CommonBindList> ClientUsersList = new List<CommonBindList>();
                DataView ClientUsersDataView = CRF.BLL.Queries.GetClientUsers();
                DataTable dt = ClientUsersDataView.ToTable();

                ClientUsersList = (from DataRow dr in dt.Rows
                                   select new CommonBindList()
                                   {
                                       Id = Convert.ToInt16(dr["userid"]),
                                       Name = Convert.ToString(dr["friendlyname"])

                                   }).ToList();

                return ClientUsersList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetSelectedUsersList(string UserIdList, string UserType)
        {

            TableView EntityList = CRF.BLL.Queries.GetSelectedUsersList(UserType, UserIdList);
            DataTable dt = EntityList.ToTable();
            List<CommonBindList> UserList = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["ID"]),
                                                 Name = Convert.ToString(dr["UserName"])

                                             }).ToList();

            return UserList;

        }

        public static List<CommonBindList> GetJobActionsList()
        {
            try
            {
                TableView tblJobActions = CRF.BLL.Queries.GetJobActionsList();
                DataTable dt = tblJobActions.ToTable();
                List<CommonBindList> JobActionsList = (from DataRow dr in dt.Rows
                                                       select new CommonBindList()
                                                       {
                                                           Id = Convert.ToInt16(dr["Id"]),
                                                           Name = Convert.ToString(dr["Description"])

                                                       }).ToList();

                return JobActionsList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetCollectionRateList()
        {
            try
            {
                TableView tblCollectinRates = CRF.BLL.Queries.GetCollectionRateList();
                DataTable dt = tblCollectinRates.ToTable();
                List<CommonBindList> CollectinRatesList = (from DataRow dr in dt.Rows
                                                           select new CommonBindList()
                                                           {
                                                               Id = Convert.ToInt16(dr["Id"]),
                                                               Name = Convert.ToString(dr["CollectionRateCode"])

                                                           }).ToList();

                return CollectinRatesList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetBillableEventTypeList()
        {
            try
            {
                BillableEventType objBillableEventType = new BillableEventType();
                TableView EntityList;
                Query query = new Query(objBillableEventType);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<CommonBindList> BillableEventTypeList = (from DataRow dr in dt.Rows
                                                              select new CommonBindList()
                                                              {
                                                                  Id = Convert.ToInt16(dr["Id"]),
                                                                  Name = Convert.ToString(dr["BillableEventName"])

                                                              }).ToList();

                return BillableEventTypeList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetPriceCodeList()
        {
            try
            {
                PriceCode objPriceCode = new PriceCode();
                TableView EntityList;
                Query query = new Query(objPriceCode);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<CommonBindList> PriceCodeList = (from DataRow dr in dt.Rows
                                                      select new CommonBindList()
                                                      {
                                                          Id = Convert.ToInt16(dr["Id"]),
                                                          Name = Convert.ToString(dr["PriceCodeName"])

                                                      }).ToList();

                return PriceCodeList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetLocationList()
        {
            try
            {
                TableView tblLocations = CRF.BLL.Queries.GetLocationList();
                DataTable dt = tblLocations.ToTable();
                List<CommonBindList> LocationList = (from DataRow dr in dt.Rows
                                                     select new CommonBindList()
                                                     {
                                                         Id = Convert.ToInt16(dr["LocationId"]),
                                                         Name = Convert.ToString(dr["Title"])

                                                     }).ToList();

                return LocationList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetCollectionStatusList()
        {
            try
            {
                TableView tblCollectionStatus = CRF.BLL.Queries.GetCollectionStatusList();
                DataTable dt = tblCollectionStatus.ToTable();
                List<CommonBindList> CollectionStatusList = (from DataRow dr in dt.Rows
                                                             select new CommonBindList()
                                                             {
                                                                 Id = Convert.ToInt16(dr["CollectionStatusId"]),
                                                                 Name = Convert.ToString(dr["Title"])

                                                             }).ToList();

                return CollectionStatusList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetPriorityList()
        {
            try
            {
                TableView tblPriority = CRF.BLL.Queries.GetPriorityList();
                DataTable dt = tblPriority.ToTable();
                List<CommonBindList> PriorityList = (from DataRow dr in dt.Rows
                                                     select new CommonBindList()
                                                     {
                                                         Id = Convert.ToInt16(dr["Id"]),
                                                         Name = Convert.ToString(dr["FriendlyName"])

                                                     }).ToList();

                return PriorityList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetCollectionActionTypes()
        {
            try
            {
                TableView tblCollectionActionTypes = CRF.BLL.Queries.GetCollectionActionTypes();
                DataTable dt = tblCollectionActionTypes.ToTable();
                List<CommonBindList> CollectionActionTypesList = (from DataRow dr in dt.Rows
                                                                  select new CommonBindList()
                                                                  {
                                                                      Id = Convert.ToInt16(dr["Id"]),
                                                                      Name = Convert.ToString(dr["Description"]),
                                                                      Code = Convert.ToString(dr["TypeCode"])

                                                                  }).ToList();

                return CollectionActionTypesList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetLawList()
        {
            try
            {
                TableView tblLawList = CRF.BLL.Queries.GetLawList();
                DataTable dt = tblLawList.ToTable();
                List<CommonBindList> LawList = (from DataRow dr in dt.Rows
                                                select new CommonBindList()
                                                {
                                                    Name = Convert.ToString(dr["LawList"]),
                                                    Code = Convert.ToString(dr["PKID"]) + "-" + Convert.ToString(dr["LawListCode"])
                                                }).ToList();

                return LawList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetCollectionStatusGroupList()
        {
            try
            {
                TableView tblStatusGroupList = CRF.BLL.Queries.CollectionStatusGroup();
                DataTable dt = tblStatusGroupList.ToTable();
                List<CommonBindList> StatusGroupList = (from DataRow dr in dt.Rows
                                                        select new CommonBindList()
                                                        {
                                                            Id = Convert.ToInt32(dr["Id"]),
                                                            Name = Convert.ToString(dr["StatusGroupDescription"])

                                                        }).ToList();

                return StatusGroupList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetReportFilesList()
        {
            try
            {
                TableView tblReportFilesList = CRF.BLL.Queries.GetCollectionReportFiles();
                DataTable dt = tblReportFilesList.ToTable();
                List<CommonBindList> ReportFilesList = (from DataRow dr in dt.Rows
                                                        select new CommonBindList()
                                                        {
                                                            Id = Convert.ToInt32(dr["ReportFileId"]),
                                                            Name = Convert.ToString(dr["ReportFileName"])

                                                        }).ToList();

                return ReportFilesList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetReportCategoryList()
        {
            try
            {
                TableView tblReportCategoryList = CRF.BLL.Queries.GetReportCatagories();
                DataTable dt = tblReportCategoryList.ToTable();
                List<CommonBindList> ReportCategoryList = (from DataRow dr in dt.Rows
                                                           select new CommonBindList()
                                                           {
                                                               Id = Convert.ToInt32(dr["Id"]),
                                                               Name = Convert.ToString(dr["ReportCategory"])

                                                           }).ToList();

                return ReportCategoryList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetCollectionUsers()
        {
            try
            {
                TableView tblCollectionUsersList = CRF.BLL.Queries.GetCollectionUsers();
                DataTable dt = tblCollectionUsersList.ToTable();
                List<CommonBindList> CollectionUsersList = (from DataRow dr in dt.Rows
                                                            select new CommonBindList()
                                                            {
                                                                Code = Convert.ToString(dr["Id"]),
                                                                Name = Convert.ToString(dr["UserName"])

                                                            }).ToList();

                return CollectionUsersList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetLocationUsers(int aLocationId)
        {
            try
            {
                TableView tblLocationUsersList = CRF.BLL.Queries.GetLocationUsers(aLocationId);
                DataTable dt = tblLocationUsersList.ToTable();
                List<CommonBindList> LocationUsersList = (from DataRow dr in dt.Rows
                                                          select new CommonBindList()
                                                          {
                                                              Code = Convert.ToString(dr["UserId"]),
                                                              Name = Convert.ToString(dr["UserName"])
                                                          }).ToList();

                return LocationUsersList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetJobViewDesksList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.JobViewDeskList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["DeskId"]),
                                                 Name = Convert.ToString(dr["DeskNum"]),
                                                 Code = Convert.ToString(dr["FriendlyName"])

                                             }).ToList();
                return List;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetJobStateList()
        {
            try
            {
                StateInfo mytable = new StateInfo();
                Query query = new Query(mytable);
                TableView tblList = DBO.Provider.GetTableView(query.SelectAll());
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["StateInitials"])

                                             }).ToList();
                return List;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetStatusGroupList()
        {
            try
            {
                JobStatusGroup mytable = new JobStatusGroup();
                Query query = new Query(mytable);
                TableView tblList = DBO.Provider.GetTableView(query.SelectAll());
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["StatusGroupDescription"])

                                             }).ToList();
                return List;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetJobDeskList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.JobDeskList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["DeskId"]),
                                                 Name = Convert.ToString(dr["FriendlyName"]),
                                                 Code = Convert.ToString(dr["DeskNum"])
                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }
        public static List<CommonBindList> GetJobStatusList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.JobStatusList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["FriendlyName"])

                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetJobViewStatusList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.JobViewStatusList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["FriendlyName"])

                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }
        public static List<CommonBindList> GetJobActionCategoriesList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.JobActionCategoriesList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Code = Convert.ToString(dr["TypeCode"]),
                                                 Name = Convert.ToString(dr["Description"])

                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }
        public static List<CommonBindList> GetJobDeskGroupList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.JobDeskGroupList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["DeskGroupName"])

                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }
        public static List<CommonBindList> GetMailReturnJobActionList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.MailReturnJobActionList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["Description"])

                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }
        public static List<CommonBindList> GetMailReturnReasonList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.MailReturnReasonList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["ReasonType"])

                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }
        public static List<CommonBindList> GetMailReturnResearchList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.MailReturnResearchList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["ResearchType"])

                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }
        public static List<CommonBindList> GetMailDeliveredResultList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.MailDeliveredResultList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["ResultDescription"])

                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }
        public static List<CommonBindList> GetMailDeliveredJobActionList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.MailDeliveredJobActionList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["Description"])

                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }
        public static List<CommonBindList> GetLienReportsList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.LienReportsList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["ReportId"]),
                                                 Name = Convert.ToString(dr["Description"])

                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }
        public static List<CommonBindList> GetReportFieldNamesList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.ReportFieldNamesList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["FieldId"]),
                                                 Name = Convert.ToString(dr["FieldName"])

                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }

        }
        public static List<CommonBindList> GetLienReportFiles()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.GetLienReportFiles();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["ReportFileId"]),
                                                 Name = Convert.ToString(dr["ReportFilename"])

                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }
        public static List<CommonBindList> GetJobDeskRulesList()
        {
            try
            {
                JobDeskRules mytable = new JobDeskRules();
                Query query = new Query(mytable);
                TableView tblList = DBO.Provider.GetTableView(query.SelectAll());
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["Description"])

                                             }).ToList();
                return List;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static IEnumerable<SelectListItem> GetMainClientList()
        {
            try
            {
                TableView tblList = (TableView)CRF.BLL.Queries.GetMainClients();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["Descr"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetSalesTeamList()
        {
            try
            {
                TableView tblList = (TableView)CRF.BLL.Queries.GetSalesTeams();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["TeamName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetServiceTeamList()
        {
            try
            {
                TableView tblList = (TableView)CRF.BLL.Queries.GetServiceTeams();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["TeamName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetClientTypeList()
        {
            try
            {
                TableView tblList = (TableView)CRF.BLL.Queries.GetClientType();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["ClientTypeId"]),
                                                 Name = Convert.ToString(dr["ClientTypeName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetContactTypeList()
        {
            try
            {
                HDS.DAL.Providers.ITable dt_ITable;
                ContractType dt_CT = new ContractType();
                dt_ITable = dt_CT;
                TableView tblList = (TableView)CRF.BLL.Queries.GetViewForConfigTable(ref dt_ITable);
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["ContractTypeName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetProductTypeList()
        {
            try
            {
                HDS.DAL.Providers.ITable dt_ITable;
                Product dt_Pt = new Product();
                dt_ITable = dt_Pt;
                TableView tblList = (TableView)CRF.BLL.Queries.GetViewForConfigTable(ref dt_ITable);
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["ID"]),
                                                 Name = Convert.ToString(dr["ProdDesc"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetAssociationList()
        {
            try
            {
                HDS.DAL.Providers.ITable dt_ITable;
                ClientAssociation dt_CA = new ClientAssociation();
                dt_ITable = dt_CA;
                TableView tblList = (TableView)CRF.BLL.Queries.GetViewForConfigTable(ref dt_ITable);
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["AssociationName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetFinDocTypesList()
        {
            try
            {
                HDS.DAL.Providers.ITable dt_ITable;
                FinDocType dt_FDT = new FinDocType();
                dt_ITable = dt_FDT;
                TableView tblList = (TableView)CRF.BLL.Queries.GetViewForConfigTable(ref dt_ITable);
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["FinDocTypeId"]),
                                                 Name = Convert.ToString(dr["FinDocTypeName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetFinDocCycleList()
        {
            try
            {
                HDS.DAL.Providers.ITable dt_ITable;
                FinDocCycle dt_FDC = new FinDocCycle();
                dt_ITable = dt_FDC;
                TableView tblList = (TableView)CRF.BLL.Queries.GetViewForConfigTable(ref dt_ITable);
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["CycleDescription"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetContactPlanList()
        {
            try
            {
                var ContactPlanView = DBO.Provider.GetTableView("Select ContactPlanId, ContactPlanCode + '-' + ContactPlan as FriendlyName From ContactPlan Where IsTenDayContactPlan = 0");
                DataTable dt = ContactPlanView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["ContactPlanId"]),
                                                 Name = Convert.ToString(dr["FriendlyName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");

            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetTenDatContactPlan()
        {
            try
            {
                var TenDayContactPlanView = DBO.Provider.GetTableView("Select ContactPlanId, ContactPlanCode + '-' + ContactPlan as FriendlyName From ContactPlan Where IsTenDayContactPlan = 1");
                DataTable dt = TenDayContactPlanView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["ContactPlanId"]),
                                                 Name = Convert.ToString(dr["FriendlyName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");

            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetContractListForClient(int ClientId)
        {
            try
            {
                string sql = "Select * from ClientContract Where BillingClientId= " + ClientId;
                var ContactView = DBO.Provider.GetTableView(sql);
                DataTable dt = ContactView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["Title"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }


        public static IEnumerable<SelectListItem> GetCommissionRates()
        {
            try
            {
                string sql = "Select id, substring(VW.CollectionRateCode,1,3) + '-' + VW.CollectionRateName AS FriendlyName From CollectionRate VW";
                var ContactView = DBO.Provider.GetTableView(sql);
                DataTable dt = ContactView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["id"]),
                                                 Name = Convert.ToString(dr["FriendlyName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetFinDocCollection()
        {
            try
            {
                string sql = "Select FinDocTypeId, FinDocTypeName From FinDocType Where IsAvailableInColl = 1";
                var FinDocView = DBO.Provider.GetTableView(sql);
                DataTable dt = FinDocView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["FinDocTypeId"]),
                                                 Name = Convert.ToString(dr["FinDocTypeName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetViewGroups()
        {
            try
            {
                List<CommonBindList> IUList = new List<CommonBindList>();
                TableView ClientViewGroupTV = CRF.BLL.Queries.ClientViewGroups();
                DataTable dt = ClientViewGroupTV.ToTable();

                IUList = (from DataRow dr in dt.Rows
                          select new CommonBindList()
                          {
                              Id = Convert.ToInt16(dr["Id"]),
                              Name = Convert.ToString(dr["ClientViewGroupName"])
                          }).ToList();
                return new SelectList(IUList, "Id", "Name");
            }

            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetReportTypes()
        {
            try
            {
                List<CommonBindList> ReportTypeList = new List<CommonBindList>();
                TableView ReportView = DBO.Provider.GetTableView("Select * from Report Where Isdisabled = 0");
                DataTable dt = ReportView.ToTable();

                ReportTypeList = (from DataRow dr in dt.Rows
                                  select new CommonBindList()
                                  {
                                      Id = Convert.ToInt16(dr["PKId"]),
                                      Name = Convert.ToString(dr["TaskName"])
                                  }).ToList();
                return new SelectList(ReportTypeList, "Id", "Name");
            }

            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetInternalUsersOpenIssues()
        {
            try
            {
                List<CommonBindList> InternalUserList = new List<CommonBindList>();
                TableView UserView = CRF.BLL.Queries.GetInternalUsers();
                DataTable dt = UserView.ToTable();

                InternalUserList = (from DataRow dr in dt.Rows
                                    select new CommonBindList()
                                    {
                                        Id = Convert.ToInt16(dr["ID"]),
                                        Name = Convert.ToString(dr["UserName"])
                                    }).ToList();
                return new SelectList(InternalUserList, "Id", "Name");
            }

            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetInternalUsersSelectList()
        {
            try
            {
                List<CommonBindList> InternalUserList = new List<CommonBindList>();
                DataView InternalUserDataView = CRF.BLL.Queries.GetInternalUsers();
                DataTable dt = InternalUserDataView.ToTable();

                InternalUserList = (from DataRow dr in dt.Rows
                                    select new CommonBindList()
                                    {
                                        Id = Convert.ToInt16(dr["ID"]),
                                        Name = Convert.ToString(dr["UserName"])
                                    }).ToList();
                return new SelectList(InternalUserList, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetCollectionServiceCodes()
        {
            try
            {
                List<CommonBindList> ReportTypeList = new List<CommonBindList>();
                TableView ReportView = DBO.Provider.GetTableView("Select * from vwClientCollectionServiceCode");
                DataTable dt = ReportView.ToTable();

                ReportTypeList = (from DataRow dr in dt.Rows
                                  select new CommonBindList()
                                  {
                                      Id = Convert.ToInt16(dr["ClientId"]),
                                      Name = Convert.ToString(dr["FriendlyName"])
                                  }).ToList();
                return new SelectList(ReportTypeList, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetStates()
        {
            try
            {
                List<CommonBindList> StateList = new List<CommonBindList>();
                StateInfo mytable = new StateInfo();
                Query query = new Query(mytable);
                TableView tblList = DBO.Provider.GetTableView(query.SelectAll());
                DataTable dt = tblList.ToTable();

                StateList = (from DataRow dr in dt.Rows
                             select new CommonBindList()
                             {
                                 Id = Convert.ToInt16(dr["Id"]),
                                 Name = Convert.ToString(dr["StateInitials"])
                             }).ToList();
                return new SelectList(StateList, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetCollectionStatuses()
        {
            try
            {
                string sql = "Select * from collectionstatus";
                var CollectionStatusView = DBO.Provider.GetTableView(sql);
                DataTable dt = CollectionStatusView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["CollectionStatusId"]),
                                                 Name = Convert.ToString(dr["CollectionStatus"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetCollectionStatusGroup()
        {
            try
            {
                string sql = "Select * from collectionstatusGroup";
                var collectionstatusGroupView = DBO.Provider.GetTableView(sql);
                DataTable dt = collectionstatusGroupView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["StatusGroupDescription"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetLocation()
        {
            try
            {
                string sql = "Select LocationId, Location from Location";
                var LocationView = DBO.Provider.GetTableView(sql);
                DataTable dt = LocationView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["LocationId"]),
                                                 Name = Convert.ToString(dr["Location"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }

        }

        public static IEnumerable<SelectListItem> GetAttorneyInfo()
        {
            try
            {
                string sql = "Select PKID, AttyCode from AttorneyInfo";
                var AttorneyInfoView = DBO.Provider.GetTableView(sql);
                DataTable dt = AttorneyInfoView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["PKID"]),
                                                 Name = Convert.ToString(dr["AttyCode"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }

        }

        public static IEnumerable<SelectListItem> GetCourtHouse()
        {
            try
            {
                string sql = "Select PKID, CourtId,Venue from CourtHouse";
                var CourtHouseView = DBO.Provider.GetTableView(sql);
                DataTable dt = CourtHouseView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["PKID"]),
                                                 //Name = Convert.ToString(dr["CourtId"])
                                                 Name = Convert.ToString(dr["CourtId"]) + "-" + Convert.ToString(dr["Venue"])

                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }

        }


        public static IEnumerable<SelectListItem> GetProcessServer()
        {
            try
            {
                string sql = "Select PKID, ProcessServerNo,MarshalOffice from ProcessServer";
                var CourtHouseView = DBO.Provider.GetTableView(sql);
                DataTable dt = CourtHouseView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["PKID"]),
                                                 //Name = Convert.ToString(dr["ProcessServerNo"])
                                                 Name = Convert.ToString(dr["ProcessServerNo"]) + "-" + Convert.ToString(dr["MarshalOffice"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }

        }

        public static IEnumerable<SelectListItem> GetLocationDebt()
        {
            try
            {
                string sql = "Select LocationId, Location, LocationDescr from Location";
                var LocationView = DBO.Provider.GetTableView(sql);
                DataTable dt = LocationView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["LocationId"]),
                                                 Name = Convert.ToString(dr["Location"]) + "-" + Convert.ToString(dr["LocationDescr"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }

        }


        public static IEnumerable<SelectListItem> GetFrequency()
        {
            try
            {
                List<CommonBindList> List = new List<CommonBindList>();
                List.Add(new CommonBindList() { Id = 1, Name = "Weekly" });
                List.Add(new CommonBindList() { Id = 2, Name = "Bi-Weekly" });
                List.Add(new CommonBindList() { Id = 3, Name = "Monthly" });

                return new SelectList(List, "Id", "Name");

            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }

        }

        public static IEnumerable<SelectListItem> GetActionHistoryCodes()
        {
            try
            {
                string sql = "Select Id, Description from JobActions";
                var JobActionsView = DBO.Provider.GetTableView(sql);
                DataTable dt = JobActionsView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["Description"])
                                             }).ToList();

                 return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }


        public static List<CommonBindList> GetActionHistoryCodes2()
        {
            try
            {
                string sql = "Select Id, ActionCode, Description from CollectionActions";
                var CollectionActionsView = DBO.Provider.GetTableView(sql);
                DataTable dt = CollectionActionsView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["ActionCode"]) + "-" + Convert.ToString(dr["Description"])
                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }


        public static List<CommonBindList> GetActionHistoryCodesByActionType(string ActionTypeValue)
        {
            try
            {
                string sql = null;
                sql = "Select Id, ActionCode, Description from CollectionActions ";

                switch (ActionTypeValue)
                {
                    case "P":
                        sql += "where IsCollection = 1 and CategoryType = 'P'";
                        break;
                    case "S":
                        sql += "where IsCollection = 1 and CategoryType = 'S'";
                        break;
                    case "F":
                        sql += "where IsForwarding = 1";
                        break;
                    case "C":
                        sql += "where IsSmallClaims = 1";
                        break;
                    default:
                        sql += "where IsCollection = 1 and CategoryType = 'P'";
                        break;
                }
                sql += "ORDER BY CASE WHEN CollectionActions.ActionCode LIKE '[0-9]%' THEN 2 ELSE 1 END, CollectionActions.ActionCode";
                var CollectionActionsView = DBO.Provider.GetTableView(sql);
                DataTable dt = CollectionActionsView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["ActionCode"]) + "-" + Convert.ToString(dr["Description"])
                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }


        public static IEnumerable<SelectListItem> GetSelectNames(string DebtAccountId)
        {
            try
            {
                string mysql = null;
                mysql = "select DebtAddressId, Case When ContactName is Null Then CAST(DebtAddressTypeId as CHAR(1)) + ' - ' + AddressLine1 When ";
                mysql += "AddressLine1 is Null Then CAST(DebtAddressTypeId as CHAR(1)) + ' - ' + ContactName Else ";
                mysql += "CAST(DebtAddressTypeId as CHAR(1)) + ' - ' + ContactName + ' - ' + AddressLine1 End as FriendlyName ";
                mysql += "from DebtAddress join Debt on debt.debtid = debtAddress.debtid ";
                mysql += "Join DebtAccount on debt.debtid = debtaccount.debtid where debtaccountid= '" + DebtAccountId + "' order by DebtAddressId";

                var CollectionActionsView = DBO.Provider.GetTableView(mysql);
                DataTable dt = CollectionActionsView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["DebtAddressId"]),
                                                 Name = Convert.ToString(dr["FriendlyName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }


        public static IEnumerable<SelectListItem> GetCreditReportStatus()
        {
            try
            {
                string sql = "Select *, StatusCode +' - ' + Description as FriendlyName from CreditReportStatus order by Description";
                var CollectionCreditReportStatus = DBO.Provider.GetTableView(sql);
                DataTable dt = CollectionCreditReportStatus.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["FriendlyName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }


        public static IEnumerable<SelectListItem> GetDocumentType()
        {
            try
            {
                List<CommonBindList> List = new List<CommonBindList>();

                List.Add(new CommonBindList() { Name = "Contract", Code = "Contract" });
                List.Add(new CommonBindList() { Name = "Dispute Docs", Code = "Dispute Docs" });
                List.Add(new CommonBindList() { Name = "Internal", Code = "Internal" });
                List.Add(new CommonBindList() { Name = "Invoices", Code = "Invoices" });
                List.Add(new CommonBindList() { Name = "Misc", Code = "Misc" });
                List.Add(new CommonBindList() { Name = "Personal Guarantee", Code = "Personal Guarantee" });
                List.Add(new CommonBindList() { Name = "Proof Of Debt", Code = "Proof Of Debt" });

                return new SelectList(List, "Name", "Code");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetAttyInfo()
        {
            try
            {
                string sql = "Select PKID, AttyCode + '-' + (case when AttorneyName is null Then ' ' else AttorneyName end) as friendlyname from AttorneyInfo order by friendlyname";
                var AttyInfoView = DBO.Provider.GetTableView(sql);
                DataTable dt = AttyInfoView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["PKID"]),
                                                 Name = Convert.ToString(dr["friendlyname"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }

        }

        public static IEnumerable<SelectListItem> GetLawListInfo()
        {
            try
            {
                string sql = "Select PKID, LawListCode from LawListInfo order by LawListCode";
                var AttyInfoView = DBO.Provider.GetTableView(sql);
                DataTable dt = AttyInfoView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["PKID"]),
                                                 Name = Convert.ToString(dr["LawListCode"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }

        }

        public static IEnumerable<SelectListItem> GetCourtIdList(string State)
        {
            try
            {
                string mysql = null;
                mysql = "Select PKID, CourtId + '-' + VenueAdd as friendlyname from CourtHouse Where VenueState = '" + State + "' order by CourtId";
                var CollectionActionsView = DBO.Provider.GetTableView(mysql);
                DataTable dt = CollectionActionsView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["PKID"]),
                                                 Name = Convert.ToString(dr["friendlyname"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }


        public static IEnumerable<SelectListItem> GetProcessServerIdList()
        {
            try
            {
                string mysql = null;
                mysql = "Select PKID, MarshalOffice from ProcessServer Order By MarshalOffice";
                var CollectionActionsView = DBO.Provider.GetTableView(mysql);
                DataTable dt = CollectionActionsView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["PKID"]),
                                                 Name = Convert.ToString(dr["MarshalOffice"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static List<CommonBindList> GetLetterTypeList(string LetterTypeValue)
        {
            try
            {
                string sql = null;
                sql = "Select PKID, LetterCode , LetterDescr from CollectionLetters vw Where IsInactive = 0 and ";
                switch (LetterTypeValue)
                {
                    case "":
                        sql += " 1 = 1";
                        break;
                    case "D":
                        sql += " LetterType = 'D'";
                        break;
                    case "DA":
                        sql += " LetterType = 'DA'";
                        break;
                    case "C":
                        sql += " LetterType = 'C'";
                        break;
                    case "CL":
                        sql += " LetterType = 'CL'";
                        break;
                    case "L":
                        sql += " LetterType = 'L'";
                        break;
                    case "S":
                        sql += " LetterType = 'S'";
                        break;
                    case "M":
                        sql += " LetterType = 'M'";
                        break;
                    default:
                        sql += " 1 = 1";
                        break;
                }
                sql += "ORDER BY CASE WHEN LetterCode LIKE '[0-9]%' THEN 2 ELSE 1 END, LetterCode";
                var CollectionActionsView = DBO.Provider.GetTableView(sql);
                DataTable dt = CollectionActionsView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["PKID"]),
                                                 Name = Convert.ToString(dr["LetterCode"]) + "-" + Convert.ToString(dr["LetterDescr"])
                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetStatusReportLetterTypeList(string LetterTypeValue)
        {
            try
            {
                string sql = null;
                sql = "Select PKID, LetterCode , Description from CollectionCustomText vw Where ";
                switch (LetterTypeValue)
                {
                    case "C":
                        sql += " LetterType = 'C'";
                        break;
                    case "CL":
                        sql += " LetterType = 'CL'";
                        break;
                    case "D":
                        sql += " LetterType = 'D'";
                        break;
                    case "DA":
                        sql += " LetterType = 'DA'";
                        break;
                    case "S":
                        sql += " LetterType = 'S'";
                        break;
                    case "L":
                        sql += " LetterType = 'L'";
                        break;
                    default:
                        sql += " LetterType = 'C'";
                        break;
                }
                sql += "ORDER BY CASE WHEN LetterCode LIKE '[0-9]%' THEN 2 ELSE 1 END, LetterCode";
                var CollectionActionsView = DBO.Provider.GetTableView(sql);
                DataTable dt = CollectionActionsView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["PKID"]),
                                                 Name = Convert.ToString(dr["LetterCode"]) + "-" + Convert.ToString(dr["Description"])
                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }

        public static IEnumerable<SelectListItem> GetLocationIdList()
        {
            try
            {
                string mysql = null;
                mysql = "Select LocationId, Location + '-' + LocationDescr as friendlyname from Location Where IsInactive = 0";
                var CollectionActionsView = DBO.Provider.GetTableView(mysql);
                DataTable dt = CollectionActionsView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["LocationId"]),
                                                 Name = Convert.ToString(dr["friendlyname"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }


        public static IEnumerable<SelectListItem> GetClientIdList()
        {
            try
            {
                string sql = "Select ClientId As ClientId,  ClientCode, ClientCode + '-' + ClientName As ClientName from Client Where IsCollectionClient = 1 and IsBranch = 0 and IsInactive = 0 Order by ClientCode";
                var ClientIdList = DBO.Provider.GetTableView(sql);
                DataTable dt = ClientIdList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["ClientId"]),
                                                 Name = Convert.ToString(dr["ClientName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetUserIdList()
        {
            try
            {
                string sql = "select ID, UserName As UserName from Portal_users where InternalUser = 1 order by UserName";
                var ClientIdList = DBO.Provider.GetTableView(sql);
                DataTable dt = ClientIdList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["ID"]),
                                                 Name = Convert.ToString(dr["UserName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }


        public static IEnumerable<SelectListItem> GetBatchIdList()
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                string sql = "Select BatchId As BatchId, BatchStatus, BatchDate from Batch Where SubmittedById = '" +user.Id + "' and batchtype in ('COLL', 'CLVW') AND BatchStatus = 'OPEN' order by BatchId asc";
                var BatchIdListView = DBO.Provider.GetTableView(sql);
                DataTable dt = BatchIdListView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["BatchId"]),
                                                 Name = Convert.ToString(dr["BatchId"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }

        }

        public static List<CommonBindList> GetServiceCodesList()
        {
            try
            {
                List<CommonBindList> IUList = new List<CommonBindList>();
                DataView ClientDataView = CRF.BLL.Queries.GetServiceCodes();
                DataTable dt = ClientDataView.ToTable();

                IUList = (from DataRow dr in dt.Rows
                          select new CommonBindList()
                          {
                              Id = Convert.ToInt16(dr["ClientId"]),
                              Name = Convert.ToString(dr["UniqueId"]),
                              Code = Convert.ToString(dr["FriendlyName"])
                          }).ToList();

                return IUList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static IEnumerable<SelectListItem> GetCommRateIdList()
        {
            try
            {
                //CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                string sql = "Select * from collectionrate order by CollectionRateCode";
                var BatchIdListView = DBO.Provider.GetTableView(sql);
                DataTable dt = BatchIdListView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["CollectionRateCode"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }

        }

        public static List<CommonBindList> GetBatchIdsList()
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                List<CommonBindList> IUList = new List<CommonBindList>();
                TableView EntityList;
                string sql = "Select BatchId As BatchId, BatchStatus, BatchDate from Batch Where SubmittedById = '" + user.Id + "' and batchtype in ('COLL', 'CLVW') AND BatchStatus = 'OPEN' order by BatchId desc";
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();

                IUList = (from DataRow dr in dt.Rows
                          select new CommonBindList()
                          {
                              Id = Convert.ToInt32(dr["BatchId"]),
                              Name = Convert.ToString(dr["BatchId"])
                          }).ToList();

                return IUList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }


        public static IEnumerable<SelectListItem> GetServiceCodes()
        {
            try
            {
                string sql = "Select * from vwClientCollectionServiceCode Order by ClientCode";
                var ClientIdList = DBO.Provider.GetTableView(sql);
                DataTable dt = ClientIdList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["ClientId"]),
                                                 Name = Convert.ToString(dr["FriendlyName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }




        public static IEnumerable<SelectListItem> GetNewCommRateIdList()
        {
            try
            {
                //CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                string sql = "Select * from collectionrate order by CollectionRateCode";
                var BatchIdListView = DBO.Provider.GetTableView(sql);
                DataTable dt = BatchIdListView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["CollectionRateCode"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }

        }

        public static IEnumerable<SelectListItem> GetNewClientIdList()
        {
            try
            {
                string sql = "Select ClientId As ClientId,  ClientCode, ClientCode + '-' + ClientName As ClientName from Client Where IsCollectionClient = 1 and IsBranch = 0 and IsInactive = 0 Order by ClientCode";
                var ClientIdList = DBO.Provider.GetTableView(sql);
                DataTable dt = ClientIdList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["ClientId"]),
                                                 Name = Convert.ToString(dr["ClientName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetDeskIdList()
        {
            try
            {
                string sql = "select *, Location + '-' + LocationDescr as FriendlyName from Location where IsInactive = 0 Order by Location";
                var ClientIdList = DBO.Provider.GetTableView(sql);
                DataTable dt = ClientIdList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["LocationId"]),
                                                 Name = Convert.ToString(dr["FriendlyName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetStatusIdList()
        {
            try
            {
                string sql = " select *,CollectionStatus as FriendlyName from CollectionStatus order by CollectionStatus";
                var ClientIdList = DBO.Provider.GetTableView(sql);
                DataTable dt = ClientIdList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["CollectionStatusId"]),
                                                 Name = Convert.ToString(dr["FriendlyName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetNewDeskIdList()
        {
            try
            {
                string sql = "select *, Location + '-' + LocationDescr as FriendlyName from Location where IsInactive = 0 Order by Location";
                var ClientIdList = DBO.Provider.GetTableView(sql);
                DataTable dt = ClientIdList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["LocationId"]),
                                                 Name = Convert.ToString(dr["FriendlyName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetNewActionIdList()
        {
            try
            {
                string sql = "select *, Description as FriendlyName from CollectionActions Order by FriendlyName";
                var ClientIdList = DBO.Provider.GetTableView(sql);
                DataTable dt = ClientIdList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["FriendlyName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetLiensNewDeskIdList()
        {
            try
            {
                string sql = "select *,DESKNUM + '-' + DeskName as FriendlyName from jobdesks";
                var ClientIdList = DBO.Provider.GetTableView(sql);
                DataTable dt = ClientIdList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["DeskId"]),
                                                 Name = Convert.ToString(dr["FriendlyName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetLiensNewActionIdList()
        {
            try
            {
                string sql = "select *,Description as FriendlyName from jobactions";
                var ClientIdList = DBO.Provider.GetTableView(sql);
                DataTable dt = ClientIdList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["FriendlyName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetReportsList(string ReportCategory)
        {
            try
            {
                string sql = "Select ReportId,ReportName, Description,  PrintFrom, ReportCategory, ReportFilename, Reporttype from vwCollectionReportsList Where ReportCategory = '" + ReportCategory + "' And PrintFrom in ('B', 'R') Order By Description";
                var BatchIdListView = DBO.Provider.GetTableView(sql);
                DataTable dt = BatchIdListView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["ReportId"]),
                                                 Name = Convert.ToString(dr["Description"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }

        }

        public static List<CommonBindList> GetReportIdListByCategoryType(string ReportCategoryValue)
        {
            try
            {
                string sql = null;
                sql = "Select ReportId,ReportName, Description,  PrintFrom, ReportCategory, ReportFilename, Reporttype from vwCollectionReportsList Where ReportCategory = '" + ReportCategoryValue + "'";
                sql += "And PrintFrom in ('B', 'R') Order By Description";

                var CollectionActionsView = DBO.Provider.GetTableView(sql);
                DataTable dt = CollectionActionsView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["ReportId"]),
                                                 Name = Convert.ToString(dr["Description"])
                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }

        public static IEnumerable<SelectListItem> GetDeskList()
        {
            try
            {
                TableView tblList = (TableView)CRF.BLL.Queries.GetLocationList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["LocationId"]),
                                                 Name = Convert.ToString(dr["Title"])
                                             }).ToList();
                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static List<CommonBindList> GetLienClientIdList()
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                TableView tblList = CRF.BLL.Providers.LiensDataEntry.GetLienClients(user);
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["ClientId"]),
                                                 Name = Convert.ToString(dr["ClientName"])
                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetLienUserIdList()
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                TableView tblList = CRF.BLL.Providers.LiensVerifiers.GetLienUsers(user);
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["ID"]),
                                                 Name = Convert.ToString(dr["UserName"])
                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }

        public static IEnumerable<SelectListItem> GetOpenLienBatchIdList()
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                string sql = "Select BatchId As BatchId, BatchStatus, BatchDate from Batch Where SubmittedById = '" + user.Id + "' and batchtype in ('LIEN', 'LIENCV') AND BatchStatus = 'OPEN' order by BatchId desc";
                var BatchIdListView = DBO.Provider.GetTableView(sql);
                DataTable dt = BatchIdListView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["BatchId"]),
                                                 Name = Convert.ToString(dr["BatchId"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }

        }

        public static List<CommonBindList> GetLienJobStateIdList()
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                TableView tblList = CRF.BLL.Providers.LiensDataEntry.GetStateInfo(user);
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Name = Convert.ToString(dr["StateInitials"]),
                                                 Code = Convert.ToString(dr["StateInitials"])
                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetJobActionIdList()
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                TableView tblList = CRF.BLL.Providers.LiensDataEntry.GetNewJobEntryActions(user, false);
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["Description"])
                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetClientInterfaceIdList()
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                List<CommonBindList> IUList = new List<CommonBindList>();
                DataView ClientDataView = CRF.BLL.Queries.GetCollectionInterfaces(user);
                DataTable dt = ClientDataView.ToTable();

                IUList = (from DataRow dr in dt.Rows
                          select new CommonBindList()
                          {
                              Id = Convert.ToInt32(dr["Id"]),
                              Name = Convert.ToString(dr["InterfaceDescr"])
                          }).ToList();

                return IUList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetLienClientInterfaceIdList()
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                List<CommonBindList> IUList = new List<CommonBindList>();
                DataView ClientDataView = CRF.BLL.Queries.GetLienInterfaces(user);
                DataTable dt = ClientDataView.ToTable();

                IUList = (from DataRow dr in dt.Rows
                          select new CommonBindList()
                          {
                              Id = Convert.ToInt32(dr["Id"]),
                              Name = Convert.ToString(dr["InterfaceDescr"])
                          }).ToList();

                return IUList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        public static List<CommonBindList> GetLienClientInterfaceIdForImportJobViewList()
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                List<CommonBindList> IUList = new List<CommonBindList>();
                DataView ClientDataView = CRF.BLL.Queries.GetJobViewInterfaces(user);
                DataTable dt = ClientDataView.ToTable();

                IUList = (from DataRow dr in dt.Rows
                          select new CommonBindList()
                          {
                              Id = Convert.ToInt32(dr["Id"]),
                              Name = Convert.ToString(dr["InterfaceDescr"])
                          }).ToList();

                return IUList;
            }
            catch (Exception ex)
            {
                return (new List<CommonBindList>());
            }
        }

        //----- Bind dropdownlist(Liens - WorkCardFilter - JobStatusGroup)
        public static IEnumerable<SelectListItem> GetLiensStatusGroup()
        {
            try
            {
                string sql = "Select * from JobStatusGroup";
                var LiensstatusGroupView = DBO.Provider.GetTableView(sql);
                DataTable dt = LiensstatusGroupView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["StatusGroupDescription"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetStatusGroupListId()
        {
            try
            {
                string sql = "select *,StatusGroupDescription as FriendlyName from jobStatusGroup where StatusGrouptype In (1, 2, 3)";
                var LiensstatusGroupView = DBO.Provider.GetTableView(sql);
                DataTable dt = LiensstatusGroupView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["StatusGroupDescription"])

                                             }).ToList();
                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                return (new List<SelectListItem>());
            }
        }
        public static IEnumerable<SelectListItem> GetJobStatusIdList()
        {
            try
            {
                string sql = " select *,JobStatus as FriendlyName from JobStatus order by JobStatus";
                var ClientIdList = DBO.Provider.GetTableView(sql);
                DataTable dt = ClientIdList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["FriendlyName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        //----- Bind dropdownlist(Liens - WorkCardFilter - PropertyTypeList)
        public static IEnumerable<SelectListItem> GetPropertyType()
        {
            try
            {
                List<CommonBindList> List = new List<CommonBindList>();
                List.Add(new CommonBindList() { Id = 1, Name = "Private" });
                List.Add(new CommonBindList() { Id = 2, Name = "Public" });
                List.Add(new CommonBindList() { Id = 3, Name = "Federal" });
                List.Add(new CommonBindList() { Id = 4, Name = "Residential" });

                return new SelectList(List, "Id", "Name");

            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }

        }


        //public static List<CommonBindList> GetLiensStatusGroupList()
        //{
        //    try
        //    {
        //        TableView tblStatusGroupList = CRF.BLL.Queries.CollectionStatusGroup();
        //        DataTable dt = tblStatusGroupList.ToTable();
        //        List<CommonBindList> StatusGroupList = (from DataRow dr in dt.Rows
        //                                                select new CommonBindList()
        //                                                {
        //                                                    Id = Convert.ToInt32(dr["Id"]),
        //                                                    Name = Convert.ToString(dr["StatusGroupDescription"])

        //                                                }).ToList();
        //        return StatusGroupList;
        //    }
        //    catch (Exception ex)
        //    {
        //        return (new List<CommonBindList>());
        //    }
        //}

        public static IEnumerable<SelectListItem> CountyRecorderNumList()
        {
            try
            {
                HDS.DAL.Providers.ITable dt_ITable;
                CountyRecorder dt_CT = new CountyRecorder();
                dt_ITable = dt_CT;
                TableView tblList = (TableView)CRF.BLL.Queries.GetViewForConfigTable(ref dt_ITable);
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["CountyRecorderID"]),
                                                 Name = Convert.ToString(dr["RecorderName"]),
                                                 Code = Convert.ToString(dr["CountyRecorderNum"])
                                             }).ToList();

                return new SelectList(List, "Code", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }


        //public static List<CommonBindList> GetBranchlist()
        //{
        //    try
        //    {
        //        Client objClient = new Client();
        //        TableView EntityList;
        //        Query query = new Query(objClient);
        //        string sql = query.BuildQuery();
        //        EntityList = DBO.Provider.GetTableView(sql);
        //        DataTable dt = EntityList.ToTable();
        //        List<CommonBindList> ClientBranchList = (from DataRow dr in dt.Rows
        //                                                      select new CommonBindList()
        //                                                      {
        //                                                          Id = Convert.ToInt16(dr["clientid"]),
        //                                                          Name = Convert.ToString(dr["ClientName"])

        //                                                      }).ToList();

        //        return ClientBranchList;
        //    }
        //    catch (Exception ex)
        //    {
        //        return (new List<CommonBindList>());
        //    }
        //}



        public static IEnumerable<SelectListItem> GetBranchlist()
        {
            try
            {

                string sql = "SELECT c.clientid,c.BranchNumber + ' - ' + ClientName as ClientName FROM CLIENT C(NOLOCK) JOIN JOB J(NOLOCK) ON J.CLIENTID = C.PARENTCLIENTID WHERE J.ID = '" + 1041169 + "' AND C.ISBRANCH = 1";
                var BranchClientView = DBO.Provider.GetTableView(sql);
                DataTable dt = BranchClientView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["clientid"]),
                                                 Name = Convert.ToString(dr["ClientName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }

        }


        public static List<CommonBindList> GetReportIdvwLienReportsListByCategoryType(string ReportCategoryValue)
        {
            try
            {
                string sql = null;
                sql = "Select ReportId,ReportName, Description,  PrintFrom, ReportCategory, ReportFilename, Reporttype from vwLienReportsList Where ReportCategory = '" + ReportCategoryValue + "'";
                sql += "And PrintFrom in ('B', 'R') Order By Description";

                var LienView = DBO.Provider.GetTableView(sql);
                DataTable dt = LienView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["ReportId"]),
                                                 Name = Convert.ToString(dr["Description"])
                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }


        public static IEnumerable<SelectListItem> GetJobDesks()
        {
            try
            {
                HDS.DAL.Providers.ITable dt_ITable;
                JobDesks dt_CT = new JobDesks();
                dt_ITable = dt_CT;
                TableView tblList = (TableView)CRF.BLL.Queries.JobDesks();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["DeskId"]),
                                                 Name = Convert.ToString(dr["DeskNum"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetJobDesksGroup()
        {
            try
            {
                HDS.DAL.Providers.ITable dt_ITable;
                JobDeskGroup dt_CT = new JobDeskGroup();
                dt_ITable = dt_CT;
                TableView tblList = (TableView)CRF.BLL.Queries.JobDeskGroup();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Name = Convert.ToString(dr["DeskGroupName"]),
                                                 Id = Convert.ToInt32(dr["Id"])
                                                
                                             }).ToList();

                return new SelectList(List, "Name", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        //public static IEnumerable<SelectListItem> GetChangeClientCodeList()
        //{
        //    try
        //    {
        //        HDS.DAL.Providers.ITable dt_ITable;
        //        Client dt_CT = new Client();
        //        dt_ITable = dt_CT;
        //        TableView tblList = (TableView)CRF.BLL.Queries.GetLienClients();
        //        DataTable dt = tblList.ToTable();
        //        List<CommonBindList> List = (from DataRow dr in dt.Rows
        //                                     select new CommonBindList()
        //                                     {
        //                                         Id = Convert.ToInt32(dr["ClientId"]),
        //                                         Name = Convert.ToString(dr["ClientName"])/* + "-" + Convert.ToString(dr["ClientName"])*/
        //                                     }).ToList();

        //        return new SelectList(List, "Id", "Name");
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLog.SendErrorToText(ex);
        //        return (new List<SelectListItem>());
        //    }
        //}
        public static IEnumerable<SelectListItem> GetChangeClientCodeList()

        {
            try
            {
                HDS.DAL.Providers.ITable dt_ITable;
                Client dt_CT = new Client();
                dt_ITable = dt_CT;
                TableView tblList = (TableView)CRF.BLL.Queries.GetLienClients();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["ClientId"]),
                                                 Name = Convert.ToString(dr["ClientName"]),/* + "-" + Convert.ToString(dr["ClientName"])*/
                                                 Code = Convert.ToString(dr["ClientCode"])/* + "-" + Convert.ToString(dr["ClientName"])*/
                                             }).ToList();

                return new SelectList(List, "Code", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetChangeClientCodeListNew()

        {
            try
            {
                HDS.DAL.Providers.ITable dt_ITable;
                Client dt_CT = new Client();
                dt_ITable = dt_CT;
                TableView tblList = (TableView)CRF.BLL.Queries.GetLienClients();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["ClientId"]),
                                                 Name = Convert.ToString(dr["ClientName"]),/* + "-" + Convert.ToString(dr["ClientName"])*/
                                                 Code = Convert.ToString(dr["ClientCode"])/* + "-" + Convert.ToString(dr["ClientName"])*/
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }
        public static IEnumerable<SelectListItem> GetInvoiceCycleList()
        {
            try
            {
                string sql = "Select * from FinDocCycle";
                var LiensstatusGroupView = DBO.Provider.GetTableView(sql);
                DataTable dt = LiensstatusGroupView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Code = Convert.ToString(dr["FinDocCycle"]),
                                                 Name = Convert.ToString(dr["CycleDescription"])
                                             }).ToList();

                return new SelectList(List, "Code", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }


        public static IEnumerable<SelectListItem> GetSubLedgerType()
        {
            try
            {
                string sql = "Select * from SubLedgerType";
                var LiensstatusGroupView = DBO.Provider.GetTableView(sql);
                DataTable dt = LiensstatusGroupView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["SubLedgerTypeId"]),
                                                 Name = Convert.ToString(dr["Description"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }


        public static IEnumerable<SelectListItem> GetLocationDetails()
        {
            try
            {
                string sql = "Select LocationId, Location from Location where IsInactive = 0";
                var LocationView = DBO.Provider.GetTableView(sql);
                DataTable dt = LocationView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["LocationId"]),
                                                 Name = Convert.ToString(dr["Location"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }

        }


        public static IEnumerable<SelectListItem> GetJobActionHistoryCodes()
        {
            try
            {
                string sql = "Select Id, ActionCode, Description from CollectionActions";
                var CollectionActionsView = DBO.Provider.GetTableView(sql);
                DataTable dt = CollectionActionsView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["ActionCode"]) + "-" + Convert.ToString(dr["Description"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }


        public static List<CommonBindList> GetJobActionHistoryCodesByActionType(string ActionTypeValue)
        {
            try
            {
                string sql = null;

                ////if (bIsJobView == true)
                ////{
                ////    sql = "Select Id, Description, IsVerifier, IsLienVerifier, CategoryType, IsJobView, IsCancelShortcutList from vwJobActionsList";
                ////}
                ////else
                ////{
                ////    sql = "Select Id, Description, IsVerifier, IsLienVerifier, CategoryType, IsJobView, IsCancelShortcutList from vwJobActionsList where IsJobView = 0";
                ////}

                sql = "Select Id, Description, IsVerifier, IsLienVerifier, CategoryType, IsJobView, IsCancelShortcutList from vwJobActionsList ";


                switch (ActionTypeValue)
                {
                    case "Phone":
                        sql += "where IsVerifier = 1 and CategoryType = 'P' ORDER BY Description ASC ";
                        break;
                    case "Received":
                        sql += "where IsVerifier = 1 and CategoryType = 'R' ORDER BY Description ASC";
                        break;
                    case "Cancel":
                        sql += "where IsVerifier = 1 and CategoryType = 'C' ORDER BY Description ASC";
                        break;
                    case "Status":
                        sql += "where IsVerifier = 1 and CategoryType = 'S' ORDER BY Description ASC";
                        break;
                    case "Review":
                        sql += "where IsVerifier = 1 and CategoryType = 'V' ORDER BY Description ASC";
                        break;
                    case "CancelShortcut":
                        sql += "where IsVerifier = 1 and IsCancelShortcutList = 1 ORDER BY Description ASC";
                        break;

                    default:
                        sql += "where IsVerifier = 1 and CategoryType = 'P' ORDER BY Description ASC ";
                        break;
                }
                //sql += "ORDER BY CASE WHEN CollectionActions.ActionCode LIKE '[0-9]%' THEN 2 ELSE 1 END, CollectionActions.ActionCode";
                var CollectionActionsView = DBO.Provider.GetTableView(sql);
                DataTable dt = CollectionActionsView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["Description"])
                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<CommonBindList>());
            }
        }


        public static IEnumerable<SelectListItem> GetJobDocumentType()
        {
            try
            {
                List<CommonBindList> List = new List<CommonBindList>();

                List.Add(new CommonBindList() { Name = "AZ Owner Ack", Code = "AZ Owner Ack" });
                List.Add(new CommonBindList() { Name = "Bankruptcy", Code = "Bankruptcy" });
                List.Add(new CommonBindList() { Name = "Building Permit", Code = "Building Permit" });
                List.Add(new CommonBindList() { Name = "Job Info Fax", Code = "Job Info Fax" });
                List.Add(new CommonBindList() { Name = "Job Info Sheet", Code = "Job Info Sheet" });
                List.Add(new CommonBindList() { Name = "Miscellaneous", Code = "Miscellaneous" });
                List.Add(new CommonBindList() { Name = "NOC", Code = "NOC" });
                List.Add(new CommonBindList() { Name = "Surety Letter", Code = "Surety Letter" });

                return new SelectList(List, "Name", "Code");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetStateFormList()
        {
            try
            {
                string sql = "select StateFormNoticeId, FriendlyName from StateFormNotices";
                var StateFormNoticesView = DBO.Provider.GetTableView(sql);
                DataTable dt = StateFormNoticesView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["StateFormNoticeId"]),
                                                 Name = Convert.ToString(dr["FriendlyName"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }

        }

        public static IEnumerable<SelectListItem> GetJobDskList()
        {
            try
            {
                TableView tblList = CRF.BLL.Queries.JobDeskList();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["DeskId"]),
                                                 Name = Convert.ToString(dr["FriendlyName"]),
                                                 Code = Convert.ToString(dr["DeskNum"])
                                             }).ToList();

                //return List;
                return new SelectList(List, "Id", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }
        public static IEnumerable<SelectListItem> GetStateList()
        {
            try
            {
                TableView tblList = (TableView)CRF.BLL.Queries.StateInfo();
                DataTable dt = tblList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 StateInitials = Convert.ToString(dr["StateInitials"]),
                                                 Name = Convert.ToString(dr["StateName"])
                                                        }).ToList();

                return new SelectList(List, "StateInitials", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetNewDeskList()
        {
            try
            {
                string sql = "select *,DESKNUM + '-' + DeskName as FriendlyName from jobdesks";
                var ClientIdList = DBO.Provider.GetTableView(sql);
                DataTable dt = ClientIdList.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 DESKNUM = Convert.ToString(dr["DESKNUM"]),
                                                 Name = Convert.ToString(dr["FriendlyName"])
                                             }).ToList();

                return new SelectList(List, "DESKNUM", "Name");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

        public static IEnumerable<SelectListItem> GetLegalParties()
        {
            try
            {
                List<CommonBindList> List = new List<CommonBindList>();
                List.Add(new CommonBindList() { Id = 0, Name = "Choose... " });
                List.Add(new CommonBindList() { Id = 1, Name = "New Designee" });
                List.Add(new CommonBindList() { Id = 2, Name = "New GC" });
                List.Add(new CommonBindList() { Id = 3, Name = "New Lender/Surety" });
                List.Add(new CommonBindList() { Id = 4, Name = "New Owner" });
                
              
                return new SelectList(List, "Id", "Name");

            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }

        }

       
        public static IEnumerable<SelectListItem> GetResearchType(string alegalpartytype)
        {
            try
            {
                string sql = null;

                switch (alegalpartytype)
                {
               
                case "Customer":
                    sql = "Select * from MailReturns Where LegalPartytype = 'CU'";
                    break;
                case "General Contractor":  case"Other General Contractor":
                    sql = "Select * from MailReturns Where LegalPartytype = 'GC'";
                    break;
                case "Owner": case "Other Owner": case"ML AGENT":
                    sql = "Select * from MailReturns Where LegalPartytype = 'OW'";
                    break;
                case "Lender/Surety": case "Other Lender/Surety":
                    sql = "Select * from MailReturns Where LegalPartytype = 'LE'";
                    break;
                case "Designee":  case "Other Designee":
                    sql = "Select * from MailReturns Where LegalPartytype = 'DE'";
                    break;
                   
                }
                var ContactPlanView = DBO.Provider.GetTableView(sql);
                DataTable dt = ContactPlanView.ToTable();
                List<CommonBindList> List = (from DataRow dr in dt.Rows
                                             select new CommonBindList()
                                             {
                                                 Id = Convert.ToInt32(dr["Id"]),
                                                 Name = Convert.ToString(dr["Description"])
                                             }).ToList();

                return new SelectList(List, "Id", "Name");

            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }
        }

    }

    #endregion
}

