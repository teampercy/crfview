﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.PropertySearch.AddressVerifiers.ViewModels
{
    public class AddressVerifiersVM
    {

        public AddressVerifiersVM()
        {
            Statelist = CommonBindList.GetStateList();
        }

        public string JobAddrSearch { get;set;}
        public string JobAddr1 { get; set; }
        public string JobAddr2 { get; set; }
        public string JobCity { get; set; }
        public string StateId { get; set; }
        public IEnumerable<SelectListItem> Statelist { get; set; }
        public string JobZip { get; set; }

        public ADAddressVerifiers objADAddressVerifiers { get; set; }
    }
}
