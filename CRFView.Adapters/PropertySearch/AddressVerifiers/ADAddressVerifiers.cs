﻿using CRF.BLL.Providers;
using CRFView.Adapters.PropertySearch.AddressVerifiers.ViewModels;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.IO;
using System.Web;

namespace CRFView.Adapters.PropertySearch.AddressVerifiers
{
    public class ADAddressVerifiers
    {
        #region Properties

        public string apn { get; set; }
        public string zoningType { get; set; }
        public string siteZoningIdent { get; set; }
        public string countrySubd { get; set; }
        public string countrysecsubd { get; set; }
        public string country { get; set; }
        public string line1 { get; set; }
        public string line2 { get; set; }
        public string locality { get; set; }
        public string postal1 { get; set; }
        public string latitude { get; set; }
        public string longitude { get; set; }
        public string propclass { get; set; }
        public string owner { get; set; }
        public string type { get; set; }
        public string description { get; set; }
        public string firstNameAndMi { get; set; }
        public string lastName { get; set; }
        public string owner2 { get; set; }
        public string mailingAddressOneLine { get; set; }
        public string amount { get; set; }
        public string interestRate { get; set; }
        public string deedType { get; set; }
        public string CompanyName { get; set; }
        public string owner2firstNameAndMi { get; set; }
        public string owner2lastName { get; set; }

        #endregion

        //public static string AddressVerifiers(string add1, string add2)
        public static ADAddressVerifiers AddressVerifiers(AddressVerifiersVM objAddressVerifiersVM)
        {

            try
            {
                string result = "";
                string add1 = "";
                string add2 = "";
                string address1 = "";
                string address2 = "";
                string city = "";
                string state = "";
                string zip = "";
                string SearchBySingleField = "";
                string[] SearchBySingleFieldValues = new string[4];
                SearchBySingleField = objAddressVerifiersVM.JobAddrSearch;


                if (objAddressVerifiersVM.JobAddrSearch != null)
                {
                    SearchBySingleFieldValues = SearchBySingleField.Split(',');

                    if (SearchBySingleFieldValues.Length == 1)
                    {
                        address1 = SearchBySingleFieldValues[0].ToString().Trim();
                    }
                    else if (SearchBySingleFieldValues.Length == 2)
                    {
                        address1 = SearchBySingleFieldValues[0].ToString().Trim();
                        city = SearchBySingleFieldValues[1].ToString().Trim();
                    }

                    else if (SearchBySingleFieldValues.Length == 3)
                    {
                        address1 = SearchBySingleFieldValues[0].ToString().Trim();
                        city = SearchBySingleFieldValues[1].ToString().Trim();
                        state = SearchBySingleFieldValues[2].ToString().Trim();
                    }
                    else if (SearchBySingleFieldValues.Length == 4)
                    {
                        address1 = SearchBySingleFieldValues[0].ToString().Trim();
                        city = SearchBySingleFieldValues[1].ToString().Trim();
                        state = SearchBySingleFieldValues[2].ToString().Trim();
                        zip = SearchBySingleFieldValues[3].ToString().Trim();
                    }
                    else if (SearchBySingleFieldValues.Length == 5)
                    {
                        address1 = SearchBySingleFieldValues[0].ToString().Trim();
                        address2 = SearchBySingleFieldValues[1].ToString().Trim();
                        city = SearchBySingleFieldValues[2].ToString().Trim();
                        state = SearchBySingleFieldValues[3].ToString().Trim();
                        zip = SearchBySingleFieldValues[4].ToString().Trim();
                    }
                }

                else
                {
                    address1 = objAddressVerifiersVM.JobAddr1;
                    address2 = objAddressVerifiersVM.JobAddr2;
                    city = objAddressVerifiersVM.JobCity;
                    state = objAddressVerifiersVM.StateId;
                    zip = objAddressVerifiersVM.JobZip;

                }
                add1 = address1 + "" + address2;
                //add2 = city + " " + state + " " + zip;
                add2 = city + "%2C" + " " + state + "%2C" + " " + zip;
                result = CRF.BLL.ATTOMAPI.VerifyProperty(add1, add2);
               //result = "<Response><status><version>1.0.0</version><code>0</code><msg>SuccessWithResult</msg><total>1</total><page>1</page><pagesize>10</pagesize><responseDateTime>2020-12-08T12:07:07</responseDateTime><transactionID>44d2dad0-1383-47a7-b8f4-e7e3bf856be0</transactionID></status><echoed_fields><jobID></jobID><loanNumber></loanNumber><preparedBy></preparedBy><resellerID></resellerID><preparedFor></preparedFor></echoed_fields><property><identifier><obPropId>18471319108031</obPropId><fips>08031</fips><apn>0219204018000</apn><attomId>184713191</attomId></identifier><lot><depth>0</depth><frontage>0</frontage><lotNum>31</lotNum><lotSize1>0.1077</lotSize1><lotSize2>4690</lotSize2><zoningType>Residential</zoningType></lot><area><blockNum>36</blockNum><countrySecSubd>Denver County</countrySecSubd><countyUse1>113</countyUse1><munCode>DE</munCode><munName>DENVER</munName><srvyRange>68W</srvyRange><srvySection>19</srvySection><srvyTownship>03S</srvyTownship><subdName>BERKELEY</subdName><subdTractNum>0</subdTractNum><taxCodeArea>0</taxCodeArea><censusTractIdent>102</censusTractIdent><censusBlockGroup>3</censusBlockGroup></area><address><country>US</country><countrySubd>CO</countrySubd><line1>4529 WINONA CT</line1><line2>DENVER, CO 80212</line2><locality>Denver</locality><matchCode>ExaStr</matchCode><oneLine>4529 WINONA CT, DENVER, CO 80212</oneLine><postal1>80212</postal1><postal2>2512</postal2><postal3>C037</postal3><stateFips>08</stateFips></address><location><accuracy>Street</accuracy><elevation>0</elevation><latitude>39.778904</latitude><longitude>-105.047624</longitude><distance>0</distance><geoid>CO08031, CS0891007, DB0803360, ND0000119198, ND0004861239, PL0820000, SB0000076114, SB0000076155, SB0000076161, SB0000135819, SB0000143772, ZI80212</geoid></location><summary><absenteeInd>OWNER OCCUPIED</absenteeInd><propClass>Single Family Residence / Townhouse</propClass><propSubType>RESIDENTIAL</propSubType><propType>SFR</propType><yearBuilt>1900</yearBuilt><propLandUse>SFR</propLandUse><propIndicator>10</propIndicator><legal1>BERKELEY B36 L31 &amp; S/2 OF L32 EXC REAR 8FT TO CITY</legal1><quitClaimFlag>False</quitClaimFlag><REOflag></REOflag></summary><utilities><heatingType>FORCED AIR</heatingType></utilities><sale><sellerName>STCHERBAK,LIDIA</sellerName><saleSearchDate>2015-8-21</saleSearchDate><saleTransDate>2015-8-21</saleTransDate><armsLengthIdent>1</armsLengthIdent><recorderThroughDate>2018-7-5</recorderThroughDate><amount><saleAmt>345000</saleAmt><saleRecDate>2015-8-25</saleRecDate><saleDisclosureType>0</saleDisclosureType><saleDocNum>0000119310</saleDocNum><saleTransType>Resale</saleTransType></amount><calculation><pricePerBed>172500</pricePerBed><pricePerSizeUnit>369</pricePerSizeUnit></calculation></sale><building><size><bldgSize>1414</bldgSize><grossSize>1414</grossSize><grossSizeAdjusted>934</grossSizeAdjusted><groundFloorSize>934</groundFloorSize><livingSize>934</livingSize><sizeInd>LIVING SQFT </sizeInd><universalSize>934</universalSize><atticSize>0</atticSize></size><rooms><bathFixtures>0</bathFixtures><baths1qtr>0</baths1qtr><baths3qtr>0</baths3qtr><bathsCalc>1</bathsCalc><bathsFull>1</bathsFull><bathsHalf>0</bathsHalf><bathsTotal>1</bathsTotal><beds>2</beds><roomsTotal>5</roomsTotal></rooms><interior><bsmtSize>480</bsmtSize><bsmtType>UNFINISHED</bsmtType><bsmtFinishedPercent>0</bsmtFinishedPercent><fplcCount>1</fplcCount><fplcInd>Y</fplcInd><fplcType>YES</fplcType></interior><construction><condition>AVERAGE</condition><wallType>BRICK</wallType><propertyStructureMajorImprovementsYear>0</propertyStructureMajorImprovementsYear></construction><parking><garageType>DETACHED GARAGE</garageType><prkgSize>240</prkgSize><prkgSpaces>0</prkgSpaces><prkgType>GARAGE DETACHED</prkgType></parking><summary><levels>1</levels><storyDesc>TYPE UNKNOWN</storyDesc><unitsCount>1</unitsCount><viewCode></viewCode></summary></building><assessment><appraised><apprImprValue>0</apprImprValue><apprLandValue>0</apprLandValue><apprTtlValue>0</apprTtlValue></appraised><assessed><assdImprValue>11276</assdImprValue><assdLandValue>19634</assdLandValue><assdTtlValue>30910</assdTtlValue></assessed><market><mktImprValue>157700</mktImprValue><mktLandValue>274600</mktLandValue><mktTtlValue>432300</mktTtlValue></market><tax><taxAmt>2229</taxAmt><taxPerSizeUnit>2.39</taxPerSizeUnit><taxYear>2019</taxYear><exemption><ExemptionAmount1>0</ExemptionAmount1><ExemptionAmount2>0</ExemptionAmount2><ExemptionAmount3>0</ExemptionAmount3><ExemptionAmount4>0</ExemptionAmount4><ExemptionAmount5>0</ExemptionAmount5></exemption></tax><delinquentyear>0</delinquentyear><improvementPercent>36</improvementPercent><fullCashValue>0</fullCashValue><owner><type>INDIVIDUAL</type><description>Individual</description><owner1><lastName>MATTICE</lastName><firstNameAndMi>MICHAEL S</firstNameAndMi></owner1><owner2><lastName>MATTICE</lastName><firstNameAndMi>SHAREE</firstNameAndMi></owner2><absenteeOwnerStatus>O</absenteeOwnerStatus><mailingAddressOneLine>4529 WINONA CT, DENVER,, CO 80212-2512</mailingAddressOneLine></owner><mortgage><FirstConcurrent><amount>0</amount><interestRate>0</interestRate><deedType>WD</deedType></FirstConcurrent><SecondConcurrent><amount>0</amount></SecondConcurrent><ThirdConcurrent><amount>0</amount></ThirdConcurrent><title><companyName>FIRST AMERICAN TITLE</companyName><companyCode>18618</companyCode></title></mortgage></assessment><vintage><lastModified>2020-1-10</lastModified><pubDate>2020-1-16</pubDate></vintage></property></Response>";

                //result = null;
                CRF.BLL.ATTOMAPI.Apidata Req = new CRF.BLL.ATTOMAPI.Apidata();

                XmlDocument xmlDoc = new XmlDocument();

                xmlDoc.LoadXml(result);
                XmlNodeList nodes = xmlDoc.DocumentElement.SelectNodes("/Response/property/identifier");
                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("apn") == null)
                    {
                        Req.apn = "";
                    }

                    else
                    {
                        Req.apn = node.SelectSingleNode("apn").InnerText;
                    }

                }
                nodes = xmlDoc.DocumentElement.SelectNodes("/Response/property/lot");
                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("zoningType") == null)
                    {
                        Req.zoningType = "";
                    }

                    else
                    {
                        Req.zoningType = node.SelectSingleNode("zoningType").InnerText;
                    }

                    if (node.SelectSingleNode("siteZoningIdent") == null)
                    {
                        Req.siteZoningIdent = "";
                    }

                    else
                    {
                        Req.siteZoningIdent = node.SelectSingleNode("siteZoningIdent").InnerText;
                    }

                }
                nodes = xmlDoc.DocumentElement.SelectNodes("/Response/property/area");
                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("countrySecSubd") == null)
                    {
                        Req.countrysecsubd = "";
                    }

                    else
                    {
                        Req.countrysecsubd = node.SelectSingleNode("countrySecSubd").InnerText;
                    }

                }
                nodes = xmlDoc.DocumentElement.SelectNodes("/Response/property/location");
                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("latitude") == null)
                    {
                        Req.latitude = "";
                    }

                    else
                    {
                        Req.latitude = node.SelectSingleNode("latitude").InnerText;
                    }

                    if (node.SelectSingleNode("longitude") == null)
                    {
                        Req.longitude = "";
                    }

                    else
                    {
                        Req.longitude = node.SelectSingleNode("longitude").InnerText;
                    }




                }

                nodes = xmlDoc.DocumentElement.SelectNodes("/Response/property/address");
                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("country") == null)
                    {
                        Req.country = "";
                    }

                    else
                    {
                        Req.country = node.SelectSingleNode("country").InnerText;
                    }

                    if (node.SelectSingleNode("countrySubd") == null)
                    {
                        Req.countrySubd = "";
                    }

                    else
                    {
                        Req.countrySubd = node.SelectSingleNode("countrySubd").InnerText;
                    }

                    if (node.SelectSingleNode("line1") == null)
                    {
                        Req.line1 = "";
                    }

                    else
                    {
                        Req.line1 = node.SelectSingleNode("line1").InnerText;
                    }

                    if (node.SelectSingleNode("line2") == null)
                    {
                        Req.line2 = "";
                    }

                    else
                    {
                        Req.line2 = node.SelectSingleNode("line2").InnerText;
                    }

                    if (node.SelectSingleNode("locality") == null)
                    {
                        Req.locality = "";
                    }

                    else
                    {
                        Req.locality = node.SelectSingleNode("locality").InnerText;
                    }

                    if (node.SelectSingleNode("postal1") == null)
                    {
                        Req.postal1 = "";
                    }

                    else
                    {
                        Req.postal1 = node.SelectSingleNode("postal1").InnerText;
                    }


                }

                nodes = xmlDoc.DocumentElement.SelectNodes("/Response/property/summary");
                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("propClass") == null)
                    {
                        Req.propclass = "";
                    }

                    else
                    {
                        Req.propclass = node.SelectSingleNode("propClass").InnerText;
                    }

                }

                nodes = xmlDoc.DocumentElement.SelectNodes("/Response/property/assessment/owner");
                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("type") == null)
                    {
                        Req.type = "";
                    }

                    else
                    {
                        Req.type = node.SelectSingleNode("type").InnerText;
                    }

                    if (node.SelectSingleNode("description") == null)
                    {
                        Req.description = "";
                    }

                    else
                    {
                        Req.description = node.SelectSingleNode("description").InnerText;
                    }

                }


                nodes = xmlDoc.DocumentElement.SelectNodes("/Response/property/assessment/owner/owner1");
                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("firstNameAndMi") == null)
                    {
                        Req.firstNameAndMi = "";
                    }

                    else
                    {
                        Req.firstNameAndMi = node.SelectSingleNode("firstNameAndMi").InnerText;
                    }

                    if (node.SelectSingleNode("lastName") == null)
                    {
                        Req.lastName = "";
                    }

                    else
                    {
                        Req.lastName = node.SelectSingleNode("lastName").InnerText;
                    }
                }

                nodes = xmlDoc.DocumentElement.SelectNodes("/Response/property/assessment/owner/owner2");
                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("firstNameAndMi") == null)
                    {
                        Req.owner2firstNameAndMi = "";
                    }

                    else
                    {
                        Req.owner2firstNameAndMi = node.SelectSingleNode("firstNameAndMi").InnerText;
                    }

                    if (node.SelectSingleNode("lastName") == null)
                    {
                        Req.owner2lastName = "";
                    }

                    else
                    {
                        Req.owner2lastName = node.SelectSingleNode("lastName").InnerText;
                    }
                }

                nodes = xmlDoc.DocumentElement.SelectNodes("/Response/property/assessment/owner");
                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("mailingAddressOneLine") == null)
                    {
                        Req.mailingAddressOneLine = "";
                    }

                    else
                    {
                        Req.mailingAddressOneLine = node.SelectSingleNode("mailingAddressOneLine").InnerText;
                    }
                }

                nodes = xmlDoc.DocumentElement.SelectNodes("/Response/property/assessment/mortgage/FirstConcurrent");
                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("deedType") == null)
                    {
                        Req.deedType = "";
                    }

                    else
                    {
                        Req.deedType = node.SelectSingleNode("deedType").InnerText;
                    }

                }

                nodes = xmlDoc.DocumentElement.SelectNodes("/Response/property/assessment/mortgage/title");
                foreach (XmlNode node in nodes)
                {
                    if (node.SelectSingleNode("companyName") == null)
                    {
                        Req.CompanyName = "";
                    }

                    else
                    {
                        Req.CompanyName = node.SelectSingleNode("companyName").InnerText;
                    }

                }


                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                ADAddressVerifiers objADAddressVerifiers = new ADAddressVerifiers();
                objADAddressVerifiers.apn = Req.apn;
                objADAddressVerifiers.zoningType = Req.zoningType;
                objADAddressVerifiers.siteZoningIdent = Req.siteZoningIdent;
                objADAddressVerifiers.countrySubd = Req.countrySubd;
                objADAddressVerifiers.countrysecsubd = Req.countrysecsubd;
                objADAddressVerifiers.country = Req.country;
                objADAddressVerifiers.line1 = Req.line1;
                objADAddressVerifiers.line2 = Req.line2;
                objADAddressVerifiers.locality = Req.locality;
                objADAddressVerifiers.postal1 = Req.postal1;
                objADAddressVerifiers.latitude = Req.latitude;
                objADAddressVerifiers.longitude = Req.longitude;
                objADAddressVerifiers.propclass = Req.propclass;
                objADAddressVerifiers.owner = Req.owner;
                objADAddressVerifiers.type = Req.type;
                objADAddressVerifiers.description = Req.description;
                objADAddressVerifiers.firstNameAndMi = Req.firstNameAndMi;
                objADAddressVerifiers.lastName = Req.lastName;
                objADAddressVerifiers.owner2 = Req.owner2;
                objADAddressVerifiers.mailingAddressOneLine = Req.mailingAddressOneLine;
                objADAddressVerifiers.amount = Req.amount;
                objADAddressVerifiers.interestRate = Req.interestRate;
                objADAddressVerifiers.deedType = Req.deedType;
                objADAddressVerifiers.CompanyName = Req.CompanyName;
                objADAddressVerifiers.owner2firstNameAndMi = Req.owner2firstNameAndMi;
                objADAddressVerifiers.owner2lastName = Req.owner2lastName;

                CRF.BLL.CRFDB.TABLES.BatchJobTitleSearch myJobTitleSearch = new CRF.BLL.CRFDB.TABLES.BatchJobTitleSearch();
                myJobTitleSearch.DateCreated = DateTime.Now;
                if (!string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["LoginCode"])))
                {
                    myJobTitleSearch.RequestedBy = (string)HttpContext.Current.Session["LoginCode"];

                }
              
                else
                {
                //    //myJobTitleSearch.RequestedBy = (string)HttpContext.Current.Session["LoginCode"];
                    myJobTitleSearch.RequestedBy = "";
               }
                myJobTitleSearch.RequestedBy = (string)HttpContext.Current.Session["LoginCode"];
                myJobTitleSearch.RequestedByUserid = user.Id;
                myJobTitleSearch.IsBillable = true;
                myJobTitleSearch.APN = Req.apn;
                myJobTitleSearch.ZoningType = Req.zoningType;
                myJobTitleSearch.SiteZoningIdent = Req.siteZoningIdent;
                myJobTitleSearch.Area = Req.countrysecsubd;
                myJobTitleSearch.Country = Req.country;
                myJobTitleSearch.State = Req.countrySubd;
                myJobTitleSearch.AddressLine1 = Req.line1;
                myJobTitleSearch.AddressLine2 = Req.line2;
                myJobTitleSearch.Locality = Req.locality;
                myJobTitleSearch.PostalCode = Req.postal1;
                myJobTitleSearch.Latitude = Req.latitude;
                myJobTitleSearch.Longitude = Req.longitude;
                myJobTitleSearch.Owner = Req.owner;
                myJobTitleSearch.Type = Req.type;
                myJobTitleSearch.Description = Req.description;
                myJobTitleSearch.OwnerName1 = Req.firstNameAndMi + " " + Req.lastName;
                myJobTitleSearch.OwnerName2 = Req.owner2firstNameAndMi + " " + Req.owner2lastName;
                myJobTitleSearch.MailingAddress = Req.mailingAddressOneLine;
                myJobTitleSearch.Amount = Convert.ToDecimal(Req.amount);
                myJobTitleSearch.IntRate = Convert.ToDecimal(Req.interestRate);
                myJobTitleSearch.DeedType = Req.deedType;
                myJobTitleSearch.Lender = Req.CompanyName;

                if (!string.IsNullOrEmpty(Convert.ToString(HttpContext.Current.Session["clientId"])))
                {
                    
                    myJobTitleSearch.Clientid = (int)HttpContext.Current.Session["clientId"];
                }
                else
                {
                    myJobTitleSearch.Clientid = 0;

                }
                // myJobTitleSearch.Clientid = (int)HttpContext.Current.Session["clientId"];
                ITable myJobTitleSearchtbl = myJobTitleSearch;
                DBO.Provider.Create(ref myJobTitleSearchtbl);
                //CRF.BLL.LienView.Provider.DAL.Create(myJobTitleSearch)

                return objADAddressVerifiers;
            }

            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return null;
            }

        }
    }
}
