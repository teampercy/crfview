﻿using CRFView.Adapters;
using System.Web;

namespace CRFView
{
   public  class AuthenticationUtility
    {
       public  static CRF.BLL.Users.CurrentUser GetUser()
        {
            CRF.BLL.Users.CurrentUser loggedInUser=new CRF.BLL.Users.CurrentUser();
            if (HttpContext.Current.Session["LoggedInUser"] != null)
            {

                CurrentUser myuser = (CurrentUser)HttpContext.Current.Session["LoggedInUser"];
                loggedInUser.Id = myuser.Id;
                loggedInUser.UserName=myuser.UserName;
                loggedInUser.ClientCode=myuser.ClientCode;
                loggedInUser.Email=myuser.Email;
                loggedInUser.SiteDataFolder=myuser.SiteDataFolder;
                loggedInUser.SettingsFolder=myuser.SettingsFolder;
                loggedInUser.ErrorLogFolder=myuser.ErrorLogFolder;
                loggedInUser.OutputFolder=myuser.OutputFolder;
                loggedInUser.UploadFolder=myuser.UploadFolder;
                loggedInUser.LastUploadPath=myuser.LastUploadPath;
                loggedInUser.LastReportPlath=myuser.LastReportPlath;
                loggedInUser.SMTPHOST=myuser.SMTPHOST;
                loggedInUser.SMTPUSER=myuser.SMTPUSER;
                loggedInUser.SMTPPASSWORD=myuser.SMTPPASSWORD;
                loggedInUser.SMTPFROM=myuser.SMTPFROM;
                loggedInUser.FAXPRINTER=myuser.FAXPRINTER;
                loggedInUser.REPORTID=myuser.REPORTID;
                loggedInUser.LastLedgerType=myuser.LastLedgerType;

            }
            return loggedInUser;

        }
    }
}