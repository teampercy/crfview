﻿using CRF.BLL.Providers;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
   public class Utility
    {

        public static bool sendEmail(string to, string from, string subject, string body,bool isSend)
        {
            List<string> toList = new List<string>();
            toList.Add(to);
            bool successFlag = sendEmail(toList, from, subject, body, isSend);
            return successFlag;
        }
        public static bool sendEmail(string to, string from, string replyTo, string subject, string body, bool isSend)
        {
            List<string> toList = new List<string>();
            toList.Add(to);
            bool successFlag = sendEmail(toList, from, replyTo, subject, body, isSend);
            return successFlag;
        }
        public static bool sendEmail(List<string> to, string from, string subject, string body, bool isSend)
        {
            return sendEmail(to, from, null, subject, body, isSend);
        }
        public static bool sendEmail(List<string> to, string from, string replyTo, string subject, string body, bool isSend)
        {
            if (String.IsNullOrEmpty(replyTo))
            {
               // replyTo = ConfigurationManager.AppSettings["SupportEmailAddress"].ToString();
            }
            bool successFlag = false;
            try
            {
                MailMessage mail = new MailMessage();
                mail.BodyEncoding = Encoding.Default;
                mail.IsBodyHtml = true;
                if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["EmailToOverride"].ToString()))
                {
                    if (to.Count > 0)
                    {
                        foreach (string emailAddress in to)
                        {
                            mail.To.Add(emailAddress);
                        }
                    }
                }
                else
                {
                    mail.To.Add(ConfigurationManager.AppSettings["EmailToOverride"].ToString());
                }

                mail.Sender = new MailAddress(ConfigurationManager.AppSettings["SupportEmailAddress"].ToString());
               // mail.ReplyTo = new MailAddress(replyTo);
                mail.From = new MailAddress(ConfigurationManager.AppSettings["SupportEmailAddress"].ToString());
                mail.Subject = subject;
                mail.Body = body;

                ServicePointManager.SecurityProtocol = (SecurityProtocolType)48 | (SecurityProtocolType)192 | (SecurityProtocolType)768 | (SecurityProtocolType)3072 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
               //ADClientReportSettings aDClientReportSettings = new ADClientReportSettings();
               // var ReportSettings = aDClientReportSettings.ReadReportSettings(1);
               SmtpClient smtp = new SmtpClient();
               // smtp.Host = ReportSettings.smtpserver;
               // smtp.Port = Int32.Parse(ReportSettings.smtpport);
                NetworkCredential basicAuthenticationInfo = new NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"].ToString(), ConfigurationManager.AppSettings["SMTPPassword"].ToString());
               // basicAuthenticationInfo.UserName = ReportSettings.smtplogin;
               // basicAuthenticationInfo.Password = ReportSettings.smtppassword;
               // //smtp.EnableSsl = true;
               // smtp.EnableSsl = ReportSettings.smtpenablessl;

                smtp.EnableSsl = true;
                smtp.UseDefaultCredentials = false;
                //smtp.Credentials = smtp.UseDefaultCredentials;
                smtp.Credentials = basicAuthenticationInfo;
                smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"].ToString(), ConfigurationManager.AppSettings["SMTPPassword"].ToString());
                if (isSend)
                {
                    smtp.Send(mail);

                }
                successFlag = true;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                //LogException("Email Sender", ex, "");
                return successFlag;
            }

            return successFlag;
        }
        public static bool sendEmail(string to, string from, string subject, string body, List<string> attachmentList, bool isSend)
        {
            List<string> toList = new List<string>();
            toList.Add(to);
            bool successFlag = sendEmail(toList, from, subject, body, attachmentList,isSend);
            return successFlag;
        }
        public static bool sendEmail(List<string> to, string from, string replyTo, string subject, string body, List<string> attachmentList, bool isSend)
        {
            
            //body = Regex.Replace(body, "\n", "<br />");
            MailMessage mail = new MailMessage();
            System.Net.Mail.Attachment attachment = null;
            SmtpClient smtp = new SmtpClient();
            if (String.IsNullOrEmpty(replyTo))
            {
             //  replyTo = ConfigurationManager.AppSettings["SupportEmailAddress"].ToString();
            }
            bool successFlag = false;
            try
            {
                 //MailMessage mail = new MailMessage();
                mail.BodyEncoding = Encoding.Default;
                mail.IsBodyHtml = true;
                //if (String.IsNullOrEmpty(ConfigurationManager.AppSettings["EmailToOverride"].ToString()))
                //{
                //    if (to.Count > 0)
                //    {
                //        foreach (string emailAddress in to)
                //        {
                //            mail.To.Add(emailAddress);
                //        }
                //    }
                //}
                //else
                //{
                //    mail.To.Add(ConfigurationManager.AppSettings["EmailToOverride"].ToString());
                //}
                //mail.Sender = new MailAddress(ConfigurationManager.AppSettings["SupportEmailAddress"].ToString());

                //mail.To.Add(to[0].ToString());
                // mail.From = new MailAddress(from);
                  mail.To.Add(to[0].ToString());
                 mail.From = new MailAddress(from);
                mail.Subject = subject;
                mail.Body = body;
              

                foreach (string filePath in attachmentList)
                {
                    if (filePath != "" && !filePath.Contains("\n"))
                    {
                        string path =  filePath;
                        if ((new Uri(path)).IsFile == false)
                        {
                            //Is Remote file(Cloud Storate URL)
                            try
                            {
                                Stream fileStream = new WebClient().OpenRead(path);
                                string fileName = filePath.Substring(path.LastIndexOf("/") + 1, path.Length - 1 - path.LastIndexOf("/"));
                                attachment = new Attachment(fileStream, fileName);
                                mail.Attachments.Add(attachment);
                            }
                            catch (Exception e)
                            {
                            }
                        }
                        else
                        {
                            //Is Local File System File
                            if (System.IO.File.Exists(filePath))
                            {
                                attachment = new Attachment(filePath);
                                mail.Attachments.Add(attachment);
                            }
                        }
                    }
                    //  System.Net.Mail.Attachment attachment;
                  

                }

                ServicePointManager.SecurityProtocol = (SecurityProtocolType)48 | (SecurityProtocolType)192 | (SecurityProtocolType)768 | (SecurityProtocolType)3072 | SecurityProtocolType.Tls | SecurityProtocolType.Ssl3;
                ADClientReportSettings aDClientReportSettings = new ADClientReportSettings();
                aDClientReportSettings = aDClientReportSettings.ReadReportSettings(1);
                
                NetworkCredential basicAuthenticationInfo = new NetworkCredential();
                smtp.Host = aDClientReportSettings.smtpserver;
                smtp.Port = Convert.ToInt32(aDClientReportSettings.smtpport);
                basicAuthenticationInfo.UserName = aDClientReportSettings.smtplogin;
                basicAuthenticationInfo.Password = aDClientReportSettings.smtppassword;
                smtp.UseDefaultCredentials = false;
                smtp.EnableSsl = aDClientReportSettings.smtpenablessl;
                smtp.Credentials = basicAuthenticationInfo;


                //smtp.Host = ConfigurationManager.AppSettings["SMTPHost"].ToString();
                //smtp.Port = Convert.ToInt32(ConfigurationManager.AppSettings["SMTPPort"].ToString());
                //NetworkCredential basicAuthenticationInfo = new NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"].ToString(), ConfigurationManager.AppSettings["SMTPPassword"].ToString());

                //smtp.EnableSsl = true;
                //smtp.UseDefaultCredentials = false;
                //smtp.Credentials = new System.Net.NetworkCredential(ConfigurationManager.AppSettings["SMTPUserName"].ToString(), ConfigurationManager.AppSettings["SMTPPassword"].ToString());
                
                if (isSend)
                {
                    smtp.Send(mail);

                }
                // Utility.LogException("Email Sender EmailDoc Method ", new Exception("after Email Method call"), "");
                //  smtp.Dispose();
                //   mail.Dispose();
                successFlag = true;
            }
            catch (SmtpException ex)
            {
                ErrorLog.SendErrorToText(ex);
                //LogException("Email Sender", ex, "");
                throw new TimeoutException();
            }
            catch (Exception ex)
            {
                throw;
            }
            finally
            {
                smtp.Dispose();
                mail.Dispose();
                if(attachment != null)
                {
                    attachment.Dispose();
                }
               
            }
            return successFlag;
        }
        public static bool sendEmail(List<string> to, string from, string subject, string body, List<string> attachmentList, bool isSend)
        {
            return sendEmail(to, from, null, subject, body, attachmentList, isSend);
        }
      
    }
}
