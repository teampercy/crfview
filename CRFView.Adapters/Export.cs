﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ExportColumn
    {

        public string ColumnName = "";

        public string FriendlyName = "";
    }
    public class ExportColumns : System.Collections.CollectionBase
    {

        public ExportColumns()
        {
        }

        public void AddColumnString(string acolumnstring)
        {
            this.List.Clear();
            string scols = acolumnstring.Replace("\r\n", String.Empty);
            if ((acolumnstring.Length < 1))
            {
                return;
            }

            if ((scols.Length < 1))
            {
                return;
            }

            ExportColumn mycol = new ExportColumn();
            string[] ss1 = scols.Split(',');
            
            foreach (string s in ss1)
            {
                if (s.Length > 0)
                {
                    string[] ss2 = s.Split('~');
                    mycol = new ExportColumn();
                    mycol.ColumnName = ss2[0];
                    if ((ss2.Length > 1))
                    {
                        mycol.FriendlyName = ss2[1];
                    }

                    this.Add(mycol);
                }

            }

        }

        public string GetString()
        {
           // ExportColumn mycol = new ExportColumn();
            string scols = String.Empty;
            foreach (ExportColumn mycol in this.List)
            {
                if ((scols.Length > 1))
                {
                    scols = (scols + ("," + "\r\n"));
                }

                scols = (scols + mycol.ColumnName);
                if ((mycol.FriendlyName.Length > 1))
                {
                    scols = (scols + ("~" + mycol.FriendlyName));
                }

            }

            scols += "\r\n";
            return scols;
        }

        public virtual void Add(string acolumnname)
        {
            ExportColumn mycol = new ExportColumn();
            mycol.ColumnName = acolumnname;
            mycol.FriendlyName = acolumnname;
            base.List.Add(mycol);
        }

        public virtual void Add(string acolumnname, string afriendlyname)
        {
            ExportColumn mycol = new ExportColumn();
            mycol.ColumnName = acolumnname;
            mycol.FriendlyName = afriendlyname;
            base.List.Add(mycol);
        }

        public virtual void Add(ExportColumn value)
        {
            base.List.Add(value);
        }

        public virtual ExportColumn Item(int index)
        {
            get
            {
                return base.List.Item(index) as ExportColumn;
            }
            set
            {
                base.List.Item(index) = value;
            }
        }
    }
}
