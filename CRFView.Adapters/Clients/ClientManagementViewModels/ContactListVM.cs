﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Clients.ClientManagementViewModels
{
    public class ContactListVM
    {
        public bool chkLiens { get; set; } = true;
        public bool chkCollection { get; set; } = true;
        public bool chkTexas { get; set; }
        public bool chkCollectView { get; set; }
        public bool chkLienView { get; set; }
        public bool chkOther { get; set; }
    }
}
