﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Clients.ClientManagementViewModels
{
    public class ContractHistoryVM
    {
        public int Id { get; set; }
        public int ContractId { get; set; }
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string ChangeType { get; set; }
        public DateTime DateChanged { get; set; }
        public string ChangeFrom { get; set; }
        public string ChangeTo { get; set; }

    }
}
