﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Clients.ClientManagementViewModels
{
    public class RapidPayClientsVM
    {

        public RapidPayClientsVM()
        {
            objADClientRapidPay = new ADClientRapidPay();
            SalesTeamList = CommonBindList.GetSalesTeamList();
            ServiceTeamList = CommonBindList.GetServiceTeamList();
            ClientTypeList = CommonBindList.GetClientTypeList();
            AllClientIdList = CommonBindList.GetClientIdList();
           

        }

        #region properties

        public IEnumerable<SelectListItem> SalesTeamList { get; set; }
        public IEnumerable<SelectListItem> ServiceTeamList { get; set; }
        public IEnumerable<SelectListItem> ClientTypeList { get; set; }

        public List<ADClientRapidPay> ObjClientRapidPayList { get; set; }
        public ADClientRapidPay objADClientRapidPay { get; set; }

        public int SalesTeamId { get; set; }
        public int ServiceTeamId { get; set; }
        public int ClientTypeId { get; set; }
        public string RbtButton { get; set; }
        public bool chkByRequestDate { get; set; }
        public bool ByAllOrders { get; set; }
        public bool ByNotProcessed { get; set; }
        public DateTime DateTimePicker1 { get; set; }
        public DateTime DateTimePicker2 { get; set; }
        public bool chkFollowUp { get; set; }
        public string Value1 { get; set; }

        public string ddlAllClientId { get; set; }
        public IEnumerable<SelectListItem> AllClientIdList { get; set; }
  

        public int JobUnits { get; set; }
        public string UnitPrice { get; set; }
        public string TotPurchaseAmt { get; set; }


        #endregion
    }
}
