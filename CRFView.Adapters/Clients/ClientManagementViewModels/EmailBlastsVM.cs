﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Clients.ClientManagementViewModels
{
    public class EmailBlastsVM
    {

        public string FromName { get; set; }

        public string FromEmail { get; set; }

        public string SMTPUserId { get; set; }

        public string SMTPPassword { get; set; }

        public string Subject { get; set; }

        public string Body { get; set; }

        public string DocumentsListBox1 { get; set; }

        public bool chkLiens { get; set; }

        public bool chkLVUsers { get; set; }

        public bool chkTexas { get; set; }

        public bool chkCollection { get; set; }

        public bool chkCVUsers { get; set; }

        public bool chkOther { get; set; }

        public bool chkOutLook { get; set; }

        public DataTable dtEmailBlastsList { get; set; }

        public List<ADBatchClientEmailBlast> EmailBlastsList { get; set; }


    }
}
