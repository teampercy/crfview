﻿using CRFView.Adapters.Clients.Client_Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Clients.ClientManagementViewModels
{
    public class IssueVM
    {
        #region Properties
        public ADIssues Obj_Issue { get; set; }
        public IEnumerable<SelectListItem> InternalUserList { get; set; }
        #endregion

        #region Constructor
        public IssueVM()
        {
            Obj_Issue = new ADIssues();
        }
        #endregion
    }
}
