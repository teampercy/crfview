﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text;
using System.Web.Mvc;
using CRFView.Adapters.Clients.Client_Management;


namespace CRFView.Adapters.Clients.ClientManagementViewModels
{
    public class ContractLienRatesVM
    { 
        #region Properties
           public int    Id                  {get;set;}     
           public int    ContractId          {get;set;}
           public string PriceCodeName       {get;set;}
           public string BillableEventName   {get;set;}
           public int    BillableEventId     {get;set;}
           public int    PriceCodeId         {get;set;}
           public bool   IsAllInclusivePrice {get;set;}
        #endregion
    }
}
