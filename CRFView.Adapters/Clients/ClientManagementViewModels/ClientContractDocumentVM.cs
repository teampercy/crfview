﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using CRF.BLL.Clients;
using CRFView.Adapters.Clients.Client_Management;
using System.Web.Mvc;

namespace CRFView.Adapters.Clients.ClientManagementViewModels
{
    public class ClientContractDocumentVM
    {
        public ADClientContractAttachments Obj_Attachment { get; set; }
        public DateTime DocumentDate { get; set; }
        public IEnumerable<SelectListItem> DocumentTypeList { get; set; }
        public string ContractId { get; set; }

        public ClientContractDocumentVM()
        {
            Obj_Attachment = new ADClientContractAttachments();
        }
    }
}
