﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using CRFView.Adapters.Clients.Client_Management;

namespace CRFView.Adapters.Clients.ClientManagementViewModels
{
    public class ClientContractVM
    {
        #region Properties
        public ADClientContract Obj_ClientContract { get; set; }
        public List<ContractLienRatesVM> LienRatesList { get; set; }
        public List<ContractHistoryVM> ContractHistoryList { get; set; }
        public List<DocumentsVM> DocumentList { get; set; }
        public IEnumerable<SelectListItem> ContractTypeList { get; set; }
        public IEnumerable<SelectListItem> ProductTypeList { get; set; }
        public IEnumerable<SelectListItem> AssociationList { get; set; }
        public IEnumerable<SelectListItem> SalesTeamsList { get; set; }
        public IEnumerable<SelectListItem> ServiceTeamsList { get; set; }
        public IEnumerable<SelectListItem> FinDocTypeList { get; set; }
        public IEnumerable<SelectListItem> FinDocCycleList { get; set; }
        public IEnumerable<SelectListItem> ContactPlanList { get; set; }
        public IEnumerable<SelectListItem> TenDayContactPlanList { get; set; }
        public IEnumerable<SelectListItem> RollOverToContractList { get; set; }
        public IEnumerable<SelectListItem> CommissionRateList { get; set; }
        public IEnumerable<SelectListItem> FinDocCollectionList { get; set; }
        public int NewRateId { get; set; }
        #endregion

        #region Constructor
        public ClientContractVM()
        {
            Obj_ClientContract = new ADClientContract();
        }
        #endregion
    }
}
