﻿using CRFView.Adapters.Clients.Client_Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Clients.ClientManagementViewModels
{
    public class ClientImageVM
    {
        public ADClientImage Obj_ADCImage { get; set; }
    }
}
