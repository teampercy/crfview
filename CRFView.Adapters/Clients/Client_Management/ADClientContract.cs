﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using CRF.BLL.Clients;
using CRF.BLL.CRFDB.VIEWS;

namespace CRFView.Adapters
{
    public class ADClientContract
    {
        #region Properties
        public int Id                         { get; set; }
        public int ParentContractId           { get; set; }
        public int BillingClientId            { get; set; }
        public int ContractTypeId             { get; set; }
        public int AssociationId              { get; set; }
        public int FinDocTypeId               { get; set; }
        public int ProductId                  { get; set; }
        public string Title                   { get; set; }
        public string ContractLanguage        { get; set; }
        public bool IsAllInclusivePrice       { get; set; }
        public bool IsTrustAccount            { get; set; }
        public int ContactPlanId              { get; set; }
        public string LastUpdatedBy           { get; set; }
        public DateTime LastUpdateDate        { get; set; }
        public bool IsClientViewCollection    { get; set; }
        public bool IsClientViewLiens         { get; set; }
        public bool WebNoteAccess             { get; set; }
        public bool ExportToCSV               { get; set; }
        public bool IsInterestShared          { get; set; }
        public bool CalcTotRcvOnPaidUs        { get; set; }
        public bool CalcTotRcvOnPaidClnt      { get; set; }
        public int CBRDays                    { get; set; }
        public bool ReportToCBR               { get; set; }
        public int HoldDaysOnPmts             { get; set; }
        public int FinDocCycleId              { get; set; }
        public bool UseBranchSummary          { get; set; }
        public bool UseClientSummary          { get; set; }
        public bool HasInvoiceLetters         { get; set; }
        public decimal LegalInterestRate      { get; set; }
        public bool AdvanceCC                 { get; set; }
        public bool IsGrossRemit              { get; set; }
        public bool IsPrecollect              { get; set; }
        public decimal ClientCostAmt          { get; set; }
        public int ServiceTeamId              { get; set; }
        public int SalesTeamId                { get; set; }
        public string ProdTeam                { get; set; }
        public decimal CollectionFeeRate      { get; set; }
        public bool IsSeparateStmt            { get; set; }
        public int CollectionRateId           { get; set; }
        public int ITDAVGAGE                  { get; set; }
        public bool IsIndPaidAgencyStmt       { get; set; }
        public bool IsIndPaidClientStmt       { get; set; }
        public decimal InterestShareRate      { get; set; }
        public bool IsIndStmt                 { get; set; }
        public bool IsAddCollectionFee        { get; set; }
        public string ContactPlanCode         { get; set; }
        public int Duration                   { get; set; }
        public bool IsLetterSetPrice          { get; set; }
        public decimal LetterSeriesPrice      { get; set; }
        public bool HasLetterSeries           { get; set; }
        public bool HasPhoneCalls             { get; set; }
        public int Letter1Days                { get; set; }
        public int Letter2Days                { get; set; }
        public int Letter3Days                { get; set; }
        public int Letter4Days                { get; set; }
        public int Letter5Days                { get; set; }
        public int Letter6Days                { get; set; }
        public decimal Letter1Price           { get; set; }
        public decimal Letter2Price           { get; set; }
        public decimal Letter3Price           { get; set; }
        public decimal Letter4Price           { get; set; }
        public decimal Letter5Price           { get; set; }
        public decimal Letter6Price           { get; set; }
        public int DefaultStatusId            { get; set; }
        public int DefaultLocationId          { get; set; }
        public int CloseLocationId            { get; set; }
        public int CloseStatusId              { get; set; }
        public int InitialCallDelay           { get; set; }
        public bool IsRollover                { get; set; }
        public int RolloverContractid         { get; set; }
        public int RolloverListDays           { get; set; }
        public bool IsIndAck                  { get; set; }
        public bool IsRegAck                  { get; set; }
        public bool IsCloseRpt                { get; set; }
        public bool IsMasterRpt               { get; set; }
        public bool IsPmtRpt                  { get; set; }
        public bool IsRolloverList            { get; set; }
        public string ServiceType             { get; set; }
        public int HoldDaysOnPmtsOutState     { get; set; }
        public decimal CollectionFeeFromBal   { get; set; }
        public bool IsNoInterest              { get; set; }
        public decimal PreCollRate            { get; set; }
        public int PreCollRateOnLtrNo         { get; set; }
        public bool IsEmailRpt                { get; set; }
        public string EmailRptTo              { get; set; }
        public bool IsNoAck                   { get; set; }
        public bool IsStatusRpt               { get; set; }
        public bool IsShowAttyFeesOnStmt      { get; set; }
        public bool IsSeparateLegalStmt       { get; set; }
        public bool IsCustomInvoice           { get; set; }
        public DateTime OwnershipDate         { get; set; }
        public bool IsEmailInvoice            { get; set; }
        public bool IsIntlClient              { get; set; }
        public bool IncludeAdvCCGross         { get; set; }
        public bool IsInterestSharedAsPrinc   { get; set; }
        public DateTime SetupDate             { get; set; }
        public int TenDayContactPlanId        { get; set; }
        public bool IsTenDayContactPlan       { get; set; }
        public bool IsClientViewJob           { get; set; }
        public bool UseJobviewMenu            { get; set; }
        public bool IsBypassCreditCardFee { get; set; }
        public bool IsACH { get; set; }
        public String InvoiceTerms { get; set; }

        public bool IsPaysACH { get; set; }
        #endregion

        #region CRUD
        public static ADClientContract ReadClientContractType(int id)
        {
            ClientContract myentity = new ClientContract();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (ClientContract)myentityItable;
            AutoMapper.Mapper.CreateMap<ClientContract, ADClientContract>();
            return (AutoMapper.Mapper.Map<ADClientContract>(myentity));
        }
       // public static bool SaveClientLienContract(ADClientContract Obj_ClCon, CurrentUser Obj_CU)
        public static bool SaveClientLienContract(ADClientContract Obj_ClCon, CRF.BLL.Users.CurrentUser Obj_CU)
        {
            try
            {

                AutoMapper.Mapper.CreateMap<ADClientContract, ClientContract>();

                #region Update Record
                if (Obj_ClCon.Id != 0)
                {
                    var mysproc = new uspbo_CRM_UpdateClientContractHistory();
                    mysproc.ContractId = Obj_ClCon.Id;
                    mysproc.UserId = Obj_CU.Id;
                    mysproc.UserName = Obj_CU.UserName;
                    mysproc.ContractTypeId = 2;

                    #region Read Record from database for comparison
                    ClientContract myentity = new ClientContract();
                    ITable myentityItable = myentity;
                    DBO.Provider.Read(Obj_ClCon.Id.ToString(), ref myentityItable);
                    myentity = (ClientContract)myentityItable;
                    AutoMapper.Mapper.CreateMap<ClientContract, ADClientContract>();
                    ADClientContract temp = AutoMapper.Mapper.Map<ADClientContract>(myentity);
                    #endregion

                    #region Determine Change Type
                    if (temp.SalesTeamId != Obj_ClCon.SalesTeamId)
                    {
                        Obj_ClCon.OwnershipDate = DateTime.Now;
                    }

                    if (Obj_ClCon.SetupDate != temp.SetupDate)
                    {
                        mysproc.ChangeType = 1;
                        if (temp.SetupDate == DateTime.MinValue)
                        {
                            mysproc.ChangeFrom = "";
                        }
                        else
                        {
                            mysproc.ChangeFrom = temp.SetupDate.ToString();
                        }
                        mysproc.ChangeTo = Obj_ClCon.SetupDate.ToString();
                        DBO.Provider.ExecNonQuery(mysproc);
                    }

                    if (Obj_ClCon.OwnershipDate != temp.OwnershipDate)
                    {
                        mysproc.ChangeType = 2;
                        mysproc.ChangeFrom = temp.OwnershipDate.ToString();
                        mysproc.ChangeTo = Obj_ClCon.OwnershipDate.ToString();
                        DBO.Provider.ExecNonQuery(mysproc);
                    }
                    if (Obj_ClCon.SalesTeamId != temp.SalesTeamId)
                    {
                        if (temp.SalesTeamId != 0)
                        {
                            ITable myTeamOld = new Team();
                            DBO.Provider.Read(temp.SalesTeamId.ToString(), ref myTeamOld);
                            Team tmObjOld = new Team();
                            tmObjOld = (Team)myTeamOld;

                            ITable myTeamNew = new Team();
                            DBO.Provider.Read(Obj_ClCon.SalesTeamId.ToString(), ref myTeamNew);
                            Team tmObjNew = new Team();
                            tmObjNew = (Team)myTeamNew;

                            mysproc.ChangeType = 3;
                            mysproc.ChangeFrom = tmObjOld.TeamName;
                            mysproc.ChangeTo = tmObjNew.TeamName;
                            DBO.Provider.ExecNonQuery(mysproc);
                        }
                    }
                    #endregion

                    myentity.Title = Obj_ClCon.Title;
                    myentity.ContractLanguage = Obj_ClCon.ContractLanguage;
                    myentity.ContractTypeId = Obj_ClCon.ContractTypeId;
                    myentity.ProductId = Obj_ClCon.ProductId;
                    myentity.AssociationId = Obj_ClCon.AssociationId;
                    myentity.IsClientViewLiens = Obj_ClCon.IsClientViewLiens;
                    myentity.IsClientViewJob = Obj_ClCon.IsClientViewJob;
                    myentity.UseJobViewMenu = Obj_ClCon.UseJobviewMenu;
                    myentity.SalesTeamId = Obj_ClCon.SalesTeamId;
                    myentity.ServiceTeamId = Obj_ClCon.ServiceTeamId;
                    myentity.ProdTeam = Obj_ClCon.ProdTeam;
                    myentity.FinDocTypeId = Obj_ClCon.FinDocTypeId;
                    myentity.FinDocCycleId = Obj_ClCon.FinDocCycleId;
                    myentity.IsEmailInvoice = Obj_ClCon.IsEmailInvoice;
                    myentity.IsAllInclusivePrice = Obj_ClCon.IsAllInclusivePrice;
                    myentity.ExportToCSV = Obj_ClCon.ExportToCSV;
                    myentity.UseBranchSummary = Obj_ClCon.UseBranchSummary;
                    myentity.UseClientSummary = Obj_ClCon.UseClientSummary;
                    myentity.SetupDate = Obj_ClCon.SetupDate;
                    myentity.OwnershipDate = Obj_ClCon.OwnershipDate;
                    myentity.IsBypassCreditCardFee = Obj_ClCon.IsBypassCreditCardFee;
                    myentity.InvoiceTerms = Obj_ClCon.InvoiceTerms;
                    myentity.IsPaysACH = Obj_ClCon.IsPaysACH;

                    ITable tmp = myentity;
                    DBO.Provider.Update(ref tmp);
                }
                #endregion
                #region Create Record
                else
                {
                    ClientContract ObjClientContract = AutoMapper.Mapper.Map<ClientContract>(Obj_ClCon);
                    ObjClientContract.ContractTypeId = 2;
                    ITable tblClientContract = ObjClientContract;
                    DBO.Provider.Create(ref tblClientContract);

                }
                #endregion
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public static bool SaveClientCollectionContract(ADClientContract Obj_ClCon, CurrentUser Obj_CU)
        {
            try
            {
                var mysproc = new uspbo_CRM_UpdateClientContractHistory();
                mysproc.ContractId = Obj_ClCon.Id;
                mysproc.UserId = Obj_CU.Id;
                mysproc.UserName = Obj_CU.UserName;
                mysproc.ContractTypeId = 1;

                #region Read Record from database for comparison
                ClientContract myentity = new ClientContract();
                ITable myentityItable = myentity;
                DBO.Provider.Read(Obj_ClCon.Id.ToString(), ref myentityItable);
                myentity = (ClientContract)myentityItable;
                AutoMapper.Mapper.CreateMap<ClientContract, ADClientContract>();
                ADClientContract temp = AutoMapper.Mapper.Map<ADClientContract>(myentity);
                #endregion

                #region Determine change type
                if (temp.SalesTeamId != Obj_ClCon.SalesTeamId)
                {
                    Obj_ClCon.OwnershipDate = DateTime.Now;
                }

                if (Obj_ClCon.SetupDate != temp.SetupDate)
                {
                    mysproc.ChangeType = 1;
                    if (temp.SetupDate == DateTime.MinValue)
                    {
                        mysproc.ChangeFrom = "";
                    }
                    else
                    {
                        mysproc.ChangeFrom = temp.SetupDate.ToString();
                    }
                    mysproc.ChangeTo = Obj_ClCon.SetupDate.ToString();
                    DBO.Provider.ExecNonQuery(mysproc);
                }
                if (Obj_ClCon.OwnershipDate != temp.OwnershipDate)
                {
                    mysproc.ChangeType = 2;
                    mysproc.ChangeFrom = temp.OwnershipDate.ToString();
                    mysproc.ChangeTo = Obj_ClCon.OwnershipDate.ToString();
                    DBO.Provider.ExecNonQuery(mysproc);
                }
                if (Obj_ClCon.SalesTeamId != temp.SalesTeamId)
                {
                    if (temp.SalesTeamId != 0)
                    {
                        ITable myTeamOld = new Team();
                        DBO.Provider.Read(temp.SalesTeamId.ToString(), ref myTeamOld);
                        Team tmObjOld = new Team();
                        tmObjOld = (Team)myTeamOld;

                        ITable myTeamNew = new Team();
                        DBO.Provider.Read(Obj_ClCon.SalesTeamId.ToString(), ref myTeamNew);
                        Team tmObjNew = new Team();
                        tmObjNew = (Team)myTeamNew;

                        mysproc.ChangeType = 3;
                        mysproc.ChangeFrom = tmObjOld.TeamName;
                        mysproc.ChangeTo = tmObjNew.TeamName;
                        DBO.Provider.ExecNonQuery(mysproc);
                    }
                }
                if(Obj_ClCon.CollectionRateId != Obj_ClCon.CollectionRateId)
                {
                    if(temp.CollectionRateId != 0)
                    {
                        ITable myCollectionOld = new CollectionRate();
                        DBO.Provider.Read(temp.CollectionRateId.ToString(), ref myCollectionOld);
                        CollectionRate tmObjOld = new CollectionRate();
                        tmObjOld = (CollectionRate)myCollectionOld;

                        ITable myCollectionRateNew = new CollectionRate();
                        DBO.Provider.Read(Obj_ClCon.CollectionRateId.ToString(), ref myCollectionRateNew);
                        CollectionRate tmObjNew = new CollectionRate();
                        tmObjNew = (CollectionRate)myCollectionRateNew;

                        mysproc.ChangeType = 4;
                        mysproc.ChangeFrom = tmObjOld.CollectionRateCode.Substring(1, 3) + "-" + tmObjOld.CollectionRateName;
                        mysproc.ChangeTo = tmObjNew.CollectionRateCode.Substring(1, 3) + "-" + tmObjNew.CollectionRateCode;
                        DBO.Provider.ExecNonQuery(mysproc);
                    }                   
                }
                #endregion

                AutoMapper.Mapper.CreateMap<ADClientContract, ClientContract>();

                #region Update Record
                if (Obj_ClCon.Id != 0)
                {
                    myentity.Title = Obj_ClCon.Title;
                    myentity.SalesTeamId = Obj_ClCon.SalesTeamId;
                    myentity.ServiceTeamId = Obj_ClCon.ServiceTeamId;
                    myentity.ContactPlanId = Obj_ClCon.ContactPlanId;
                    myentity.IsTenDayContactPlan = Obj_ClCon.IsTenDayContactPlan;
                    myentity.TenDayContactPlanId = Obj_ClCon.TenDayContactPlanId;
                    myentity.IsTrustAccount = Obj_ClCon.IsTrustAccount;
                    myentity.SetupDate = Obj_ClCon.SetupDate;
                    myentity.OwnershipDate = Obj_ClCon.OwnershipDate;
                    myentity.IsClientViewCollection = Obj_ClCon.IsClientViewCollection;
                    myentity.IsIntlClient = Obj_ClCon.IsIntlClient;
                    myentity.CollectionRateId = Obj_ClCon.CollectionRateId;
                    myentity.HoldDaysOnPmts = Obj_ClCon.HoldDaysOnPmts;
                    myentity.HoldDaysOnPmtsOutState = Obj_ClCon.HoldDaysOnPmtsOutState;
                    myentity.FinDocTypeId = Obj_ClCon.FinDocTypeId;
                    myentity.IsCustomInvoice = Obj_ClCon.IsCustomInvoice;
                    myentity.FinDocCycleId = Obj_ClCon.FinDocCycleId;
                    myentity.IsEmailInvoice = Obj_ClCon.IsEmailInvoice;
                    myentity.IsGrossRemit = Obj_ClCon.IsGrossRemit;
                    myentity.IncludeAdvCCGross = Obj_ClCon.IncludeAdvCCGross;
                    myentity.IsIndStmt = Obj_ClCon.IsIndStmt;
                    myentity.IsIndPaidAgencyStmt = Obj_ClCon.IsIndPaidAgencyStmt;
                    myentity.IsIndPaidClientStmt = Obj_ClCon.IsIndPaidClientStmt;
                    myentity.IsShowAttyFeesOnStmt = Obj_ClCon.IsShowAttyFeesOnStmt;
                    myentity.IsSeparateLegalStmt = Obj_ClCon.IsSeparateLegalStmt;
                    myentity.IsNoInterest = Obj_ClCon.IsNoInterest;
                    myentity.IsInterestShared = Obj_ClCon.IsInterestShared;
                    myentity.InterestShareRate = Obj_ClCon.InterestShareRate;
                    myentity.IsInterestSharedAsPrinc = Obj_ClCon.IsInterestSharedAsPrinc;
                    myentity.AdvanceCC = Obj_ClCon.AdvanceCC;
                    myentity.IsAddCollectionFee = Obj_ClCon.IsAddCollectionFee;
                    myentity.ClientCostAmt = Obj_ClCon.ClientCostAmt;
                    myentity.CollectionFeeRate = Obj_ClCon.CollectionFeeRate;
                    myentity.CollectionFeeFromBal = Obj_ClCon.CollectionFeeFromBal;
                    myentity.Duration = Obj_ClCon.Duration;
                    myentity.RolloverListDays = Obj_ClCon.RolloverListDays;
                    myentity.IsRolloverList = Obj_ClCon.IsRolloverList;
                    myentity.IsRollover = Obj_ClCon.IsRollover;
                    myentity.RolloverContractid = Obj_ClCon.RolloverContractid;
                    myentity.IsLetterSetPrice = Obj_ClCon.IsLetterSetPrice;
                    myentity.LetterSeriesPrice = Obj_ClCon.LetterSeriesPrice;
                    myentity.PreCollRate = Obj_ClCon.PreCollRate;
                    myentity.PreCollRateOnLtrNo = Obj_ClCon.PreCollRateOnLtrNo;
                    myentity.IsBypassCreditCardFee = Obj_ClCon.IsBypassCreditCardFee;
                    myentity.InvoiceTerms = Obj_ClCon.InvoiceTerms;
                    myentity.IsPaysACH = Obj_ClCon.IsPaysACH;
                    myentity.IsACH = Obj_ClCon.IsACH;

                    ITable tmp = myentity;
                    DBO.Provider.Update(ref tmp);
                }
                #endregion
                #region Create Record
                else
                {
                    ClientContract ObjClientContract = AutoMapper.Mapper.Map<ClientContract>(Obj_ClCon);
                    ObjClientContract.ContractTypeId = 1;
                    ITable tblClientContract = ObjClientContract;
                    DBO.Provider.Create(ref tblClientContract);
                }
                #endregion

                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public static List<NewLienRatesVM> ratelist(CurrentUser CU_Obj, int Id, int BillableEventId, bool IsAllInclusivePrice)
        {
            AutoMapper.Mapper.CreateMap<CurrentUser, CRF.BLL.Users.CurrentUser>();
            CRF.BLL.Users.CurrentUser CurrUsr_Obj = AutoMapper.Mapper.Map<CRF.BLL.Users.CurrentUser>(CU_Obj);
            var myPricing = new CRF.BLL.CRFDB.TABLES.ClientContractPricing();
            ITable Ipricing = myPricing;
            DBO.Provider.Read(Id.ToString(), ref Ipricing);
            myPricing = (ClientContractPricing)Ipricing;
            TableView MYVIEW;
            MYVIEW = CRF.BLL.Clients.Processes.GetRatesForBillableEvent(CurrUsr_Obj, BillableEventId, IsAllInclusivePrice);
            MYVIEW.Sort = CRF.BLL.CRFDB.VIEWS.vwPriceCode.ColumnNames.descr;
            DataTable dtRates = MYVIEW.ToTable();
            List<NewLienRatesVM> RateList = (from DataRow dr in dtRates.Rows
                                             select new NewLienRatesVM()
                                             {
                                                 descr = ((dr["descr"] != DBNull.Value ? dr["descr"].ToString() : "")),
                                                 BillableEventName = ((dr["BillableEventName"] != DBNull.Value ? dr["BillableEventName"].ToString() : "")),
                                                 Id = ((dr["Id"] != DBNull.Value ? dr["Id"].ToString() : "")),
                                                 PriceCode = ((dr["PriceCode"] != DBNull.Value ? dr["PriceCode"].ToString() : "")),
                                                 IsAllInclusivePrice = ((dr["IsAllInclusivePrice"] != DBNull.Value ? dr["IsAllInclusivePrice"].ToString() : "")),
                                                 PrelimPercentage = ((dr["PrelimPercentage"] != DBNull.Value ? dr["PrelimPercentage"].ToString() : "")),
                                                 UsePrelimVolume = ((dr["UsePrelimVolume"] != DBNull.Value ? dr["UsePrelimVolume"].ToString() : "")),
                                                 BillableEventId = ((dr["BillableEventId"] != DBNull.Value ? dr["BillableEventId"].ToString() : "")),
                                                 IsFreeFormPricing = ((dr["IsFreeFormPricing"] != DBNull.Value ? dr["IsFreeFormPricing"].ToString() : ""))
                                             }).ToList();
            return RateList;
        }
        
        public static bool ChangeLienRate(CurrentUser CU_Obj, string Id, string value, string text, string PriceCodeNameOld, string ContractId)
        {
            try
            {
                ClientEditVM ClientEditVM_Obj = new ClientEditVM();
                AutoMapper.Mapper.CreateMap<CurrentUser, CRF.BLL.Users.CurrentUser>();
                CRF.BLL.Users.CurrentUser CerrUsr_Obj = AutoMapper.Mapper.Map<CRF.BLL.Users.CurrentUser>(CU_Obj);

                var mysproc = new uspbo_CRM_UpdateClientContractHistory();
                mysproc.UserId = CerrUsr_Obj.Id;
                mysproc.UserName = CerrUsr_Obj.UserName;
                mysproc.ChangeType = 4;
                mysproc.ChangeFrom = PriceCodeNameOld;
                mysproc.ChangeTo = text;
                mysproc.ContractTypeId = 2;
                mysproc.ContractId = Convert.ToInt32( ContractId);
                if (mysproc.ChangeFrom != mysproc.ChangeTo)
                {
                    DBO.Provider.ExecNonQuery(mysproc);
                }
                ITable Ipricing = new ClientContractPricing();
                DBO.Provider.Read(Id.ToString(), ref Ipricing);
                ClientContractPricing tmp = (ClientContractPricing)Ipricing;
                tmp.PriceCodeId = Convert.ToInt32(value);
                ITable Ipricing2 = tmp;
                DBO.Provider.Update(ref Ipricing2);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }

            
        }
        #endregion

        #region Local Methods
        public static void BindDropdowns(ref ClientContractVM CCVM_Obj,int ClientId=0)
        {
            CCVM_Obj.ContractTypeList = CommonBindList.GetContactTypeList();
            CCVM_Obj.ProductTypeList = CommonBindList.GetProductTypeList();
            CCVM_Obj.ServiceTeamsList = CommonBindList.GetServiceTeamList();
            CCVM_Obj.SalesTeamsList = CommonBindList.GetSalesTeamList();
            CCVM_Obj.FinDocTypeList = CommonBindList.GetFinDocTypesList();
            CCVM_Obj.FinDocCycleList = CommonBindList.GetFinDocCycleList();
            CCVM_Obj.AssociationList = CommonBindList.GetAssociationList();
            CCVM_Obj.ContactPlanList = CommonBindList.GetContactPlanList();
            CCVM_Obj.TenDayContactPlanList = CommonBindList.GetTenDatContactPlan();
            CCVM_Obj.RollOverToContractList = CommonBindList.GetContractListForClient(ClientId);
            CCVM_Obj.CommissionRateList = CommonBindList.GetCommissionRates();
        }

        public static List<ContractLienRatesVM> GetLienRate(CurrentUser CU_Obj, int id)
        {
            /////////////////////////////////////////////////////////////////////
            var mycontract = new ClientContract();
            ITable Icontract = mycontract;
            DBO.Provider.Read(id.ToString(), ref Icontract);
            mycontract = (ClientContract)Icontract;

            var myview = new vwContractPricing();
            var myquery = new Query(myview);
            string mysql;
            myquery.AddCondition(HDS.DAL.COMMON.LogicalOperators._And, vwContractPricing.ColumnNames.ContractId, HDS.DAL.COMMON.Conditions._IsEqual, id.ToString());
            myview.IsAllInclusivePrice = mycontract.IsAllInclusivePrice;
            mysql = myquery.BuildQuery();
            var PricingView = DBO.Provider.GetTableView(mysql);
            DataTable dt_LienRatesC = PricingView.ToTable();
            List<ContractLienRatesVM> LienRateList = new List<ContractLienRatesVM>();
            if (dt_LienRatesC.Rows.Count >= 1)
            {
                LienRateList = (from DataRow dr in dt_LienRatesC.Rows
                                select new ContractLienRatesVM()
                                {
                                    Id = Convert.ToInt32(dr["Id"]),
                                    ContractId = Convert.ToInt32(dr["ContractId"]),
                                    PriceCodeName = ((dr["PriceCodeName"] != DBNull.Value ? dr["PriceCodeName"].ToString() : "")),
                                    BillableEventName = ((dr["BillableEventName"] != DBNull.Value ? dr["BillableEventName"].ToString() : "")),
                                    BillableEventId = ((dr["BillableEventId"] != DBNull.Value ? Convert.ToInt32(dr["BillableEventId"]) : 0)),
                                    PriceCodeId = ((dr["PriceCodeId"] != DBNull.Value ? Convert.ToInt32(dr["PriceCodeId"]) : 0)),
                                    IsAllInclusivePrice = ((dr["IsAllInclusivePrice"] != DBNull.Value ? Convert.ToBoolean(dr["IsAllInclusivePrice"]) : false))
                                }).ToList();
            }
            return LienRateList;

            /////////////////////////////////////////////////////////////////////
            //string sql = "Select Id  from ClientContract Where BillingClientId = '" + id + "' and ContractTypeId=2";
            //System.Data.DataSet ds = DBO.Provider.GetDataSet(sql);
            //DataTable dt = ds.Tables[0];
            //var df = DBO.Provider.GetTableView(sql);
            //AutoMapper.Mapper.CreateMap<CurrentUser, CRF.BLL.Users.CurrentUser>();
            //CRF.BLL.Users.CurrentUser CerrUsr_Obj = AutoMapper.Mapper.Map<CRF.BLL.Users.CurrentUser>(CU_Obj);            
            //var abc = CRF.BLL.Clients.Processes.GetPricingForContact(CerrUsr_Obj, 8303);
            //DataTable dt_LienRates = abc.ToTable();
            //List<ContractLienRatesVM> LienRateList = new List<ContractLienRatesVM>();
            //if (dt_LienRates.Rows.Count > 1)
            //{
            //    LienRateList = (from DataRow dr in dt_LienRates.Rows
            //                    select new ContractLienRatesVM()
            //                    {
            //                       // id =  
            //                        ContractId = Convert.ToInt32(dr["ContractId"]),
            //                        PriceCodeName = ((dr["PriceCodeName"] != DBNull.Value ? dr["PriceCodeName"].ToString() : "")),
            //                        BillableEventName = ((dr["BillableEventName"] != DBNull.Value ? dr["BillableEventName"].ToString() : "")),
            //                        BillableEventId = ((dr["BillableEventId"] != DBNull.Value ? Convert.ToInt32(dr["BillableEventId"]) : 0)),
            //                        PriceCodeId = ((dr["PriceCodeId"] != DBNull.Value ? Convert.ToInt32(dr["PriceCodeId"]) : 0)),
            //                        IsAllInclusivePrice = ((dr["IsAllInclusivePrice"] != DBNull.Value ? Convert.ToBoolean(dr["IsAllInclusivePrice"]) : false))
            //                    }).ToList();
            //}
            //return LienRateList;
        }

        public static List<ContractHistoryVM> GetLienHistory(CurrentUser CU_Obj,int id)
        {

            AutoMapper.Mapper.CreateMap<CurrentUser, CRF.BLL.Users.CurrentUser>();
            CRF.BLL.Users.CurrentUser CerrUsr_Obj = AutoMapper.Mapper.Map<CRF.BLL.Users.CurrentUser>(CU_Obj);
            CRF.BLL.Clients.ClientInfo myentityinfo = CRF.BLL.Clients.Processes.GetClientInfo(CerrUsr_Obj, id);
            TableView LienContractHistory = myentityinfo.LienContractHistory();
            DataTable dtLienContractHistory = LienContractHistory.ToTable();
            var abc = myentityinfo.Contracts();
            List<ContractHistoryVM> LienHistoryList = new List<ContractHistoryVM>();
            if (dtLienContractHistory.Rows.Count >= 1)
            {
                LienHistoryList = (from DataRow dr in dtLienContractHistory.Rows
                                select new ContractHistoryVM()
                                {
                                    Id = Convert.ToInt32(dr["Id"]),
                                    ContractId = Convert.ToInt32(dr["ContractId"]),
                                    UserId = Convert.ToInt32(dr["UserId"]),
                                    UserName = ((dr["UserName"] != DBNull.Value ? dr["UserName"].ToString() : "")),
                                    ChangeType = ((dr["ChangeType"] != DBNull.Value ? dr["ChangeType"].ToString() : "")),
                                    DateChanged = ((dr["DateChanged"] != DBNull.Value ? Convert.ToDateTime( dr["DateChanged"]) : DateTime.MinValue)),
                                    ChangeFrom = ((dr["ChangeFrom"] != DBNull.Value ? dr["ChangeFrom"].ToString() : "")),
                                    ChangeTo = ((dr["ChangeTo"] != DBNull.Value ? dr["ChangeTo"].ToString() : "")),
                                }).ToList();
            }
            return LienHistoryList;
        }

        public static List<ContractHistoryVM> GetCollectionHistory(CurrentUser CU_Obj, int id)
        {

            AutoMapper.Mapper.CreateMap<CurrentUser, CRF.BLL.Users.CurrentUser>();
            CRF.BLL.Users.CurrentUser CerrUsr_Obj = AutoMapper.Mapper.Map<CRF.BLL.Users.CurrentUser>(CU_Obj);
            CRF.BLL.Clients.ClientInfo myentityinfo = CRF.BLL.Clients.Processes.GetClientInfo(CerrUsr_Obj, id);
            TableView CollectionContractHistory = myentityinfo.CollContractHistory();
            DataTable dtLienContractHistory = CollectionContractHistory.ToTable();
            var abc = myentityinfo.Contracts();
            List<ContractHistoryVM> CollectionHistoryList = new List<ContractHistoryVM>();
            if (dtLienContractHistory.Rows.Count >= 1)
            {
                CollectionHistoryList = (from DataRow dr in dtLienContractHistory.Rows
                                   select new ContractHistoryVM()
                                   {
                                       Id = Convert.ToInt32(dr["Id"]),
                                       ContractId = Convert.ToInt32(dr["ContractId"]),
                                       UserId = Convert.ToInt32(dr["UserId"]),
                                       UserName = ((dr["UserName"] != DBNull.Value ? dr["UserName"].ToString() : "")),
                                       ChangeType = ((dr["ChangeType"] != DBNull.Value ? dr["ChangeType"].ToString() : "")),
                                       DateChanged = ((dr["DateChanged"] != DBNull.Value ? Convert.ToDateTime(dr["DateChanged"]) : DateTime.MinValue)),
                                       ChangeFrom = ((dr["ChangeFrom"] != DBNull.Value ? dr["ChangeFrom"].ToString() : "")),
                                       ChangeTo = ((dr["ChangeTo"] != DBNull.Value ? dr["ChangeTo"].ToString() : "")),
                                   }).ToList();
            }
            return CollectionHistoryList;
        }

        public static List<DocumentsVM> GetDocuments(CurrentUser CU_Obj, int id)
        {
            string sql = "Select Id, DateCreated, UserCode, DocumentType, DocumentDescription from ClientContractAttachments ";
            sql += "Where ContractId = " + id;
            System.Data.DataSet ds = DBO.Provider.GetDataSet(sql);
            DataTable dt_documents = ds.Tables[0];
            List<DocumentsVM> DocumentList = new List<DocumentsVM>();
            if (dt_documents.Rows.Count >= 1)
            {
                DocumentList = (from DataRow dr in dt_documents.Rows
                                select new DocumentsVM()
                                {
                                    Id = Convert.ToInt32(dr["Id"]),
                                    DateCreated = ((dr["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dr["DateCreated"]) :DateTime.MinValue)),
                                    UserCode = ((dr["UserCode"] != DBNull.Value ? dr["UserCode"].ToString() : "")),
                                    DocumentType = ((dr["DocumentType"] != DBNull.Value ? dr["DocumentType"].ToString() : "")),
                                    DocumentDescription = ((dr["DocumentDescription"] != DBNull.Value ? dr["DocumentDescription"].ToString() : "")),
                                }).ToList();
            }
            return DocumentList;
        }

        public static List<ADClientContract> GetClientContractForId(CurrentUser CU_Obj,int id)
        {
            AutoMapper.Mapper.CreateMap<CurrentUser, CRF.BLL.Users.CurrentUser>();
            CRF.BLL.Users.CurrentUser CerrUsr_Obj = AutoMapper.Mapper.Map<CRF.BLL.Users.CurrentUser>(CU_Obj);
            CRF.BLL.Clients.ClientInfo myentityinfo = CRF.BLL.Clients.Processes.GetClientInfo(CerrUsr_Obj, id);
            TableView ViewContracts = myentityinfo.Contracts();
            DataTable dtContracts = ViewContracts.ToTable();
            List<ADClientContract> ClientContractList = new List<ADClientContract>();
            if (dtContracts.Rows.Count >= 1)
            {
                ClientContractList = (from DataRow dr in dtContracts.Rows
                                      select new ADClientContract()
                                      {
                                          Id = Convert.ToInt32(dr["Id"]),
                                       Title = ((dr["Title"] != DBNull.Value ? dr["Title"].ToString() : "")),
                                       ContractTypeId = Convert.ToInt32(dr["ContractTypeId"]),
                                      }).ToList();
            }
                return ClientContractList;
        }

        public static List<ADClient> GetBranchesForId(CurrentUser CU_Obj, int id)
        {
            AutoMapper.Mapper.CreateMap<CurrentUser, CRF.BLL.Users.CurrentUser>();
            CRF.BLL.Users.CurrentUser CerrUsr_Obj = AutoMapper.Mapper.Map<CRF.BLL.Users.CurrentUser>(CU_Obj);
            CRF.BLL.Clients.ClientInfo myentityinfo = CRF.BLL.Clients.Processes.GetClientInfo(CerrUsr_Obj, id);
            TableView Viewbranches = myentityinfo.Branches();
            DataTable dtBranches = Viewbranches.ToTable();
            List<ADClient> BranchList = new List<ADClient>();
            if (dtBranches.Rows.Count >= 1)
            {
                BranchList = (from DataRow dr in dtBranches.Rows
                                      select new ADClient()
                                      {
                                          ClientId = Convert.ToInt32(dr["ClientId"]),
                                          BranchNumber = ((dr["BranchNumber"] != DBNull.Value ? dr["BranchNumber"].ToString() : "")),
                                          ClientName = (dr["ClientName"] != DBNull.Value ? dr["ClientName"].ToString() : ""),
                                          ContactName = ((dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : "")),
                                          PhoneNo = (dr["PhoneNo"] !=DBNull.Value ? dr["PhoneNo"].ToString() : ""),
                                          Email = (dr["Email"] != DBNull.Value ? dr["Email"].ToString() : "")
                                      }).ToList();
            }
            return BranchList;
        }



        #endregion
    }
}
