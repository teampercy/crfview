﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using CRF.BLL.Clients;
using System.IO;

namespace CRFView.Adapters.Clients.Client_Management
{
    public class ADClientContractAttachments
    {
        #region Properties
        public int Id                      {get;set;}
        public int ContractId              {get;set;}
        public int UserId                  {get;set;}
        public string UserCode             {get;set;}
        public DateTime DateCreated        {get;set;}
        public string FileName             {get;set;}
        public string DocumentType         {get;set;}
        public string DocumentDescription  {get;set;}
        public byte[] DocumentImage        {get;set;}
        #endregion

        #region CRUD
        public static List<ADClientContractAttachments> GetLienContractDocuments(int ContractId)
        {
            string MYSQL  = "Select Id, DateCreated, UserCode, DocumentType, DocumentDescription from ClientContractAttachments" ;
            MYSQL += "Where ContractId = " + ContractId;
            DataSet MYDT = DBO.Provider.GetDataSet(MYSQL);
            List<ADClientContractAttachments> AttachmentList = new List<ADClientContractAttachments>();
            return AttachmentList;
        }

        public static void SaveAttachmentDocument(ADClientContractAttachments obj_Attachment)
        {

            ITable Iattachment;
            AutoMapper.Mapper.CreateMap<ADClientContractAttachments, ClientContractAttachments>();
            ClientContractAttachments objCCA = AutoMapper.Mapper.Map<ClientContractAttachments>(obj_Attachment);
            Iattachment = objCCA;
            DBO.Provider.Create(ref Iattachment);

        }

        public static bool DeleteAttachmentDocument (string Id)
        {
            try
            {
                ITable Iattachment;
                ClientContractAttachments objCCA = new ClientContractAttachments();
                objCCA.Id = Convert.ToInt32(Id);
                Iattachment = objCCA;
                DBO.Provider.Delete(ref Iattachment);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }

        }

        public static ADClientContractAttachments ReadAttachmentDocument(string Id)
        {
            ClientContractAttachments myentity = new ClientContractAttachments();
            ITable myentityItable = myentity;
            DBO.Provider.Read(Id, ref myentityItable);
            myentity = (ClientContractAttachments)myentityItable;
            AutoMapper.Mapper.CreateMap<ClientContractAttachments, ADClientContractAttachments>();

            return (AutoMapper.Mapper.Map<ADClientContractAttachments>(myentity));
        }

        #endregion

        public static void SaveDocProperties(CurrentUser CU_Obj,ADClientContractAttachments OBj_CCD ,string path)
        {
            AutoMapper.Mapper.CreateMap<CurrentUser, CRF.BLL.Users.CurrentUser>();
            CRF.BLL.Users.CurrentUser CurrUsr_Obj = AutoMapper.Mapper.Map<CRF.BLL.Users.CurrentUser>(CU_Obj);
            byte[] bytes = File.ReadAllBytes(path);
            OBj_CCD.DocumentImage = bytes;
            OBj_CCD.FileName = path.Substring(path.LastIndexOf("\\")+1);
            OBj_CCD.UserCode = CurrUsr_Obj.UserName;
            OBj_CCD.UserId = CurrUsr_Obj.Id;
            OBj_CCD.DateCreated = DateTime.Now;
            ITable Iattachment;
            AutoMapper.Mapper.CreateMap<ADClientContractAttachments, ClientContractAttachments>();
            ClientContractAttachments objCCA = AutoMapper.Mapper.Map<ClientContractAttachments>(OBj_CCD);
            Iattachment = objCCA;
            DBO.Provider.Create(ref Iattachment);
        }

        public static bool UpdateDocProperies(CurrentUser CU_Obj,ADClientContractAttachments OBj_CCD)
        {
            try
            {
                ClientContractAttachments myentity = new ClientContractAttachments();
                ITable myentityItable = myentity;
                DBO.Provider.Read(Convert.ToString(OBj_CCD.Id), ref myentityItable);
                myentity = (ClientContractAttachments)myentityItable;
                AutoMapper.Mapper.CreateMap<CurrentUser, CRF.BLL.Users.CurrentUser>();
                CRF.BLL.Users.CurrentUser CurrUsr_Obj = AutoMapper.Mapper.Map<CRF.BLL.Users.CurrentUser>(CU_Obj);
                myentity.UserId = CurrUsr_Obj.Id;
                myentity.UserCode = CurrUsr_Obj.UserName;
                myentity.DocumentType = OBj_CCD.DocumentType;
                myentity.DocumentDescription = OBj_CCD.DocumentDescription;
                ITable myentityItableUpdate = myentity;
                DBO.Provider.Update(ref myentityItableUpdate);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }

        }

        public static void SaveFileToBeViewed(byte[] ByteArray,string filePath)
        {
            File.WriteAllBytes(filePath, ByteArray);
        }
    }
}
