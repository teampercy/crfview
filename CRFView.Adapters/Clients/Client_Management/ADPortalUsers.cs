﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using CRF.BLL.Clients;

namespace CRFView.Adapters.Clients.Client_Management
{
    public class ADPortalUsers
    {
        #region Properties
        public int ID { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string LoginCode { get; set; }
        public string LoginPassword { get; set; }
        public DateTime ExpireDate { get; set; }
        public DateTime ResetDate { get; set; }
        public string PasswordHint1 { get; set; }
        public string PasswordHint2 { get; set; }
        public string FName { get; set; }
        public string LName { get; set; }
        public string AddressLine2 { get; set; }
        public string AddressLine3 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
        public string PhoneNo { get; set; }
        public string Fax { get; set; }
        public string Note { get; set; }
        public int InternalUser { get; set; }
        public int ClientId { get; set; }
        public int ManagerId { get; set; }
        public byte[] SignatureLogo { get; set; }
        public string Alias1 { get; set; }
        public string Alias2 { get; set; }
        public string RoleList { get; set; }
        public int IsClientViewManager { get; set; }
        public string GroupList { get; set; }
        public string IPRestrictList { get; set; }
        public string ModuleList { get; set; }
        public int ClientViewGroupId { get; set; }
        public int LastClientId { get; set; }
        public string LastBranchNum { get; set; }
        public string LastJobState { get; set; }
        public string SignatureLine1 { get; set; }
        public string SignatureLine2 { get; set; }
        public string SignatureLine3 { get; set; }
        public string SignatureLine4 { get; set; }
        public DateTime LastLoginDate { get; set; }
        public string url { get; set; }
        public string nickname { get; set; }
        public string theme { get; set; }
        public string address { get; set; }
        public string cellno { get; set; }
        public string faxno { get; set; }
        public bool isnotauthenticated { get; set; }
        public bool isinactive { get; set; }
        public string LastMachineId { get; set; }
        public int LocationId { get; set; }
        public DateTime DOB { get; set; }
        public bool IsClientViewAdmin { get; set; }
        public string PhoneExt { get; set; }
        #endregion

        #region CRUD

        public static ADPortalUsers GetUserForId(int id)
        {
            try
            {
                ADPortalUsers Obj_ADPortalUsers = new ADPortalUsers();
                var myitem = new Portal_Users();
                ITable myitemItable = myitem;
                DBO.Provider.Read(id.ToString(), ref myitemItable);
                myitem = (Portal_Users)myitemItable;
                AutoMapper.Mapper.CreateMap<Portal_Users, ADPortalUsers>();
                return (AutoMapper.Mapper.Map<ADPortalUsers>(myitem));
            }
            catch(Exception ex)
            {
                return new Client_Management.ADPortalUsers();
            }
        }

        public static bool SaveUpdatePortalUser(ADPortalUsers ObjSource)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADPortalUsers, Portal_Users>();
                ITable tbluser;
                if (ObjSource.ID != 0)
                {
                    ADPortalUsers loadPortalUser = GetUserForId(Convert.ToInt32(ObjSource.ID));

                    loadPortalUser.ClientId = ObjSource.ClientId;
                    loadPortalUser.UserName = ObjSource.UserName;
                    loadPortalUser.LoginCode = ObjSource.LoginCode;
                    loadPortalUser.LoginPassword = ObjSource.LoginPassword;
                    loadPortalUser.PhoneNo = ObjSource.PhoneNo;
                    loadPortalUser.PhoneExt = ObjSource.PhoneExt;
                    loadPortalUser.Email = ObjSource.Email;
                    loadPortalUser.DOB = ObjSource.DOB;
                    loadPortalUser.ClientViewGroupId = ObjSource.ClientViewGroupId;
                    loadPortalUser.IsClientViewAdmin = ObjSource.IsClientViewAdmin;
                    loadPortalUser.IsClientViewManager = ObjSource.IsClientViewManager;
                    loadPortalUser.isinactive = ObjSource.isinactive;

                    Portal_Users objPortalUser = AutoMapper.Mapper.Map<Portal_Users>(loadPortalUser);
                    tbluser = objPortalUser;
                    DBO.Provider.Update(ref tbluser);
                }
                else
                {
                    Portal_Users objPortalUser = AutoMapper.Mapper.Map<Portal_Users>(ObjSource);
                    tbluser = objPortalUser;
                    DBO.Provider.Create(ref tbluser);

                }
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
        #endregion

    }
}
