﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using CRF.BLL.Clients;
    
namespace CRFView.Adapters
{
    public class ADClientLienInfo
    {
        #region properties
            public int Id { get; set; }
            public int ClientId { get; set; }
            public int ServiceTeamId { get; set; }
            public int SalesTeamId { get; set; }
            public int ProdTeamId { get; set; }
            public string ContactName { get; set; }
            public string ContactTitle { get; set; }
            public string ContactEmail { get; set; }
            public string ContactPhone { get; set; }
            public string ContactSalutation { get; set; }
            public DateTime ContactDOB { get; set; }
            public string ContactName2 { get; set; }
            public string ContactTitle2 { get; set; }
            public string ContactEmail2 { get; set; }
            public string ContactPhone2 { get; set; }
            public string ContactSalutation2 { get; set; }
            public DateTime ContactDOB2 { get; set; }
            public string InvoiceName { get; set; }
            public string InvoiceAttn { get; set; }
            public string InvoiceAddr1 { get; set; }
            public string InvoiceAddr2 { get; set; }
            public string InvoiceCity { get; set; }
            public string InvoiceState { get; set; }
            public string InvoiceZip { get; set; }
            public string InvoiceEmail { get; set; }
            public string InvoiceFax { get; set; }
            public bool IsActive { get; set; }
            public bool BranchBox { get; set; }
            public bool NoEmail { get; set; }
            public bool UseBranchNTO { get; set; }
            public bool UseBranchAck { get; set; }
            public bool GreenCard { get; set; }
            public bool CustSend { get; set; }
            public bool CustSendReg { get; set; }
            public bool AZCert { get; set; }
            public bool JointCK { get; set; }
            public bool TXApproved { get; set; }
            public bool TXCustSend { get; set; }
            public bool NOGCCall { get; set; }
            public bool NOCustCall { get; set; }
            public bool ActivateOIR { get; set; }
            public bool SendOIR { get; set; }
            public bool SendAck { get; set; }
            public bool SendNTO { get; set; }
            public bool SendNTOList { get; set; }
            public int OtherLglThreshold { get; set; }
            public string LaborType { get; set; }
            public string PhoneExt { get; set; }
            public string NoticePhoneNo { get; set; }
            public string LienContactName { get; set; }
            public string LienAddressLine1 { get; set; }
            public string LienAddressLine2 { get; set; }
            public string LienCity { get; set; }
            public string LienState { get; set; }
            public string LienZip { get; set; }
            public string LienContactTitle { get; set; }
            public string LienPhoneNo { get; set; }
            public string LienFax { get; set; }
            public string LienEmail { get; set; }
            public string LienSalutation { get; set; }
            public string LienSpecialInstruction { get; set; }
            public string LastUpdatedBy { get; set; }
            public DateTime LastUpdateDate { get; set; }
            public string InvoiceContactTitle { get; set; }
            public DateTime SetupDate { get; set; }
            public string ContactFax { get; set; }
            public string ContactFax2 { get; set; }
            public string Note { get; set; }
            public string ContactAddr1 { get; set; }
            public string ContactAddr2 { get; set; }
            public string ContactCity { get; set; }
            public string ContactState { get; set; }
            public string ContactZip { get; set; }
            public decimal LegalInterest { get; set; }
            public string InvoicePhoneNo { get; set; }
            public string ContactPhoneExt { get; set; }
            public string ContactPhone2Ext { get; set; }
            public bool TXPublicOwner { get; set; }
            public bool TXCopy { get; set; }
            public bool FLCertBox { get; set; }
            public bool KYNonStatBox { get; set; }
            public int TXPrelimGrp { get; set; }
            public string ReportEmail { get; set; }
            public bool TXCustSendReg { get; set; }
            public bool NoTitleSearch { get; set; }
            public bool JobAlertReqBox { get; set; }
            public bool IsFATExclude { get; set; }
            public bool SearchCustByRef { get; set; }
            public string NoAutoFaxState { get; set; }
            public string NoticePhoneNoWeb { get; set; }
            public bool EmailReports { get; set; }
            public string EmailReportsTo { get; set; }
            public bool NoDupValidation { get; set; }
            public bool IsCustomCoverLetter { get; set; }
            public bool IsIndividualNTO { get; set; }
            public bool IsCustomReportFile { get; set; }
            public string CustomReportFile { get; set; }
            public bool UseBranchSignature { get; set; }
            public bool SendAlert { get; set; }
            public bool EmailAck { get; set; }
            public string EmailAckTo { get; set; }
            public bool EmailLienAlerts { get; set; }
            public string EmailLienAlertsTo { get; set; }
            public bool IsNOCApproved { get; set; }
            public bool GCSendReg { get; set; }
            public bool DataEntryFee { get; set; }
            public string VAInfo { get; set; }
            public bool ReVerifyOwner { get; set; }
            public bool GreenCardTX { get; set; }
            public bool SuretySendReg { get; set; }
            public string GreencardThreshold { get; set; }
        #endregion

        #region Cutom Property
        public string DataEntryBox1 { get; set; }
        #endregion

        #region CRUD
        // Load Lien Info
        public ADClientLienInfo LoadLienInfo(int? id)
        {
            try
            {
                ClientLienInfo myitem = new ClientLienInfo();
                ITable myentityItable = (ITable)myitem;
                if (id != null)
                {
                    DBO.Provider.Read(id.ToString(), ref myentityItable);
                }
                AutoMapper.Mapper.CreateMap<ClientLienInfo, ADClientLienInfo>();
                return (AutoMapper.Mapper.Map<ADClientLienInfo>(myitem));
            }
            catch (Exception ex)
            {
                return (new ADClientLienInfo());
            }
        }

        // Update Lien Info
        public static bool UpdateLienInfo(ADClientLienInfo Obj_AdLien)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADClientLienInfo, ClientLienInfo>();
                ITable tblClientLienInfo;
                Obj_AdLien.GreencardThreshold = Obj_AdLien.GreencardThreshold.Replace(@"$", "").Replace(@",", "").ToString();
                if (Obj_AdLien.Id != 0)
                {
                    AutoMapper.Mapper.CreateMap<ADClientLienInfo,ClientLienInfo>();
                    ClientLienInfo Obj_ClientLienInfo = AutoMapper.Mapper.Map<ClientLienInfo>(Obj_AdLien);
                    tblClientLienInfo = Obj_ClientLienInfo;
                    DBO.Provider.Update(ref tblClientLienInfo);
                }
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public static ADClientLienInfo GetClientLienInfo(DataTable dt)
        {
            try
            { 
            List<ADClientLienInfo> ClientLienInfoList = (from DataRow dr in dt.Rows
                                         select new ADClientLienInfo()
                                         {
                                             Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                             ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
                                             ServiceTeamId = ((dr["ServiceTeamId"] != DBNull.Value ? Convert.ToInt32(dr["ServiceTeamId"]) : 0)),
                                             SalesTeamId = ((dr["SalesTeamId"] != DBNull.Value ? Convert.ToInt32(dr["SalesTeamId"]) : 0)),
                                             ProdTeamId = ((dr["ProdTeamId"] != DBNull.Value ? Convert.ToInt32(dr["ProdTeamId"]) : 0)),
                                             ContactName = (dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : ""),
                                             ContactTitle = (dr["ContactTitle"] != DBNull.Value ? dr["ContactTitle"].ToString() : ""),
                                             ContactEmail = (dr["ContactEmail"] != DBNull.Value ? dr["ContactEmail"].ToString() : ""),
                                             ContactPhone = (dr["ContactPhone"] != DBNull.Value ? dr["ContactPhone"].ToString() : ""),
                                             ContactSalutation = (dr["ContactSalutation"] != DBNull.Value ? dr["ContactSalutation"].ToString() : ""),
                                             ContactDOB = ((dr["ContactDOB"] != DBNull.Value ? Convert.ToDateTime(dr["ContactDOB"]) : DateTime.MinValue)),
                                             ContactName2 = (dr["ContactName2"] != DBNull.Value ? dr["ContactName2"].ToString() : ""),
                                             ContactTitle2 = (dr["ContactTitle2"] != DBNull.Value ? dr["ContactTitle2"].ToString() : ""),
                                             ContactEmail2 = (dr["ContactEmail2"] != DBNull.Value ? dr["ContactEmail2"].ToString() : ""),
                                             ContactPhone2 = (dr["ContactPhone2"] != DBNull.Value ? dr["ContactPhone2"].ToString() : ""),
                                             ContactSalutation2 = (dr["ContactSalutation2"] != DBNull.Value ? dr["ContactSalutation2"].ToString() : ""),
                                             ContactDOB2 = ((dr["ContactDOB2"] != DBNull.Value ? Convert.ToDateTime(dr["ContactDOB2"]) : DateTime.MinValue)),
                                             InvoiceName = (dr["InvoiceName"] != DBNull.Value ? dr["InvoiceName"].ToString() : ""),
                                             InvoiceAttn = (dr["InvoiceAttn"] != DBNull.Value ? dr["InvoiceAttn"].ToString() : ""),
                                             InvoiceAddr1 = (dr["InvoiceAddr1"] != DBNull.Value ? dr["InvoiceAddr1"].ToString() : ""),
                                             InvoiceAddr2 = (dr["InvoiceAddr2"] != DBNull.Value ? dr["InvoiceAddr2"].ToString() : ""),
                                             InvoiceCity = (dr["InvoiceCity"] != DBNull.Value ? dr["InvoiceCity"].ToString() : ""),
                                             InvoiceState = (dr["InvoiceState"] != DBNull.Value ? dr["InvoiceState"].ToString() : ""),
                                             InvoiceZip = (dr["InvoiceZip"] != DBNull.Value ? dr["InvoiceZip"].ToString() : ""),
                                             InvoiceEmail = (dr["InvoiceEmail"] != DBNull.Value ? dr["InvoiceEmail"].ToString() : ""),
                                             InvoiceFax = (dr["InvoiceFax"] != DBNull.Value ? dr["InvoiceFax"].ToString() : ""),
                                             IsActive = ((dr["IsActive"] != DBNull.Value ? Convert.ToBoolean(dr["IsActive"]) : false)),
                                             BranchBox = ((dr["BranchBox"] != DBNull.Value ? Convert.ToBoolean(dr["BranchBox"]) : false)),
                                             NoEmail = ((dr["NoEmail"] != DBNull.Value ? Convert.ToBoolean(dr["NoEmail"]) : false)),
                                             UseBranchNTO = ((dr["UseBranchNTO"] != DBNull.Value ? Convert.ToBoolean(dr["UseBranchNTO"]) : false)),
                                             UseBranchAck = ((dr["UseBranchAck"] != DBNull.Value ? Convert.ToBoolean(dr["UseBranchAck"]) : false)),
                                             GreenCard = ((dr["GreenCard"] != DBNull.Value ? Convert.ToBoolean(dr["GreenCard"]) : false)),
                                             CustSend = ((dr["CustSend"] != DBNull.Value ? Convert.ToBoolean(dr["CustSend"]) : false)),
                                             CustSendReg = ((dr["CustSendReg"] != DBNull.Value ? Convert.ToBoolean(dr["CustSendReg"]) : false)),
                                             AZCert = ((dr["AZCert"] != DBNull.Value ? Convert.ToBoolean(dr["AZCert"]) : false)),
                                             JointCK = ((dr["JointCK"] != DBNull.Value ? Convert.ToBoolean(dr["JointCK"]) : false)),
                                             TXApproved = ((dr["TXApproved"] != DBNull.Value ? Convert.ToBoolean(dr["TXApproved"]) : false)),
                                             TXCustSend = ((dr["TXCustSend"] != DBNull.Value ? Convert.ToBoolean(dr["TXCustSend"]) : false)),
                                             NOGCCall = ((dr["NOGCCall"] != DBNull.Value ? Convert.ToBoolean(dr["NOGCCall"]) : false)),
                                             NOCustCall = ((dr["NOCustCall"] != DBNull.Value ? Convert.ToBoolean(dr["NOCustCall"]) : false)),
                                             ActivateOIR = ((dr["ActivateOIR"] != DBNull.Value ? Convert.ToBoolean(dr["ActivateOIR"]) : false)),
                                             SendOIR = ((dr["SendOIR"] != DBNull.Value ? Convert.ToBoolean(dr["SendOIR"]) : false)),
                                             SendAck = ((dr["SendAck"] != DBNull.Value ? Convert.ToBoolean(dr["SendAck"]) : false)),
                                             SendNTO = ((dr["SendNTO"] != DBNull.Value ? Convert.ToBoolean(dr["SendNTO"]) : false)),
                                             SendNTOList = ((dr["SendNTOList"] != DBNull.Value ? Convert.ToBoolean(dr["SendNTOList"]) : false)),
                                             OtherLglThreshold = ((dr["OtherLglThreshold"] != DBNull.Value ? Convert.ToInt32(dr["OtherLglThreshold"]) : 0)),
                                             LaborType = (dr["LaborType"] != DBNull.Value ? dr["LaborType"].ToString() : ""),
                                             PhoneExt = (dr["PhoneExt"] != DBNull.Value ? dr["PhoneExt"].ToString() : ""),
                                             NoticePhoneNo = (dr["NoticePhoneNo"] != DBNull.Value ? dr["NoticePhoneNo"].ToString() : ""),
                                             LienContactName = (dr["LienContactName"] != DBNull.Value ? dr["LienContactName"].ToString() : ""),
                                             LienAddressLine1 = (dr["LienAddressLine1"] != DBNull.Value ? dr["LienAddressLine1"].ToString() : ""),
                                             LienAddressLine2 = (dr["LienAddressLine2"] != DBNull.Value ? dr["LienAddressLine2"].ToString() : ""),
                                             LienCity = (dr["LienCity"] != DBNull.Value ? dr["LienCity"].ToString() : ""),
                                             LienState = (dr["LienState"] != DBNull.Value ? dr["LienState"].ToString() : ""),
                                             LienZip = (dr["LienZip"] != DBNull.Value ? dr["LienZip"].ToString() : ""),
                                             LienContactTitle = (dr["LienContactTitle"] != DBNull.Value ? dr["LienContactTitle"].ToString() : ""),
                                             LienPhoneNo = (dr["LienPhoneNo"] != DBNull.Value ? dr["LienPhoneNo"].ToString() : ""),
                                             LienFax = (dr["LienFax"] != DBNull.Value ? dr["LienFax"].ToString() : ""),
                                             LienEmail = (dr["LienEmail"] != DBNull.Value ? dr["LienEmail"].ToString() : ""),
                                             LienSalutation = (dr["LienSalutation"] != DBNull.Value ? dr["LienSalutation"].ToString() : ""),
                                             LienSpecialInstruction = (dr["LienSpecialInstruction"] != DBNull.Value ? dr["LienSpecialInstruction"].ToString() : ""),
                                             LastUpdatedBy = (dr["LastUpdatedBy"] != DBNull.Value ? dr["LastUpdatedBy"].ToString() : ""),
                                             LastUpdateDate = ((dr["LastUpdateDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastUpdateDate"]) : DateTime.MinValue)),
                                             InvoiceContactTitle = (dr["InvoiceContactTitle"] != DBNull.Value ? dr["InvoiceContactTitle"].ToString() : ""),
                                             SetupDate = ((dr["SetupDate"] != DBNull.Value ? Convert.ToDateTime(dr["SetupDate"]) : DateTime.MinValue)),
                                             ContactFax = (dr["ContactFax"] != DBNull.Value ? dr["ContactFax"].ToString() : ""),
                                             ContactFax2 = (dr["ContactFax2"] != DBNull.Value ? dr["ContactFax2"].ToString() : ""),
                                             Note = (dr["Note"] != DBNull.Value ? dr["Note"].ToString() : ""),
                                             ContactAddr1 = (dr["ContactAddr1"] != DBNull.Value ? dr["ContactAddr1"].ToString() : ""),
                                             ContactAddr2 = (dr["ContactAddr2"] != DBNull.Value ? dr["ContactAddr2"].ToString() : ""),
                                             ContactCity = (dr["ContactCity"] != DBNull.Value ? dr["ContactCity"].ToString() : ""),
                                             ContactState = (dr["ContactState"] != DBNull.Value ? dr["ContactState"].ToString() : ""),
                                             ContactZip = (dr["ContactZip"] != DBNull.Value ? dr["ContactZip"].ToString() : ""),
                                             LegalInterest = ((dr["LegalInterest"] != DBNull.Value ? Convert.ToDecimal(dr["LegalInterest"]) : 0)),
                                             InvoicePhoneNo = (dr["InvoicePhoneNo"] != DBNull.Value ? dr["InvoicePhoneNo"].ToString() : ""),
                                             ContactPhoneExt = (dr["ContactPhoneExt"] != DBNull.Value ? dr["ContactPhoneExt"].ToString() : ""),
                                             ContactPhone2Ext = (dr["ContactPhone2Ext"] != DBNull.Value ? dr["ContactPhone2Ext"].ToString() : ""),
                                             TXPublicOwner = ((dr["TXPublicOwner"] != DBNull.Value ? Convert.ToBoolean(dr["TXPublicOwner"]) : false)),
                                             TXCopy = ((dr["TXCopy"] != DBNull.Value ? Convert.ToBoolean(dr["TXCopy"]) : false)),
                                             FLCertBox = ((dr["FLCertBox"] != DBNull.Value ? Convert.ToBoolean(dr["FLCertBox"]) : false)),
                                             KYNonStatBox = ((dr["KYNonStatBox"] != DBNull.Value ? Convert.ToBoolean(dr["KYNonStatBox"]) : false)),
                                             TXPrelimGrp = ((dr["TXPrelimGrp"] != DBNull.Value ? Convert.ToInt32(dr["TXPrelimGrp"]) : 0)),
                                             ReportEmail = (dr["ReportEmail"] != DBNull.Value ? dr["ReportEmail"].ToString() : ""),
                                             TXCustSendReg = ((dr["TXCustSendReg"] != DBNull.Value ? Convert.ToBoolean(dr["TXCustSendReg"]) : false)),
                                             NoTitleSearch = ((dr["NoTitleSearch"] != DBNull.Value ? Convert.ToBoolean(dr["NoTitleSearch"]) : false)),
                                             JobAlertReqBox = ((dr["JobAlertReqBox"] != DBNull.Value ? Convert.ToBoolean(dr["JobAlertReqBox"]) : false)),
                                             IsFATExclude = ((dr["IsFATExclude"] != DBNull.Value ? Convert.ToBoolean(dr["IsFATExclude"]) : false)),
                                             SearchCustByRef = ((dr["SearchCustByRef"] != DBNull.Value ? Convert.ToBoolean(dr["SearchCustByRef"]) : false)),
                                             NoAutoFaxState = (dr["NoAutoFaxState"] != DBNull.Value ? dr["NoAutoFaxState"].ToString() : ""),
                                             NoticePhoneNoWeb = (dr["NoticePhoneNoWeb"] != DBNull.Value ? dr["NoticePhoneNoWeb"].ToString() : ""),
                                             EmailReports = ((dr["EmailReports"] != DBNull.Value ? Convert.ToBoolean(dr["EmailReports"]) : false)),
                                             EmailReportsTo = (dr["EmailReportsTo"] != DBNull.Value ? dr["EmailReportsTo"].ToString() : ""),
                                             NoDupValidation = ((dr["NoDupValidation"] != DBNull.Value ? Convert.ToBoolean(dr["NoDupValidation"]) : false)),
                                             IsCustomCoverLetter = ((dr["IsCustomCoverLetter"] != DBNull.Value ? Convert.ToBoolean(dr["IsCustomCoverLetter"]) : false)),
                                             IsIndividualNTO = ((dr["IsIndividualNTO"] != DBNull.Value ? Convert.ToBoolean(dr["IsIndividualNTO"]) : false)),
                                             IsCustomReportFile = ((dr["IsCustomReportFile"] != DBNull.Value ? Convert.ToBoolean(dr["IsCustomReportFile"]) : false)),
                                             CustomReportFile = (dr["CustomReportFile"] != DBNull.Value ? dr["CustomReportFile"].ToString() : ""),
                                             UseBranchSignature = ((dr["UseBranchSignature"] != DBNull.Value ? Convert.ToBoolean(dr["UseBranchSignature"]) : false)),
                                             SendAlert = ((dr["SendAlert"] != DBNull.Value ? Convert.ToBoolean(dr["SendAlert"]) : false)),
                                             EmailAck = ((dr["EmailAck"] != DBNull.Value ? Convert.ToBoolean(dr["EmailAck"]) : false)),
                                             EmailAckTo = (dr["EmailAckTo"] != DBNull.Value ? dr["EmailAckTo"].ToString() : ""),
                                             EmailLienAlerts = ((dr["EmailLienAlerts"] != DBNull.Value ? Convert.ToBoolean(dr["EmailLienAlerts"]) : false)),
                                             EmailLienAlertsTo = (dr["EmailLienAlertsTo"] != DBNull.Value ? dr["EmailLienAlertsTo"].ToString() : ""),
                                             IsNOCApproved = ((dr["IsNOCApproved"] != DBNull.Value ? Convert.ToBoolean(dr["IsNOCApproved"]) : false)),
                                             GCSendReg = ((dr["GCSendReg"] != DBNull.Value ? Convert.ToBoolean(dr["GCSendReg"]) : false)),
                                             DataEntryFee = ((dr["DataEntryFee"] != DBNull.Value ? Convert.ToBoolean(dr["DataEntryFee"]) : false)),
                                             VAInfo = (dr["VAInfo"] != DBNull.Value ? dr["VAInfo"].ToString() : ""),
                                             ReVerifyOwner = ((dr["ReVerifyOwner"] != DBNull.Value ? Convert.ToBoolean(dr["ReVerifyOwner"]) : false)),
                                             GreenCardTX = ((dr["GreenCardTX"] != DBNull.Value ? Convert.ToBoolean(dr["GreenCardTX"]) : false)),
                                         }).ToList();
            return ClientLienInfoList.First();
            }
            catch (Exception ex)
            {
                return (new ADClientLienInfo());

            }
        }


        //Read Details from database
        public static ADClientLienInfo ReadClientLienInfoByClientId(int ClientId)
        {
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            ClientLienInfo myentity = new ClientLienInfo();
            myentity = CRF.BLL.Providers.LiensDataEntry.GetLienInfoforClient(user, ClientId);
            AutoMapper.Mapper.CreateMap<ClientLienInfo, ADClientLienInfo>();
            return (AutoMapper.Mapper.Map<ADClientLienInfo>(myentity));
        }
        #endregion
    }
}
