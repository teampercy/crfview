﻿using CRF.BLL;
using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADClientRapidPay
    {

        #region Properties

        public int RapidPayId { get; set; }
        public int ClientId { get; set; }
        public string ClientName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Email { get; set; }
        public string PhoneNo { get; set; }
        public string PhoneExt { get; set; }
        public string Mobile { get; set; }
        public string Fax { get; set; }
        public string Note { get; set; }
        public DateTime NextContactDate { get; set; }
        public string ContactName { get; set; }
        public string ContactTitle { get; set; }
        public string ContactSalutation { get; set; }
        public string ContactDOB { get; set; }
        public string LoginCode { get; set; }
        public string LoginPassWord { get; set; }
        public DateTime CreatedDate { get; set; }
        public DateTime SetupDate { get; set; }
        public bool IsCancelled { get; set; }
        public string ClientCode { get; set; }
        public string PasswordHint1 { get; set; }
        public string PasswordHint2 { get; set; }
        public DateTime OrderDate { get; set; }
        public bool IsReorder { get; set; }
        public string LaborType { get; set; }
        public string CouponCode { get; set; }
        public int JobUnits { get; set; }

        public Decimal UnitPrice { get; set; }
        public string CCType { get; set; }
        public string CCName { get; set; }
        public string CCCardNO { get; set; }
        public string CCExpDate { get; set; }
        public string CCSSIDNo { get; set; }
        public bool BillingSameAsMain { get; set; }
        public string BillingAddressLine1 { get; set; }
        public string BillingAddressLine2 { get; set; }
        public string BillingState { get; set; }
        public string BillingPostalCode { get; set; }
        public int TotUnitsPurchased { get; set; }

        public int TotUnitsUsed { get; set; }
        public int TotUnitsAdjusted { get; set; }
        public decimal TotPurchaseAmt { get; set; }
        public bool IsOrderPrinted { get; set; }
        public string BillingName { get; set; }
        public string BillingCity { get; set; }
        public string RefBy { get; set; }

        #endregion

        #region CRUD

        public static ADClientRapidPay ReadClientRapidPay(int RapidPayId)
        {
            ClientRapidPay myentity = new ClientRapidPay();
            ITable myentityItable = myentity;
            DBO.Provider.Read(RapidPayId.ToString(), ref myentityItable);
            myentity = (ClientRapidPay)myentityItable;
            AutoMapper.Mapper.CreateMap<ClientRapidPay, ADClientRapidPay>();
            return (AutoMapper.Mapper.Map<ADClientRapidPay>(myentity));
        }

        public static List<ADClientRapidPay> GetInitialList()
        {
            try
            {
                RapidPayClientsVM objRapidPayClientsVM = new RapidPayClientsVM();

                TableView EntityList;
                var Sp_Obj = new uspbo_CRM_RapidPayGetPendingList();
                EntityList = DBO.Provider.GetTableView(Sp_Obj);
                DataTable dt = EntityList.ToTable();
                List<ADClientRapidPay> ClientRapidPayList = (from DataRow dr in dt.Rows
                                                             select new ADClientRapidPay()
                                                             {

                                                                 RapidPayId = ((dr["RapidPayId"] != DBNull.Value ? Convert.ToInt32(dr["RapidPayId"]) : 0)),
                                                                 ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
                                                                 ClientName = ((dr["ClientName"] != DBNull.Value ? dr["ClientName"].ToString() : "")),
                                                                 AddressLine1 = ((dr["AddressLine1"] != DBNull.Value ? dr["AddressLine1"].ToString() : "")),
                                                                 AddressLine2 = ((dr["AddressLine2"] != DBNull.Value ? dr["AddressLine2"].ToString() : "")),

                                                                 City = ((dr["City"] != DBNull.Value ? dr["City"].ToString() : "")),
                                                                 State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),
                                                                 PostalCode = ((dr["PostalCode"] != DBNull.Value ? dr["PostalCode"].ToString() : "")),
                                                                 Email = ((dr["Email"] != DBNull.Value ? dr["Email"].ToString() : "")),
                                                                 PhoneNo = ((dr["PhoneNo"] != DBNull.Value ? dr["PhoneNo"].ToString() : "")),
                                                                 PhoneExt = ((dr["PhoneExt"] != DBNull.Value ? dr["PhoneExt"].ToString() : "")),
                                                                 Mobile = ((dr["Mobile"] != DBNull.Value ? dr["Mobile"].ToString() : "")),
                                                                 Fax = ((dr["Fax"] != DBNull.Value ? dr["Fax"].ToString() : "")),
                                                                 Note = ((dr["Note"] != DBNull.Value ? dr["Note"].ToString() : "")),

                                                                 NextContactDate = ((dr["NextContactDate"] != DBNull.Value ? Convert.ToDateTime(dr["NextContactDate"]) : DateTime.MinValue)),

                                                                 ContactName = ((dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : "")),
                                                                 ContactTitle = ((dr["ContactTitle"] != DBNull.Value ? dr["ContactTitle"].ToString() : "")),
                                                                 ContactSalutation = ((dr["ContactSalutation"] != DBNull.Value ? dr["ContactSalutation"].ToString() : "")),
                                                                 ContactDOB = ((dr["ContactDOB"] != DBNull.Value ? dr["ContactDOB"].ToString() : "")),
                                                                 LoginCode = ((dr["LoginCode"] != DBNull.Value ? dr["LoginCode"].ToString() : "")),
                                                                 LoginPassWord = ((dr["LoginPassWord"] != DBNull.Value ? dr["LoginPassWord"].ToString() : "")),

                                                                 CreatedDate = ((dr["CreatedDate"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedDate"]) : DateTime.MinValue)),
                                                                 SetupDate = ((dr["SetupDate"] != DBNull.Value ? Convert.ToDateTime(dr["SetupDate"]) : DateTime.MinValue)),

                                                                 IsCancelled = ((dr["IsCancelled"] != DBNull.Value ? Convert.ToBoolean(dr["IsCancelled"]) : false)),

                                                                 ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                                                 PasswordHint1 = ((dr["PasswordHint1"] != DBNull.Value ? dr["PasswordHint1"].ToString() : "")),
                                                                 PasswordHint2 = ((dr["PasswordHint2"] != DBNull.Value ? dr["PasswordHint2"].ToString() : "")),

                                                                 OrderDate = ((dr["OrderDate"] != DBNull.Value ? Convert.ToDateTime(dr["OrderDate"]) : DateTime.MinValue)),

                                                                 IsReorder = ((dr["IsReorder"] != DBNull.Value ? Convert.ToBoolean(dr["IsReorder"]) : false)),

                                                                 LaborType = ((dr["LaborType"] != DBNull.Value ? dr["LaborType"].ToString() : "")),
                                                                 CouponCode = ((dr["CouponCode"] != DBNull.Value ? dr["CouponCode"].ToString() : "")),

                                                                 JobUnits = ((dr["JobUnits"] != DBNull.Value ? Convert.ToInt32(dr["JobUnits"]) : 0)),
                                                                 UnitPrice = Convert.ToDecimal(dr["UnitPrice"]),

                                                                 CCType = ((dr["CCType"] != DBNull.Value ? dr["CCType"].ToString() : "")),
                                                                 CCName = ((dr["CCName"] != DBNull.Value ? dr["CCName"].ToString() : "")),
                                                                 CCCardNO = ((dr["CCCardNO"] != DBNull.Value ? dr["CCCardNO"].ToString() : "")),
                                                                 CCExpDate = ((dr["CCExpDate"] != DBNull.Value ? dr["CCExpDate"].ToString() : "")),
                                                                 CCSSIDNo = ((dr["CCSSIDNo"] != DBNull.Value ? dr["CCSSIDNo"].ToString() : "")),

                                                                 BillingSameAsMain = ((dr["BillingSameAsMain"] != DBNull.Value ? Convert.ToBoolean(dr["BillingSameAsMain"]) : false)),

                                                                 BillingAddressLine1 = ((dr["BillingAddressLine1"] != DBNull.Value ? dr["BillingAddressLine1"].ToString() : "")),
                                                                 BillingAddressLine2 = ((dr["BillingAddressLine2"] != DBNull.Value ? dr["BillingAddressLine2"].ToString() : "")),
                                                                 BillingState = ((dr["BillingState"] != DBNull.Value ? dr["BillingState"].ToString() : "")),
                                                                 BillingPostalCode = ((dr["BillingPostalCode"] != DBNull.Value ? dr["BillingPostalCode"].ToString() : "")),

                                                                 TotUnitsPurchased = ((dr["TotUnitsPurchased"] != DBNull.Value ? Convert.ToInt32(dr["TotUnitsPurchased"]) : 0)),
                                                                 TotUnitsUsed = ((dr["TotUnitsUsed"] != DBNull.Value ? Convert.ToInt32(dr["TotUnitsUsed"]) : 0)),
                                                                 TotUnitsAdjusted = ((dr["TotUnitsAdjusted"] != DBNull.Value ? Convert.ToInt32(dr["TotUnitsAdjusted"]) : 0)),

                                                                 TotPurchaseAmt = ((dr["TotPurchaseAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotPurchaseAmt"]) : 0)),

                                                                 //TotPurchaseAmt = Convert.ToDecimal(dr["TotPurchaseAmt"]),

                                                                 IsOrderPrinted = ((dr["IsOrderPrinted"] != DBNull.Value ? Convert.ToBoolean(dr["IsOrderPrinted"]) : false)),

                                                                 BillingName = ((dr["BillingName"] != DBNull.Value ? dr["BillingName"].ToString() : "")),
                                                                 BillingCity = ((dr["BillingCity"] != DBNull.Value ? dr["BillingCity"].ToString() : "")),
                                                                 RefBy = ((dr["RefBy"] != DBNull.Value ? dr["RefBy"].ToString() : "")),





                                                             }).ToList();
                return ClientRapidPayList;
            }
            catch (Exception ex)
            {
                return (new List<ADClientRapidPay>());
            }
        }


        public static List<ADClientRapidPay> GetInitialFilterList(RapidPayClientsVM ObjRapidPayClientsVM)
        {
            try
            {
                if (ObjRapidPayClientsVM.RbtButton == "ByNotProcessed")
                {
                    ObjRapidPayClientsVM.ByNotProcessed = false;
                }
                else
                {
                    ObjRapidPayClientsVM.ByAllOrders = true; 
                }

                TableView EntityList;
                var Sp_Obj = new uspbo_CRM_RapidPayGetPendingList();
                Sp_Obj.IsDateRange = false;
                Sp_Obj.IsOrderPrinted = false;

                if (ObjRapidPayClientsVM.ByAllOrders == true)
                {
                    Sp_Obj.IsOrderPrinted = true;
                }
                if (ObjRapidPayClientsVM.chkByRequestDate == true)
                {
                    Sp_Obj.IsDateRange = true;
                    Sp_Obj.FromDate = ObjRapidPayClientsVM.DateTimePicker1;
                    Sp_Obj.ThruDate = ObjRapidPayClientsVM.DateTimePicker2;
                }

                EntityList = DBO.Provider.GetTableView(Sp_Obj);
                DataTable dt = EntityList.ToTable();
                List<ADClientRapidPay> ClientRapidPayList = (from DataRow dr in dt.Rows
                                                             select new ADClientRapidPay()
                                                             {

                                                                 RapidPayId = ((dr["RapidPayId"] != DBNull.Value ? Convert.ToInt32(dr["RapidPayId"]) : 0)),
                                                                 ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
                                                                 ClientName = ((dr["ClientName"] != DBNull.Value ? dr["ClientName"].ToString() : "")),
                                                                 AddressLine1 = ((dr["AddressLine1"] != DBNull.Value ? dr["AddressLine1"].ToString() : "")),
                                                                 AddressLine2 = ((dr["AddressLine2"] != DBNull.Value ? dr["AddressLine2"].ToString() : "")),

                                                                 City = ((dr["City"] != DBNull.Value ? dr["City"].ToString() : "")),
                                                                 State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),
                                                                 PostalCode = ((dr["PostalCode"] != DBNull.Value ? dr["PostalCode"].ToString() : "")),
                                                                 Email = ((dr["Email"] != DBNull.Value ? dr["Email"].ToString() : "")),
                                                                 PhoneNo = ((dr["PhoneNo"] != DBNull.Value ? dr["PhoneNo"].ToString() : "")),
                                                                 PhoneExt = ((dr["PhoneExt"] != DBNull.Value ? dr["PhoneExt"].ToString() : "")),
                                                                 Mobile = ((dr["Mobile"] != DBNull.Value ? dr["Mobile"].ToString() : "")),
                                                                 Fax = ((dr["Fax"] != DBNull.Value ? dr["Fax"].ToString() : "")),
                                                                 Note = ((dr["Note"] != DBNull.Value ? dr["Note"].ToString() : "")),

                                                                 NextContactDate = ((dr["NextContactDate"] != DBNull.Value ? Convert.ToDateTime(dr["NextContactDate"]) : DateTime.MinValue)),

                                                                 ContactName = ((dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : "")),
                                                                 ContactTitle = ((dr["ContactTitle"] != DBNull.Value ? dr["ContactTitle"].ToString() : "")),
                                                                 ContactSalutation = ((dr["ContactSalutation"] != DBNull.Value ? dr["ContactSalutation"].ToString() : "")),
                                                                 ContactDOB = ((dr["ContactDOB"] != DBNull.Value ? dr["ContactDOB"].ToString() : "")),
                                                                 LoginCode = ((dr["LoginCode"] != DBNull.Value ? dr["LoginCode"].ToString() : "")),
                                                                 LoginPassWord = ((dr["LoginPassWord"] != DBNull.Value ? dr["LoginPassWord"].ToString() : "")),

                                                                 CreatedDate = ((dr["CreatedDate"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedDate"]) : DateTime.MinValue)),
                                                                 SetupDate = ((dr["SetupDate"] != DBNull.Value ? Convert.ToDateTime(dr["SetupDate"]) : DateTime.MinValue)),

                                                                 IsCancelled = ((dr["IsCancelled"] != DBNull.Value ? Convert.ToBoolean(dr["IsCancelled"]) : false)),

                                                                 ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                                                 PasswordHint1 = ((dr["PasswordHint1"] != DBNull.Value ? dr["PasswordHint1"].ToString() : "")),
                                                                 PasswordHint2 = ((dr["PasswordHint2"] != DBNull.Value ? dr["PasswordHint2"].ToString() : "")),

                                                                 OrderDate = ((dr["OrderDate"] != DBNull.Value ? Convert.ToDateTime(dr["OrderDate"]) : DateTime.MinValue)),

                                                                 IsReorder = ((dr["IsReorder"] != DBNull.Value ? Convert.ToBoolean(dr["IsReorder"]) : false)),

                                                                 LaborType = ((dr["LaborType"] != DBNull.Value ? dr["LaborType"].ToString() : "")),
                                                                 CouponCode = ((dr["CouponCode"] != DBNull.Value ? dr["CouponCode"].ToString() : "")),

                                                                 JobUnits = ((dr["JobUnits"] != DBNull.Value ? Convert.ToInt32(dr["JobUnits"]) : 0)),
                                                                 UnitPrice = Convert.ToDecimal(dr["UnitPrice"]),

                                                                 CCType = ((dr["CCType"] != DBNull.Value ? dr["CCType"].ToString() : "")),
                                                                 CCName = ((dr["CCName"] != DBNull.Value ? dr["CCName"].ToString() : "")),
                                                                 CCCardNO = ((dr["CCCardNO"] != DBNull.Value ? dr["CCCardNO"].ToString() : "")),
                                                                 CCExpDate = ((dr["CCExpDate"] != DBNull.Value ? dr["CCExpDate"].ToString() : "")),
                                                                 CCSSIDNo = ((dr["CCSSIDNo"] != DBNull.Value ? dr["CCSSIDNo"].ToString() : "")),

                                                                 BillingSameAsMain = ((dr["BillingSameAsMain"] != DBNull.Value ? Convert.ToBoolean(dr["BillingSameAsMain"]) : false)),

                                                                 BillingAddressLine1 = ((dr["BillingAddressLine1"] != DBNull.Value ? dr["BillingAddressLine1"].ToString() : "")),
                                                                 BillingAddressLine2 = ((dr["BillingAddressLine2"] != DBNull.Value ? dr["BillingAddressLine2"].ToString() : "")),
                                                                 BillingState = ((dr["BillingState"] != DBNull.Value ? dr["BillingState"].ToString() : "")),
                                                                 BillingPostalCode = ((dr["BillingPostalCode"] != DBNull.Value ? dr["BillingPostalCode"].ToString() : "")),

                                                                 TotUnitsPurchased = ((dr["TotUnitsPurchased"] != DBNull.Value ? Convert.ToInt32(dr["TotUnitsPurchased"]) : 0)),
                                                                 TotUnitsUsed = ((dr["TotUnitsUsed"] != DBNull.Value ? Convert.ToInt32(dr["TotUnitsUsed"]) : 0)),
                                                                 TotUnitsAdjusted = ((dr["TotUnitsAdjusted"] != DBNull.Value ? Convert.ToInt32(dr["TotUnitsAdjusted"]) : 0)),

                                                                 TotPurchaseAmt = ((dr["TotPurchaseAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotPurchaseAmt"]) : 0)),

                                                                 //TotPurchaseAmt = Convert.ToDecimal(dr["TotPurchaseAmt"]),

                                                                 IsOrderPrinted = ((dr["IsOrderPrinted"] != DBNull.Value ? Convert.ToBoolean(dr["IsOrderPrinted"]) : false)),

                                                                 BillingName = ((dr["BillingName"] != DBNull.Value ? dr["BillingName"].ToString() : "")),
                                                                 BillingCity = ((dr["BillingCity"] != DBNull.Value ? dr["BillingCity"].ToString() : "")),
                                                                 RefBy = ((dr["RefBy"] != DBNull.Value ? dr["RefBy"].ToString() : "")),





                                                             }).ToList();
                return ClientRapidPayList;
            }
            catch (Exception ex)
            {
                return (new List<ADClientRapidPay>());
            }
        }


        public int AddRapidPayClients(ADClientRapidPay updatedClientRapidPay, RapidPayClientsVM ObjRapidPayClientsVM)
        {
            try
            {
              

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                var myds = new DataSet();

               

              
                updatedClientRapidPay.ClientId = Convert.ToInt32( ObjRapidPayClientsVM.ddlAllClientId);
                string sql = "Select ClientCode from Client Where ClientId = " + ObjRapidPayClientsVM.ddlAllClientId;
                myds = DBO.Provider.GetDataSet(sql);
                DataTable dt = myds.Tables[0];
                updatedClientRapidPay.ClientCode = dt.Rows[0]["ClientCode"].ToString();
                //updatedClientRapidPay.ClientCode = sql;
                if (ObjRapidPayClientsVM.RbtButton == "rbtMasterCard")
                {
                    updatedClientRapidPay.CCType = "MasterCard";
                }
                else
                {
                    updatedClientRapidPay.CCType = "Visa";
                }


                //decimal unit = Convert.ToDecimal(ObjRapidPayClientsVM.UnitPrice.Trim('$'));
                //decimal UnitPrice = Convert.ToDecimal(unit);
                updatedClientRapidPay.JobUnits = Convert.ToInt32(ObjRapidPayClientsVM.JobUnits);

                updatedClientRapidPay.UnitPrice = Convert.ToDecimal(ObjRapidPayClientsVM.UnitPrice.Trim('$'));

                updatedClientRapidPay.TotPurchaseAmt = Convert.ToDecimal(ObjRapidPayClientsVM.TotPurchaseAmt.Trim('$'));

                AutoMapper.Mapper.CreateMap<ADClientRapidPay, ClientRapidPay>();
                ITable tblClientRapidPay;

                ClientRapidPay objClientRapidPay = AutoMapper.Mapper.Map<ClientRapidPay>(updatedClientRapidPay);
                tblClientRapidPay = (ITable)objClientRapidPay;
                DBO.Provider.Create(ref tblClientRapidPay);
                return 1;

            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public int UpdateRapidPayClients(RapidPayClientsVM objRapidPayClientsVM)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADClientRapidPay, ClientRapidPay>();
                ITable tblClientRapidPay;

            

                ADClientRapidPay objADClientRapidPaybOriginal = ReadClientRapidPay(objRapidPayClientsVM.objADClientRapidPay.RapidPayId);
                //ADClientRapidPay objADClientRapidPaybOriginal = objRapidPayClientsVM.objADClientRapidPay;


                objADClientRapidPaybOriginal.OrderDate = DateTime.Now;

                objADClientRapidPaybOriginal.ClientId = objADClientRapidPaybOriginal.ClientId;
                objADClientRapidPaybOriginal.ClientName = objRapidPayClientsVM.objADClientRapidPay.ClientName;
                objADClientRapidPaybOriginal.AddressLine1 = objRapidPayClientsVM.objADClientRapidPay.AddressLine1;
                objADClientRapidPaybOriginal.AddressLine2 = objRapidPayClientsVM.objADClientRapidPay.AddressLine2;
                objADClientRapidPaybOriginal.City = objRapidPayClientsVM.objADClientRapidPay.City;
                objADClientRapidPaybOriginal.State = objRapidPayClientsVM.objADClientRapidPay.State;
                objADClientRapidPaybOriginal.PostalCode = objRapidPayClientsVM.objADClientRapidPay.PostalCode;
                objADClientRapidPaybOriginal.Email = objRapidPayClientsVM.objADClientRapidPay.Email;
                objADClientRapidPaybOriginal.PhoneNo = objRapidPayClientsVM.objADClientRapidPay.PhoneNo;
                objADClientRapidPaybOriginal.PhoneExt = objRapidPayClientsVM.objADClientRapidPay.PhoneExt;
                objADClientRapidPaybOriginal.Mobile = objRapidPayClientsVM.objADClientRapidPay.Mobile;
                objADClientRapidPaybOriginal.Fax = objRapidPayClientsVM.objADClientRapidPay.Fax;
                objADClientRapidPaybOriginal.Note = objRapidPayClientsVM.objADClientRapidPay.Note;
                objADClientRapidPaybOriginal.NextContactDate = objRapidPayClientsVM.objADClientRapidPay.NextContactDate;
                objADClientRapidPaybOriginal.ContactName = objRapidPayClientsVM.objADClientRapidPay.ContactName;
                objADClientRapidPaybOriginal.ContactTitle = objRapidPayClientsVM.objADClientRapidPay.ContactTitle;
                objADClientRapidPaybOriginal.ContactSalutation = objRapidPayClientsVM.objADClientRapidPay.ContactSalutation;
                objADClientRapidPaybOriginal.ContactDOB = objRapidPayClientsVM.objADClientRapidPay.ContactDOB;
                objADClientRapidPaybOriginal.LoginCode = objRapidPayClientsVM.objADClientRapidPay.LoginCode;
                objADClientRapidPaybOriginal.LoginPassWord = objRapidPayClientsVM.objADClientRapidPay.LoginPassWord;
                objADClientRapidPaybOriginal.CreatedDate = objRapidPayClientsVM.objADClientRapidPay.CreatedDate;
                objADClientRapidPaybOriginal.SetupDate = objRapidPayClientsVM.objADClientRapidPay.SetupDate;
                objADClientRapidPaybOriginal.IsCancelled = objRapidPayClientsVM.objADClientRapidPay.IsCancelled;
                objADClientRapidPaybOriginal.ClientCode = objRapidPayClientsVM.objADClientRapidPay.ClientCode;
                objADClientRapidPaybOriginal.PasswordHint1 = objRapidPayClientsVM.objADClientRapidPay.PasswordHint1;
                objADClientRapidPaybOriginal.PasswordHint2 = objRapidPayClientsVM.objADClientRapidPay.PasswordHint2;
                objADClientRapidPaybOriginal.OrderDate = objRapidPayClientsVM.objADClientRapidPay.OrderDate;
                objADClientRapidPaybOriginal.IsReorder = objRapidPayClientsVM.objADClientRapidPay.IsReorder;
                objADClientRapidPaybOriginal.LaborType = objRapidPayClientsVM.objADClientRapidPay.LaborType;
                objADClientRapidPaybOriginal.CouponCode = objRapidPayClientsVM.objADClientRapidPay.CouponCode;
                objADClientRapidPaybOriginal.JobUnits = objRapidPayClientsVM.JobUnits;
                objADClientRapidPaybOriginal.UnitPrice = Convert.ToDecimal(objRapidPayClientsVM.UnitPrice.Trim('$'));
                objADClientRapidPaybOriginal.CCType = objRapidPayClientsVM.objADClientRapidPay.CCType;
                objADClientRapidPaybOriginal.CCName = objRapidPayClientsVM.objADClientRapidPay.CCName;
                objADClientRapidPaybOriginal.CCCardNO = objRapidPayClientsVM.objADClientRapidPay.CCCardNO;
                objADClientRapidPaybOriginal.CCExpDate = objRapidPayClientsVM.objADClientRapidPay.CCExpDate;
                objADClientRapidPaybOriginal.CCSSIDNo = objRapidPayClientsVM.objADClientRapidPay.CCSSIDNo;
                objADClientRapidPaybOriginal.BillingSameAsMain = objRapidPayClientsVM.objADClientRapidPay.BillingSameAsMain;
                objADClientRapidPaybOriginal.BillingAddressLine1 = objRapidPayClientsVM.objADClientRapidPay.BillingAddressLine1;
                objADClientRapidPaybOriginal.BillingAddressLine2 = objRapidPayClientsVM.objADClientRapidPay.BillingAddressLine2;
                objADClientRapidPaybOriginal.BillingState = objRapidPayClientsVM.objADClientRapidPay.BillingState;
                objADClientRapidPaybOriginal.BillingPostalCode = objRapidPayClientsVM.objADClientRapidPay.BillingPostalCode;
                objADClientRapidPaybOriginal.TotUnitsPurchased = objRapidPayClientsVM.objADClientRapidPay.TotUnitsPurchased;
                objADClientRapidPaybOriginal.TotUnitsUsed = objRapidPayClientsVM.objADClientRapidPay.TotUnitsUsed;
                objADClientRapidPaybOriginal.TotUnitsAdjusted = objRapidPayClientsVM.objADClientRapidPay.TotUnitsAdjusted;
                objADClientRapidPaybOriginal.TotPurchaseAmt = Convert.ToDecimal(objRapidPayClientsVM.TotPurchaseAmt.Trim('$'));
                objADClientRapidPaybOriginal.IsOrderPrinted = objRapidPayClientsVM.objADClientRapidPay.IsOrderPrinted;
                objADClientRapidPaybOriginal.BillingName = objRapidPayClientsVM.objADClientRapidPay.BillingName;
                objADClientRapidPaybOriginal.BillingCity = objRapidPayClientsVM.objADClientRapidPay.BillingCity;
                objADClientRapidPaybOriginal.RefBy = objRapidPayClientsVM.objADClientRapidPay.RefBy;

                ClientRapidPay objClientRapidPay = AutoMapper.Mapper.Map<ClientRapidPay>(objADClientRapidPaybOriginal);
                tblClientRapidPay = (ITable)objClientRapidPay;
                DBO.Provider.Update(ref tblClientRapidPay);
                return 1;

            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static DataTable LoadInfo(int RapidPayId)
        {
            try
            {
                string sql = "Select * from ClientRapidPay where RapidPayId = " + RapidPayId;
                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }



        //public string PrintBatchReport(RapidPayClientsVM objRapidPayClientsVM)
        //{
        //    string pdfPath = null;
        //    try
        //    {
        //        CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

        //        var myds = new DataSet();
        //        var Sp_OBJ = new cview_Reports_GetAccountsInBatches();

        
        //CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;
        //if (objBatchReportOptionsVM.RbtPrintOption == "Send To PDF")
        //{
        //    mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
        //}

        //TableView myview = DBO.Provider.GetTableView(Sp_OBJ);
           //TableView myview = CRF.BLL.Providers.CollectionsDataEntry.GetNewAccountList(user, Sp_OBJ);



        //        pdfPath = CRF.BLL.Providers.CollectionsDataEntry.PrintBatchList(user, mymode, myview);
        //    }

        //    catch (Exception ex)
        //    {
        //        return "";
        //    }
        //    return pdfPath;
        //}


        //Print RapidPay Clients
        public int PrintRapidPayClients(string RapidPayId)
        {
            string pdfPath = null;
            try
            {
                AutoMapper.Mapper.CreateMap<ADClientRapidPay, ClientRapidPay>();
                ITable tblClientRapidPay;
                ADClientRapidPay objADClientRapidPaybOriginal = ReadClientRapidPay(Convert.ToInt32(RapidPayId));

                //objADClientRapidPaybOriginal.IsOrderPrinted = true;
                //objADClientRapidPaybOriginal.OrderDate = DateTime.Now;
               
                ClientRapidPay objClientRapidPay = AutoMapper.Mapper.Map<ClientRapidPay>(objADClientRapidPaybOriginal);
                tblClientRapidPay = (ITable)objClientRapidPay;
                DBO.Provider.Update(ref tblClientRapidPay);

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                var myds = new DataSet();

                var sql = "Select * from ClientRapidPay where RapidPayId = " + RapidPayId;

                myds = DBO.Provider.GetDataSet(sql);
                
                pdfPath = CRF.BLL.COMMON.Reporting.PrintDataReport(user, "RapidPayOrder.DRL", "RapidPayOrder", myds, CRF.BLL.COMMON.PrintMode.PrintToPDF, objADClientRapidPaybOriginal.ClientCode, "", "");

            //   pdfPath = Globals.RenderC1Report(user, myds.Tables[0], myreportfilename, myreportname, mymode, "", sFromDate, sThruDate);

                //CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;
                //mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;

                //TableView myview = DBO.Provider.GetTableView(sql);


                return 1;

            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        // check if client code already exists while insertung a new client. 
        public bool IsDuplicateClient(string Clientcode)
        {
            string sql = "Select ClientCode from Client Where ClientCode = '" + Clientcode + "'";
            System.Data.DataSet ds = DBO.Provider.GetDataSet(sql);
            if (ds.Tables[0].Rows.Count > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }


        #endregion

    }
}
