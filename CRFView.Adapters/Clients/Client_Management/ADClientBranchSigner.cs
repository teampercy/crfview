﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using CRF.BLL.Clients;
using CRFView.Adapters.Clients.Client_Management;
using System.IO;

namespace CRFView.Adapters.Clients.Client_Management
{
    public class ADClientBranchSigner
    {
        #region Properties
        public int Id { get; set; }
        public int ClientId { get; set; }
        public byte[] Signature { get; set; }
        public string Signer { get; set; }
        #endregion

        #region CRUD
        public static ADClientBranchSigner GetClientBranchSigner(string ClientId)
        {
            try
            {
                ADClientBranchSigner Obj_BranchSigner = new ADClientBranchSigner();
                ClientBranchSigner myitem = new ClientBranchSigner();
                string MYSQL = "Select Top 1 * from clientbranchsigner where clientid = " + ClientId;
                System.Data.DataSet ds = DBO.Provider.GetDataSet(MYSQL);
                DataTable dt = ds.Tables[0];
               
                if(dt.Rows.Count>=1)
                {
                    string Id = dt.Rows[0]["Id"].ToString();
                    ClientBranchSigner myentity = new ClientBranchSigner();
                    ITable myentityItable = myentity;
                    DBO.Provider.Read(Id, ref myentityItable);
                    myentity = (ClientBranchSigner)myentityItable;
                    AutoMapper.Mapper.CreateMap<ClientBranchSigner, ADClientBranchSigner>();
                    Obj_BranchSigner = AutoMapper.Mapper.Map<ADClientBranchSigner>(myentity);
                }
                else
                {

                }
                return Obj_BranchSigner;
            }
            catch (Exception ex)
            {
                return new ADClientBranchSigner();
            }
        }

        public static bool CreateUpdateSignature(ADClientBranchSigner source , string  ContactName)
        {
            try
            {
                ClientBranchSigner myitem = new ClientBranchSigner();
                string MYSQL = "Select Top 1 * from clientbranchsigner where clientid = " + source.ClientId;
                System.Data.DataSet ds = DBO.Provider.GetDataSet(MYSQL);
                DataTable dt = ds.Tables[0];
                //string ClientId  = dt.Rows[0]["ClientId"].ToString();
                ITable Isigner;
                if (dt.Rows.Count >= 1)
                {
                    AutoMapper.Mapper.CreateMap<ADClientBranchSigner, ClientBranchSigner>();
                    var IDConverted = source.ClientId.ToString();
                    ADClientBranchSigner loadClientBranch = GetClientBranchSigner(IDConverted);

                    loadClientBranch.Signature = source.Signature;

                    ClientBranchSigner objBranchSigner = AutoMapper.Mapper.Map<ClientBranchSigner>(loadClientBranch);
                    Isigner = objBranchSigner;
                    DBO.Provider.Update(ref Isigner);
                }
                else
                {
                    myitem.ClientId = source.ClientId;
                    myitem.Signature = source.Signature;
                    myitem.Signer = ContactName;
                    Isigner = myitem;
                    DBO.Provider.Create(ref Isigner);
                }
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }
        #endregion

    }
}
