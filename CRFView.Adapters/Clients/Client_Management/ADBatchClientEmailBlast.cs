﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using System.Collections.Generic;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using HDS.DAL.Providers;
using System;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections.ObjectModel;

namespace CRFView.Adapters
{
    public class ADBatchClientEmailBlast
    {
        #region Properties
        public int BatchClientEmailBlastId { get; set; }
        public int BatchId { get; set; }
        public string ClientId { get; set; }
        public string ClientCode { get; set; }
        public string Type { get; set; }
        public string ContactName { get; set; }
        public string ContactTitle { get; set; }
        public string PhoneNo { get; set; }
        public string Email { get; set; }
        public DateTime DateSent { get; set; }
        public int Status { get; set; }
        public bool Skip { get; set; }
        #endregion




        #region CRUD
        public static DataTable GetEmailBlastsDt(EmailBlastsVM objEmailBlastsVM)
        {
       
        try
        {
            bool IsRecreate = true;

            var sp_obj = new uspbo_CRM_GetEmailList();
            sp_obj.TxContacts = objEmailBlastsVM.chkTexas;
            sp_obj.LienContacts = objEmailBlastsVM.chkLiens;
            sp_obj.CollectionContacts = objEmailBlastsVM.chkCollection;
            sp_obj.OtherContacts = objEmailBlastsVM.chkOther;
            sp_obj.LienViewUsers = objEmailBlastsVM.chkLVUsers;
            sp_obj.CollectViewUsers = objEmailBlastsVM.chkCVUsers;
            sp_obj.IsRecreate = IsRecreate;

            var EntityList = DBO.Provider.GetTableView(sp_obj);
            DataTable dt = EntityList.ToTable();
            return (dt);


        }
        catch (Exception ex)
        {
            return (new DataTable());
        }
      }


        public static List<ADBatchClientEmailBlast> GetEmailBlastsList(DataTable dt)
        {
            List<ADBatchClientEmailBlast> List = (from DataRow dr in dt.Rows
                                      select new ADBatchClientEmailBlast()

                                      {
                                          BatchClientEmailBlastId = ((dr["BatchClientEmailBlastId"] != DBNull.Value ? Convert.ToInt32(dr["BatchClientEmailBlastId"]) : Int32.MinValue)),
                                          BatchId = ((dr["BatchId"] != DBNull.Value ? Convert.ToInt32(dr["BatchId"]) : Int32.MinValue)),
                                          ClientId = ((dr["ClientId"] != DBNull.Value ? dr["ClientId"].ToString() : "")),
                                          ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                          Type = ((dr["Type"] != DBNull.Value ? dr["Type"].ToString() : "")),
                                          ContactName = ((dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : "")),
                                          ContactTitle = ((dr["ContactTitle"] != DBNull.Value ? dr["ContactTitle"].ToString() : "")),
                                          PhoneNo = ((dr["PhoneNo"] != DBNull.Value ? dr["PhoneNo"].ToString() : "")),
                                          Email = ((dr["Email"] != DBNull.Value ? dr["Email"].ToString() : "")),
                                          DateSent = ((dr["DateSent"] != DBNull.Value ? Convert.ToDateTime(dr["DateSent"]) : DateTime.MinValue)),
                                          Status = ((dr["Status"] != DBNull.Value ? Convert.ToInt32(dr["Status"]) : Int32.MinValue)),
                                          Skip = ((dr["Skip"] != DBNull.Value ? Convert.ToBoolean(dr["Skip"]) : false)),
                                          
                                      }).ToList();

            return List;
        }

        public static bool SendEmailBlastDetails(EmailBlastsVM objEmailBlastsVM, List<ADBatchClientEmailBlast> GetEmailBlastsListView)
        {

            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                Agency myagency = new Agency();
                BatchClientEmailBlast myrecipient = new BatchClientEmailBlast();

                ITable IAcc = myagency;
                DBO.Provider.Read(Convert.ToString(1), ref IAcc);
                myagency = (Agency)IAcc;

                int myidx = 0;
                var myli = GetEmailBlastsListView;

                
                Collection<string> myc = new Collection<string>();




                //ICollection<T> o = new ICollection<T>
                //var myc = new Collection<Task>;

                //foreach(List<ADBatchClientEmailBlast> myli in  GetEmailBlastsListView)


                //    For Each myli In Me.ExtendedListView1.Items

                //    If myli.Checked = True And myli.Col4 = "YES" Then
                //        myview.FillByKeyValue(TABLES.BatchClientEmailBlast.ColumnNames.BatchClientEmailBlastId, myli.KeyVaue, myrecipient)
                //        Dim s As String = myview.RowItem(TABLES.BatchClientEmailBlast.ColumnNames.Email) & "~" & myview.RowItem(TABLES.BatchClientEmailBlast.ColumnNames.ContactName)
                //        myc.Add(s)
                //        myview.FillEntity(myrecipient)
                //        myrecipient.Status = 1
                //        myrecipient.DateSent = Now()
                //        DBO.Update(myrecipient)
                //    End If

                //Next


                //if (objEmailBlastsVM.chkOutLook == true && GetEmailBlastsListView.Count>0)
                //{
                //     CRF.BLL.CRFView.CRFView.SendOutLookEmail(objEmailBlastsVM.FromEmail, objEmailBlastsVM.FromName, objEmailBlastsVM.Subject, objEmailBlastsVM.Body, myc, objEmailBlastsVM.DocumentsListBox1);
                //}
                //else if(myli.Count > 0)
                //{
                //    CRF.BLL.CRFView.CRFView.SendEmail(myagency.SMTPMAILSERVER, objEmailBlastsVM.FromEmail, objEmailBlastsVM.SMTPUserId, objEmailBlastsVM.SMTPPassword, objEmailBlastsVM.FromName, objEmailBlastsVM.Subject, objEmailBlastsVM.Body, myc, objEmailBlastsVM.DocumentsListBox1);
                //}

                CRF.BLL.Clients.Processes.PostEmailList(user);




            }
            catch (Exception ex)
            {

                throw ex;
            }

            return true;
        }





        #endregion
    }

}
