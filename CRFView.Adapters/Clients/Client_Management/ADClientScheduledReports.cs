﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using CRF.BLL.Clients;
using CRFView.Adapters.Clients.Client_Management;

namespace CRFView.Adapters.Clients.Client_Management
{
    public class ADClientScheduledReports
    {
        #region Properties
        public int Id { get; set; }
        public int ClientId { get; set; }
        public bool Inactive { get; set; }
        public int ScheduledReportId { get; set; }
        public string Frequency { get; set; }
        public string DayofWeek { get; set; }
        public DateTime LastRunDate { get; set; }
        public DateTime NextRunDate { get; set; }
        public string EmailTo { get; set; }
        public string Subject { get; set; }
        #endregion

        #region CRUD

        public static bool SaveUpdateScheduledReport(ADClientScheduledReports Obj_ScheduledReport)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADClientScheduledReports, ClientScheduledReports>();
                ITable tblScheduleReport;
                if (Obj_ScheduledReport.Id != 0)
                {
                    ADClientScheduledReports loadScheduledReport = ReadScheduledReport(Obj_ScheduledReport.Id);
                    loadScheduledReport.ScheduledReportId = Obj_ScheduledReport.ScheduledReportId;
                    loadScheduledReport.EmailTo = Obj_ScheduledReport.EmailTo;
                    loadScheduledReport.NextRunDate = DateTime.Now;
                    ClientScheduledReports objScheduledReport = AutoMapper.Mapper.Map<ClientScheduledReports>(loadScheduledReport);
                    tblScheduleReport = objScheduledReport;
                    DBO.Provider.Update(ref tblScheduleReport);
                }
                else
                {
                    Obj_ScheduledReport.NextRunDate = DateTime.Now;
                    ClientScheduledReports objScheduledReport = AutoMapper.Mapper.Map<ClientScheduledReports>(Obj_ScheduledReport);
                    tblScheduleReport = objScheduledReport;
                    DBO.Provider.Create(ref tblScheduleReport);

                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool DeleteScheduledReport(string Id)
        {
            try
            {
                ITable IReport;
                ClientScheduledReports objRep = new ClientScheduledReports();
                objRep.Id = Convert.ToInt32(Id);
                IReport = objRep;
                DBO.Provider.Delete(ref IReport);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public static ADClientScheduledReports ReadScheduledReport(int id)
        {
            ClientScheduledReports myentity = new ClientScheduledReports();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (ClientScheduledReports)myentityItable;
            AutoMapper.Mapper.CreateMap<ClientScheduledReports, ADClientScheduledReports>();
            return (AutoMapper.Mapper.Map<ADClientScheduledReports>(myentity));
        }

        public static void BindDropDown()
        {
            var temp = DBO.Provider.GetTableView("Select * from Report Where Isdisabled = 0");
        }
        #endregion
    }
}
