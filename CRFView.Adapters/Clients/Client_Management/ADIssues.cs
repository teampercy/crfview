﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using CRF.BLL.Clients;
using CRFView.Adapters.Clients.Client_Management;
using System.IO;
using System.Web.Mvc;
using Fare;

namespace CRFView.Adapters.Clients.Client_Management
{
    public class ADIssues
    {
        #region Properties
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public string IssueTitle { get; set; }
        [AllowHtml]
        public string IssueDescription { get; set; }
        public int IssueTypeId { get; set; }
        public bool IsUrgent { get; set; }
        public int IssueStatusId { get; set; }
        public int IssueCreatorId { get; set; }
        public int IssueAssignedId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateClosed { get; set; }
        public bool Disabled { get; set; }
        public DateTime DateUpdated { get; set; }
        public DateTime NextContactDate { get; set; }
        public string LastStatus { get; set; }
        public int ClientId { get; set; }
        #endregion

        #region CRUD

        public static bool SaveNoteClientFollowup(ADIssues ObjSource, CurrentUser CU_Obj)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<CurrentUser, CRF.BLL.Users.CurrentUser>();
                CRF.BLL.Users.CurrentUser CurrUsr_Obj = AutoMapper.Mapper.Map<CRF.BLL.Users.CurrentUser>(CU_Obj);

                ObjSource.DateCreated = DateTime.Now;
                ObjSource.DateClosed = DateTime.Now;
                ObjSource.IssueCreatorId = CurrUsr_Obj.Id;
                AutoMapper.Mapper.CreateMap<ADIssues, Issues>();
                Issues objIssue = AutoMapper.Mapper.Map<Issues>(ObjSource);
                ITable tblIssue;
                tblIssue = objIssue;
                DBO.Provider.Create(ref tblIssue);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public static ADIssues GetIssue(int id)
        {
            try
            {
                ADIssues Obj_ADIssue = new ADIssues();
                var myitem = new Issues();
                ITable myitemItable = myitem;
                DBO.Provider.Read(id.ToString(), ref myitemItable);
                myitem = (Issues)myitemItable;
                AutoMapper.Mapper.CreateMap<Issues, ADIssues>();
                return (AutoMapper.Mapper.Map<ADIssues>(myitem));
            }
            catch (Exception ex)
            {
                return new Client_Management.ADIssues();
            }
        }

        public static bool SaveUpdateClosedIssue(ADIssues ObjSource, CurrentUser CU_Obj)
        {
            try
            {

               // if(ObjSource.IssueDescription.StartsWith("<p>"))
                //{
                    ObjSource.IssueDescription = ObjSource.IssueDescription.Replace("<p>", "");
                //}

               // if(ObjSource.IssueDescription.EndsWith("</p>"))
               // {
                    ObjSource.IssueDescription = ObjSource.IssueDescription.Replace("</p>", "");
               // }
                CRF.BLL.CRFView.CRFView.GetText(ObjSource.IssueDescription);
                 AutoMapper.Mapper.CreateMap<ADIssues, Issues>();
                ITable tblIssue;
                if (ObjSource.Id != 0)
                {
                    ObjSource.DateCreated = DateTime.Now;
                    ObjSource.DateClosed = DateTime.Now;
                    ObjSource.IssueCreatorId = CU_Obj.Id;
                    Issues objIssue = AutoMapper.Mapper.Map<Issues>(ObjSource);
                    tblIssue = objIssue;
                    DBO.Provider.Update(ref tblIssue);
                }
                else
                {
                    ObjSource.DateCreated = DateTime.Now;
                    ObjSource.DateClosed = DateTime.Now;
                    ObjSource.IssueCreatorId = CU_Obj.Id;
                    Issues objIssue = AutoMapper.Mapper.Map<Issues>(ObjSource);
                    tblIssue = objIssue;
                    DBO.Provider.Create(ref tblIssue);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool SaveUpdateOpenIssue(ADIssues ObjSource, CurrentUser CU_Obj)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADIssues, Issues>();
                ITable tblIssue;
                if (ObjSource.Id != 0)
                {
                    ObjSource.DateCreated = DateTime.Now;
                    ObjSource.IssueAssignedId = ObjSource.IssueAssignedId;
                    ObjSource.IssueCreatorId = CU_Obj.Id;
                    Issues objIssue = AutoMapper.Mapper.Map<Issues>(ObjSource);
                    tblIssue = objIssue;
                    DBO.Provider.Update(ref tblIssue);
                }
                else
                {
                    ObjSource.DateCreated = DateTime.Now;
                    ObjSource.IssueCreatorId = CU_Obj.Id;
                    ObjSource.IssueAssignedId = ObjSource.IssueAssignedId;
                    Issues objIssue = AutoMapper.Mapper.Map<Issues>(ObjSource);
                    tblIssue = objIssue;
                    DBO.Provider.Create(ref tblIssue);
                }
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool DeleteIssueWithId (string id)
        {
            try
            {
                ITable IIssue;
                Issues objIssue = new Issues();
                objIssue.Id = Convert.ToInt32(id);
                IIssue = objIssue;
                DBO.Provider.Delete(ref IIssue);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool CloseIssue(ADIssues ObjSource, CurrentUser CU_Obj)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADIssues, Issues>();
                ITable tblIssue;
                ObjSource.DateCreated = DateTime.Now;
                ObjSource.IssueAssignedId = ObjSource.IssueAssignedId;
                ObjSource.IssueCreatorId = CU_Obj.Id;
                ObjSource.DateClosed = DateTime.Now;
                Issues objIssue = AutoMapper.Mapper.Map<Issues>(ObjSource);
                tblIssue = objIssue;
                DBO.Provider.Update(ref tblIssue);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }

        }


        #endregion



    }
}
