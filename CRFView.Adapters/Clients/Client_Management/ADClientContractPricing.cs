﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Clients.Client_Management
{
    public class ADClientContractPricing
    {
        #region Properties
        public int Id { get; set; }
        public int BillableEventId { get; set; }
        public int ContractId { get; set; }
        public int PriceCodeId { get; set; }
        #endregion
    }
}
