﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.TXNoticeCycle.PrintLetters.ViewModels
{
   public  class TxNoticePrintLettersVM
    {

        public  TxNoticePrintLettersVM()
        {

        }
        public string RbtPrintAction { get; set; }
        public int JobId { get; set; }
        public bool chkNoMailLog { get; set; }
        public bool chkSendToPrinter { get; set; }

    }
}
