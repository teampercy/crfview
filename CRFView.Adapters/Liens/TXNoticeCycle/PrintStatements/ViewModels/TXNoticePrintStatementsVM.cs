﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Liens.TXNoticeCycle.PrintStatements.ViewModels
{
    public class TXNoticePrintStatementsVM
    {


        public TXNoticePrintStatementsVM()
        {
            GetClientList = CommonBindList.GetChangeClientCodeList();
        }

        public IEnumerable<SelectListItem> GetClientList { get; set; }
        public string ClientId { get; set; }
        public bool chkAllClient { get; set; }
        public string DateTimePicker1 { get; set; }
        public string DateTimePicker2 { get; set; }
        public string RbtPrintOption { get; set; }


    }
}
