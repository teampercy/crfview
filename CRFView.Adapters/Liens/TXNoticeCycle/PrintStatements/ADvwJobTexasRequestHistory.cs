﻿using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Liens.TXNoticeCycle.PrintStatements.ViewModels;
using HDS.DAL.COMMON;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADvwJobTexasRequestHistory
    {

        #region Properties

        public int ClientId { get; set; }
        public int JobId { get; set; }
        public string BranchNumber { get; set; }
        public string ClientCode { get; set; }
        public string ClientName { get; set; }
        public string JobNum { get; set; }
        public string JobName { get; set; }
        public string JobAdd1 { get; set; }
        public string JobAdd2 { get; set; }
        public string JobCity { get; set; }
        public string JobState { get; set; }
        public string JobZip { get; set; }
        public string JobCounty { get; set; }
        public string LaborType { get; set; }
        public DateTime StartDate { get; set; }
        public string PrelimDate { get; set; }
        public string EquipRental { get; set; }
        public bool PublicJob { get; set; }
        public bool JointCk { get; set; }
        public bool SuretyBox { get; set; }
        public decimal EstBalance { get; set; }
        public DateTime CurrDate { get; set; }
        public string EquipRate { get; set; }
        public bool UseBranchNTO { get; set; }
        public bool ResidentialBox { get; set; }
        public bool IsRentalCo { get; set; }
        public string Note { get; set; }
        public bool ResendBox { get; set; }
        public bool AmendedBox { get; set; }
        public bool ResendOwner { get; set; }
        public bool ResendGC { get; set; }
        public bool ResendCust { get; set; }
        public bool ResendLender { get; set; }
        public bool CustSend { get; set; }
        public DateTime NoticeSent { get; set; }
        public DateTime ResendDate { get; set; }
        public DateTime AmendedDate { get; set; }
        public string CustJobNum { get; set; }
        public string NoticePhNum { get; set; }
        public string BondNum { get; set; }
        public string APNNum { get; set; }
        public DateTime EndDate { get; set; }
        public string ContactName { get; set; }
        public bool AZCert { get; set; }
        public bool FLCertBox { get; set; }
        public bool SendNTOList { get; set; }
        public bool SendNTO { get; set; }
        public bool GreenCard { get; set; }
        public int Age { get; set; }
        public string LienContact { get; set; }
        public string LienAdd1 { get; set; }
        public string LienAdd2 { get; set; }
        public string LienCity { get; set; }
        public string LienState { get; set; }
        public string LienZip { get; set; }
        public decimal LegalInterest { get; set; }
        public string RecorderNum { get; set; }
        public DateTime FileDate { get; set; }
        public string BookNum { get; set; }
        public string PageNum { get; set; }
        public string LienPhone { get; set; }
        public string LienFax { get; set; }
        public string CustRefNum { get; set; }
        public string CustName { get; set; }
        public string CustAdd1 { get; set; }
        public string CustAdd2 { get; set; }
        public string CustCity { get; set; }
        public string CustState { get; set; }
        public string CustZip { get; set; }
        public string NTOContact { get; set; }
        public string NTOContactTitle { get; set; }
        public string NTOAdd1 { get; set; }
        public string NTOAdd2 { get; set; }
        public string NTOCity { get; set; }
        public string NTOState { get; set; }
        public string NTOZip { get; set; }
        public string NTOEmail { get; set; }
        public string NTOPhone { get; set; }
        public string NTOFax { get; set; }
        public int TXPrelimGrp { get; set; }
        public bool PrivateJob { get; set; }
        public string LienContactTitle { get; set; }
        public string LienEmail { get; set; }
        public string LienAs { get; set; }
        public string NTOAs { get; set; }
        public string BranchAs { get; set; }
        public string CustContact { get; set; }
        public bool SendOIR { get; set; }
        public bool SendAck { get; set; }
        public int OtherLglThreshold { get; set; }
        public string EmpName { get; set; }
        public string EmpTitle { get; set; }
        public string EmpPhNum { get; set; }
        public string EmpFaxNum { get; set; }
        public string JobStatus { get; set; }
        public string JobStatusDescr { get; set; }
        public string JobStatusType { get; set; }
        public DateTime DateAssigned { get; set; }
        public DateTime VerifiedDate { get; set; }
        public DateTime LienAssignDate { get; set; }
        public DateTime LienDate { get; set; }
        public string StatusCode { get; set; }
        public string DeskNum { get; set; }
        public string FolioNum { get; set; }
        public string ReleaseDocNum { get; set; }
        public bool PrelimBox { get; set; }
        public bool RushOrder { get; set; }
        public bool VerifyJob { get; set; }
        public bool PODBox { get; set; }
        public bool FederalJob { get; set; }
        public bool MechanicLien { get; set; }
        public bool TitleVerifiedBox { get; set; }
        public string CustCertNum { get; set; }
        public string GCCertNum { get; set; }
        public string LenderCertNum { get; set; }
        public string OwnerCertNum { get; set; }
        public string CustPhone { get; set; }
        public string CustFax { get; set; }
        public bool IsTXNotice { get; set; }
        public bool IsTX60Day { get; set; }
        public bool IsTX90Day { get; set; }
        public DateTime DateRequested { get; set; }
        public DateTime DatePrinted { get; set; }
        public decimal AmountOwed { get; set; }
        public string MonthDebtIncurred { get; set; }
        public string StmtOfAccts { get; set; }
        public string JobCityStateZip { get; set; }
        public string CustCityStateZip { get; set; }
        public int JobNoticeRequestId { get; set; }

        #endregion

        #region CRUD


        // print TXNotice PrintStatements

        public static string PrintTXNoticePrintStatementsReport(TXNoticePrintStatementsVM objTXNoticePrintStatementsVM)
        {
            string pdfPath = null;
            string sFromDate = null;
            string sThruDate = null;

            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var Sp_OBJ = new uspbo_PrelimNotices_GetTexasStatements();

                if (objTXNoticePrintStatementsVM.chkAllClient == false)
                {
                    Sp_OBJ.ClientId = objTXNoticePrintStatementsVM.ClientId;
                }

                Sp_OBJ.FromDate = Convert.ToDateTime(objTXNoticePrintStatementsVM.DateTimePicker1, System.Globalization.CultureInfo.InvariantCulture);
                Sp_OBJ.ThruDate = Convert.ToDateTime(objTXNoticePrintStatementsVM.DateTimePicker2, System.Globalization.CultureInfo.InvariantCulture);
                //sFromDate = objAccountingSmanCommissionsVM.DateTimePicker1;
                //sThruDate = objAccountingSmanCommissionsVM.DateTimePicker2;

                //TableView myview = CRF.BLL.Providers.DBO.Provider.GetTableView(Sp_OBJ);

                CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;
                //mymode = mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                if (objTXNoticePrintStatementsVM.RbtPrintOption == "optToPDF")
                {
                    mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                }

                pdfPath = Convert.ToString( CRF.BLL.Providers.LiensProcesses.GetTexasStatements(user, Sp_OBJ, mymode, ""));

            }

            catch (Exception ex)
            {
                return "";
            }



            return pdfPath;

        }




        #endregion



    }
}
