﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.Providers;
using CRFView.Adapters.Liens.TXNoticeCycle.SmartMailerUpdate.ViewModels;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADSmartMailerUpdate
    {

        public static bool GetDataSmartMailerUpdate(string afilepath)
        {
            CRF.BLL.Users.CurrentUser auser = new CRF.BLL.Users.CurrentUser();

            var sr = new StreamReader(afilepath);
            int mycount = 0;
            bool myreturn = true;
            var myline = "";

            do
            {
                myline = sr.ReadLine();

                if(mycount > 0)
                {
                    myreturn = ProcessLineSmartMailerUpdate(auser, afilepath, myline);
                }
                mycount += 1;

            }
            while (sr.Peek() > -1 && myreturn == true);


            return true;
        }



        public static bool ProcessLineSmartMailerUpdate(CRF.BLL.Users.CurrentUser auser, string afilepath, String aline)
        {
            string[] sfields = CRF.BLL.Collections.DataEntry.ImportEDI.ImportUtils.CSVParser(aline, ",");

            if (sfields.Length < 63)
            {
                return false;
            }
            if (sfields[4] == "")
            {
                return false;
            }
            if (sfields[9] == "CN")
            {
                return false;
            }

            var mysproc = new uspbo_LiensDayEnd_SmartMailerUpdate();

            mysproc.LegalPartyId = Convert.ToInt32(sfields[16]);
            mysproc.JobId = Convert.ToInt32(sfields[3]);
            mysproc.TypeCode = sfields[17];
            mysproc.Company = sfields[4];
            mysproc.Street = sfields[5];
            mysproc.Street2 = sfields[7];
            mysproc.City = sfields[8];
            mysproc.State = sfields[9];

            if(DBNull.Value.Equals(sfields[11]))
            {
                mysproc.Zip = sfields[10];
            }
            else
            {
                mysproc.Zip = sfields[10] + sfields[11];
            }

            DBO.Provider.ExecNonQuery(mysproc);

            return true;

        }




    }
}
