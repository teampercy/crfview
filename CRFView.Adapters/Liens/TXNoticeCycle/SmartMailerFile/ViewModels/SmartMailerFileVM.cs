﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.TXNoticeCycle.SmartMailerFile.ViewModels
{
    public class SmartMailerFileVM
    {

        public string DateTimePicker1 { get; set; }

        public string Message { get; set; }

    }
}
