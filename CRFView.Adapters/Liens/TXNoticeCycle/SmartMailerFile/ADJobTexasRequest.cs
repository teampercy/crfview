﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Export;
using CRF.BLL.Providers;
using CRFView.Adapters.Liens.TXNoticeCycle.SmartMailerFile.ViewModels;
using CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADJobTexasRequest
    {

        #region Properties

        public int Id { get; set; }
        public int JobId { get; set; }
        public string SubmittedBy { get; set; }
        public int SubmittedByUserId { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateProcessed { get; set; }
        public bool IsTX60Day { get; set; }
        public bool IsTX90Day { get; set; }
        public bool IsProcessed { get; set; }
        public decimal AmountOwed { get; set; }
        public string MonthDebtIncurred { get; set; }
        public string StmtOfAccts { get; set; }
        public bool RushBox { get; set; }
        public bool IsOnHold { get; set; }

        #endregion


        #region CRUD

        public static ADJobTexasRequest ReadJobTexasRequest(int id)
        {
            JobTexasRequest myentity = new JobTexasRequest();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (JobTexasRequest)myentityItable;
            AutoMapper.Mapper.CreateMap<JobTexasRequest, ADJobTexasRequest>();
            return (AutoMapper.Mapper.Map<ADJobTexasRequest>(myentity));
        }



        // print Smart Mailer File Details

        public static string PrintSmartMailerFile(SmartMailerFileVM objSmartMailerFileVM)
        {
            string pdfPath = null;
            string sFromDate = null;
            string sThruDate = null;

            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var mysproc = new uspbo_PrelimNotices_GetSmartMailerFileTX();

                mysproc.THRUDATE = objSmartMailerFileVM.DateTimePicker1;

                TableView MyView = CRF.BLL.Providers.DBO.Provider.GetTableView(mysproc);

                if (MyView.Count < 1)
                {
                    return "";
                }

                pdfPath = CRF.BLL.CRFView.CRFView.GetFileName(ref user, "TXSMARTMAILEREXTRACT", "TXSMARTMAILER", ".CSV", true);

                //MYVIEW.ExportToCSV(myspreadsheetpath)
                CRF.BLL.Export.ExportColumns mycols = new CRF.BLL.Export.ExportColumns();

                mycols.Add("JobId");
                mycols.Add("LegalName");
                mycols.Add("LegalAdd1");
                mycols.Add("LegalAdd2");
                mycols.Add("LegalCity");
                mycols.Add("LegalState");
                mycols.Add("LegalZip");
                mycols.Add("Barcode");
                mycols.Add("ClientCode");
                mycols.Add("Greencard");
                mycols.Add("Secondaryid");
                mycols.Add("TypeCode");


                DataView MyDataView = (DataView)MyView;

                 CRF.BLL.Export.Export.ExportToCSV(ref MyDataView, ref pdfPath, ref mycols, false);

               // Export.ExportToCSV(MYVIEW.Table, myspreadsheetpath, mycols, False)

                //pdfPath = Convert.ToString(ADJobTexasRequest.ExportToFileAll(MyView));

                //  pdfPath = ADJobTexasRequest.ExportToFile(MyView);



                var result = "Completed (" + MyView.Count + ") Records";

            }

            catch (Exception ex)
            {
                return "";
            }



            return pdfPath;

        }




        public static bool ExportToFileAll(TableView MyView)
        {
            CRF.BLL.Export.ExportColumns mycols = new CRF.BLL.Export.ExportColumns();
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

            string sfilepath = CRF.BLL.CRFView.CRFView.GetFileName(ref user, "TXSMARTMAILEREXTRACT", "TXSMARTMAILER", ".CSV", true);

            mycols.Add("JobId");
            mycols.Add("LegalName");
            mycols.Add("LegalAdd1");
            mycols.Add("LegalAdd2");
            mycols.Add("LegalCity");
            mycols.Add("LegalState");
            mycols.Add("LegalZip");
            mycols.Add("Barcode");
            mycols.Add("ClientCode");
            mycols.Add("Greencard");
            mycols.Add("Secondaryid");
            mycols.Add("TypeCode");

            MyView.ExportToCSV(sfilepath);

            return true;

        }
        //private static string ExportToFile(DataView aview)
        //{
        //    CRF.BLL.Export.ExportColumns mycols = new CRF.BLL.Export.ExportColumns();
        //    CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
        //    string sfilepath = CRF.BLL.CRFView.CRFView.GetFileName(ref user, "Management", areportname, ".CSV");
        //    string fileName = CRF.BLL.Export.Export.ExportToCSV(ref aview, ref sfilepath, ref mycols, true);
        //    if (!string.IsNullOrEmpty(fileName))
        //    {
        //        return fileName;
        //    }
        //    else
        //    {
        //        return "";
        //    }
        //}



        public static List<ADJobTexasRequest> GetJobTexasRequestList(DataTable dt)
        {
            try
            {
                if (dt.Rows.Count > 0)
                {

                    List<ADJobTexasRequest> TexasRequest = (from DataRow dr in dt.Rows
                                                                    select new ADJobTexasRequest()
                                                                    {
                                                                        Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                                                        SubmittedBy = ((dr["SubmittedBy"] != DBNull.Value ? dr["SubmittedBy"].ToString() : "")),
                                                                        DateCreated = ((dr["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dr["DateCreated"]) : DateTime.MinValue)),
                                                                        IsTX60Day = ((dr["IsTX60Day"] != DBNull.Value ? Convert.ToBoolean(dr["IsTX60Day"]) : false)),
                                                                        IsTX90Day = ((dr["IsTX90Day"] != DBNull.Value ? Convert.ToBoolean(dr["IsTX90Day"]): false)),
                                                                        AmountOwed = ((dr["AmountOwed"] != DBNull.Value ? Convert.ToDecimal(dr["AmountOwed"]) : 0)),
                                                                        MonthDebtIncurred = ((dr["MonthDebtIncurred"] != DBNull.Value ? dr["MonthDebtIncurred"].ToString() : "")),
                                                                        RushBox = ((dr["RushBox"] != DBNull.Value ? Convert.ToBoolean(dr["RushBox"]) : false)),
                                                                        IsOnHold = ((dr["IsOnHold"] != DBNull.Value ? Convert.ToBoolean(dr["IsOnHold"]) : false)),

                                                                    }).ToList();
                    return TexasRequest;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return (new List<ADJobTexasRequest>());
            }
        }


        public static int SaveDetails(TexasRequestVM objtexasRequestVM)
        {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            ITable tblI;

            ADJobTexasRequest objADJobTexasRequest = new ADJobTexasRequest();
            objADJobTexasRequest = ADJobTexasRequest.ReadJobTexasRequest(Convert.ToInt32(objtexasRequestVM.Id));

            //ADClientGeneralContractor objADClientGeneralContractor = ReadClientGeneralContractor(CustId);
            AutoMapper.Mapper.CreateMap<ADJobTexasRequest, JobTexasRequest>();
            JobTexasRequest myentity;
            myentity = AutoMapper.Mapper.Map<JobTexasRequest>(objADJobTexasRequest);

            myentity.JobId = objtexasRequestVM.JobId;
            myentity.AmountOwed = objtexasRequestVM.OBJADJobTexasRequest.AmountOwed;
            if (objtexasRequestVM.RdbBtn == "60Day")
            {
                myentity.IsTX60Day = true;
            }
            if (objtexasRequestVM.RdbBtn == "90Day")
            {
                myentity.IsTX90Day = true;
            }
           // myentity.IsTX60Day = objtexasRequestVM.OBJADJobTexasRequest.IsTX60Day;
            //myentity.IsTX90Day = objtexasRequestVM.OBJADJobTexasRequest.IsTX90Day;
            myentity.MonthDebtIncurred = objtexasRequestVM.OBJADJobTexasRequest.MonthDebtIncurred;
            myentity.StmtOfAccts = objtexasRequestVM.OBJADJobTexasRequest.StmtOfAccts;
            myentity.IsOnHold = objtexasRequestVM.OBJADJobTexasRequest.IsOnHold;

            myentity.DateCreated = objADJobTexasRequest.DateCreated;
            myentity.SubmittedBy = objADJobTexasRequest.SubmittedBy;
            myentity.SubmittedByUserId = objADJobTexasRequest.SubmittedByUserId;
         

            if (objtexasRequestVM.Id < 1)
            {
                myentity.DateCreated = DateTime.Now;
                myentity.SubmittedBy = user.UserName;
                myentity.SubmittedByUserId = user.Id;
                tblI = myentity;

                DBO.Provider.Create(ref tblI);
                
            }
            else
            {
                myentity.Id = objtexasRequestVM.Id;
                tblI = myentity;
                DBO.Provider.Update(ref tblI);
            }
           
            return 1;
        }


        public static bool DeleteTexasRequestDetails(int Id, int JobId)
        {
            try
            {
                ITable tblJobTexasRequest;
                JobTexasRequest objJobTexasRequest = new JobTexasRequest();
                objJobTexasRequest.Id = Convert.ToInt32(Id);
                tblJobTexasRequest = objJobTexasRequest;
                DBO.Provider.Delete(ref tblJobTexasRequest);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion



    }
}
