﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Liens.DataEntry.DataEntry.ViewModels
{
    public class BatchJobVM
    {
        public BatchJobVM()
        {
           
        }

        #region Properties
        public List<ADBatchJob> objBatchJobList { get; set; }
        public List<ADBatchJobLegalParties> objBatchJobLegalPartiesList { get; set; }
        public List<ADStateInfo> objStateInfoList { get; set; }
        public List<ADClient> objClientList { get; set; }
        public List<ADClientLienInfo> objClientLienInfoList { get; set; }
        public List<ADJobAttachments> objJobAttachmentsList { get; set; }

        public ADBatchJob  objBatchJob { get; set; }
        public ADBatchJobLegalParties objBatchJobLegalParties { get; set; }
        public ADStateInfo  objStateInfo { get; set; }
        public ADClient objClient { get; set; }
        public ADClientLienInfo objClientLienInfo { get; set; }
        public ADJobActions objJobActions { get; set; }
        public ADCompanyInfo objCompanyInfo { get; set; }
        public  ADZipCode objZipCode { get; set; }

        public string Id { get; set; }
      
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }

        public DataTable objZipCodeDt { get; set; }

        public List<ADZipCode> objZipCodeList { get; set; }
    
        #endregion

    }
}
