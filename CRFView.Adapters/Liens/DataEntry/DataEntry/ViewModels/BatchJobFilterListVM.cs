﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Liens.DataEntry.DataEntry.ViewModels
{
    public class BatchJobFilterListVM
    {

        #region Properties
        public List<ADvwBatchJobs> ObjBatchJobs { get; set; }

        public string SortType { get; set; }
        public List<SelectListItem> SortTypeList = new List<SelectListItem>();

        public string SortBy { get; set; }
        public List<SelectListItem> SortByList = new List<SelectListItem>();

        #endregion

        public BatchJobFilterListVM()
        {
            populateLists();

        }


        public int listCount;
        //public int listCount;
        private void populateLists()
        {
            SortByList.Add(new SelectListItem { Text = "Cust Fax", Value = "-1" });
            //SortByList.Add(new SelectListItem { Text = "Sort By", Value = "-1" });
            SortByList.Add(new SelectListItem { Text = "Cust Email", Value = "1" });
            //SortByList.Add(new SelectListItem { Text = "Contact", Value = "2" });
            SortByList.Add(new SelectListItem { Text = "GC Fax", Value = "2" });
            //SortByList.Add(new SelectListItem { Text = "Debtor Ref#", Value = "4" });
            SortByList.Add(new SelectListItem { Text = "GC Email", Value = "3" });
           

            SortTypeList.Add(new SelectListItem { Text = "Ascending", Value = "-1" });
            SortTypeList.Add(new SelectListItem { Text = "Descending", Value = "1" });
        }

    
}

}
