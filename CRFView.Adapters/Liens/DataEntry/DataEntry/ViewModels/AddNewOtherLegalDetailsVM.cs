﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.DataEntry.DataEntry.ViewModels
{
    public class AddNewOtherLegalDetailsVM
    {
        public AddNewOtherLegalDetailsVM()
        {
            objBatchJobLegalParties = new ADBatchJobLegalParties();
        }

        public ADBatchJobLegalParties objBatchJobLegalParties { get; set; }

        public string objBatchJobId { get; set; }
    }
}
