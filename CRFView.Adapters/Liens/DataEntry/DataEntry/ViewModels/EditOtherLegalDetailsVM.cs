﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.DataEntry.DataEntry.ViewModels
{
    public class EditOtherLegalDetailsVM
    {
        public EditOtherLegalDetailsVM()
        {
             
        }

        public ADBatchJobLegalParties objBatchJobLegalPartiesEdit { get; set; }

        public string objBatchJobId { get; set; }

        public string Id { get; set; }
    }
}
