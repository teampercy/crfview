﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Liens.DataEntry.DataEntry.ViewModels
{
    public class EditBatchJobVM
    {
        public EditBatchJobVM()
        {
            ClientIdList = CommonBindList.GetLienClientIdList();
            JobStateIdList = CommonBindList.GetLienJobStateIdList();
        }

        #region Properties
        public ADBatchJob objBatchJobEdit { get; set; }


        public string ddlClientId { get; set; }
        public List<CommonBindList> ClientIdList { get; set; }

        public string ddlJobStateId { get; set; }
        public List<CommonBindList> JobStateIdList { get; set; }

        public string Id { get; set; }

        #endregion
    }
}
