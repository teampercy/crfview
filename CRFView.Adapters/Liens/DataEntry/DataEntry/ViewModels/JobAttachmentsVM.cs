﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Liens.DataEntry.DataEntry.ViewModels
{
    public class JobAttachmentsVM
    {
        public JobAttachmentsVM()
        {
            objJobAttachments = new ADJobAttachments();
            objJobAttachmentsEdit = new ADJobAttachments();
            DocumentTypeList = CommonBindList.GetDocumentType();
        }

        public ADJobAttachments objJobAttachments { get; set; }
        public ADJobAttachments objJobAttachmentsEdit { get; set; }

        public string ddlDocumentType { get; set; }
        public IEnumerable<SelectListItem> DocumentTypeList { get; set; }
        public string DocumentType { get; set; }

        public string BatchJobId { get; set; }
        public string Id { get; set; }
    }
}
