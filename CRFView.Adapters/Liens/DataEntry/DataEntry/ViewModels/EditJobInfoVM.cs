﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.DataEntry.DataEntry.ViewModels
{
    public class EditJobInfoVM
    {
        public EditJobInfoVM()
        {
            JobActionIdList = CommonBindList.GetJobActionIdList();
        }

        #region Properties
        public ADBatchJob objBatchJobEdit { get; set; }
        public string Id { get; set; }

        public string ddlJobActionId { get; set; }
        public List<CommonBindList> JobActionIdList { get; set; }

        public string RbtJobType { get; set; }
        #endregion
    }
}
