﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.DataEntry.DataEntry.ViewModels
{
   public  class EditOwnerInfoVM
    {
        public EditOwnerInfoVM()
        {

        }

        #region Properties
        public ADBatchJob objBatchJobEdit { get; set; }
        public string Id { get; set; }

        #endregion
    }
}
