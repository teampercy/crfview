﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
 using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADJobAttachments
    {

        #region Properties
        public int Id { get; set; }
        public int JobId { get; set; }
        public int UserId { get; set; }
        public string UserCode { get; set; }
        public bool Processed { get; set; }
        public DateTime DateCreated { get; set; }
        public string FileName { get; set; }
        public string DocumentType { get; set; }
        public string DocumentDescription { get; set; }
        public byte[] DocumentImage { get; set; }
        public int BatchJobId { get; set; }
        public bool IsClientview { get; set; }
        #endregion
        
        #region CRUD
        public static List<ADJobAttachments> GetJobAttachments(DataTable dt)
        {
            List<ADJobAttachments> Documents = (from DataRow dr in dt.Rows
                                                       select new ADJobAttachments()
                                                       {
                                                           Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                                           DateCreated = ((dr["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dr["DateCreated"]) : DateTime.MinValue)),
                                                           DocumentType = ((dr["DocumentType"] != DBNull.Value ? dr["DocumentType"].ToString() : "")),
                                                           DocumentDescription = ((dr["DocumentDescription"] != DBNull.Value ? dr["DocumentDescription"].ToString() : "")),
                                                           UserCode = ((dr["UserCode"] != DBNull.Value ? dr["UserCode"].ToString() : "")),
                                                       }).ToList();
            if (Documents.Count == 0)
            {
                return (new List<ADJobAttachments>());
            }
            return Documents;
        }

        public static ADJobAttachments ReadJobAttachment(int Id)
        {
            JobAttachments myentity = new JobAttachments();
            ITable myentityItable = myentity;
            DBO.Provider.Read(Id.ToString(), ref myentityItable);
            myentity = (JobAttachments)myentityItable;
            AutoMapper.Mapper.CreateMap<JobAttachments, ADJobAttachments>();
            return (AutoMapper.Mapper.Map<ADJobAttachments>(myentity));
        }

        public static int SaveJobAttachment(ADJobAttachments objUpdatedADJobAttachments)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADJobAttachments, JobAttachments>();
                ITable tblADJobAttachments;
                JobAttachments objJobAttachments;

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                if (objUpdatedADJobAttachments.Id != 0)
                {
                    ADJobAttachments objADJobAttachmentsOriginal = ReadJobAttachment(Convert.ToInt32(objUpdatedADJobAttachments.Id));

                    if (objUpdatedADJobAttachments.DateCreated.ToString() == "" || objUpdatedADJobAttachments.DateCreated == null)
                    {
                    }
                    else
                    {
                        objADJobAttachmentsOriginal.DateCreated = objUpdatedADJobAttachments.DateCreated;
                    }

                    if (objUpdatedADJobAttachments.DocumentType == "" || objUpdatedADJobAttachments.DocumentType == null)
                    {
                    }
                    else
                    {
                        objADJobAttachmentsOriginal.DocumentType = objUpdatedADJobAttachments.DocumentType.Trim();
                    }

                    if (objUpdatedADJobAttachments.FileName == "" || objUpdatedADJobAttachments.FileName == null)
                    {
                    }
                    else
                    {
                        objADJobAttachmentsOriginal.FileName = objUpdatedADJobAttachments.FileName.Trim();
                    }

                    if (objUpdatedADJobAttachments.DocumentDescription == "" || objUpdatedADJobAttachments.DocumentDescription == null)
                    {
                    }
                    else
                    {
                        objADJobAttachmentsOriginal.DocumentDescription = objUpdatedADJobAttachments.DocumentDescription.Trim();
                    }

                    if (objUpdatedADJobAttachments.DocumentImage == null)
                    {
                    }
                    else
                    {
                        objADJobAttachmentsOriginal.DocumentImage = objUpdatedADJobAttachments.DocumentImage;
                    }
                    objADJobAttachmentsOriginal.DocumentDescription = "[Filename: " + objADJobAttachmentsOriginal.FileName + "]  ";

                    objADJobAttachmentsOriginal.UserId = user.Id;
                    objADJobAttachmentsOriginal.UserCode = user.UserName;
                    objJobAttachments = AutoMapper.Mapper.Map<JobAttachments>(objADJobAttachmentsOriginal);
                    tblADJobAttachments = (ITable)objJobAttachments;
                    DBO.Provider.Update(ref tblADJobAttachments);
                }
                else
                {
                    objUpdatedADJobAttachments.UserId = user.Id;
                    objUpdatedADJobAttachments.UserCode = user.UserName;
                    objJobAttachments = AutoMapper.Mapper.Map<JobAttachments>(objUpdatedADJobAttachments);
                    tblADJobAttachments = (ITable)objJobAttachments;
                    DBO.Provider.Create(ref tblADJobAttachments);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static bool DeleteDocument(string Id)
        {
            try
            {
                ITable IJobAttachments;
                JobAttachments objJobAttachments = new JobAttachments();
                objJobAttachments.Id = Convert.ToInt32(Id);
                IJobAttachments = objJobAttachments;
                DBO.Provider.Delete(ref IJobAttachments);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static void SaveFileToBeViewed(byte[] ByteArray, string filePath)
        {
            File.WriteAllBytes(filePath, ByteArray);
        }


        public static List<ADJobAttachments> GetJobAttachments(string BatchJobId)
        {
            string mysql = "Select Id, DateCreated,UserCode, DocumentType,DocumentDescription from JobAttachments ";
            mysql += "  Where BatchJobId > 0 and BatchJobId = " + BatchJobId;
            var JobAttachmentsView = DBO.Provider.GetTableView(mysql);
            DataTable dt = JobAttachmentsView.ToTable();

            List<ADJobAttachments> Documents = (from DataRow dr in dt.Rows
                                                       select new ADJobAttachments()
                                                       {
                                                           Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                                           DateCreated = ((dr["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dr["DateCreated"]) : DateTime.MinValue)),
                                                           DocumentType = ((dr["DocumentType"] != DBNull.Value ? dr["DocumentType"].ToString() : "")),
                                                           DocumentDescription = ((dr["DocumentDescription"] != DBNull.Value ? dr["DocumentDescription"].ToString() : "")),
                                                           UserCode = ((dr["UserCode"] != DBNull.Value ? dr["UserCode"].ToString() : "")),
                                                       }).ToList();
            if (Documents.Count == 0)
            {
                return (new List<ADJobAttachments>());
            }
            return Documents;
        }

        public static List<ADJobAttachments> GetJobAttachmentsByJobId(string JobId)
        {
            string mysql = "Select Id, DateCreated,UserCode,FileName,DocumentImage, DocumentType,DocumentDescription from JobAttachments ";
            mysql += "where JobId =" + JobId;
            var JobAttachmentsView = DBO.Provider.GetTableView(mysql);
            DataTable dt = JobAttachmentsView.ToTable();

            List<ADJobAttachments> Documents = (from DataRow dr in dt.Rows
                                                select new ADJobAttachments()
                                                {
                                                    Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                                    DateCreated = ((dr["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dr["DateCreated"]) : DateTime.MinValue)),
                                                    DocumentType = ((dr["DocumentType"] != DBNull.Value ? dr["DocumentType"].ToString() : "")),
                                                    DocumentDescription = ((dr["DocumentDescription"] != DBNull.Value ? dr["DocumentDescription"].ToString() : "")),
                                                    UserCode = ((dr["UserCode"] != DBNull.Value ? dr["UserCode"].ToString() : "")),
                                                    FileName = (dr["FileName"] != DBNull.Value ? dr["FileName"].ToString() : ""),
                                                    //DocumentImage = dr["DocumentImage"] != DBNull.Value ? Convert.ToByte[](dr["DocumentImage"]).ToArray() : "",

                                                }).ToList();
            if (Documents.Count == 0)
            {
                return (new List<ADJobAttachments>());
            }
            return Documents;
        }


        #endregion

    }
}
