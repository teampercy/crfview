﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using HDS.DAL.COMMON;
using System.Data;
using CRF.BLL.Providers;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Liens.DataEntry.DataEntry.ViewModels;

namespace CRFView.Adapters
{
    public class ADvwBatchJobs
    {

        #region Properties
        public DateTime FileDate { get; set; }
        public string BatchStatus { get; set; }
        public DateTime ProcessOn { get; set; }
        public string BatchType { get; set; }
        public DateTime BatchDate { get; set; }
        public int OtherLglThreshold { get; set; }
        public bool SendAck { get; set; }
        public bool SendOIR { get; set; }
        public string BranchAs { get; set; }
        public string NTOAs { get; set; }
        public string LienAs { get; set; }
        public string LienEmail { get; set; }
        public string LienContactTitle { get; set; }
        public int TXPrelimGrp { get; set; }
        public string LienFax { get; set; }
        public string LienPhone { get; set; }
        public decimal LegalInterest { get; set; }
        public string LienZip { get; set; }
        public string LienState { get; set; }
        public string LienCity { get; set; }
        public string LienAdd2 { get; set; }
        public string LienAdd1 { get; set; }
        public string LienContact { get; set; }
        public bool SendNTO { get; set; }
        public bool SendNTOList { get; set; }
        public bool FLCertBox { get; set; }
        public bool AZCert { get; set; }
        public string ContactName { get; set; }
        public string NoticePhNum { get; set; }
        public bool CustSend { get; set; }
        public bool IsRentalCo { get; set; }
        public bool UseBranchNTO { get; set; }
        public DateTime CurrDate { get; set; }
        public string LaborType { get; set; }
        public string ClientName { get; set; }
        public string ClientCode { get; set; }
        public int Id { get; set; }
        public int BatchId { get; set; }
        public DateTime SubmittedOn { get; set; }
        public int SubmittedByUserId { get; set; }
        public int ClientId { get; set; }
        public string PlacementSource { get; set; }
        public string BranchNum { get; set; }
        public string JobNum { get; set; }
        public string JobName { get; set; }
        public string JobAdd1 { get; set; }
        public string JobAdd2 { get; set; }
        public string JobCity { get; set; }
        public string JobState { get; set; }
        public string JobZip { get; set; }
        public string JobCounty { get; set; }
        public decimal EstBalance { get; set; }
        public string APNNum { get; set; }
        public string AZLotAllocate { get; set; }
        public string PONum { get; set; }
        public string BondNum { get; set; }
        public string FolioNum { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime RevDate { get; set; }
        public string SpecialInstruction { get; set; }
        public string EquipRate { get; set; }
        public string EquipRental { get; set; }
        public string Note { get; set; }
        public int CustId { get; set; }
        public string CustJobNum { get; set; }
        public string CustRefNum { get; set; }
        public string CustName { get; set; }
        public string CustContactName { get; set; }
        public string CustAdd1 { get; set; }
        public string CustAdd2 { get; set; }
        public string CustCity { get; set; }
        public string CustState { get; set; }
        public string CustZip { get; set; }
        public string CustPhone1 { get; set; }
        public string CustPhone2 { get; set; }
        public string CustFax { get; set; }
        public string CustEmail { get; set; }
        public string CustMatchingKey { get; set; }
        public int GenId { get; set; }
        public string GCRefNum { get; set; }
        public string GCName { get; set; }
        public string GCContactName { get; set; }
        public string GCAdd1 { get; set; }
        public string GCAdd2 { get; set; }
        public string GCCity { get; set; }
        public string GCState { get; set; }
        public string GCZip { get; set; }
        public string GCPhone1 { get; set; }
        public string GCPhone2 { get; set; }
        public string GCFax { get; set; }
        public string GCEmail { get; set; }
        public string GCMatchingKey { get; set; }
        public string OwnrName { get; set; }
        public string OwnrAdd1 { get; set; }
        public string OwnrAdd2 { get; set; }
        public string OwnrCity { get; set; }
        public string OwnrState { get; set; }
        public string OwnrZip { get; set; }
        public string OwnrPhone1 { get; set; }
        public string OwnrPhone2 { get; set; }
        public string OwnrFax { get; set; }
        public string LenderName { get; set; }
        public string LenderAdd1 { get; set; }
        public string LenderAdd2 { get; set; }
        public string LenderCity { get; set; }
        public string LenderState { get; set; }
        public string LenderZip { get; set; }
        public string LenderPhone1 { get; set; }
        public string LenderPhone2 { get; set; }
        public string LenderFax { get; set; }
        public string DesigneeName { get; set; }
        public string DesigneeAdd1 { get; set; }
        public string DesigneeAdd2 { get; set; }
        public string DesigneeCity { get; set; }
        public string DesigneeState { get; set; }
        public string DesigneeZip { get; set; }
        public string DesigneePhone1 { get; set; }
        public string DesigneePhone2 { get; set; }
        public string DesigneeFax { get; set; }
        public bool SameAsCust { get; set; }
        public bool SameAsGC { get; set; }
        public bool PrelimASIS { get; set; }
        public bool PrivateJob { get; set; }
        public bool RushOrderML { get; set; }
        public bool MechanicLien { get; set; }
        public bool StopNoticeBox { get; set; }
        public bool PrelimBox { get; set; }
        public bool VerifyJob { get; set; }
        public bool TitleVerifiedBox { get; set; }
        public bool RushOrder { get; set; }
        public bool GreenCard { get; set; }
        public bool PublicJob { get; set; }
        public bool ResidentialBox { get; set; }
        public bool JointCk { get; set; }
        public bool FederalJob { get; set; }
        public bool LienRelease { get; set; }
        public bool PaymentBond { get; set; }
        public bool IsError { get; set; }
        public bool IsWarning { get; set; }
        public bool NCInfo { get; set; }
        public string ErrorDescription { get; set; }
        public int DeskNumId { get; set; }
        public int StatusCodeId { get; set; }
        public int JobActionId { get; set; }
        public DateTime NoticeSent { get; set; }
        public DateTime VerifiedDate { get; set; }
        public string AddressLine1 { get; set; }
        public string OwnerName { get; set; }
        public DateTime UpdatedOn { get; set; }
        public string UserName { get; set; }
        public bool VerifyOWOnly { get; set; }
        public bool VerifyOWOnlyTX { get; set; }
        public bool VerifyOWOnlySend { get; set; }
        #endregion


        #region CRUD
        public static List<ADvwBatchJobs> GetInitialList(FilterVM objFilterVM, string sortBy)
        {
            try
            {
                TableView EntityList;
                var sp_obj = new uspbo_LiensDataEntry_GetJobsInBatches();

                sp_obj.ErrorsOnly = false;
                sp_obj.BatchFROM = Convert.ToString(objFilterVM.BatchIdList.FirstOrDefault().Value);
                sp_obj.BatchTO = Convert.ToString(objFilterVM.BatchIdList.FirstOrDefault().Value);
                sp_obj.SelectByBatch = true;

                if (string.IsNullOrEmpty(sortBy) == false)
                {
                    sp_obj.SortColType = sortBy;
                }
                EntityList = DBO.Provider.GetTableView(sp_obj);
                DataTable dt = EntityList.ToTable();
                List<ADvwBatchJobs> BatchJobList = (from DataRow dr in dt.Rows
                                                   select new ADvwBatchJobs()
                                                   {
                                                       FileDate = ((dr["FileDate"] != DBNull.Value ? Convert.ToDateTime(dr["FileDate"]) : DateTime.MinValue)),
                                                       BatchStatus = ((dr["BatchStatus"] != DBNull.Value ? dr["BatchStatus"].ToString() : "")),
                                                       ProcessOn = ((dr["ProcessOn"] != DBNull.Value ? Convert.ToDateTime(dr["ProcessOn"]) : DateTime.MinValue)),
                                                       BatchType = ((dr["BatchType"] != DBNull.Value ? dr["BatchType"].ToString() : "")),
                                                       BatchDate = ((dr["BatchDate"] != DBNull.Value ? Convert.ToDateTime(dr["BatchDate"]) : DateTime.MinValue)),
                                                       OtherLglThreshold = ((dr["OtherLglThreshold"] != DBNull.Value ? Convert.ToInt32(dr["OtherLglThreshold"]) : 0)),
                                                       SendAck = ((dr["SendAck"] != DBNull.Value ? Convert.ToBoolean(dr["SendAck"]) : false)),
                                                       SendOIR = ((dr["SendOIR"] != DBNull.Value ? Convert.ToBoolean(dr["SendOIR"]) : false)),
                                                       BranchAs = ((dr["BranchAs"] != DBNull.Value ? dr["BranchAs"].ToString() : "")),
                                                       NTOAs = ((dr["NTOAs"] != DBNull.Value ? dr["NTOAs"].ToString() : "")),
                                                       LienAs = ((dr["LienAs"] != DBNull.Value ? dr["LienAs"].ToString() : "")),
                                                       LienEmail = ((dr["LienEmail"] != DBNull.Value ? dr["LienEmail"].ToString() : "")),
                                                       LienContactTitle = ((dr["LienContactTitle"] != DBNull.Value ? dr["LienContactTitle"].ToString() : "")),
                                                       TXPrelimGrp = ((dr["TXPrelimGrp"] != DBNull.Value ? Convert.ToInt32(dr["TXPrelimGrp"]) : 0)),
                                                       LienFax = ((dr["LienFax"] != DBNull.Value ? dr["LienFax"].ToString() : "")),
                                                       LienPhone = ((dr["LienPhone"] != DBNull.Value ? dr["LienPhone"].ToString() : "")),
                                                       LegalInterest = ((dr["LegalInterest"] != DBNull.Value ? Convert.ToDecimal(dr["LegalInterest"]) : 0)),
                                                       LienZip = ((dr["LienZip"] != DBNull.Value ? dr["LienZip"].ToString() : "")),
                                                       LienState = ((dr["LienState"] != DBNull.Value ? dr["LienState"].ToString() : "")),
                                                       LienCity = ((dr["LienCity"] != DBNull.Value ? dr["LienCity"].ToString() : "")),
                                                       LienAdd2 = ((dr["LienAdd2"] != DBNull.Value ? dr["LienAdd2"].ToString() : "")),
                                                       LienAdd1 = ((dr["LienAdd1"] != DBNull.Value ? dr["LienAdd1"].ToString() : "")),
                                                       LienContact = ((dr["LienContact"] != DBNull.Value ? dr["LienContact"].ToString() : "")),
                                                       SendNTO = ((dr["SendNTO"] != DBNull.Value ? Convert.ToBoolean(dr["SendNTO"]) : false)),
                                                       SendNTOList = ((dr["SendNTOList"] != DBNull.Value ? Convert.ToBoolean(dr["SendNTOList"]) : false)),
                                                       FLCertBox = ((dr["FLCertBox"] != DBNull.Value ? Convert.ToBoolean(dr["FLCertBox"]) : false)),
                                                       AZCert = ((dr["AZCert"] != DBNull.Value ? Convert.ToBoolean(dr["AZCert"]) : false)),
                                                       ContactName = ((dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : "")),
                                                       NoticePhNum = ((dr["NoticePhNum"] != DBNull.Value ? dr["NoticePhNum"].ToString() : "")),
                                                       CustSend = ((dr["CustSend"] != DBNull.Value ? Convert.ToBoolean(dr["CustSend"]) : false)),
                                                       IsRentalCo = ((dr["IsRentalCo"] != DBNull.Value ? Convert.ToBoolean(dr["IsRentalCo"]) : false)),
                                                       UseBranchNTO = ((dr["UseBranchNTO"] != DBNull.Value ? Convert.ToBoolean(dr["UseBranchNTO"]) : false)),
                                                       CurrDate = ((dr["CurrDate"] != DBNull.Value ? Convert.ToDateTime(dr["CurrDate"]) : DateTime.MinValue)),
                                                       LaborType = ((dr["LaborType"] != DBNull.Value ? dr["LaborType"].ToString() : "")),
                                                       ClientName = ((dr["ClientName"] != DBNull.Value ? dr["ClientName"].ToString() : "")),
                                                       ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                                       Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                                       BatchId = ((dr["BatchId"] != DBNull.Value ? Convert.ToInt32(dr["BatchId"]) : 0)),
                                                       SubmittedOn = ((dr["SubmittedOn"] != DBNull.Value ? Convert.ToDateTime(dr["SubmittedOn"]) : DateTime.MinValue)),
                                                       SubmittedByUserId = ((dr["SubmittedByUserId"] != DBNull.Value ? Convert.ToInt32(dr["SubmittedByUserId"]) : 0)),
                                                       ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
                                                       PlacementSource = ((dr["PlacementSource"] != DBNull.Value ? dr["PlacementSource"].ToString() : "")),
                                                       BranchNum = ((dr["BranchNum"] != DBNull.Value ? dr["BranchNum"].ToString() : "")),
                                                       JobNum = ((dr["JobNum"] != DBNull.Value ? dr["JobNum"].ToString() : "")),
                                                       JobName = ((dr["JobName"] != DBNull.Value ? dr["JobName"].ToString() : "")),
                                                       JobAdd1 = ((dr["JobAdd1"] != DBNull.Value ? dr["JobAdd1"].ToString() : "")),
                                                       JobAdd2 = ((dr["JobAdd2"] != DBNull.Value ? dr["JobAdd2"].ToString() : "")),
                                                       JobCity = ((dr["JobCity"] != DBNull.Value ? dr["JobCity"].ToString() : "")),
                                                       JobState = ((dr["JobState"] != DBNull.Value ? dr["JobState"].ToString() : "")),
                                                       JobZip = ((dr["JobZip"] != DBNull.Value ? dr["JobZip"].ToString() : "")),
                                                       JobCounty = ((dr["JobCounty"] != DBNull.Value ? dr["JobCounty"].ToString() : "")),
                                                       EstBalance = ((dr["EstBalance"] != DBNull.Value ? Convert.ToDecimal(dr["EstBalance"]) : 0)),
                                                       APNNum = ((dr["APNNum"] != DBNull.Value ? dr["APNNum"].ToString() : "")),
                                                       AZLotAllocate = ((dr["AZLotAllocate"] != DBNull.Value ? dr["AZLotAllocate"].ToString() : "")),
                                                       PONum = ((dr["PONum"] != DBNull.Value ? dr["PONum"].ToString() : "")),
                                                       BondNum = ((dr["BondNum"] != DBNull.Value ? dr["BondNum"].ToString() : "")),
                                                       FolioNum = ((dr["FolioNum"] != DBNull.Value ? dr["FolioNum"].ToString() : "")),
                                                       StartDate = ((dr["StartDate"] != DBNull.Value ? Convert.ToDateTime(dr["StartDate"]) : DateTime.MinValue)),
                                                       EndDate = ((dr["EndDate"] != DBNull.Value ? Convert.ToDateTime(dr["EndDate"]) : DateTime.MinValue)),
                                                       RevDate = ((dr["RevDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevDate"]) : DateTime.MinValue)),
                                                       SpecialInstruction = ((dr["SpecialInstruction"] != DBNull.Value ? dr["SpecialInstruction"].ToString() : "")),
                                                       EquipRate = ((dr["EquipRate"] != DBNull.Value ? dr["EquipRate"].ToString() : "")),
                                                       EquipRental = ((dr["EquipRental"] != DBNull.Value ? dr["EquipRental"].ToString() : "")),
                                                       Note = ((dr["Note"] != DBNull.Value ? dr["Note"].ToString() : "")),
                                                       CustId = ((dr["CustId"] != DBNull.Value ? Convert.ToInt32(dr["CustId"]) : 0)),
                                                       CustJobNum = ((dr["CustJobNum"] != DBNull.Value ? dr["CustJobNum"].ToString() : "")),
                                                       CustRefNum = ((dr["CustRefNum"] != DBNull.Value ? dr["CustRefNum"].ToString() : "")),
                                                       CustName = ((dr["CustName"] != DBNull.Value ? dr["CustName"].ToString() : "")),
                                                       CustContactName = ((dr["CustContactName"] != DBNull.Value ? dr["CustContactName"].ToString() : "")),
                                                       CustAdd1 = ((dr["CustAdd1"] != DBNull.Value ? dr["CustAdd1"].ToString() : "")),
                                                       CustAdd2 = ((dr["CustAdd2"] != DBNull.Value ? dr["CustAdd2"].ToString() : "")),
                                                       CustCity = ((dr["CustCity"] != DBNull.Value ? dr["CustCity"].ToString() : "")),
                                                       CustState = ((dr["CustState"] != DBNull.Value ? dr["CustState"].ToString() : "")),
                                                       CustZip = ((dr["CustZip"] != DBNull.Value ? dr["CustZip"].ToString() : "")),
                                                       CustPhone1 = ((dr["CustPhone1"] != DBNull.Value ? dr["CustPhone1"].ToString() : "")),
                                                       CustPhone2 = ((dr["CustPhone2"] != DBNull.Value ? dr["CustPhone2"].ToString() : "")),
                                                       CustFax = ((dr["CustFax"] != DBNull.Value ? dr["CustFax"].ToString() : "")),
                                                       CustEmail = ((dr["CustEmail"] != DBNull.Value ? dr["CustEmail"].ToString() : "")),
                                                       CustMatchingKey = ((dr["CustMatchingKey"] != DBNull.Value ? dr["CustMatchingKey"].ToString() : "")),
                                                       GenId = ((dr["GenId"] != DBNull.Value ? Convert.ToInt32(dr["GenId"]) : 0)),
                                                       GCRefNum = ((dr["GCRefNum"] != DBNull.Value ? dr["GCRefNum"].ToString() : "")),
                                                       GCName = ((dr["GCName"] != DBNull.Value ? dr["GCName"].ToString() : "")),
                                                       GCContactName = ((dr["GCContactName"] != DBNull.Value ? dr["GCContactName"].ToString() : "")),
                                                       GCAdd1 = ((dr["GCAdd1"] != DBNull.Value ? dr["GCAdd1"].ToString() : "")),
                                                       GCAdd2 = ((dr["GCAdd2"] != DBNull.Value ? dr["GCAdd2"].ToString() : "")),
                                                       GCCity = ((dr["GCCity"] != DBNull.Value ? dr["GCCity"].ToString() : "")),
                                                       GCState = ((dr["GCState"] != DBNull.Value ? dr["GCState"].ToString() : "")),
                                                       GCZip = ((dr["GCZip"] != DBNull.Value ? dr["GCZip"].ToString() : "")),
                                                       GCPhone1 = ((dr["GCPhone1"] != DBNull.Value ? dr["GCPhone1"].ToString() : "")),
                                                       GCPhone2 = ((dr["GCPhone2"] != DBNull.Value ? dr["GCPhone2"].ToString() : "")),
                                                       GCFax = ((dr["GCFax"] != DBNull.Value ? dr["GCFax"].ToString() : "")),
                                                       GCEmail = ((dr["GCEmail"] != DBNull.Value ? dr["GCEmail"].ToString() : "")),
                                                       GCMatchingKey = ((dr["GCMatchingKey"] != DBNull.Value ? dr["GCMatchingKey"].ToString() : "")),
                                                       OwnrName = ((dr["OwnrName"] != DBNull.Value ? dr["OwnrName"].ToString() : "")),
                                                       OwnrAdd1 = ((dr["OwnrAdd1"] != DBNull.Value ? dr["OwnrAdd1"].ToString() : "")),
                                                       OwnrAdd2 = ((dr["OwnrAdd2"] != DBNull.Value ? dr["OwnrAdd2"].ToString() : "")),
                                                       OwnrCity = ((dr["OwnrCity"] != DBNull.Value ? dr["OwnrCity"].ToString() : "")),
                                                       OwnrState = ((dr["OwnrState"] != DBNull.Value ? dr["OwnrState"].ToString() : "")),
                                                       OwnrZip = ((dr["OwnrZip"] != DBNull.Value ? dr["OwnrZip"].ToString() : "")),
                                                       OwnrPhone1 = ((dr["OwnrPhone1"] != DBNull.Value ? dr["OwnrPhone1"].ToString() : "")),
                                                       OwnrPhone2 = ((dr["OwnrPhone2"] != DBNull.Value ? dr["OwnrPhone2"].ToString() : "")),
                                                       OwnrFax = ((dr["OwnrFax"] != DBNull.Value ? dr["OwnrFax"].ToString() : "")),
                                                       LenderName = ((dr["LenderName"] != DBNull.Value ? dr["LenderName"].ToString() : "")),
                                                       LenderAdd1 = ((dr["LenderAdd1"] != DBNull.Value ? dr["LenderAdd1"].ToString() : "")),
                                                       LenderAdd2 = ((dr["LenderAdd2"] != DBNull.Value ? dr["LenderAdd2"].ToString() : "")),
                                                       LenderCity = ((dr["LenderCity"] != DBNull.Value ? dr["LenderCity"].ToString() : "")),
                                                       LenderState = ((dr["LenderState"] != DBNull.Value ? dr["LenderState"].ToString() : "")),
                                                       LenderZip = ((dr["LenderZip"] != DBNull.Value ? dr["LenderZip"].ToString() : "")),
                                                       LenderPhone1 = ((dr["LenderPhone1"] != DBNull.Value ? dr["LenderPhone1"].ToString() : "")),
                                                       LenderPhone2 = ((dr["LenderPhone2"] != DBNull.Value ? dr["LenderPhone2"].ToString() : "")),
                                                       LenderFax = ((dr["LenderFax"] != DBNull.Value ? dr["LenderFax"].ToString() : "")),
                                                       DesigneeName = ((dr["DesigneeName"] != DBNull.Value ? dr["DesigneeName"].ToString() : "")),
                                                       DesigneeAdd1 = ((dr["DesigneeAdd1"] != DBNull.Value ? dr["DesigneeAdd1"].ToString() : "")),
                                                       DesigneeAdd2 = ((dr["DesigneeAdd2"] != DBNull.Value ? dr["DesigneeAdd2"].ToString() : "")),
                                                       DesigneeCity = ((dr["DesigneeCity"] != DBNull.Value ? dr["DesigneeCity"].ToString() : "")),
                                                       DesigneeState = ((dr["DesigneeState"] != DBNull.Value ? dr["DesigneeState"].ToString() : "")),
                                                       DesigneeZip = ((dr["DesigneeZip"] != DBNull.Value ? dr["DesigneeZip"].ToString() : "")),
                                                       DesigneePhone1 = ((dr["DesigneePhone1"] != DBNull.Value ? dr["DesigneePhone1"].ToString() : "")),
                                                       DesigneePhone2 = ((dr["DesigneePhone2"] != DBNull.Value ? dr["DesigneePhone2"].ToString() : "")),
                                                       DesigneeFax = ((dr["DesigneeFax"] != DBNull.Value ? dr["DesigneeFax"].ToString() : "")),
                                                       SameAsCust = ((dr["SameAsCust"] != DBNull.Value ? Convert.ToBoolean(dr["SameAsCust"]) : false)),
                                                       SameAsGC = ((dr["SameAsGC"] != DBNull.Value ? Convert.ToBoolean(dr["SameAsGC"]) : false)),
                                                       PrelimASIS = ((dr["PrelimASIS"] != DBNull.Value ? Convert.ToBoolean(dr["PrelimASIS"]) : false)),
                                                       PrivateJob = ((dr["PrivateJob"] != DBNull.Value ? Convert.ToBoolean(dr["PrivateJob"]) : false)),
                                                       RushOrderML = ((dr["RushOrderML"] != DBNull.Value ? Convert.ToBoolean(dr["RushOrderML"]) : false)),
                                                       MechanicLien = ((dr["MechanicLien"] != DBNull.Value ? Convert.ToBoolean(dr["MechanicLien"]) : false)),
                                                       StopNoticeBox = ((dr["StopNoticeBox"] != DBNull.Value ? Convert.ToBoolean(dr["StopNoticeBox"]) : false)),
                                                       PrelimBox = ((dr["PrelimBox"] != DBNull.Value ? Convert.ToBoolean(dr["PrelimBox"]) : false)),
                                                       VerifyJob = ((dr["VerifyJob"] != DBNull.Value ? Convert.ToBoolean(dr["VerifyJob"]) : false)),
                                                       TitleVerifiedBox = ((dr["TitleVerifiedBox"] != DBNull.Value ? Convert.ToBoolean(dr["TitleVerifiedBox"]) : false)),
                                                       RushOrder = ((dr["RushOrder"] != DBNull.Value ? Convert.ToBoolean(dr["RushOrder"]) : false)),
                                                       GreenCard = ((dr["GreenCard"] != DBNull.Value ? Convert.ToBoolean(dr["GreenCard"]) : false)),
                                                       PublicJob = ((dr["PublicJob"] != DBNull.Value ? Convert.ToBoolean(dr["PublicJob"]) : false)),
                                                       ResidentialBox = ((dr["ResidentialBox"] != DBNull.Value ? Convert.ToBoolean(dr["ResidentialBox"]) : false)),
                                                       JointCk = ((dr["JointCk"] != DBNull.Value ? Convert.ToBoolean(dr["JointCk"]) : false)),
                                                       FederalJob = ((dr["FederalJob"] != DBNull.Value ? Convert.ToBoolean(dr["FederalJob"]) : false)),
                                                       LienRelease = ((dr["LienRelease"] != DBNull.Value ? Convert.ToBoolean(dr["LienRelease"]) : false)),
                                                       PaymentBond = ((dr["PaymentBond"] != DBNull.Value ? Convert.ToBoolean(dr["PaymentBond"]) : false)),
                                                       IsError = ((dr["IsError"] != DBNull.Value ? Convert.ToBoolean(dr["IsError"]) : false)),
                                                       IsWarning = ((dr["IsWarning"] != DBNull.Value ? Convert.ToBoolean(dr["IsWarning"]) : false)),
                                                       NCInfo = ((dr["NCInfo"] != DBNull.Value ? Convert.ToBoolean(dr["NCInfo"]) : false)),
                                                       ErrorDescription = ((dr["ErrorDescription"] != DBNull.Value ? dr["ErrorDescription"].ToString() : "")),
                                                       DeskNumId = ((dr["DeskNumId"] != DBNull.Value ? Convert.ToInt32(dr["DeskNumId"]) : 0)),
                                                       StatusCodeId = ((dr["StatusCodeId"] != DBNull.Value ? Convert.ToInt32(dr["StatusCodeId"]) : 0)),
                                                       JobActionId = ((dr["JobActionId"] != DBNull.Value ? Convert.ToInt32(dr["JobActionId"]) : 0)),
                                                       NoticeSent = ((dr["NoticeSent"] != DBNull.Value ? Convert.ToDateTime(dr["NoticeSent"]) : DateTime.MinValue)),
                                                       VerifiedDate = ((dr["VerifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["VerifiedDate"]) : DateTime.MinValue)),
                                                       AddressLine1 = ((dr["AddressLine1"] != DBNull.Value ? dr["AddressLine1"].ToString() : "")),
                                                       OwnerName = ((dr["OwnerName"] != DBNull.Value ? dr["OwnerName"].ToString() : "")),
                                                       UpdatedOn = ((dr["UpdatedOn"] != DBNull.Value ? Convert.ToDateTime(dr["UpdatedOn"]) : DateTime.MinValue)),
                                                       UserName = ((dr["UserName"] != DBNull.Value ? dr["UserName"].ToString() : "")),
                                                       VerifyOWOnly = ((dr["VerifyOWOnly"] != DBNull.Value ? Convert.ToBoolean(dr["VerifyOWOnly"]) : false)),
                                                       VerifyOWOnlyTX = ((dr["VerifyOWOnlyTX"] != DBNull.Value ? Convert.ToBoolean(dr["VerifyOWOnlyTX"]) : false)),
                                                       VerifyOWOnlySend = ((dr["VerifyOWOnlySend"] != DBNull.Value ? Convert.ToBoolean(dr["VerifyOWOnlySend"]) : false)),
                                                   }).ToList();
                return BatchJobList;
            }
            catch (Exception ex)
            {
                return (new List<ADvwBatchJobs>());
            }
        }

        public static List<ADvwBatchJobs> GetFilteredList(FilterVM objFilterVM, string sortBy)
        {
            try
            {
                TableView EntityList;
                //uspbo_LiensDataEntry_GetJobsInBatches_bckup_12_17_2021
                var sp_obj = new uspbo_LiensDataEntry_GetJobsInBatches();
                if (objFilterVM != null)
                {
                    if (objFilterVM.FilterObj.JobName != null && objFilterVM.FilterObj.JobName.Trim().Length > 0)
                    {
                        sp_obj.JobName = "%" + objFilterVM.FilterObj.JobName.Trim() + "%";
                    }
                    if (objFilterVM.FilterObj.JobNumber != null && objFilterVM.FilterObj.JobNumber.Trim().Length > 0)
                    {
                        sp_obj.JobNum = "%" + objFilterVM.FilterObj.JobNumber.Trim() + "%";
                    }
                }

                if (string.IsNullOrEmpty(sortBy) == false)
                {
                    sp_obj.SortColType = sortBy;
                }
                sp_obj.ErrorsOnly = false;
                if(objFilterVM.FilterObj.ddlBatchId == null)
                {
                    sp_obj.BatchFROM = Convert.ToString(objFilterVM.BatchIdList.FirstOrDefault().Value);
                    sp_obj.BatchTO = Convert.ToString(objFilterVM.BatchIdList.FirstOrDefault().Value);
                }
                else
                {
                    sp_obj.BatchFROM = Convert.ToString(objFilterVM.FilterObj.ddlBatchId);
                    sp_obj.BatchTO = Convert.ToString(objFilterVM.FilterObj.ddlBatchId);
                }
               
                sp_obj.SelectByBatch = true;

                EntityList = DBO.Provider.GetTableView(sp_obj);
                DataTable dt = EntityList.ToTable();
                List<ADvwBatchJobs> BatchJobList = (from DataRow dr in dt.Rows
                                                    select new ADvwBatchJobs()
                                                    {
                                                        FileDate = ((dr["FileDate"] != DBNull.Value ? Convert.ToDateTime(dr["FileDate"]) : DateTime.MinValue)),
                                                        BatchStatus = ((dr["BatchStatus"] != DBNull.Value ? dr["BatchStatus"].ToString() : "")),
                                                        ProcessOn = ((dr["ProcessOn"] != DBNull.Value ? Convert.ToDateTime(dr["ProcessOn"]) : DateTime.MinValue)),
                                                        BatchType = ((dr["BatchType"] != DBNull.Value ? dr["BatchType"].ToString() : "")),
                                                        BatchDate = ((dr["BatchDate"] != DBNull.Value ? Convert.ToDateTime(dr["BatchDate"]) : DateTime.MinValue)),
                                                        OtherLglThreshold = ((dr["OtherLglThreshold"] != DBNull.Value ? Convert.ToInt32(dr["OtherLglThreshold"]) : 0)),
                                                        SendAck = ((dr["SendAck"] != DBNull.Value ? Convert.ToBoolean(dr["SendAck"]) : false)),
                                                        SendOIR = ((dr["SendOIR"] != DBNull.Value ? Convert.ToBoolean(dr["SendOIR"]) : false)),
                                                        BranchAs = ((dr["BranchAs"] != DBNull.Value ? dr["BranchAs"].ToString() : "")),
                                                        NTOAs = ((dr["NTOAs"] != DBNull.Value ? dr["NTOAs"].ToString() : "")),
                                                        LienAs = ((dr["LienAs"] != DBNull.Value ? dr["LienAs"].ToString() : "")),
                                                        LienEmail = ((dr["LienEmail"] != DBNull.Value ? dr["LienEmail"].ToString() : "")),
                                                        LienContactTitle = ((dr["LienContactTitle"] != DBNull.Value ? dr["LienContactTitle"].ToString() : "")),
                                                        TXPrelimGrp = ((dr["TXPrelimGrp"] != DBNull.Value ? Convert.ToInt32(dr["TXPrelimGrp"]) : 0)),
                                                        LienFax = ((dr["LienFax"] != DBNull.Value ? dr["LienFax"].ToString() : "")),
                                                        LienPhone = ((dr["LienPhone"] != DBNull.Value ? dr["LienPhone"].ToString() : "")),
                                                        LegalInterest = ((dr["LegalInterest"] != DBNull.Value ? Convert.ToDecimal(dr["LegalInterest"]) : 0)),
                                                        LienZip = ((dr["LienZip"] != DBNull.Value ? dr["LienZip"].ToString() : "")),
                                                        LienState = ((dr["LienState"] != DBNull.Value ? dr["LienState"].ToString() : "")),
                                                        LienCity = ((dr["LienCity"] != DBNull.Value ? dr["LienCity"].ToString() : "")),
                                                        LienAdd2 = ((dr["LienAdd2"] != DBNull.Value ? dr["LienAdd2"].ToString() : "")),
                                                        LienAdd1 = ((dr["LienAdd1"] != DBNull.Value ? dr["LienAdd1"].ToString() : "")),
                                                        LienContact = ((dr["LienContact"] != DBNull.Value ? dr["LienContact"].ToString() : "")),
                                                        SendNTO = ((dr["SendNTO"] != DBNull.Value ? Convert.ToBoolean(dr["SendNTO"]) : false)),
                                                        SendNTOList = ((dr["SendNTOList"] != DBNull.Value ? Convert.ToBoolean(dr["SendNTOList"]) : false)),
                                                        FLCertBox = ((dr["FLCertBox"] != DBNull.Value ? Convert.ToBoolean(dr["FLCertBox"]) : false)),
                                                        AZCert = ((dr["AZCert"] != DBNull.Value ? Convert.ToBoolean(dr["AZCert"]) : false)),
                                                        ContactName = ((dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : "")),
                                                        NoticePhNum = ((dr["NoticePhNum"] != DBNull.Value ? dr["NoticePhNum"].ToString() : "")),
                                                        CustSend = ((dr["CustSend"] != DBNull.Value ? Convert.ToBoolean(dr["CustSend"]) : false)),
                                                        IsRentalCo = ((dr["IsRentalCo"] != DBNull.Value ? Convert.ToBoolean(dr["IsRentalCo"]) : false)),
                                                        UseBranchNTO = ((dr["UseBranchNTO"] != DBNull.Value ? Convert.ToBoolean(dr["UseBranchNTO"]) : false)),
                                                        CurrDate = ((dr["CurrDate"] != DBNull.Value ? Convert.ToDateTime(dr["CurrDate"]) : DateTime.MinValue)),
                                                        LaborType = ((dr["LaborType"] != DBNull.Value ? dr["LaborType"].ToString() : "")),
                                                        ClientName = ((dr["ClientName"] != DBNull.Value ? dr["ClientName"].ToString() : "")),
                                                        ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                                        Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                                        BatchId = ((dr["BatchId"] != DBNull.Value ? Convert.ToInt32(dr["BatchId"]) : 0)),
                                                        SubmittedOn = ((dr["SubmittedOn"] != DBNull.Value ? Convert.ToDateTime(dr["SubmittedOn"]) : DateTime.MinValue)),
                                                        SubmittedByUserId = ((dr["SubmittedByUserId"] != DBNull.Value ? Convert.ToInt32(dr["SubmittedByUserId"]) : 0)),
                                                        ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
                                                        PlacementSource = ((dr["PlacementSource"] != DBNull.Value ? dr["PlacementSource"].ToString() : "")),
                                                        BranchNum = ((dr["BranchNum"] != DBNull.Value ? dr["BranchNum"].ToString() : "")),
                                                        JobNum = ((dr["JobNum"] != DBNull.Value ? dr["JobNum"].ToString() : "")),
                                                        JobName = ((dr["JobName"] != DBNull.Value ? dr["JobName"].ToString() : "")),
                                                        JobAdd1 = ((dr["JobAdd1"] != DBNull.Value ? dr["JobAdd1"].ToString() : "")),
                                                        JobAdd2 = ((dr["JobAdd2"] != DBNull.Value ? dr["JobAdd2"].ToString() : "")),
                                                        JobCity = ((dr["JobCity"] != DBNull.Value ? dr["JobCity"].ToString() : "")),
                                                        JobState = ((dr["JobState"] != DBNull.Value ? dr["JobState"].ToString() : "")),
                                                        JobZip = ((dr["JobZip"] != DBNull.Value ? dr["JobZip"].ToString() : "")),
                                                        JobCounty = ((dr["JobCounty"] != DBNull.Value ? dr["JobCounty"].ToString() : "")),
                                                        EstBalance = ((dr["EstBalance"] != DBNull.Value ? Convert.ToDecimal(dr["EstBalance"]) : 0)),
                                                        APNNum = ((dr["APNNum"] != DBNull.Value ? dr["APNNum"].ToString() : "")),
                                                        AZLotAllocate = ((dr["AZLotAllocate"] != DBNull.Value ? dr["AZLotAllocate"].ToString() : "")),
                                                        PONum = ((dr["PONum"] != DBNull.Value ? dr["PONum"].ToString() : "")),
                                                        BondNum = ((dr["BondNum"] != DBNull.Value ? dr["BondNum"].ToString() : "")),
                                                        FolioNum = ((dr["FolioNum"] != DBNull.Value ? dr["FolioNum"].ToString() : "")),
                                                        StartDate = ((dr["StartDate"] != DBNull.Value ? Convert.ToDateTime(dr["StartDate"]) : DateTime.MinValue)),
                                                        EndDate = ((dr["EndDate"] != DBNull.Value ? Convert.ToDateTime(dr["EndDate"]) : DateTime.MinValue)),
                                                        RevDate = ((dr["RevDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevDate"]) : DateTime.MinValue)),
                                                        SpecialInstruction = ((dr["SpecialInstruction"] != DBNull.Value ? dr["SpecialInstruction"].ToString() : "")),
                                                        EquipRate = ((dr["EquipRate"] != DBNull.Value ? dr["EquipRate"].ToString() : "")),
                                                        EquipRental = ((dr["EquipRental"] != DBNull.Value ? dr["EquipRental"].ToString() : "")),
                                                        Note = ((dr["Note"] != DBNull.Value ? dr["Note"].ToString() : "")),
                                                        CustId = ((dr["CustId"] != DBNull.Value ? Convert.ToInt32(dr["CustId"]) : 0)),
                                                        CustJobNum = ((dr["CustJobNum"] != DBNull.Value ? dr["CustJobNum"].ToString() : "")),
                                                        CustRefNum = ((dr["CustRefNum"] != DBNull.Value ? dr["CustRefNum"].ToString() : "")),
                                                        CustName = ((dr["CustName"] != DBNull.Value ? dr["CustName"].ToString() : "")),
                                                        CustContactName = ((dr["CustContactName"] != DBNull.Value ? dr["CustContactName"].ToString() : "")),
                                                        CustAdd1 = ((dr["CustAdd1"] != DBNull.Value ? dr["CustAdd1"].ToString() : "")),
                                                        CustAdd2 = ((dr["CustAdd2"] != DBNull.Value ? dr["CustAdd2"].ToString() : "")),
                                                        CustCity = ((dr["CustCity"] != DBNull.Value ? dr["CustCity"].ToString() : "")),
                                                        CustState = ((dr["CustState"] != DBNull.Value ? dr["CustState"].ToString() : "")),
                                                        CustZip = ((dr["CustZip"] != DBNull.Value ? dr["CustZip"].ToString() : "")),
                                                        CustPhone1 = ((dr["CustPhone1"] != DBNull.Value ? dr["CustPhone1"].ToString() : "")),
                                                        CustPhone2 = ((dr["CustPhone2"] != DBNull.Value ? dr["CustPhone2"].ToString() : "")),
                                                        CustFax = ((dr["CustFax"] != DBNull.Value ? dr["CustFax"].ToString() : "")),
                                                        CustMatchingKey = ((dr["CustMatchingKey"] != DBNull.Value ? dr["CustMatchingKey"].ToString() : "")),
                                                        GenId = ((dr["GenId"] != DBNull.Value ? Convert.ToInt32(dr["GenId"]) : 0)),
                                                        GCRefNum = ((dr["GCRefNum"] != DBNull.Value ? dr["GCRefNum"].ToString() : "")),
                                                        GCName = ((dr["GCName"] != DBNull.Value ? dr["GCName"].ToString() : "")),
                                                        GCContactName = ((dr["GCContactName"] != DBNull.Value ? dr["GCContactName"].ToString() : "")),
                                                        GCAdd1 = ((dr["GCAdd1"] != DBNull.Value ? dr["GCAdd1"].ToString() : "")),
                                                        GCAdd2 = ((dr["GCAdd2"] != DBNull.Value ? dr["GCAdd2"].ToString() : "")),
                                                        GCCity = ((dr["GCCity"] != DBNull.Value ? dr["GCCity"].ToString() : "")),
                                                        GCState = ((dr["GCState"] != DBNull.Value ? dr["GCState"].ToString() : "")),
                                                        GCZip = ((dr["GCZip"] != DBNull.Value ? dr["GCZip"].ToString() : "")),
                                                        GCPhone1 = ((dr["GCPhone1"] != DBNull.Value ? dr["GCPhone1"].ToString() : "")),
                                                        GCPhone2 = ((dr["GCPhone2"] != DBNull.Value ? dr["GCPhone2"].ToString() : "")),
                                                        GCFax = ((dr["GCFax"] != DBNull.Value ? dr["GCFax"].ToString() : "")),
                                                        GCMatchingKey = ((dr["GCMatchingKey"] != DBNull.Value ? dr["GCMatchingKey"].ToString() : "")),
                                                        OwnrName = ((dr["OwnrName"] != DBNull.Value ? dr["OwnrName"].ToString() : "")),
                                                        OwnrAdd1 = ((dr["OwnrAdd1"] != DBNull.Value ? dr["OwnrAdd1"].ToString() : "")),
                                                        OwnrAdd2 = ((dr["OwnrAdd2"] != DBNull.Value ? dr["OwnrAdd2"].ToString() : "")),
                                                        OwnrCity = ((dr["OwnrCity"] != DBNull.Value ? dr["OwnrCity"].ToString() : "")),
                                                        OwnrState = ((dr["OwnrState"] != DBNull.Value ? dr["OwnrState"].ToString() : "")),
                                                        OwnrZip = ((dr["OwnrZip"] != DBNull.Value ? dr["OwnrZip"].ToString() : "")),
                                                        OwnrPhone1 = ((dr["OwnrPhone1"] != DBNull.Value ? dr["OwnrPhone1"].ToString() : "")),
                                                        OwnrPhone2 = ((dr["OwnrPhone2"] != DBNull.Value ? dr["OwnrPhone2"].ToString() : "")),
                                                        OwnrFax = ((dr["OwnrFax"] != DBNull.Value ? dr["OwnrFax"].ToString() : "")),
                                                        LenderName = ((dr["LenderName"] != DBNull.Value ? dr["LenderName"].ToString() : "")),
                                                        LenderAdd1 = ((dr["LenderAdd1"] != DBNull.Value ? dr["LenderAdd1"].ToString() : "")),
                                                        LenderAdd2 = ((dr["LenderAdd2"] != DBNull.Value ? dr["LenderAdd2"].ToString() : "")),
                                                        LenderCity = ((dr["LenderCity"] != DBNull.Value ? dr["LenderCity"].ToString() : "")),
                                                        LenderState = ((dr["LenderState"] != DBNull.Value ? dr["LenderState"].ToString() : "")),
                                                        LenderZip = ((dr["LenderZip"] != DBNull.Value ? dr["LenderZip"].ToString() : "")),
                                                        LenderPhone1 = ((dr["LenderPhone1"] != DBNull.Value ? dr["LenderPhone1"].ToString() : "")),
                                                        LenderPhone2 = ((dr["LenderPhone2"] != DBNull.Value ? dr["LenderPhone2"].ToString() : "")),
                                                        LenderFax = ((dr["LenderFax"] != DBNull.Value ? dr["LenderFax"].ToString() : "")),
                                                        DesigneeName = ((dr["DesigneeName"] != DBNull.Value ? dr["DesigneeName"].ToString() : "")),
                                                        DesigneeAdd1 = ((dr["DesigneeAdd1"] != DBNull.Value ? dr["DesigneeAdd1"].ToString() : "")),
                                                        DesigneeAdd2 = ((dr["DesigneeAdd2"] != DBNull.Value ? dr["DesigneeAdd2"].ToString() : "")),
                                                        DesigneeCity = ((dr["DesigneeCity"] != DBNull.Value ? dr["DesigneeCity"].ToString() : "")),
                                                        DesigneeState = ((dr["DesigneeState"] != DBNull.Value ? dr["DesigneeState"].ToString() : "")),
                                                        DesigneeZip = ((dr["DesigneeZip"] != DBNull.Value ? dr["DesigneeZip"].ToString() : "")),
                                                        DesigneePhone1 = ((dr["DesigneePhone1"] != DBNull.Value ? dr["DesigneePhone1"].ToString() : "")),
                                                        DesigneePhone2 = ((dr["DesigneePhone2"] != DBNull.Value ? dr["DesigneePhone2"].ToString() : "")),
                                                        DesigneeFax = ((dr["DesigneeFax"] != DBNull.Value ? dr["DesigneeFax"].ToString() : "")),
                                                        SameAsCust = ((dr["SameAsCust"] != DBNull.Value ? Convert.ToBoolean(dr["SameAsCust"]) : false)),
                                                        SameAsGC = ((dr["SameAsGC"] != DBNull.Value ? Convert.ToBoolean(dr["SameAsGC"]) : false)),
                                                        PrelimASIS = ((dr["PrelimASIS"] != DBNull.Value ? Convert.ToBoolean(dr["PrelimASIS"]) : false)),
                                                        PrivateJob = ((dr["PrivateJob"] != DBNull.Value ? Convert.ToBoolean(dr["PrivateJob"]) : false)),
                                                        RushOrderML = ((dr["RushOrderML"] != DBNull.Value ? Convert.ToBoolean(dr["RushOrderML"]) : false)),
                                                        MechanicLien = ((dr["MechanicLien"] != DBNull.Value ? Convert.ToBoolean(dr["MechanicLien"]) : false)),
                                                        StopNoticeBox = ((dr["StopNoticeBox"] != DBNull.Value ? Convert.ToBoolean(dr["StopNoticeBox"]) : false)),
                                                        PrelimBox = ((dr["PrelimBox"] != DBNull.Value ? Convert.ToBoolean(dr["PrelimBox"]) : false)),
                                                        VerifyJob = ((dr["VerifyJob"] != DBNull.Value ? Convert.ToBoolean(dr["VerifyJob"]) : false)),
                                                        TitleVerifiedBox = ((dr["TitleVerifiedBox"] != DBNull.Value ? Convert.ToBoolean(dr["TitleVerifiedBox"]) : false)),
                                                        RushOrder = ((dr["RushOrder"] != DBNull.Value ? Convert.ToBoolean(dr["RushOrder"]) : false)),
                                                        GreenCard = ((dr["GreenCard"] != DBNull.Value ? Convert.ToBoolean(dr["GreenCard"]) : false)),
                                                        PublicJob = ((dr["PublicJob"] != DBNull.Value ? Convert.ToBoolean(dr["PublicJob"]) : false)),
                                                        ResidentialBox = ((dr["ResidentialBox"] != DBNull.Value ? Convert.ToBoolean(dr["ResidentialBox"]) : false)),
                                                        JointCk = ((dr["JointCk"] != DBNull.Value ? Convert.ToBoolean(dr["JointCk"]) : false)),
                                                        FederalJob = ((dr["FederalJob"] != DBNull.Value ? Convert.ToBoolean(dr["FederalJob"]) : false)),
                                                        LienRelease = ((dr["LienRelease"] != DBNull.Value ? Convert.ToBoolean(dr["LienRelease"]) : false)),
                                                        PaymentBond = ((dr["PaymentBond"] != DBNull.Value ? Convert.ToBoolean(dr["PaymentBond"]) : false)),
                                                        IsError = ((dr["IsError"] != DBNull.Value ? Convert.ToBoolean(dr["IsError"]) : false)),
                                                        IsWarning = ((dr["IsWarning"] != DBNull.Value ? Convert.ToBoolean(dr["IsWarning"]) : false)),
                                                        NCInfo = ((dr["NCInfo"] != DBNull.Value ? Convert.ToBoolean(dr["NCInfo"]) : false)),
                                                        ErrorDescription = ((dr["ErrorDescription"] != DBNull.Value ? dr["ErrorDescription"].ToString() : "")),
                                                        DeskNumId = ((dr["DeskNumId"] != DBNull.Value ? Convert.ToInt32(dr["DeskNumId"]) : 0)),
                                                        StatusCodeId = ((dr["StatusCodeId"] != DBNull.Value ? Convert.ToInt32(dr["StatusCodeId"]) : 0)),
                                                        JobActionId = ((dr["JobActionId"] != DBNull.Value ? Convert.ToInt32(dr["JobActionId"]) : 0)),
                                                        NoticeSent = ((dr["NoticeSent"] != DBNull.Value ? Convert.ToDateTime(dr["NoticeSent"]) : DateTime.MinValue)),
                                                        VerifiedDate = ((dr["VerifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["VerifiedDate"]) : DateTime.MinValue)),
                                                        AddressLine1 = ((dr["AddressLine1"] != DBNull.Value ? dr["AddressLine1"].ToString() : "")),
                                                        OwnerName = ((dr["OwnerName"] != DBNull.Value ? dr["OwnerName"].ToString() : "")),
                                                        UpdatedOn = ((dr["UpdatedOn"] != DBNull.Value ? Convert.ToDateTime(dr["UpdatedOn"]) : DateTime.MinValue)),
                                                        UserName = ((dr["UserName"] != DBNull.Value ? dr["UserName"].ToString() : "")),
                                                        VerifyOWOnly = ((dr["VerifyOWOnly"] != DBNull.Value ? Convert.ToBoolean(dr["VerifyOWOnly"]) : false)),
                                                        VerifyOWOnlyTX = ((dr["VerifyOWOnlyTX"] != DBNull.Value ? Convert.ToBoolean(dr["VerifyOWOnlyTX"]) : false)),
                                                        VerifyOWOnlySend = ((dr["VerifyOWOnlySend"] != DBNull.Value ? Convert.ToBoolean(dr["VerifyOWOnlySend"]) : false)),
                                                    }).ToList();
                return BatchJobList;
            }
            catch (Exception ex)
            {
                return (new List<ADvwBatchJobs>());
            }
        }

        #endregion
    }
}
