﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.Providers;
using CRFView.Adapters.Liens.DataEntry.DataEntry.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.DataEntry.DataEntry
{
    public class BatchJobInfo
    {
        #region Properties()
        #endregion

        #region CRUD
        public static BatchJobVM LoadInfo(string Id)
        {
            var myds = new DataSet();
            var mysproc = new uspbo_LiensDataEntry_GetNewJobInfo();
            mysproc.BatchJobId = Id;
            myds = DBO.Provider.GetDataSet(mysproc);
            BatchJobVM objBatchJobVM = new BatchJobVM();

            //BatchJob Table
            DataTable dtBatchJob = myds.Tables[0];
            objBatchJobVM.objBatchJob = ADBatchJob.GetBatchJobInfo(dtBatchJob);


            //BatchJobLegalParties Table
            DataTable dtBatchJobLegalParties = myds.Tables[1];  
            objBatchJobVM.objBatchJobLegalParties = ADBatchJobLegalParties.GetBatchJobLegalParties(dtBatchJobLegalParties);
            objBatchJobVM.objBatchJobLegalPartiesList = ADBatchJobLegalParties.BatchJobLegalPartiesList(dtBatchJobLegalParties);

            //StateInfo Table
            DataTable dtStateInfo = myds.Tables[2];
            objBatchJobVM.objStateInfo = ADStateInfo.GetStateInfo(dtStateInfo);

            //Client Table
            DataTable dtClient = myds.Tables[3];
            objBatchJobVM.objClient = ADClient.GetClient(dtClient);

            //ClientLienInfo Table
            DataTable dtClientLienInfo = myds.Tables[4];
            objBatchJobVM.objClientLienInfo = ADClientLienInfo.GetClientLienInfo(dtClientLienInfo);

            //Job Actions
            objBatchJobVM.objJobActions = ADJobActions.ReadJobAction(objBatchJobVM.objBatchJob.JobActionId);

            //Job Attachments
            objBatchJobVM.objJobAttachmentsList = ADJobAttachments.GetJobAttachments(Id);

            return objBatchJobVM;
        }

        #endregion

    }
}
