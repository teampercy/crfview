﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.DataEntry.DataEntry
{
    public class Filter
    {
        public Filter()
        {

        }

        #region Properties

        public string JobName { get; set; }
        public string JobNumber { get; set; }
        public string ddlBatchId { get; set; }
        public bool chkErrors { get; set; }

        #endregion

    }
}
