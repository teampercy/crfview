﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADZipCode
    {
        #region Properties
 public int     ZIPCodeId { get; set; }  
public string ZIPCode             { get; set; }
public string ZIPType             { get; set; }
public string CityName            { get; set; }
public string CityType            { get; set; }
public string CountyName        { get; set; }
public string CountyFIPS        { get; set; }
public string StateName          { get; set; }
public string StateAbbr          { get; set; }
public string StateFIPS          { get; set; }
public string MSACode           { get; set; }
public string AreaCode            { get; set; }
public string TimeZone            { get; set; }
public string UTC                   { get; set; }
public string DST                   { get; set; }
public string Latitude              { get; set; }
public string Longitude { get; set; }
        #endregion

        #region CRUD
        public static ADZipCode ReadZipCode(int id)
        {
            ZipCode myentity = new ZipCode();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (ZipCode)myentityItable;
            AutoMapper.Mapper.CreateMap<ZipCode, ADZipCode>();
            return (AutoMapper.Mapper.Map<ADZipCode>(myentity));
        }

        public static List<ADZipCode> GetZipCode(DataTable dt)
        {
            List<ADZipCode> ZipCodeList = (from DataRow dr in dt.Rows
                                                                  select new ADZipCode()
                                                                  {
                                                                      ZIPCodeId = ((dr["ZIPCodeId"] != DBNull.Value ? Convert.ToInt32(dr["ZIPCodeId"]) : 0)),
                                                                      ZIPCode = ((dr["ZIPCode"] != DBNull.Value ? dr["ZIPCode"].ToString() : "")),
                                                                      ZIPType = ((dr["ZIPType"] != DBNull.Value ? dr["ZIPType"].ToString() : "")),
                                                                      CityName = ((dr["CityName"] != DBNull.Value ? dr["CityName"].ToString() : "")),
                                                                      CityType = ((dr["CityType"] != DBNull.Value ? dr["CityType"].ToString() : "")),
                                                                      CountyName = ((dr["CountyName"] != DBNull.Value ? dr["CountyName"].ToString() : "")),
                                                                      CountyFIPS = ((dr["CountyFIPS"] != DBNull.Value ? dr["CountyFIPS"].ToString() : "")),
                                                                      StateName = ((dr["StateName"] != DBNull.Value ? dr["StateName"].ToString() : "")),
                                                                      StateAbbr = ((dr["StateAbbr"] != DBNull.Value ? dr["StateAbbr"].ToString() : "")),
                                                                      StateFIPS = ((dr["StateFIPS"] != DBNull.Value ? dr["StateFIPS"].ToString() : "")),
                                                                      MSACode = ((dr["MSACode"] != DBNull.Value ? dr["MSACode"].ToString() : "")),
                                                                      AreaCode = ((dr["AreaCode"] != DBNull.Value ? dr["AreaCode"].ToString() : "")),
                                                                      TimeZone = ((dr["TimeZone"] != DBNull.Value ? dr["TimeZone"].ToString() : "")),
                                                                      UTC = ((dr["UTC"] != DBNull.Value ? dr["UTC"].ToString() : "")),
                                                                      DST = ((dr["DST"] != DBNull.Value ? dr["DST"].ToString() : "")),
                                                                      Latitude = ((dr["Latitude"] != DBNull.Value ? dr["Latitude"].ToString() : "")),
                                                                      Longitude = ((dr["Longitude"] != DBNull.Value ? dr["Longitude"].ToString() : "")),
                                                                  }).ToList();
            return ZipCodeList;
        }


        public static DataTable GetZipCodeDtList(string City, string State, string Zip)
        {
            try
            {
                string mysql = "select top(50)* from ZipCode where 1 = 1 ";
                if(City.Length > 1)
                {
                    mysql += " and CityName like '" + City +"%'";
                }
                if (State.Length > 1)
                {
                    mysql += " and StateAbbr like '" + State + "%'";
                }
                if (Zip.Length > 1)
                {
                    mysql += " and  ZIPCode like '" + Zip + "%'";
                }
                mysql+= " order by CityName asc";

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(mysql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }



        #endregion
    }
}
