﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADBatchJobLegalParties
    {
        #region Properties
        public int Id { get; set; }
        public int BatchJobId { get; set; }
        public string TypeCode { get; set; }
        public string RefNum { get; set; }
        public string AddressName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Telephone1 { get; set; }
        public string Telephone2 { get; set; }
        public string Telephone3 { get; set; }
        public string Fax { get; set; }
        public bool IsPrimary { get; set; }
        public bool MLAgent { get; set; }
        public bool IsNonUSAddr { get; set; }
        #endregion
        
        #region CRUD
        public static ADBatchJobLegalParties ReadBatchJobLegalParties(int id)
        {
             BatchJobLegalParties myentity = new BatchJobLegalParties();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (BatchJobLegalParties)myentityItable;
            AutoMapper.Mapper.CreateMap<BatchJobLegalParties, ADBatchJobLegalParties>();
            return (AutoMapper.Mapper.Map<ADBatchJobLegalParties>(myentity));
        }

        public static ADBatchJobLegalParties GetBatchJobLegalParties(DataTable dt)
        {
         try
            { 
            List<ADBatchJobLegalParties> BatchJobLegalPartiesList = (from DataRow dr in dt.Rows
                                               select new ADBatchJobLegalParties()
                                               {
                                                   Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                                   BatchJobId = ((dr["BatchJobId"] != DBNull.Value ? Convert.ToInt32(dr["BatchJobId"]) : 0)),
                                                   TypeCode = ((dr["TypeCode"] != DBNull.Value ? dr["TypeCode"].ToString() : "")),
                                                   RefNum = ((dr["RefNum"] != DBNull.Value ? dr["RefNum"].ToString() : "")),
                                                   AddressName = ((dr["AddressName"] != DBNull.Value ? dr["AddressName"].ToString() : "")),
                                                   AddressLine1 = ((dr["AddressLine1"] != DBNull.Value ? dr["AddressLine1"].ToString() : "")),
                                                   AddressLine2 = ((dr["AddressLine2"] != DBNull.Value ? dr["AddressLine2"].ToString() : "")),
                                                   City = ((dr["City"] != DBNull.Value ? dr["City"].ToString() : "")),
                                                   State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),
                                                   PostalCode = ((dr["PostalCode"] != DBNull.Value ? dr["PostalCode"].ToString() : "")),
                                                   Telephone1 = ((dr["Telephone1"] != DBNull.Value ? dr["Telephone1"].ToString() : "")),
                                                   Telephone2 = ((dr["Telephone2"] != DBNull.Value ? dr["Telephone2"].ToString() : "")),
                                                   Telephone3 = ((dr["Telephone3"] != DBNull.Value ? dr["Telephone3"].ToString() : "")),
                                                   Fax = ((dr["Fax"] != DBNull.Value ? dr["Fax"].ToString() : "")),
                                                   IsPrimary = ((dr["IsPrimary"] != DBNull.Value ? Convert.ToBoolean(dr["IsPrimary"]) : false)),
                                                   MLAgent = ((dr["MLAgent"] != DBNull.Value ? Convert.ToBoolean(dr["MLAgent"]) : false)),
                                                   IsNonUSAddr = ((dr["IsNonUSAddr"] != DBNull.Value ? Convert.ToBoolean(dr["IsNonUSAddr"]) : false)),
                                               }).ToList();
            return BatchJobLegalPartiesList.First();
            }
            catch(Exception ex)
            {
                return (new ADBatchJobLegalParties());

            }
        }

        public static List<ADBatchJobLegalParties> BatchJobLegalPartiesList(DataTable dt)
        {
            List<ADBatchJobLegalParties> BatchJobLegalPartiesList = (from DataRow dr in dt.Rows
                                               select new ADBatchJobLegalParties()
                                               {
                                                   Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                                   BatchJobId = ((dr["BatchJobId"] != DBNull.Value ? Convert.ToInt32(dr["BatchJobId"]) : 0)),
                                                   TypeCode = ((dr["TypeCode"] != DBNull.Value ? dr["TypeCode"].ToString() : "")),
                                                   RefNum = ((dr["RefNum"] != DBNull.Value ? dr["RefNum"].ToString() : "")),
                                                   AddressName = ((dr["AddressName"] != DBNull.Value ? dr["AddressName"].ToString() : "")),
                                                   AddressLine1 = ((dr["AddressLine1"] != DBNull.Value ? dr["AddressLine1"].ToString() : "")),
                                                   AddressLine2 = ((dr["AddressLine2"] != DBNull.Value ? dr["AddressLine2"].ToString() : "")),
                                                   City = ((dr["City"] != DBNull.Value ? dr["City"].ToString() : "")),
                                                   State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),
                                                   PostalCode = ((dr["PostalCode"] != DBNull.Value ? dr["PostalCode"].ToString() : "")),
                                                   Telephone1 = ((dr["Telephone1"] != DBNull.Value ? dr["Telephone1"].ToString() : "")),
                                                   Telephone2 = ((dr["Telephone2"] != DBNull.Value ? dr["Telephone2"].ToString() : "")),
                                                   Telephone3 = ((dr["Telephone3"] != DBNull.Value ? dr["Telephone3"].ToString() : "")),
                                                   Fax = ((dr["Fax"] != DBNull.Value ? dr["Fax"].ToString() : "")),
                                                   IsPrimary = ((dr["IsPrimary"] != DBNull.Value ? Convert.ToBoolean(dr["IsPrimary"]) : false)),
                                                   MLAgent = ((dr["MLAgent"] != DBNull.Value ? Convert.ToBoolean(dr["MLAgent"]) : false)),
                                                   IsNonUSAddr = ((dr["IsNonUSAddr"] != DBNull.Value ? Convert.ToBoolean(dr["IsNonUSAddr"]) : false)),
                                               }).ToList();
            return BatchJobLegalPartiesList;
        }


        public static int SaveLienBatchJobLegalParties(ADBatchJobLegalParties objUpdatedBatchJobLegalParties)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADBatchJobLegalParties, BatchJobLegalParties>();
                ITable tblBatchJobLegalParties;
                BatchJobLegalParties objBatchJobLegalParties;

                if (objUpdatedBatchJobLegalParties.Id != 0)
                {
                    ADBatchJobLegalParties objADBatchJobLegalPartiesOriginal = ReadBatchJobLegalParties(Convert.ToInt32(objUpdatedBatchJobLegalParties.Id));

                    if (objUpdatedBatchJobLegalParties.BatchJobId.ToString() == "" || objUpdatedBatchJobLegalParties.BatchJobId == null)
                    {
                    }
                    else
                    {
                        objADBatchJobLegalPartiesOriginal.BatchJobId = objUpdatedBatchJobLegalParties.BatchJobId;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJobLegalParties.TypeCode))
                    {
                    }
                    else
                    {
                        objADBatchJobLegalPartiesOriginal.TypeCode = objUpdatedBatchJobLegalParties.TypeCode;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJobLegalParties.AddressName))
                    {
                    }
                    else
                    {
                        objADBatchJobLegalPartiesOriginal.AddressName = objUpdatedBatchJobLegalParties.AddressName;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJobLegalParties.AddressLine1))
                    {
                    }
                    else
                    {
                        objADBatchJobLegalPartiesOriginal.AddressLine1 = objUpdatedBatchJobLegalParties.AddressLine1;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJobLegalParties.AddressLine2))
                    {
                    }
                    else
                    {
                        objADBatchJobLegalPartiesOriginal.AddressLine2 = objUpdatedBatchJobLegalParties.AddressLine2;
                    }

                    if (objUpdatedBatchJobLegalParties.IsNonUSAddr.ToString() == "" || objUpdatedBatchJobLegalParties.IsNonUSAddr == null)
                    {
                    }
                    else
                    {
                        objADBatchJobLegalPartiesOriginal.IsNonUSAddr = objUpdatedBatchJobLegalParties.IsNonUSAddr;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJobLegalParties.City))
                    {
                    }
                    else
                    {
                        objADBatchJobLegalPartiesOriginal.City = objUpdatedBatchJobLegalParties.City;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJobLegalParties.State))
                    {
                    }
                    else
                    {
                        objADBatchJobLegalPartiesOriginal.State = objUpdatedBatchJobLegalParties.State;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJobLegalParties.PostalCode))
                    {
                    }
                    else
                    {
                        objADBatchJobLegalPartiesOriginal.PostalCode = objUpdatedBatchJobLegalParties.PostalCode;
                    }


                    if (string.IsNullOrEmpty(objUpdatedBatchJobLegalParties.Telephone1))
                    {
                    }
                    else
                    {
                        objADBatchJobLegalPartiesOriginal.Telephone1 = objUpdatedBatchJobLegalParties.Telephone1;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJobLegalParties.Telephone2))
                    {
                    }
                    else
                    {
                        objADBatchJobLegalPartiesOriginal.Telephone2 = objUpdatedBatchJobLegalParties.Telephone2;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJobLegalParties.RefNum))
                    {
                    }
                    else
                    {
                        objADBatchJobLegalPartiesOriginal.RefNum = objUpdatedBatchJobLegalParties.RefNum;
                    }

                    objBatchJobLegalParties = AutoMapper.Mapper.Map<BatchJobLegalParties>(objADBatchJobLegalPartiesOriginal);
                    tblBatchJobLegalParties = (ITable)objBatchJobLegalParties;
                    DBO.Provider.Update(ref tblBatchJobLegalParties);
                }
                else
                {
                    objBatchJobLegalParties = AutoMapper.Mapper.Map<BatchJobLegalParties>(objUpdatedBatchJobLegalParties);
                    tblBatchJobLegalParties = (ITable)objBatchJobLegalParties;
                    DBO.Provider.Create(ref tblBatchJobLegalParties);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int SaveCityStateZipOtherLegalInfo(ADBatchJobLegalParties objUpdatedBatchJobLegalParties)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADBatchJobLegalParties, BatchJobLegalParties>();
                ITable tblBatchJobLegalParties;
                BatchJobLegalParties objBatchJobLegalParties;

                if (objUpdatedBatchJobLegalParties.Id != 0)
                {
                    ADBatchJobLegalParties objADBatchJobLegalPartiesOriginal = ReadBatchJobLegalParties(Convert.ToInt32(objUpdatedBatchJobLegalParties.Id));
                      
                    if (string.IsNullOrEmpty(objUpdatedBatchJobLegalParties.City))
                    {
                    }
                    else
                    {
                        objADBatchJobLegalPartiesOriginal.City = objUpdatedBatchJobLegalParties.City;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJobLegalParties.State))
                    {
                    }
                    else
                    {
                        objADBatchJobLegalPartiesOriginal.State = objUpdatedBatchJobLegalParties.State;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJobLegalParties.PostalCode))
                    {
                    }
                    else
                    {
                        objADBatchJobLegalPartiesOriginal.PostalCode = objUpdatedBatchJobLegalParties.PostalCode;
                    }

                    objBatchJobLegalParties = AutoMapper.Mapper.Map<BatchJobLegalParties>(objADBatchJobLegalPartiesOriginal);
                    tblBatchJobLegalParties = (ITable)objBatchJobLegalParties;
                    DBO.Provider.Update(ref tblBatchJobLegalParties);
                }
                else
                {
                    objBatchJobLegalParties = AutoMapper.Mapper.Map<BatchJobLegalParties>(objUpdatedBatchJobLegalParties);
                    tblBatchJobLegalParties = (ITable)objBatchJobLegalParties;
                    DBO.Provider.Create(ref tblBatchJobLegalParties);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }


        public static bool DeleteBatchJobLegalParties(string Id)
        {
            try
            {
                ITable IReport;
                BatchJobLegalParties objBatchJobLegalParties = new BatchJobLegalParties();
                objBatchJobLegalParties.Id  = Convert.ToInt32(Id);
                IReport = objBatchJobLegalParties;
                DBO.Provider.Delete(ref IReport);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static DataTable BatchJobLegalPartiesList(string Id)
        {
            try
            {
                string sql = "Select * from BatchJobLegalParties Where BatchJobId = " + Id;
                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        //public static List<ADBatchJobLegalParties> GetBatchJobLegalPartiesList(DataTable dt)
        //{
        //    //DataTable dt = GetCustomerLookUpDt(objDeleteCustomerVM);

        //    List<ADBatchJobLegalParties> List = (from DataRow dr in dt.Rows
        //                                   select new ADBatchJobLegalParties()
        //                                   {
        //                                       TypeCode = 

        //                                       //ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
        //                                       //Id = ((dr["Id"] != DBNull.Value ? dr["Id"].ToString() : "")),
        //                                       //Name = ((dr["Name"] != DBNull.Value ? dr["Name"].ToString() : "")),
        //                                       //Ref = ((dr["Ref"] != DBNull.Value ? dr["Ref"].ToString() : "")),
        //                                       //Address = ((dr["Address"] != DBNull.Value ? dr["Address"].ToString() : "")),
        //                                   }).ToList();

        //    return List;
        //}


        #endregion
    }
}
