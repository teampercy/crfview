﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADBatchCustomerAnalytic
    {
        #region Properties

        public int Id { get; set; }
        public int BatchId { get; set; }
        public int ClientId { get; set; }
        public string CustRefNum { get; set; }
        public decimal TotalExposure { get; set; }
        public DateTime OldestInvoiceDate { get; set; }
        public decimal PendingCredit { get; set; }
        public decimal CurrentBalance { get; set; }
        public decimal Bucket30Day { get; set; }
        public decimal Bucket60Day { get; set; }
        public decimal Bucket90Day { get; set; }
        public decimal Bucket120Day { get; set; }
        public decimal Bucket150Day { get; set; }
        public decimal YTDRevenue { get; set; }
        public decimal TotalBalance { get; set; }
        public DateTime LastPayDate { get; set; }
        public DateTime NSFDate { get; set; }
        public int WeightedAvgOpenDays { get; set; }
        public int WeightedAvgPayDays { get; set; }
        public int Tenure { get; set; }
        public DateTime DateCreated { get; set; }
        public int Custid { get; set; }
        public bool IsError { get; set; }
        public string ErrorDescription { get; set; }
        public bool IsProcessed { get; set; }

        #endregion
    }
}
