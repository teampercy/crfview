﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.DataEntry.ImportJobView.ViewModels
{
    public class ImportJobViewVM
    {
        public ImportJobViewVM()
        {
            ClientInterfaceList = CommonBindList.GetLienClientInterfaceIdForImportJobViewList();
        }
        public string FileName { get; set; }
        public string ddlClientInterface { get; set; }
        public List<CommonBindList> ClientInterfaceList { get; set; }

        public string EDIInfo { get; set; }
        public string EDIInstruction { get; set; }
        public string ClientInterfaceId { get; set; }

        public ADClientInterface objClientInterface { get; set; }
    }

}
