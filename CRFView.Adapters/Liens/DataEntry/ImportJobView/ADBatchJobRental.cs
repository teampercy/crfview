﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADBatchJobRental
    {
        #region Properties
        public int Id { get; set; }
        public int BatchId { get; set; }
        public int ClientId { get; set; }
        public string CustRefNum { get; set; }
        public string BranchNum { get; set; }
        public string JobNum { get; set; }
        public string Status { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ContractType { get; set; }
        public string RANum { get; set; }
        public int IsError { get; set; }
        public string ErrorDescription { get; set; }
        public int CustId { get; set; }
        public string ChangeType { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsProcessed { get; set; }
        public int JobId { get; set; }
        public int JobRentalId { get; set; }
        #endregion

    }
}
