﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADBatchCustomer
    {
        #region Properties
        public int BatchCustId { get; set; }
        public int BatchId { get; set; }
        public int ClientId { get; set; }
        public string CustRefNum { get; set; }
        public string CustomerName { get; set; }
        public string NARPAcctNo { get; set; }
        public string ParentCompany { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string ContactName { get; set; }
        public string AreaCode { get; set; }
        public string PhoneNo { get; set; }
        public string FaxAreaCode { get; set; }
        public string FaxPhoneNo { get; set; }
        public string Status { get; set; }
        public string BranchNum { get; set; }
        public string SmanNum { get; set; }
        public string DNNFlag { get; set; }
        public string ChangeType { get; set; }
        public DateTime DateCreated { get; set; }
        public int CustId { get; set; }
        public bool IsError { get; set; }
        public string ErrorDescription { get; set; }
        public bool IsProcessed { get; set; }
        #endregion

    }
}
