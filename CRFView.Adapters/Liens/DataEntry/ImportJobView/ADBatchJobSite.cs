﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADBatchJobSite
    {
        #region Properties
        public int Id { get; set; }
        public int BatchId { get; set; }
        public int ClientId { get; set; }
        public string CustRefNum { get; set; }
        public string JobNum { get; set; }
        public string JobName { get; set; }
        public string JobAddr1 { get; set; }
        public string JobAddr2 { get; set; }
        public string JobCity { get; set; }
        public string JobState { get; set; }
        public string JobZip { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsError { get; set; }
        public string ErrorDescription { get; set; }
        public int CustId { get; set; }
        public bool IsProcessed { get; set; }
        #endregion
    }
}
