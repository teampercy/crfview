﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADBatchJobInvoice
    {
        #region Properties
        public int Id { get; set; }
        public int BatchId { get; set; }
        public int ClientId { get; set; }
        public string CustRefNum { get; set; }
        public string BranchNum { get; set; }
        public DateTime InvoiceDate { get; set; }
        public string InvoiceNum { get; set; }
        public int SeqNo { get; set; }
        public string ContractType { get; set; }
        public decimal OriginalBalance { get; set; }
        public decimal CurrentBalance { get; set; }
        public decimal OrigWriteOffAmt { get; set; }
        public decimal Recoveries { get; set; }
        public decimal Adjustments { get; set; }
        public decimal WriteOffAmt { get; set; }
        public string NSFFlag { get; set; }
        public string ClientStatus { get; set; }
        public string UCCFlag { get; set; }
        public string ChangeType { get; set; }
        public DateTime DateCreated { get; set; }
        public int CustId { get; set; }
        public bool IsError { get; set; }
        public string ErrorDescription { get; set; }
        public bool IsProcessed { get; set; }
        public int JobRentalId { get; set; }
        public int JobInvoiceId { get; set; }
        public string JobNum { get; set; }
        #endregion
    }
}
