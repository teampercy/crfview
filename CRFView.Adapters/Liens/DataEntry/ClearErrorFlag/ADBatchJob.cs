﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using HDS.DAL.Providers;
using CRF.BLL.CRFDB.SPROCS;
using HDS.DAL.COMMON;
using CRF.BLL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRFView.Adapters.Liens.DataEntry.CreateWebBatches.ViewModels;

namespace CRFView.Adapters
{
    public class ADBatchJob
    {
        #region Proprties
        public int Id { get; set; }
        public int BatchId { get; set; }
        public int ClientId { get; set; }
        public string BranchNum { get; set; }
        public string JobName { get; set; }
        public string JobAdd1 { get; set; }
        public string JobAdd2 { get; set; }
        public string JobCity { get; set; }
        public string JobState { get; set; }
        public string JobZip { get; set; }
        public string JobNum { get; set; }
        public decimal EstBalance { get; set; }
        public string APNNum { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime RevDate { get; set; }
        public string CustJobNum { get; set; }
        public string CustRefNum { get; set; }
        public string CustName { get; set; }
        public string CustContactName { get; set; }
        public string CustAdd1 { get; set; }
        public string CustAdd2 { get; set; }
        public string CustCity { get; set; }
        public string CustState { get; set; }
        public string CustZip { get; set; }
        public string CustPhone1 { get; set; }
        public string CustPhone2 { get; set; }
        public string CustFax { get; set; }
        public string CustMatchingKey { get; set; }
        public string GCRefNum { get; set; }
        public string GCName { get; set; }
        public string GCContactName { get; set; }
        public string GCAdd1 { get; set; }
        public string GCAdd2 { get; set; }
        public string GCCity { get; set; }
        public string GCState { get; set; }
        public string GCZip { get; set; }
        public string GCPhone1 { get; set; }
        public string GCPhone2 { get; set; }
        public string GCFax { get; set; }
        public string GCMatchingKey { get; set; }
        public string OwnrName { get; set; }
        public string OwnrAdd1 { get; set; }
        public string OwnrAdd2 { get; set; }
        public string OwnrCity { get; set; }
        public string OwnrState { get; set; }
        public string OwnrZip { get; set; }
        public string OwnrPhone1 { get; set; }
        public string OwnrPhone2 { get; set; }
        public string OwnrFax { get; set; }
        public string LenderName { get; set; }
        public string LenderAdd1 { get; set; }
        public string LenderAdd2 { get; set; }
        public string LenderCity { get; set; }
        public string LenderState { get; set; }
        public string LenderZip { get; set; }
        public string LenderPhone1 { get; set; }
        public string LenderPhone2 { get; set; }
        public string LenderFax { get; set; }
        public string PONum { get; set; }
        public int StatusCodeId { get; set; }
        public int DeskNumId { get; set; }
        public string JobCounty { get; set; }
        public string BondNum { get; set; }
        public string SpecialInstruction { get; set; }
        public string FolioNum { get; set; }
        public string Note { get; set; }
        public bool PrelimBox { get; set; }
        public bool RushOrder { get; set; }
        public bool VerifyJob { get; set; }
        public bool VerifyJobASIS { get; set; }
        public bool FederalJob { get; set; }
        public bool ResidentialBox { get; set; }
        public bool GreenCard { get; set; }
        public bool MechanicLien { get; set; }
        public bool PublicJob { get; set; }
        public bool RushOrderML { get; set; }
        public bool StopNoticeBox { get; set; }
        public bool LienRelease { get; set; }
        public bool PaymentBond { get; set; }
        public bool JointCk { get; set; }
        public bool TitleVerifiedBox { get; set; }
        public bool SameAsGC { get; set; }
        public bool SameAsCust { get; set; }
        public bool NCInfo { get; set; }
        public string EquipRate { get; set; }
        public string EquipRental { get; set; }
        public int CustId { get; set; }
        public int GenId { get; set; }
        public bool IsError { get; set; }
        public bool IsWarning { get; set; }
        public string ErrorDescription { get; set; }
        public int SubmittedByUserId { get; set; }
        public DateTime SubmittedOn { get; set; }
        public string PlacementSource { get; set; }
        public bool PrelimASIS { get; set; }
        public string DesigneeName { get; set; }
        public string DesigneeAdd1 { get; set; }
        public string DesigneeAdd2 { get; set; }
        public string DesigneeCity { get; set; }
        public string DesigneeState { get; set; }
        public string DesigneeZip { get; set; }
        public string DesigneePhone1 { get; set; }
        public string DesigneePhone2 { get; set; }
        public string DesigneeFax { get; set; }
        public bool PrivateJob { get; set; }
        public int JobActionId { get; set; }
        public string AZLotAllocate { get; set; }
        public DateTime NoticeSent { get; set; }
        public DateTime VerifiedDate { get; set; }
        public DateTime LastUpdateOn { get; set; }
        public int MinCustId { get; set; }
        public int MinGenId { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int RptClientId { get; set; }
        public bool BackUpBox { get; set; }
        public string HistoryNote { get; set; }
        public string CustSpecialInstruction { get; set; }
        public bool OwnerSourceGC { get; set; }
        public string GCSpecialInstruction { get; set; }
        public bool AZCert { get; set; }
        public string RANum { get; set; }
        public int CloneJobId { get; set; }
        public string TempKey { get; set; }
        public bool MLAgent { get; set; }
        public string BuildingPermitNum { get; set; }
        public bool SuretyBox { get; set; }
        public bool IsAmendment { get; set; }
        public int JobId { get; set; }
        public bool NOCBox { get; set; }
        public int LenderId { get; set; }
        public int OwnerId { get; set; }
        public bool RushOrderVerify { get; set; }
        public bool DataEntryFee { get; set; }
        public string CustEmail { get; set; }
        public string GCEmail { get; set; }
        public bool IsCUNonUSAddr { get; set; }
        public bool IsGCNonUSAddr { get; set; }
        public bool IsOWNonUSAddr { get; set; }
        public bool IsLENonUSAddr { get; set; }
        public bool IsDENonUSAddr { get; set; }
        public bool VerifyOWOnly { get; set; }
        public bool VerifyOWOnlyTX { get; set; }
        public bool VerifyOWOnlySend { get; set; }
        public string FilterKey { get; set; }
        #endregion

        public bool chkCopyJobAddr { get; set; }

        #region CRUD

        //Read Details from database
        public static ADBatchJob ReadBatchJob(int id)
        {
            BatchJob myentity = new BatchJob();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (BatchJob)myentityItable;
            AutoMapper.Mapper.CreateMap<BatchJob, ADBatchJob>();
            return (AutoMapper.Mapper.Map<ADBatchJob>(myentity));
        }

        public static ADBatchJob ReadBatchJobByBatchJobId(string BatchJobId)
        {
            try
            {
                string mysql = "Select * from vwBatchJobExport Where Id = " + BatchJobId;
                var AddressTableView = DBO.Provider.GetTableView(mysql);
                DataTable dt = AddressTableView.ToTable();
                if (dt.Rows.Count > 0)
                {
                    return ConvertDataTableToClass(dt);
                }
                else
                {
                    return new ADBatchJob();
                }
            }
            catch (Exception ex)
            {
                return new ADBatchJob();
            }
        }

        public static int SaveBatchJob(ADvwBatchJobExport objUpdatedBatchJob)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADBatchJob, BatchJob>();
                ITable tblBatchJob;
                BatchJob objBatchJob;

                if (objUpdatedBatchJob.Id != 0)
                {
                    ADBatchJob objADBatchJobOriginal = ReadBatchJobByBatchJobId(Convert.ToString(objUpdatedBatchJob.Id));

                    objADBatchJobOriginal.IsError = objUpdatedBatchJob.IsError;

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.ErrorDescription))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.ErrorDescription = objUpdatedBatchJob.ErrorDescription;
                    }

                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objADBatchJobOriginal);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Update(ref tblBatchJob);
                }
                else
                {
                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objUpdatedBatchJob);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Create(ref tblBatchJob);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static ADBatchJob GetBatchJobInfo(DataTable dt)
        {
            try
            {
                List<ADBatchJob> BatchJobList = (from DataRow dr in dt.Rows
                                                 select new ADBatchJob()
                                                 {
                                                     Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                                     BatchId = ((dr["BatchId"] != DBNull.Value ? Convert.ToInt32(dr["BatchId"]) : 0)),
                                                     ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
                                                     BranchNum = ((dr["BranchNum"] != DBNull.Value ? dr["BranchNum"].ToString() : "")),
                                                     JobName = ((dr["JobName"] != DBNull.Value ? dr["JobName"].ToString() : "")),
                                                     JobAdd1 = ((dr["JobAdd1"] != DBNull.Value ? dr["JobAdd1"].ToString() : "")),
                                                     JobAdd2 = ((dr["JobAdd2"] != DBNull.Value ? dr["JobAdd2"].ToString() : "")),
                                                     JobCity = ((dr["JobCity"] != DBNull.Value ? dr["JobCity"].ToString() : "")),
                                                     JobState = ((dr["JobState"] != DBNull.Value ? dr["JobState"].ToString() : "")),
                                                     JobZip = ((dr["JobZip"] != DBNull.Value ? dr["JobZip"].ToString() : "")),
                                                     JobNum = ((dr["JobNum"] != DBNull.Value ? dr["JobNum"].ToString() : "")),
                                                     EstBalance = ((dr["EstBalance"] != DBNull.Value ? Convert.ToDecimal(dr["EstBalance"]) : 0)),
                                                     APNNum = ((dr["APNNum"] != DBNull.Value ? dr["APNNum"].ToString() : "")),
                                                     StartDate = ((dr["StartDate"] != DBNull.Value ? Convert.ToDateTime(dr["StartDate"]) : DateTime.MinValue)),
                                                     EndDate = ((dr["EndDate"] != DBNull.Value ? Convert.ToDateTime(dr["EndDate"]) : DateTime.MinValue)),
                                                     RevDate = ((dr["RevDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevDate"]) : DateTime.MinValue)),
                                                     CustJobNum = ((dr["CustJobNum"] != DBNull.Value ? dr["CustJobNum"].ToString() : "")),
                                                     CustRefNum = ((dr["CustRefNum"] != DBNull.Value ? dr["CustRefNum"].ToString() : "")),
                                                     CustName = ((dr["CustName"] != DBNull.Value ? dr["CustName"].ToString() : "")),
                                                     CustContactName = ((dr["CustContactName"] != DBNull.Value ? dr["CustContactName"].ToString() : "")),
                                                     CustAdd1 = ((dr["CustAdd1"] != DBNull.Value ? dr["CustAdd1"].ToString() : "")),
                                                     CustAdd2 = ((dr["CustAdd2"] != DBNull.Value ? dr["CustAdd2"].ToString() : "")),
                                                     CustCity = ((dr["CustCity"] != DBNull.Value ? dr["CustCity"].ToString() : "")),
                                                     CustState = ((dr["CustState"] != DBNull.Value ? dr["CustState"].ToString() : "")),
                                                     CustZip = ((dr["CustZip"] != DBNull.Value ? dr["CustZip"].ToString() : "")),
                                                     CustPhone1 = ((dr["CustPhone1"] != DBNull.Value ? dr["CustPhone1"].ToString() : "")),
                                                     CustPhone2 = ((dr["CustPhone2"] != DBNull.Value ? dr["CustPhone2"].ToString() : "")),
                                                     CustFax = ((dr["CustFax"] != DBNull.Value ? dr["CustFax"].ToString() : "")),
                                                     CustMatchingKey = ((dr["CustMatchingKey"] != DBNull.Value ? dr["CustMatchingKey"].ToString() : "")),
                                                     GCRefNum = ((dr["GCRefNum"] != DBNull.Value ? dr["GCRefNum"].ToString() : "")),
                                                     GCName = ((dr["GCName"] != DBNull.Value ? dr["GCName"].ToString() : "")),
                                                     GCContactName = ((dr["GCContactName"] != DBNull.Value ? dr["GCContactName"].ToString() : "")),
                                                     GCAdd1 = ((dr["GCAdd1"] != DBNull.Value ? dr["GCAdd1"].ToString() : "")),
                                                     GCAdd2 = ((dr["GCAdd2"] != DBNull.Value ? dr["GCAdd2"].ToString() : "")),
                                                     GCCity = ((dr["GCCity"] != DBNull.Value ? dr["GCCity"].ToString() : "")),
                                                     GCState = ((dr["GCState"] != DBNull.Value ? dr["GCState"].ToString() : "")),
                                                     GCZip = ((dr["GCZip"] != DBNull.Value ? dr["GCZip"].ToString() : "")),
                                                     GCPhone1 = ((dr["GCPhone1"] != DBNull.Value ? dr["GCPhone1"].ToString() : "")),
                                                     GCPhone2 = ((dr["GCPhone2"] != DBNull.Value ? dr["GCPhone2"].ToString() : "")),
                                                     GCFax = ((dr["GCFax"] != DBNull.Value ? dr["GCFax"].ToString() : "")),
                                                     GCMatchingKey = ((dr["GCMatchingKey"] != DBNull.Value ? dr["GCMatchingKey"].ToString() : "")),
                                                     OwnrName = ((dr["OwnrName"] != DBNull.Value ? dr["OwnrName"].ToString() : "")),
                                                     OwnrAdd1 = ((dr["OwnrAdd1"] != DBNull.Value ? dr["OwnrAdd1"].ToString() : "")),
                                                     OwnrAdd2 = ((dr["OwnrAdd2"] != DBNull.Value ? dr["OwnrAdd2"].ToString() : "")),
                                                     OwnrCity = ((dr["OwnrCity"] != DBNull.Value ? dr["OwnrCity"].ToString() : "")),
                                                     OwnrState = ((dr["OwnrState"] != DBNull.Value ? dr["OwnrState"].ToString() : "")),
                                                     OwnrZip = ((dr["OwnrZip"] != DBNull.Value ? dr["OwnrZip"].ToString() : "")),
                                                     OwnrPhone1 = ((dr["OwnrPhone1"] != DBNull.Value ? dr["OwnrPhone1"].ToString() : "")),
                                                     OwnrPhone2 = ((dr["OwnrPhone2"] != DBNull.Value ? dr["OwnrPhone2"].ToString() : "")),
                                                     OwnrFax = ((dr["OwnrFax"] != DBNull.Value ? dr["OwnrFax"].ToString() : "")),
                                                     LenderName = ((dr["LenderName"] != DBNull.Value ? dr["LenderName"].ToString() : "")),
                                                     LenderAdd1 = ((dr["LenderAdd1"] != DBNull.Value ? dr["LenderAdd1"].ToString() : "")),
                                                     LenderAdd2 = ((dr["LenderAdd2"] != DBNull.Value ? dr["LenderAdd2"].ToString() : "")),
                                                     LenderCity = ((dr["LenderCity"] != DBNull.Value ? dr["LenderCity"].ToString() : "")),
                                                     LenderState = ((dr["LenderState"] != DBNull.Value ? dr["LenderState"].ToString() : "")),
                                                     LenderZip = ((dr["LenderZip"] != DBNull.Value ? dr["LenderZip"].ToString() : "")),
                                                     LenderPhone1 = ((dr["LenderPhone1"] != DBNull.Value ? dr["LenderPhone1"].ToString() : "")),
                                                     LenderPhone2 = ((dr["LenderPhone2"] != DBNull.Value ? dr["LenderPhone2"].ToString() : "")),
                                                     LenderFax = ((dr["LenderFax"] != DBNull.Value ? dr["LenderFax"].ToString() : "")),
                                                     PONum = ((dr["PONum"] != DBNull.Value ? dr["PONum"].ToString() : "")),
                                                     StatusCodeId = ((dr["StatusCodeId"] != DBNull.Value ? Convert.ToInt32(dr["StatusCodeId"]) : 0)),
                                                     DeskNumId = ((dr["DeskNumId"] != DBNull.Value ? Convert.ToInt32(dr["DeskNumId"]) : 0)),
                                                     JobCounty = ((dr["JobCounty"] != DBNull.Value ? dr["JobCounty"].ToString() : "")),
                                                     BondNum = ((dr["BondNum"] != DBNull.Value ? dr["BondNum"].ToString() : "")),
                                                     SpecialInstruction = ((dr["SpecialInstruction"] != DBNull.Value ? dr["SpecialInstruction"].ToString() : "")),
                                                     FolioNum = ((dr["FolioNum"] != DBNull.Value ? dr["FolioNum"].ToString() : "")),
                                                     Note = ((dr["Note"] != DBNull.Value ? dr["Note"].ToString() : "")),
                                                     PrelimBox = ((dr["PrelimBox"] != DBNull.Value ? Convert.ToBoolean(dr["PrelimBox"]) : false)),
                                                     RushOrder = ((dr["RushOrder"] != DBNull.Value ? Convert.ToBoolean(dr["RushOrder"]) : false)),
                                                     VerifyJob = ((dr["VerifyJob"] != DBNull.Value ? Convert.ToBoolean(dr["VerifyJob"]) : false)),
                                                     VerifyJobASIS = ((dr["VerifyJobASIS"] != DBNull.Value ? Convert.ToBoolean(dr["VerifyJobASIS"]) : false)),
                                                     FederalJob = ((dr["FederalJob"] != DBNull.Value ? Convert.ToBoolean(dr["FederalJob"]) : false)),
                                                     ResidentialBox = ((dr["ResidentialBox"] != DBNull.Value ? Convert.ToBoolean(dr["ResidentialBox"]) : false)),
                                                     GreenCard = ((dr["GreenCard"] != DBNull.Value ? Convert.ToBoolean(dr["GreenCard"]) : false)),
                                                     MechanicLien = ((dr["MechanicLien"] != DBNull.Value ? Convert.ToBoolean(dr["MechanicLien"]) : false)),
                                                     PublicJob = ((dr["PublicJob"] != DBNull.Value ? Convert.ToBoolean(dr["PublicJob"]) : false)),
                                                     RushOrderML = ((dr["RushOrderML"] != DBNull.Value ? Convert.ToBoolean(dr["RushOrderML"]) : false)),
                                                     StopNoticeBox = ((dr["StopNoticeBox"] != DBNull.Value ? Convert.ToBoolean(dr["StopNoticeBox"]) : false)),
                                                     LienRelease = ((dr["LienRelease"] != DBNull.Value ? Convert.ToBoolean(dr["LienRelease"]) : false)),
                                                     PaymentBond = ((dr["PaymentBond"] != DBNull.Value ? Convert.ToBoolean(dr["PaymentBond"]) : false)),
                                                     JointCk = ((dr["JointCk"] != DBNull.Value ? Convert.ToBoolean(dr["JointCk"]) : false)),
                                                     TitleVerifiedBox = ((dr["TitleVerifiedBox"] != DBNull.Value ? Convert.ToBoolean(dr["TitleVerifiedBox"]) : false)),
                                                     SameAsGC = ((dr["SameAsGC"] != DBNull.Value ? Convert.ToBoolean(dr["SameAsGC"]) : false)),
                                                     SameAsCust = ((dr["SameAsCust"] != DBNull.Value ? Convert.ToBoolean(dr["SameAsCust"]) : false)),
                                                     NCInfo = ((dr["NCInfo"] != DBNull.Value ? Convert.ToBoolean(dr["NCInfo"]) : false)),
                                                     EquipRate = ((dr["EquipRate"] != DBNull.Value ? dr["EquipRate"].ToString() : "")),
                                                     EquipRental = ((dr["EquipRental"] != DBNull.Value ? dr["EquipRental"].ToString() : "")),
                                                     CustId = ((dr["CustId"] != DBNull.Value ? Convert.ToInt32(dr["CustId"]) : 0)),
                                                     GenId = ((dr["GenId"] != DBNull.Value ? Convert.ToInt32(dr["GenId"]) : 0)),
                                                     IsError = ((dr["IsError"] != DBNull.Value ? Convert.ToBoolean(dr["IsError"]) : false)),
                                                     IsWarning = ((dr["IsWarning"] != DBNull.Value ? Convert.ToBoolean(dr["IsWarning"]) : false)),
                                                     ErrorDescription = ((dr["ErrorDescription"] != DBNull.Value ? dr["ErrorDescription"].ToString() : "")),
                                                     SubmittedByUserId = ((dr["SubmittedByUserId"] != DBNull.Value ? Convert.ToInt32(dr["SubmittedByUserId"]) : 0)),
                                                     SubmittedOn = ((dr["SubmittedOn"] != DBNull.Value ? Convert.ToDateTime(dr["SubmittedOn"]) : DateTime.MinValue)),
                                                     PlacementSource = ((dr["PlacementSource"] != DBNull.Value ? dr["PlacementSource"].ToString() : "")),
                                                     PrelimASIS = ((dr["PrelimASIS"] != DBNull.Value ? Convert.ToBoolean(dr["PrelimASIS"]) : false)),
                                                     DesigneeName = ((dr["DesigneeName"] != DBNull.Value ? dr["DesigneeName"].ToString() : "")),
                                                     DesigneeAdd1 = ((dr["DesigneeAdd1"] != DBNull.Value ? dr["DesigneeAdd1"].ToString() : "")),
                                                     DesigneeAdd2 = ((dr["DesigneeAdd2"] != DBNull.Value ? dr["DesigneeAdd2"].ToString() : "")),
                                                     DesigneeCity = ((dr["DesigneeCity"] != DBNull.Value ? dr["DesigneeCity"].ToString() : "")),
                                                     DesigneeState = ((dr["DesigneeState"] != DBNull.Value ? dr["DesigneeState"].ToString() : "")),
                                                     DesigneeZip = ((dr["DesigneeZip"] != DBNull.Value ? dr["DesigneeZip"].ToString() : "")),
                                                     DesigneePhone1 = ((dr["DesigneePhone1"] != DBNull.Value ? dr["DesigneePhone1"].ToString() : "")),
                                                     DesigneePhone2 = ((dr["DesigneePhone2"] != DBNull.Value ? dr["DesigneePhone2"].ToString() : "")),
                                                     DesigneeFax = ((dr["DesigneeFax"] != DBNull.Value ? dr["DesigneeFax"].ToString() : "")),
                                                     PrivateJob = ((dr["PrivateJob"] != DBNull.Value ? Convert.ToBoolean(dr["PrivateJob"]) : false)),
                                                     JobActionId = ((dr["JobActionId"] != DBNull.Value ? Convert.ToInt32(dr["JobActionId"]) : 0)),
                                                     AZLotAllocate = ((dr["AZLotAllocate"] != DBNull.Value ? dr["AZLotAllocate"].ToString() : "")),
                                                     NoticeSent = ((dr["NoticeSent"] != DBNull.Value ? Convert.ToDateTime(dr["NoticeSent"]) : DateTime.MinValue)),
                                                     VerifiedDate = ((dr["VerifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["VerifiedDate"]) : DateTime.MinValue)),
                                                     LastUpdateOn = ((dr["LastUpdateOn"] != DBNull.Value ? Convert.ToDateTime(dr["LastUpdateOn"]) : DateTime.MinValue)),
                                                     MinCustId = ((dr["MinCustId"] != DBNull.Value ? Convert.ToInt32(dr["MinCustId"]) : 0)),
                                                     MinGenId = ((dr["MinGenId"] != DBNull.Value ? Convert.ToInt32(dr["MinGenId"]) : 0)),
                                                     UpdatedOn = ((dr["UpdatedOn"] != DBNull.Value ? Convert.ToDateTime(dr["UpdatedOn"]) : DateTime.MinValue)),
                                                     RptClientId = ((dr["RptClientId"] != DBNull.Value ? Convert.ToInt32(dr["RptClientId"]) : 0)),
                                                     BackUpBox = ((dr["BackUpBox"] != DBNull.Value ? Convert.ToBoolean(dr["BackUpBox"]) : false)),
                                                     HistoryNote = ((dr["HistoryNote"] != DBNull.Value ? dr["HistoryNote"].ToString() : "")),
                                                     CustSpecialInstruction = ((dr["CustSpecialInstruction"] != DBNull.Value ? dr["CustSpecialInstruction"].ToString() : "")),
                                                     OwnerSourceGC = ((dr["OwnerSourceGC"] != DBNull.Value ? Convert.ToBoolean(dr["OwnerSourceGC"]) : false)),
                                                     GCSpecialInstruction = ((dr["GCSpecialInstruction"] != DBNull.Value ? dr["GCSpecialInstruction"].ToString() : "")),
                                                     AZCert = ((dr["AZCert"] != DBNull.Value ? Convert.ToBoolean(dr["AZCert"]) : false)),
                                                     RANum = ((dr["RANum"] != DBNull.Value ? dr["RANum"].ToString() : "")),
                                                     CloneJobId = ((dr["CloneJobId"] != DBNull.Value ? Convert.ToInt32(dr["CloneJobId"]) : 0)),
                                                     TempKey = ((dr["TempKey"] != DBNull.Value ? dr["TempKey"].ToString() : "")),
                                                     MLAgent = ((dr["MLAgent"] != DBNull.Value ? Convert.ToBoolean(dr["MLAgent"]) : false)),
                                                     BuildingPermitNum = ((dr["BuildingPermitNum"] != DBNull.Value ? dr["BuildingPermitNum"].ToString() : "")),
                                                     SuretyBox = ((dr["SuretyBox"] != DBNull.Value ? Convert.ToBoolean(dr["SuretyBox"]) : false)),
                                                     IsAmendment = ((dr["IsAmendment"] != DBNull.Value ? Convert.ToBoolean(dr["IsAmendment"]) : false)),
                                                     JobId = ((dr["JobId"] != DBNull.Value ? Convert.ToInt32(dr["JobId"]) : 0)),
                                                     NOCBox = ((dr["NOCBox"] != DBNull.Value ? Convert.ToBoolean(dr["NOCBox"]) : false)),
                                                     LenderId = ((dr["LenderId"] != DBNull.Value ? Convert.ToInt32(dr["LenderId"]) : 0)),
                                                     OwnerId = ((dr["OwnerId"] != DBNull.Value ? Convert.ToInt32(dr["OwnerId"]) : 0)),
                                                     RushOrderVerify = ((dr["RushOrderVerify"] != DBNull.Value ? Convert.ToBoolean(dr["RushOrderVerify"]) : false)),
                                                     DataEntryFee = ((dr["DataEntryFee"] != DBNull.Value ? Convert.ToBoolean(dr["DataEntryFee"]) : false)),
                                                     CustEmail = ((dr["CustEmail"] != DBNull.Value ? dr["CustEmail"].ToString() : "")),
                                                     GCEmail = ((dr["GCEmail"] != DBNull.Value ? dr["GCEmail"].ToString() : "")),
                                                     IsCUNonUSAddr = ((dr["IsCUNonUSAddr"] != DBNull.Value ? Convert.ToBoolean(dr["IsCUNonUSAddr"]) : false)),
                                                     IsGCNonUSAddr = ((dr["IsGCNonUSAddr"] != DBNull.Value ? Convert.ToBoolean(dr["IsGCNonUSAddr"]) : false)),
                                                     IsOWNonUSAddr = ((dr["IsOWNonUSAddr"] != DBNull.Value ? Convert.ToBoolean(dr["IsOWNonUSAddr"]) : false)),
                                                     IsLENonUSAddr = ((dr["IsLENonUSAddr"] != DBNull.Value ? Convert.ToBoolean(dr["IsLENonUSAddr"]) : false)),
                                                     IsDENonUSAddr = ((dr["IsDENonUSAddr"] != DBNull.Value ? Convert.ToBoolean(dr["IsDENonUSAddr"]) : false)),
                                                     VerifyOWOnly = ((dr["VerifyOWOnly"] != DBNull.Value ? Convert.ToBoolean(dr["VerifyOWOnly"]) : false)),
                                                     VerifyOWOnlyTX = ((dr["VerifyOWOnlyTX"] != DBNull.Value ? Convert.ToBoolean(dr["VerifyOWOnlyTX"]) : false)),
                                                     FilterKey = ((dr["FilterKey"] != DBNull.Value ? dr["FilterKey"].ToString() : "")),
                                                 }).ToList();
                return BatchJobList.First();
            }
            catch (Exception ex)
            {
                return (new ADBatchJob());
            }
        }

        public static int SaveLienBatchJob(ADBatchJob objUpdatedBatchJob)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADBatchJob, BatchJob>();
                ITable tblBatchJob;
                BatchJob objBatchJob;

                if (objUpdatedBatchJob.Id != 0)
                {
                    ADBatchJob objADBatchJobOriginal = ReadBatchJob(Convert.ToInt32(objUpdatedBatchJob.Id));

                    if (objUpdatedBatchJob.ClientId.ToString() == "" || objUpdatedBatchJob.ClientId == null)
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.ClientId = objUpdatedBatchJob.ClientId;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.BranchNum))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.BranchNum = objUpdatedBatchJob.BranchNum;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.JobState))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.JobState = objUpdatedBatchJob.JobState;
                    }


                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objADBatchJobOriginal);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Update(ref tblBatchJob);
                }
                else
                {
                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objUpdatedBatchJob);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Create(ref tblBatchJob);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int SaveNewLienBatchJobInfo(string BatchId)
        {
            try
            {
                ADBatchJob objADBatchJob = new ADBatchJob();
                AutoMapper.Mapper.CreateMap<ADBatchJob, BatchJob>();
                ITable tblBatchJob;
                BatchJob objBatchJob;

                objADBatchJob.BatchId = Convert.ToInt32(BatchId);
                objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objADBatchJob);
                tblBatchJob = (ITable)objBatchJob;
                DBO.Provider.Create(ref tblBatchJob);

                return objBatchJob.Id;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }


        public static int SaveLienCustomerInfo(ADBatchJob objUpdatedBatchJob)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADBatchJob, BatchJob>();
                ITable tblBatchJob;
                BatchJob objBatchJob;

                if (objUpdatedBatchJob.Id != 0)
                {
                    ADBatchJob objADBatchJobOriginal = ReadBatchJob(Convert.ToInt32(objUpdatedBatchJob.Id));

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.CustName))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.CustName = objUpdatedBatchJob.CustName;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.CustRefNum))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.CustRefNum = objUpdatedBatchJob.CustRefNum;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.CustContactName))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.CustContactName = objUpdatedBatchJob.CustContactName;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.CustAdd1))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.CustAdd1 = objUpdatedBatchJob.CustAdd1;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.CustAdd2))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.CustAdd2 = objUpdatedBatchJob.CustAdd2;
                    }

                    if (objUpdatedBatchJob.IsCUNonUSAddr.ToString() == "" || objUpdatedBatchJob.IsCUNonUSAddr == null)
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.IsCUNonUSAddr = objUpdatedBatchJob.IsCUNonUSAddr;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.CustCity))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.CustCity = objUpdatedBatchJob.CustCity;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.CustState))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.CustState = objUpdatedBatchJob.CustState;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.CustZip))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.CustZip = objUpdatedBatchJob.CustZip;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.CustPhone1))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.CustPhone1 = objUpdatedBatchJob.CustPhone1;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.CustFax))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.CustFax = objUpdatedBatchJob.CustFax;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.CustEmail))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.CustEmail = objUpdatedBatchJob.CustEmail;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.SpecialInstruction))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.SpecialInstruction = objUpdatedBatchJob.SpecialInstruction;
                    }

                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objADBatchJobOriginal);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Update(ref tblBatchJob);
                }
                else
                {
                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objUpdatedBatchJob);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Create(ref tblBatchJob);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int SaveLienJobInfo(ADBatchJob objUpdatedBatchJob)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADBatchJob, BatchJob>();
                ITable tblBatchJob;
                BatchJob objBatchJob;

                if (objUpdatedBatchJob.Id != 0)
                {
                    ADBatchJob objADBatchJobOriginal = ReadBatchJob(Convert.ToInt32(objUpdatedBatchJob.Id));

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.JobNum))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.JobNum = objUpdatedBatchJob.JobNum;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.JobName))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.JobName = objUpdatedBatchJob.JobName;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.JobAdd1))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.JobAdd1 = objUpdatedBatchJob.JobAdd1;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.JobAdd2))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.JobAdd2 = objUpdatedBatchJob.JobAdd2;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.JobCity))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.JobCity = objUpdatedBatchJob.JobCity;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.JobState))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.JobState = objUpdatedBatchJob.JobState;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.JobZip))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.JobZip = objUpdatedBatchJob.JobZip;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.JobCounty))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.JobCounty = objUpdatedBatchJob.JobCounty;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.APNNum))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.APNNum = objUpdatedBatchJob.APNNum;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.PONum))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.PONum = objUpdatedBatchJob.PONum;
                    }

                    if (objUpdatedBatchJob.JobActionId.ToString() == "" || objUpdatedBatchJob.JobActionId == null)
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.JobActionId = objUpdatedBatchJob.JobActionId;
                    }

                    if (objUpdatedBatchJob.PrivateJob.ToString() == "" || objUpdatedBatchJob.PrivateJob == null)
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.PrivateJob = objUpdatedBatchJob.PrivateJob;
                    }

                    if (objUpdatedBatchJob.PublicJob.ToString() == "" || objUpdatedBatchJob.PublicJob == null)
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.PublicJob = objUpdatedBatchJob.PublicJob;
                    }

                    if (objUpdatedBatchJob.FederalJob.ToString() == "" || objUpdatedBatchJob.FederalJob == null)
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.FederalJob = objUpdatedBatchJob.FederalJob;
                    }

                    if (objUpdatedBatchJob.ResidentialBox.ToString() == "" || objUpdatedBatchJob.ResidentialBox == null)
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.ResidentialBox = objUpdatedBatchJob.ResidentialBox;
                    }

                    if (objUpdatedBatchJob.RushOrder.ToString() == "" || objUpdatedBatchJob.RushOrder == null)
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.RushOrder = objUpdatedBatchJob.RushOrder;
                    }

                    if (objUpdatedBatchJob.GreenCard.ToString() == "" || objUpdatedBatchJob.GreenCard == null)
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.GreenCard = objUpdatedBatchJob.GreenCard;
                    }

                    if (objUpdatedBatchJob.BackUpBox.ToString() == "" || objUpdatedBatchJob.BackUpBox == null)
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.BackUpBox = objUpdatedBatchJob.BackUpBox;
                    }

                    if (objUpdatedBatchJob.RushOrderML.ToString() == "" || objUpdatedBatchJob.RushOrderML == null)
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.RushOrderML = objUpdatedBatchJob.RushOrderML;
                    }

                    if (objUpdatedBatchJob.JointCk.ToString() == "" || objUpdatedBatchJob.JointCk == null)
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.JointCk = objUpdatedBatchJob.JointCk;
                    }

                    if (objUpdatedBatchJob.NOCBox.ToString() == "" || objUpdatedBatchJob.NOCBox == null)
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.NOCBox = objUpdatedBatchJob.NOCBox;
                    }

                    if (objUpdatedBatchJob.RushOrderVerify.ToString() == "" || objUpdatedBatchJob.RushOrderVerify == null)
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.RushOrderVerify = objUpdatedBatchJob.RushOrderVerify;
                    }

                    if (objUpdatedBatchJob.DataEntryFee.ToString() == "" || objUpdatedBatchJob.DataEntryFee == null)
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.DataEntryFee = objUpdatedBatchJob.DataEntryFee;
                    }

                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objADBatchJobOriginal);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Update(ref tblBatchJob);
                }
                else
                {
                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objUpdatedBatchJob);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Create(ref tblBatchJob);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int SaveLienCityStateZipJobInfo(ADBatchJob objUpdatedBatchJob)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADBatchJob, BatchJob>();
                ITable tblBatchJob;
                BatchJob objBatchJob;

                if (objUpdatedBatchJob.Id != 0)
                {
                    ADBatchJob objADBatchJobOriginal = ReadBatchJob(Convert.ToInt32(objUpdatedBatchJob.Id));


                    if (string.IsNullOrEmpty(objUpdatedBatchJob.JobCity))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.JobCity = objUpdatedBatchJob.JobCity;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.JobState))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.JobState = objUpdatedBatchJob.JobState;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.JobZip))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.JobZip = objUpdatedBatchJob.JobZip;
                    }

                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objADBatchJobOriginal);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Update(ref tblBatchJob);
                }
                else
                {
                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objUpdatedBatchJob);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Create(ref tblBatchJob);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }


        public static int SaveLienMoreJobInfo(ADBatchJob objUpdatedBatchJob)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADBatchJob, BatchJob>();
                ITable tblBatchJob;
                BatchJob objBatchJob;

                if (objUpdatedBatchJob.Id != 0)
                {
                    ADBatchJob objADBatchJobOriginal = ReadBatchJob(Convert.ToInt32(objUpdatedBatchJob.Id));

                    if (objUpdatedBatchJob.StartDate == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.StartDate = objUpdatedBatchJob.StartDate;
                    }

                    if (objUpdatedBatchJob.EndDate == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.EndDate = objUpdatedBatchJob.EndDate;
                    }

                    if (objUpdatedBatchJob.EstBalance == 0)
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.EstBalance = objUpdatedBatchJob.EstBalance;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.SpecialInstruction))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.SpecialInstruction = objUpdatedBatchJob.SpecialInstruction;
                    }


                    if (string.IsNullOrEmpty(objUpdatedBatchJob.FolioNum))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.FolioNum = objUpdatedBatchJob.FolioNum;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.EquipRate))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.EquipRate = objUpdatedBatchJob.EquipRate;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.EquipRental))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.EquipRental = objUpdatedBatchJob.EquipRental;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.RANum))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.RANum = objUpdatedBatchJob.RANum;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.FilterKey))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.FilterKey = objUpdatedBatchJob.FilterKey;
                    }

                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objADBatchJobOriginal);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Update(ref tblBatchJob);
                }
                else
                {
                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objUpdatedBatchJob);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Create(ref tblBatchJob);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int SaveGCSameAsCust(string Id)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADBatchJob, BatchJob>();
                ITable tblBatchJob;
                BatchJob objBatchJob;

                if ((Convert.ToInt32(Id) != 0))
                {
                    ADBatchJob objADBatchJobOriginal = ReadBatchJob(Convert.ToInt32(Id));

                    objADBatchJobOriginal.GCName = objADBatchJobOriginal.CustName;
                    objADBatchJobOriginal.GCContactName = objADBatchJobOriginal.CustContactName;
                    objADBatchJobOriginal.GCAdd1 = objADBatchJobOriginal.CustAdd1;
                    objADBatchJobOriginal.GCAdd2 = objADBatchJobOriginal.CustAdd2;
                    objADBatchJobOriginal.GCCity = objADBatchJobOriginal.CustCity;
                    objADBatchJobOriginal.GCState = objADBatchJobOriginal.CustState;
                    objADBatchJobOriginal.GCZip = objADBatchJobOriginal.CustZip;
                    objADBatchJobOriginal.GCPhone1 = objADBatchJobOriginal.CustPhone1;
                    objADBatchJobOriginal.GCFax = objADBatchJobOriginal.CustFax;
                    objADBatchJobOriginal.GCEmail = objADBatchJobOriginal.CustEmail;
                    objADBatchJobOriginal.SameAsCust = true;

                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objADBatchJobOriginal);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Update(ref tblBatchJob);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int SaveGeneralContractorInfo(ADBatchJob objUpdatedBatchJob)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADBatchJob, BatchJob>();
                ITable tblBatchJob;
                BatchJob objBatchJob;

                if (objUpdatedBatchJob.Id != 0)
                {
                    ADBatchJob objADBatchJobOriginal = ReadBatchJob(Convert.ToInt32(objUpdatedBatchJob.Id));

                    objADBatchJobOriginal.SameAsCust = false;

                    if (objUpdatedBatchJob.OwnerSourceGC.ToString() == "" || objUpdatedBatchJob.OwnerSourceGC == null)
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.OwnerSourceGC = objUpdatedBatchJob.OwnerSourceGC;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.GCName))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.GCName = objUpdatedBatchJob.GCName;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.GCRefNum))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.GCRefNum = objUpdatedBatchJob.GCRefNum;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.GCContactName))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.GCContactName = objUpdatedBatchJob.GCContactName;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.GCAdd1))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.GCAdd1 = objUpdatedBatchJob.GCAdd1;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.GCAdd2))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.GCAdd2 = objUpdatedBatchJob.GCAdd2;
                    }

                    if (objUpdatedBatchJob.IsGCNonUSAddr.ToString() == "" || objUpdatedBatchJob.IsGCNonUSAddr == null)
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.IsGCNonUSAddr = objUpdatedBatchJob.IsGCNonUSAddr;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.GCCity))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.GCCity = objUpdatedBatchJob.GCCity;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.GCState))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.GCState = objUpdatedBatchJob.GCState;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.GCZip))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.GCZip = objUpdatedBatchJob.GCZip;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.GCPhone1))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.GCPhone1 = objUpdatedBatchJob.GCPhone1;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.GCFax))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.GCFax = objUpdatedBatchJob.GCFax;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.GCEmail))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.GCEmail = objUpdatedBatchJob.GCEmail;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.SpecialInstruction))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.SpecialInstruction = objUpdatedBatchJob.SpecialInstruction;
                    }

                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objADBatchJobOriginal);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Update(ref tblBatchJob);
                }
                else
                {
                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objUpdatedBatchJob);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Create(ref tblBatchJob);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int SaveOwnerSameAsGC(string Id)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADBatchJob, BatchJob>();
                ITable tblBatchJob;
                BatchJob objBatchJob;

                if ((Convert.ToInt32(Id) != 0))
                {
                    ADBatchJob objADBatchJobOriginal = ReadBatchJob(Convert.ToInt32(Id));

                    objADBatchJobOriginal.OwnrName = objADBatchJobOriginal.GCName;
                    objADBatchJobOriginal.OwnrAdd1 = objADBatchJobOriginal.GCAdd1;
                    objADBatchJobOriginal.OwnrAdd2 = objADBatchJobOriginal.GCAdd2;
                    objADBatchJobOriginal.OwnrCity = objADBatchJobOriginal.GCCity;
                    objADBatchJobOriginal.OwnrState = objADBatchJobOriginal.GCState;
                    objADBatchJobOriginal.OwnrZip = objADBatchJobOriginal.GCZip;
                    objADBatchJobOriginal.OwnrPhone1 = objADBatchJobOriginal.GCPhone1;
                    objADBatchJobOriginal.OwnrFax = objADBatchJobOriginal.GCFax;
                    objADBatchJobOriginal.SameAsGC = true;

                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objADBatchJobOriginal);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Update(ref tblBatchJob);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int SaveOwnerchkCopyJobAddr(string Id)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADBatchJob, BatchJob>();
                ITable tblBatchJob;
                BatchJob objBatchJob;

                if ((Convert.ToInt32(Id) != 0))
                {
                    ADBatchJob objADBatchJobOriginal = ReadBatchJob(Convert.ToInt32(Id));


                    objADBatchJobOriginal.OwnrAdd1 = objADBatchJobOriginal.JobAdd1;
                    objADBatchJobOriginal.OwnrAdd2 = objADBatchJobOriginal.JobAdd2;
                    objADBatchJobOriginal.OwnrCity = objADBatchJobOriginal.JobCity;
                    objADBatchJobOriginal.OwnrState = objADBatchJobOriginal.JobState;
                    objADBatchJobOriginal.OwnrZip = objADBatchJobOriginal.JobZip;
                    objADBatchJobOriginal.chkCopyJobAddr = true;

                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objADBatchJobOriginal);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Update(ref tblBatchJob);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int SaveOwnerMLAgent(string Id)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADBatchJob, BatchJob>();
                ITable tblBatchJob;
                BatchJob objBatchJob;

                if ((Convert.ToInt32(Id) != 0))
                {
                    ADBatchJob objADBatchJobOriginal = ReadBatchJob(Convert.ToInt32(Id));

                    
                    objADBatchJobOriginal.MLAgent = true;

                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objADBatchJobOriginal);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Update(ref tblBatchJob);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int SaveOwnerInfo(ADBatchJob objUpdatedBatchJob)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADBatchJob, BatchJob>();
                ITable tblBatchJob;
                BatchJob objBatchJob;

                if (objUpdatedBatchJob.Id != 0)
                {
                    ADBatchJob objADBatchJobOriginal = ReadBatchJob(Convert.ToInt32(objUpdatedBatchJob.Id));

                    objADBatchJobOriginal.SameAsGC = false;

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.OwnrName))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.OwnrName = objUpdatedBatchJob.OwnrName;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.OwnrAdd1))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.OwnrAdd1 = objUpdatedBatchJob.OwnrAdd1;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.OwnrAdd2))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.OwnrAdd2 = objUpdatedBatchJob.OwnrAdd2;
                    }

                    if (objUpdatedBatchJob.IsOWNonUSAddr.ToString() == "" || objUpdatedBatchJob.IsOWNonUSAddr == null)
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.IsOWNonUSAddr = objUpdatedBatchJob.IsOWNonUSAddr;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.OwnrCity))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.OwnrCity = objUpdatedBatchJob.OwnrCity;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.OwnrState))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.OwnrState = objUpdatedBatchJob.OwnrState;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.OwnrZip))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.OwnrZip = objUpdatedBatchJob.OwnrZip;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.OwnrPhone1))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.OwnrPhone1 = objUpdatedBatchJob.OwnrPhone1;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.OwnrFax))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.OwnrFax = objUpdatedBatchJob.OwnrFax;
                    }

                    if (objUpdatedBatchJob.MLAgent.ToString() == "" || objUpdatedBatchJob.MLAgent == null)
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.MLAgent = objUpdatedBatchJob.MLAgent;
                    }

                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objADBatchJobOriginal);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Update(ref tblBatchJob);
                }
                else
                {
                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objUpdatedBatchJob);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Create(ref tblBatchJob);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int SaveCityStateZipOwnerInfo(ADBatchJob objUpdatedBatchJob)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADBatchJob, BatchJob>();
                ITable tblBatchJob;
                BatchJob objBatchJob;

                if (objUpdatedBatchJob.Id != 0)
                {
                    ADBatchJob objADBatchJobOriginal = ReadBatchJob(Convert.ToInt32(objUpdatedBatchJob.Id));

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.OwnrCity))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.OwnrCity = objUpdatedBatchJob.OwnrCity;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.OwnrState))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.OwnrState = objUpdatedBatchJob.OwnrState;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.OwnrZip))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.OwnrZip = objUpdatedBatchJob.OwnrZip;
                    }

                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objADBatchJobOriginal);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Update(ref tblBatchJob);
                }
                else
                {
                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objUpdatedBatchJob);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Create(ref tblBatchJob);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int SaveLenderSuretyInfo(ADBatchJob objUpdatedBatchJob)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADBatchJob, BatchJob>();
                ITable tblBatchJob;
                BatchJob objBatchJob;

                if (objUpdatedBatchJob.Id != 0)
                {
                    ADBatchJob objADBatchJobOriginal = ReadBatchJob(Convert.ToInt32(objUpdatedBatchJob.Id));

                    if (objUpdatedBatchJob.SuretyBox.ToString() == "" || objUpdatedBatchJob.SuretyBox == null)
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.SuretyBox = objUpdatedBatchJob.SuretyBox;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.LenderName))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.LenderName = objUpdatedBatchJob.LenderName;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.LenderAdd1))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.LenderAdd1 = objUpdatedBatchJob.LenderAdd1;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.LenderAdd2))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.LenderAdd2 = objUpdatedBatchJob.LenderAdd2;
                    }

                    if (objUpdatedBatchJob.IsLENonUSAddr.ToString() == "" || objUpdatedBatchJob.IsLENonUSAddr == null)
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.IsLENonUSAddr = objUpdatedBatchJob.IsLENonUSAddr;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.LenderCity))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.LenderCity = objUpdatedBatchJob.LenderCity;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.LenderState))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.LenderState = objUpdatedBatchJob.LenderState;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.LenderZip))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.LenderZip = objUpdatedBatchJob.LenderZip;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.LenderPhone1))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.LenderPhone1 = objUpdatedBatchJob.LenderPhone1;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.LenderFax))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.LenderFax = objUpdatedBatchJob.LenderFax;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.BondNum))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.BondNum = objUpdatedBatchJob.BondNum;
                    }

                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objADBatchJobOriginal);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Update(ref tblBatchJob);
                }
                else
                {
                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objUpdatedBatchJob);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Create(ref tblBatchJob);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int SaveCityStateZipLenderSuretyInfo(ADBatchJob objUpdatedBatchJob)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADBatchJob, BatchJob>();
                ITable tblBatchJob;
                BatchJob objBatchJob;

                if (objUpdatedBatchJob.Id != 0)
                {
                    ADBatchJob objADBatchJobOriginal = ReadBatchJob(Convert.ToInt32(objUpdatedBatchJob.Id));


                    if (string.IsNullOrEmpty(objUpdatedBatchJob.LenderCity))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.LenderCity = objUpdatedBatchJob.LenderCity;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.LenderState))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.LenderState = objUpdatedBatchJob.LenderState;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.LenderZip))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.LenderZip = objUpdatedBatchJob.LenderZip;
                    }

                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objADBatchJobOriginal);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Update(ref tblBatchJob);
                }
                else
                {
                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objUpdatedBatchJob);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Create(ref tblBatchJob);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int SaveDesigneeInfo(ADBatchJob objUpdatedBatchJob)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADBatchJob, BatchJob>();
                ITable tblBatchJob;
                BatchJob objBatchJob;

                if (objUpdatedBatchJob.Id != 0)
                {
                    ADBatchJob objADBatchJobOriginal = ReadBatchJob(Convert.ToInt32(objUpdatedBatchJob.Id));


                    if (string.IsNullOrEmpty(objUpdatedBatchJob.DesigneeName))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.DesigneeName = objUpdatedBatchJob.DesigneeName;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.DesigneeAdd1))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.DesigneeAdd1 = objUpdatedBatchJob.DesigneeAdd1;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.DesigneeAdd2))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.DesigneeAdd2 = objUpdatedBatchJob.DesigneeAdd2;
                    }

                    if (objUpdatedBatchJob.IsDENonUSAddr.ToString() == "" || objUpdatedBatchJob.IsDENonUSAddr == null)
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.IsDENonUSAddr = objUpdatedBatchJob.IsDENonUSAddr;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.DesigneeCity))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.DesigneeCity = objUpdatedBatchJob.DesigneeCity;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.DesigneeState))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.DesigneeState = objUpdatedBatchJob.DesigneeState;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.DesigneeZip))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.DesigneeZip = objUpdatedBatchJob.DesigneeZip;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.DesigneePhone1))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.DesigneePhone1 = objUpdatedBatchJob.DesigneePhone1;
                    }

                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objADBatchJobOriginal);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Update(ref tblBatchJob);
                }
                else
                {
                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objUpdatedBatchJob);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Create(ref tblBatchJob);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int SaveCityStateZipDesigneeInfo(ADBatchJob objUpdatedBatchJob)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADBatchJob, BatchJob>();
                ITable tblBatchJob;
                BatchJob objBatchJob;

                if (objUpdatedBatchJob.Id != 0)
                {
                    ADBatchJob objADBatchJobOriginal = ReadBatchJob(Convert.ToInt32(objUpdatedBatchJob.Id));

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.DesigneeCity))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.DesigneeCity = objUpdatedBatchJob.DesigneeCity;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.DesigneeState))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.DesigneeState = objUpdatedBatchJob.DesigneeState;
                    }

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.DesigneeZip))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.DesigneeZip = objUpdatedBatchJob.DesigneeZip;
                    }

                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objADBatchJobOriginal);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Update(ref tblBatchJob);
                }
                else
                {
                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objUpdatedBatchJob);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Create(ref tblBatchJob);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int SaveLegalInfo(ADBatchJob objUpdatedBatchJob)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADBatchJob, BatchJob>();
                ITable tblBatchJob;
                BatchJob objBatchJob;

                if (objUpdatedBatchJob.Id != 0)
                {
                    ADBatchJob objADBatchJobOriginal = ReadBatchJob(Convert.ToInt32(objUpdatedBatchJob.Id));

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.Note))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.Note = objUpdatedBatchJob.Note;
                    }

                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objADBatchJobOriginal);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Update(ref tblBatchJob);
                }
                else
                {
                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objUpdatedBatchJob);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Create(ref tblBatchJob);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }


        public static int SaveNotesInfo(ADBatchJob objUpdatedBatchJob)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADBatchJob, BatchJob>();
                ITable tblBatchJob;
                BatchJob objBatchJob;

                if (objUpdatedBatchJob.Id != 0)
                {
                    ADBatchJob objADBatchJobOriginal = ReadBatchJob(Convert.ToInt32(objUpdatedBatchJob.Id));

                    if (string.IsNullOrEmpty(objUpdatedBatchJob.HistoryNote))
                    {
                    }
                    else
                    {
                        objADBatchJobOriginal.HistoryNote = objUpdatedBatchJob.HistoryNote;
                    }

                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objADBatchJobOriginal);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Update(ref tblBatchJob);
                }
                else
                {
                    objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objUpdatedBatchJob);
                    tblBatchJob = (ITable)objBatchJob;
                    DBO.Provider.Create(ref tblBatchJob);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }
        #endregion


        #region Local Methods
        public static ADBatchJob ConvertDataTableToClass(DataTable dt)
        {
            List<ADBatchJob> BatchJobList = (from DataRow dr in dt.Rows
                                             select new ADBatchJob()
                                             {
                                                 ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"].ToString()) : 0)),
                                                 BranchNum = ((dr["BranchNum"] != DBNull.Value ? Convert.ToString(dr["BranchNum"]) : "")),
                                                 JobNum = ((dr["JobNum"] != DBNull.Value ? Convert.ToString(dr["JobNum"]) : "")),
                                                 JobName = ((dr["JobName"] != DBNull.Value ? Convert.ToString(dr["JobName"]) : "")),
                                                 APNNum = ((dr["APNNum"] != DBNull.Value ? Convert.ToString(dr["APNNum"]) : "")),
                                                 AZLotAllocate = ((dr["AZLotAllocate"] != DBNull.Value ? Convert.ToString(dr["AZLotAllocate"]) : "")),
                                                 JobAdd1 = ((dr["JobAdd1"] != DBNull.Value ? Convert.ToString(dr["JobAdd1"]) : "")),
                                                 JobAdd2 = ((dr["JobAdd2"] != DBNull.Value ? Convert.ToString(dr["JobAdd2"]) : "")),
                                                 JobCity = ((dr["JobCity"] != DBNull.Value ? Convert.ToString(dr["JobCity"]) : "")),
                                                 JobState = ((dr["JobState"] != DBNull.Value ? Convert.ToString(dr["JobState"]) : "")),
                                                 JobZip = ((dr["JobZip"] != DBNull.Value ? Convert.ToString(dr["JobZip"]) : "")),
                                                 JobCounty = ((dr["JobCounty"] != DBNull.Value ? Convert.ToString(dr["JobCounty"]) : "")),
                                                 EstBalance = ((dr["EstBalance"] != DBNull.Value ? Convert.ToDecimal(dr["EstBalance"]) : 0)),
                                                 CustId = ((dr["CustId"] != DBNull.Value ? Convert.ToInt32(dr["CustId"].ToString()) : 0)),
                                                 CustJobNum = ((dr["CustJobNum"] != DBNull.Value ? Convert.ToString(dr["CustJobNum"]) : "")),
                                                 CustRefNum = ((dr["CustRefNum"] != DBNull.Value ? Convert.ToString(dr["CustRefNum"]) : "")),
                                                 CustName = ((dr["CustName"] != DBNull.Value ? Convert.ToString(dr["CustName"]) : "")),
                                                 CustContactName = ((dr["CustContactName"] != DBNull.Value ? Convert.ToString(dr["CustContactName"]) : "")),
                                                 CustAdd1 = ((dr["CustAdd1"] != DBNull.Value ? Convert.ToString(dr["CustAdd1"]) : "")),
                                                 CustAdd2 = ((dr["CustAdd2"] != DBNull.Value ? Convert.ToString(dr["CustAdd2"]) : "")),
                                                 CustCity = ((dr["CustCity"] != DBNull.Value ? Convert.ToString(dr["CustCity"]) : "")),
                                                 CustState = ((dr["CustState"] != DBNull.Value ? Convert.ToString(dr["CustState"]) : "")),
                                                 CustZip = ((dr["CustZip"] != DBNull.Value ? Convert.ToString(dr["CustZip"]) : "")),
                                                 CustPhone1 = ((dr["CustPhone1"] != DBNull.Value ? Convert.ToString(dr["CustPhone1"]) : "")),
                                                 CustPhone2 = ((dr["CustPhone2"] != DBNull.Value ? Convert.ToString(dr["CustPhone2"]) : "")),
                                                 CustFax = ((dr["CustFax"] != DBNull.Value ? Convert.ToString(dr["CustFax"]) : "")),
                                                 GenId = ((dr["GenId"] != DBNull.Value ? Convert.ToInt32(dr["GenId"].ToString()) : 0)),
                                                 GCRefNum = ((dr["GCRefNum"] != DBNull.Value ? Convert.ToString(dr["GCRefNum"]) : "")),
                                                 GCName = ((dr["GCName"] != DBNull.Value ? Convert.ToString(dr["GCName"]) : "")),
                                                 GCContactName = ((dr["GCContactName"] != DBNull.Value ? Convert.ToString(dr["GCContactName"]) : "")),
                                                 GCAdd1 = ((dr["GCAdd1"] != DBNull.Value ? Convert.ToString(dr["GCAdd1"]) : "")),
                                                 GCAdd2 = ((dr["GCAdd2"] != DBNull.Value ? Convert.ToString(dr["GCAdd2"]) : "")),
                                                 GCCity = ((dr["GCCity"] != DBNull.Value ? Convert.ToString(dr["GCCity"]) : "")),
                                                 GCState = ((dr["GCState"] != DBNull.Value ? Convert.ToString(dr["GCState"]) : "")),
                                                 GCZip = ((dr["GCZip"] != DBNull.Value ? Convert.ToString(dr["GCZip"]) : "")),
                                                 GCPhone1 = ((dr["GCPhone1"] != DBNull.Value ? Convert.ToString(dr["GCPhone1"]) : "")),
                                                 GCPhone2 = ((dr["GCPhone2"] != DBNull.Value ? Convert.ToString(dr["GCPhone2"]) : "")),
                                                 GCFax = ((dr["GCFax"] != DBNull.Value ? Convert.ToString(dr["GCFax"]) : "")),
                                                 OwnrName = ((dr["OwnrName"] != DBNull.Value ? Convert.ToString(dr["OwnrName"]) : "")),
                                                 OwnrAdd1 = ((dr["OwnrAdd1"] != DBNull.Value ? Convert.ToString(dr["OwnrAdd1"]) : "")),
                                                 OwnrAdd2 = ((dr["OwnrAdd2"] != DBNull.Value ? Convert.ToString(dr["OwnrAdd2"]) : "")),
                                                 OwnrCity = ((dr["OwnrCity"] != DBNull.Value ? Convert.ToString(dr["OwnrCity"]) : "")),
                                                 OwnrState = ((dr["OwnrState"] != DBNull.Value ? Convert.ToString(dr["OwnrState"]) : "")),
                                                 OwnrZip = ((dr["OwnrZip"] != DBNull.Value ? Convert.ToString(dr["OwnrZip"]) : "")),
                                                 OwnrPhone1 = ((dr["OwnrPhone1"] != DBNull.Value ? Convert.ToString(dr["OwnrPhone1"]) : "")),
                                                 Note = ((dr["Note"] != DBNull.Value ? Convert.ToString(dr["Note"]) : "")),
                                                 OwnrPhone2 = ((dr["OwnrPhone2"] != DBNull.Value ? Convert.ToString(dr["OwnrPhone2"]) : "")),
                                                 OwnrFax = ((dr["OwnrFax"] != DBNull.Value ? Convert.ToString(dr["OwnrFax"]) : "")),
                                                 PONum = ((dr["PONum"] != DBNull.Value ? Convert.ToString(dr["PONum"]) : "")),
                                                 BondNum = ((dr["BondNum"] != DBNull.Value ? Convert.ToString(dr["BondNum"]) : "")),
                                                 FolioNum = ((dr["FolioNum"] != DBNull.Value ? Convert.ToString(dr["FolioNum"]) : "")),
                                                 PrelimBox = ((dr["PrelimBox"] != DBNull.Value ? Convert.ToBoolean(dr["PrelimBox"]) : false)),
                                                 VerifyJob = ((dr["VerifyJob"] != DBNull.Value ? Convert.ToBoolean(dr["VerifyJob"]) : false)),
                                                 TitleVerifiedBox = ((dr["TitleVerifiedBox"] != DBNull.Value ? Convert.ToBoolean(dr["TitleVerifiedBox"]) : false)),
                                                 RushOrder = ((dr["RushOrder"] != DBNull.Value ? Convert.ToBoolean(dr["RushOrder"]) : false)),
                                                 GreenCard = ((dr["GreenCard"] != DBNull.Value ? Convert.ToBoolean(dr["GreenCard"]) : false)),
                                                 PublicJob = ((dr["PublicJob"] != DBNull.Value ? Convert.ToBoolean(dr["PublicJob"]) : false)),
                                                 ResidentialBox = ((dr["ResidentialBox"] != DBNull.Value ? Convert.ToBoolean(dr["ResidentialBox"]) : false)),
                                                 JointCk = ((dr["JointCk"] != DBNull.Value ? Convert.ToBoolean(dr["JointCk"]) : false)),
                                                 StartDate = ((dr["StartDate"] != DBNull.Value ? Convert.ToDateTime(dr["StartDate"]) : DateTime.MinValue)),
                                                 SpecialInstruction = ((dr["SpecialInstruction"] != DBNull.Value ? Convert.ToString(dr["SpecialInstruction"]) : "")),
                                                 EquipRate = ((dr["EquipRate"] != DBNull.Value ? Convert.ToString(dr["EquipRate"]) : "")),
                                                 EquipRental = ((dr["EquipRental"] != DBNull.Value ? Convert.ToString(dr["EquipRental"]) : "")),
                                                 BatchId = ((dr["BatchId"] != DBNull.Value ? Convert.ToInt32(dr["BatchId"].ToString()) : 0)),
                                                 SubmittedOn = ((dr["SubmittedOn"] != DBNull.Value ? Convert.ToDateTime(dr["SubmittedOn"]) : DateTime.MinValue)),
                                                 SubmittedByUserId = ((dr["SubmittedByUserId"] != DBNull.Value ? Convert.ToInt32(dr["SubmittedByUserId"].ToString()) : 0)),
                                                 PrelimASIS = ((dr["PrelimASIS"] != DBNull.Value ? Convert.ToBoolean(dr["PrelimASIS"]) : false)),
                                                 RevDate = ((dr["RevDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevDate"]) : DateTime.MinValue)),
                                                 PrivateJob = ((dr["PrivateJob"] != DBNull.Value ? Convert.ToBoolean(dr["PrivateJob"]) : false)),
                                                 RushOrderML = ((dr["RushOrderML"] != DBNull.Value ? Convert.ToBoolean(dr["RushOrderML"]) : false)),
                                                 MechanicLien = ((dr["MechanicLien"] != DBNull.Value ? Convert.ToBoolean(dr["MechanicLien"]) : false)),
                                                 StopNoticeBox = ((dr["StopNoticeBox"] != DBNull.Value ? Convert.ToBoolean(dr["StopNoticeBox"]) : false)),
                                                 EndDate = ((dr["EndDate"] != DBNull.Value ? Convert.ToDateTime(dr["EndDate"]) : DateTime.MinValue)),
                                                 Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"].ToString()) : 0)),
                                                 LenderName = ((dr["LenderName"] != DBNull.Value ? Convert.ToString(dr["LenderName"]) : "")),
                                                 LenderAdd1 = ((dr["LenderAdd1"] != DBNull.Value ? Convert.ToString(dr["LenderAdd1"]) : "")),
                                                 LenderAdd2 = ((dr["LenderAdd2"] != DBNull.Value ? Convert.ToString(dr["LenderAdd2"]) : "")),
                                                 LenderCity = ((dr["LenderCity"] != DBNull.Value ? Convert.ToString(dr["LenderCity"]) : "")),
                                                 LenderState = ((dr["LenderState"] != DBNull.Value ? Convert.ToString(dr["LenderState"]) : "")),
                                                 LenderZip = ((dr["LenderZip"] != DBNull.Value ? Convert.ToString(dr["LenderZip"]) : "")),
                                                 LenderPhone1 = ((dr["LenderPhone1"] != DBNull.Value ? Convert.ToString(dr["LenderPhone1"]) : "")),
                                                 LenderPhone2 = ((dr["LenderPhone2"] != DBNull.Value ? Convert.ToString(dr["LenderPhone2"]) : "")),
                                                 LenderFax = ((dr["LenderFax"] != DBNull.Value ? Convert.ToString(dr["LenderFax"]) : "")),
                                                 DesigneeName = ((dr["DesigneeName"] != DBNull.Value ? Convert.ToString(dr["DesigneeName"]) : "")),
                                                 DesigneeAdd1 = ((dr["DesigneeAdd1"] != DBNull.Value ? Convert.ToString(dr["DesigneeAdd1"]) : "")),
                                                 DesigneeAdd2 = ((dr["DesigneeAdd2"] != DBNull.Value ? Convert.ToString(dr["DesigneeAdd2"]) : "")),
                                                 DesigneeCity = ((dr["DesigneeCity"] != DBNull.Value ? Convert.ToString(dr["DesigneeCity"]) : "")),
                                                 DesigneeState = ((dr["DesigneeState"] != DBNull.Value ? Convert.ToString(dr["DesigneeState"]) : "")),
                                                 DesigneeZip = ((dr["DesigneeZip"] != DBNull.Value ? Convert.ToString(dr["DesigneeZip"]) : "")),
                                                 DesigneePhone1 = ((dr["DesigneePhone1"] != DBNull.Value ? Convert.ToString(dr["DesigneePhone1"]) : "")),
                                                 DesigneePhone2 = ((dr["DesigneePhone2"] != DBNull.Value ? Convert.ToString(dr["DesigneePhone2"]) : "")),
                                                 DesigneeFax = ((dr["DesigneeFax"] != DBNull.Value ? Convert.ToString(dr["DesigneeFax"]) : "")),
                                                 LienRelease = ((dr["LienRelease"] != DBNull.Value ? Convert.ToBoolean(dr["LienRelease"]) : false)),
                                                 PaymentBond = ((dr["PaymentBond"] != DBNull.Value ? Convert.ToBoolean(dr["PaymentBond"]) : false)),
                                                 SameAsCust = ((dr["SameAsCust"] != DBNull.Value ? Convert.ToBoolean(dr["SameAsCust"]) : false)),
                                                 NoticeSent = ((dr["NoticeSent"] != DBNull.Value ? Convert.ToDateTime(dr["NoticeSent"]) : DateTime.MinValue)),
                                                 VerifiedDate = ((dr["VerifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["VerifiedDate"]) : DateTime.MinValue)),
                                                 BackUpBox = ((dr["BackUpBox"] != DBNull.Value ? Convert.ToBoolean(dr["BackUpBox"]) : false)),
                                                 FederalJob = ((dr["FederalJob"] != DBNull.Value ? Convert.ToBoolean(dr["FederalJob"]) : false)),
                                                 ErrorDescription = ((dr["ErrorDescription"] != DBNull.Value ? Convert.ToString(dr["ErrorDescription"]) : "")),
                                                 IsError = ((dr["IsError"] != DBNull.Value ? Convert.ToBoolean(dr["IsError"]) : false)),
                                             }).ToList();
            return BatchJobList.First();
        }

        public static DataTable GetLienBatchJobForWebPlacements()
        {
            try
            {
                string sql = "Select * from batchJob where BatchId = -1";

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        //Print LienBatch WebPlacements
        public string PrintLienBatches(LienBatchWebPlacementsVM objLienBatchWebPlacementsVM)
        {
            string pdfPath = null;
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                //user.SettingsFolder = user.SettingsFolder + "\\" ;

                var mysproc = new uspbo_LiensDataEntry_BatchWebPlacements();
                mysproc.UserId = user.Id;
                mysproc.UserName = user.UserName;
                DBO.Provider.ExecNonQuery(mysproc);

                var mysql = "Select * from vwBatchJobExport where BatchId = -1 and iserror = 1";

                TableView myview = DBO.Provider.GetTableView(mysql);

                DataSet myds = DBO.Provider.GetDataSet(mysql);

                DataTable mytbl = myds.Tables[0];

                CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;

                if (myview.Count > 0)
                {
                    pdfPath = Globals.RenderC1Report(user, mytbl, "LienReports.xml", "WebPlacementErrorList", mymode);
                }
                else
                {
                    pdfPath = "No WebPlacementError";
                }
            }

            catch (Exception ex)
            {
                return "";
            }
            return pdfPath;
        }

        #endregion
    }
}