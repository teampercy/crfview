﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.CRFDB.VIEWS;
using CRF.BLL.Providers;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADvwBatchJobExport
    {
        #region Properties
        public int ClientId { get; set; }
        public string ClientCode { get; set; }
        public string BranchNum { get; set; }
        public string JobNum { get; set; }
        public string JobName { get; set; }
        public string APNNum { get; set; }
        public string AZLotAllocate { get; set; }
        public string JobAdd1 { get; set; }
        public string JobAdd2 { get; set; }
        public string JobCity { get; set; }
        public string JobState { get; set; }
        public string JobZip { get; set; }
        public string JobCounty { get; set; }
        public decimal EstBalance { get; set; }
        public int CustId { get; set; }
        public string CustJobNum { get; set; }
        public string CustRefNum { get; set; }
        public string CustName { get; set; }
        public string CustContactName { get; set; }
        public string CustAdd1 { get; set; }
        public string CustAdd2 { get; set; }
        public string CustCity { get; set; }
        public string CustState { get; set; }
        public string CustZip { get; set; }
        public string CustPhone1 { get; set; }
        public string CustPhone2 { get; set; }
        public string CustFax { get; set; }
        public int GenId { get; set; }
        public string GCRefNum { get; set; }
        public string GCName { get; set; }
        public string GCContactName { get; set; }
        public string GCAdd1 { get; set; }
        public string GCAdd2 { get; set; }
        public string GCCity { get; set; }
        public string GCState { get; set; }
        public string GCZip { get; set; }
        public string GCPhone1 { get; set; }
        public string GCPhone2 { get; set; }
        public string GCFax { get; set; }
        public string OwnrName { get; set; }
        public string OwnrAdd1 { get; set; }
        public string OwnrAdd2 { get; set; }
        public string OwnrCity { get; set; }
        public string OwnrState { get; set; }
        public string OwnrZip { get; set; }
        public string OwnrPhone1 { get; set; }
        public string Note { get; set; }
        public string OwnrPhone2 { get; set; }
        public string OwnrFax { get; set; }
        public string PONum { get; set; }
        public string BondNum { get; set; }
        public string FolioNum { get; set; }
        public bool PrelimBox { get; set; }
        public bool VerifyJob { get; set; }
        public bool TitleVerifiedBox { get; set; }
        public bool RushOrder { get; set; }
        public bool GreenCard { get; set; }
        public bool PublicJob { get; set; }
        public bool ResidentialBox { get; set; }
        public bool JointCk { get; set; }
        public DateTime StartDate { get; set; }
        public string SpecialInstruction { get; set; }
        public string EquipRate { get; set; }
        public string EquipRental { get; set; }
        public int BatchId { get; set; }
        public DateTime SubmittedOn { get; set; }
        public int SubmittedByUserId { get; set; }
        public bool PrelimASIS { get; set; }
        public DateTime RevDate { get; set; }
        public bool PrivateJob { get; set; }
        public bool RushOrderML { get; set; }
        public bool MechanicLien { get; set; }
        public bool StopNoticeBox { get; set; }
        public DateTime EndDate { get; set; }
        public int Id { get; set; }
        public string LenderName { get; set; }
        public string LenderAdd1 { get; set; }
        public string LenderAdd2 { get; set; }
        public string LenderCity { get; set; }
        public string LenderState { get; set; }
        public string LenderZip { get; set; }
        public string LenderPhone1 { get; set; }
        public string LenderPhone2 { get; set; }
        public string LenderFax { get; set; }
        public string DesigneeName { get; set; }
        public string DesigneeAdd1 { get; set; }
        public string DesigneeAdd2 { get; set; }
        public string DesigneeCity { get; set; }
        public string DesigneeState { get; set; }
        public string DesigneeZip { get; set; }
        public string DesigneePhone1 { get; set; }
        public string DesigneePhone2 { get; set; }
        public string DesigneeFax { get; set; }
        public bool LienRelease { get; set; }
        public bool PaymentBond { get; set; }
        public bool SameAsCust { get; set; }
        public DateTime NoticeSent { get; set; }
        public DateTime VerifiedDate { get; set; }
        public bool BackUpBox { get; set; }
        public bool FederalJob { get; set; }
        public string ErrorDescription { get; set; }
        public bool IsError { get; set; }
        public string UserName { get; set; }
        public string BatchType { get; set; }
        public string BatchStatus { get; set; }
        #endregion

        #region CRUD
        public static ADvwBatchJobExport ReadBatchJobByBatchJobId(string BatchJobId)
        {
            try
            {
                string mysql = "Select * from vwBatchJobExport Where Id = " + BatchJobId;
                var AddressTableView = DBO.Provider.GetTableView(mysql);
                DataTable dt = AddressTableView.ToTable();
                if(dt.Rows.Count > 0)
                {
                    return ConvertDataTableToClass(dt);
                }
                else
                {
                    return new ADvwBatchJobExport();
                }               
            }
            catch (Exception ex)
            {
                return new ADvwBatchJobExport();
            }
        }


    

        #endregion

        #region Local Methods
        public static ADvwBatchJobExport ConvertDataTableToClass(DataTable dt)
        {
            List<ADvwBatchJobExport> BatchJobList = (from DataRow dr in dt.Rows
                                               select new ADvwBatchJobExport()
                                               {
                                                   ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"].ToString()) : 0)),
                                                   ClientCode = ((dr["ClientCode"] != DBNull.Value ? Convert.ToString(dr["ClientCode"]) : "")),
                                                   BranchNum = ((dr["BranchNum"] != DBNull.Value ? Convert.ToString(dr["BranchNum"]) : "")),
                                                   JobNum = ((dr["JobNum"] != DBNull.Value ? Convert.ToString(dr["JobNum"]) : "")),
                                                   JobName = ((dr["JobName"] != DBNull.Value ? Convert.ToString(dr["JobName"]) : "")),
                                                   APNNum = ((dr["APNNum"] != DBNull.Value ? Convert.ToString(dr["APNNum"]) : "")),
                                                   AZLotAllocate = ((dr["AZLotAllocate"] != DBNull.Value ? Convert.ToString(dr["AZLotAllocate"]) : "")),
                                                   JobAdd1 = ((dr["JobAdd1"] != DBNull.Value ? Convert.ToString(dr["JobAdd1"]) : "")),
                                                   JobAdd2 = ((dr["JobAdd2"] != DBNull.Value ? Convert.ToString(dr["JobAdd2"]) : "")),
                                                   JobCity = ((dr["JobCity"] != DBNull.Value ? Convert.ToString(dr["JobCity"]) : "")),
                                                   JobState = ((dr["JobState"] != DBNull.Value ? Convert.ToString(dr["JobState"]) : "")),
                                                   JobZip = ((dr["JobZip"] != DBNull.Value ? Convert.ToString(dr["JobZip"]) : "")),
                                                   JobCounty = ((dr["JobCounty"] != DBNull.Value ? Convert.ToString(dr["JobCounty"]) : "")),
                                                   EstBalance = ((dr["EstBalance"] != DBNull.Value ? Convert.ToDecimal(dr["EstBalance"]) : 0)),
                                                   CustId = ((dr["CustId"] != DBNull.Value ? Convert.ToInt32(dr["CustId"].ToString()) : 0)),
                                                   CustJobNum = ((dr["CustJobNum"] != DBNull.Value ? Convert.ToString(dr["CustJobNum"]) : "")),
                                                   CustRefNum = ((dr["CustRefNum"] != DBNull.Value ? Convert.ToString(dr["CustRefNum"]) : "")),
                                                   CustName = ((dr["CustName"] != DBNull.Value ? Convert.ToString(dr["CustName"]) : "")),
                                                   CustContactName = ((dr["CustContactName"] != DBNull.Value ? Convert.ToString(dr["CustContactName"]) : "")),
                                                   CustAdd1 = ((dr["CustAdd1"] != DBNull.Value ? Convert.ToString(dr["CustAdd1"]) : "")),
                                                   CustAdd2 = ((dr["CustAdd2"] != DBNull.Value ? Convert.ToString(dr["CustAdd2"]) : "")),
                                                   CustCity = ((dr["CustCity"] != DBNull.Value ? Convert.ToString(dr["CustCity"]) : "")),
                                                   CustState = ((dr["CustState"] != DBNull.Value ? Convert.ToString(dr["CustState"]) : "")),
                                                   CustZip = ((dr["CustZip"] != DBNull.Value ? Convert.ToString(dr["CustZip"]) : "")),
                                                   CustPhone1 = ((dr["CustPhone1"] != DBNull.Value ? Convert.ToString(dr["CustPhone1"]) : "")),
                                                   CustPhone2 = ((dr["CustPhone2"] != DBNull.Value ? Convert.ToString(dr["CustPhone2"]) : "")),
                                                   CustFax = ((dr["CustFax"] != DBNull.Value ? Convert.ToString(dr["CustFax"]) : "")),
                                                   GenId = ((dr["GenId"] != DBNull.Value ? Convert.ToInt32(dr["GenId"].ToString()) : 0)),
                                                   GCRefNum = ((dr["GCRefNum"] != DBNull.Value ? Convert.ToString(dr["GCRefNum"]) : "")),
                                                   GCName = ((dr["GCName"] != DBNull.Value ? Convert.ToString(dr["GCName"]) : "")),
                                                   GCContactName = ((dr["GCContactName"] != DBNull.Value ? Convert.ToString(dr["GCContactName"]) : "")),
                                                   GCAdd1 = ((dr["GCAdd1"] != DBNull.Value ? Convert.ToString(dr["GCAdd1"]) : "")),
                                                   GCAdd2 = ((dr["GCAdd2"] != DBNull.Value ? Convert.ToString(dr["GCAdd2"]) : "")),
                                                   GCCity = ((dr["GCCity"] != DBNull.Value ? Convert.ToString(dr["GCCity"]) : "")),
                                                   GCState = ((dr["GCState"] != DBNull.Value ? Convert.ToString(dr["GCState"]) : "")),
                                                   GCZip = ((dr["GCZip"] != DBNull.Value ? Convert.ToString(dr["GCZip"]) : "")),
                                                   GCPhone1 = ((dr["GCPhone1"] != DBNull.Value ? Convert.ToString(dr["GCPhone1"]) : "")),
                                                   GCPhone2 = ((dr["GCPhone2"] != DBNull.Value ? Convert.ToString(dr["GCPhone2"]) : "")),
                                                   GCFax = ((dr["GCFax"] != DBNull.Value ? Convert.ToString(dr["GCFax"]) : "")),
                                                   OwnrName = ((dr["OwnrName"] != DBNull.Value ? Convert.ToString(dr["OwnrName"]) : "")),
                                                   OwnrAdd1 = ((dr["OwnrAdd1"] != DBNull.Value ? Convert.ToString(dr["OwnrAdd1"]) : "")),
                                                   OwnrAdd2 = ((dr["OwnrAdd2"] != DBNull.Value ? Convert.ToString(dr["OwnrAdd2"]) : "")),
                                                   OwnrCity = ((dr["OwnrCity"] != DBNull.Value ? Convert.ToString(dr["OwnrCity"]) : "")),
                                                   OwnrState = ((dr["OwnrState"] != DBNull.Value ? Convert.ToString(dr["OwnrState"]) : "")),
                                                   OwnrZip = ((dr["OwnrZip"] != DBNull.Value ? Convert.ToString(dr["OwnrZip"]) : "")),
                                                   OwnrPhone1 = ((dr["OwnrPhone1"] != DBNull.Value ? Convert.ToString(dr["OwnrPhone1"]) : "")),
                                                   Note = ((dr["Note"] != DBNull.Value ? Convert.ToString(dr["Note"]) : "")),
                                                   OwnrPhone2 = ((dr["OwnrPhone2"] != DBNull.Value ? Convert.ToString(dr["OwnrPhone2"]) : "")),
                                                   OwnrFax = ((dr["OwnrFax"] != DBNull.Value ? Convert.ToString(dr["OwnrFax"]) : "")),
                                                   PONum = ((dr["PONum"] != DBNull.Value ? Convert.ToString(dr["PONum"]) : "")),
                                                   BondNum = ((dr["BondNum"] != DBNull.Value ? Convert.ToString(dr["BondNum"]) : "")),
                                                   FolioNum = ((dr["FolioNum"] != DBNull.Value ? Convert.ToString(dr["FolioNum"]) : "")),
                                                   PrelimBox = ((dr["PrelimBox"] != DBNull.Value ? Convert.ToBoolean(dr["PrelimBox"]) : false)),
                                                   VerifyJob = ((dr["VerifyJob"] != DBNull.Value ? Convert.ToBoolean(dr["VerifyJob"]) : false)),
                                                   TitleVerifiedBox = ((dr["TitleVerifiedBox"] != DBNull.Value ? Convert.ToBoolean(dr["TitleVerifiedBox"]) : false)),
                                                   RushOrder = ((dr["RushOrder"] != DBNull.Value ? Convert.ToBoolean(dr["RushOrder"]) : false)),
                                                   GreenCard = ((dr["GreenCard"] != DBNull.Value ? Convert.ToBoolean(dr["GreenCard"]) : false)),
                                                   PublicJob = ((dr["PublicJob"] != DBNull.Value ? Convert.ToBoolean(dr["PublicJob"]) : false)),
                                                   ResidentialBox = ((dr["ResidentialBox"] != DBNull.Value ? Convert.ToBoolean(dr["ResidentialBox"]) : false)),
                                                   JointCk = ((dr["JointCk"] != DBNull.Value ? Convert.ToBoolean(dr["JointCk"]) : false)),
                                                   StartDate = ((dr["StartDate"] != DBNull.Value ? Convert.ToDateTime(dr["StartDate"]) : DateTime.MinValue)),
                                                   SpecialInstruction = ((dr["SpecialInstruction"] != DBNull.Value ? Convert.ToString(dr["SpecialInstruction"]) : "")),
                                                   EquipRate = ((dr["EquipRate"] != DBNull.Value ? Convert.ToString(dr["EquipRate"]) : "")),
                                                   EquipRental = ((dr["EquipRental"] != DBNull.Value ? Convert.ToString(dr["EquipRental"]) : "")),
                                                   BatchId = ((dr["BatchId"] != DBNull.Value ? Convert.ToInt32(dr["BatchId"].ToString()) : 0)),
                                                   SubmittedOn = ((dr["SubmittedOn"] != DBNull.Value ? Convert.ToDateTime(dr["SubmittedOn"]) : DateTime.MinValue)),
                                                   SubmittedByUserId = ((dr["SubmittedByUserId"] != DBNull.Value ? Convert.ToInt32(dr["SubmittedByUserId"].ToString()) : 0)),
                                                   PrelimASIS = ((dr["PrelimASIS"] != DBNull.Value ? Convert.ToBoolean(dr["PrelimASIS"]) : false)),
                                                   RevDate = ((dr["RevDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevDate"]) : DateTime.MinValue)),
                                                   PrivateJob = ((dr["PrivateJob"] != DBNull.Value ? Convert.ToBoolean(dr["PrivateJob"]) : false)),
                                                   RushOrderML = ((dr["RushOrderML"] != DBNull.Value ? Convert.ToBoolean(dr["RushOrderML"]) : false)),
                                                   MechanicLien = ((dr["MechanicLien"] != DBNull.Value ? Convert.ToBoolean(dr["MechanicLien"]) : false)),
                                                   StopNoticeBox = ((dr["StopNoticeBox"] != DBNull.Value ? Convert.ToBoolean(dr["StopNoticeBox"]) : false)),
                                                   EndDate = ((dr["EndDate"] != DBNull.Value ? Convert.ToDateTime(dr["EndDate"]) : DateTime.MinValue)),
                                                   Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"].ToString()) : 0)),
                                                   LenderName = ((dr["LenderName"] != DBNull.Value ? Convert.ToString(dr["LenderName"]) : "")),
                                                   LenderAdd1 = ((dr["LenderAdd1"] != DBNull.Value ? Convert.ToString(dr["LenderAdd1"]) : "")),
                                                   LenderAdd2 = ((dr["LenderAdd2"] != DBNull.Value ? Convert.ToString(dr["LenderAdd2"]) : "")),
                                                   LenderCity = ((dr["LenderCity"] != DBNull.Value ? Convert.ToString(dr["LenderCity"]) : "")),
                                                   LenderState = ((dr["LenderState"] != DBNull.Value ? Convert.ToString(dr["LenderState"]) : "")),
                                                   LenderZip = ((dr["LenderZip"] != DBNull.Value ? Convert.ToString(dr["LenderZip"]) : "")),
                                                   LenderPhone1 = ((dr["LenderPhone1"] != DBNull.Value ? Convert.ToString(dr["LenderPhone1"]) : "")),
                                                   LenderPhone2 = ((dr["LenderPhone2"] != DBNull.Value ? Convert.ToString(dr["LenderPhone2"]) : "")),
                                                   LenderFax = ((dr["LenderFax"] != DBNull.Value ? Convert.ToString(dr["LenderFax"]) : "")),
                                                   DesigneeName = ((dr["DesigneeName"] != DBNull.Value ? Convert.ToString(dr["DesigneeName"]) : "")),
                                                   DesigneeAdd1 = ((dr["DesigneeAdd1"] != DBNull.Value ? Convert.ToString(dr["DesigneeAdd1"]) : "")),
                                                   DesigneeAdd2 = ((dr["DesigneeAdd2"] != DBNull.Value ? Convert.ToString(dr["DesigneeAdd2"]) : "")),
                                                   DesigneeCity = ((dr["DesigneeCity"] != DBNull.Value ? Convert.ToString(dr["DesigneeCity"]) : "")),
                                                   DesigneeState = ((dr["DesigneeState"] != DBNull.Value ? Convert.ToString(dr["DesigneeState"]) : "")),
                                                   DesigneeZip = ((dr["DesigneeZip"] != DBNull.Value ? Convert.ToString(dr["DesigneeZip"]) : "")),
                                                   DesigneePhone1 = ((dr["DesigneePhone1"] != DBNull.Value ? Convert.ToString(dr["DesigneePhone1"]) : "")),
                                                   DesigneePhone2 = ((dr["DesigneePhone2"] != DBNull.Value ? Convert.ToString(dr["DesigneePhone2"]) : "")),
                                                   DesigneeFax = ((dr["DesigneeFax"] != DBNull.Value ? Convert.ToString(dr["DesigneeFax"]) : "")),
                                                   LienRelease = ((dr["LienRelease"] != DBNull.Value ? Convert.ToBoolean(dr["LienRelease"]) : false)),
                                                   PaymentBond = ((dr["PaymentBond"] != DBNull.Value ? Convert.ToBoolean(dr["PaymentBond"]) : false)),
                                                   SameAsCust = ((dr["SameAsCust"] != DBNull.Value ? Convert.ToBoolean(dr["SameAsCust"]) : false)),
                                                   NoticeSent = ((dr["NoticeSent"] != DBNull.Value ? Convert.ToDateTime(dr["NoticeSent"]) : DateTime.MinValue)),
                                                   VerifiedDate = ((dr["VerifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["VerifiedDate"]) : DateTime.MinValue)),
                                                   BackUpBox = ((dr["BackUpBox"] != DBNull.Value ? Convert.ToBoolean(dr["BackUpBox"]) : false)),
                                                   FederalJob = ((dr["FederalJob"] != DBNull.Value ? Convert.ToBoolean(dr["FederalJob"]) : false)),
                                                   ErrorDescription = ((dr["ErrorDescription"] != DBNull.Value ? Convert.ToString(dr["ErrorDescription"]) : "")),
                                                   IsError = ((dr["IsError"] != DBNull.Value ? Convert.ToBoolean(dr["IsError"]) : false)),
                                                   UserName = ((dr["UserName"] != DBNull.Value ? Convert.ToString(dr["UserName"]) : "")),                                                   
                                                   BatchType = ((dr["BatchType"] != DBNull.Value ? Convert.ToString(dr["BatchType"]) : "")),
                                                   BatchStatus = ((dr["BatchStatus"] != DBNull.Value ? Convert.ToString(dr["BatchStatus"]) : "")),
                                               }).ToList();
            return BatchJobList.First();
        }
        #endregion
    }
}
