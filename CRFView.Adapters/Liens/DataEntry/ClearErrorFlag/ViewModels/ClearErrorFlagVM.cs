﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace CRFView.Adapters.Liens.DataEntry.ClearErrorFlag.ViewModels
{
    public class ClearErrorFlagVM
    {
        #region Properties

        public string ByBatchJobId { get; set; }
        
        public ADvwBatchJobExport ObjADvwBatchJobExport { get; set; }
   
        #endregion


        public ClearErrorFlagVM()
        {
            ObjADvwBatchJobExport = new ADvwBatchJobExport();
             
        }
    }
}
