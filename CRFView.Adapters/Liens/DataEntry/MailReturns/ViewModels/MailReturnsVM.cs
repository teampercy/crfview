﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;

namespace CRFView.Adapters.Liens.DataEntry.MailReturns.ViewModels
{
    public class MailReturnsVM
    {
        public MailReturnsVM()
        {
            BatchMailReturnList = new List<ADBatchMailReturn>();
        }

        public string RbtActionChangeOption { get; set; }
        public string CertNum { get; set; }
        public List<ADBatchMailReturn> BatchMailReturnList { get; set; }
        public DataTable dtBatchMailReturnList { get; set; }
        
        public DataTable dtGetJobNoticeSentDetails { get; set; }
    }
}
