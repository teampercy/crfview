﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using CRF.BLL.Providers;
using HDS.DAL.Providers;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Liens.DataEntry.MailReturns.ViewModels;

namespace CRFView.Adapters
{
    public class ADBatchMailReturn
    {
        #region Properties
        public int Id { get; set; }
        public int BatchId { get; set; }
        public string CertNum { get; set; }
        public string MailStatus { get; set; }
        public int JobId { get; set; }
        public int JobNoticeHistoryId { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsError { get; set; }
        public string ErrorDescription { get; set; }
        public string LegalPartyType { get; set; }
        #endregion

        #region CRUD
        public static DataTable GetBatchMailReturnDetailDt()
        {
            try
            {
                string mysql = "Select Id,BatchId, JobId, CertNum, MailStatus, LegalPartytype, JobNoticeHistoryId,DateCreated,IsError,ErrorDescription from BatchMailReturn where batchid = -1";

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(mysql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        public static List<ADBatchMailReturn> GetBatchMailReturnDetailList(DataTable dt)
        {
            List<ADBatchMailReturn> Notes = (from DataRow dr in dt.Rows
                                             select new ADBatchMailReturn()
                                             {
                                                 Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                                 BatchId = ((dr["BatchId"] != DBNull.Value ? Convert.ToInt32(dr["BatchId"]) : 0)),
                                                 CertNum = ((dr["CertNum"] != DBNull.Value ? dr["CertNum"].ToString() : "")),
                                                 MailStatus = ((dr["MailStatus"] != DBNull.Value ? dr["MailStatus"].ToString() : "")),
                                                 JobId = ((dr["JobId"] != DBNull.Value ? Convert.ToInt32(dr["JobId"]) : 0)),
                                                 JobNoticeHistoryId = ((dr["JobNoticeHistoryId"] != DBNull.Value ? Convert.ToInt32(dr["JobNoticeHistoryId"]) : 0)),
                                                 DateCreated = ((dr["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dr["DateCreated"]) : DateTime.MinValue)),
                                                 IsError = ((dr["IsError"] != DBNull.Value ? Convert.ToBoolean(dr["IsError"]) : false)),
                                                 ErrorDescription = ((dr["ErrorDescription"] != DBNull.Value ? dr["ErrorDescription"].ToString() : "")),
                                                 LegalPartyType = ((dr["LegalPartyType"] != DBNull.Value ? dr["LegalPartyType"].ToString() : "")),
                                             }).ToList();
            return Notes;
        }

        public static DataTable GetJobNoticeSentDetails(string CertNum)
        {
            try
            {
                string sql = "Select JobId, NoticeHistoryId, TypeCode from vwJobNoticeSent where CertNum = '" + CertNum + "'";


                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        public static ADBatchMailReturn ReadBatchMailReturn(int Id)
        {
            BatchMailReturn myentity = new BatchMailReturn();
            ITable myentityItable = myentity;
            DBO.Provider.Read(Id.ToString(), ref myentityItable);
            myentity = (BatchMailReturn)myentityItable;
            AutoMapper.Mapper.CreateMap<BatchMailReturn, ADBatchMailReturn>();
            return (AutoMapper.Mapper.Map<ADBatchMailReturn>(myentity));
        }

        public static int SaveBatchMailReturn(ADBatchMailReturn objUpdatedADBatchMailReturn)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADBatchMailReturn, BatchMailReturn>();
                ITable tblBatchMailReturn;
                BatchMailReturn objBatchMailReturn;

                if (objUpdatedADBatchMailReturn.Id != 0)
                {
                    ADBatchMailReturn objADBatchMailReturnOriginal = ReadBatchMailReturn(Convert.ToInt32(objUpdatedADBatchMailReturn.Id));

                    if (objUpdatedADBatchMailReturn.BatchId.ToString() == "" || objUpdatedADBatchMailReturn.BatchId == 0)
                    {
                    }
                    else
                    {
                        objADBatchMailReturnOriginal.BatchId = objUpdatedADBatchMailReturn.BatchId;
                    }

                    if (objUpdatedADBatchMailReturn.CertNum == "" || objUpdatedADBatchMailReturn.CertNum == null)
                    {
                    }
                    else
                    {
                        objADBatchMailReturnOriginal.CertNum = objUpdatedADBatchMailReturn.CertNum;
                    }

                    if (objUpdatedADBatchMailReturn.JobId.ToString() == "" || objUpdatedADBatchMailReturn.JobId == 0)
                    {
                    }
                    else
                    {
                        objADBatchMailReturnOriginal.JobId = objUpdatedADBatchMailReturn.JobId;
                    }

                    if (objUpdatedADBatchMailReturn.JobNoticeHistoryId.ToString() == "" || objUpdatedADBatchMailReturn.JobNoticeHistoryId == 0)
                    {
                    }
                    else
                    {
                        objADBatchMailReturnOriginal.JobNoticeHistoryId = objUpdatedADBatchMailReturn.JobNoticeHistoryId;
                    }

                    if (objUpdatedADBatchMailReturn.MailStatus == "" || objUpdatedADBatchMailReturn.MailStatus == null)
                    {
                    }
                    else
                    {
                        objADBatchMailReturnOriginal.MailStatus = objUpdatedADBatchMailReturn.MailStatus;
                    }

                    if (objUpdatedADBatchMailReturn.LegalPartyType == "" || objUpdatedADBatchMailReturn.LegalPartyType == null)
                    {
                    }
                    else
                    {
                        objADBatchMailReturnOriginal.LegalPartyType = objUpdatedADBatchMailReturn.LegalPartyType;
                    }

                    objADBatchMailReturnOriginal.DateCreated = DateTime.Now;

                    objBatchMailReturn = AutoMapper.Mapper.Map<BatchMailReturn>(objADBatchMailReturnOriginal);
                    tblBatchMailReturn = (ITable)objBatchMailReturn;
                    DBO.Provider.Update(ref tblBatchMailReturn);
                }
                else
                {
                    objUpdatedADBatchMailReturn.DateCreated = DateTime.Now;
                    objBatchMailReturn = AutoMapper.Mapper.Map<BatchMailReturn>(objUpdatedADBatchMailReturn);
                    tblBatchMailReturn = (ITable)objBatchMailReturn;
                    DBO.Provider.Create(ref tblBatchMailReturn);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int PostBatchMailReturns(MailReturnsVM objMailReturnsVM)
        {
            try
            {

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var MYSPROC = new uspbo_LiensDataEntry_PostMailReturns();
                MYSPROC.UserId = user.Id;
                MYSPROC.UserName = user.UserName;
                DBO.Provider.ExecNonQuery(MYSPROC);
               return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }
        #endregion

    }
}
