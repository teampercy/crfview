﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Liens.Reports.LienReports.ViewModels
{
    public class LienReportsVM
    {
        public LienReportsVM()
        {
            ClientIdList = CommonBindList.GetClientIdList();
            SalesTeamIdList = CommonBindList.GetSalesTeamList();
            JobDeskList = CommonBindList.GetJobDesks();
            JobDeskGroupList = CommonBindList.GetJobDesksGroup();
            StatusGroupList = CommonBindList.GetLiensStatusGroup(); 
            AllStatusList = CommonBindList.GetJobStatusIdList();
        }



        public string RbtPrintOption { get; set; }
        public string RbtReportCategoryOption { get; set; }


        public string ddlReportId { get; set; }
      

        public bool chkAllClients { get; set; }
        public IEnumerable<SelectListItem> ClientIdList { get; set; }
        public string ddlClientId { get; set; }

        public bool chkAllDesks { get; set; }
        
        public IEnumerable<SelectListItem> JobDeskList { get; set; }
        public string ddlDeskId { get; set; }
        
        public bool chkAllDesksGroup { get; set; }
        
        public IEnumerable<SelectListItem> JobDeskGroupList { get; set; }
        public string ddlDeskGroupId { get; set; }

        public bool chkAllSman { get; set; }
        public IEnumerable<SelectListItem> SalesTeamIdList { get; set; }
        public string ddlSalesTeamId { get; set; }

        public bool chkByAssignDate { get; set; }
        public DateTime AssignDateFrom { get; set; }
        public DateTime AssignDateThru { get; set; }

        public bool chkByLienAssignDate { get; set; }
        public DateTime LienAssignDateFrom { get; set; }
        public DateTime LienAssignDateThru { get; set; }

        public bool chkByNoticeSentDate { get; set; }
        public DateTime NoticeSentDateFrom { get; set; }
        public DateTime NoticeSentDateThru { get; set; }

        public bool chkByLienDate { get; set; }
        public DateTime LienDateFrom { get; set; }
        public DateTime LienDateThru { get; set; }

        public bool chkByTrxDate { get; set; }
        public DateTime TrxDateFrom { get; set; }
        public DateTime TrxDateThru { get; set; }

        public bool chkByJobState { get; set; }
        public string JobState { get; set; }

        public bool chkAllStatusGroup { get; set; }
        public IEnumerable<SelectListItem> StatusGroupList { get; set; }
        public string JobStatusGroupId { get; set; }

        public bool chkAllStatus { get; set; }
        public IEnumerable<SelectListItem> AllStatusList { get; set; }
        public string ddlStatusId { get; set; }







        public bool chkAllJobIds { get; set; }
        public string JobId { get; set; }





        public string value { get; set; }

        public bool valuebool { get; set; }

      
    }
}
