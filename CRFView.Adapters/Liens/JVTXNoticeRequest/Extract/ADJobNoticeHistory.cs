﻿using CRF.BLL;
using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADJobNoticeHistory
    {

        #region Properties
        public int Id { get; set; }
        public int JobNoticeRequestId { get; set; }
        public string CertNum { get; set; }
        public DateTime LastPrinted { get; set; }
        public bool IsNoticePrinted { get; set; }
        public bool IsPrimary { get; set; }
        public int CustId { get; set; }
        public int GCID { get; set; }
        public int LegalPartyId { get; set; }
        public string NoticeImage { get; set; }
        public string TypeCode { get; set; }
        public string LegalPartyType { get; set; }
        public string LegalPartyName { get; set; }
        public string CoverName { get; set; }
        public string CoverAdd1 { get; set; }
        public string CoverAdd2 { get; set; }
        public string CoverCity { get; set; }
        public string CoverState { get; set; }
        public string CoverZip { get; set; }
        public bool HasErrors { get; set; }
        public string ErrorDescr { get; set; }
        public string HistoryDisplayOrder { get; set; }
        public bool IsRegularPostage { get; set; }
        public bool IsMailingLogPostage { get; set; }
        public string PDFImage { get; set; }
        public bool MLAgent { get; set; }
        public string MailStatus { get; set; }
        public DateTime MailStatusDate { get; set; }
        public bool IsNonUSAddr { get; set; }
        public DateTime DeliveredDate { get; set; }
        #endregion

        #region CRUD
        public static ADJobNoticeHistory ReadJobNoticeHistory(int Id)
        {
            JobNoticeHistory myentity = new JobNoticeHistory();
            ITable myentityItable = myentity;
            DBO.Provider.Read(Id.ToString(), ref myentityItable);
            myentity = (JobNoticeHistory)myentityItable;
            AutoMapper.Mapper.CreateMap<JobNoticeHistory, ADJobNoticeHistory>();
            return (AutoMapper.Mapper.Map<ADJobNoticeHistory>(myentity));
        }

        public static bool FreeBillingUpdate(string JobNoticeHistoryId)
        {
            try
            {
               // CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                var sproc = new uspbo_Jobs_FreeBillNoticeRequest();
                sproc.HistoryId = Convert.ToInt32(JobNoticeHistoryId);


                DBO.Provider.ExecNonQuery(sproc);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //public static string GetPrintNoticeCopy(int JobNoticeHistoryId, int JobId)
        //{
        //    try
        //    {
        //        CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
        //        CRF.BLL.COMMON.StandardReport myreport = new CRF.BLL.COMMON.StandardReport();
        //         CRF.BLL.Liens.Reports.JobPrelimNotice mynotice = new CRF.BLL.Liens.Reports.JobPrelimNotice(user);
        //        JobNoticeRequestForms myform = new JobNoticeRequestForms();
        //        ITable tblI = myform;
        //        // Dim myform As New TABLES.JobNoticeRequestForms

        //        var sproc = new uspbo_Jobs_GetJobNoticeHistoryInfo();
        //        sproc.JobNoticeHistoryId = JobNoticeHistoryId;

        //        var myview = DBO.Provider.GetTableView(sproc);
        //        CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;

        //        string myfilename = "";
        //        myfilename = JobId.ToString();
        //        myfilename += "-" + JobNoticeHistoryId + ".PDF";
        //        string mydest = CRF.BLL.CRFView.CRFView.GetKeyValue("JOBNOTICEIMAGEPATH") + myfilename;

        //        if (System.IO.File.Exists(mydest) == true)
        //        {
        //            return mydest;
        //        }
        //        if (System.IO.Directory.Exists(CRF.BLL.CRFView.CRFView.GetKeyValue("JOBNOTICEIMAGEPATH")) == false)
        //        {
        //            mydest = CRF.BLL.PDFLIB.TEMPPDFDIR + myfilename + ".pdf";
        //        }

        //        var mysproc = new uspbo_PrelimNotices_GetNoticeInfo();
        //        mysproc.JobNoticeHistoryId = JobNoticeHistoryId;

        //        DataSet myds = CRF.BLL.Providers.DBO.Provider.GetDataSet(mysproc);
        //        DataTable dtmynoticeinfo = myds.Tables[0];
        //        DataTable dtmyview3 = myds.Tables[2];
        //        TableView mynoticeinfo = new TableView(myds.Tables[0]);
        //        TableView myview3 = new TableView(myds.Tables[2]);
        //        // TableView mynoticeinfo = DBO.Provider.GetTableView(myds.Tables[0]);
        //        string mydate = DateTime.Today.ToString("yyyyMMdd");
                
        //        TableView myforms = new TableView(myds.Tables[1]);
        //        DataTable dtmyforms = myds.Tables[1];

        //        ArrayList myfiles = new ArrayList();

        //        myreport.ReportDefPath = user.SettingsFolder + "\\Reports\\PrelimNotice.xml";
        //        myreport.UserName = user.UserName;
        //        myreport.OutputFolder = user.OutputFolder + "\\";
        //        myreport.PrintMode = CRF.BLL.COMMON.PrintMode.PrintToPDF;

        //        mynotice.ReportDefPath = myreport.ReportDefPath;
        //        mynotice.OutputFolder = myreport.OutputFolder;
        //        mynotice.UserName = myreport.UserName;
        //        mynotice.PrintMode = myreport.PrintMode;

        //        myforms.MoveFirst();
                
        //        myforms.Sort = "SEQNO ASC";
        //        // while(dtmyforms != null)
        //        foreach (DataRow row in dtmyforms.Rows)
        //        {

        //            myforms.FillEntity(ref tblI);
        //            myreport.ReportName = myform.ReportName;
        //            myreport.DataView = myview;
        //            myreport.Sort = "";
        //            myfilename = "";

        //            myfilename = mynoticeinfo.get_RowItem("JobId").ToString();
        //            myfilename += "-" + mynoticeinfo.get_RowItem("JobNoticeHistoryId");
        //            myfilename += "-" + myforms.get_RowItem("SeqNo");
        //            myreport.FilePrefix = myfilename;

        //            switch (myform.SeqNo)
        //            {
        //                case 1:
        //                    myreport.DataView = mynoticeinfo;
        //                    myreport.RenderReport(false);
        //                    myfiles.Add(myreport.ReportFileName);
        //                    break;

        //                case 2:
        //                    //TableView MYVIEW = DBO.Provider.GetTableView(mysproc);
        //                    mynotice.ExecuteC1Report(mynoticeinfo, myfilename);
        //                    myfiles.Add(mynotice.ReportFileName);
        //                    break;
                           
        //                case 4:
        //                    myreport.DataView = myview3;
        //                    myreport.RenderReport(false);
        //                    myfiles.Add(myreport.ReportFileName);
        //                    break;

        //                case 5:
        //                    // skip statement logic is done below
        //                    // ---- statement logic
                 
        //                    try
        //                    {
             
        //                        if (myds.Tables[4].Rows.Count == 1)
        //                        {
        //                            var mysp = new uspbo_PrelimNotices_GetTexasStatement();
        //                            mysp.JobNoticeHistoryId = JobNoticeHistoryId.ToString();
        //                            DataSet myds1 = CRF.BLL.Providers.DBO.Provider.GetDataSet(mysp);
        //                            user.UserName = user.UserName;
        //                            user.OutputFolder = user.OutputFolder;
        //                            user.SettingsFolder = user.SettingsFolder;
        //                            string myfile = CRF.BLL.COMMON.Reporting.PrintDataReport(user, "TexasStatement.DRL", "TexasStatement", myds1.Tables[0], CRF.BLL.COMMON.PrintMode.PrintToPDF, myfilename, "", "");
        //                            myfiles.Add(myfile);
        //                        }
                                
        //                    }
        //                    catch (Exception ex)
        //                    {
        //                        string err = ex.Message;
        //                        ErrorLog.SendErrorToText(ex);
        //                        return "";
        //                    }
                          
        //                    break;
        //                default:
        //                    myreport.DataView = mynoticeinfo;
        //                    myreport.RenderReport(false);
        //                    myfiles.Add(myreport.ReportFileName);
        //                    break;
        //            }

        //            myforms.MoveNext();
        //        }

        //        string s =  PDFLIB.TEMPPDFDIR + myfilename + ".pdf";

        //        CRF.BLL.PDFLIB.CombinePDFS(myfiles, mydest);
        //        return mydest;
        //        }
        //    catch (Exception ex)
        //    {
        //        return "";
        //    }
        //}

        public static bool SaveMailReturns(MailReturnVM objMailReturnVM)
        {
            try
            {

                AutoMapper.Mapper.CreateMap<ADJobNoticeHistory, JobNoticeHistory>();
                ITable tblJobNoticeHistory;
                JobNoticeHistory OBJJobNoticeHistory;
                ADJobNoticeHistory objADJobNoticeHistory = ADJobNoticeHistory.ReadJobNoticeHistory(Convert.ToInt32(objMailReturnVM.JobNoticeHistoryId));

                if (objMailReturnVM.RbtOption == "Returned")
                {
                    objADJobNoticeHistory.MailStatus = "Returned";
                }
                else if(objMailReturnVM.RbtOption == "Refused")
                {
                    objADJobNoticeHistory.MailStatus = "Refused";
                }
                else 
                {
                    objADJobNoticeHistory.MailStatus = "Forwarded";
                }

                objADJobNoticeHistory.MailStatusDate = DateTime.Now;
                OBJJobNoticeHistory = AutoMapper.Mapper.Map<JobNoticeHistory>(objADJobNoticeHistory);
                tblJobNoticeHistory = (ITable)OBJJobNoticeHistory;
                DBO.Provider.Update(ref tblJobNoticeHistory);

                String sql ="";
                int myActionId = 0;
                ADMailReturns OBJADMailReturns = new ADMailReturns();

                if (objMailReturnVM.RbtOption == "Returned")
                {
                    OBJADMailReturns = ADMailReturns.ReadMailReturns(Convert.ToInt32(objMailReturnVM.ResearchId));
                    myActionId = OBJADMailReturns.ActionId;
                }
                else if (objMailReturnVM.RbtOption == "Refused")
                {
                    switch (objMailReturnVM.LegalPartyType)
                    {

                        case "Customer":
                            sql = "Select * from MailDelivered Where ResultId = 3 and LegalPartytype = 'CU'";
                            break;
                        case "General Contractor":case "Other General Contractor":
                            sql = "Select * from MailDelivered Where ResultId = 3 and LegalPartytype = 'GC'";
                            break;
                        case "Owner":case "Other Owner":case "ML AGENT":
                            sql = "Select * from MailDelivered Where ResultId = 3 and LegalPartytype = 'OW'";
                            break;
                        case "Lender/Surety":case "Other Lender/Surety":
                            sql = "Select * from MailDelivered Where ResultId = 3 and LegalPartytype = 'LE'";
                            break;
                        case "Designee":case "Other Designee":
                            sql = "Select * from MailDelivered Where ResultId = 3 and LegalPartytype = 'DE'";
                            break;

                    }
                }

                else
                {
                    switch (objMailReturnVM.LegalPartyType)
                    {

                        case "Customer":
                            sql = "Select * from MailDelivered Where ResultId = 2 and LegalPartytype = 'CU'";
                            break;
                        case "General Contractor":
                        case "Other General Contractor":
                            sql = "Select * from MailDelivered Where ResultId = 2 and LegalPartytype = 'GC'";
                            break;
                        case "Owner":
                        case "Other Owner":
                        case "ML AGENT":
                            sql = "Select * from MailDelivered Where ResultId = 2 and LegalPartytype = 'OW'";
                            break;
                        case "Lender/Surety":
                        case "Other Lender/Surety":
                            sql = "Select * from MailDelivered Where ResultId = 2 and LegalPartytype = 'LE'";
                            break;
                        case "Designee":
                        case "Other Designee":
                            sql = "Select * from MailDelivered Where ResultId = 2 and LegalPartytype = 'DE'";
                            break;

                    }
                }

                if (objMailReturnVM.RbtOption == "Refused" || objMailReturnVM.RbtOption == "Forwarded")
                {
                    var CBRView = DBO.Provider.GetTableView(sql);
                    DataTable dtCBR = CBRView.ToTable();
                    myActionId = Convert.ToInt32(dtCBR.Rows[0]["ActionId"]);
                }
             
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                var sproc = new uspbo_Jobs_CreateActionHistory();
                sproc.JobId = objMailReturnVM.JobId;
                sproc.ActionId = myActionId;
                sproc.UserId = user.Id;
                sproc.UserName = user.UserName;
                sproc.LegalPartyType = null;
                DBO.Provider.ExecNonQuery(sproc);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static string ViewSignitureCopy(int JobNoticeHistoryId, string ClientCode)
        {
            try
            {

                AutoMapper.Mapper.CreateMap<ADJobNoticeHistory, JobNoticeHistory>();
                ITable tblJobNoticeHistory;
                JobNoticeHistory OBJJobNoticeHistory;
                ADJobNoticeHistory objADJobNoticeHistory = ADJobNoticeHistory.ReadJobNoticeHistory(Convert.ToInt32(JobNoticeHistoryId));

                string myfilename = "";
                myfilename = ClientCode + "_" + objADJobNoticeHistory.CertNum.Replace(" ", "") + ".PDF";
               
                string mydest = CRF.BLL.CRFView.CRFView.GetKeyValue("SIGNITURECOPYPATH") + myfilename;
                
                if(System.IO.File.Exists(mydest))
                {
                    return mydest;
                }
                else
                {
                    return "";
                }

                
            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static string GetPODCopy(int JobNoticeHistoryId, string LegalPartyType, int JobId)
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                var sproc = new uspbo_Jobs_GetJobNoticeHistoryInfo();
                sproc.JobNoticeHistoryId = JobNoticeHistoryId;

                var myview = DBO.Provider.GetTableView(sproc);
                CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;

                string myfileprefix = "";
                 myfileprefix = JobId.ToString();
                myfileprefix += "-" + LegalPartyType;
                string myreportname = "PODRequest";
                string sReportName = PrintLienReports(user, mymode, myview, myreportname, myfileprefix);

                if (System.IO.File.Exists(sReportName))
                {
                    return sReportName;
                }
                
                var MYSPROC = new uspbo_Jobs_CreateActionHistory();
                MYSPROC.JobId = JobId;
                MYSPROC.ActionId = 12;
                MYSPROC.UserId = user.Id;
                MYSPROC.UserName = user.UserName;
                MYSPROC.LegalPartyType = LegalPartyType;
                DBO.Provider.ExecNonQuery(sproc);

                return "";

            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static string GetRecendCopy(int JobNoticeHistoryId, string LegalPartyType, int JobId)
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                var sproc = new uspbo_Jobs_GetJobNoticeHistoryInfo();
                sproc.JobNoticeHistoryId = JobNoticeHistoryId;

                var myview = DBO.Provider.GetTableView(sproc);
                CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;

                string myfileprefix = "";
                myfileprefix = JobId.ToString();
                myfileprefix += "-" + LegalPartyType;
                string myreportname = "RecensionLetter";
                string sReportName = PrintLienReports(user, mymode, myview, myreportname, myfileprefix);

                if (System.IO.File.Exists(sReportName))
                {
                    return sReportName;
                }

                var MYSPROC = new uspbo_Jobs_CreateActionHistory();
                MYSPROC.JobId = JobId;
                MYSPROC.ActionId = 178;
                MYSPROC.UserId = user.Id;
                MYSPROC.UserName = user.UserName;
                MYSPROC.LegalPartyType = LegalPartyType;
                DBO.Provider.ExecNonQuery(sproc);

                return "";

            }
            catch (Exception ex)
            {
                return "";
            }
        }

        public static string PrintNotes(int JobId)
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                //PrinterSettings settings = new PrinterSettings();
                //string MYNAME = settings.PrinterName;
                string pdfPath = null;
                var sql = "SELECT  vw.JobId, pu.UserName, vw.DateCreated, vw.Note FROM VWJobHistory VW With (Nolock) JOIN Portal_Users pu With (Nolock) on vw.EnteredByUserId = pu.Id WHERE JobId = " + JobId;
                var EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();

                var myview = DBO.Provider.GetTableView(sql);
                CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;

                string myfileprefix = "";
                myfileprefix = JobId.ToString();

                string myreportname = "JobNotes";

                pdfPath = PrintLienReports(user, mymode, myview, myreportname, myfileprefix);



                return pdfPath;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return "";
            }


        }
        public static string PrintLienReports(CRF.BLL.Users.CurrentUser auser, CRF.BLL.COMMON.PrintMode aprintmode, TableView aview , string areportname , string afileprefix)
        { 
             CRF.BLL.COMMON.StandardReport myreport = new CRF.BLL.COMMON.StandardReport();
           myreport.ReportDefPath = auser.SettingsFolder + "\\Reports\\LienReports.xml";
            myreport.UserName = auser.UserName;
            myreport.OutputFolder = auser.OutputFolder + "\\";
            myreport.ReportName = areportname;
            myreport.TableIndex = 1;
            myreport.DataView = aview;
            myreport.PrintMode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
            myreport.Sort = "";
            myreport.FilePrefix = afileprefix;

            myreport.RenderReport();
            return myreport.ReportFileName;
            //return myreport.ReportFileName;
        }
        #endregion
    }
}
