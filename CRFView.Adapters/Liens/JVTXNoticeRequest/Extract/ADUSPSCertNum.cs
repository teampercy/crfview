﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADUSPSCertNum
    {
        #region Properties
        public int Id { get; set; }
        public string ApplicationIdentifier { get; set; }
        public string ServiceTypeCode { get; set; }
        public string SourceIdentifier { get; set; }
        public string MailerId { get; set; }
        public string SeqNo { get; set; }
        public string MaxSeqNo { get; set; }

        #endregion

        #region CRUD
        public static ADUSPSCertNum ReadUSPSCertNum(int Id)
        {
            USPSCertNum myentity = new USPSCertNum();
            ITable myentityItable = myentity;
            DBO.Provider.Read(Id.ToString(), ref myentityItable);
            myentity = (USPSCertNum)myentityItable;
            AutoMapper.Mapper.CreateMap<USPSCertNum, ADUSPSCertNum>();
            return (AutoMapper.Mapper.Map<ADUSPSCertNum>(myentity));
        }

        public static int SaveUSPSCertNum(ADUSPSCertNum objUpdatedADUSPSCertNum)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADUSPSCertNum, USPSCertNum>();
                ITable tblUSPSCertNum;
                USPSCertNum objUSPSCertNum;
                 
                if (objUpdatedADUSPSCertNum.Id != 0)
                {
                    ADUSPSCertNum objADUSPSCertNumOriginal = ReadUSPSCertNum(Convert.ToInt32(objUpdatedADUSPSCertNum.Id));
                    
                    if (string.IsNullOrEmpty(objUpdatedADUSPSCertNum.SeqNo))
                    {
                    }
                    else
                    {
                        objADUSPSCertNumOriginal.SeqNo = objUpdatedADUSPSCertNum.SeqNo;
                    }


                    objUSPSCertNum = AutoMapper.Mapper.Map<USPSCertNum>(objADUSPSCertNumOriginal);
                    tblUSPSCertNum = (ITable)objUSPSCertNum;
                    DBO.Provider.Update(ref tblUSPSCertNum);
                }
                else
                {
                    objUSPSCertNum = AutoMapper.Mapper.Map<USPSCertNum>(objUpdatedADUSPSCertNum);
                    tblUSPSCertNum = (ITable)objUSPSCertNum;
                    DBO.Provider.Create(ref tblUSPSCertNum);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        #endregion
    }
}
