﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.JVTXNoticeRequest.PrintLetters.ViewModels
{
    public class JVTXPrintLettersVM
    {

        public JVTXPrintLettersVM()
        { }

        public string RbtPrintAction { get; set; }
        public int JobId { get; set; }
        public bool chkNoMailLog { get; set; }
        public bool chkSendToPrinter { get; set; }
        public bool chkPrintToPdf { get; set; }
        public int MajorGroup { get; set; }
        public int PrintGroup { get; set; }
    }
}
