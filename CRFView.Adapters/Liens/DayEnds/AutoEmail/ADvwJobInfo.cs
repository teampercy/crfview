﻿using CRFView.Adapters.Liens.DayEnds.AutoEmail.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.DayEnds
{
    public class ADvwJobInfo
    {
        #region Properties

        public string FromDate { get; set; }
        public string ThruDate { get; set; }

        #endregion

       /// #region CRUD
        //public static string PrintAutoEmail(AutoEmailVM objAutoEmailVM)
        //{
        //    //string myprt = new Windows.Forms.PrintDialog;
        //    // var myprt = CRF.BLL.COMMON.
        //    //String MYNAME = myprt.PrinterSettings.PrinterName
        //    string pdfPath = null;
        //    //string sFromDate = null;
        //    //string sThruDate = null;

        //    try
        //    {

        //        CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

        //        var Sp_OBJ = new uspbo_LiensDayEnd_GetAutoEmails();

        //        if (objAutoFaxVM.chkAllClient == false)
        //        {
        //            Sp_OBJ.ClientId = Convert.ToInt32(objAutoFaxVM.ClientId);
        //        }
        //        else
        //        {
        //            Sp_OBJ.ClientId = 0;
        //        }


        //        Sp_OBJ.FromDate = objAutoFaxVM.objADvwJobInfo.FromDate.ToShortDateString();
        //        Sp_OBJ.ThruDate = objAutoFaxVM.objADvwJobInfo.ThruDate.AddDays(1).ToShortDateString();
        //        //sFromDate = objClientAcknowledgmentsVM.InvoiceFromDate.ToShortDateString();
        //        //sThruDate = objClientAcknowledgmentsVM.InvoiceThruDate.AddDays(1).ToShortDateString();


        //        //TableView myview = DBO.Provider.GetTableView(Sp_OBJ);
        //        //TableView myview = CRF.BLL.Providers.DBO.Provider.GetTableView(Sp_OBJ);

        //        DataSet ds = CRF.BLL.Providers.DBO.Provider.GetDataSet(Sp_OBJ);

        //        CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;
        //        mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;

        //        if (objAutoFaxVM.RbtPrintOption == "Send To PDF")
        //        {
        //            mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
        //        }
        //        if (objAutoFaxVM.RbtPrintOption == "Fax")
        //        {

        //            CRF.BLL.CRFView.CRFView.SetDefaultPrinter(CRF.BLL.CRFView.CRFView.GetKeyValue("FAXPRINTER"));

        //        }



        //        //  DataTable mytbl = ds.Tables[0];



        //        //string myreport = "PrelimAck";
        //        //TableView vw = new TableView(ds.Tables[0]);
        //        ////DBO.Provider.GetTableView(Convert.ToString(ds.Tables[0]));
        //        //pdfPath = CRF.BLL.Globals.RenderC1Report(user, ds.Tables[0], "LIENREPORTS.xml", myreport, mymode, "PRELIMACK", "", "");
        //        ////pdfPath = CRF.BLL.COMMON.Reporting.PrintC1Report(user, "LIENREPORTS.xml", myreport, vw, mymode, "PRELIMACK", "", "");

        //        ////   TableView vw  =  new DAL.COMMON.TableView(ds.Tables[0]);


        //        //myreport = "PrelimAckBranch";
        //        //TableView vw1 = new TableView(ds.Tables[1]);
        //        ////TableView vw1 = DBO.Provider.GetTableView(Convert.ToString(ds.Tables[1]));
        //        //CRF.BLL.Globals.RenderC1Report(user, ds.Tables[1], "LIENREPORTS.xml", myreport, mymode, "PRELIMACK", "", "");
        //        ////pdfPath = CRF.BLL.COMMON.Reporting.PrintC1Report(user, "LIENREPORTS.xml", myreport, vw1, mymode, "PRELIMACK", "", "");



        //        TableView vw = new TableView(ds.Tables[0]);
        //        string myreport = "FaxCustAuto";
        //        CRF.BLL.Globals.RenderC1Report(user, ds.Tables[0], "LIENREPORTS.xml", myreport, mymode, "CUSTAUTOFAX", "", "");
        //        //pdfPath = CRF.BLL.COMMON.Reporting.PrintC1Report(user, "LIENREPORTS.xml", myreport,vw, mymode, "CUSTAUTOFAX", "", "");

        //        TableView vw2 = new TableView(ds.Tables[1]);
        //        myreport = "FaxGenAuto";
        //        CRF.BLL.Globals.RenderC1Report(user, ds.Tables[1], "LIENREPORTS.xml", myreport, mymode, "GCAUTOFAX", "", "");
        //        // pdfPath = CRF.BLL.COMMON.Reporting.PrintC1Report(user, "LIENREPORTS.xml", myreport, vw2, mymode, "GCAUTOFAX", "", "");

        //        //' customer auto faxes
        //        TableView vw3 = new TableView(ds.Tables[2]);
        //        myreport = "FaxCustAutoTX";
        //        CRF.BLL.Globals.RenderC1Report(user, ds.Tables[2], "LIENREPORTS.xml", myreport, mymode, "GCAUTOFAX", "", "");
        //        //  pdfPath = CRF.BLL.COMMON.Reporting.PrintC1Report(user, "LIENREPORTS.xml", myreport, vw3, mymode, "CUSTAUTOFAXTX", "", "");

        //        TableView vw4 = new TableView(ds.Tables[3]);
        //        myreport = "FaxGenAutoTX";
        //        CRF.BLL.Globals.RenderC1Report(user, ds.Tables[3], "LIENREPORTS.xml", myreport, mymode, "GCAUTOFAX", "", "");
        //        // pdfPath = CRF.BLL.COMMON.Reporting.PrintC1Report(user, "LIENREPORTS.xml", myreport, vw4, mymode, "GCAUTOFAXTX", "", "");

        //    }

        //    catch (Exception ex)
        //    {
        //        return "";
        //    }
        //    return pdfPath;
        //}
        //#endregion
    }
}
