﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using CRFView.Adapters.Liens.DayEnds.PublicException.ViewModels;
using CRFView.Adapters.Liens.Managements.ChangeDateRequested.ViewModels;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADPublicException
    {

        #region Properties


        public bool PublicJob { get; set; }
        public string JobId { get; set; }
        public string JobName { get; set; }
        public string OwnerName { get; set; }
        public bool NCInfo { get; set; }
        public bool FederalJob { get; set; }
        public string ClientCode { get; set; }
        public bool SendAsIsBox { get; set; }
        public string PlacementSource { get; set; }
        public string ClientName { get; set; }
        public string JobNum { get; set; }
        public string JobAdd1 { get; set; }
        public string jobCity { get; set; }
        public string DeskNum { get; set; }
        public string StatusCode { get; set; }
        public DateTime NoticeSent { get; set; }
        public DateTime DateAssigned { get; set; }
        public DateTime RevDate { get; set; }
        public string JobState { get; set; }
        public decimal EstBalance { get; set; }
        public DateTime StartDate { get; set; }
        public string BranchNumber { get; set; }
        public String CustName { get; set; }
        public String LenderName { get; set; }
        public String GCName { get; set; }
        public System.DateTime VerifiedDate { get; set; }
        public String JobCity { get; set; }

        public static TableView myview;
        public  static DataTable dt = new DataTable();
        public static ITable myentityItable;

        #endregion


        #region CRUD


        public static ADJob ReadJob(int JobID)
        {
            Job myentity = new Job();
            ITable myentityItable = myentity;
            DBO.Provider.Read(JobID.ToString(), ref myentityItable);
            myentity = (Job)myentityItable;
            AutoMapper.Mapper.CreateMap<Job, ADJob>();
            return (AutoMapper.Mapper.Map<ADJob>(myentity));
        }
        public static DataTable GetNonPublicExceotionDt(PublicExceptionVM objPublicExceptionVM)
        {
            try
            {
                var sp_obj = new uspbo_LiensDayEnd_PublicJobException();

                if(objPublicExceptionVM.Rbtbtn == "optNoticeDate")
                {
                    sp_obj.ActionType = 1;
                    sp_obj.NoticeFROM = objPublicExceptionVM.DateTimePicker1;
                    sp_obj.NoticeTO = objPublicExceptionVM.DateTimePicker2;
                }
                else if(objPublicExceptionVM.Rbtbtn == "optTXRequest")
                {
                    sp_obj.ActionType = 2;
                }
                else
                {
                    sp_obj.ActionType = 5;                //VerifiedSent (or TN)
                }



               


                //DBO.GetTableView 
                var myview = DBO.Provider.GetTableView(sp_obj);
                dt = myview.ToTable();
                return (dt);


            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        public static DataTable GetPublicExceotionDt(PublicExceptionVM objPublicExceptionVM)
        {
            try
            {
                var sp_obj = new uspbo_LiensDayEnd_PublicJobException();

                if (objPublicExceptionVM.Rbtbtn == "optNoticeDate")
                {
                    sp_obj.ActionType = 3;
                    sp_obj.NoticeFROM = objPublicExceptionVM.DateTimePicker1;
                    sp_obj.NoticeTO = objPublicExceptionVM.DateTimePicker2;
                }
                else if (objPublicExceptionVM.Rbtbtn == "optTXRequest")
                {
                    sp_obj.ActionType = 4;
                }
                else
                {
                    sp_obj.ActionType = 6;                //VerifiedSent (or TN)
                }





                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sp_obj);
                DataTable dt = EntityList.ToTable();
                return (dt);


            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }


        public static List<ADPublicException> GetNonPublicExceotionList(DataTable dt)
        {
            List<ADPublicException> NonPublicList = (from DataRow dr in dt.Rows
                                                select new ADPublicException()

                                                {

                                                    PublicJob = ((dr["PublicJob"] != DBNull.Value ? Convert.ToBoolean(dr["PublicJob"]) : false)),
                                                    JobId = ((dr["JobId"] != DBNull.Value ? dr["JobId"].ToString() : "")),

                                                    JobName = ((dr["JobName"] != DBNull.Value ? dr["JobName"].ToString() : "")),
                                                    OwnerName = ((dr["OwnerName"] != DBNull.Value ? dr["OwnerName"].ToString() : "")),
                                                    NCInfo = ((dr["NCInfo"] != DBNull.Value ? Convert.ToBoolean(dr["NCInfo"]) : false)),
                                                    FederalJob = ((dr["FederalJob"] != DBNull.Value ? Convert.ToBoolean(dr["FederalJob"]) : false)),
                                                    ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                                    SendAsIsBox = ((dr["SendAsIsBox"] != DBNull.Value ? Convert.ToBoolean(dr["SendAsIsBox"]) : false)),
                                                    PlacementSource = ((dr["PlacementSource"] != DBNull.Value ? dr["PlacementSource"].ToString() : "")),
                                                    ClientName = ((dr["ClientName"] != DBNull.Value ? dr["ClientName"].ToString() : "")),
                                                    JobNum = ((dr["JobNum"] != DBNull.Value ? dr["JobNum"].ToString() : "")),
                                                    JobAdd1 = ((dr["JobAdd1"] != DBNull.Value ? dr["JobAdd1"].ToString() : "")),
                                                    jobCity = ((dr["jobCity"] != DBNull.Value ? dr["jobCity"].ToString() : "")),
                                                    DeskNum = ((dr["DeskNum"] != DBNull.Value ? dr["DeskNum"].ToString() : "")),
                                                    StatusCode = ((dr["StatusCode"] != DBNull.Value ? dr["StatusCode"].ToString() : "")),
                                                    NoticeSent = ((dr["NoticeSent"] != DBNull.Value ? Convert.ToDateTime(dr["NoticeSent"]) : DateTime.MinValue)),
                                                    DateAssigned = ((dr["DateAssigned"] != DBNull.Value ? Convert.ToDateTime(dr["DateAssigned"]) : DateTime.MinValue)),
                                                    RevDate = ((dr["RevDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevDate"]) : DateTime.MinValue)),
                                                    JobState = ((dr["JobState"] != DBNull.Value ? dr["JobState"].ToString() : "")),
                                                    EstBalance = ((dr["EstBalance"] != DBNull.Value ? Convert.ToDecimal(dr["EstBalance"]) : 0)),
                                                    StartDate = ((dr["StartDate"] != DBNull.Value ? Convert.ToDateTime(dr["StartDate"]) : DateTime.MinValue)),
                                                    BranchNumber = ((dr["BranchNumber"] != DBNull.Value ? dr["BranchNumber"].ToString() : "")),

                                                }).ToList();
            return NonPublicList;
        }
        public static DataTable LoadInfo(string JobId)
        {
            try
            {
               

             string sql = "Select * from vwJobInfo where JobId = " + JobId;
               
                var EntityList = DBO.Provider.GetTableView(sql);
                 DataTable dt = EntityList.ToTable();

                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }
        public static int UpdatePublicException_Action(ChangeDateRequestedVM objChangeDateRequestedVM)
        {

            try
            {
                string sOldType = "";
               
                ADJob objADJobOriginal = ReadJob(Convert.ToInt32(objChangeDateRequestedVM.JobId));

              

                if (objADJobOriginal.PrivateJob == true)
                {
                    sOldType = "Private Job";
                }
                if (objADJobOriginal.ResidentialBox == true)
                {
                    sOldType = "Residential Job";
                }

                LogChanges(objChangeDateRequestedVM.JobId, sOldType);
                objADJobOriginal.PublicJob = true;
                if (objADJobOriginal.SendAsIsBox == true && objADJobOriginal.PlacementSource == "WEB")
                {
                    objADJobOriginal.NCInfo = true;
                }


                AutoMapper.Mapper.CreateMap<ADJob, Job>();
                Job myentity;
                myentity = AutoMapper.Mapper.Map<Job>(objADJobOriginal);

                ITable tblI = myentity;
                DBO.Provider.Update(ref tblI);
              
                DBO.Provider.Read(objChangeDateRequestedVM.JobId, ref tblI);
                
                DataRow dr = dt.Rows[0];
                dr[0] = objADJobOriginal.PublicJob;
                dr[4] = objADJobOriginal.NCInfo;
            
                return 1;
            }

            catch (Exception ex)
            {
                return 0;
            }

        }
        public static int LogChanges(string aJobId, string sOldType)
        {
            var mysproc = new uspbo_Jobs_UpdateChangeLog();
            mysproc.JobId = Convert.ToInt32(aJobId);
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            mysproc.UserId = user.Id;
            mysproc.UserName = user.UserName.Trim();
            mysproc.ChangeType = 0;
            mysproc.ChangeFrom = sOldType.Trim() + " ";
            mysproc.ChangeTo = "Public Job";
            DBO.Provider.ExecNonQuery(mysproc);
            return 1;
        }

        #endregion


    }
}
