﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.NoticeCycle.Extract.ViewModels
{
    public class NoticeCycleExtractVM
    {

        #region Properties
        public DateTime ThruDate { get; set; }
        //public int JobId { get; set; }
        public bool chkAssignCerts { get; set; }
        public string LastCertNumber { get; set; }

        public ADUSPSCertNum objUSPSCertNum { get; set; }
        #endregion

    }
}
