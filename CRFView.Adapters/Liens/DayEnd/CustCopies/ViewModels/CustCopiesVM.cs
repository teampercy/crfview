﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Liens.DayEnd.CustCopies.ViewModels
{
    public class CustCopiesVM
    {


        public CustCopiesVM()
        {
            GetClientList = CommonBindList.GetClientIdList();
        }

        public IEnumerable<SelectListItem> GetClientList { get; set; }


        public string ClientId { get; set; }

       // public ADvwJobInventory objADvwJobInventory { get; set; }
        public bool chkAllClient { get; set; }
        public string RbtPrintOption { get; set; }
        public string InvoiceFromDate { get; set; }
        public string InvoiceThruDate { get; set; }

    }
}
