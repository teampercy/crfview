﻿using CRF.BLL;
using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using CRFView.Adapters.Liens.DayEnd.ClientAcknowledgments.ViewModels;
using CRFView.Adapters.Liens.DayEnd.CustCopies.ViewModels;
using CRFView.Adapters.Liens.DayEnd.NoticeCopies.ViewModels;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADvwJobInventory
    {

        #region Properties

        public int jobid { get; set; }
        public string jobname { get; set; }
        public string jobnum { get; set; }
        public string branchnum { get; set; }
        public string jobadd1 { get; set; }
        public string jobstate { get; set; }
        public string jobzip { get; set; }
        public string jobcity { get; set; }
        public decimal estbalance { get; set; }
        public string clientcode { get; set; }
        public string clientcustomer { get; set; }
        public string gcname { get; set; }
        public string ownername { get; set; }
        public string lendername { get; set; }
        public string ponum { get; set; }
        public string statuscode { get; set; }
        public DateTime dateassigned { get; set; }
        public DateTime lienassigndate { get; set; }
        public DateTime revdate { get; set; }
        public DateTime noticesent { get; set; }
        public DateTime liendate { get; set; }
        public DateTime noidate { get; set; }
        public DateTime verifieddate { get; set; }
        public DateTime lastnoticeprintdat { get; set; }
        public string clientname { get; set; }
        public int clientid { get; set; }
        public string apnnum { get; set; }
        public string jobcounty { get; set; }
        public string custrefnum { get; set; }
        public DateTime amendeddate { get; set; }
        public bool prelimbox { get; set; }
        public bool rushorder { get; set; }
        public bool verifyjob { get; set; }
        public bool podbox { get; set; }
        public bool federaljob { get; set; }
        public bool residentialbox { get; set; }
        public bool greencard { get; set; }
        public string problemmsg { get; set; }
        public string custname { get; set; }
        public DateTime submittedon { get; set; }
        public string username { get; set; }
        public bool ncinfo { get; set; }
        public string generalcontractor { get; set; }
        public bool titleverifiedbox { get; set; }
        public bool sendasisbox { get; set; }
        public string jobstatusdescr { get; set; }
        public string ntodays { get; set; }
        public bool nocompbox { get; set; }
        public DateTime startdate { get; set; }
        public bool publicjob { get; set; }
        public bool privatejob { get; set; }
        public bool rushorderverify { get; set; }
        public string jobadd2 { get; set; }
        public string placementsource { get; set; }
        public bool verifyowonly { get; set; }
        public bool verifyowonlysend { get; set; }
        public bool verifyowonlytx { get; set; }


        #endregion

        #region CRUD

        public static string PrintClientAcknowledgments(ClientAcknowledgmentsVM objClientAcknowledgmentsVM)
        {
            //string myprt = new Windows.Forms.PrintDialog;
            // var myprt = CRF.BLL.COMMON.
            //String MYNAME = myprt.PrinterSettings.PrinterName
            string pdfPath = null;
            //string sFromDate = null;
            //string sThruDate = null;

            try
            {

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var Sp_OBJ = new uspbo_LiensDayEnd_GetPrelimAcks();
                Sp_OBJ.FromDate = objClientAcknowledgmentsVM.InvoiceFromDate.ToShortDateString();
                Sp_OBJ.ThruDate = objClientAcknowledgmentsVM.InvoiceThruDate.ToShortDateString();
                //sFromDate = objClientAcknowledgmentsVM.InvoiceFromDate.ToShortDateString();
                //sThruDate = objClientAcknowledgmentsVM.InvoiceThruDate.AddDays(1).ToShortDateString();


                //TableView myview = DBO.Provider.GetTableView(Sp_OBJ);
                //TableView myview = CRF.BLL.Providers.DBO.Provider.GetTableView(Sp_OBJ);

                DataSet ds = CRF.BLL.Providers.DBO.Provider.GetDataSet(Sp_OBJ);

                CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;
                mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;

                if (objClientAcknowledgmentsVM.RbtPrintOption == "Send To PDF")
                {
                    mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                }
                if (objClientAcknowledgmentsVM.RbtPrintOption == "Fax")
                {

                    CRF.BLL.CRFView.CRFView.SetDefaultPrinter(CRF.BLL.CRFView.CRFView.GetKeyValue("FAXPRINTER"));

                }



                //  DataTable mytbl = ds.Tables[0];



                string myreport = "PrelimAck";
                TableView vw = new TableView(ds.Tables[0]);
                //DBO.Provider.GetTableView(Convert.ToString(ds.Tables[0]));
                pdfPath = CRF.BLL.Globals.RenderC1Report(user, ds.Tables[0], "LIENREPORTS.xml", myreport, mymode, "PRELIMACK", "", "");
                //pdfPath = CRF.BLL.COMMON.Reporting.PrintC1Report(user, "LIENREPORTS.xml", myreport, vw, mymode, "PRELIMACK", "", "");

                //   TableView vw  =  new DAL.COMMON.TableView(ds.Tables[0]);


                myreport = "PrelimAckBranch";
                TableView vw1 = new TableView(ds.Tables[1]);
                //TableView vw1 = DBO.Provider.GetTableView(Convert.ToString(ds.Tables[1]));
                CRF.BLL.Globals.RenderC1Report(user, ds.Tables[1], "LIENREPORTS.xml", myreport, mymode, "PRELIMACK", "", "");
                //pdfPath = CRF.BLL.COMMON.Reporting.PrintC1Report(user, "LIENREPORTS.xml", myreport, vw1, mymode, "PRELIMACK", "", "");



                //string myreportfilename = Globals.CurrentUser.SettingsFolder + "LienReports.xml";
                //string myreportname = "LienRevenueClientSum";

                //pdfPath = Globals.RenderC1Report(user, mytbl, myreportfilename, myreportname, mymode, "", sFromDate, sThruDate);

            }

            catch (Exception ex)
            {
                return "";
            }
            return pdfPath;
        }



        public static string PrintCustCopies(CustCopiesVM objCustCopiesVM)
        {


            string pdfPath = null;
            //string sFromDate = null;
            //string sThruDate = null;

            try
            {

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var mymode = new CRF.BLL.COMMON.PrintMode();
                mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;
                if (objCustCopiesVM.RbtPrintOption == "Send To PDF")
                {
                    mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;

                }

                var myNOTICE = new CRF.BLL.Liens.Reports.JobPrelimNotice(user);
                myNOTICE.PrintMode = mymode;

                var myreport = new CRF.BLL.COMMON.StandardReport();
                string ReportDefPath = Globals.CurrentUser.SettingsFolder + "PrelimNotice.xml";
                string OutputFolder = Globals.CurrentUser.OutputFolder;
                string UserName = Globals.CurrentUser.UserName;
                myreport.PrintMode = mymode;



                //myreport.PrintMode = mymode

                var mysproc = new uspbo_LiensDayEnd_GetDailyActivityList();
                mysproc.FromDate = Convert.ToDateTime(objCustCopiesVM.InvoiceFromDate, System.Globalization.CultureInfo.InvariantCulture);
                mysproc.ThruDate = Convert.ToDateTime(objCustCopiesVM.InvoiceThruDate, System.Globalization.CultureInfo.InvariantCulture);
                //sFromDate = objClientAcknowledgmentsVM.InvoiceFromDate.ToShortDateString();
                //sThruDate = objClientAcknowledgmentsVM.InvoiceThruDate.AddDays(1).ToShortDateString();

                if (objCustCopiesVM.chkAllClient == false)
                {
                    mysproc.ClientId = Convert.ToInt32(objCustCopiesVM.ClientId);
                }

                DataSet ds = CRF.BLL.Providers.DBO.Provider.GetDataSet(mysproc);
                TableView vw = new TableView(ds.Tables[1]);

                vw.MoveFirst();

                while (vw.EOF() == false)
                {
                    var mysprocByClient = new uspbo_LiensDayEnd_GetDailyActivityByClient();
                    mysprocByClient.FromDate = mysproc.FromDate;
                    mysprocByClient.ThruDate = mysproc.ThruDate;
                    mysprocByClient.ClientId = Convert.ToInt32(vw.get_RowItem("ClientId"));

                    //mysprocByClient.ClientId = vw.RowItem("ClientId")
                    DataSet ds1 = CRF.BLL.Providers.DBO.Provider.GetDataSet(mysprocByClient);

                    TableView vw2 = new TableView(ds1.Tables[2]);
                    ClientLienInfo mycli = new ClientLienInfo();
                    ITable myentityItable = mycli;

                    TableView vw3 = new TableView(ds1.Tables[5]);
                    vw3.FillEntity(ref myentityItable);

                    if ((mycli.CustSendReg == true) || (mycli.TXCustSend == true) || (mycli.GCSendReg == true))
                    {
                        vw2.MoveFirst();
                        while (vw2.EOF() == false)
                        {
                            if ((mycli.CustSendReg == true && Convert.ToBoolean(vw2.get_RowItem("SameAsCust")) == false) || (mycli.TXCustSend == true && vw2.get_RowItem("JobState") == "TX" && Convert.ToBoolean(vw2.get_RowItem("SameAsCust")) == false) || (mycli.GCSendReg == true && Convert.ToBoolean(vw2.get_RowItem("GCSendReg")) == true && Convert.ToBoolean(vw2.get_RowItem("SameAsCust")) == true))
                            {
                                var SP2 = new uspbo_Jobs_GetNoticeInfoForCopy();
                                SP2.Id = Convert.ToString(vw2.get_RowItem("NoticeHistoryId"));
                                DataSet myds = CRF.BLL.Providers.DBO.Provider.GetDataSet(SP2);

                                TableView VW4 = new TableView(myds.Tables[0]);
                                TableView mycover = new TableView(myds.Tables[2]);
                                //Dim VW4 As DAL.COMMON.TableView = DBO.GetTableView(SP2)
                                VW4.MoveFirst();

                                myNOTICE.DataView = VW4;
                                myNOTICE.FilePrefix = "CUC-" + vw.get_RowItem("ClientCode") + "-" + VW4.get_RowItem("JobId") + "-2";
                                myNOTICE.ReportName = Convert.ToString(VW4.get_RowItem("FormCode"));
                                //myNOTICE.RenderReport(false);

                                pdfPath = CRF.BLL.Globals.RenderC1Report(user, myds.Tables[0], ReportDefPath, myNOTICE.ReportName, mymode, myNOTICE.FilePrefix, "", "");


                                VW4.MoveFirst();
                                myreport.DataView = VW4;

                                if (mycli.IsCustomCoverLetter == true)
                                {
                                    myreport.FilePrefix = "CUC-" + vw.get_RowItem("ClientCode") + "-" + VW4.get_RowItem("JobId") + "-1";
                                    myreport.ReportName = mycover.get_RowItem("ReportName") + "Cust";
                                }

                                else
                                {
                                    myreport.FilePrefix = "CUC-" + vw.get_RowItem("ClientCode") + "-" + VW4.get_RowItem("JobId") + "-1";
                                    myreport.ReportName = "CoverLetterCust";
                                }

                              //  myreport.RenderReport(false);

                                pdfPath = CRF.BLL.Globals.RenderC1Report(user, myds.Tables[0], ReportDefPath, myNOTICE.ReportName, mymode, myNOTICE.FilePrefix, "", "");

                            }


                            vw2.MoveNext();
                        }

                    }


                    vw.MoveNext();

                }




            }

            catch (Exception ex)
            {
                return "";
            }
            return pdfPath;
        }




        public static string PrintNoticeCopies(NoticeCopiesVM objNoticeCopiesVM)
        {


            string pdfPath = null;
            //string sFromDate = null;
            //string sThruDate = null;

            try
            {

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var mymode = new CRF.BLL.COMMON.PrintMode();
                mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;
                if (objNoticeCopiesVM.RbtPrintOption == "Send To PDF")
                {
                    mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;

                }

                var myreport = new CRF.BLL.COMMON.StandardReport();
                myreport.ReportDefPath = Globals.CurrentUser.SettingsFolder + "PrelimNotice.xml";
                myreport.OutputFolder = Globals.CurrentUser.OutputFolder;
                myreport.UserName = Globals.CurrentUser.UserName;
                myreport.PrintMode = mymode;

                var myNOTICE = new CRF.BLL.Liens.Reports.JobPrelimNotice(user);
                myNOTICE.PrintMode = mymode;





                var mysproc = new uspbo_LiensDayEnd_GetDailyActivityList();
                mysproc.FromDate = Convert.ToDateTime(objNoticeCopiesVM.InvoiceFromDate, System.Globalization.CultureInfo.InvariantCulture);
                mysproc.ThruDate = Convert.ToDateTime(objNoticeCopiesVM.InvoiceThruDate, System.Globalization.CultureInfo.InvariantCulture);
                //sFromDate = objClientAcknowledgmentsVM.InvoiceFromDate.ToShortDateString();
                //sThruDate = objClientAcknowledgmentsVM.InvoiceThruDate.AddDays(1).ToShortDateString();

                if (objNoticeCopiesVM.chkAllClient == false)
                {
                    mysproc.ClientId = Convert.ToInt32(objNoticeCopiesVM.ClientId);
                }

                DataSet ds = CRF.BLL.Providers.DBO.Provider.GetDataSet(mysproc);
                TableView vw = new TableView(ds.Tables[1]);

                vw.MoveFirst();

                    //while (vw.EOF() == false)
                    //{
                    var sp1 = new uspbo_LiensDayEnd_GetDailyActivityByClient();
                    sp1.FromDate = mysproc.FromDate;
                    sp1.ThruDate = mysproc.ThruDate;
                    sp1.ClientId = Convert.ToInt32(vw.get_RowItem("ClientId"));

                    //mysprocByClient.ClientId = vw.RowItem("ClientId")
                    DataSet ds1 = CRF.BLL.Providers.DBO.Provider.GetDataSet(sp1);

                    TableView vw2 = new TableView(ds1.Tables[1]);
                    ClientLienInfo mycli = new ClientLienInfo();
                    ITable myentityItable = mycli;

                    TableView vw3 = new TableView(ds1.Tables[4]);
                    vw3.FillEntity(ref myentityItable);

                    //if (mycli.SendNTO == true && mycli.UseBranchNTO == false)
                    //{
                        // string ReportDefPath = Globals.CurrentUser.SettingsFolder + "PrelimNotice.xml";
                        myreport.ReportDefPath = Globals.CurrentUser.SettingsFolder + "LienReports.xml";
                        myreport.DataView = vw2;
                        myreport.FilePrefix = "CLT-" + vw.get_RowItem("ClientCode");
                        myreport.ReportName = "PrelimSent";
                        //myreport.RenderReport(false);
                       pdfPath = CRF.BLL.Globals.RenderC1Report(user, ds.Tables[1], myreport.ReportDefPath, myreport.ReportName, mymode, myreport.FilePrefix, "", "");
                       // myreport.RenderReport(false);

                        vw2.MoveFirst();


                        myreport.ReportDefPath = Globals.CurrentUser.SettingsFolder + "PrelimNotice.xml";

                        while (vw2.EOF() == false)
                        {
                            var sp2 = new uspbo_Jobs_GetNoticeInfoForCopy();
                            sp2.Id = Convert.ToString(vw2.get_RowItem("NoticeHistoryId"));
                            DataSet ds2 = CRF.BLL.Providers.DBO.Provider.GetDataSet(sp2);
                            TableView VW4 = new TableView(ds2.Tables[1]);
                            VW4.MoveFirst();

                            myNOTICE.DataView = VW4;
                            string FilePrefix1 = "CLC-" + vw.get_RowItem("ClientCode")+ "-" + VW4.get_RowItem("JobId")+ "-2"   ;
                            string ReportName1 = Convert.ToString(VW4.get_RowItem("FormCode"));
                           pdfPath = CRF.BLL.Globals.RenderC1Report(user, ds2.Tables[1], myreport.ReportDefPath, ReportName1, mymode, FilePrefix1, "", "");
                            vw2.MoveNext();
                        }

                    //}

                    vw.MoveNext();

                //}

                //Branch clients
                //TableView vwBranchClients = new TableView(ds.Tables[2]);
                //vwBranchClients.MoveFirst();

                //while (vwBranchClients.EOF() == false)
                //{

                //    var spClientBranch = new uspbo_LiensDayEnd_GetDailyActivityByClientBranch();
                //    spClientBranch.FromDate = mysproc.FromDate;
                //    spClientBranch.ThruDate = mysproc.ThruDate;
                //    spClientBranch.ClientId = Convert.ToInt32(vwBranchClients.get_RowItem("ClientId"));

                //    spClientBranch.BranchNum = Convert.ToString(vwBranchClients.get_RowItem("BranchNum"));

                //    DataSet myds1 = CRF.BLL.Providers.DBO.Provider.GetDataSet(spClientBranch);
                //    TableView vwBranch = new TableView(myds1.Tables[0]);

                //    //ClientLienInfo mycli = new ClientLienInfo();
                //    //ITable myentityItable = mycli;

                //    TableView vwCLI = new TableView(myds1.Tables[1]);
                //    vwCLI.FillEntity(ref myentityItable);

                //    if (mycli.SendNTO == true && mycli.UseBranchNTO == true)
                //    {
                //        myreport.ReportDefPath = Globals.CurrentUser.SettingsFolder + "LienReports.xml";
                //        myreport.DataView = vwBranch;
                //        myreport.FilePrefix = "CLT-" + vwBranchClients.get_RowItem("ClientCode") + "_" + vwBranchClients.get_RowItem("BranchNum");
                //        myreport.ReportName = "PrelimSentBranch";
                //        //myreport.RenderReport(false);

                //        pdfPath = CRF.BLL.Globals.RenderC1Report(user, myds1.Tables[1], myreport.ReportDefPath, myreport.ReportName, mymode, myreport.FilePrefix, "", "");
                //        vwBranch.MoveFirst();


                //        myreport.ReportDefPath = Globals.CurrentUser.SettingsFolder + "LienReports.xml";

                //        while (vwBranch.EOF() == false)
                //        {
                //            var spNoticeCopy = new uspbo_Jobs_GetNoticeInfoForCopy();
                //            spNoticeCopy.Id = Convert.ToString(vwBranch.get_RowItem("NoticeHistoryId"));

                //            DataSet myds2 = CRF.BLL.Providers.DBO.Provider.GetDataSet(spNoticeCopy);
                //            TableView vwNoticeCopy = new TableView(myds2.Tables[0]);
                //            vwNoticeCopy.MoveFirst();

                //            myNOTICE.DataView = vwNoticeCopy;
                //            myNOTICE.FilePrefix = "CLC-" + vwBranchClients.get_RowItem("ClientCode") + "-" + vwBranchClients.get_RowItem("BranchNum") + "-" + vwNoticeCopy.get_RowItem("JobId") + "-2";
                //            myNOTICE.ReportName = Convert.ToString(vwNoticeCopy.get_RowItem("FormCode"));
                //            //myNOTICE.RenderReport(false);
                //            pdfPath = CRF.BLL.Globals.RenderC1Report(user, myds2.Tables[0], myreport.ReportDefPath, myreport.ReportName, mymode, myreport.FilePrefix, "", "");
                //            vwBranch.MoveNext();





                //        }

                //    }

                //    vwBranchClients.MoveNext();


                //}


            }

            catch (Exception ex)
            {
                return "";
            }
            return pdfPath;
        }












        #endregion

    }
}
