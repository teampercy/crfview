﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CRFView.Adapters.Liens.DayEnds.PublicException.ViewModels
{
    public class PublicExceptionVM
    {
        public PublicExceptionVM()
        {

            //ObjChangeDateRequested = new ADvwJobPrelimRequests();

            NonPublicExceptionList = new List<ADPublicException>();
        }


        public string Rbtbtn { get; set; }
        public string DateTimePicker1 { get; set; }
        public string DateTimePicker2 { get; set; }

        public bool PublicJob { get; set; }

        public DataTable dtNonPublicExceptionList { get; set; }

        public List<ADPublicException> NonPublicExceptionList { get; set; }
       
    public ADPublicException objADPublicException { get; set; }
    }
}
