﻿
using CRFView.Adapters.Liens.LiensDayEnd.AutoFax.ViewModels;
using HDS.DAL.COMMON;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using CRFView.Adapters.Liens.DayEnds.AutoEmail.ViewModels;
using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.Liens.DataEntry.ImportEDI;
using System.Web.UI.WebControls;
using C1.Win.BarCode;
using CRF.BLL.CRFView;
using HDS.WEBLIB.Common;
using C1.C1Preview;
using System.Collections.ObjectModel;
using CRF.BLL.CRFDB.VIEWS;
using HDS.DAL.Providers;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
      public class ADvwJobInfo
    {
        #region Properties

        public DateTime FromDate { get; set; }
        public DateTime ThruDate { get; set; }
        public string CustFaxNum { get; set; }
        public string GCFaxNum { get; set; }

        #endregion

        #region CRUD


        public static ADvwJobInfo ReadvwJobInfo(string id)
        {
            vwJobInfo myentity = new vwJobInfo();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (vwJobInfo)myentityItable;
            AutoMapper.Mapper.CreateMap<vwJobInfo, ADvwJobInfo>();
            return (AutoMapper.Mapper.Map<ADvwJobInfo>(myentity));
        }


        public static ADvwCollectionReportsList ReadvwCollectionReports(int ReportId)
        {
            vwCollectionReportsList myentity = new vwCollectionReportsList();
            ITable myentityItable = myentity;
            DBO.Provider.Read(ReportId.ToString(), ref myentityItable);
            myentity = (vwCollectionReportsList)myentityItable;
            AutoMapper.Mapper.CreateMap<vwCollectionReportsList, ADvwCollectionReportsList>();
            return (AutoMapper.Mapper.Map<ADvwCollectionReportsList>(myentity));
        }
        public static string PrintAutoFax(AutoFaxVM objAutoFaxVM)
        {
            //string myprt = new Windows.Forms.PrintDialog;
            // var myprt = CRF.BLL.COMMON.
            //String MYNAME = myprt.PrinterSettings.PrinterName
            string pdfPath = null;
            //string sFromDate = null;
            //string sThruDate = null;
            
            try
            {

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var Sp_OBJ = new uspbo_LiensDayEnd_GetAutoFaxes();
               

                if (objAutoFaxVM.chkAllClient == false)
                {
                    Sp_OBJ.ClientId = Convert.ToInt32(objAutoFaxVM.ClientId);
                }
                else
                {
                    Sp_OBJ.ClientId = 0;
                }

                Sp_OBJ.FromDate = objAutoFaxVM.DateTimePicker1;
                Sp_OBJ.ThruDate = objAutoFaxVM.DateTimePicker2;
                //    Sp_OBJ.ThruDate = Convert.ToDateTime(objAutoFaxVM.DateTimePicker2).AddDays(1).ToShortDateString(); 

                //Sp_OBJ.ThruDate = Convert.ToDateTime(objAutoFaxVM.DateTimePicker2).AddDays(1).ToString("MM/dd/yyyy");

               // Sp_OBJ.ThruDate = Convert.ToDateTime(objAutoFaxVM.DateTimePicker2, System.Globalization.CultureInfo.InvariantCulture).AddDays(1).ToShortDateString();


                DataSet ds = CRF.BLL.Providers.DBO.Provider.GetDataSet(Sp_OBJ);

                CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;
                mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;

                if (objAutoFaxVM.RbtPrintOption == "PDF")
                {
                    mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                }
                if (objAutoFaxVM.RbtPrintOption == "Fax")
                {

                    CRF.BLL.CRFView.CRFView.SetDefaultPrinter(CRF.BLL.CRFView.CRFView.GetKeyValue("FAXPRINTER"));

                }






                TableView vw = new TableView(ds.Tables[0]);
                string myreport = "FaxCustAuto";
                pdfPath =  CRF.BLL.Globals.RenderC1Report(user, ds.Tables[0],"LIENREPORTS.xml", myreport, mymode, "CUSTAUTOFAX", "", "");
                //pdfPath = CRF.BLL.COMMON.Reporting.PrintC1Report(user, "LIENREPORTS.xml", myreport,vw, mymode, "CUSTAUTOFAX", "", "");

            



                TableView vw2 = new TableView(ds.Tables[1]);
                myreport = "FaxGenAuto";
                pdfPath = CRF.BLL.Globals.RenderC1Report(user,ds.Tables[1], "LIENREPORTS.xml", myreport, mymode, "GCAUTOFAX", "", "");
               // pdfPath = CRF.BLL.COMMON.Reporting.PrintC1Report(user, "LIENREPORTS.xml", myreport, vw2, mymode, "GCAUTOFAX", "", "");

                    //' customer auto faxes
                TableView vw3 = new TableView(ds.Tables[2]);
                 myreport = "FaxCustAutoTX";
                 CRF.BLL.Globals.RenderC1Report(user, ds.Tables[2], "LIENREPORTS.xml", myreport, mymode, "GCAUTOFAX", "", "");
               //  pdfPath = CRF.BLL.COMMON.Reporting.PrintC1Report(user, "LIENREPORTS.xml", myreport, vw3, mymode, "CUSTAUTOFAXTX", "", "");

                TableView vw4 = new TableView(ds.Tables[3]);
                myreport = "FaxGenAutoTX";
                CRF.BLL.Globals.RenderC1Report(user, ds.Tables[3], "LIENREPORTS.xml", myreport, mymode, "GCAUTOFAX", "", "");
               // pdfPath = CRF.BLL.COMMON.Reporting.PrintC1Report(user, "LIENREPORTS.xml", myreport, vw4, mymode, "GCAUTOFAXTX", "", "");

            }

            catch (Exception ex)
            {
                return "";
            }
            return pdfPath;
        }



        public static string PrintAutoEmail(AutoEmailVM objAutoEmailVM)
        {
            //string myprt = new Windows.Forms.PrintDialog;
            // var myprt = CRF.BLL.COMMON.
            //String MYNAME = myprt.PrinterSettings.PrinterName
            string pdfPath = null;
            //string sFromDate = null;
            //string sThruDate = null;

            try
            {
                

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var Sp_OBJ = new uspbo_LiensDayEnd_GetAutoEmails();

                Sp_OBJ.FromDate = objAutoEmailVM.FromDate;
                Sp_OBJ.ThruDate = objAutoEmailVM.ThruDate;

                DataSet ds = CRF.BLL.Providers.DBO.Provider.GetDataSet(Sp_OBJ);


                // customer auto emails
                TableView vw = new TableView(ds.Tables[0]);
                if(vw.Count > 0)
                {
                      SendEmail("CU", vw);
                }

                //gc auto emails
                TableView vw2 = new TableView(ds.Tables[1]);
                if (vw.Count > 0)
                {
                    SendEmail("GC", vw2);
                }

                //TX customer auto emails
                TableView vw3 = new TableView(ds.Tables[2]);
                if (vw.Count > 0)
                {
                    SendEmail("CU", vw3);
                }

                //gc auto emails
                TableView vw4 = new TableView(ds.Tables[3]);
                if (vw.Count > 0)
                {
                    SendEmail("GC", vw4);
                }

                var result = "Completed";

            }

            catch (Exception ex)
            {
                return "";
            }
            return "Completed";
        }


        public static void SendEmail(string aEmailType, HDS.DAL.COMMON.TableView myview)
        {

            string sSender = "MyJobInfo@crfsolutions.com";
            string sSubject = "";
            string sRecipient = "";
            string sBody = "";
            string sInsert = "";
            string sClientType = "";

            

            //Dim ToCollection As New Collection
            //Dim AttachCollection As New Collection

            myview.MoveFirst();

            //while (myview.EOF() == false)
            //{

                sSubject = "Job information Request for " + myview.get_RowItem("JobName") + " (#" + myview.get_RowItem("JobId") + ")";
                if (aEmailType == "CU")
                {
                    sRecipient = Convert.ToString(myview.get_RowItem("CustEmail"));
                }
                else
                {
                    sRecipient = Convert.ToString(myview.get_RowItem("GCEmail"));
                }

                sBody = "[ClientName] has supplied labor, material or equipment to [CustomerName] for the project listed below. We " + Environment.NewLine;
                sBody += "are reaching out to you because they have contracted with us to make sure their records for the job " + Environment.NewLine;
                sBody += "listed below are accurate and complete." + Environment.NewLine;
                sBody += Environment.NewLine;


                sBody += "Job Name: [JobName]" + Environment.NewLine;
                sBody += "Job Address: [JobAdd1] [JobAdd2], [JobCity], [JobState] [JobZip]" + Environment.NewLine;
                sBody += "PO#: [PONum]" + Environment.NewLine;
                sBody += "Job#: [JobNum]" + Environment.NewLine;
                sBody += Environment.NewLine;

                sBody += "If you have a job sheet for this project, please send it to us. If not, please confirm the job address, " + Environment.NewLine;
                sBody += "provide us with the name and address of the Property Owner and the Prime Contractor. Please provide " + Environment.NewLine;
                sBody += "Surety if there is a Payment Bond or Lender, if applicable, for this project." + Environment.NewLine;
                sBody += Environment.NewLine;

                sBody += "If you have any questions you can call us at [EmpPhNum], or you can simply reply to this email with the above information." + Environment.NewLine;
                sBody += Environment.NewLine;
                sBody += "Thank you";
                sBody += Environment.NewLine;
                sBody += Environment.NewLine;

                sBody += "Job Information (If different from above)" + Environment.NewLine;
                sBody += Environment.NewLine;

                sBody += "_________________________________________" + Environment.NewLine;
                sBody += Environment.NewLine;
                sBody += "_________________________________________" + Environment.NewLine;
                sBody += Environment.NewLine;
                sBody += "_________________________________________" + Environment.NewLine;
                sBody += Environment.NewLine;
                sBody += "Property Owner" + Environment.NewLine;
                sBody += Environment.NewLine;
                sBody += "_________________________________________" + Environment.NewLine;
                sBody += Environment.NewLine;
                sBody += "_________________________________________" + Environment.NewLine;
                sBody += Environment.NewLine;
                sBody += "_________________________________________" + Environment.NewLine;
                sBody += Environment.NewLine;
                sBody += "Prime/General Contractor" + Environment.NewLine;
                sBody += Environment.NewLine;
                sBody += "_________________________________________" + Environment.NewLine;
                sBody += Environment.NewLine;
                sBody += "_________________________________________" + Environment.NewLine;
                sBody += Environment.NewLine;
                sBody += "_________________________________________" + Environment.NewLine;
                sBody += Environment.NewLine;
                sBody += "Construction Lender/Surety Company (If applicable)" + Environment.NewLine;
                sBody += Environment.NewLine;
                sBody += "_________________________________________" + Environment.NewLine;
                sBody += Environment.NewLine;
                sBody += "_________________________________________" + Environment.NewLine;
                sBody += Environment.NewLine;
                sBody += "_________________________________________" + Environment.NewLine;


             
                sBody = sBody.Replace("[ClientName]", Convert.ToString(myview.get_RowItem("ClientName")));
                sBody = sBody.Replace("[CustomerName]", Convert.ToString(myview.get_RowItem("CustName")));
                sBody = sBody.Replace("[JobName]", Convert.ToString(myview.get_RowItem("JobName")));
                sBody = sBody.Replace("[JobAdd1]", Convert.ToString(myview.get_RowItem("JobAdd1")));
                sBody = sBody.Replace("[JobAdd2]", Convert.ToString(myview.get_RowItem("JobAdd2")));
                sBody = sBody.Replace("[JobCity]", Convert.ToString(myview.get_RowItem("JobCity")));


                sBody = sBody.Replace("[JobState]", Convert.ToString(myview.get_RowItem("JobState")));
                sBody = sBody.Replace("[JobZip]", Convert.ToString(myview.get_RowItem("JobZip")));
                sBody = sBody.Replace("[PONum]", Convert.ToString(myview.get_RowItem("PONum")));
                sBody = sBody.Replace("[JobNum]", Convert.ToString(myview.get_RowItem("JobNum")));
                sBody = sBody.Replace("[JobNum]", Convert.ToString(myview.get_RowItem("JobNum")));
                //sBody = sBody.Replace("[EmpPhNum]", "(" + myview.get_RowItem("EmpPhNum").ToString().Substring(1, 3) + ") " + myview.get_RowItem("EmpPhNum").ToString().Substring(4, 3) + "-" + myview.get_RowItem("EmpPhNum").ToString().Substring(7, 4));
                sBody = sBody.Replace("[EmpPhNum]", "(" + myview.get_RowItem("EmpPhNum").ToString().Substring(0, 3) + ") " + myview.get_RowItem("EmpPhNum").ToString().Substring(3, 3) + "-" + myview.get_RowItem("EmpPhNum").ToString().Substring(6, 4));

                var s =   CRF.BLL.CRFView.CRFView.SendSMTPEmail(sSender, sSubject, sBody, sRecipient);

               //if(s == false )
               // {
               //     return false;
               // }
               //else
               // {
               //     return true;
               // }


                //myview.MoveNext();
                

            //}
           
        }
        public static DataTable GetJobInfo(string id)
        {
            string sql = "Select * from vwJobInfo where JobId = " + id;

            var EntityList = DBO.Provider.GetTableView(sql);
            DataTable dt = EntityList.ToTable();
            return dt;
        }
        public static DataTable GetClientInfo(string id)
        {
            string sql = "Select * from ClientLienInfo where JobId = " + id;

            var EntityList = DBO.Provider.GetTableView(sql);
            DataTable dt = EntityList.ToTable();
            return dt;
        }
        public static bool NoCustFax(string id)
        {
            DataTable dt = GetJobInfo(id);
            DataTable dtClientInfo = GetJobInfo(id);
            string CustFaxNum = "";
            bool NoFaxCU = false;
            bool OwnerSourceCust = false;
            bool OwnerSourceCustFax = false;
            bool NOCustCall = false;
            if (dt.Rows[0]["CustFaxNum"] == DBNull.Value || dt.Rows[0]["CustFaxNum"].ToString() == "")
            {
                CustFaxNum = "";
            }
            else
            {
                CustFaxNum = dt.Rows[0]["CustFaxNum"].ToString();
            }
            if (dt.Rows[0]["NoFaxCU"] == DBNull.Value || dt.Rows[0]["NoFaxCU"].ToString() == "")
            {
                NoFaxCU = false;
            }
            else
            {
                NoFaxCU = (bool)dt.Rows[0]["NoFaxCU"];
            }
            if (dt.Rows[0]["OwnerSourceCust"] == DBNull.Value || dt.Rows[0]["OwnerSourceCust"].ToString() == "")
            {
                OwnerSourceCust = false;
            }
            else
            {
                OwnerSourceCust = (bool)dt.Rows[0]["OwnerSourceCust"];
            }

            if (dt.Rows[0]["OwnerSourceCustFax"] == DBNull.Value || dt.Rows[0]["OwnerSourceCustFax"].ToString() == "")
            {
                OwnerSourceCustFax = false;
            }
            else
            {
                OwnerSourceCustFax = (bool)dt.Rows[0]["OwnerSourceCustFax"];
            }

            if (dt.Rows[0]["NOCustCall"] == DBNull.Value || dt.Rows[0]["NOCustCall"].ToString() == "")
            {
                NOCustCall = false;
            }
            else
            {
                NOCustCall = (bool)dtClientInfo.Rows[0]["NOCustCall"];
            }

           
            if (CustFaxNum.Length < 3 || NoFaxCU == true  || OwnerSourceCust == true || OwnerSourceCustFax == true || NOCustCall == true)
           {
                return true;
          }
            //  If Len(myjobinfo.CustFaxNum) < 3 Or myjobinfo.NoFaxCU = True Or myjobinfo.OwnerSourceCust = True Or myjobinfo.OwnerSourceCustFax = True Or mycli.NOCustCall = True Then
            //    Return True
            //End If
            return false;
        }
        public static bool NoGCFax(string id)
        {

            DataTable dt = GetJobInfo(id);
            DataTable dtClientInfo = GetJobInfo(id);

            string GCFaxNum = "";
            bool NoFaxGC = false;
            bool OwnerSourceGC = false;
            bool OwnerSourceGCFax = false;
            bool NoCallsCustGc = false;
            bool NOGCCall = false;
            if (dt.Rows[0]["GCFaxNum"] == DBNull.Value || dt.Rows[0]["GCFaxNum"].ToString() == "")
            {
                GCFaxNum = "";
            }
            else
            {
                GCFaxNum = dt.Rows[0]["GCFaxNum"].ToString();
            }
            if (dt.Rows[0]["NoFaxGC"] == DBNull.Value || dt.Rows[0]["NoFaxGC"].ToString() == "")
            {
                NoFaxGC = false;
            }
            else
            {
                NoFaxGC = (bool)dt.Rows[0]["NoFaxGC"];
            }
            if (dt.Rows[0]["OwnerSourceGC"] == DBNull.Value || dt.Rows[0]["OwnerSourceGC"].ToString() == "")
            {
                OwnerSourceGC = false;
            }
            else
            {
                OwnerSourceGC = (bool)dt.Rows[0]["OwnerSourceGC"];
            }

            if (dt.Rows[0]["OwnerSourceGCFax"] == DBNull.Value || dt.Rows[0]["OwnerSourceGCFax"].ToString() == "")
            {
                OwnerSourceGCFax = false;
            }
            else
            {
                OwnerSourceGCFax = (bool)dt.Rows[0]["OwnerSourceGCFax"];
            }

            if (dt.Rows[0]["NoCallsCustGc"] == DBNull.Value || dt.Rows[0]["NoCallsCustGc"].ToString() == "")
            {
                NoCallsCustGc = false;
            }
            else
            {
                NoCallsCustGc = (bool)dtClientInfo.Rows[0]["NoCallsCustGc"];
            }

            if (dt.Rows[0]["NOGCCall"] == DBNull.Value || dt.Rows[0]["NOGCCall"].ToString() == "")
            {
                NOGCCall = false;
            }
            else
            {
                NOGCCall = (bool)dtClientInfo.Rows[0]["NOGCCall"];
            }


            if (GCFaxNum.Length < 3 || NoFaxGC == true || OwnerSourceGC == true || OwnerSourceGCFax == true || NoCallsCustGc == true || NOGCCall == true)
            {
                return true;
            }
          

            return false;
        }

        public static bool NoCustEmail(string id)
        {

            DataTable dt = GetJobInfo(id);
            DataTable dtClientInfo = GetJobInfo(id);
            
            string CustEmail = "";
            bool NoEmailCU = false;
            bool OwnerSourceCust = false;
            bool OwnerSourceCustFax = false;
            bool NOCustCall = false;
            if (dt.Rows[0]["CustEmail"] == DBNull.Value || dt.Rows[0]["CustEmail"].ToString() == "")
            {
                CustEmail = "";
            }
            else
            {
                CustEmail = dt.Rows[0]["CustEmail"].ToString();
            }
            if (dt.Rows[0]["NoEmailCU"] == DBNull.Value || dt.Rows[0]["NoEmailCU"].ToString() == "")
            {
                NoEmailCU = false;
            }
            else
            {
                NoEmailCU = (bool)dt.Rows[0]["NoEmailCU"];
            }
            if (dt.Rows[0]["OwnerSourceCust"] == DBNull.Value || dt.Rows[0]["OwnerSourceCust"].ToString() == "")
            {
                OwnerSourceCust = false;
            }
            else
            {
                OwnerSourceCust = (bool)dt.Rows[0]["OwnerSourceCust"];
            }

            if (dt.Rows[0]["OwnerSourceCustFax"] == DBNull.Value || dt.Rows[0]["OwnerSourceCustFax"].ToString() == "")
            {
                OwnerSourceCustFax = false;
            }
            else
            {
                OwnerSourceCustFax = (bool)dt.Rows[0]["OwnerSourceCust"];
            }

            if (dt.Rows[0]["NOCustCall"] == DBNull.Value || dt.Rows[0]["NOCustCall"].ToString() == "")
            {
                NOCustCall = false;
            }
            else
            {
                NOCustCall = (bool)dtClientInfo.Rows[0]["NOCustCall"];
            }

            if (CustEmail.Length < 3 || NoEmailCU == true || OwnerSourceCust == true || OwnerSourceCustFax == true || NOCustCall == true)
            {
                return true;
            }
            

            return false;
        }
        public static bool NoGCEmail(string id)
        {

            DataTable dt = GetJobInfo(id);
            DataTable dtClientInfo = GetJobInfo(id);
          
            string GCEmail = "";
            bool NoEmailGC = false;
            bool OwnerSourceGC = false;
            bool OwnerSourceGCFax = false;
            bool NoCallsCustGc = false;
            bool NOGCCall = false;
            if (dt.Rows.Count > 0 || dtClientInfo.Rows.Count > 0)
            {

                if (dt.Rows[0]["GCEmail"] == DBNull.Value || dt.Rows[0]["GCEmail"].ToString() == "")
                {
                    GCEmail = "";
                }
                else
                {
                    GCEmail = dt.Rows[0]["GCEmail"].ToString();
                }
                if (dt.Rows[0]["NoEmailGC"] == DBNull.Value || dt.Rows[0]["NoEmailGC"].ToString() == "")
                {
                    NoEmailGC = false;
                }
                else
                {
                    NoEmailGC = (bool)dt.Rows[0]["NoEmailGC"];
                }
                if (dt.Rows[0]["OwnerSourceGC"] == DBNull.Value || dt.Rows[0]["OwnerSourceGC"].ToString() == "")
                {
                    OwnerSourceGC = false;
                }
                else
                {
                    OwnerSourceGC = (bool)dt.Rows[0]["OwnerSourceGC"];
                }

                if (dt.Rows[0]["OwnerSourceGCFax"] == DBNull.Value || dt.Rows[0]["OwnerSourceGCFax"].ToString() == "")
                {
                    OwnerSourceGCFax = false;
                }
                else
                {
                    OwnerSourceGCFax = (bool)dt.Rows[0]["OwnerSourceGCFax"];
                }

                if (dt.Rows[0]["NoCallsCustGc"] == DBNull.Value || dt.Rows[0]["NoCallsCustGc"].ToString() == "")
                {
                    NoCallsCustGc = false;
                }
                else
                {
                    NoCallsCustGc = (bool)dtClientInfo.Rows[0]["NoCallsCustGc"];
                }

                if (dt.Rows[0]["NOGCCall"] == DBNull.Value || dt.Rows[0]["NOGCCall"].ToString() == "")
                {
                    NOGCCall = false;
                }
                else
                {
                    NOGCCall = (bool)dtClientInfo.Rows[0]["NOGCCall"];
                }


                if (GCEmail.Length < 3 || NoEmailGC == true || NoCallsCustGc == true || OwnerSourceGC == true || OwnerSourceGCFax == true || NOGCCall == true)
                {
                    return true;
                }

            }
            return false;
        }

        public static bool BranchCustflag(string BranchNumber, int ClientId)
        {
            string sql = "Select IsNoCallCustBranch From Client Where ParentClientId = " + ClientId + " And IsBranch = 1 and BranchNumber = '" + BranchNumber + "'";

            var EntityList = DBO.Provider.GetTableView(sql);
            DataTable dt = EntityList.ToTable();

            bool IsNoCallCustBranch = false;
            if(dt.Rows.Count > 0)
            {

           
            if (dt.Rows[0]["IsNoCallCustBranch"] == null || dt.Rows[0]["IsNoCallCustBranch"].ToString() == "")
            {
                 IsNoCallCustBranch = false;
            }
            else
            {
                 IsNoCallCustBranch = (bool)dt.Rows[0]["IsNoCallCustBranch"];
            }
            }
            else
            {
                IsNoCallCustBranch = false;
            }

            if (dt.Rows.Count > 0)
            {
                if(IsNoCallCustBranch == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
            
        }

        public static bool BranchGcflag(string BranchNumber, int ClientId)
        {
            string sql = "Select IsNoCallGCBranch From Client Where ParentClientId = " + ClientId + " And IsBranch = 1 and BranchNumber = '" + BranchNumber + "'";

            var EntityList = DBO.Provider.GetTableView(sql);
            DataTable dt = EntityList.ToTable();

            bool IsNoCallGCBranch = false;
            if (dt.Rows.Count > 0)
            {


                if (dt.Rows[0]["IsNoCallGCBranch"] == null || dt.Rows[0]["IsNoCallGCBranch"].ToString() == "")
                {
                    IsNoCallGCBranch = false;
                }
                else
                {
                    IsNoCallGCBranch = (bool)dt.Rows[0]["IsNoCallGCBranch"];
                }
            }
            else
            {
                IsNoCallGCBranch = false;
            }
            if (dt.Rows.Count > 0)
            {
                if (IsNoCallGCBranch == true)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }

        }

        #endregion

    }
}
