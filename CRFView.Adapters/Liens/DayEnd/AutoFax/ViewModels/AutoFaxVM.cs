﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Liens.LiensDayEnd.AutoFax.ViewModels
{
       public  class AutoFaxVM
    {
        public  AutoFaxVM()
        {
            GetClientList = CommonBindList.GetClientIdList();
        }
        public IEnumerable<SelectListItem> GetClientList { get; set; }

        public string DateTimePicker1 { get; set; }
        public string DateTimePicker2 { get; set; }

        public string ClientId { get; set; }
    
        public bool chkAllClient { get; set; }
        public string RbtPrintOption { get; set; }
    }
}
