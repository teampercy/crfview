﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.DayEnds.AutoEmail.ViewModels
{
    public class AutoEmailVM
    {
        public AutoEmailVM()
        {
           // GetClientList = CommonBindList.GetClientIdList();
        }
       // public IEnumerable<SelectListItem> GetClientList { get; set; }

        public ADvwJobInfo objADvwJobInfo { get; set; }
        public string ClientId { get; set; }
        public string FromDate { get; set; }
        public string ThruDate { get; set; }

        public string Message { get; set; }


        //public bool chkAllClient { get; set; }
        //public string RbtPrintOption { get; set; }
    }
}
