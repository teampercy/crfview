﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard
{
    public class ADvwLienNotes
    {

        #region Properties

        public int Id { get; set; }
        public int JobId { get; set; }
        public string UserId { get; set; }
        public int EventTypeId { get; set; }
        public DateTime DateCreated { get; set; }
        public string Subject { get; set; }
        public string Note { get; set; }
        public bool ClientView { get; set; }
        public int ActionTypeId { get; set; }
        public DateTime CloseDate { get; set; }
        public int EnteredByUserId { get; set; }
        public int ClosedByUserId { get; set; }
        public bool SQLBox { get; set; }
        public int AltKeyId { get; set; }
        public bool IsConverted { get; set; }
        public int NoteTypeId { get; set; }
        public string NoteType { get; set; }

        public string Type { get; set; }

        #endregion

        #region CRUD

        public static List<ADvwLienNotes> GetNotes(DataTable dt)
        {
            List<ADvwLienNotes> Notes = (from DataRow dr in dt.Rows
                                         select new ADvwLienNotes()
                                         {
                                             Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                             UserId = ((dr["UserId"] != DBNull.Value ? dr["UserId"].ToString() : "")),
                                             DateCreated = ((dr["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dr["DateCreated"]) : DateTime.MinValue)),
                                             Note = ((dr["UserId"] != DBNull.Value ? dr["Note"].ToString() : "")),
                                             EnteredByUserId = ((dr["EnteredByUserId"] != DBNull.Value ? Convert.ToInt32(dr["EnteredByUserId"]) : 0)),
                                             NoteTypeId = ((dr["NoteTypeId"] != DBNull.Value ? Convert.ToInt32(dr["NoteTypeId"]) : 0)),
                                             NoteType = ((dr["NoteType"] != DBNull.Value ? dr["NoteType"].ToString() : "")),
                                             Type = ((dr["type"] != DBNull.Value ? dr["type"].ToString() : "")),
                                         }).ToList();
            return Notes;
        }


        #endregion



    }
}
