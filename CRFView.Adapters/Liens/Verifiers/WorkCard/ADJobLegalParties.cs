﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard
{
   public class ADJobLegalParties
    {

        #region Properties
        public int Id { get; set; }
        public int JobId { get; set; }
        public string TypeCode { get; set; }
        public string CertNum { get; set; }
        public string RefNum { get; set; }
        public string AddressName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Telephone1 { get; set; }
        public string Telephone2 { get; set; }
        public string Telephone3 { get; set; }
        public string Fax { get; set; }
        public bool BadAddressFlag { get; set; }
        public bool IsPrimary { get; set; }
        public int SecondaryId { get; set; }
        public bool MLAgent { get; set; }
        public String MatchingKey { get; set; }
        public bool IsNonUSAddr { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        #endregion


        #region CRUD
        public static ADJobLegalParties ReadJobLegalParties(int id)
    {
        JobLegalParties myentity = new JobLegalParties();
        ITable myentityItable = myentity;
        DBO.Provider.Read(id.ToString(), ref myentityItable);
        myentity = (JobLegalParties)myentityItable;
        AutoMapper.Mapper.CreateMap<JobLegalParties, ADJobLegalParties>();
        return (AutoMapper.Mapper.Map<ADJobLegalParties>(myentity));
    }
        public static ADJobLegalParties ReadJobLegalPartiesByCondition(string aDesigneeId,[Optional] bool IsPrimary)
        {
            OwnerEditVM objOwnerEditVM = new OwnerEditVM();
            //int DesigneeID = 0;
            int DesigneeID = Convert.ToInt32(aDesigneeId);
            if (Convert.ToInt32(aDesigneeId) == 0)
            {
                DesigneeID = Convert.ToInt32(aDesigneeId);
            }
            else if(DesigneeID > 0)
            {
              objOwnerEditVM.objADJobLegalParties = ReadJobLegalParties(DesigneeID);
            }
            else if(IsPrimary == false)
            {
                objOwnerEditVM.objADJobLegalParties = ReadJobLegalParties(DesigneeID);
            }
            else
            {
                objOwnerEditVM.objADJobLegalParties = ReadJobLegalParties(DesigneeID);
            }
                
           return objOwnerEditVM.objADJobLegalParties;
        }
        public static DataTable GetOWListDt(string searchInitial, string ClientId, string RdbBtnVal)
        {
            try
            {
                var sp_obj = new uspbo_Jobs_GetOWList();

                sp_obj.ClientId = Convert.ToInt32(ClientId);
                
                if (RdbBtnVal == "N")
                {
                    sp_obj.Name = $"{searchInitial}%";
                }
                else
                {
                    sp_obj.Address = $"{searchInitial}%";
                }


                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sp_obj);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        public static List<string> GetOWLookUpList1(DataTable dt)
        {

            var yy = new List<string>();
            foreach (DataRow dr in dt.Rows)
            {
                string name = dr["name"].ToString();
                //string Ref = dr["Ref"].ToString();
                string Address = dr["Address"].ToString();
                string Id = dr["Id"].ToString();


                yy.Add(name + "|" + Address + "|" + Id);
            }
            //yy.Add(dr["name"].ToString());
            //yy.Add(dr["Address"].ToString());


            return yy;
        }


        public static List<ADJobLegalParties> GetOWLookUpList(DataTable dt)
        {
            //DataTable dt = GetCustomerLookUpDt(objDeleteCustomerVM);

            List<ADJobLegalParties> List = (from DataRow dr in dt.Rows
                                                    select new ADJobLegalParties()
                                                    {
                                                        //ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
                                                        Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                                        Name = ((dr["Name"] != DBNull.Value ? dr["Name"].ToString() : "")),
                                                        // Ref = ((dr["Ref"] != DBNull.Value ? dr["Ref"].ToString() : "")),
                                                        Address = ((dr["Address"] != DBNull.Value ? dr["Address"].ToString() : "")),
                                                    }).ToList();

            return List;
        }

        public static DataTable GetLenderListDt(string searchInitial, string ClientId, string RdbBtnVal)
        {
            try
            {
                var sp_obj = new uspbo_Jobs_GetLEList();

                sp_obj.ClientId = Convert.ToInt32(ClientId);
               
                if (RdbBtnVal == "N")
                {
                    sp_obj.Name = $"{searchInitial}%";
                }
                else
                {
                    sp_obj.Address = $"{searchInitial}%";
                }


                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sp_obj);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        public static DataTable GetDesigneeListDt(string searchInitial, string ClientId, string RdbBtnVal)
        {
            try
            {
                var sp_obj = new uspbo_Jobs_GetDEList();

                sp_obj.ClientId = Convert.ToInt32(ClientId);
                if (RdbBtnVal == "N")
                {
                    sp_obj.Name = $"{searchInitial}%";
                }
                else
                {
                    sp_obj.Address = $"{searchInitial}%";
                }



                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sp_obj);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }
        public static List<ADJobLegalParties> GetJobLegalPartiesList(string JobID)
        {
            
            JobVM ObjJobVM;
            //DataTable dt = GetCustomerLookUpDt(objDeleteCustomerVM);
            DataTable dt;
            ObjJobVM = JobInfo.LoadJobInfo(JobID);
            dt = ObjJobVM.dtNotice;
            
            List<ADJobLegalParties> List = (from DataRow dr in dt.Rows
                                            select new ADJobLegalParties()
                                            {
                                                //ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
                                                Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                                TypeCode = ((dr["TypeCode"] != DBNull.Value ? dr["TypeCode"].ToString() : "")),
                                                IsPrimary = ((dr["IsPrimary"] != DBNull.Value ? Convert.ToBoolean(dr["IsPrimary"]) : false)),
                                                // Ref = ((dr["Ref"] != DBNull.Value ? dr["Ref"].ToString() : "")),
                                                AddressName = ((dr["AddressName"] != DBNull.Value ? dr["AddressName"].ToString() : "")),
                                            }).ToList();

            return List;
        }

        public static List<ADJobLegalParties> JobLegalPartiesList(DataTable dt)
        {
            List<ADJobLegalParties> JobLegalPartiesList = (from DataRow dr in dt.Rows
                                            select new ADJobLegalParties()
                                            {
                                                //ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
                                                Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                                TypeCode = ((dr["TypeCode"] != DBNull.Value ? dr["TypeCode"].ToString() : "")),
                                                IsPrimary = ((dr["IsPrimary"] != DBNull.Value ? Convert.ToBoolean(dr["IsPrimary"]) : false)),
                                                // Ref = ((dr["Ref"] != DBNull.Value ? dr["Ref"].ToString() : "")),
                                                AddressName = ((dr["AddressName"] != DBNull.Value ? dr["AddressName"].ToString() : "")),
                                                Telephone1 = ((dr["Telephone1"] != DBNull.Value ? dr["Telephone1"].ToString() : "")),
                                                
                                            }).ToList();
            return JobLegalPartiesList;
        }





        public static int SaveOwner(OwnerEditVM objOwnerEditVM, int OwnrID,int PrimaryOwnerId)
        {
            try
            {
                objOwnerEditVM.objJobEdit = ADJob.ReadJob(Convert.ToInt32(objOwnerEditVM.JobId));
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                bool value = false;
                bool IsSameAsCustChecked = false;
               
                ADJobLegalParties objADJobLegalParties = new ADJobLegalParties();
                //ADClientGeneralContractor objADClientGeneralContractor = ReadClientGeneralContractor(CustId);
                AutoMapper.Mapper.CreateMap<ADJobLegalParties, JobLegalParties>();
                JobLegalParties myentity;
                myentity = AutoMapper.Mapper.Map<JobLegalParties>(objADJobLegalParties);
                
                myentity.Id = OwnrID;
                myentity.JobId = Convert.ToInt32(objOwnerEditVM.JobId);
                myentity.AddressName = objOwnerEditVM.objADJobLegalParties.AddressName;
                myentity.Telephone1 = objOwnerEditVM.objADJobLegalParties.Telephone1;
                myentity.AddressLine1 = objOwnerEditVM.objADJobLegalParties.AddressLine1;
                myentity.AddressLine2 = objOwnerEditVM.objADJobLegalParties.AddressLine2;
                myentity.City = objOwnerEditVM.objADJobLegalParties.City;
                myentity.State = objOwnerEditVM.objADJobLegalParties.State;
                myentity.PostalCode = objOwnerEditVM.objADJobLegalParties.PostalCode;
                myentity.Fax = objOwnerEditVM.objADJobLegalParties.Fax;
                myentity.TypeCode = "OW";


                if (objOwnerEditVM.objADJobLegalParties.IsNonUSAddr == true)
                {
                    myentity.IsNonUSAddr = objOwnerEditVM.objADJobLegalParties.IsNonUSAddr;
                }
                else
                {
                    objOwnerEditVM.objADJobLegalParties.IsNonUSAddr = false;
                }

                if (objOwnerEditVM.objADJobLegalParties.MLAgent == true)
                {
                    myentity.MLAgent = objOwnerEditVM.objADJobLegalParties.MLAgent;
                }
                else
                {
                    objOwnerEditVM.objADJobLegalParties.MLAgent = false;
                }

              if(OwnrID < 1 && objOwnerEditVM.objJobEdit.OwnrId > 1)
                {
                    myentity.IsPrimary = false;
                    objOwnerEditVM.IsPrimary = false;
                }
                if (objOwnerEditVM.objJobEdit.OwnrId < 1)
                {
                    myentity.IsPrimary = true;
                    objOwnerEditVM.IsPrimary = true;
                }

                if(objOwnerEditVM.objJobEdit.OwnrId < 1)
                {
                    LogOwnerChanges(OwnrID, myentity, (objOwnerEditVM.objJobEdit.OwnrId == 0) ? true : false);
                    ITable tblI = myentity;

                    DBO.Provider.Create(ref tblI);
                }
                else
                {
                    if (objOwnerEditVM.chkChangeOWToLE == true)
                    { 
                        myentity.MLAgent = false;
                        myentity.TypeCode = "LE";
                    LogOwnerChanges(OwnrID, myentity, (objOwnerEditVM.objJobEdit.OwnrId == 0) ? true : false, false,true);
                    }

                    else if(objOwnerEditVM.chkChangeOWToDE == true)
                    {
                        myentity.MLAgent = false;
                        myentity.TypeCode = "DE";

                        LogOwnerChanges(OwnrID, myentity, (objOwnerEditVM.objJobEdit.OwnrId == 0) ? true : false, false, true);
                    }
                    else if(objOwnerEditVM.chkChangeToPrimary == true)
                    {
                        ADJobLegalParties myPrimaryOwnr = ADJobLegalParties.ReadJobLegalParties(PrimaryOwnerId);
                        myPrimaryOwnr.IsPrimary = false;
                        ITable tblJobLegalParties = (ITable)myPrimaryOwnr;
                        DBO.Provider.Update(ref tblJobLegalParties);
                        LogOwnerChanges(OwnrID, myentity, (objOwnerEditVM.objJobEdit.OwnrId == 0) ? true : false, false,false, true);
                        myentity.IsPrimary = true;
                        objOwnerEditVM.IsPrimary = true;
                    }
                    else
                    {
                        LogOwnerChanges(OwnrID, myentity, (objOwnerEditVM.objJobEdit.OwnrId == 0) ? true : false);

                    }
                    ITable tblI = myentity;
                    DBO.Provider.Update(ref tblI);
                }
                AutoMapper.Mapper.CreateMap<ADJob, Job>();
                Job myJob;
                myJob = AutoMapper.Mapper.Map<Job>(objOwnerEditVM.objJobEdit);

                if (objOwnerEditVM.IsPrimary == true)
                {
                    objOwnerEditVM.objJobEdit.OwnrId = myentity.Id;
                    if(objOwnerEditVM.chkMakeSameAsGc == true)
                    {
                        objOwnerEditVM.objJobEdit.SameAsGC = objOwnerEditVM.chkMakeSameAsGc;
                        
                        ITable tbljob = myJob;
                        DBO.Provider.Update(ref tbljob);
                    }
                }
              
                return 1;


            }

            catch (Exception ex)
            {
                return 0;
            }
        }

        public static void LogOwnerChanges(int LegalPartyId,JobLegalParties myentity, bool IsCreate, [Optional] bool IsDelete, [Optional] bool IsChangeToLE, [Optional] bool IsChangeToPrimary, [Optional] bool IsChangeToDE)
        {
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

            OwnerEditVM objOwnerEditVM = new OwnerEditVM();
            ADJobLegalParties objADJobLegalParties = ADJobLegalParties.ReadJobLegalParties(LegalPartyId);
            var mysproc = new CRF.BLL.CRFDB.SPROCS.uspbo_Jobs_UpdateChangeLog();

            mysproc.JobId = myentity.JobId;
            mysproc.UserId = user.Id;
            mysproc.UserName = user.UserName;
            mysproc.ChangeType = 4;
            mysproc.SecondaryId = objADJobLegalParties.Id;

            

            if (myentity.Id > 0)
            {

                mysproc.ChangeFrom = objADJobLegalParties.AddressName.Trim() + "~";
                mysproc.ChangeFrom += objADJobLegalParties.AddressLine1.Trim() + "~";
                mysproc.ChangeFrom += objADJobLegalParties.AddressLine2.Trim() + "~";
                mysproc.ChangeFrom += objADJobLegalParties.City.Trim() + "~";
                mysproc.ChangeFrom += objADJobLegalParties.State.Trim() + "~";
                mysproc.ChangeFrom += objADJobLegalParties.PostalCode.Trim() + "~";
              
                
            }

            if (IsDelete == true)
            {
                mysproc.ChangeTo = "DELETED";
            }
            else if(IsChangeToLE == true)
            {
                mysproc.ChangeTo = "Changed to Lender";
            }
            else if (IsChangeToDE == true)
            {
                mysproc.ChangeTo = "Changed to Primary";
            }
            else if (IsChangeToPrimary == true)
            {
                mysproc.ChangeTo = "Changed to Designee";
            }
            else
            {

            mysproc.ChangeTo = myentity.AddressName.Trim() + "~";
                mysproc.ChangeTo += myentity.AddressLine1.Trim() + "~";
                mysproc.ChangeTo += myentity.AddressLine2.Trim() + "~";
                mysproc.ChangeTo += myentity.City.Trim() + "~";
                mysproc.ChangeTo += myentity.State.Trim() + "~";
                mysproc.ChangeTo += myentity.PostalCode.Trim() + "~";
                
            }

            if (mysproc.ChangeFrom != mysproc.ChangeTo)
            {
                DBO.Provider.ExecNonQuery(mysproc);
            }

        }


        public static int DeleteOwner(OwnerEditVM objOwnerEditVM, int OwnrID)
        {
            try
            {
                bool value = false;
                objOwnerEditVM.objJobEdit = ADJob.ReadJob(Convert.ToInt32(objOwnerEditVM.JobId));
                ADJobLegalParties myentity = new ADJobLegalParties();
                var myLegalParties = new JobLegalParties();
               
                ITable ILegalParties = myLegalParties;
                DBO.Provider.Read(OwnrID.ToString(), ref ILegalParties);
                myLegalParties = (JobLegalParties)ILegalParties;




                LogOwnerChanges(OwnrID, myLegalParties, (objOwnerEditVM.objJobEdit.OwnrId == 0) ? true : false);
                ILegalParties = myLegalParties;
                DBO.Provider.Delete(ref ILegalParties);
                if (objOwnerEditVM.IsPrimary == true)
                {
                    var myacc = new Job();

                    ITable IJob = myacc;
                    DBO.Provider.Read(myacc.Id.ToString(), ref IJob);
                    
                    myacc.SameAsGC = false;
                    myacc.OwnrId = 0;
                    IJob = myacc;
                    DBO.Provider.Update(ref IJob);
                }

                var mysproc = new CRF.BLL.CRFDB.SPROCS.uspbo_Jobs_UpdatePrimaryLegalParty();
                mysproc.JobId = objOwnerEditVM.objJobEdit.Id;
                DBO.Provider.ExecNonQuery(mysproc);
               
                return 1;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static int SaveLender(OwnerEditVM objOwnerEditVM, int LenderId)
        {
            try
            {
                objOwnerEditVM.objJobEdit = ADJob.ReadJob(Convert.ToInt32(objOwnerEditVM.JobId));

                ADJobLegalParties objADJobLegalParties = new ADJobLegalParties();
                //ADClientGeneralContractor objADClientGeneralContractor = ReadClientGeneralContractor(CustId);
                AutoMapper.Mapper.CreateMap<ADJobLegalParties, JobLegalParties>();
                JobLegalParties myentity;
                myentity = AutoMapper.Mapper.Map<JobLegalParties>(objADJobLegalParties);

                myentity.Id = LenderId;
                myentity.JobId = Convert.ToInt32(objOwnerEditVM.JobId);
                myentity.AddressName = objOwnerEditVM.objADJobLegalParties.AddressName;
                myentity.Telephone1 = objOwnerEditVM.objADJobLegalParties.Telephone1;
                myentity.Fax = objOwnerEditVM.objADJobLegalParties.Fax;
                myentity.AddressLine1 = objOwnerEditVM.objADJobLegalParties.AddressLine1;
                myentity.AddressLine2 = objOwnerEditVM.objADJobLegalParties.AddressLine2;
                myentity.City = objOwnerEditVM.objADJobLegalParties.City;
                myentity.State = objOwnerEditVM.objADJobLegalParties.State;
                myentity.PostalCode = objOwnerEditVM.objADJobLegalParties.PostalCode;
                myentity.IsPrimary = objOwnerEditVM.IsPrimary;
                myentity.TypeCode = "LE";
                myentity.RefNum = objOwnerEditVM.objADJobLegalParties.RefNum;
                myentity.IsNonUSAddr = objOwnerEditVM.objADJobLegalParties.IsNonUSAddr;

                if(LenderId == 0 && Convert.ToInt32(objOwnerEditVM.objJobEdit.LenderId) > 1)
                {
                    myentity.IsPrimary = false;
                }
                if(objOwnerEditVM.objJobEdit.LenderId < 1)
                {
                    myentity.IsPrimary = true;
                    objOwnerEditVM.IsPrimary = true;
                }

                ITable tblI = myentity;
                if (LenderId < 1)
                {
                    LogLenderChanges(LenderId, myentity, (objOwnerEditVM.objJobEdit.LenderId == 0) ? true : false);
                     tblI = myentity;
                    DBO.Provider.Create(ref tblI);
                }
                else
                {
                    if(objOwnerEditVM.chkChangeLEToOW == true)
                    {
                        myentity.RefNum = "";
                        myentity.TypeCode = "OW";

                        LogLenderChanges(LenderId, myentity, (objOwnerEditVM.objJobEdit.LenderId == 0) ? true : false,false,true);

                    }
                    else
                    {
                        LogLenderChanges(LenderId, myentity, (objOwnerEditVM.objJobEdit.LenderId == 0) ? true : false);

                    }
                     tblI = myentity;
                    DBO.Provider.Update(ref tblI);
                }
                if(objOwnerEditVM.IsPrimary == true)
                {
                    
                    objOwnerEditVM.objJobEdit = ADJob.ReadJob(Convert.ToInt32(objOwnerEditVM.JobId));
                    AutoMapper.Mapper.CreateMap<ADJob, Job>();
                    Job myjob;
                    myjob = AutoMapper.Mapper.Map<Job>(objOwnerEditVM.objJobEdit);
                    myjob.SuretyBox = objOwnerEditVM.chkSuretyBox;
                    myjob.LenderId = myentity.Id;
                    ITable tblJob = myjob;
                    DBO.Provider.Update(ref tblJob);

                }
                


                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static void LogLenderChanges(int LegalPartyId, JobLegalParties myentity, bool IsCreate, [Optional] bool IsDelete, [Optional] bool IsChangeToOW)
        {
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

            OwnerEditVM objOwnerEditVM = new OwnerEditVM();
            ADJobLegalParties objADJobLegalParties = ADJobLegalParties.ReadJobLegalParties(LegalPartyId);
            var mysproc = new CRF.BLL.CRFDB.SPROCS.uspbo_Jobs_UpdateChangeLog();

            mysproc.JobId = myentity.JobId;
            mysproc.UserId = user.Id;
            mysproc.UserName = user.UserName;
            mysproc.ChangeType = 3;
            mysproc.SecondaryId = objADJobLegalParties.Id;

           

            if (myentity.Id > 0)
            {

                mysproc.ChangeFrom = objADJobLegalParties.AddressName.Trim() + "~";
                mysproc.ChangeFrom += objADJobLegalParties.AddressLine1.Trim() + "~";
                mysproc.ChangeFrom += objADJobLegalParties.AddressLine2.Trim() + "~";
                mysproc.ChangeFrom += objADJobLegalParties.City.Trim() + "~";
                mysproc.ChangeFrom += objADJobLegalParties.State.Trim() + "~";
                mysproc.ChangeFrom += objADJobLegalParties.PostalCode.Trim() + "~";


            }

            if (IsDelete == true)
            {
                mysproc.ChangeTo = "DELETED";
            }
            else if (IsChangeToOW == true)
            {
                mysproc.ChangeTo = "Changed to Owner";
            }
           
            else
            {

                mysproc.ChangeTo = myentity.AddressName.Trim() + "~";
                mysproc.ChangeTo += myentity.AddressLine1.Trim() + "~";
                mysproc.ChangeTo += myentity.AddressLine2.Trim() + "~";
                mysproc.ChangeTo += myentity.City.Trim() + "~";
                mysproc.ChangeTo += myentity.State.Trim() + "~";
                mysproc.ChangeTo += myentity.PostalCode.Trim() + "~";

            }

            if (mysproc.ChangeFrom != mysproc.ChangeTo)
            {
                DBO.Provider.ExecNonQuery(mysproc);
            }

        }

        public static int DeleteLender(OwnerEditVM objOwnerEditVM, int LenderId)
        {
            try
            {
                bool value = false;
                objOwnerEditVM.objADJobLegalParties = ADJobLegalParties.ReadJobLegalParties(LenderId);

                AutoMapper.Mapper.CreateMap<ADJobLegalParties, JobLegalParties>();
                JobLegalParties myentity;
                myentity = AutoMapper.Mapper.Map<JobLegalParties>(objOwnerEditVM.objADJobLegalParties);

                ITable ILegalParties = myentity;


                ILegalParties = myentity;



                LogLenderChanges(LenderId, myentity, (LenderId == 0) ? true : false,true);
                ILegalParties = myentity;
                DBO.Provider.Delete(ref ILegalParties);
                if (objOwnerEditVM.IsPrimary == true)
                {

                    objOwnerEditVM.objJobEdit = ADJob.ReadJob(Convert.ToInt32(objOwnerEditVM.JobId));
                    AutoMapper.Mapper.CreateMap<ADJob, Job>();
                    Job myjob;

                    myjob = AutoMapper.Mapper.Map<Job>(objOwnerEditVM.objJobEdit);
                    myjob.LenderId = 0;
                    ITable IJob = myjob;
                    DBO.Provider.Update(ref IJob);
                }

                var mysproc = new CRF.BLL.CRFDB.SPROCS.uspbo_Jobs_UpdatePrimaryLegalParty();
                mysproc.JobId = objOwnerEditVM.objJobEdit.Id;
                DBO.Provider.ExecNonQuery(mysproc);

                return 1;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static int SaveDesigee(OwnerEditVM objOwnerEditVM, int NewDesigneeID,int DesigneeID,bool IsPrimary)
        {
           
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                JobLegalParties myentity = new JobLegalParties();
                ADJobLegalParties objADJobLegalParties = new ADJobLegalParties();
                //ADClientGeneralContractor objADClientGeneralContractor = ReadClientGeneralContractor(CustId);
               // AutoMapper.Mapper.CreateMap<ADJobLegalParties, JobLegalParties>();
                //  JobLegalParties myentity;
               // myentity = AutoMapper.Mapper.Map<JobLegalParties>(objADJobLegalParties);

                objOwnerEditVM.objJobEdit = ADJob.ReadJob(Convert.ToInt32(objOwnerEditVM.JobId));


                if (NewDesigneeID == 0)
                {
                    myentity.Id = DesigneeID;
                }
                else
                {
                    myentity.Id = NewDesigneeID;
                }

                myentity.JobId = Convert.ToInt32(objOwnerEditVM.JobId);
                myentity.AddressName = objOwnerEditVM.objADJobLegalParties.AddressName;
                myentity.Telephone1 = objOwnerEditVM.objADJobLegalParties.Telephone1;
                myentity.Fax = objOwnerEditVM.objADJobLegalParties.Fax;
                myentity.AddressLine1 = objOwnerEditVM.objADJobLegalParties.AddressLine1;
                myentity.AddressLine2 = objOwnerEditVM.objADJobLegalParties.AddressLine2;
                myentity.City = objOwnerEditVM.objADJobLegalParties.City;
                myentity.State = objOwnerEditVM.objADJobLegalParties.State;
                myentity.PostalCode = objOwnerEditVM.objADJobLegalParties.PostalCode;
                myentity.IsNonUSAddr = objOwnerEditVM.objADJobLegalParties.IsNonUSAddr;
                myentity.IsPrimary = IsPrimary;
                myentity.TypeCode = "DE";
                objOwnerEditVM.objADJobLegalParties = ReadJobLegalParties(Convert.ToInt32(objOwnerEditVM.JobId));
          
                if(DesigneeID == 0 && objOwnerEditVM.objJobEdit.DesigneeId > 0)
                {
                    myentity.IsPrimary = false;
                    IsPrimary = false;
                }
                if (objOwnerEditVM.objJobEdit.DesigneeId < 1)
                {
                    myentity.IsPrimary = true;
                    IsPrimary = true;
                }

          //New primary/secondary Designee

                if(DesigneeID < 1)
                {
                    LogDesigeeChanges(DesigneeID,myentity, (objOwnerEditVM.objJobEdit.DesigneeId == 0) ? true : false);
                    ITable tblI = myentity;
                    DBO.Provider.Create(ref tblI);
                }
                else
                {
                    LogDesigeeChanges(DesigneeID, myentity, (objOwnerEditVM.objJobEdit.DesigneeId == 0) ? true : false);
                    ITable tblI = myentity;
                    DBO.Provider.Update(ref tblI);
                }

                if(IsPrimary == true)
                {
                    objOwnerEditVM.objJobEdit = ADJob.ReadJob(Convert.ToInt32(objOwnerEditVM.JobId));
                    objOwnerEditVM.objJobEdit.DesigneeId = myentity.Id;
                    Job Jobmyentity;
                    Jobmyentity = AutoMapper.Mapper.Map<Job>(objOwnerEditVM.objJobEdit);
                    ITable tblJob = myentity;
                    DBO.Provider.Update(ref tblJob);
                }


                return 1;


            }

            catch (Exception ex)
            {
                return 0;
            }
        }

        public static void LogDesigeeChanges(int LegalPartyId, JobLegalParties myentity, bool IsCreate, [Optional] bool IsDelete)
        {
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

            OwnerEditVM objOwnerEditVM = new OwnerEditVM();
            ADJobLegalParties objADJobLegalParties = ADJobLegalParties.ReadJobLegalParties(LegalPartyId);
            var mysproc = new CRF.BLL.CRFDB.SPROCS.uspbo_Jobs_UpdateChangeLog();

            mysproc.JobId = myentity.JobId;
            mysproc.UserId = user.Id;
            mysproc.UserName = user.UserName;
            mysproc.ChangeType = 5;
            mysproc.SecondaryId = objADJobLegalParties.Id;



            if (myentity.Id > 0)
            {

                mysproc.ChangeFrom = objADJobLegalParties.AddressName.Trim() + "~";
                mysproc.ChangeFrom += objADJobLegalParties.AddressLine1.Trim() + "~";
                mysproc.ChangeFrom += objADJobLegalParties.AddressLine2.Trim() + "~";
                mysproc.ChangeFrom += objADJobLegalParties.City.Trim() + "~";
                mysproc.ChangeFrom += objADJobLegalParties.State.Trim() + "~";
                mysproc.ChangeFrom += objADJobLegalParties.PostalCode.Trim() + "~";


            }

            if (IsDelete == true)
            {
                mysproc.ChangeTo = "DELETED";
            }

            else
            {

                mysproc.ChangeTo = myentity.AddressName.Trim() + "~";
                mysproc.ChangeTo += myentity.AddressLine1.Trim() + "~";
                mysproc.ChangeTo += myentity.AddressLine2.Trim() + "~";
                mysproc.ChangeTo += myentity.City.Trim() + "~";
                mysproc.ChangeTo += myentity.State.Trim() + "~";
                mysproc.ChangeTo += myentity.PostalCode.Trim() + "~";

            }

            if (mysproc.ChangeFrom != mysproc.ChangeTo)
            {
                DBO.Provider.ExecNonQuery(mysproc);
            }

        }

        public static int DeleteDesigee(OwnerEditVM objOwnerEditVM, int DesigneeID)
        {
            try
            {
                bool value = false;
                objOwnerEditVM.objJobEdit = ADJob.ReadJob(Convert.ToInt32(objOwnerEditVM.JobId));
               // ADJobLegalParties myentity = new ADJobLegalParties();
               // myentity = (ADJobLegalParties)AutoMapper.Mapper.CreateMap<JobLegalParties, ADJobLegalParties>();
               // var myLegalParties = new JobLegalParties();
                objOwnerEditVM.objADJobLegalParties = ReadJobLegalParties(DesigneeID);
                JobLegalParties myentity = new JobLegalParties();
                ADJobLegalParties objADJobLegalParties = new ADJobLegalParties();
               
                ITable ILegalParties = (ITable)myentity;


                // ILegalParties = myLegalParties;
                myentity.Id = objOwnerEditVM.objADJobLegalParties.Id;
                myentity.JobId = Convert.ToInt32(objOwnerEditVM.JobId);
                myentity.AddressName = objOwnerEditVM.objADJobLegalParties.AddressName;
                myentity.Telephone1 = objOwnerEditVM.objADJobLegalParties.Telephone1;
                myentity.Fax = objOwnerEditVM.objADJobLegalParties.Fax;
                myentity.AddressLine1 = objOwnerEditVM.objADJobLegalParties.AddressLine1;
                myentity.AddressLine2 = objOwnerEditVM.objADJobLegalParties.AddressLine2;
                myentity.City = objOwnerEditVM.objADJobLegalParties.City;
                myentity.State = objOwnerEditVM.objADJobLegalParties.State;
                myentity.PostalCode = objOwnerEditVM.objADJobLegalParties.PostalCode;
                myentity.IsNonUSAddr = objOwnerEditVM.objADJobLegalParties.IsNonUSAddr;
               // myentity.IsPrimary = IsPrimary;
                myentity.TypeCode = "DE";


                LogDesigeeChanges(DesigneeID, myentity, (objOwnerEditVM.objJobEdit.DesigneeId == 0) ? true : false,true);
                ILegalParties = myentity;
                DBO.Provider.Delete(ref ILegalParties);
                if (objOwnerEditVM.IsPrimaryDesignee == true)
                {
                    var myacc = new Job();

                    ITable IJob = myacc;
                    DBO.Provider.Read(objOwnerEditVM.JobId, ref IJob);

                    myacc.SameAsGC = false;
                    myacc.OwnrId = 0;
                    IJob = myacc;
                    DBO.Provider.Update(ref IJob);
                }

                var mysproc = new CRF.BLL.CRFDB.SPROCS.uspbo_Jobs_UpdatePrimaryLegalParty();
                mysproc.JobId = objOwnerEditVM.objJobEdit.Id;
                DBO.Provider.ExecNonQuery(mysproc);

                return 1;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        #endregion
    }

}
