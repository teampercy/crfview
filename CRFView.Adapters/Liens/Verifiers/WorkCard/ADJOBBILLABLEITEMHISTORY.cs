﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard
{
    public class ADJOBBILLABLEITEMHISTORY
    {

        #region Properties

        public int JobBillableItemHistoryId { get; set; }
        public int JobId { get; set; }
        public int JobNoticeRequestId { get; set; }
        public int JobOtherNoticeInternalId { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsProcessed { get; set; }
        public DateTime DateProcessed { get; set; }
        public int ClientFinDocId { get; set; }
        public int EventTypeId { get; set; }
        public int Qty { get; set; }
        public Decimal Price { get; set; }
        public int JobActionHistoryId { get; set; }
        public int BatchId { get; set; }
        public int ContractId { get; set; }
        public string SubBreak { get; set; }
        public int PriceCodeId { get; set; }
        public int MajorEventTypeId { get; set; }
        public int ParentEventTypeId { get; set; }
        public string Description { get; set; }
        public Decimal OtherFees { get; set; }
        public string AdditonalItems { get; set; }
        public bool IsActioned { get; set; }
        public int ActionId { get; set; }
        public int JobNoticeHistoryId { get; set; }

        #endregion


        #region CRUD

        public static List<ADJOBBILLABLEITEMHISTORY> GetJOBBILLABLEITEMHISTORYList(DataTable dt)
        {
            try
            {
                if (dt.Rows.Count > 0)
                {

                    List<ADJOBBILLABLEITEMHISTORY> ActionHistory = (from DataRow dr in dt.Rows
                                                                select new ADJOBBILLABLEITEMHISTORY()
                                                                {
                                                                    DateCreated = ((dr["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dr["DateCreated"]) : DateTime.MinValue)),
                                                                    Description = ((dr["Description"] != DBNull.Value ? dr["Description"].ToString() : "")),
                                                                    Qty = ((dr["Qty"] != DBNull.Value ? Convert.ToInt32(dr["Qty"].ToString()) : 0)),
                                                                    Price = ((dr["Price"] != DBNull.Value ? Convert.ToDecimal(dr["Price"]) : 0)),
                                                                    DateProcessed = ((dr["DateProcessed"] != DBNull.Value ? Convert.ToDateTime(dr["DateProcessed"]) : DateTime.MinValue)),
                                                                    ClientFinDocId = ((dr["ClientFinDocId"] != DBNull.Value ? Convert.ToInt32(dr["ClientFinDocId"].ToString()) : 0)),

                                                                }).ToList();
                    return ActionHistory;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return (new List<ADJOBBILLABLEITEMHISTORY>());
            }
        }


        #endregion




    }
}
