﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard
{
   public class ADJobVerifiedSentRequest
    {
        #region Properties

        public int Id { get; set; }
        public int JobId { get; set; }
        public string SubmittedBy { get; set; }
        public int SubmittedByUserId { get; set; }
        public DateTime DateCreated { get; set; }
        public string AmountOwed { get; set; }
        public string MonthDebtIncurred { get; set; }
        public string StmtOfAccts { get; set; }



        #region Custom Property
        public bool IsMnthlyReqDelete { get; set; }
        #endregion

        #endregion

        #region CRUD

        public static ADJobVerifiedSentRequest ReadJobVerifiedSentRequest(int id)
        {
            JobVerifiedSentRequest myentity = new JobVerifiedSentRequest();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (JobVerifiedSentRequest)myentityItable;
            AutoMapper.Mapper.CreateMap<JobVerifiedSentRequest, ADJobVerifiedSentRequest>();
            return (AutoMapper.Mapper.Map<ADJobVerifiedSentRequest>(myentity));
        }
        public static List<ADJobVerifiedSentRequest> GetJobVerifiedSentRequest(DataTable dt)
        {
            try
            {
                if (dt.Rows.Count > 0)
                {

                    List<ADJobVerifiedSentRequest> SentRequest = (from DataRow dr in dt.Rows
                                                            select new ADJobVerifiedSentRequest()
                                                            {
                                                                Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                                                SubmittedBy = ((dr["SubmittedBy"] != DBNull.Value ? dr["SubmittedBy"].ToString() : "")),
                                                                DateCreated = ((dr["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dr["DateCreated"]) : DateTime.MinValue)),
                                                                AmountOwed = ((dr["AmountOwed"] != DBNull.Value ? dr["AmountOwed"].ToString() : "")),
                                                                MonthDebtIncurred = ((dr["MonthDebtIncurred"] != DBNull.Value ? dr["MonthDebtIncurred"].ToString() : "")),
                                                                
                                                            }).ToList();
                    return SentRequest;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return (new List<ADJobVerifiedSentRequest>());
            }
        }

        public static int SaveDetails(ADJobVerifiedSentRequest objADJobVerifiedSentRequest)
        {
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            ITable tblI;

            ADJobVerifiedSentRequest OBJADJobVerifiedSentRequest = new ADJobVerifiedSentRequest();
            OBJADJobVerifiedSentRequest = ADJobVerifiedSentRequest.ReadJobVerifiedSentRequest(Convert.ToInt32(objADJobVerifiedSentRequest.Id));

            //ADClientGeneralContractor objADClientGeneralContractor = ReadClientGeneralContractor(CustId);
            AutoMapper.Mapper.CreateMap<ADJobVerifiedSentRequest, JobVerifiedSentRequest>();
            JobVerifiedSentRequest myentity;
            myentity = AutoMapper.Mapper.Map<JobVerifiedSentRequest>(OBJADJobVerifiedSentRequest);

            myentity.JobId = objADJobVerifiedSentRequest.JobId;
            string r = objADJobVerifiedSentRequest.AmountOwed;
            myentity.AmountOwed = Convert.ToDecimal(r.Replace(@"$", ""));
            //myentity.AmountOwed = Convert.ToDecimal(objADJobVerifiedSentRequest.AmountOwed);
            myentity.MonthDebtIncurred = objADJobVerifiedSentRequest.MonthDebtIncurred;
            myentity.StmtOfAccts = objADJobVerifiedSentRequest.StmtOfAccts;


            //myentity.DateCreated = objADJobVerifiedSentRequest.DateCreated;
            //myentity.SubmittedBy = objADJobVerifiedSentRequest.SubmittedBy;
            //myentity.SubmittedByUserId = objADJobVerifiedSentRequest.SubmittedByUserId;


            if (objADJobVerifiedSentRequest.Id < 1)
            {
                myentity.DateCreated = DateTime.Now;
                myentity.SubmittedBy = user.UserName;
                myentity.SubmittedByUserId = user.Id;
                tblI = myentity;

                DBO.Provider.Create(ref tblI);

            }
            else
            {
                myentity.Id = objADJobVerifiedSentRequest.Id;
                tblI = myentity;
                DBO.Provider.Update(ref tblI);
            }

            return 1;
        }

        public static bool DeleteMonthlyRequestDetails(int Id, int JobId)
        {
            try
            {
                ITable tblJobVerifiedSentRequest;
                JobVerifiedSentRequest objJobVerifiedSentRequest = new JobVerifiedSentRequest();
                objJobVerifiedSentRequest.Id = Convert.ToInt32(Id);
                tblJobVerifiedSentRequest = objJobVerifiedSentRequest;
                DBO.Provider.Delete(ref tblJobVerifiedSentRequest);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion
    }
}
