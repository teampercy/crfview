﻿using CRF.BLL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard
{
    public class ADJobRental
    {
        #region Properties

        public int Id { get; set; }
        public int JobId { get; set; }
        public int BatchId { get; set; }
        public string RANum { get; set; }
        public string BranchNum { get; set; }
        public string JobNum { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public string ContractType { get; set; }
        public string ClientStatus { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateUpdated { get; set; }
        public int SubmittedByUserId { get; set; }
        public string SubmittedBy { get; set; }
        public string ChangeType { get; set; }
        public int BatchJobRentalId { get; set; }
        public Decimal OriginalBalance { get; set; }
        public Decimal CurrentBalance { get; set; }
        public Decimal EstBalance { get; set; }
        public string PONum { get; set; }
        public string EquipOrderedBy { get; set; }
        public string CustRefNum { get; set; }

        // Custom Property

        public string EquipDescription { get; set; }

        #endregion

        #region CRUD



        public static DataTable GetJobRentalDt(string JobId)
        {
            try
            {
                string MYSQL = "";

                MYSQL = "Select jr.RANum, jr.PONum, EquipDescription, jd.EquipOrderedby from Job j (nolock) join JobRental jr (nolock) on j.Id = jr.JobId join JobRentalDesc jd on jr.Id = jd.JobRentalId";

                MYSQL += " Where j.Id = " + JobId + " Order By jr.RANum";

                var CBRView = DBO.Provider.GetTableView(MYSQL);
                DataTable dtJobRental = CBRView.ToTable();
                
                return (dtJobRental);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }


        public static List<ADJobRental> GetJobRentalList(DataTable dt)
        {
            try
            {
                if (dt.Rows.Count > 0)
                {

                    List<ADJobRental> JobRental = (from DataRow dr in dt.Rows
                                                                select new ADJobRental()
                                                                {

                                                                    RANum = ((dr["RANum"] != DBNull.Value ? dr["RANum"].ToString() : "")),
                                                                    PONum = ((dr["PONum"] != DBNull.Value ? dr["PONum"].ToString() : "")),
                                                                    EquipDescription = ((dr["EquipDescription"] != DBNull.Value ? dr["EquipDescription"].ToString() : "")),
                                                                    EquipOrderedBy = ((dr["EquipOrderedBy"] != DBNull.Value ? dr["EquipOrderedBy"].ToString() : "")),

                                                                }).ToList();
                    return JobRental;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return (new List<ADJobRental>());
            }
        }


        #endregion
    }
}
