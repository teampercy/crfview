﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels;
using HDS.DAL.Providers;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard
{
    public class ADJobHistory
    {

        #region Properties

        public int Id { get; set; }
        public int JobId { get; set; }
        public string UserId { get; set; }
        public int EventTypeId { get; set; }
        public DateTime DateCreated { get; set; }
        public string Subject { get; set; }
        public string Note { get; set; }
        public bool ClientView { get; set; }
        public int ActionTypeId { get; set; }
        public DateTime CloseDate { get; set; }
        public int EnteredByUserId { get; set; }
        public int ClosedByUserId { get; set; }
        public bool SQLBox { get; set; }
        public int AltKeyId { get; set; }
        public bool IsConverted { get; set; }
        public int NoteTypeId { get; set; }

        #endregion


        #region CRUD

        public static ADJobHistory ReadNote(int id)
        {
            JobHistory myentity = new JobHistory();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (JobHistory)myentityItable;
            AutoMapper.Mapper.CreateMap<JobHistory, ADJobHistory>();
            return (AutoMapper.Mapper.Map<ADJobHistory>(myentity));
        }


        //public static DataTable NoteList(JobVM ObjJobVM, string RadioButtonValue)
        //{
        //    try
        //    {
        //        DataView dv = new DataView(ObjJobVM.dtNotes);
        //        //ObjJobVM.dtNotes.DefaultView.RowFilter = "NoteTypeId=1";
        //        // HDS.DAL.COMMON.TableView s = DBO.Provider.GetTableView((IDBO)ObjJobVM.dtNotes);
        //        if (RadioButtonValue == "Misc")
        //        {
                    
        //            dv.RowFilter = "NoteTypeId=1";
        //            //ObjJobVM.dtNotes.DefaultView.RowFilter = "NoteTypeId=1";
                 
        //        }
        //        else if(RadioButtonValue == "Internal")
        //        {
        //            dv.RowFilter = "NoteTypeId=2";
        //        }
        //        else if (RadioButtonValue == "Review")
        //        {
        //            dv.RowFilter =  "NoteTypeId=3";
        //        }
        //        else
        //        {
        //            dv.RowFilter = "";
        //        }

        //        DataTable dt = dv.ToTable();
        //        //ObjJobVM.dtNotes = s.ToTable();
        //        ADJobHistory objUpdatedADJobHistory = new ADJobHistory();
        //        //ObjJobVM.ObjNotes = ADvwLienNotes.GetNotes(dt);
        //         return dt;
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLog.SendErrorToText(ex);
        //        return ObjJobVM.dtNotes;
        //    }
            
        //}
        public static int SaveADJobHistoryNote(ADJobHistory objUpdatedADJobHistory)
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                AutoMapper.Mapper.CreateMap<ADJobHistory, JobHistory>();
                ITable tblDebtAccountNote;
                JobHistory objDebtAccountNote;
                ADJobHistory objADJobHistoryOriginal = ReadNote(objUpdatedADJobHistory.Id);

                if (objUpdatedADJobHistory.NoteTypeId == 1)
                    {
                        objADJobHistoryOriginal.NoteTypeId = 1;
                    }
                    else if (objUpdatedADJobHistory.NoteTypeId == 2)
                    {
                        objADJobHistoryOriginal.NoteTypeId = 2;
                    }
                    else
                    {
                        objADJobHistoryOriginal.NoteTypeId = 3;
                    }
                if (objUpdatedADJobHistory.Id != 0)
                {

                    objADJobHistoryOriginal.Note = objUpdatedADJobHistory.Note;
                    //ADJobHistory objADJobHistoryOriginal = ReadNote(objUpdatedADJobHistory.Id);

                    //if (objUpdatedADJobHistory.Note.ToString() == "" || objUpdatedADJobHistory.Note == null)
                    //{
                    //}
                    //else
                    //{
                    //    objADJobHistoryOriginal.Note = objUpdatedADJobHistory.Note;
                    //}

                    if (objUpdatedADJobHistory.NoteTypeId.ToString() == "" || objUpdatedADJobHistory.NoteTypeId == 0)
                    {
                    }
                    else
                    {
                        objADJobHistoryOriginal.NoteTypeId = objUpdatedADJobHistory.NoteTypeId;
                    }

                  
                    objADJobHistoryOriginal.DateCreated = DateTime.Now;
                    //CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                    objADJobHistoryOriginal.UserId = user.UserName;
                    objADJobHistoryOriginal.EnteredByUserId = user.Id;

                    objDebtAccountNote = AutoMapper.Mapper.Map<JobHistory>(objADJobHistoryOriginal);
                    tblDebtAccountNote = (ITable)objDebtAccountNote;
                    DBO.Provider.Update(ref tblDebtAccountNote);
                }
                else
                {
                    objUpdatedADJobHistory.DateCreated = DateTime.Now;

                    //CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                    objUpdatedADJobHistory.UserId = user.UserName;
                    objUpdatedADJobHistory.EnteredByUserId = user.Id;

                    objDebtAccountNote = AutoMapper.Mapper.Map<JobHistory>(objUpdatedADJobHistory);
                    tblDebtAccountNote = (ITable)objDebtAccountNote;
                    DBO.Provider.Create(ref tblDebtAccountNote);
                }

                string s = "Review Note on: JobId " + objUpdatedADJobHistory.JobId;
                Collection myc = new Collection();
                Collection mya = new Collection();
                

                //if (objUpdatedADJobHistory.NoteTypeId == 1)
                //{
                    //CRF.BLL.CRFView.CRFView.SendOutLookEmail(user.Email, user.UserName, s, objADJobHistoryOriginal.Note, myc, mya, true);
               // }

                ADJob objJobEdit = new ADJob();
                 AutoMapper.Mapper.CreateMap<ADJob, Job>();
                Job myjob;
               
               int t = objUpdatedADJobHistory.JobId;
                objJobEdit = ADJob.ReadJob(Convert.ToInt32(objUpdatedADJobHistory.JobId));
                //myjob.Id = t;
                objJobEdit.IsReviewNote = true;
                objJobEdit.LastUpdatedById = user.Id;
                objJobEdit.LastUpdatedBy = user.UserName;
                objJobEdit.LastUpdatedOn = DateAndTime.Now;
                myjob = AutoMapper.Mapper.Map<Job>(objJobEdit);
             
                ITable tblJob = (ITable)myjob;
                DBO.Provider.Update(ref tblJob);

                    return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }


        public static bool DeleteJobHistoryNote(string JobId)
        {
            try
            {
                ITable IReport;
                JobHistory objJobHistory = new JobHistory();
                objJobHistory.Id = Convert.ToInt32(JobId);
                IReport = objJobHistory;
                DBO.Provider.Delete(ref IReport);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public static int SaveADJobHistoryNoteForSentEmail(ADJobHistory objUpdatedADJobHistory)
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                AutoMapper.Mapper.CreateMap<ADJobHistory, JobHistory>();
                ITable tblDebtAccountNote;
                JobHistory objDebtAccountNote;
                ADJobHistory objADJobHistoryOriginal = ReadNote(objUpdatedADJobHistory.Id);

                if (objUpdatedADJobHistory.NoteTypeId == 1)
                {
                    objADJobHistoryOriginal.NoteTypeId = 1;
                }
                else if (objUpdatedADJobHistory.NoteTypeId == 2)
                {
                    objADJobHistoryOriginal.NoteTypeId = 2;
                }
                else
                {
                    objADJobHistoryOriginal.NoteTypeId = 3;
                }
                
                    objUpdatedADJobHistory.DateCreated = DateTime.Now;

                    //CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                    objUpdatedADJobHistory.UserId = user.UserName;
                    objUpdatedADJobHistory.EnteredByUserId = user.Id;

                    objDebtAccountNote = AutoMapper.Mapper.Map<JobHistory>(objUpdatedADJobHistory);
                    tblDebtAccountNote = (ITable)objDebtAccountNote;
                    DBO.Provider.Create(ref tblDebtAccountNote);
              

                //string s = "Review Note on: JobId " + objUpdatedADJobHistory.JobId;
                //Collection myc = new Collection();
                //Collection mya = new Collection();


                ////if (objUpdatedADJobHistory.NoteTypeId == 1)
                ////{
                ////CRF.BLL.CRFView.CRFView.SendOutLookEmail(user.Email, user.UserName, s, objADJobHistoryOriginal.Note, myc, mya, true);
                //// }

                //ADJob objJobEdit = new ADJob();
                //AutoMapper.Mapper.CreateMap<ADJob, Job>();
                //Job myjob;

                //int t = objUpdatedADJobHistory.JobId;
                //objJobEdit = ADJob.ReadJob(Convert.ToInt32(objUpdatedADJobHistory.JobId));
                ////myjob.Id = t;
                //objJobEdit.IsReviewNote = true;
                //objJobEdit.LastUpdatedById = user.Id;
                //objJobEdit.LastUpdatedBy = user.UserName;
                //objJobEdit.LastUpdatedOn = DateAndTime.Now;
                //myjob = AutoMapper.Mapper.Map<Job>(objJobEdit);

                //ITable tblJob = (ITable)myjob;
                //DBO.Provider.Update(ref tblJob);

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }


        #endregion


    }
}
