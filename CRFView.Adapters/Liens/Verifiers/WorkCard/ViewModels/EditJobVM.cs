﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels
{
    public class EditJobVM
    {
        public EditJobVM()
        {
            CountyRecorderList = CommonBindList.CountyRecorderNumList();
            JobDeskList = CommonBindList.GetLiensNewDeskIdList();
        }
        public IEnumerable<SelectListItem> CountyRecorderList { get; set; }
        public IEnumerable<SelectListItem> JobDeskList { get; set; }
        public ADJob obj_JobEdit { get; set; }
        public ADClient obj_ClientEdit { get; set; }
        public ADvwJobsList ObjvwJobList { get; set; }
        public string DeskNum { get; set; }
        public string Editrbt { get; set; }
        public string RbtPropertyType { get; set; }
    }
}
