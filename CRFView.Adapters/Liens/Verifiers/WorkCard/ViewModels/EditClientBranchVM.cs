﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels
{
    public class EditClientBranchVM
    {

        public EditClientBranchVM()
        {
           // ClientBranchList = CommonBindList.GetBranchlist();
           //ClientBranchList = 
        }

        public string JobId { get; set; }
        public IEnumerable<SelectListItem> ClientBranchList { get; set; }
        public List<ADClient> objClientList { get; set; }
        public ADClient obj_ClientEdit { get; set; }
        public ADvwJobsList objvwJobsList { get; set; }
        public bool IsJobView { get; set; }

        public string ClientId { get; set; }
    }
}
