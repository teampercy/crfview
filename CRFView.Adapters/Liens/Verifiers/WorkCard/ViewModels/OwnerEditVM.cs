﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels
{
   public class OwnerEditVM
    {
        public OwnerEditVM()
        {
            IsPrimary = true;
            OWList = new List<ADJobLegalParties>();
            LenderList = new List<ADJobLegalParties>();
            DesigneeList = new List<ADJobLegalParties>();
        }

        public ADJobLegalParties objADJobLegalParties { get; set; }
        public ADJob objJobEdit { get; set; }
        public ADStateInfo objADStateInfo { get; set; }
        public string cboName { get; set; }
        public string RdbBtn { get; set; }
        public bool btnDelete { get; set; }
        public bool btnSelect { get; set; }
        public bool chkChangeToPrimary { get; set; }
        public bool chkCopyJobAddr { get; set; }
        public bool chkChangeOWToLE { get; set; }
        public bool chkMakeSameAsGc { get; set; }
        public bool chkChangeOWToDE { get; set; }
        public bool MLAgent { get; set; }
        public bool IschkChangeToPrimary { get; set; }
        public bool IschkCopyJobAddr { get; set; }
        public bool IschkChangeOWToLE { get; set; }
        public bool IschkMakeSameAsGc { get; set; }
        public bool chkChangeLEToOW { get; set; }
        public bool chkSuretyBox { get; set; }
        public bool IschkSuretyBox { get; set; }
        public bool IsMLAgent { get; set; }
        public string ClientId { get; set; }
        public string JobId { get; set; }
        public int OwnrId { get; set; }
       public bool IsPrimary { get; set; }
        public bool IsPrimaryDesignee { get; set; }
        public bool IsValidStateZip { get; set; }
        public bool ValidCityZip { get; set; }
        public DataTable dtOWList { get; set; }
        public DataTable dtLenderList { get; set; }
        public DataTable dtDesigneeList { get; set; }
        public List<ADJobLegalParties> OWList { get; set; }
        public List<ADJobLegalParties> LenderList { get; set; }
        public List<ADJobLegalParties> DesigneeList { get; set; }
        public ADClientGeneralContractor objADClientGeneralContractor { get; set; }
        public ADJob obj_JobEdit { get; set; }
    }
}
