﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels
{
    public class ReceiptientGridVM
    {
        public string keyvalue { get; set; }
        public string To { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
    }
}
