﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels
{
    public class CustomerEditVM
    {
        public  CustomerEditVM()
        {
            CustomerList = new List<ADClientCustomer>();
            IsPrimary = true;
        }

        public string ClientId { get; set; }
        public bool IsJobView { get; set; }
        public string JobId { get; set; }
        public bool IsPrimary { get; set; }
        public bool SameAsCust { get; set; }
        public bool DeleteButton { get; set; }
        public ADJob obj_JobEdit { get; set; }

        public ADJobLegalParties obj_ADJobLegalParties { get; set; }
        public string cboCustName { get; set; }
        public bool IsValidStateZip { get; set; }
        public bool ValidCityZip { get; set; }
        public string RdbBtn { get; set; }
        public bool SameAsGC { get; set; }
        public ADClientCustomer obj_ClientCustomer { get; set; }

        public DataTable dtCustomeList { get; set; }
        public List<ADClientCustomer> CustomerList { get; set; }
    }
}
