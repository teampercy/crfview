﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels
{
    public class FilterJobsVM
    {
        public FilterJobsVM()
        {
            LiensStatusGroupList = CommonBindList.GetLiensStatusGroup();
            LiensStatusGroupPropertyTypeList = CommonBindList.GetPropertyType();
        }

        public Filter FilterObj { get; set; }
        public IEnumerable<SelectListItem> LiensStatusGroupList { get; set; }
        public IEnumerable<SelectListItem> LiensStatusGroupPropertyTypeList { get; set; }
    }
}
