﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels
{
    public class MailReturnVM
    {
        public  MailReturnVM()
        {
            //ResearchList = CommonBindList.GetResearchType();
        }

        public string RbtOption { get; set; }
        public string ResearchId { get; set; }
        public bool optReturned { get; set; }
        public bool optRefused { get; set; }
        public bool optForwarded { get; set; }
        public bool visblBox { get; set; }
        public int JobNoticeHistoryId { get; set; }
        public int JobId { get; set; }
        public string LegalPartyType { get; set; }

        public IEnumerable<SelectListItem> ResearchList { get; set; }
        public ADJobNoticeHistory objADJobNoticeHistory { get; set; }
    }
}
