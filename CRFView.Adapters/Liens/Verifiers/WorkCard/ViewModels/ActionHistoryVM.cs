﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels
{
    public class ActionHistoryVM
    {
        public ActionHistoryVM()
        {
            RbtActionType = "Phone";
            ActionHistoryCodesList = CommonBindList.GetActionHistoryCodes();


        }


        public string RbtActionType { get; set; }
        public string SelectedActionHistoryCodesId { get; set; }
        public string ddlActionHistoryCodeId { get; set; }
        public IEnumerable<SelectListItem> ActionHistoryCodesList { get; set; }
        public string JobId { get; set; }
        public string StatusCodeId { get; set; }

    }
}
