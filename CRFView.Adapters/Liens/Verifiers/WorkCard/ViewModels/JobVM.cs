﻿using CRFView.Adapters.Collections.Collectors.WorkCard;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels
{
    public class JobVM
    {
        public JobVM()
        {
            GetJobDeskList = CommonBindList.GetJobDesks();
            GetLegalPartiesList = CommonBindList.GetLegalParties();
        }
        #region Properties

        public string RbtNoteType { get; set; }
        public bool chkNewDsk { get; set; }
        public bool chkNewRev { get; set; }
        public string NewRevDate { get; set; }
        public IEnumerable<SelectListItem> GetJobDeskList { get; set; }
        public string DeskId { get; set; }
        public IEnumerable<SelectListItem> GetLegalPartiesList { get; set; }
        public string Id { get; set; }
        public ADvwJobsList ObjvwJobList { get; set; }
        public ADJob ObjJobList { get; set; }
        public List<ADvwLienNotes> ObjNotes { get; set; }
        public List<ADvwJOBACTIONHISTORY> ObjJOBACTIONHISTORY { get; set; }
        public List<ADJOBBILLABLEITEMHISTORY> ObjJOBBILLABLEITEMHISTORY { get; set; }

        public List<ADJobTexasRequest> ObjADJobTexasRequest { get; set; }

        public List<ADJobVerifiedSentRequest> ObjADJobVerifiedSentRequest { get; set; }

        public List<ADJobOtherNoticeInternal> ObjJobOtherNoticeInternalList { get; set; }
        public List<ADJobAttachments> ObjDocuments { get; set; }
        public ADClientLienInfo obj_ClientLienInfo { get; set; }
        public ADClient obj_ClientEdit { get; set; }
        public ADClientGeneralContractor objADClientGeneralContractor { get; set; }
        public List<ADJobAlerts> objADJobAlerts { get; set; }
        public List<ADJobAlerts> objJobAlerts { get; set; }
        //public ADClient objClientList { get; set; }
        public IEnumerable<SelectListItem> objADStateFormNotices { get; set; }

        public EditClientBranchVM ObjEditBranchVM { get; set; }
        public string FileName { get; set; }

        public ADClientCustomer obj_ClientCustomer { get; set; }
        public ADStateInfo objADStateInfo { get; set; }
        public string ClientId { get; set; }
        public IEnumerable<SelectListItem> ClientBranchList { get; set; }

        public List<ADJobLegalParties> objLegalPartiesList { get; set; }
        public string Test { get; set; }
        public bool ChkTest { get; set; }

        public DataTable dtNotes { get; set; }
        public DataTable dtNotice { get; set; }
        public DataTable dtJobAlert { get; set; }
        public DataTable dtStateInfo { get; set; }
        public DataTable dtClientCustomer { get; set; }
        public DataTable dtGC { get; set; }
        public DataTable dtJobMatchList { get; set; }
        public DataTable dtJobMatchListNJ { get; set; }
        public DataTable jobdt { get; set; }

        public DataTable dtClientInfo { get; set; }

        public ADJobNoticePrelimRequest objADJobNoticePrelimRequest {get; set ;}
        public ADJobLegalParties objADJobLegalParties { get; set; }
        public ADJobLegalParties objJobLegalParties { get; set; }
        public List<ADJobNoticePrelimRequest> JobNoticePrelimRequestList { get; set; }
        public List<ADvwJobNoticeRequestHistory> vwJobNoticeRequestHistoryList { get; set; }

        #endregion
    }
}
