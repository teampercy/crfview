﻿using CRF.BLL.CRFDB.TABLES;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels
{
    public class ClientVM
    {
        public string To { get; set; }
        public string DebtAccountId { get; set; }

        public string Subject { get; set; }
        public string Recipients { get; set; }
        public bool OtherReceiptient { get; set; }
        public string NewReceiptient { get; set; }
        public string Message { get; set; }
        public int JobNo { get; set; }
        public string JobName { get; set; }
        public string ClientName { get; set; }
        public int JobId { get; set; }
        public string CUSTNAME { get; set; }
        public string PO { get; set; }
        public string ClientCode { get; set; }
        public List<ADJobAttachments> ListaDJobAttachments { get; set; }
        public List<ADDebtAccountAttachment> ListADDebtAccountAttachment { get; set; }
       public List<ReceiptientGridVM> ReceiptientGridList { get; set; }
        public List<ADClientLienInfo> ClientLienInfoList { get; set; }
        public ADClientCollectionInfo ClientCollectionInfoList { get; set; }
        public string AttachmentPath { get; set; }
        public string path { get; set; }


    }
  
}
