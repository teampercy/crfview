﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels
{
   public class NoticeEditVM
    {
        public NoticeEditVM()
        {
        }

        public bool RushBox { get; set; }
        public string RbtNoticeType { get; set; }

        public List<ADJobLegalParties> objLegalPartiesList { get; set; }
        public List<ADJobLegalParties> LegalPartiesList { get; set; }
        public List<ADJobAlerts> objADJobAlerts { get; set; }
        public List<ADJobAlerts> objJobAlertsList { get; set; }
        public ADJobAlerts objJobAlerts { get; set; }
        public ADJob objADJob { get; set; }
        public DataView dv { get; set; }
        public DataView dv1 { get; set; }
        public DataTable dt1 { get; set; }
       public bool ValidationWarnings { get; set; }
        public bool GroupBox1 { get; set; }
        public bool GroupBox2 { get; set; }
        public bool OK_Button { get; set; }
        public bool Panel1 { get; set; }
        public bool btncheck { get; set; }
        public bool btnUncheck { get; set; }
        public bool ListView { get; set; }
        
        public bool CheckAll { get; set; }
        public ADvwJobsList ObjvwJobList { get; set; }
        public bool  IsValidStateZip { get; set; }

        public int Id { get; set; }
        public bool ischeck { get; set; }
        public string JobId { get; set; }
        public string TypeCode { get; set; }
        public string IsPrimary { get; set; }
        public string AddressName { get; set; }

    }
}
