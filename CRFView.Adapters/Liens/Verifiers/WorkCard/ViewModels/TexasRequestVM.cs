﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels
{
    public class TexasRequestVM
    {

        public TexasRequestVM()
        {
            

        }
       
        public ADJobTexasRequest OBJADJobTexasRequest { get; set; }

        public string RdbBtn { get; set; }

        public int Id { get; set; }
        public int JobId { get; set; }
        public bool IsDelete { get; set; }
    }
}
