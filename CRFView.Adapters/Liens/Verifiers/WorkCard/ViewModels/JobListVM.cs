﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels
{
    public class JobListVM
    {
        public JobListVM()
        {
            FilterResult = true;
            WebNoteResult = true;
            JVWebNoteListResult = true;
            TXReviewResult = true;
            populateLists();
            EmailFaxpopulateLists();
        }

       
        #region Properties
        public List<ADvwJobsList> ObjJobList { get; set; }
        public List<ADvwJobsList> ObjWebNotesList { get; set; }

        public List<ADvwJobsList> objGetMonthlyJobListJV { get; set; }
        public string SortType { get; set; }
        public List<SelectListItem> SortTypeList = new List<SelectListItem>();

        public string SortBy { get; set; }
        public List<SelectListItem> SortByList = new List<SelectListItem>();

        public string EmailFaxSortType { get; set; }
        public List<SelectListItem> EmailFaxSortTypeList = new List<SelectListItem>();

        public string EmailFaxSortBy { get; set; }
        public List<SelectListItem> EmailFaxSortByList = new List<SelectListItem>();
        public bool FilterResult { get; set; }
        public bool WebNoteResult { get; set; }
        public bool JVWebNoteListResult { get; set; }
        public bool TXReviewResult { get; set; }

        public string rbtOption { get; set; }

     
        #endregion
        private void populateLists()
        {
            SortByList.Add(new SelectListItem { Text = "CRFS #", Value = "-1" });
            SortByList.Add(new SelectListItem { Text = "Client #", Value = "1" });
            SortByList.Add(new SelectListItem { Text = "Desk #", Value = "2" });
            SortByList.Add(new SelectListItem { Text = "Job Name / Job #", Value = "3" });
            SortByList.Add(new SelectListItem { Text = "Job Address", Value = "4" });
            SortByList.Add(new SelectListItem { Text = "Job Status", Value = "5" });
            SortByList.Add(new SelectListItem { Text = "Customer #", Value = "6" });
            SortByList.Add(new SelectListItem { Text = "Customer Name", Value = "7" });
            SortByList.Add(new SelectListItem { Text = "General Contractor", Value = "8" });
            SortByList.Add(new SelectListItem { Text = "Assigned Date", Value = "9" });
            SortByList.Add(new SelectListItem { Text = "Start Date", Value = "10" });
            SortByList.Add(new SelectListItem { Text = "Review Date", Value = "11" });
            SortByList.Add(new SelectListItem { Text = "Notice Deadline", Value = "12" });
            SortByList.Add(new SelectListItem { Text = "Notice Sent", Value = "13" });
            SortByList.Add(new SelectListItem { Text = "NTO Days", Value = "14" });
            SortByList.Add(new SelectListItem { Text = "Est Balance", Value = "15" });
            SortByList.Add(new SelectListItem { Text = "Job Balance", Value = "16" });
            //SortByList.Add(new SelectListItem { Text = "Property Type", Value = "17" });
            SortByList.Add(new SelectListItem { Text = "JV Status", Value = "18" });
            SortByList.Add(new SelectListItem { Text = "Branch #", Value = "19" });
           // SortByList.Add(new SelectListItem { Text = "Document", Value = "20" });
            


            SortTypeList.Add(new SelectListItem { Text = "Ascending", Value = "-1" });
            SortTypeList.Add(new SelectListItem { Text = "Descending", Value = "1" });
        }

        private void EmailFaxpopulateLists()
        {
            EmailFaxSortByList.Add(new SelectListItem { Text = "Cust Fax", Value = "-1" });
            //SortByList.Add(new SelectListItem { Text = "Sort By", Value = "-1" });
            EmailFaxSortByList.Add(new SelectListItem { Text = "Cust Email", Value = "1" });
            //SortByList.Add(new SelectListItem { Text = "Contact", Value = "2" });
            EmailFaxSortByList.Add(new SelectListItem { Text = "GC Fax", Value = "2" });
            //SortByList.Add(new SelectListItem { Text = "Debtor Ref#", Value = "4" });
            EmailFaxSortByList.Add(new SelectListItem { Text = "GC Email", Value = "3" });
            EmailFaxSortByList.Add(new SelectListItem { Text = "GC PhoNo", Value = "4" });


            EmailFaxSortTypeList.Add(new SelectListItem { Text = "Ascending", Value = "-1" });
            EmailFaxSortTypeList.Add(new SelectListItem { Text = "Descending", Value = "1" });
        }
    }
}
