﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels
{
    public class EditOtherNoticesVM
    {
        public ADJob objJob { get; set; }
        public ADJobLegalParties objJobLegalParties { get; set; }
        public ADClient objADClient { get; set; }
        public string Test { get; set; }
        public string JobId { get; set; }


    }
}
