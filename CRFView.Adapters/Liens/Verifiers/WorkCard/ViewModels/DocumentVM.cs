﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels
{
    public class DocumentVM
    {

        public DocumentVM()
        {
           
            DocumentTypeList = CommonBindList.GetJobDocumentType();
        }

        public string ddlDocumentType { get; set; }
        public IEnumerable<SelectListItem> DocumentTypeList { get; set; }
        public ADJobAttachments ObjJobAttachments { get; set; }
        public ADJobAttachments ObjJobAttachmentsEdit { get; set; }
        public string DocumentType { get; set; }
        public string JobId { get; set; }
        public string Id { get; set; }

    }
}
