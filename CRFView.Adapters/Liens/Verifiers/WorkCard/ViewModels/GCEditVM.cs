﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels
{
    public class GCEditVM
    {
        public GCEditVM()
        {
            ADJob obj_JobEdit = new ADJob();
            GCList = new List<ADClientGeneralContractor>();
            //IsPrimary = true;
        }

        public string cboName {get; set; }
        public ADJob obj_JobEdit { get; set; }
        public ADClientGeneralContractor objADClientGeneralContractor { get; set;}
        public string ClientId { get; set; }
        public string JobId { get; set; }
        public string RdbBtn { get; set; }
        public DataTable dtGCList { get; set; }

        public bool btnDelete { get; set; }
        public bool MakeSameAsCustomerchkbox { get; set; }
        public bool MakeSameAsCustomerchked { get; set; }
        public bool myClientNoCustCall { get; set; }

        public bool SameAsGC { get; set; }
        public bool IsPrimary { get; set; }
        public bool IsValidStateZip { get; set; }
        public bool ValidCityZip { get; set; }
        public bool IsGCUnverified { get; set; }
        public bool IsSameAsCustomerChecked { get; set; }
        public ADClientCustomer obj_ClientCustomer { get; set; }
        public ADJob obj_Job { get; set; }

        public List<ADClientGeneralContractor> GCList { get; set; }
    }
}
