﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels
{
    public class RentalInfoVM
    {
        public RentalInfoVM()
        {
            JobRentalList = new List<ADJobRental>();
        }

        public DataTable dtJobRentalList { get; set; }
        public ADJob obj_JobEdit { get; set; }
        public string Test { get; set; }
        public ADvwJobsList ObjvwJobList { get; set; }

        public List<ADJobRental> JobRentalList { get; set; }

    }
}
