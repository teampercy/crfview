﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using HDS.DAL.Providers;
using Microsoft.VisualBasic;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard
{
    public class ADvwJOBACTIONHISTORY
    {

        #region Properties

        public int JobActionHistoryId { get; set; }
        public int JobId { get; set; }
        public int ActionId { get; set; }
        public DateTime DateCreated { get; set; }
        public string RequestedBy { get; set; }
        public int RequestedByUserId { get; set; }
        public bool IsProcessed { get; set; }
        public DateTime DateProcessed { get; set; }
        public string Description { get; set; }
        public int EventTypeId { get; set; }
        public bool IsBillableEvent { get; set; }
        public string BillableEventCode { get; set; }
        public string BillableEventName { get; set; }
        public string LegalPartyType { get; set; }
        public bool IsMailReturnAction { get; set; }

        #endregion

        #region CRUD

        public static List<ADvwJOBACTIONHISTORY> GetActionHistoryList(DataTable dt)
        {
            try
            {
                if (dt.Rows.Count > 0)
                {

                    List<ADvwJOBACTIONHISTORY> ActionHistory = (from DataRow dr in dt.Rows
                                                                select new ADvwJOBACTIONHISTORY()
                                                                {
                                                                    JobActionHistoryId = ((dr["JobActionHistoryId"] != DBNull.Value ? Convert.ToInt32(dr["JobActionHistoryId"].ToString()) : 0)),
                                                                    JobId = ((dr["JobId"] != DBNull.Value ? Convert.ToInt32(dr["JobId"].ToString()) : 0)),
                                                                    ActionId = ((dr["ActionId"] != DBNull.Value ? Convert.ToInt32(dr["ActionId"].ToString()) : 0)),
                                                                    DateCreated = ((dr["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dr["DateCreated"]) : DateTime.MinValue)),
                                                                    Description = ((dr["Description"] != DBNull.Value ? dr["Description"].ToString() : "")),
                                                                    RequestedBy = ((dr["RequestedBy"] != DBNull.Value ? dr["RequestedBy"].ToString() : "")),
                                                                    RequestedByUserId = ((dr["RequestedByUserId"] != DBNull.Value ? Convert.ToInt32(dr["RequestedByUserId"].ToString()) : 0)),
                                                                    LegalPartyType = ((dr["LegalPartyType"] != DBNull.Value ? dr["LegalPartyType"].ToString() : "")),

                                                                }).ToList();
                    return ActionHistory;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return (new List<ADvwJOBACTIONHISTORY>());
            }
        }


        public static int JobReviewed(int JobID)
        {

            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

            NoteVM objNote = new NoteVM();
            objNote.objADJob = ADJob.ReadJob(Convert.ToInt32(JobID));
            objNote.objADJob.ManagerReviewed = true;
            objNote.objADJob.LastUpdatedById = user.Id;
            objNote.objADJob.LastUpdatedBy = user.UserName;
            objNote.objADJob.LastUpdatedOn = DateTime.Now;



            objNote.objADJob = ADJob.ReadJob(Convert.ToInt32(JobID));
            AutoMapper.Mapper.CreateMap<ADJob, Job>();
            Job myjob;
            myjob = AutoMapper.Mapper.Map<Job>(objNote.objADJob);

           
            

             //myjob = AutoMapper.Mapper.Map<Job>(objNote.objADJob);
             
                ITable tblJob = (ITable)myjob;
                DBO.Provider.Update(ref tblJob);

            var mysproc = new CRF.BLL.CRFDB.SPROCS.uspbo_Jobs_CreateActionHistory();

            mysproc.JobId = objNote.objADJob.Id;
             mysproc.ActionId = 232;
            mysproc.UserId = user.Id;
            mysproc.UserName = user.UserName;
            DBO.Provider.ExecNonQuery(mysproc);
           

            return 1;
        }


        public static int DeleteActionHistory(int JobActionHistoryId)
        {
            var sproc = new uspbo_Jobs_DeleteActionHistory();
            sproc.JobActionHistoryId = JobActionHistoryId;
            DBO.Provider.ExecNonQuery(sproc);

            return 1;
        }

        #endregion


    }
}
