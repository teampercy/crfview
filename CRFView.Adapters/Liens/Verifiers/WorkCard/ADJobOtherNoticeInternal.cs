﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard
{
    public class ADJobOtherNoticeInternal
    {

        #region Properties

        public int JobOtherNoticeInternalId { get; set; }
        public int JobId { get; set; }
        public int FormId { get; set; }
        public string FormCode { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DatePrinted { get; set; }
        public string RequestedBy { get; set; }
        public int RequestedByUserId { get; set; }
        public bool IsProcessed { get; set; }
        public DateTime DateProcessed { get; set; }
        public decimal BalanceDueAmt { get; set; }
        public decimal ContractAmt { get; set; }
        public decimal PaymentAmt { get; set; }
        public decimal FinanceChargeAmt { get; set; }
        public decimal ExtrasToContractAmt { get; set; }
        public decimal PendingChangeOrdersAmt { get; set; }
        public decimal DisputedClaimsAmt { get; set; }
        public string FileDate { get; set; }
        public string SuiteFileDate { get; set; }
        public string ContractDate { get; set; }
        public string NOISendDate { get; set; }
        public string WorkPerformedMMYY { get; set; }
        public string StructureType { get; set; }
        public string AmendmentsToContractAmt { get; set; }
        public string AgreedUponCreditsAmt { get; set; }
        public string ContractPaymentsAmt { get; set; }
        public string AmendmentPaymentsAmt { get; set; }
        public string BlockNumber { get; set; }
        public string LotNumber { get; set; }
        public string DueDate { get; set; }
        public string DescribePublicContract { get; set; }
        public string DaysCreditIsExtended { get; set; }
        public string NoticeofContractDate { get; set; }
        public string Terms { get; set; }
        public string Signer { get; set; }
        public decimal LienFee { get; set; }
        public decimal LienReleaseFee { get; set; }
        public decimal LienExtensionFee { get; set; }
        public decimal ExtraPageFee { get; set; }
        public decimal CarrierFee { get; set; }
        public decimal PostageFee { get; set; }
        public decimal OwnerServiceFee { get; set; }
        public decimal CopyFee { get; set; }
        public decimal TitleResearchFee { get; set; }
        public int EventTypeId { get; set; }
        public int JobActionHistoryId { get; set; }
        public decimal AgreedChangeOrdersAmt { get; set; }
        public bool RushBox { get; set; }
        public bool IsAdditionalFee { get; set; }
        public string DateNoticeSent { get; set; }
        public bool IsFreeBilling { get; set; }
        public string RegisteredAgent { get; set; }
        public string ExhibitBLabel { get; set; }
        public string BondNumber { get; set; }
        public string AltClaimantAdd1 { get; set; }
        public string AltClaimantAdd2 { get; set; }
        public string AltClaimantCity { get; set; }
        public string AltClaimantState { get; set; }
        public string AltClaimantZip { get; set; }
        public string AltClaimantContact { get; set; }
        public decimal ToBeDueAmt { get; set; }
        public decimal TotalFees { get; set; }
        public DateTime DocumentDate { get; set; }

        #endregion

        #region CRUD
        //Read Details from database
        public static ADJobOtherNoticeInternal ReadJobOtherNoticeInternal(int id)
        {
            JobOtherNoticeInternal myentity = new JobOtherNoticeInternal();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (JobOtherNoticeInternal)myentityItable;
            AutoMapper.Mapper.CreateMap<JobOtherNoticeInternal, ADJobOtherNoticeInternal>();
            return (AutoMapper.Mapper.Map<ADJobOtherNoticeInternal>(myentity));
        }
        public static List<ADJobOtherNoticeInternal> GetJobOtherNoticeInternalList(DataTable dt)
        {
            try
            {
                if (dt.Rows.Count > 0)
                {

                    List<ADJobOtherNoticeInternal> ActionHistory = (from DataRow dr in dt.Rows
                                                                    select new ADJobOtherNoticeInternal()
                                                                    {
                                                                        JobOtherNoticeInternalId = ((dr["JobOtherNoticeInternalId"] != DBNull.Value ? Convert.ToInt32(dr["JobOtherNoticeInternalId"]) : 0)),
                                                                        RequestedBy = ((dr["RequestedBy"] != DBNull.Value ? dr["RequestedBy"].ToString() : "")),
                                                                        DateCreated = ((dr["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dr["DateCreated"]) : DateTime.MinValue)),
                                                                        DateProcessed = ((dr["DateProcessed"] != DBNull.Value ? Convert.ToDateTime(dr["DateProcessed"]) : DateTime.MinValue)),
                                                                        FormCode = ((dr["FormCode"] != DBNull.Value ? dr["FormCode"].ToString() : "")),

                                                                    }).ToList();
                    return ActionHistory;
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                return (new List<ADJobOtherNoticeInternal>());
            }
        }

        //Save EndDateOtherNotices
        public static int saveEndDateOtherNotices(EndDateVM objEndDateVM)
        {
            try
            {


                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                ADJobOtherNoticeInternal objADJobOtherNoticeInternal = new ADJobOtherNoticeInternal();
                objADJobOtherNoticeInternal = ADJobOtherNoticeInternal.ReadJobOtherNoticeInternal(Convert.ToInt32(objEndDateVM.JobId));
                AutoMapper.Mapper.CreateMap<ADJobOtherNoticeInternal, JobOtherNoticeInternal>();
                JobOtherNoticeInternal objJobOtherNoticeInternal;



                objADJobOtherNoticeInternal.JobId = Convert.ToInt32(objEndDateVM.JobId);
                objADJobOtherNoticeInternal.RequestedBy = user.UserName;
                objADJobOtherNoticeInternal.RequestedByUserId = user.Id;

                if (objEndDateVM.RadioButton2 == true)
                {
                    objADJobOtherNoticeInternal.FormId = -1;
                    objADJobOtherNoticeInternal.FormCode = "Additional Filing Fees";
                }
                else
                {
                    //objADJobOtherNoticeInternal.FormId = Convert.ToInt32(objEndDateVM.FormId);
                    String[] spearator = objEndDateVM.FormId.Split(',');
                    objADJobOtherNoticeInternal.FormId = Convert.ToInt32(spearator[0]);
                    objADJobOtherNoticeInternal.FormCode = spearator[1];
                }

                objADJobOtherNoticeInternal.Signer = objEndDateVM.objADJobOtherNoticeInternal.Signer;

                if(objEndDateVM.objADJobOtherNoticeInternal.DocumentDate == null || objEndDateVM.objADJobOtherNoticeInternal.DocumentDate == DateTime.MinValue)
                {
                    objADJobOtherNoticeInternal.DocumentDate = DateTime.Today;
                }
                else
                {
                    objADJobOtherNoticeInternal.DocumentDate = objEndDateVM.objADJobOtherNoticeInternal.DocumentDate;
                }

               
                if (Convert.ToInt32(objEndDateVM.Id) < 1)
                {
                    objADJobOtherNoticeInternal.DateCreated = DateTime.Now;
                    objJobOtherNoticeInternal = AutoMapper.Mapper.Map<JobOtherNoticeInternal>(objADJobOtherNoticeInternal);
                    ITable tblJobOtherNoticeInternal = (ITable)objJobOtherNoticeInternal;

                    DBO.Provider.Create(ref tblJobOtherNoticeInternal);
                }
                else
                {
                    objADJobOtherNoticeInternal.JobOtherNoticeInternalId = Convert.ToInt32(objEndDateVM.Id);
                    objJobOtherNoticeInternal = AutoMapper.Mapper.Map<JobOtherNoticeInternal>(objADJobOtherNoticeInternal);
                    ITable tblJobOtherNoticeInternal = (ITable)objJobOtherNoticeInternal;

                    DBO.Provider.Update(ref tblJobOtherNoticeInternal);
                }

                if (objADJobOtherNoticeInternal.FormId > 0)
                {
                    var sproc = new uspbo_Jobs_CreateOtherNoticeEvent();
                    sproc.JobOtherNoticeInternaId = objADJobOtherNoticeInternal.JobOtherNoticeInternalId;
                    DBO.Provider.ExecNonQuery(sproc);
                    string NoticePath = CRF.BLL.Providers.LiensVerifiers.PrintOtherNotice(user, objADJobOtherNoticeInternal.JobOtherNoticeInternalId);
                }
            


            return 1;
      
    }
         catch (Exception ex)
            {
                return 0;
            }
        }
        public static int DeleteEndDateOtherNotices(EndDateVM objEndDateVM)
        {
            try
            {

                    var sproc = new uspbo_Jobs_DeleteOtherNoticeEvent();
                    sproc.JobOtherNoticeInternaId = Convert.ToInt32(objEndDateVM.Id);
                     DBO.Provider.ExecNonQuery(sproc);
                    //string NoticePath = CRF.BLL.Providers.LiensVerifiers.PrintOtherNotice(user, objADJobOtherNoticeInternal.JobOtherNoticeInternalId);
                
                return 1;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }
        #endregion


    }
}
