﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.Providers;
using CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard
{
   public class ADJobAlerts
    {
     
        public int Id { get; set; }
        public int JobId { get; set; }
        public int AlertLevel { get; set; }
        public string Category { get; set; }
        public string Description { get; set; }



        public static NoticeEditVM GetAlerts(string JobId,string JobType)
        {
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            NoticeEditVM objNoticeEditVM = new NoticeEditVM();
            JobVM ObjJobVM = JobInfo.LoadJobInfo(JobId);
            var sproc = new uspbo_Jobs_UpdateJobAlerts_Notice();
            sproc.JobId = JobId;
           DBO.Provider.ExecNonQuery(sproc);

            String sSql = "Select * From JobAlerts with (nolock) ";
            sSql += "Where JobId = " + JobId;
            DataSet myds = DBO.Provider.GetDataSet(sSql);

            objNoticeEditVM.dv  = new DataView(myds.Tables[0]);
            objNoticeEditVM.dv.RowFilter = "AlertLevel >=10 and AlertLevel < 20 ";
            DataTable dt = objNoticeEditVM.dv.ToTable();
            objNoticeEditVM.objADJobAlerts = GetJobAlerts(dt);

            objNoticeEditVM.dv1 = new DataView(myds.Tables[0]);
            objNoticeEditVM.dv1.RowFilter = "AlertLevel = 9 ";
            DataTable dt1 = objNoticeEditVM.dv1.ToTable();
            objNoticeEditVM.objJobAlertsList = GetJobAlerts(dt1);

            objNoticeEditVM.objADJob = ADJob.ReadJob(Convert.ToInt32(JobId));
            
            if (JobType == "Verify Job" || JobType == "Verify OW Only" || JobType == "Verify OW Only TX") {
                if (dt.Rows.Count > 0) {

                    objNoticeEditVM = LoadData(ObjJobVM, dt, dt1, -1, true);


                }
                var MYSPROC = new uspbo_Jobs_UpdateVerifyStatus();

                MYSPROC.JobId = Convert.ToInt32(JobId);
                MYSPROC.UserId = user.Id;
                MYSPROC.UserName = user.UserName;
                MYSPROC.JobType = JobType;
                DBO.Provider.ExecNonQuery(MYSPROC);
            }
            else
            {
                objNoticeEditVM = LoadData(ObjJobVM, dt, dt1, -1, false);


            
            }
            objNoticeEditVM.dt1 = dt;

            return objNoticeEditVM;
        }

        public static List<ADJobAlerts> GetJobAlerts(DataTable dt)
    {
        List<ADJobAlerts> JobAlertsList = (from DataRow dr in dt.Rows
                                           select new ADJobAlerts()
                                           {
                                               Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                               JobId = ((dr["JobId"] != DBNull.Value ? Convert.ToInt32(dr["JobId"]) : 0)),
                                               AlertLevel = ((dr["AlertLevel"] != DBNull.Value ? Convert.ToInt32(dr["AlertLevel"]) : 0)),
                                               Category = ((dr["Category"] != DBNull.Value ? dr["Category"].ToString() : "")),
                                               Description = ((dr["Description"] != DBNull.Value ? dr["Description"].ToString() : "")),
                                           }).ToList();
       
        return JobAlertsList;
    }

        public static NoticeEditVM LoadData(JobVM ObjJobVM,DataTable NoticeAlerts,DataTable NoticeWarnings,int aNoticeId, bool IsVerify)
        {
            NoticeEditVM objNoticeEditVM = new NoticeEditVM();
            objNoticeEditVM.GroupBox1 = false;
            objNoticeEditVM.GroupBox2 = true;
            objNoticeEditVM.OK_Button = true;
            objNoticeEditVM.RushBox = ObjJobVM.ObjvwJobList.RushOrder;
            objNoticeEditVM.ValidationWarnings = false;
            if (NoticeWarnings.Rows.Count > 0)
            {
                objNoticeEditVM.ValidationWarnings = true;
                objNoticeEditVM.objJobAlertsList.Clear();
                objNoticeEditVM.objJobAlertsList = ADJobAlerts.GetJobAlerts(NoticeWarnings);
            }



            if (NoticeAlerts.Rows.Count > 0)
            {
                //objNoticeEditVM.objJobAlertsList.Clear();
                objNoticeEditVM.objJobAlertsList = ADJobAlerts.GetJobAlerts(NoticeAlerts);
            }

            if (IsVerify == false)
            {
               
                DateTime mynoticesent = ObjJobVM.ObjvwJobList.NoticeSent;
                DateTime mydateassigned = ObjJobVM.ObjvwJobList.DateAssigned;
                if (mynoticesent < mydateassigned)
                {
                    objNoticeEditVM.btncheck = false;
                    objNoticeEditVM.btnUncheck = false;
                    objNoticeEditVM.ListView = false;
                }
                else
                {
                    objNoticeEditVM.Panel1 = true;
                    objNoticeEditVM.btncheck = true;
                    objNoticeEditVM.btnUncheck = true;
                    objNoticeEditVM.ListView = true;
                    objNoticeEditVM.CheckAll = false;
                    objNoticeEditVM.RushBox = false;
                }

                objNoticeEditVM.objLegalPartiesList = ADJobLegalParties.GetJobLegalPartiesList(ObjJobVM.ObjvwJobList.JobId.ToString());

                if (mynoticesent < mydateassigned) { 
                    objNoticeEditVM.CheckAll = true;
                }
                else
                {
                    objNoticeEditVM.CheckAll = false;
                }
                         
                
            }
          
            return objNoticeEditVM;


        }
    }

}
