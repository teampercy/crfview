﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels;
using HDS.DAL.Providers;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard
{
    public class JobInfo
    {
        #region Properties()
        #endregion

        #region CRUD

        public static JobVM LoadJobInfo(string JobID)
        {
            var myacc = new Job();
            ITable IAcc = myacc;
            DBO.Provider.Read(JobID, ref IAcc);
            myacc = (Job)IAcc;
            var myds = new DataSet();
            var mysproc = new uspbo_Jobs_GetJobInfoforVerifier();
            mysproc.JobId = JobID;
            myds = DBO.Provider.GetDataSet(mysproc);
            var mytableview = DBO.Provider.GetTableView(mysproc);
            
            DataTable dt = myds.Tables[0];
            
            JobVM Obj_Job = new JobVM();
            Obj_Job.jobdt = myds.Tables[0];
            Obj_Job.ObjvwJobList = GetJobInfo(dt);

           if(Obj_Job.ObjvwJobList.PublicJob == true)
            {
                Obj_Job.ObjvwJobList.PropertyType = "Public";
            }
           else if (Obj_Job.ObjvwJobList.FederalJob == true)
            {
                Obj_Job.ObjvwJobList.PropertyType = "Federal";
            }
           else if (Obj_Job.ObjvwJobList.ResidentialBox == true)
            {
                Obj_Job.ObjvwJobList.PropertyType = "Residential";
            }
            else
            {
                Obj_Job.ObjvwJobList.PropertyType = "Private";
            }
           
           

            DataTable dtClient = myds.Tables[6];
            Obj_Job.obj_ClientEdit = ADClient.GetClient(dtClient);

            DataTable dtClientLienInfo = myds.Tables[7];
            Obj_Job.obj_ClientLienInfo = ADClientLienInfo.GetClientLienInfo(dtClientLienInfo);

            Obj_Job.dtClientInfo = myds.Tables[17];
            if(!(Obj_Job.dtClientInfo.Rows.Count == 0))
            {
                Obj_Job.ClientId = Obj_Job.dtClientInfo.Rows[0]["ClientId"].ToString();

            }
            else
            {
                Obj_Job.ClientId = 0.ToString();
            }
            // Obj_Job.ObjEditBranchVM.ClientId = dtClientInfo.Rows[0]["ClientId"].ToString();

            DataTable dttable = myds.Tables[18];

            Obj_Job.dtStateInfo = myds.Tables[13];
            Obj_Job.objADStateInfo = ADStateInfo.GetStateInfo(Obj_Job.dtStateInfo);
            //Obj_Job.ObjEditBranchVM.BranchList = CommonBindList.GetBranchlist();
            // List<ClientBranchList> list
            // List<ADClient> s =
            // List<ADClient> list = ADClient.GetBranchInfo(dttable);
            //Obj_Job.ObjEditBranchVM.objClientList = list;
            //Obj_Job.ClientBranchList = GetBranchInfo(dttable);
            //Obj_Job.ObjEditBranchVM.ClientBranchList  = Converter< GetBranchInfo(dttable)

            //DataTable dtJob = myds.Tables[22];
            //Obj_Job.ObjJobList = ADJob.GetJob(dtJob);

            //If myentityinfo.IsServeCust = True Then
            //    mytableedit.CustInfo.BackColor = Color.LightSteelBlue
            //End If`

            DataTable dtJOBACTIONHISTORY = myds.Tables[14];
            //dtJOBACTIONHISTORY.DefaultView.Sort = "Description ASC";


            Obj_Job.ObjJOBACTIONHISTORY = ADvwJOBACTIONHISTORY.GetActionHistoryList(dtJOBACTIONHISTORY);

            DataTable dtJOBBILLABLEITEMHISTORY = myds.Tables[15];
            Obj_Job.ObjJOBBILLABLEITEMHISTORY = ADJOBBILLABLEITEMHISTORY.GetJOBBILLABLEITEMHISTORYList(dtJOBBILLABLEITEMHISTORY);


            DataTable dtNotes = myds.Tables[1];
            DataView dvNotes = new DataView(dtNotes);
            dvNotes.Sort = "Id DESC";
            DataTable dtNotesDesc = dvNotes.ToTable();
            Obj_Job.ObjNotes = ADvwLienNotes.GetNotes(dtNotesDesc);

            DataTable dtNotesvwJobNoticeRequestHistory = myds.Tables[2];
            Obj_Job.vwJobNoticeRequestHistoryList = ADvwJobNoticeRequestHistory.GetvwJobNoticeRequestHistoryList(dtNotesvwJobNoticeRequestHistory);

            DataTable dtJobAlerts = myds.Tables[8];
            DataView dv = new DataView(dtJobAlerts);
            dv.RowFilter = "Description ='NOC State' OR Description ='Rush Job' OR Description LIKE '%NTO Days Remaining%'"; // query example = "id = 10"
           DataTable dtAlerts = dv.ToTable();
            Obj_Job.objADJobAlerts = ADJobAlerts.GetJobAlerts(dtAlerts);

            string mysql = "Select Id, DateCreated,UserCode, DocumentType,DocumentDescription from JobAttachments (nolock)";
            mysql += " Where JobId = " + JobID + " Order By Id";
            var CBRView = DBO.Provider.GetTableView(mysql);
            DataTable dtdocument = CBRView.ToTable();
            Obj_Job.ObjDocuments = ADJobAttachments.GetJobAttachments(dtdocument);

            //string mysqlquery = "Select RequestedBy, DateCreated,DateProcessed, FormCode from JobOtherNoticeInternal (nolock)";
            //mysqlquery += " Where JobId = " + JobID;
            //var CBRViewtbl = DBO.Provider.GetTableView(mysqlquery);

            DataTable dtJobNoticePrelimRequest = myds.Tables[9];
            Obj_Job.JobNoticePrelimRequestList = ADJobNoticePrelimRequest.GetJobNoticePrelimRequestList(dtJobNoticePrelimRequest);
            DataTable dtJobOtherNotices = myds.Tables[11];
            Obj_Job.ObjJobOtherNoticeInternalList = ADJobOtherNoticeInternal.GetJobOtherNoticeInternalList(dtJobOtherNotices);

            // var myview = DBO.Provider.GetTableView(mytableview.Table[1]);
            // myview.RowFilter = "NoteTypeId=1";
            Obj_Job.dtNotice = myds.Tables[3];
            DataView LegalParty = new DataView(Obj_Job.dtNotice);
            LegalParty.RowFilter = "IsPrimary=0 OR TypeCode='DE'"; 
            DataTable JobLegalParty = LegalParty.ToTable();
            Obj_Job.objLegalPartiesList = ADJobLegalParties.JobLegalPartiesList(JobLegalParty);

            Obj_Job.dtClientCustomer = myds.Tables[4];
            Obj_Job.obj_ClientCustomer = ADClientCustomer.GetClientCustomerList(Obj_Job.dtClientCustomer);

            Obj_Job.dtGC = myds.Tables[5];
            Obj_Job.objADClientGeneralContractor = ADClientGeneralContractor.GetGCList(Obj_Job.dtGC);

            DataTable dtTexasRequest = myds.Tables[10];
            Obj_Job.ObjADJobTexasRequest = ADJobTexasRequest.GetJobTexasRequestList(dtTexasRequest);

            DataTable dtMonthlyRequests = myds.Tables[16];
            Obj_Job.ObjADJobVerifiedSentRequest = ADJobVerifiedSentRequest.GetJobVerifiedSentRequest(dtMonthlyRequests);

            DataTable dtStateForms = myds.Tables[12];
            DataView StateForms = new DataView(dtStateForms);
            StateForms.RowFilter = "CRFView = 1";
            DataTable StateFormNotices = StateForms.ToTable();
            Obj_Job.objADStateFormNotices = ADStateFormNotices.GetStateFormNoticesList(StateFormNotices);

            Obj_Job.dtJobMatchList = myds.Tables[23];
            Obj_Job.dtJobMatchListNJ = myds.Tables[24];
            Obj_Job.objADJobLegalParties = ADJobLegalParties.ReadJobLegalParties(Obj_Job.ObjvwJobList.OwnrId);
            Obj_Job.objJobLegalParties = ADJobLegalParties.ReadJobLegalParties(Obj_Job.ObjvwJobList.LenderId);

            return Obj_Job;
        }



        public static bool LogJobChanges(EditJobVM objUpdateJob)
        {
            try
            {
            

                    ADJob objAdJob = ADJob.ReadJob(objUpdateJob.obj_JobEdit.Id);
                    //ADClient objADclient = ADClient.ReadClientBranch(objUpdateJob.obj_ClientEdit.ClientId);
                    CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                if (objUpdateJob.obj_JobEdit.PrivateJob == true)
                {
                    objUpdateJob.obj_JobEdit.sOldType = "Private Job";
                }

                if (objUpdateJob.obj_JobEdit.PrivateJob == true)
                {
                    objUpdateJob.obj_JobEdit.sOldType = "Private Job";
                }
                if (objUpdateJob.obj_JobEdit.PublicJob == true)
                {
                    objUpdateJob.obj_JobEdit.sOldType = "Public Job";
                }
                if (objUpdateJob.obj_JobEdit.FederalJob == true)
                {
                    objUpdateJob.obj_JobEdit.sOldType = "Federal Job";
                }
                if (objUpdateJob.obj_JobEdit.ResidentialBox == true)
                {
                    objUpdateJob.obj_JobEdit.sOldType = "Residential Job";
                }

                if (objUpdateJob.obj_JobEdit.PrivateJob == true)
                {
                    objUpdateJob.obj_JobEdit.sNewType = "Private Job";
                }
                if (objUpdateJob.obj_JobEdit.PublicJob == true)
                {
                    objUpdateJob.obj_JobEdit.sNewType = "Public Job";
                }
                if (objUpdateJob.obj_JobEdit.FederalJob == true)
                {
                    objUpdateJob.obj_JobEdit.sNewType = "Federal Job";
                }
                if (objUpdateJob.obj_JobEdit.ResidentialBox == true)
                {
                    objUpdateJob.obj_JobEdit.sNewType = "Residential Job";
                }

                if (objUpdateJob.obj_JobEdit.PrelimBox == true)
                {
                    objUpdateJob.obj_JobEdit.sOldJobType = "Prelim NTO";
                }
                if (objUpdateJob.obj_JobEdit.VerifyJob == true)
                {
                    objUpdateJob.obj_JobEdit.sOldJobType = "Verify Job";
                }
                if (objUpdateJob.obj_JobEdit.TitleVerifiedBox == true)
                {
                    objUpdateJob.obj_JobEdit.sOldJobType = "Verified As Is";
                }
                if (objUpdateJob.obj_JobEdit.SendAsIsBox == true)
                {
                    objUpdateJob.obj_JobEdit.sOldJobType = "Send As Is";
                }
                if (objUpdateJob.obj_JobEdit.VerifyOWOnly == true)
                {
                    objUpdateJob.obj_JobEdit.sOldJobType = "Verify OW Only";
                }
                if (objUpdateJob.obj_JobEdit.VerifyOWOnlySend == true)
                {
                    objUpdateJob.obj_JobEdit.sOldJobType = "Verify OW Only Send";
                }
                if (objUpdateJob.obj_JobEdit.VerifyOWOnlyTX == true)
                {
                    objUpdateJob.obj_JobEdit.sOldJobType = "Verify OW Only TX";
                }

                if (objUpdateJob.obj_JobEdit.PrelimBox == true)
                {
                    objUpdateJob.obj_JobEdit.sNewJobType = "Prelim NTO";
                }
                if (objUpdateJob.obj_JobEdit.VerifyJob == true)
                {
                    objUpdateJob.obj_JobEdit.sNewJobType = "Verify Job";
                }
                if (objUpdateJob.obj_JobEdit.TitleVerifiedBox == true)
                {
                    objUpdateJob.obj_JobEdit.sNewJobType = "Verified As Is";
                }
                if (objUpdateJob.obj_JobEdit.SendAsIsBox == true)
                {
                    objUpdateJob.obj_JobEdit.sNewJobType = "Send As Is";
                }
                if (objUpdateJob.obj_JobEdit.VerifyOWOnly == true)
                {
                    objUpdateJob.obj_JobEdit.sNewJobType = "Verify OW Only";
                }
                if (objUpdateJob.obj_JobEdit.VerifyOWOnlySend == true)
                {
                    objUpdateJob.obj_JobEdit.sNewJobType = "Verify OW Only Send";
                }
                if (objUpdateJob.obj_JobEdit.VerifyOWOnlyTX == true)
                {
                    objUpdateJob.obj_JobEdit.sNewJobType = "Verify OW Only TX";
                }


                var mysproc = new CRF.BLL.CRFDB.SPROCS.uspbo_Jobs_UpdateChangeLog();
                mysproc.JobId = objUpdateJob.obj_JobEdit.Id;
                mysproc.UserId = user.Id;
                mysproc.UserName = user.UserName;
                mysproc.ChangeType = 0;

                mysproc.ChangeFrom = objAdJob.JobName.Trim() + "~";
                mysproc.ChangeFrom = objAdJob.JobNum.Trim() + "~";
                mysproc.ChangeFrom = objAdJob.PONum.Trim() + "~";
                //mysproc.ChangeFrom = objAdJob.EstBalance.Trim() + "~";
                mysproc.ChangeFrom = objAdJob.JobAdd1.Trim() + "~";
                mysproc.ChangeFrom = objAdJob.JobAdd2.Trim() + "~";
                mysproc.ChangeFrom = objAdJob.JobCity.Trim() + "~";
                mysproc.ChangeFrom = objAdJob.JobState.Trim() + "~";
                mysproc.ChangeFrom = objAdJob.JobZip.Trim();

                if (objUpdateJob.obj_JobEdit.JobName == null || objUpdateJob.obj_JobEdit.JobName == "")
                {

                    objUpdateJob.obj_JobEdit.JobName = "";
                }
                mysproc.ChangeTo = objUpdateJob.obj_JobEdit.JobName.Trim() + "~";
                if (objUpdateJob.obj_JobEdit.JobNum == null || objUpdateJob.obj_JobEdit.JobNum == "")
                {
                    objUpdateJob.obj_JobEdit.JobNum = "";
                }
                mysproc.ChangeTo = objUpdateJob.obj_JobEdit.JobNum.Trim() + "~";

                if (objUpdateJob.obj_JobEdit.PONum == null || objUpdateJob.obj_JobEdit.PONum == "")
                {

                    objUpdateJob.obj_JobEdit.PONum = "";
                }
                mysproc.ChangeTo = objUpdateJob.obj_JobEdit.PONum.Trim() + "~";

                //if (objUpdateJob.obj_JobEdit.JobNum == null || objUpdateJob.obj_JobEdit.JobNum == "")
                //{

                //    objUpdateJob.obj_JobEdit.JobNum = "";
                // }
                if (objUpdateJob.obj_JobEdit.JobAdd1 == null || objUpdateJob.obj_JobEdit.JobAdd1 == "")
                {

                    objUpdateJob.obj_JobEdit.JobAdd1 = "";
                }
                mysproc.ChangeTo = objUpdateJob.obj_JobEdit.JobAdd1.Trim() + "~";

                if (objUpdateJob.obj_JobEdit.JobAdd2 == null || objUpdateJob.obj_JobEdit.JobAdd2 == "")
                {

                    objUpdateJob.obj_JobEdit.JobAdd2 = "";
                }
                mysproc.ChangeTo = objUpdateJob.obj_JobEdit.JobAdd2.Trim() + "~";

                if (objUpdateJob.obj_JobEdit.JobCity == null || objUpdateJob.obj_JobEdit.JobCity == "")
                {

                    objUpdateJob.obj_JobEdit.JobCity = "";
                }
                mysproc.ChangeTo = objUpdateJob.obj_JobEdit.JobCity.Trim() + "~";

                if (objUpdateJob.obj_JobEdit.JobState == null || objUpdateJob.obj_JobEdit.JobState == "")
                {

                    objUpdateJob.obj_JobEdit.JobState = "";
                }
                mysproc.ChangeTo = objUpdateJob.obj_JobEdit.JobState.Trim() + "~";

                if (objUpdateJob.obj_JobEdit.JobZip == null || objUpdateJob.obj_JobEdit.JobZip == "")
                {

                    objUpdateJob.obj_JobEdit.JobZip = "";
                }
                mysproc.ChangeTo = objUpdateJob.obj_JobEdit.JobZip.Trim();

                
                if (mysproc.ChangeFrom != mysproc.ChangeTo)
                {
                    DBO.Provider.ExecNonQuery(mysproc);
                }


                if (objUpdateJob.obj_JobEdit.sOldType != objUpdateJob.obj_JobEdit.sNewType)
                {
                    mysproc.ChangeFrom = objUpdateJob.obj_JobEdit.sOldType.Trim() + " ";
                    mysproc.ChangeTo = objUpdateJob.obj_JobEdit.sNewType.Trim();
                    DBO.Provider.ExecNonQuery(mysproc);
                }
                if (objUpdateJob.obj_JobEdit.sOldJobType != objUpdateJob.obj_JobEdit.sNewJobType)
                {
                    mysproc.ChangeFrom = objUpdateJob.obj_JobEdit.sOldJobType.Trim() + " ";
                    mysproc.ChangeTo = objUpdateJob.obj_JobEdit.sNewJobType.Trim();
                    DBO.Provider.ExecNonQuery(mysproc);
                }

                if (objUpdateJob.obj_JobEdit.sOldJobType != objUpdateJob.obj_JobEdit.sNewJobType)
                {
                    var mysproc2 = new CRF.BLL.CRFDB.SPROCS.uspbo_Jobs_UpdateJobType();
                    mysproc2.JobId = objUpdateJob.obj_JobEdit.Id;
                    if(objUpdateJob.obj_JobEdit.sNewJobType == "Prelim NTO")
                    {
                        mysproc2.JobTypeOption = 1;
                    }
                    if (objUpdateJob.obj_JobEdit.sNewJobType == "Verify Job")
                    {
                        mysproc2.JobTypeOption = 2;
                    }
                    if (objUpdateJob.obj_JobEdit.sNewJobType == "Verified As Is")
                    {
                        mysproc2.JobTypeOption = 3;
                    }
                    if (objUpdateJob.obj_JobEdit.sNewJobType == "Send As Is")
                    {
                        mysproc2.JobTypeOption = 4;
                    }
                    if (objUpdateJob.obj_JobEdit.sNewJobType == "Verify OW Only")
                    {
                        mysproc2.JobTypeOption = 5;
                    }
                    if (objUpdateJob.obj_JobEdit.sNewJobType == "Verify OW Only Send")
                    {
                        mysproc2.JobTypeOption = 6;
                    }
                    if (objUpdateJob.obj_JobEdit.sNewJobType == "Verify OW Only TX")
                    {
                        mysproc2.JobTypeOption = 7;
                    }
                }
                    return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        #endregion

        #region Local Methods
        public static ADvwJobsList GetJobInfo(DataTable dt)
        {
            List<ADvwJobsList> vwJob = new List<ADvwJobsList>();
            try
            {
 
           
             vwJob = (from DataRow dr in dt.Rows
                                        select new ADvwJobsList()
                                        {
                                            ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
                                            JobId = ((dr["JobId"] != DBNull.Value ? Convert.ToInt32(dr["JobId"]) : 0)),
                                            BranchNumber = ((dr["BranchNumber"] != DBNull.Value ? dr["BranchNumber"].ToString() : "")),
                                            ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                            ClientName = ((dr["ClientName"] != DBNull.Value ? dr["ClientName"].ToString() : "")),
                                            JobNum = ((dr["JobNum"] != DBNull.Value ? dr["JobNum"].ToString() : "")),
                                            JobName = ((dr["JobName"] != DBNull.Value ? dr["JobName"].ToString() : "")),
                                            JobAdd1 = ((dr["JobAdd1"] != DBNull.Value ? dr["JobAdd1"].ToString() : "")),
                                            JobAdd2 = ((dr["JobAdd2"] != DBNull.Value ? dr["JobAdd2"].ToString() : "")),
                                            JobCity = ((dr["JobCity"] != DBNull.Value ? dr["JobCity"].ToString() : "")),
                                            JobState = ((dr["JobState"] != DBNull.Value ? dr["JobState"].ToString() : "")),
                                            JobZip = ((dr["JobZip"] != DBNull.Value ? dr["JobZip"].ToString() : "")),
                                            JobCounty = ((dr["JobCounty"] != DBNull.Value ? dr["JobCounty"].ToString() : "")),
                                            LaborType = ((dr["LaborType"] != DBNull.Value ? dr["LaborType"].ToString() : "")),
                                            StartDate = ((dr["StartDate"] != DBNull.Value ? Convert.ToDateTime(dr["StartDate"]) : DateTime.MinValue)),
                                            PrelimDate = ((dr["PrelimDate"] != DBNull.Value ? Convert.ToDateTime(dr["PrelimDate"]) : DateTime.MinValue)),
                                            EquipRental = ((dr["EquipRental"] != DBNull.Value ? dr["EquipRental"].ToString() : "")),
                                            PublicJob = ((dr["PublicJob"] != DBNull.Value ? Convert.ToBoolean(dr["PublicJob"]) : false)),
                                            JointCk = ((dr["JointCk"] != DBNull.Value ? Convert.ToBoolean(dr["JointCk"]) : false)),
                                            SuretyBox = ((dr["SuretyBox"] != DBNull.Value ? Convert.ToBoolean(dr["SuretyBox"]) : false)),
                                            EstBalance = ((dr["EstBalance"] != DBNull.Value ? Convert.ToDecimal(dr["EstBalance"]) : 0)),
                                            CurrDate = ((dr["CurrDate"] != DBNull.Value ? Convert.ToDateTime(dr["CurrDate"]) : DateTime.MinValue)),
                                            EquipRate = ((dr["EquipRate"] != DBNull.Value ? dr["EquipRate"].ToString() : "")),
                                            UseBranchNTO = ((dr["UseBranchNTO"] != DBNull.Value ? Convert.ToBoolean(dr["UseBranchNTO"]) : false)),
                                            ResidentialBox = ((dr["ResidentialBox"] != DBNull.Value ? Convert.ToBoolean(dr["ResidentialBox"]) : false)),
                                            IsRentalCo = ((dr["IsRentalCo"] != DBNull.Value ? Convert.ToBoolean(dr["IsRentalCo"]) : false)),
                                            Note = ((dr["Note"] != DBNull.Value ? dr["Note"].ToString() : "")),
                                            ResendBox = ((dr["ResendBox"] != DBNull.Value ? Convert.ToBoolean(dr["ResendBox"]) : false)),
                                            AmendedBox = ((dr["AmendedBox"] != DBNull.Value ? Convert.ToBoolean(dr["AmendedBox"]) : false)),
                                            ResendOwner = ((dr["ResendOwner"] != DBNull.Value ? Convert.ToBoolean(dr["ResendOwner"]) : false)),
                                            ResendGC = ((dr["ResendGC"] != DBNull.Value ? Convert.ToBoolean(dr["ResendGC"]) : false)),
                                            ResendCust = ((dr["ResendCust"] != DBNull.Value ? Convert.ToBoolean(dr["ResendCust"]) : false)),
                                            ResendLender = ((dr["ResendLender"] != DBNull.Value ? Convert.ToBoolean(dr["ResendLender"]) : false)),
                                            CustSend = ((dr["CustSend"] != DBNull.Value ? Convert.ToBoolean(dr["CustSend"]) : false)),
                                            NoticeSent = ((dr["NoticeSent"] != DBNull.Value ? Convert.ToDateTime(dr["NoticeSent"]) : DateTime.MinValue)),
                                            ResendDate = ((dr["ResendDate"] != DBNull.Value ? Convert.ToDateTime(dr["ResendDate"]) : DateTime.MinValue)),
                                            AmendedDate = ((dr["AmendedDate"] != DBNull.Value ? Convert.ToDateTime(dr["AmendedDate"]) : DateTime.MinValue)),
                                            CustJobNum = ((dr["CustJobNum"] != DBNull.Value ? dr["CustJobNum"].ToString() : "")),
                                            NoticePhNum = ((dr["NoticePhNum"] != DBNull.Value ? dr["NoticePhNum"].ToString() : "")),
                                            BondNum = ((dr["BondNum"] != DBNull.Value ? dr["BondNum"].ToString() : "")),
                                            APNNum = ((dr["APNNum"] != DBNull.Value ? dr["APNNum"].ToString() : "")),
                                            EndDate = ((dr["EndDate"] != DBNull.Value ? Convert.ToDateTime(dr["EndDate"]) : DateTime.MinValue)),
                                            ContactName = ((dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : "")),
                                            AZCert = ((dr["AZCert"] != DBNull.Value ? Convert.ToBoolean(dr["AZCert"]) : false)),
                                            FLCertBox = ((dr["FLCertBox"] != DBNull.Value ? Convert.ToBoolean(dr["FLCertBox"]) : false)),
                                            SendNTOList = ((dr["SendNTOList"] != DBNull.Value ? Convert.ToBoolean(dr["SendNTOList"]) : false)),
                                            SendNTO = ((dr["SendNTO"] != DBNull.Value ? Convert.ToBoolean(dr["SendNTO"]) : false)),
                                            GreenCard = ((dr["GreenCard"] != DBNull.Value ? Convert.ToBoolean(dr["GreenCard"]) : false)),
                                            Age = ((dr["Age"] != DBNull.Value ? Convert.ToInt32(dr["Age"]) : 0)),
                                            LienContact = ((dr["LienContact"] != DBNull.Value ? dr["LienContact"].ToString() : "")),
                                            LienAdd1 = ((dr["LienAdd1"] != DBNull.Value ? dr["LienAdd1"].ToString() : "")),
                                            LienAdd2 = ((dr["LienAdd2"] != DBNull.Value ? dr["LienAdd2"].ToString() : "")),
                                            LienCity = ((dr["LienCity"] != DBNull.Value ? dr["LienCity"].ToString() : "")),
                                            LienState = ((dr["LienState"] != DBNull.Value ? dr["LienState"].ToString() : "")),
                                            LienZip = ((dr["LienZip"] != DBNull.Value ? dr["LienZip"].ToString() : "")),
                                            LegalInterest = ((dr["LegalInterest"] != DBNull.Value ? Convert.ToDecimal(dr["LegalInterest"]) : 0)),
                                            RecorderNum = ((dr["RecorderNum"] != DBNull.Value ? dr["RecorderNum"].ToString() : "")),
                                            FileDate = ((dr["FileDate"] != DBNull.Value ? Convert.ToDateTime(dr["FileDate"]) : DateTime.MinValue)),
                                            BookNum = ((dr["BookNum"] != DBNull.Value ? dr["BookNum"].ToString() : "")),
                                            PageNum = ((dr["PageNum"] != DBNull.Value ? dr["PageNum"].ToString() : "")),
                                            LienPhone = ((dr["LienPhone"] != DBNull.Value ? dr["LienPhone"].ToString() : "")),
                                            LienFax = ((dr["LienFax"] != DBNull.Value ? dr["LienFax"].ToString() : "")),
                                            PrelimReq = ((dr["PrelimReq"] != DBNull.Value ? dr["PrelimReq"].ToString() : "")),
                                            PrelimTime = ((dr["PrelimTime"] != DBNull.Value ? dr["PrelimTime"].ToString() : "")),
                                            PrelimInfo = ((dr["PrelimInfo"] != DBNull.Value ? dr["PrelimInfo"].ToString() : "")),
                                            MechTime = ((dr["MechTime"] != DBNull.Value ? dr["MechTime"].ToString() : "")),
                                            MechPeriod = ((dr["MechPeriod"] != DBNull.Value ? dr["MechPeriod"].ToString() : "")),
                                            MechServe = ((dr["MechServe"] != DBNull.Value ? dr["MechServe"].ToString() : "")),
                                            MechInfo = ((dr["MechInfo"] != DBNull.Value ? dr["MechInfo"].ToString() : "")),
                                            StopNotice = ((dr["StopNotice"] != DBNull.Value ? dr["StopNotice"].ToString() : "")),
                                            StopNoticeInfo = ((dr["StopNoticeInfo"] != DBNull.Value ? dr["StopNoticeInfo"].ToString() : "")),
                                            MiscInfo = ((dr["MiscInfo"] != DBNull.Value ? dr["MiscInfo"].ToString() : "")),
                                            OwnerServBox = ((dr["OwnerServBox"] != DBNull.Value ? Convert.ToBoolean(dr["OwnerServBox"]) : false)),
                                            GCServBox = ((dr["GCServBox"] != DBNull.Value ? Convert.ToBoolean(dr["GCServBox"]) : false)),
                                            LenderServBox = ((dr["LenderServBox"] != DBNull.Value ? Convert.ToBoolean(dr["LenderServBox"]) : false)),
                                            CustServBox = ((dr["CustServBox"] != DBNull.Value ? Convert.ToBoolean(dr["CustServBox"]) : false)),
                                            CourtServeBox = ((dr["CourtServeBox"] != DBNull.Value ? Convert.ToBoolean(dr["CourtServeBox"]) : false)),
                                            EstBalanceBox = ((dr["EstBalanceBox"] != DBNull.Value ? Convert.ToBoolean(dr["EstBalanceBox"]) : false)),
                                            NOIStateBox = ((dr["NOIStateBox"] != DBNull.Value ? Convert.ToBoolean(dr["NOIStateBox"]) : false)),
                                            ResidentialRulesBox = ((dr["ResidentialRulesBox"] != DBNull.Value ? Convert.ToBoolean(dr["ResidentialRulesBox"]) : false)),
                                            WebLien = ((dr["WebLien"] != DBNull.Value ? Convert.ToBoolean(dr["WebLien"]) : false)),
                                            WebNTO = ((dr["WebNTO"] != DBNull.Value ? Convert.ToBoolean(dr["WebNTO"]) : false)),
                                            NOCBox = ((dr["NOCBox"] != DBNull.Value ? Convert.ToBoolean(dr["NOCBox"]) : false)),

                                            NTODays = ((dr["NTODays"] != DBNull.Value ? dr["NTODays"].ToString() : "")),
                                            LienDays = ((dr["LienDays"] != DBNull.Value ? dr["LienDays"].ToString() : "")),
                                            LenderName = ((dr["LenderName"] != DBNull.Value ? dr["LenderName"].ToString() : "")),
                                            LenderAdd1 = ((dr["LenderAdd1"] != DBNull.Value ? dr["LenderAdd1"].ToString() : "")),
                                            LenderAdd2 = ((dr["LenderAdd2"] != DBNull.Value ? dr["LenderAdd2"].ToString() : "")),
                                            LenderCity = ((dr["LenderCity"] != DBNull.Value ? dr["LenderCity"].ToString() : "")),
                                            LenderState = ((dr["LenderState"] != DBNull.Value ? dr["LenderState"].ToString() : "")),
                                            LenderZip = ((dr["LenderZip"] != DBNull.Value ? dr["LenderZip"].ToString() : "")),
                                            LenderPhone = ((dr["LenderPhone"] != DBNull.Value ? dr["LenderPhone"].ToString() : "")),
                                            LenderFax = ((dr["LenderFax"] != DBNull.Value ? dr["LenderFax"].ToString() : "")),
                                            GCNum = ((dr["GCNum"] != DBNull.Value ? dr["GCNum"].ToString() : "")),
                                            GCName = ((dr["GCName"] != DBNull.Value ? dr["GCName"].ToString() : "")),
                                            GCAdd1 = ((dr["GCAdd1"] != DBNull.Value ? dr["GCAdd1"].ToString() : "")),
                                            GCAdd2 = ((dr["GCAdd2"] != DBNull.Value ? dr["GCAdd2"].ToString() : "")),
                                            GCCity = ((dr["GCCity"] != DBNull.Value ? dr["GCCity"].ToString() : "")),
                                            GCState = ((dr["GCState"] != DBNull.Value ? dr["GCState"].ToString() : "")),
                                            GCZip = ((dr["GCZip"] != DBNull.Value ? dr["GCZip"].ToString() : "")),
                                            GCPhNum = ((dr["GCPhNum"] != DBNull.Value ? dr["GCPhNum"].ToString() : "")),
                                            CustRefNum = ((dr["CustRefNum"] != DBNull.Value ? dr["CustRefNum"].ToString() : "")),
                                            CustName = ((dr["CustName"] != DBNull.Value ? dr["CustName"].ToString() : "")),
                                            CustAdd1 = ((dr["CustAdd1"] != DBNull.Value ? dr["CustAdd1"].ToString() : "")),
                                            CustAdd2 = ((dr["CustAdd2"] != DBNull.Value ? dr["CustAdd2"].ToString() : "")),
                                            CustCity = ((dr["CustCity"] != DBNull.Value ? dr["CustCity"].ToString() : "")),
                                            CustState = ((dr["CustState"] != DBNull.Value ? dr["CustState"].ToString() : "")),
                                            CustZip = ((dr["CustZip"] != DBNull.Value ? dr["CustZip"].ToString() : "")),
                                            CustPhNUm = ((dr["CustPhNUm"] != DBNull.Value ? dr["CustPhNUm"].ToString() : "")),
                                            OwnerName = ((dr["OwnerName"] != DBNull.Value ? dr["OwnerName"].ToString() : "")),
                                            OwnerAdd1 = ((dr["OwnerAdd1"] != DBNull.Value ? dr["OwnerAdd1"].ToString() : "")),
                                            OwnerAdd2 = ((dr["OwnerAdd2"] != DBNull.Value ? dr["OwnerAdd2"].ToString() : "")),
                                            OwnerCity = ((dr["OwnerCity"] != DBNull.Value ? dr["OwnerCity"].ToString() : "")),
                                            OwnerState = ((dr["OwnerState"] != DBNull.Value ? dr["OwnerState"].ToString() : "")),
                                            OwnerZip = ((dr["OwnerZip"] != DBNull.Value ? dr["OwnerZip"].ToString() : "")),
                                            OwnerPhNum = ((dr["OwnerPhNum"] != DBNull.Value ? dr["OwnerPhNum"].ToString() : "")),
                                            OwnerFax = ((dr["OwnerFax"] != DBNull.Value ? dr["OwnerFax"].ToString() : "")),
                                            NTOContact = ((dr["NTOContact"] != DBNull.Value ? dr["NTOContact"].ToString() : "")),
                                            NTOContactTitle = ((dr["NTOContactTitle"] != DBNull.Value ? dr["NTOContactTitle"].ToString() : "")),
                                            NTOAdd1 = ((dr["NTOAdd1"] != DBNull.Value ? dr["NTOAdd1"].ToString() : "")),
                                            NTOAdd2 = ((dr["NTOAdd2"] != DBNull.Value ? dr["NTOAdd2"].ToString() : "")),
                                            NTOCity = ((dr["NTOCity"] != DBNull.Value ? dr["NTOCity"].ToString() : "")),
                                            NTOState = ((dr["NTOState"] != DBNull.Value ? dr["NTOState"].ToString() : "")),
                                            NTOZip = ((dr["NTOZip"] != DBNull.Value ? dr["NTOZip"].ToString() : "")),
                                            NTOEmail = ((dr["NTOEmail"] != DBNull.Value ? dr["NTOEmail"].ToString() : "")),
                                            NTOPhone = ((dr["NTOPhone"] != DBNull.Value ? dr["NTOPhone"].ToString() : "")),
                                            NTOFax = ((dr["NTOFax"] != DBNull.Value ? dr["NTOFax"].ToString() : "")),
                                            DesigneeName = ((dr["DesigneeName"] != DBNull.Value ? dr["DesigneeName"].ToString() : "")),
                                            DesigneeAdd1 = ((dr["DesigneeAdd1"] != DBNull.Value ? dr["DesigneeAdd1"].ToString() : "")),
                                            DesigneeAdd2 = ((dr["DesigneeAdd2"] != DBNull.Value ? dr["DesigneeAdd2"].ToString() : "")),
                                            DesigneeCity = ((dr["DesigneeCity"] != DBNull.Value ? dr["DesigneeCity"].ToString() : "")),
                                            DesigneeState = ((dr["DesigneeState"] != DBNull.Value ? dr["DesigneeState"].ToString() : "")),
                                            DesigneeZip = ((dr["DesigneeZip"] != DBNull.Value ? dr["DesigneeZip"].ToString() : "")),
                                            DesigneePhNum = ((dr["DesigneePhNum"] != DBNull.Value ? dr["DesigneePhNum"].ToString() : "")),
                                            TXPrelimGrp = ((dr["TXPrelimGrp"] != DBNull.Value ? Convert.ToInt32(dr["TXPrelimGrp"]) : 0)),
                                            PrivateJob = ((dr["PrivateJob"] != DBNull.Value ? Convert.ToBoolean(dr["PrivateJob"]) : false)),

                                            StateName = ((dr["StateName"] != DBNull.Value ? dr["StateName"].ToString() : "")),
                                            LienContactTitle = ((dr["LienContactTitle"] != DBNull.Value ? dr["LienContactTitle"].ToString() : "")),
                                            LienEmail = ((dr["LienEmail"] != DBNull.Value ? dr["LienEmail"].ToString() : "")),
                                            LienAs = ((dr["LienAs"] != DBNull.Value ? dr["LienAs"].ToString() : "")),
                                            NTOAs = ((dr["NTOAs"] != DBNull.Value ? dr["NTOAs"].ToString() : "")),
                                            BranchAs = ((dr["BranchAs"] != DBNull.Value ? dr["BranchAs"].ToString() : "")),
                                            CustContact = ((dr["CustContact"] != DBNull.Value ? dr["CustContact"].ToString() : "")),
                                            SendOIR = ((dr["SendOIR"] != DBNull.Value ? Convert.ToBoolean(dr["SendOIR"]) : false)),
                                            SendAck = ((dr["SendAck"] != DBNull.Value ? Convert.ToBoolean(dr["SendAck"]) : false)),
                                            OtherLglThreshold = ((dr["OtherLglThreshold"] != DBNull.Value ? Convert.ToInt32(dr["OtherLglThreshold"]) : 0)),
                                            EmpName = ((dr["EmpName"] != DBNull.Value ? dr["EmpName"].ToString() : "")),
                                            EmpTitle = ((dr["EmpTitle"] != DBNull.Value ? dr["EmpTitle"].ToString() : "")),
                                            EmpPhNum = ((dr["EmpPhNum"] != DBNull.Value ? dr["EmpPhNum"].ToString() : "")),
                                            EmpFaxNum = ((dr["EmpFaxNum"] != DBNull.Value ? dr["EmpFaxNum"].ToString() : "")),
                                            JobStatus = ((dr["JobStatus"] != DBNull.Value ? dr["JobStatus"].ToString() : "")),
                                            JobStatusDescr = ((dr["JobStatusDescr"] != DBNull.Value ? dr["JobStatusDescr"].ToString() : "")),
                                            JobStatusType = ((dr["JobStatusType"] != DBNull.Value ? Convert.ToInt32(dr["JobStatusType"]) : 0)),
                                            DateAssigned = ((dr["DateAssigned"] != DBNull.Value ? Convert.ToDateTime(dr["DateAssigned"]) : DateTime.MinValue)),
                                            VerifiedDate = ((dr["VerifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["VerifiedDate"]) : DateTime.MinValue)),
                                            LienAssignDate = ((dr["LienAssignDate"] != DBNull.Value ? Convert.ToDateTime(dr["LienAssignDate"]) : DateTime.MinValue)),
                                            LienDate = ((dr["LienDate"] != DBNull.Value ? Convert.ToDateTime(dr["LienDate"]) : DateTime.MinValue)),
                                            StatusCode = ((dr["StatusCode"] != DBNull.Value ? dr["StatusCode"].ToString() : "")),
                                            DeskNum = ((dr["DeskNum"] != DBNull.Value ? dr["DeskNum"].ToString() : "")),
                                            FolioNum = ((dr["FolioNum"] != DBNull.Value ? dr["FolioNum"].ToString() : "")),
                                            ReleaseDocNum = ((dr["ReleaseDocNum"] != DBNull.Value ? dr["ReleaseDocNum"].ToString() : "")),
                                            PrelimBox = ((dr["PrelimBox"] != DBNull.Value ? Convert.ToBoolean(dr["PrelimBox"]) : false)),
                                            RushOrder = ((dr["RushOrder"] != DBNull.Value ? Convert.ToBoolean(dr["RushOrder"]) : false)),
                                            VerifyJob = ((dr["VerifyJob"] != DBNull.Value ? Convert.ToBoolean(dr["VerifyJob"]) : false)),
                                            PODBox = ((dr["PODBox"] != DBNull.Value ? Convert.ToBoolean(dr["PODBox"]) : false)),
                                            FederalJob = ((dr["FederalJob"] != DBNull.Value ? Convert.ToBoolean(dr["FederalJob"]) : false)),
                                            MechanicLien = ((dr["MechanicLien"] != DBNull.Value ? Convert.ToBoolean(dr["MechanicLien"]) : false)),
                                            TitleVerifiedBox = ((dr["TitleVerifiedBox"] != DBNull.Value ? Convert.ToBoolean(dr["TitleVerifiedBox"]) : false)),

                                            CustFaxNum = ((dr["CustFaxNum"] != DBNull.Value ? dr["CustFaxNum"].ToString() : "")),
                                            PONum = ((dr["PONum"] != DBNull.Value ? dr["PONum"].ToString() : "")),
                                            GCJobNum = ((dr["GCJobNum"] != DBNull.Value ? dr["GCJobNum"].ToString() : "")),
                                            GCFaxNum = ((dr["GCFaxNum"] != DBNull.Value ? dr["GCFaxNum"].ToString() : "")),
                                            GCContact = ((dr["GCContact"] != DBNull.Value ? dr["GCContact"].ToString() : "")),
                                            RevDate = ((dr["RevDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevDate"]) : DateTime.MinValue)),
                                            RushOrderML = ((dr["RushOrderML"] != DBNull.Value ? Convert.ToBoolean(dr["RushOrderML"]) : false)),
                                            NoCallCust = ((dr["NoCallCust"] != DBNull.Value ? Convert.ToBoolean(dr["NoCallCust"]) : false)),
                                            NoCallGC = ((dr["NoCallGC"] != DBNull.Value ? Convert.ToBoolean(dr["NoCallGC"]) : false)),
                                            TODeskNum = ((dr["TODeskNum"] != DBNull.Value ? dr["TODeskNum"].ToString() : "")),
                                            JobAlertBox = ((dr["JobAlertBox"] != DBNull.Value ? Convert.ToBoolean(dr["JobAlertBox"]) : false)),
                                            CanDate = ((dr["CanDate"] != DBNull.Value ? Convert.ToDateTime(dr["CanDate"]) : DateTime.MinValue)),
                                            SpecialInstruction = ((dr["SpecialInstruction"] != DBNull.Value ? dr["SpecialInstruction"].ToString() : "")),
                                            TXSub90 = ((dr["TXSub90"] != DBNull.Value ? Convert.ToBoolean(dr["TXSub90"]) : false)),
                                            TXSubSub60 = ((dr["TXSubSub60"] != DBNull.Value ? Convert.ToBoolean(dr["TXSubSub60"]) : false)),
                                            AZLotAllocate = ((dr["AZLotAllocate"] != DBNull.Value ? dr["AZLotAllocate"].ToString() : "")),
                                            RefNum = ((dr["RefNum"] != DBNull.Value ? dr["RefNum"].ToString() : "")),
                                            LienAmt = ((dr["LienAmt"] != DBNull.Value ? Convert.ToDecimal(dr["LienAmt"]) : 0)),
                                            ReleaseDate = ((dr["ReleaseDate"] != DBNull.Value ? Convert.ToDateTime(dr["ReleaseDate"]) : DateTime.MinValue)),
                                            BondDate = ((dr["BondDate"] != DBNull.Value ? Convert.ToDateTime(dr["BondDate"]) : DateTime.MinValue)),
                                            LienRelease = ((dr["LienRelease"] != DBNull.Value ? Convert.ToBoolean(dr["LienRelease"]) : false)),
                                            ProblemMsg = ((dr["ProblemMsg"] != DBNull.Value ? dr["ProblemMsg"].ToString() : "")),
                                            PaymentBond = ((dr["PaymentBond"] != DBNull.Value ? Convert.ToBoolean(dr["PaymentBond"]) : false)),
                                            ReleaseRush = ((dr["ReleaseRush"] != DBNull.Value ? Convert.ToBoolean(dr["ReleaseRush"]) : false)),
                                            MechLienRush = ((dr["MechLienRush"] != DBNull.Value ? Convert.ToBoolean(dr["MechLienRush"]) : false)),
                                            LienExtBox = ((dr["LienExtBox"] != DBNull.Value ? Convert.ToBoolean(dr["LienExtBox"]) : false)),
                                            PODDate = ((dr["PODDate"] != DBNull.Value ? Convert.ToDateTime(dr["PODDate"]) : DateTime.MinValue)),
                                            LienExtDate = ((dr["LienExtDate"] != DBNull.Value ? Convert.ToDateTime(dr["LienExtDate"]) : DateTime.MinValue)),
                                            SNDate = ((dr["SNDate"] != DBNull.Value ? Convert.ToDateTime(dr["SNDate"]) : DateTime.MinValue)),
                                            RecordingFee = ((dr["RecordingFee"] != DBNull.Value ? Convert.ToDecimal(dr["RecordingFee"]) : 0)),
                                            NOIBox = ((dr["NOIBox"] != DBNull.Value ? Convert.ToBoolean(dr["NOIBox"]) : false)),
                                            NOIDate = ((dr["NOIDate"] != DBNull.Value ? Convert.ToDateTime(dr["NOIDate"]) : DateTime.MinValue)),
                                            ProbRequest2 = ((dr["ProbRequest2"] != DBNull.Value ? Convert.ToBoolean(dr["ProbRequest2"]) : false)),
                                            ProbRequest3 = ((dr["ProbRequest3"] != DBNull.Value ? Convert.ToBoolean(dr["ProbRequest3"]) : false)),

                                            BondReleaseDate = ((dr["BondReleaseDate"] != DBNull.Value ? Convert.ToDateTime(dr["BondReleaseDate"]) : DateTime.MinValue)),
                                            CountyRecorderNum = ((dr["CountyRecorderNum"] != DBNull.Value ? dr["CountyRecorderNum"].ToString() : "")),
                                            BondReleaseBox = ((dr["BondReleaseBox"] != DBNull.Value ? Convert.ToBoolean(dr["BondReleaseBox"]) : false)),
                                            SNReleaseDate = ((dr["SNReleaseDate"] != DBNull.Value ? Convert.ToDateTime(dr["SNReleaseDate"]) : DateTime.MinValue)),
                                            SNReleaseBox = ((dr["SNReleaseBox"] != DBNull.Value ? Convert.ToBoolean(dr["SNReleaseBox"]) : false)),
                                            SameAsGC = ((dr["SameAsGC"] != DBNull.Value ? Convert.ToBoolean(dr["SameAsGC"]) : false)),
                                            SNAssignDate = ((dr["SNAssignDate"] != DBNull.Value ? Convert.ToDateTime(dr["SNAssignDate"]) : DateTime.MinValue)),
                                            BondAssignDate = ((dr["BondAssignDate"] != DBNull.Value ? Convert.ToDateTime(dr["BondAssignDate"]) : DateTime.MinValue)),
                                            SNReleaseAssign = ((dr["SNReleaseAssign"] != DBNull.Value ? Convert.ToDateTime(dr["SNReleaseAssign"]) : DateTime.MinValue)),
                                            BCReleaseAssign = ((dr["BCReleaseAssign"] != DBNull.Value ? Convert.ToDateTime(dr["BCReleaseAssign"]) : DateTime.MinValue)),
                                            LienReleaseAssign = ((dr["LienReleaseAssign"] != DBNull.Value ? Convert.ToDateTime(dr["LienReleaseAssign"]) : DateTime.MinValue)),
                                            LEAssignDate = ((dr["LEAssignDate"] != DBNull.Value ? Convert.ToDateTime(dr["LEAssignDate"]) : DateTime.MinValue)),
                                            LienDDD = ((dr["LienDDD"] != DBNull.Value ? Convert.ToDateTime(dr["LienDDD"]) : DateTime.MinValue)),
                                            BondDDD = ((dr["BondDDD"] != DBNull.Value ? Convert.ToDateTime(dr["BondDDD"]) : DateTime.MinValue)),
                                            SNDDD = ((dr["SNDDD"] != DBNull.Value ? Convert.ToDateTime(dr["SNDDD"]) : DateTime.MinValue)),
                                            NCInfo = ((dr["NCInfo"] != DBNull.Value ? Convert.ToBoolean(dr["NCInfo"]) : false)),
                                            BackupBox = ((dr["BackupBox"] != DBNull.Value ? Convert.ToBoolean(dr["BackupBox"]) : false)),
                                            SameAsOwner = ((dr["SameAsOwner"] != DBNull.Value ? Convert.ToBoolean(dr["SameAsOwner"]) : false)),
                                            NOIAssignDate = ((dr["NOIAssignDate"] != DBNull.Value ? Convert.ToDateTime(dr["NOIAssignDate"]) : DateTime.MinValue)),
                                            SendAsIsBox = ((dr["SendAsIsBox"] != DBNull.Value ? Convert.ToBoolean(dr["SendAsIsBox"]) : false)),
                                            SameAsCust = ((dr["SameAsCust"] != DBNull.Value ? Convert.ToBoolean(dr["SameAsCust"]) : false)),
                                            FATDate = ((dr["FATDate"] != DBNull.Value ? Convert.ToDateTime(dr["FATDate"]) : DateTime.MinValue)),

                                            FATHit = ((dr["FATHit"] != DBNull.Value ? Convert.ToBoolean(dr["FATHit"]) : false)),
                                            OwnerSourceFAT = ((dr["OwnerSourceFAT"] != DBNull.Value ? Convert.ToBoolean(dr["OwnerSourceFAT"]) : false)),
                                            OwnerSourceAccurint = ((dr["OwnerSourceAccurint"] != DBNull.Value ? Convert.ToBoolean(dr["OwnerSourceAccurint"]) : false)),
                                            OwnerSourceCust = ((dr["OwnerSourceCust"] != DBNull.Value ? Convert.ToBoolean(dr["OwnerSourceCust"]) : false)),
                                            OwnerSourceCustFax = ((dr["OwnerSourceCustFax"] != DBNull.Value ? Convert.ToBoolean(dr["OwnerSourceCustFax"]) : false)),
                                            OwnerSourceGC = ((dr["OwnerSourceGC"] != DBNull.Value ? Convert.ToBoolean(dr["OwnerSourceGC"]) : false)),
                                            OwnerSourceGCFax = ((dr["OwnerSourceGCFax"] != DBNull.Value ? Convert.ToBoolean(dr["OwnerSourceGCFax"]) : false)),
                                            OwnerSourceClient = ((dr["OwnerSourceClient"] != DBNull.Value ? Convert.ToBoolean(dr["OwnerSourceClient"]) : false)),
                                            ReleaseFileDate = ((dr["ReleaseFileDate"] != DBNull.Value ? Convert.ToDateTime(dr["ReleaseFileDate"]) : DateTime.MinValue)),
                                            FaxReceived = ((dr["FaxReceived"] != DBNull.Value ? Convert.ToBoolean(dr["FaxReceived"]) : false)),
                                            EmpEmail = ((dr["EmpEmail"] != DBNull.Value ? dr["EmpEmail"].ToString() : "")),
                                            NOGCCall = ((dr["NOGCCall"] != DBNull.Value ? Convert.ToBoolean(dr["NOGCCall"]) : false)),
                                            NOCustCall = ((dr["NOCustCall"] != DBNull.Value ? Convert.ToBoolean(dr["NOCustCall"]) : false)),
                                            CustId = ((dr["CustId"] != DBNull.Value ? Convert.ToInt32(dr["CustId"]) : 0)),
                                            GenId = ((dr["GenId"] != DBNull.Value ? Convert.ToInt32(dr["GenId"]) : 0)),
                                            OwnrId = ((dr["OwnrId"] != DBNull.Value ? Convert.ToInt32(dr["OwnrId"]) : 0)),
                                            LenderId = ((dr["LenderId"] != DBNull.Value ? Convert.ToInt32(dr["LenderId"]) : 0)),
                                            DesigneeId = ((dr["DesigneeId"] != DBNull.Value ? Convert.ToInt32(dr["DesigneeId"]) : 0)),

                                            CustCertNum = ((dr["CustCertNum"] != DBNull.Value ? dr["CustCertNum"].ToString() : "")),
                                            GCCertNum = ((dr["GCCertNum"] != DBNull.Value ? dr["GCCertNum"].ToString() : "")),
                                            OwnerCertNum = ((dr["OwnerCertNum"] != DBNull.Value ? dr["OwnerCertNum"].ToString() : "")),
                                            LenderCertNum = ((dr["LenderCertNum"] != DBNull.Value ? dr["LenderCertNum"].ToString() : "")),

                                            BatchId = ((dr["BatchId"] != DBNull.Value ? Convert.ToInt32(dr["BatchId"]) : 0)),
                                            SubmittedBy = ((dr["SubmittedBy"] != DBNull.Value ? dr["SubmittedBy"].ToString() : "")),
                                            LastUpdatedOn = ((dr["LastUpdatedOn"] != DBNull.Value ? Convert.ToDateTime(dr["LastUpdatedOn"]) : DateTime.MinValue)),
                                            LastUpdatedBy = ((dr["LastUpdatedBy"] != DBNull.Value ? dr["LastUpdatedBy"].ToString() : "")),
                                            LastUpdatedById = ((dr["LastUpdatedById"] != DBNull.Value ? Convert.ToInt32(dr["LastUpdatedById"]) : 0)),
                                            SubmittedOn = ((dr["SubmittedOn"] != DBNull.Value ? Convert.ToDateTime(dr["SubmittedOn"]) : DateTime.MinValue)),
                                            PlacementSource = ((dr["PlacementSource"] != DBNull.Value ? dr["PlacementSource"].ToString() : "")),
                                            IsFATExclude = ((dr["IsFATExclude"] != DBNull.Value ? Convert.ToBoolean(dr["IsFATExclude"]) : false)),
                                            ClientPhoneNO = ((dr["ClientPhoneNO"] != DBNull.Value ? dr["ClientPhoneNO"].ToString() : "")),
                                            NoAutoFaxState = ((dr["NoAutoFaxState"] != DBNull.Value ? dr["NoAutoFaxState"].ToString() : "")),
                                            ManagerReviewed = ((dr["ManagerReviewed"] != DBNull.Value ? Convert.ToBoolean(dr["ManagerReviewed"]) : false)),
                                            MultipleOwnerBox = ((dr["MultipleOwnerBox"] != DBNull.Value ? Convert.ToBoolean(dr["MultipleOwnerBox"]) : false)),
                                            RANum = ((dr["RANum"] != DBNull.Value ? dr["RANum"].ToString() : "")),
                                            SubmittedByUserId = ((dr["SubmittedByUserId"] != DBNull.Value ? Convert.ToInt32(dr["SubmittedByUserId"]) : 0)),
                                            TXApproved = ((dr["TXApproved"] != DBNull.Value ? Convert.ToBoolean(dr["TXApproved"]) : false)),
                                            OrigJobAdd1 = ((dr["OrigJobAdd1"] != DBNull.Value ? dr["OrigJobAdd1"].ToString() : "")),
                                            BuildingPermitNum = ((dr["BuildingPermitNum"] != DBNull.Value ? dr["BuildingPermitNum"].ToString() : "")),

                                            NOCDate = ((dr["NOCDate"] != DBNull.Value ? Convert.ToDateTime(dr["NOCDate"]) : DateTime.MinValue)),
                                            JobCompletionDate = ((dr["JobCompletionDate"] != DBNull.Value ? Convert.ToDateTime(dr["JobCompletionDate"]) : DateTime.MinValue)),
                                            ContractTypeId = ((dr["ContractTypeId"] != DBNull.Value ? Convert.ToInt32(dr["ContractTypeId"]) : 0)),
                                            TeamCode = ((dr["TeamCode"] != DBNull.Value ? dr["TeamCode"].ToString() : "")),
                                            IsBillableLegalDesc = ((dr["IsBillableLegalDesc"] != DBNull.Value ? Convert.ToBoolean(dr["IsBillableLegalDesc"]) : false)),
                                            LienDeadlineDate = ((dr["LienDeadlineDate"] != DBNull.Value ? Convert.ToDateTime(dr["LienDeadlineDate"]) : DateTime.MinValue)),
                                            BondDeadlineDate = ((dr["BondDeadlineDate"] != DBNull.Value ? Convert.ToDateTime(dr["BondDeadlineDate"]) : DateTime.MinValue)),
                                            SNDeadlineDate = ((dr["SNDeadlineDate"] != DBNull.Value ? Convert.ToDateTime(dr["SNDeadlineDate"]) : DateTime.MinValue)),
                                            NOIDeadlineDate = ((dr["NOIDeadlineDate"] != DBNull.Value ? Convert.ToDateTime(dr["NOIDeadlineDate"]) : DateTime.MinValue)),
                                            NOCDocNum = ((dr["NOCDocNum"] != DBNull.Value ? dr["NOCDocNum"].ToString() : "")),
                                            NOCRecDate = ((dr["NOCRecDate"] != DBNull.Value ? Convert.ToDateTime(dr["NOCRecDate"]) : DateTime.MinValue)),
                                            NOCompBox = ((dr["NOCompBox"] != DBNull.Value ? Convert.ToBoolean(dr["NOCompBox"]) : false)),
                                            RushOrderVerify = ((dr["RushOrderVerify"] != DBNull.Value ? Convert.ToBoolean(dr["RushOrderVerify"]) : false)),
                                            MaterialDescription = ((dr["MaterialDescription"] != DBNull.Value ? dr["MaterialDescription"].ToString() : "")),
                                            MaterialPrice = ((dr["MaterialPrice"] != DBNull.Value ? Convert.ToDecimal(dr["MaterialPrice"]) : 0)),
                                            LenderBondNum = ((dr["LenderBondNum"] != DBNull.Value ? dr["LenderBondNum"].ToString() : "")),
                                            StopNoticeBox = ((dr["StopNoticeBox"] != DBNull.Value ? Convert.ToBoolean(dr["StopNoticeBox"]) : false)),
                                            NoCallsCustGc = ((dr["NoCallsCustGc"] != DBNull.Value ? Convert.ToBoolean(dr["NoCallsCustGc"]) : false)),
                                            VerifyOWOnly = ((dr["VerifyOWOnly"] != DBNull.Value ? Convert.ToBoolean(dr["VerifyOWOnly"]) : false)),
                                            VerifyOWOnlySend = ((dr["VerifyOWOnlySend"] != DBNull.Value ? Convert.ToBoolean(dr["VerifyOWOnlySend"]) : false)),
                                            VerifyOWOnlyTX = ((dr["VerifyOWOnlyTX"] != DBNull.Value ? Convert.ToBoolean(dr["VerifyOWOnlyTX"]) : false)),
                                            FilterKey = ((dr["FilterKey"] != DBNull.Value ? dr["FilterKey"].ToString() : "")),
                                            IsJobView = ((dr["IsJobView"] != DBNull.Value ? Convert.ToBoolean(dr["IsJobView"]) : false)),
                                            IsNoticeRequestStatus = ((dr["IsNoticeRequestStatus"] != DBNull.Value ? Convert.ToBoolean(dr["IsNoticeRequestStatus"]) : false)),
                                            ForeclosureDeadlineDate = ((dr["ForeclosureDeadlineDate"] != DBNull.Value ? Convert.ToDateTime(dr["ForeclosureDeadlineDate"]) : DateTime.MinValue)),
                                            BondSuitDeadlineDate = ((dr["BondSuitDeadlineDate"] != DBNull.Value ? Convert.ToDateTime(dr["BondSuitDeadlineDate"]) : DateTime.MinValue)),
                                            BondSuitDate = ((dr["BondSuitDate"] != DBNull.Value ? Convert.ToDateTime(dr["BondSuitDate"]) : DateTime.MinValue)),

                                            JobContactName = ((dr["JobContactName"] != DBNull.Value ? dr["JobContactName"].ToString() : "")),
                                            JobContactPhoneNo = ((dr["JobContactPhoneNo"] != DBNull.Value ? dr["JobContactPhoneNo"].ToString() : "")),
                                            JobBalance = ((dr["JobBalance"] != DBNull.Value ? Convert.ToDecimal(dr["JobBalance"]) : 0)),
                                            JVStatusCode = ((dr["JVStatusCode"] != DBNull.Value ? dr["JVStatusCode"].ToString() : "")),




                                        }).ToList();


            return vwJob.First();
        }
        catch (Exception ex)
            {
                return vwJob.FirstOrDefault();
            }

}

        public static IEnumerable<SelectListItem> GetBranchInfo(string JobID)
        {
            List<ADClient> vwBranch = new List<ADClient>();
            try
            {
                var myacc = new Job();
                ITable IAcc = myacc;
                DBO.Provider.Read(JobID, ref IAcc);
                myacc = (Job)IAcc;
                var myds = new DataSet();
                var mysproc = new uspbo_Jobs_GetJobInfoforVerifier();
                mysproc.JobId = JobID;
                myds = DBO.Provider.GetDataSet(mysproc);
                //DataTable dt = myds.Tables[0];
               // JobVM Obj_Job = new JobVM();
               // Obj_Job.ObjvwJobList = GetJobInfo(dt);

                //DataTable dtClient = myds.Tables[6];
                //Obj_Job.obj_ClientEdit = ADClient.GetClient(dtClient);

                //DataTable dtClientLienInfo = myds.Tables[7];
                //Obj_Job.obj_ClientLienInfo = ADClientLienInfo.GetClientLienInfo(dtClientLienInfo);

                DataTable dttable = myds.Tables[18];

                vwBranch = (from DataRow dr in dttable.Rows
                         select new ADClient()
                         {
                             ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
                             //JobId = ((dr["JobId"] != DBNull.Value ? Convert.ToInt32(dr["JobId"]) : 0)),
                             //BranchNumber = ((dr["BranchNumber"] != DBNull.Value ? dr["BranchNumber"].ToString() : "")),
                             //ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                             ClientName = ((dr["ClientName"] != DBNull.Value ? dr["ClientName"].ToString() : "")),
                           


                         }).ToList();


                return new SelectList(vwBranch, "ClientId", "ClientName");
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<SelectListItem>());
            }

        }

        public static int UpdateBranchNumber(string JobID,string BranchNumber)
        {
           
            var myacc = new Job();
          
            ITable IAcc = myacc;
            DBO.Provider.Read(JobID, ref IAcc);
            myacc.BranchNum = BranchNumber;
            IAcc = myacc;
            DBO.Provider.Update(ref IAcc);

            return 1;
           
        }


        #endregion
    }
}


