﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard
{
   public class ADvwJobNoticeRequestHistory
    {
        #region Properties

        public int JobId { get; set; }
        public string CertNum { get; set; }
        public string JobName { get; set; }
        public string JobNum { get; set; }
        public DateTime DateRequested { get; set; }
        public DateTime DatePrinted { get; set; }
        public DateTime DateInvoiced { get; set; }
        public string TypeCode { get; set; }
        public string LegalPartyType { get; set; }
        public string LegalPartyName { get; set; }
        public int JobNoticeHistoryId { get; set; }
        public int JobNoticeRequestId { get; set; }
        public string PrintGroup { get; set; }
        public string FormCode { get; set; }
        public bool IsRequestPrinted { get; set; }
        public bool IsInvoiced { get; set; }
        public int BatchId { get; set; }
        public bool IsTXNotice { get; set; }
        public bool IsTX60Day { get; set; }
        public bool IsTX90Day { get; set; }
        public string SubmittedBy { get; set; }
        public string PrintMajorGroup { get; set; }
        public bool HasErrors { get; set; }
        public string ErrorDescr { get; set; }
        public bool IsNoticePrinted { get; set; }
        public string HistoryDisplayOrder { get; set; }
        public bool IsPrimary { get; set; }
        public string ClientCode { get; set; }
        public string BranchNumber { get; set; }
        public string MainClientCode { get; set; }
        public int ClientId { get; set; }
        public bool IsRegularPostage { get; set; }
        public bool IsCertifiedMail { get; set; }
        public string PostageType { get; set; }
        public decimal PostageRate { get; set; }
        public string CoverName { get; set; }
        public string CoverAdd1 { get; set; }
        public string CoverAdd2 { get; set; }
        public string CoverCity { get; set; }
        public string CoverState { get; set; }
        public string CoverZip { get; set; }
        public bool Greencard { get; set; }
        public string StmtOfAccts { get; set; }
        public bool PrintMailingLog { get; set; }
        public bool IsMailingLogPostage { get; set; }
        public string BarCode { get; set; }
        public bool IsFreeBilling { get; set; }
        public string JobState { get; set; }
        public bool MLAgent { get; set; }
        public bool RushOrder { get; set; }
        public string MailStatus { get; set; }
        public DateTime MailStatusDate { get; set; }
        public bool IsNonUSAddr { get; set; }
        public DateTime DeliveredDate { get; set; }

        #endregion

        #region CRUD
        public static List<ADvwJobNoticeRequestHistory> GetvwJobNoticeRequestHistoryList(DataTable dt)
        {
            try
            {
                List<ADvwJobNoticeRequestHistory> list = (from DataRow dr in dt.Rows
                                                          select new ADvwJobNoticeRequestHistory()
                                                          {
                                                              JobNoticeHistoryId = ((dr["JobNoticeHistoryId"] != DBNull.Value ? Convert.ToInt32(dr["JobNoticeHistoryId"]) : 0)),
                                                              DateRequested = ((dr["DateRequested"] != DBNull.Value ? Convert.ToDateTime(dr["DateRequested"]) : DateTime.MinValue)),
                                                              DatePrinted = ((dr["DatePrinted"] != DBNull.Value ? Convert.ToDateTime(dr["DatePrinted"]) : DateTime.MinValue)),
                                                              FormCode = ((dr["FormCode"] != DBNull.Value ? (dr["FormCode"]).ToString() : "")),
                                                              LegalPartyType = ((dr["LegalPartyType"] != DBNull.Value ? (dr["LegalPartyType"]).ToString() : "")),
                                                              LegalPartyName = ((dr["LegalPartyName"] != DBNull.Value ? (dr["LegalPartyName"]).ToString() : "")),
                                                              CertNum = ((dr["CertNum"] != DBNull.Value ? (dr["CertNum"]).ToString() : "")),
                                                              MailStatus = ((dr["MailStatus"] != DBNull.Value ? dr["MailStatus"].ToString() : "")),
                                                              MailStatusDate = ((dr["MailStatusDate"] != DBNull.Value ? Convert.ToDateTime(dr["MailStatusDate"]) : DateTime.MinValue)),
                                                              DeliveredDate = ((dr["DeliveredDate"] != DBNull.Value ? Convert.ToDateTime(dr["DeliveredDate"]) : DateTime.MinValue)),
                                                              IsFreeBilling = ((dr["IsFreeBilling"] != DBNull.Value ? Convert.ToBoolean(dr["IsFreeBilling"]) : false)),
                                                              //RushOrder = ((dr["RushOrder"] != DBNull.Value ? Convert.ToBoolean(dr["RushOrder"]) : false)),
                                                              
                                                          }).ToList();
                return list;
            }
            catch(Exception ex)
            {
                return (new List<ADvwJobNoticeRequestHistory>());

            }
        }
        #endregion
    }
}
