﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using CRFView.Adapters.Liens.Managements.ChangeClient.ViewModels;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace CRFView.Adapters
{
    public class ADvwJobsList
    {
        #region Properties

        public int ClientId { get; set; }
        public int JobId { get; set; }
        public string BranchNumber { get; set; }
        public string BranchNum { get; set; }
        public string ClientCode { get; set; }
        public string ClientName { get; set; }
        public string JobNum { get; set; }
        public string JobName { get; set; }
        public string JobAdd1 { get; set; }
        public string JobAdd2 { get; set; }
        public string JobCity { get; set; }
        public string JobState { get; set; }
        public string JobZip { get; set; }
        public string JobCounty { get; set; }
        public string LaborType { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime PrelimDate { get; set; }
        public string EquipRental { get; set; }
        public bool PublicJob { get; set; }
        public bool JointCk { get; set; }
        public bool SuretyBox { get; set; }
        public decimal EstBalance { get; set; }
        public DateTime CurrDate { get; set; }
        public string EquipRate { get; set; }
        public bool UseBranchNTO { get; set; }
        public bool ResidentialBox { get; set; }
        public bool IsRentalCo { get; set; }
        public string Note { get; set; }
        public bool ResendBox { get; set; }
        public bool AmendedBox { get; set; }
        public bool ResendOwner { get; set; }
        public bool ResendGC { get; set; }
        public bool ResendCust { get; set; }
        public bool ResendLender { get; set; }
        public bool CustSend { get; set; }
        public DateTime NoticeSent { get; set; }
        public DateTime ResendDate { get; set; }
        public DateTime AmendedDate { get; set; }
        public string CustJobNum { get; set; }
        public string NoticePhNum { get; set; }
        public string BondNum { get; set; }
        public string APNNum { get; set; }
        public DateTime EndDate { get; set; }
        public string ContactName { get; set; }
        public bool AZCert { get; set; }
        public bool FLCertBox { get; set; }
        public bool SendNTOList { get; set; }
        public bool SendNTO { get; set; }
        public bool GreenCard { get; set; }
        public int Age { get; set; }
        public string LienContact { get; set; }
        public string LienAdd1 { get; set; }
        public string LienAdd2 { get; set; }
        public string LienCity { get; set; }
        public string LienState { get; set; }
        public string LienZip { get; set; }
        public decimal LegalInterest { get; set; }
        public string RecorderNum { get; set; }
        public DateTime FileDate { get; set; }
        public string BookNum { get; set; }
        public string PageNum { get; set; }
        public string LienPhone { get; set; }
        public string LienFax { get; set; }
        public string PrelimReq { get; set; }
        public string PrelimTime { get; set; }
        public string PrelimInfo { get; set; }
        public string MechTime { get; set; }
        public string MechPeriod { get; set; }
        public string MechServe { get; set; }
        public string MechInfo { get; set; }
        public string StopNotice { get; set; }
        public string StopNoticeInfo { get; set; }
        public string MiscInfo { get; set; }
        public bool OwnerServBox { get; set; }
        public bool GCServBox { get; set; }
        public bool LenderServBox { get; set; }
        public bool CustServBox { get; set; }
        public bool CourtServeBox { get; set; }
        public bool EstBalanceBox { get; set; }
        public bool NOIStateBox { get; set; }
        public bool ResidentialRulesBox { get; set; }
        public bool WebLien { get; set; }
        public bool WebNTO { get; set; }
        public bool NOCBox { get; set; }
        public string NTODays { get; set; }
        public string LienDays { get; set; }
        public string LenderName { get; set; }
        public string LenderAdd1 { get; set; }
        public string LenderAdd2 { get; set; }
        public string LenderCity { get; set; }
        public string LenderState { get; set; }
        public string LenderZip { get; set; }
        public string LenderPhone { get; set; }
        public string LenderFax { get; set; }
        public string GCNum { get; set; }
        public string GCName { get; set; }
        public string GCAdd1 { get; set; }
        public string GCAdd2 { get; set; }
        public string GCCity { get; set; }
        public string GCState { get; set; }
        public string GCZip { get; set; }
        public string GCPhNum { get; set; }
        public string CustRefNum { get; set; }
        public string CustName { get; set; }
        public string CustAdd1 { get; set; }
        public string CustAdd2 { get; set; }
        public string CustCity { get; set; }
        public string CustState { get; set; }
        public string CustZip { get; set; }
        public string CustPhNUm { get; set; }
        public string OwnerName { get; set; }
        public string OwnerName1 { get; set; }
        public string OwnerAdd1 { get; set; }
        public string OwnerAdd2 { get; set; }
        public string OwnerCity { get; set; }
        public string OwnerState { get; set; }
        public string OwnerZip { get; set; }
        public string OwnerPhNum { get; set; }
        public string OwnerFax { get; set; }
        public string NTOContact { get; set; }
        public string NTOContactTitle { get; set; }
        public string NTOAdd1 { get; set; }
        public string NTOAdd2 { get; set; }
        public string NTOCity { get; set; }
        public string NTOState { get; set; }
        public string NTOZip { get; set; }
        public string NTOEmail { get; set; }
        public string NTOPhone { get; set; }
        public string NTOFax { get; set; }
        public string DesigneeName { get; set; }
        public string DesigneeAdd1 { get; set; }
        public string DesigneeAdd2 { get; set; }
        public string DesigneeCity { get; set; }
        public string DesigneeState { get; set; }
        public string DesigneeZip { get; set; }
        public string DesigneePhNum { get; set; }
        public int TXPrelimGrp { get; set; }
        public bool PrivateJob { get; set; }
        public string StateName { get; set; }
        public string LienContactTitle { get; set; }
        public string LienEmail { get; set; }
        public string LienAs { get; set; }
        public string NTOAs { get; set; }
        public string BranchAs { get; set; }
        public string CustContact { get; set; }
        public bool SendOIR { get; set; }
        public bool SendAck { get; set; }
        public int OtherLglThreshold { get; set; }
        public string EmpName { get; set; }
        public string EmpTitle { get; set; }
        public string EmpPhNum { get; set; }
        public string EmpFaxNum { get; set; }
        public string JobStatus { get; set; }
        public string JobStatusDescr { get; set; }
        public int JobStatusType { get; set; }
        public DateTime DateAssigned { get; set; }
        public DateTime VerifiedDate { get; set; }
        public DateTime LienAssignDate { get; set; }
        public DateTime LienDate { get; set; }
        public string StatusCode { get; set; }
        public string DeskNum { get; set; }
        public string FolioNum { get; set; }
        public string ReleaseDocNum { get; set; }
        public bool PrelimBox { get; set; }
        public bool RushOrder { get; set; }
        public bool VerifyJob { get; set; }
        public bool PODBox { get; set; }
        public bool FederalJob { get; set; }
        public bool MechanicLien { get; set; }
        public bool TitleVerifiedBox { get; set; }
        public string CustFaxNum { get; set; }
        public string PONum { get; set; }
        public string GCJobNum { get; set; }
        public string GCFaxNum { get; set; }
        public string GCContact { get; set; }
        public DateTime RevDate { get; set; }
        public bool RushOrderML { get; set; }
        public bool NoCallCust { get; set; }
        public bool NoCallGC { get; set; }
        public string TODeskNum { get; set; }
        public bool JobAlertBox { get; set; }
        public DateTime CanDate { get; set; }
        public string SpecialInstruction { get; set; }
        public bool TXSub90 { get; set; }
        public bool TXSubSub60 { get; set; }
        public string AZLotAllocate { get; set; }
        public string RefNum { get; set; }
        public decimal LienAmt { get; set; }
        public DateTime ReleaseDate { get; set; }
        public DateTime BondDate { get; set; }
        public bool LienRelease { get; set; }
        public string ProblemMsg { get; set; }
        public bool PaymentBond { get; set; }
        public bool ReleaseRush { get; set; }
        public bool MechLienRush { get; set; }
        public bool LienExtBox { get; set; }
        public DateTime PODDate { get; set; }
        public DateTime LienExtDate { get; set; }
        public DateTime SNDate { get; set; }
        public decimal RecordingFee { get; set; }
        public bool NOIBox { get; set; }
        public DateTime NOIDate { get; set; }
        public bool ProbRequest2 { get; set; }
        public bool ProbRequest3 { get; set; }
        public DateTime BondReleaseDate { get; set; }
        public string CountyRecorderNum { get; set; }
        public bool BondReleaseBox { get; set; }
        public DateTime SNReleaseDate { get; set; }
        public bool SNReleaseBox { get; set; }
        public bool SameAsGC { get; set; }
        public DateTime SNAssignDate { get; set; }
        public DateTime BondAssignDate { get; set; }
        public DateTime SNReleaseAssign { get; set; }
        public DateTime BCReleaseAssign { get; set; }
        public DateTime LienReleaseAssign { get; set; }
        public DateTime LEAssignDate { get; set; }
        public DateTime LienDDD { get; set; }
        public DateTime BondDDD { get; set; }
        public DateTime SNDDD { get; set; }
        public bool NCInfo { get; set; }
        public bool BackupBox { get; set; }
        public bool SameAsOwner { get; set; }
        public DateTime NOIAssignDate { get; set; }
        public bool SendAsIsBox { get; set; }
        public bool SameAsCust { get; set; }
        public DateTime FATDate { get; set; }
        public bool FATHit { get; set; }
        public bool OwnerSourceFAT { get; set; }
        public bool OwnerSourceAccurint { get; set; }
        public bool OwnerSourceCust { get; set; }
        public bool OwnerSourceCustFax { get; set; }
        public bool OwnerSourceGC { get; set; }
        public bool OwnerSourceGCFax { get; set; }
        public bool OwnerSourceAccessor { get; set; }
        public bool OwnerSourceClient { get; set; }
        public DateTime ReleaseFileDate { get; set; }
        public bool FaxReceived { get; set; }
        public string EmpEmail { get; set; }
        public bool NOGCCall { get; set; }
        public bool NOCustCall { get; set; }
        public int CustId { get; set; }
        public int GenId { get; set; }
        public int OwnrId { get; set; }
        public int LenderId { get; set; }
        public int DesigneeId { get; set; }
        public string CustCertNum { get; set; }
        public string GCCertNum { get; set; }
        public string OwnerCertNum { get; set; }
        public string LenderCertNum { get; set; }
        public int BatchId { get; set; }
        public string SubmittedBy { get; set; }
        public DateTime LastUpdatedOn { get; set; }
        public string LastUpdatedBy { get; set; }
        public int LastUpdatedById { get; set; }
        public DateTime SubmittedOn { get; set; }
        public string PlacementSource { get; set; }
        public bool IsFATExclude { get; set; }
        public string ClientPhoneNO { get; set; }
        public string NoAutoFaxState { get; set; }
        public bool ManagerReviewed { get; set; }
        public bool MultipleOwnerBox { get; set; }
        public string RANum { get; set; }
        public int SubmittedByUserId { get; set; }
        public bool TXApproved { get; set; }
        public string OrigJobAdd1 { get; set; }
        public string BuildingPermitNum { get; set; }
        public DateTime NOCDate { get; set; }
        public DateTime JobCompletionDate { get; set; }
        public int ContractTypeId { get; set; }
        public string TeamCode { get; set; }
        public bool IsBillableLegalDesc { get; set; }
        public DateTime LienDeadlineDate { get; set; }
        public DateTime BondDeadlineDate { get; set; }
        public DateTime SNDeadlineDate { get; set; }
        public DateTime NOIDeadlineDate { get; set; }
        public string NOCDocNum { get; set; }
        public DateTime NOCRecDate { get; set; }
        public bool NOCompBox { get; set; }
        public bool RushOrderVerify { get; set; }
        public string MaterialDescription { get; set; }
        public decimal MaterialPrice { get; set; }
        public string LenderBondNum { get; set; }
        public bool StopNoticeBox { get; set; }
        public bool NoCallsCustGc { get; set; }
        public bool VerifyOWOnly { get; set; }
        public bool VerifyOWOnlySend { get; set; }
        public bool VerifyOWOnlyTX { get; set; }
        public string FilterKey { get; set; }
        public bool IsJobView { get; set; }
        public bool IsNoticeRequestStatus { get; set; }
        public DateTime ForeclosureDeadlineDate { get; set; }
        public DateTime BondSuitDeadlineDate { get; set; }
        public DateTime ForeclosureDate { get; set; }
        public DateTime BondSuitDate { get; set; }
        public string JobContactName { get; set; }
        public string JobContactPhoneNo { get; set; }
        public decimal JobBalance { get; set; }
        public string JVStatusCode { get; set; }
        public DateTime NoticeDeadlineDate { get; set; }
        public bool NoNTO { get; set; }
        public bool IsGCUnverified { get; set; }
        public bool Document { get; set; }





        //----- Job List Show Property
        public string TitleDeskNum { get; set; }
        public System.DateTime CreateDate { get; set; }
        public string GCRefNum { get; set; }
        public string GeneralContractor { get; set; }
        public string OwnrRefNuM { get; set; }
        public string OwnrName { get; set; }
        public string LenderRefNum { get; set; }
        public DateTime LastNoticePrintDate { get; set; }
        public string ClientCustomer { get; set; }
        public string ContactTitle { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string ContactSalutation { get; set; }
        public int SalesTeamId { get; set; }
        public string MatchingKey { get; set; }
        public string LienContactName { get; set; }
        public string LienContactEmail { get; set; }
        public int StatusCodeId { get; set; }
        public string CustFax { get; set; }
        public string GCFax { get; set; }      
        public string StatusGroupDescription { get; set; }
        public int StatusGroup { get; set; }
        public bool NoWebNote { get; set; }
        public string CollectorName { get; set; }
        public string CollectorPhoneNo { get; set; }
        public string CustEmail { get; set; }
        public string GCEmail { get; set; }
        public bool IsReviewNote { get; set; }


        //-- Custom Property 
        public string JobType { get; set; }
        public string PropertyType { get; set; }
        public string RbtJobType { get; set; }
 		public string ClientIds { get; set; }

        








        #endregion



        public static List<ADvwJobsList> GetInitialList(string sortBy)
        {
            try
            {

                TableView EntityList;
                var Sp_Obj = new uspbo_Jobs_GetJobList();
                //Sp_Obj.JobId = null;
                Sp_Obj.JobId = "-1";
                Sp_Obj.SortColType = sortBy;

                EntityList = DBO.Provider.GetTableView(Sp_Obj);
                //To  make JobId Ascending  order
                //EntityList.Sort = "JobId ASC";
                DataTable dt = EntityList.ToTable();
               

                List<ADvwJobsList> JobList = (from DataRow dr in dt.Rows
                                             select new ADvwJobsList()
                                             {
                                                 JobId = Convert.ToInt32(dr["JobId"]),

                                                 ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                                 StatusCode = ((dr["StatusCode"] != DBNull.Value ? dr["StatusCode"].ToString() : "")),
                                                 JobName = ((dr["JobName"] != DBNull.Value ? dr["JobName"].ToString() : "")),
                                                 JobNum = ((dr["JobNum"] != DBNull.Value ? dr["JobNum"].ToString() : "")),
                                                 JobAdd1 = ((dr["JobAdd1"] != DBNull.Value ? dr["JobAdd1"].ToString() : "")),
                                                 JobCity = ((dr["JobCity"] != DBNull.Value ? dr["JobCity"].ToString() : "")),
                                                 JobState = ((dr["JobState"] != DBNull.Value ? dr["JobState"].ToString() : "")),
                                                 JobCounty = ((dr["JobCounty"] != DBNull.Value ? dr["JobCounty"].ToString() : "")),
                                                 CustRefNum = ((dr["CustRefNum"] != DBNull.Value ? dr["CustRefNum"].ToString() : "")),
                                                 ClientCustomer = ((dr["ClientCustomer"] != DBNull.Value ? dr["ClientCustomer"].ToString() : "")),
                                                 GeneralContractor = ((dr["GeneralContractor"] != DBNull.Value ? dr["GeneralContractor"].ToString() : "")),
                                                 OwnrName = ((dr["OwnrName"] != DBNull.Value ? dr["OwnrName"].ToString() : "")),
                                                 EstBalance = dr["EstBalance"] != DBNull.Value ? Convert.ToDecimal(dr["EstBalance"]) : 0,
                                                 JobBalance = dr["JobBalance"] != DBNull.Value ? Convert.ToDecimal(dr["JobBalance"]) : 0,
                                                 DeskNum = ((dr["DeskNum"] != DBNull.Value ? dr["DeskNum"].ToString() : "")),
                                                 CustId = ((dr["CustId"] != DBNull.Value ? Convert.ToInt32(dr["CustId"]) : 0)),
                                                 CustFax = ((dr["CustFax"] != DBNull.Value ? dr["CustFax"].ToString() : "")),
                                                 CustEmail = ((dr["CustEmail"] != DBNull.Value ? dr["CustEmail"].ToString() : "")),
                                                 GCFax = ((dr["GCFax"] != DBNull.Value ? dr["GCFax"].ToString() : "")),
                                                 GCEmail = ((dr["GCEmail"] != DBNull.Value ? dr["GCEmail"].ToString() : "")),
                                                 DateAssigned = ((dr["DateAssigned"] != DBNull.Value ? Convert.ToDateTime(dr["DateAssigned"]) : DateTime.MinValue)),
                                                 StartDate = ((dr["StartDate"] != DBNull.Value ? Convert.ToDateTime(dr["StartDate"]) : DateTime.MinValue)),
                                                 RevDate = ((dr["RevDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevDate"]) : DateTime.MinValue)),
                                                 NoticeSent = ((dr["NoticeSent"] != DBNull.Value ? Convert.ToDateTime(dr["NoticeSent"]) : DateTime.MinValue)),
                                                 VerifiedDate = ((dr["VerifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["VerifiedDate"]) : DateTime.MinValue)),
                                                 Document = ((dr["Document"] != DBNull.Value ? Convert.ToBoolean(dr["Document"]) : false)),
                                                 JVStatusCode = ((dr["JVStatusCode"] != DBNull.Value ? Convert.ToString(dr["JVStatusCode"]) : "")),
                                                 BranchNum = ((dr["BranchNum"] != DBNull.Value ? dr["BranchNum"].ToString() : "")),
                                                 JobStatusDescr = ((dr["JobStatusDescr"] != DBNull.Value ? dr["JobStatusDescr"].ToString() : "")),
                                                 NoticeDeadlineDate = ((dr["NoticeDeadlineDate"] != DBNull.Value ? Convert.ToDateTime(dr["NoticeDeadlineDate"]) : DateTime.MinValue)),
                                                 PropertyType = GetPropertyType(Convert.ToBoolean(dr["PrivateJob"]), Convert.ToBoolean(dr["FederalJob"]), Convert.ToBoolean(dr["ResidentialBox"]), Convert.ToBoolean(dr["PublicJob"]))

                                             }).ToList();
                return JobList;
            }
            catch (Exception ex)
            {
                return (new List<ADvwJobsList>());
            }
        }




        public static List<ADvwJobsList> GetMatchedList(string JobId)
        {
            try
            {
                uspbo_Jobs_GetJobListMatches sp_obj = new uspbo_Jobs_GetJobListMatches();
                sp_obj.JobId = JobId;

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sp_obj);
                DataTable dt = EntityList.ToTable();

                return (GetList(dt));
            }
            catch (Exception ex)
            {
                return (new List<ADvwJobsList>());
            }
        }

        public static List<ADvwJobsList> GetFilteredList(Adapters.Liens.Verifiers.WorkCard.Filter ObjFilter, string sortBy)
        {
            try
            {
                TableView EntityList;
                DataTable dt = null;
                //DBO.GetTableView(mysearchinfo)

                if (ObjFilter.IsWebNoteList == true)
                {

                    uspbo_Jobs_GetJobListWithWebNotes mysproc = new uspbo_Jobs_GetJobListWithWebNotes();
                    mysproc.SortColType = sortBy;
                    EntityList = DBO.Provider.GetTableView(mysproc);
                    //To  make JobId Ascending  order
                    //EntityList.Sort = "JobId ASC";
                    dt = EntityList.ToTable();
                    return (GetWebNotesList(dt));
                }

                if (ObjFilter.IsJVWebNoteList == true)
                {

                    uspbo_Jobs_GetJobListWithWebNotes mysproc = new uspbo_Jobs_GetJobListWithWebNotes();
                    mysproc.IsJobView = true;
                    mysproc.SortColType = sortBy;
                    EntityList = DBO.Provider.GetTableView(mysproc);
                    //To  make JobId Ascending  order
                    //EntityList.Sort = "JobId ASC";
                    dt = EntityList.ToTable();
                    return (GetWebNotesList(dt));
                }

                if (ObjFilter.IsTXReviewList == true)
                {
                    uspbo_Jobs_GetMonthlyJobListJV Mysproc = new uspbo_Jobs_GetMonthlyJobListJV();
                    //var EntityList = DBO.Provider.GetTableView(mysproc);

                    if (ObjFilter.ClientCode != null && ObjFilter.ClientCode.Length > 0)
                    {
                        Mysproc.ClientCode = ObjFilter.ClientCode.Trim();
                    }
                    Mysproc.JobState = "TX";
                    Mysproc.SortColType = sortBy;
                    EntityList = DBO.Provider.GetTableView(Mysproc);
                    //To  make JobId Ascending  order
                   // EntityList.Sort = "JobId ASC";
                    dt = EntityList.ToTable();
                    return (GetMonthlyJobListJVList(dt));
                }

                //uspbo_Jobs_GetJobList sp_obj = new uspbo_Jobs_GetJobList();
                var sp_obj = new uspbo_Jobs_GetJobList();
                if (string.IsNullOrEmpty(ObjFilter.JobId))
                {
                    sp_obj.JobId = "-1";
                }
                else
                {
                    sp_obj.JobId = ObjFilter.JobId;
                }
                #region filter Options

                if (ObjFilter.JobName != null && ObjFilter.JobName.Length > 0)
                {
                    if (ObjFilter.chkWildCardName == true)
                    {
                        sp_obj.JobName = "%" + ObjFilter.JobName.Trim() + "%";
                    }
                    else
                    {
                        sp_obj.JobName = ObjFilter.JobName.Trim() + "%";
                    }
                    sp_obj.JobId = "";
                }

                if (ObjFilter.JobAdd1 != null && ObjFilter.JobAdd1.Length > 0)
                {
                    if (ObjFilter.chkWildcardAddr == true)
                    {
                        sp_obj.JobAdd1 = "%" + ObjFilter.JobAdd1.Trim() + "%";
                    }
                    else
                    {
                        sp_obj.JobAdd1 = ObjFilter.JobAdd1.Trim() + "%";
                    }
                    sp_obj.JobId = "";
                }
                if (ObjFilter.JobNum != null && ObjFilter.JobNum.Length > 0)
                {
                    sp_obj.JobNumber = ObjFilter.JobNum.Trim() + "%";
                    sp_obj.JobId = "";
                }
                if (ObjFilter.JobState != null && ObjFilter.JobState.Length > 0)
                {
                    sp_obj.JobState = ObjFilter.JobState.Trim();
                    sp_obj.JobId = "";
                }
                if (ObjFilter.JobCity != null && ObjFilter.JobCity.Length > 0)
                {
                    sp_obj.JobCity = "%" + ObjFilter.JobCity.Trim() + "%";
                    sp_obj.JobId = "";
                }
                if (ObjFilter.JobCounty != null && ObjFilter.JobCounty.Length > 0)
                {
                    sp_obj.JobCounty = "%" + ObjFilter.JobCounty.Trim() + "%";
                    sp_obj.JobId = "";
                }
                if (ObjFilter.ClientCode != null && ObjFilter.ClientCode.Length > 0)
                {
                    if (ObjFilter.chkWildcard == true)
                    {
                        sp_obj.ClientCode = "%" + ObjFilter.ClientCode.Trim() + "%";
                    }
                    else
                    {
                        sp_obj.ClientCode = ObjFilter.ClientCode.Trim();
                    }
                    sp_obj.JobId = "";
                }
                if (ObjFilter.BranchNumber != null && ObjFilter.BranchNumber.Length > 0)
                {
                    sp_obj.BRANCHNUMBER = ObjFilter.BranchNumber.Trim() + "%";
                    sp_obj.JobId = "";
                }
                if (ObjFilter.Customer != null && ObjFilter.Customer.Length > 0)
                {
                    if (ObjFilter.chkWildcardCust == true)
                    {
                        sp_obj.ClientCustomer = "%" + ObjFilter.Customer.Trim() + "%";
                    }
                    else
                    {
                        sp_obj.ClientCustomer = ObjFilter.Customer.Trim() + "%";
                    }
                    sp_obj.JobId = "";
                }
                if (ObjFilter.CustomerRef != null && ObjFilter.CustomerRef.Length > 0)
                {
                    sp_obj.CustRefNum = ObjFilter.CustomerRef.Trim();
                    sp_obj.JobId = "";
                }
                if (ObjFilter.GeneralContractor != null && ObjFilter.GeneralContractor.Length > 0)
                {
                    if (ObjFilter.chkWildcardGC == true)
                    {
                        sp_obj.GeneralContractor = "%" + ObjFilter.GeneralContractor.Trim() + "%";
                    }
                    else
                    {
                        sp_obj.GeneralContractor = ObjFilter.GeneralContractor.Trim() + "%";
                    }
                    sp_obj.JobId = "";
                }
                if (ObjFilter.PropertyOwner != null && ObjFilter.PropertyOwner.Length > 0)
                {
                    if (ObjFilter.chkWildcardOwner == true)
                    {
                        sp_obj.PropOwner = "%" + ObjFilter.PropertyOwner.Trim() + "%";
                    }
                    else
                    {
                        sp_obj.PropOwner = ObjFilter.PropertyOwner.Trim() + "%";
                    }
                    sp_obj.JobId = "";
                }
                if (ObjFilter.LenderName != null && ObjFilter.LenderName.Length > 0)
                {
                    if (ObjFilter.chkWildcardLender == true)
                    {
                        sp_obj.LenderName = "%" + ObjFilter.LenderName.Trim() + "%";
                    }
                    else
                    {
                        sp_obj.LenderName = ObjFilter.LenderName.Trim() + "%";
                    }
                    sp_obj.JobId = "";
                }
                if (ObjFilter.BondNum != null && ObjFilter.BondNum.Length > 0)
                {
                    sp_obj.BondNum = ObjFilter.BondNum.Trim() + "%";
                    sp_obj.JobId = "";
                }
                if (ObjFilter.StatusCode != null && ObjFilter.StatusCode.Length > 0)
                {
                    sp_obj.StatusCode = ObjFilter.StatusCode.Trim();
                    sp_obj.JobId = "";
                }
                if (ObjFilter.DeskNum != null && ObjFilter.DeskNum.Length > 0)
                {
                    sp_obj.DESKNUM = ObjFilter.DeskNum.Trim();
                    sp_obj.JobId = "";
                }
                if (ObjFilter.chkIncludeClosed == true)
                {
                    sp_obj.IncludeClosed = Convert.ToBoolean(1);
                    sp_obj.JobId = "";
                }
                if (ObjFilter.TODeskNum != null && ObjFilter.TODeskNum.Length > 0)
                {
                    sp_obj.TODESKNUM = ObjFilter.TODeskNum.Trim();
                    sp_obj.JobId = "";
                }
                if (ObjFilter.chkReviewNotes == true)
                {
                    sp_obj.IsReviewNote = Convert.ToBoolean(1);
                    sp_obj.JobId = "";
                }
                if (ObjFilter.EstBalance != null && ObjFilter.EstBalance.Length > 0)
                {
                    sp_obj.EstBalance = ObjFilter.EstBalance.Trim();
                    sp_obj.JobId = "";
                }

                if (string.IsNullOrEmpty(Convert.ToString(ObjFilter.DateAssigned)) == false && Convert.ToDateTime(ObjFilter.DateAssigned) != DateTime.MinValue)
                {
                    sp_obj.AssignDate = ObjFilter.DateAssigned.ToString("yyyy-MM-dd");
                    sp_obj.JobId = "";
                    //.NOIDate = VM.NOIDate.ToString("yyyy-MM-dd");
                }
                if (ObjFilter.chkReviewDate == true)
                {
                    sp_obj.ReviewDate = ObjFilter.ReviewDate.ToString("yyyy-MM-dd");
                    sp_obj.JobId = "";
                }
                if (ObjFilter.chkStatusGroup == true)
                {
                    sp_obj.StatusGroupId = ObjFilter.StatusGroup;
                    sp_obj.JobId = "";
                }
                if (ObjFilter.chkPropertyType == true)
                {
                    sp_obj.PropertyType = ObjFilter.PropertyType;
                    sp_obj.JobId = "";
                }


                if (ObjFilter.chkPendingNotices == true)
                {
                    sp_obj.IsPrelimRequests = Convert.ToBoolean(1);
                    sp_obj.JobId = "";
                }
                if (ObjFilter.chkNotManagerReviewed == true)
                {
                    sp_obj.IsNotReviewed = Convert.ToBoolean(1);
                    sp_obj.JobId = "";
                }
                if (ObjFilter.chkTXNotReviewed == true)
                {
                    sp_obj.IsNotReviewed = Convert.ToBoolean(1);
                    sp_obj.JobId = "";
                }
                if (string.IsNullOrEmpty(Convert.ToString(ObjFilter.DateNoticeSent)) == false && Convert.ToDateTime(ObjFilter.DateNoticeSent) != DateTime.MinValue)
                {
                    sp_obj.NoticeSentDate = ObjFilter.DateNoticeSent.ToString("yyyy-MM-dd");
                    sp_obj.JobId = "";
                }
                if (string.IsNullOrEmpty(Convert.ToString(ObjFilter.CanDate)) == false && Convert.ToDateTime(ObjFilter.CanDate) != DateTime.MinValue)
                {
                    sp_obj.CanDate = ObjFilter.CanDate.ToString("yyyy-MM-dd");
                    sp_obj.JobId = "";
                }
                if (string.IsNullOrEmpty(Convert.ToString(ObjFilter.NOIDate)) == false && Convert.ToDateTime(ObjFilter.NOIDate) != DateTime.MinValue)
                {
                    sp_obj.NOIDate = ObjFilter.NOIDate.ToString("yyyy-MM-dd");
                    sp_obj.JobId = "";
                }
                if (string.IsNullOrEmpty(Convert.ToString(ObjFilter.VerifiedDate)) == false && Convert.ToDateTime(ObjFilter.VerifiedDate) != DateTime.MinValue)
                {
                    sp_obj.VerifiedDate = ObjFilter.VerifiedDate.ToString("yyyy-MM-dd");
                    sp_obj.JobId = "";
                }
                if (string.IsNullOrEmpty(Convert.ToString(ObjFilter.LienAssigned)) == false && Convert.ToDateTime(ObjFilter.LienAssigned) != DateTime.MinValue)
                {
                    sp_obj.LienAssignDate = ObjFilter.LienAssigned.ToString("yyyy-MM-dd");
                    sp_obj.JobId = "";
                }
                if (string.IsNullOrEmpty(Convert.ToString(ObjFilter.LienSent)) == false && Convert.ToDateTime(ObjFilter.LienSent) != DateTime.MinValue)
                {
                    sp_obj.LienSendDate = ObjFilter.LienSent.ToString("yyyy-MM-dd");
                    sp_obj.JobId = "";
                }
                if (string.IsNullOrEmpty(Convert.ToString(ObjFilter.StartDate)) == false && Convert.ToDateTime(ObjFilter.StartDate) != DateTime.MinValue)
                {
                    sp_obj.StartDate = ObjFilter.StartDate.ToString("yyyy-MM-dd");
                    sp_obj.JobId = "";
                }



                //ObjFilter.DateAssigned = null;

                int intParseResult = 0;
                sp_obj.AssignedOnly = Convert.ToBoolean(1);
                if (ObjFilter.chkUnassigned == true)
                {
                    sp_obj.AssignedOnly = Convert.ToBoolean(0);
                }

                if (Int32.TryParse(ObjFilter.JobId, out intParseResult) && Convert.ToInt32(ObjFilter.JobId) > 0)
                {
                    sp_obj.JobId = ObjFilter.JobId;
                }

                sp_obj.SortColType = sortBy;
                #endregion
                //DBO.GetTableView(mysearchinfo)
                EntityList = DBO.Provider.GetTableView(sp_obj);

                dt = EntityList.ToTable();

                return (GetList(dt));
            }

            catch (Exception ex)
            {
                return (new List<ADvwJobsList>());
            }
        }


        public static List<ADvwJobsList> GetList(DataTable dt)
        {
            List<ADvwJobsList> JobList = (from DataRow dr in dt.Rows
                                         select new ADvwJobsList()
                                         {
                                             JobId = Convert.ToInt32(dr["JobId"]),
                                             ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                             StatusCode = ((dr["StatusCode"] != DBNull.Value ? dr["StatusCode"].ToString() : "")),
                                             JobName = ((dr["JobName"] != DBNull.Value ? dr["JobName"].ToString() : "")),
                                             JobNum = ((dr["JobNum"] != DBNull.Value ? dr["JobNum"].ToString() : "")),
                                             JobAdd1 = ((dr["JobAdd1"] != DBNull.Value ? dr["JobAdd1"].ToString() : "")),
                                             JobCity = ((dr["JobCity"] != DBNull.Value ? dr["JobCity"].ToString() : "")),
                                             JobState = ((dr["JobState"] != DBNull.Value ? dr["JobState"].ToString() : "")),
                                             JobCounty = ((dr["JobCounty"] != DBNull.Value ? dr["JobCounty"].ToString() : "")),
                                             CustRefNum = ((dr["CustRefNum"] != DBNull.Value ? dr["CustRefNum"].ToString() : "")),
                                             ClientCustomer = ((dr["ClientCustomer"] != DBNull.Value ? dr["ClientCustomer"].ToString() : "")),
                                             GeneralContractor = ((dr["GeneralContractor"] != DBNull.Value ? dr["GeneralContractor"].ToString() : "")),
                                             OwnrName = ((dr["OwnrName"] != DBNull.Value ? dr["OwnrName"].ToString() : "")),
                                             LenderName = ((dr["LenderName"] != DBNull.Value ? dr["LenderName"].ToString() : "")),
                                             CustId = ((dr["CustId"] != DBNull.Value ? Convert.ToInt32(dr["CustId"]) : 0)),
                                             EstBalance = Convert.ToDecimal(dr["EstBalance"] != DBNull.Value ? dr["EstBalance"] : ""),
                                             DeskNum = ((dr["DeskNum"] != DBNull.Value ? dr["DeskNum"].ToString() : "")),
                                             CustFax = ((dr["CustFax"] != DBNull.Value ? dr["CustFax"].ToString() : "")),
                                             CustEmail = ((dr["CustEmail"] != DBNull.Value ? dr["CustEmail"].ToString() : "")),
                                             GCFax = ((dr["GCFax"] != DBNull.Value ? dr["GCFax"].ToString() : "")),
                                             GCEmail = ((dr["GCEmail"] != DBNull.Value ? dr["GCEmail"].ToString() : "")),
                                             DateAssigned = ((dr["DateAssigned"] != DBNull.Value ? Convert.ToDateTime(dr["DateAssigned"]) : DateTime.MinValue)),
                                             StartDate = ((dr["StartDate"] != DBNull.Value ? Convert.ToDateTime(dr["StartDate"]) : DateTime.MinValue)),
                                             RevDate = ((dr["RevDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevDate"]) : DateTime.MinValue)),
                                             NoticeSent = ((dr["NoticeSent"] != DBNull.Value ? Convert.ToDateTime(dr["NoticeSent"]) : DateTime.MinValue)),
                                             VerifiedDate = ((dr["VerifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["VerifiedDate"]) : DateTime.MinValue)),
                                             PONum = ((dr["PONum"] != DBNull.Value ? dr["PONum"].ToString() : "")),
                                             //BondNum = ((dr["BondNum"] != DBNull.Value ? dr["BondNum"].ToString() : "")),
                                              JobBalance = dr["JobBalance"] != DBNull.Value ? Convert.ToDecimal(dr["JobBalance"]) : 0,
                                             Document = ((dr["Document"] != DBNull.Value ? Convert.ToBoolean(dr["Document"]) : false)),
                                             JVStatusCode = ((dr["JVStatusCode"] != DBNull.Value ? Convert.ToString(dr["JVStatusCode"]) : "")),
                                             BranchNum = ((dr["BranchNum"] != DBNull.Value ? dr["BranchNum"].ToString() : "")),
                                             JobStatusDescr = ((dr["JobStatusDescr"] != DBNull.Value ? dr["JobStatusDescr"].ToString() : "")),
                                             NoticeDeadlineDate = ((dr["NoticeDeadlineDate"] != DBNull.Value ? Convert.ToDateTime(dr["NoticeDeadlineDate"]) : DateTime.MinValue)),
                                             PropertyType = GetPropertyType(Convert.ToBoolean(dr["PrivateJob"]), Convert.ToBoolean(dr["FederalJob"]), Convert.ToBoolean(dr["ResidentialBox"]), Convert.ToBoolean(dr["PublicJob"]))


                                         }).ToList();
            return JobList;
        }

        public static List<ADvwJobsList> GetWebNotesList(DataTable dt)
        {
            List<ADvwJobsList> JobList = (from DataRow dr in dt.Rows
                                          select new ADvwJobsList()
                                          {
                                              JobId = Convert.ToInt32(dr["JobId"]),
                                              ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                              StatusCode = ((dr["StatusCode"] != DBNull.Value ? dr["StatusCode"].ToString() : "")),
                                              JobName = ((dr["JobName"] != DBNull.Value ? dr["JobName"].ToString() : "")),
                                              JobNum = ((dr["JobNum"] != DBNull.Value ? dr["JobNum"].ToString() : "")),
                                              JobAdd1 = ((dr["JobAdd1"] != DBNull.Value ? dr["JobAdd1"].ToString() : "")),
                                              JobCity = ((dr["JobCity"] != DBNull.Value ? dr["JobCity"].ToString() : "")),
                                              JobState = ((dr["JobState"] != DBNull.Value ? dr["JobState"].ToString() : "")),
                                              JobCounty = ((dr["JobCounty"] != DBNull.Value ? dr["JobCounty"].ToString() : "")),
                                              CustRefNum = ((dr["CustRefNum"] != DBNull.Value ? dr["CustRefNum"].ToString() : "")),
                                              EstBalance = Convert.ToDecimal(dr["EstBalance"] != DBNull.Value ? dr["EstBalance"] : ""),
                                              GeneralContractor = ((dr["GeneralContractor"] != DBNull.Value ? dr["GeneralContractor"].ToString() : "")),
                                              OwnrName = ((dr["OwnrName"] != DBNull.Value ? dr["OwnrName"].ToString() : "")),
                                              LenderName = ((dr["LenderName"] != DBNull.Value ? dr["LenderName"].ToString() : "")),
                                              ClientCustomer = ((dr["ClientCustomer"] != DBNull.Value ? dr["ClientCustomer"].ToString() : "")),
                                              DeskNum = ((dr["DeskNum"] != DBNull.Value ? dr["DeskNum"].ToString() : "")),
                                              PONum = ((dr["PONum"] != DBNull.Value ? dr["PONum"].ToString() : "")),
                                              OwnerName1 = ((dr["OwnrName1"] != DBNull.Value ? dr["OwnrName1"].ToString() : "")),
                                              DateAssigned = ((dr["DateAssigned"] != DBNull.Value ? Convert.ToDateTime(dr["DateAssigned"]) : DateTime.MinValue)),
                                              LienAssignDate = ((dr["LienAssignDate"] != DBNull.Value ? Convert.ToDateTime(dr["LienAssignDate"]) : DateTime.MinValue)),
                                              RevDate = ((dr["RevDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevDate"]) : DateTime.MinValue)),
                                              NoticeSent = ((dr["NoticeSent"] != DBNull.Value ? Convert.ToDateTime(dr["NoticeSent"]) : DateTime.MinValue)),
                                              LienDate = ((dr["LienDate"] != DBNull.Value ? Convert.ToDateTime(dr["LienDate"]) : DateTime.MinValue)),
                                              NOIDate = ((dr["NOIDate"] != DBNull.Value ? Convert.ToDateTime(dr["NOIDate"]) : DateTime.MinValue)),
                                              VerifiedDate = ((dr["VerifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["VerifiedDate"]) : DateTime.MinValue)),
                                              LastNoticePrintDate = ((dr["LastNoticePrintDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastNoticePrintDate"]) : DateTime.MinValue)),
                                              CustId = ((dr["CustId"] != DBNull.Value ? Convert.ToInt32(dr["CustId"]) : 0)),
                                              CustFax = ((dr["CustFax"] != DBNull.Value ? dr["CustFax"].ToString() : "")),
                                              CustEmail = ((dr["CustEmail"] != DBNull.Value ? dr["CustEmail"].ToString() : "")),
                                              GCFax = ((dr["GCFax"] != DBNull.Value ? dr["GCFax"].ToString() : "")),
                                              GCEmail = ((dr["GCEmail"] != DBNull.Value ? dr["GCEmail"].ToString() : "")),
                                              StartDate = ((dr["StartDate"] != DBNull.Value ? Convert.ToDateTime(dr["StartDate"]) : DateTime.MinValue)),
                                              //BondNum = ((dr["BondNum"] != DBNull.Value ? dr["BondNum"].ToString() : "")),
                                              JobBalance = dr["JobBalance"] != DBNull.Value ? Convert.ToDecimal(dr["JobBalance"]) : 0,
                                              Document = ((dr["Document"] != DBNull.Value ? Convert.ToBoolean(dr["Document"]) : false)),
                                              JVStatusCode = ((dr["JVStatusCode"] != DBNull.Value ? Convert.ToString(dr["JVStatusCode"]) : "")),
                                              BranchNum = ((dr["BranchNum"] != DBNull.Value ? dr["BranchNum"].ToString() : "")),
                                              JobStatusDescr = ((dr["JobStatusDescr"] != DBNull.Value ? dr["JobStatusDescr"].ToString() : "")),
                                              NoticeDeadlineDate = ((dr["NoticeDeadlineDate"] != DBNull.Value ? Convert.ToDateTime(dr["NoticeDeadlineDate"]) : DateTime.MinValue)),
                                              PropertyType = GetPropertyType(Convert.ToBoolean(dr["PrivateJob"]), Convert.ToBoolean(dr["FederalJob"]), Convert.ToBoolean(dr["ResidentialBox"]), Convert.ToBoolean(dr["PublicJob"]))

                                          }).ToList();
            return JobList;
        }

        public static List<ADvwJobsList> GetMonthlyJobListJVList(DataTable dt)
        {
            List<ADvwJobsList> JobList = (from DataRow dr in dt.Rows
                                          select new ADvwJobsList()
                                          {
                                              JobId = Convert.ToInt32(dr["JobId"]),
                                              ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                              StatusCode = ((dr["StatusCode"] != DBNull.Value ? dr["StatusCode"].ToString() : "")),
                                              JobName = ((dr["JobName"] != DBNull.Value ? dr["JobName"].ToString() : "")),
                                              JobNum = ((dr["JobNum"] != DBNull.Value ? dr["JobNum"].ToString() : "")),
                                              JobAdd1 = ((dr["JobAdd1"] != DBNull.Value ? dr["JobAdd1"].ToString() : "")),
                                              JobCity = ((dr["JobCity"] != DBNull.Value ? dr["JobCity"].ToString() : "")),
                                              JobState = ((dr["JobState"] != DBNull.Value ? dr["JobState"].ToString() : "")),
                                              JobCounty = ((dr["JobCounty"] != DBNull.Value ? dr["JobCounty"].ToString() : "")),
                                              CustRefNum = ((dr["CustRefNum"] != DBNull.Value ? dr["CustRefNum"].ToString() : "")),
                                              EstBalance = dr["EstBalance"] != DBNull.Value ? Convert.ToDecimal(dr["EstBalance"]) : 0,
                                              ClientCustomer = ((dr["ClientCustomer"] != DBNull.Value ? dr["ClientCustomer"].ToString() : "")),
                                              GeneralContractor = ((dr["GeneralContractor"] != DBNull.Value ? dr["GeneralContractor"].ToString() : "")),
                                              OwnrName = ((dr["OwnrName"] != DBNull.Value ? dr["OwnrName"].ToString() : "")),
                                              DeskNum = ((dr["DeskNum"] != DBNull.Value ? dr["DeskNum"].ToString() : "")),
                                              LenderName = ((dr["LenderName"] != DBNull.Value ? dr["LenderName"].ToString() : "")),
                                              PONum = ((dr["PONum"] != DBNull.Value ? dr["PONum"].ToString() : "")),
                                              CustFax = ((dr["CustFax"] != DBNull.Value ? dr["CustFax"].ToString() : "")),
                                              GCFax = ((dr["GCFax"] != DBNull.Value ? dr["GCFax"].ToString() : "")),
                                              DateAssigned = ((dr["DateAssigned"] != DBNull.Value ? Convert.ToDateTime(dr["DateAssigned"]) : DateTime.MinValue)),
                                              StartDate = ((dr["StartDate"] != DBNull.Value ? Convert.ToDateTime(dr["StartDate"]) : DateTime.MinValue)),
                                              RevDate = ((dr["RevDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevDate"]) : DateTime.MinValue)),
                                              NoticeSent = ((dr["NoticeSent"] != DBNull.Value ? Convert.ToDateTime(dr["NoticeSent"]) : DateTime.MinValue)),
                                              VerifiedDate = ((dr["VerifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["VerifiedDate"]) : DateTime.MinValue)),
                                              BondNum = ((dr["BondNum"] != DBNull.Value ? dr["BondNum"].ToString() : "")),

                                              BranchNum = ((dr["BranchNum"] != DBNull.Value ? dr["BranchNum"].ToString() : "")),
                                              JobBalance = dr["JobBalance"] != DBNull.Value ? Convert.ToDecimal(dr["JobBalance"]) : 0,
                                              CustId = ((dr["CustId"] != DBNull.Value ? Convert.ToInt32(dr["CustId"]) : 0)),
                                              //CustEmail = ((dr["CustEmail"] != DBNull.Value ? dr["CustEmail"].ToString() : "")),
                                              //GCEmail = ((dr["GCEmail"] != DBNull.Value ? dr["GCEmail"].ToString() : "")),
                                              //BondNum = ((dr["BondNum"] != DBNull.Value ? dr["BondNum"].ToString() : "")),
                                              Document = ((dr["Document"] != DBNull.Value ? Convert.ToBoolean(dr["Document"]) : false)),
                                              JVStatusCode = ((dr["JVStatusCode"] != DBNull.Value ? Convert.ToString(dr["JVStatusCode"]) : "")),
                                              JobStatusDescr = ((dr["JobStatusDescr"] != DBNull.Value ? dr["JobStatusDescr"].ToString() : "")),
                                              NoticeDeadlineDate = ((dr["NoticeDeadlineDate"] != DBNull.Value ? Convert.ToDateTime(dr["NoticeDeadlineDate"]) : DateTime.MinValue)),
                                              PropertyType = GetPropertyType(Convert.ToBoolean(dr["PrivateJob"]), Convert.ToBoolean(dr["FederalJob"]), Convert.ToBoolean(dr["ResidentialBox"]), Convert.ToBoolean(dr["PublicJob"]))


                                          }).ToList();
            return JobList;
        }

        public static List<ADvwJobsList> GetJobList(string sortBy)
        {
            try
            {

                
                TableView EntityList;
                //var Sp_Obj = new uspbo_LiensEmailFaxUpdates_GetActiveJobs();
                var Sp_Obj = new uspbo_Jobs_GetJobList();
                //Sp_Obj.JobId = null;
                Sp_Obj.StatusGroupId = "1";
                Sp_Obj.SortColType = sortBy;

                EntityList = DBO.Provider.GetTableView(Sp_Obj);
                
                DataTable dt = EntityList.ToTable();


                List<ADvwJobsList> JobList = (from DataRow dr in dt.Rows
                                              select new ADvwJobsList()
                                              {
                                                  JobId = Convert.ToInt32(dr["JobId"]),

                                                  ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                                  StatusCode = ((dr["StatusCode"] != DBNull.Value ? dr["StatusCode"].ToString() : "")),
                                                  JobName = ((dr["JobName"] != DBNull.Value ? dr["JobName"].ToString() : "")),
                                                  JobNum = ((dr["JobNum"] != DBNull.Value ? dr["JobNum"].ToString() : "")),
                                                  JobAdd1 = ((dr["JobAdd1"] != DBNull.Value ? dr["JobAdd1"].ToString() : "")),
                                                  JobCity = ((dr["JobCity"] != DBNull.Value ? dr["JobCity"].ToString() : "")),
                                                  JobState = ((dr["JobState"] != DBNull.Value ? dr["JobState"].ToString() : "")),
                                                  JobCounty = ((dr["JobCounty"] != DBNull.Value ? dr["JobCounty"].ToString() : "")),
                                                  CustRefNum = ((dr["CustRefNum"] != DBNull.Value ? dr["CustRefNum"].ToString() : "")),
                                                  ClientCustomer = ((dr["ClientCustomer"] != DBNull.Value ? dr["ClientCustomer"].ToString() : "")),
                                                  GeneralContractor = ((dr["GeneralContractor"] != DBNull.Value ? dr["GeneralContractor"].ToString() : "")),
                                                  CustPhNUm = ((dr["CustPhoneNo"] != DBNull.Value ? dr["CustPhoneNo"].ToString() : "")),
                                                  CustFax = ((dr["CustFax"] != DBNull.Value ? dr["CustFax"].ToString() : "")),
                                                  CustEmail = ((dr["CustEmail"] != DBNull.Value ? dr["CustEmail"].ToString() : "")),
                                                  GCPhNum = ((dr["GCPhoneNo"] != DBNull.Value ? dr["GCPhoneNo"].ToString() : "")),
                                                  GCFax = ((dr["GCFax"] != DBNull.Value ? dr["GCFax"].ToString() : "")),
                                                  GCEmail = ((dr["GCEmail"] != DBNull.Value ? dr["GCEmail"].ToString() : "")),

                                               
                                              }).ToList();
                return JobList;
            }
            catch (Exception ex)
            {
                return (new List<ADvwJobsList>());
            }
        }

        public static string GetPropertyType(bool prPrivate, bool prFederal, bool prResidential, bool prPublic)
        {
            string Property = "";
            if(prPrivate == true)
            {
                Property = "Private";
            }
            if (prFederal == true)
            {
                Property = "Federal";
            }
            if (prResidential == true)
            {
                Property = "Residential";
            }
            if (prPublic == true)
            {
                Property = "Public";
            }
            return Property;
        }

    }
}
