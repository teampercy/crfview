﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using CRFView.Adapters.Liens.Managements.ChangeClient.ViewModels;
using CRFView.Adapters.Liens.JVTXNoticeRequest.Extract.ViewModels;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using CRFView.Adapters.Liens.JVTXNoticeRequest.PrintLetters.ViewModels;
using CRF.BLL.COMMON;
using System.Collections;
using CRF.BLL;
using System.Web;
using CRFView.Adapters.Liens.Managements.ChangeJobInfo.ViewModels;
using CRFView.Adapters.Liens.Managements.ChangeDateRequested.ViewModels;
using CRFView.Adapters.Liens.NoticeCycle.Extract.ViewModels;
using CRFView.Adapters.Collections.Management.DeskAssignment.ViewModels;
using CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels;
using CRFView.Adapters.Liens.Verifiers.WorkCard;
using System.Drawing.Printing;

namespace CRFView.Adapters
{
    public class ADJob
    {
        #region Properties
        public int Id { get; set; }
        public int BatchId { get; set; }
        public int ClientId { get; set; }
        public string JobName { get; set; }
        public string JobAdd1 { get; set; }
        public string JobAdd2 { get; set; }
        public string JobCity { get; set; }
        public string JobState { get; set; }
        public string JobZip { get; set; }
        public string JobNum { get; set; }
        public string RefNum { get; set; }
        public string APNNum { get; set; }
        public decimal EstBalance { get; set; }
        public DateTime DateAssigned { get; set; }
        public DateTime StartDate { get; set; }
        public System.DateTime RevDate { get; set; }
        public DateTime CanDate { get; set; }
        public DateTime EndDate { get; set; }
        public DateTime FileDate { get; set; }
        public DateTime LienDate { get; set; }
        public DateTime ResendDate { get; set; }
        public DateTime LienAssignDate { get; set; }
        public DateTime AmendedDate { get; set; }
        public DateTime VerifiedDate { get; set; }
        public DateTime NoticeSent { get; set; }
        public string BookNum { get; set; }
        public string PageNum { get; set; }
        public string PONum { get; set; }
        public string StatusCode { get; set; }
        public string DeskNum { get; set; }
        public int DeskNumId { get; set; }
        public int StatusCodeId { get; set; }
        public int ToDeskNumId { get; set; }
        public int TitleDeskNumId { get; set; }
        public string TODeskNum { get; set; }
        public string JobCounty { get; set; }
        public string BondNum { get; set; }
        public string RecorderNum { get; set; }
        public string SpecialInstruction { get; set; }
        public string FolioNum { get; set; }
        public string ReleaseDocNum { get; set; }
        public string TitleDeskNum { get; set; }
        public string Note { get; set; }


        public DateTime ReleaseFileDate { get; set; }
        public DateTime CreateDate { get; set; }
        public string PropOwner { get; set; }
        public string LenderName { get; set; }
        public string MatchingKey { get; set; }
        public bool PrelimBox { get; set; }
        public bool RushOrder { get; set; }
        public bool VerifyJob { get; set; }
        public bool PODBox { get; set; }
        public bool FederalJob { get; set; }
        public bool ResidentialBox { get; set; }
        public bool GreenCard { get; set; }
        public bool MechanicLien { get; set; }
        public bool PublicJob { get; set; }
        public int BatchJobId { get; set; }
        public int BadAddressFlag { get; set; }
        public decimal LienAmt { get; set; }
        public decimal RecordingFee { get; set; }
        public bool RushOrderML { get; set; }
        public bool CanOrder { get; set; }
        public bool StopNoticeBox { get; set; }
        public bool LienRelease { get; set; }
        public bool SuretyBox { get; set; }
        public bool PaymentBond { get; set; }
        public bool ResendBox { get; set; }
        public bool JointCk { get; set; }
        public string CustJobNum { get; set; }
        public string ProblemMsg { get; set; }
        public string GCJobNum { get; set; }
        public string BranchNum { get; set; }


        public bool ReleaseRush { get; set; }
        public bool MechLienRush { get; set; }
        public bool LienExtBox { get; set; }
        public bool ResendOwner { get; set; }
        public bool ResendGC { get; set; }
        public bool ResendLender { get; set; }
        public bool ResendCust { get; set; }
        public bool BackupBox { get; set; }
        public bool NOIBox { get; set; }
        public bool AmendedBox { get; set; }
        public bool ProbRequest2 { get; set; }
        public bool ProbRequest3 { get; set; }
        public bool TXSub90 { get; set; }
        public bool TXSubSub60 { get; set; }
        public bool BondReleaseBox { get; set; }
        public bool SNReleaseBox { get; set; }
        public bool TitleVerifiedBox { get; set; }
        public bool SameAsGC { get; set; }
        public bool FATHit { get; set; }
        public bool SendAsIsBox { get; set; }
        public bool SameAsCust { get; set; }
        public bool NCInfo { get; set; }
        public bool PrivateJob { get; set; }
        public bool SameAsOwner { get; set; }
        public DateTime ReleaseDate { get; set; }
        public DateTime BondDate { get; set; }
        public DateTime PODDate { get; set; }
        public DateTime LienExtDate { get; set; }
        public DateTime SNDate { get; set; }
        public DateTime NOIDate { get; set; }
        public DateTime BondReleaseDate { get; set; }
        public DateTime SNReleaseDate { get; set; }
        public DateTime NOIAssignDate { get; set; }
        public DateTime SNAssignDate { get; set; }
        public DateTime BondAssignDate { get; set; }
        public DateTime SNReleaseAssign { get; set; }
        public DateTime BCReleaseAssign { get; set; }
        public DateTime LienReleaseAssign { get; set; }
        public DateTime LEAssignDate { get; set; }
        public DateTime LienDDD { get; set; }
        public DateTime BondDDD { get; set; }
        public DateTime SNDDD { get; set; }
        public DateTime FATDate { get; set; }
        public DateTime LastNoticePrintDate { get; set; }
        public DateTime SubmittedOn { get; set; }

        public int WebCRSLienNum { get; set; }
        public int CustId { get; set; }
        public int GenId { get; set; }
        public int OwnrId { get; set; }
        public int LenderId { get; set; }
        public int DesigneeId { get; set; }
        public int BatchJobSiteId { get; set; }
        public int ParentJobId { get; set; }
        public int JVDeskNumId { get; set; }
        public int JVStatusCodeId { get; set; }
        public int SubmittedByUserId { get; set; }
        public int LastUpdatedById { get; set; }
        public int NTOClientId { get; set; }
        public string CountyRecorderNum { get; set; }
        public string EquipRate { get; set; }
        public string EquipRental { get; set; }
        public string CustCertNum { get; set; }
        public string GCCertNum { get; set; }
        public string OwnerCertNum { get; set; }
        public string LenderCertNum { get; set; }
        public string AZLotAllocate { get; set; }
        public string PlacementSource { get; set; }
        public string SubmittedBy { get; set; }
        public string LastUpdatedBy { get; set; }
        public string RANum { get; set; }
        public string OrigJobAdd1 { get; set; }
        public string BuildingPermitNum { get; set; }
        public bool JobAlertBox { get; set; }
        public bool CancelML { get; set; }
        public bool FaxReceived { get; set; }
        public bool OwnerSourceFAT { get; set; }
        public bool OwnerSourceAccurint { get; set; }
        public bool OwnerSourceCust { get; set; }
        public bool OwnerSourceCustFax { get; set; }
        public bool OwnerSourceGC { get; set; }
        public bool OwnerSourceGCFax { get; set; }
        public bool OwnerSourceAccessor { get; set; }
        public bool OwnerSourceClient { get; set; }
        public bool MultipleOwnerBox { get; set; }
        public bool ProbRequest1 { get; set; }
        public bool ManagerReviewed { get; set; }
        public bool IsBillableLegalDesc { get; set; }
        public bool NOCompBox { get; set; }
        public bool IsDataEntryFee { get; set; }

        public DateTime LastUpdatedOn { get; set; }
        public DateTime NOCDate { get; set; }
        public DateTime JobCompletionDate { get; set; }
        public DateTime LienDeadlineDate { get; set; }
        public DateTime BondDeadlineDate { get; set; }
        public DateTime SNDeadlineDate { get; set; }
        public DateTime NOIDeadlineDate { get; set; }
        public DateTime NOCRecDate { get; set; }
        public DateTime PIFDate { get; set; }
        public DateTime CloseDate { get; set; }
        public DateTime NoticeDeadlineDate { get; set; }
        public DateTime ForeclosureDeadlineDate { get; set; }
        public DateTime BondSuitDeadlineDate { get; set; }
        public DateTime ForeclosureDate { get; set; }
        public DateTime BondSuitDate { get; set; }

        public string JVDeskNum { get; set; }
        public string JVStatusCode { get; set; }
        public string MaterialDescription { get; set; }
        public string NOCDocNum { get; set; }
        public string FilterKey { get; set; }
        public string CollectorName { get; set; }
        public string CollectorPhoneNo { get; set; }

        public bool VerifyOWOnly { get; set; }
        public bool VerifyOWOnlySend { get; set; }
        public bool VerifyOWOnlyTX { get; set; }
        public bool IsJobView { get; set; }
        public bool JobViewService { get; set; }
        public bool IsReviewNote { get; set; }
        public bool NoNTO { get; set; }
        public bool RushOrderVerify { get; set; }

        public decimal MaterialPrice { get; set; }
        public decimal JobBalance { get; set; }
        public decimal JobOrigBalance { get; set; }
        public decimal MergedOrigBalance { get; set; }
        public decimal MergedJobBalance { get; set; }
        public decimal MergedEstBalance { get; set; }



        //-- Custom Property 
        public string sOldType { get; set; }
        public string sNewType { get; set; }
        public string sOldJobType { get; set; }
        public string sNewJobType { get; set; }
        public int RbtJobType { get; set; }

        #endregion



        #region CRUD

        public static ADJob ReadJob(int JobID)
        {
            Job myentity = new Job();
            ITable myentityItable = myentity;
            DBO.Provider.Read(JobID.ToString(), ref myentityItable);
            myentity = (Job)myentityItable;
            AutoMapper.Mapper.CreateMap<Job, ADJob>();
            return (AutoMapper.Mapper.Map<ADJob>(myentity));
        }


        public static bool UpdateJobEditView(ADJob objUpdatedADJob, EditJobVM objUpdateJob)
        {
            try
            {
                ADJob objADJobOriginal = ReadJob(objUpdatedADJob.Id);

                objADJobOriginal.StatusCode = objUpdatedADJob.StatusCode;
                objADJobOriginal.LastUpdatedById = objUpdatedADJob.LastUpdatedById;
                objADJobOriginal.LastUpdatedBy = objUpdatedADJob.LastUpdatedBy;
                objADJobOriginal.LastUpdatedOn = objUpdatedADJob.LastUpdatedOn;

                if (objUpdateJob.Editrbt == "PrelimBox")
                {
                    objADJobOriginal.PrelimBox = true;
                    objADJobOriginal.VerifyJob = false;
                    objADJobOriginal.TitleVerifiedBox = false;
                    objADJobOriginal.SendAsIsBox = false;
                    objADJobOriginal.VerifyOWOnly = false;
                    objADJobOriginal.VerifyOWOnlySend = false;
                    objADJobOriginal.VerifyOWOnlyTX = false;
                }
                if (objUpdateJob.Editrbt == "VerifyJob")
                {
                    objADJobOriginal.VerifyJob = true;
                    objADJobOriginal.PrelimBox = false;
                    objADJobOriginal.TitleVerifiedBox = false;
                    objADJobOriginal.SendAsIsBox = false;
                    objADJobOriginal.VerifyOWOnly = false;
                    objADJobOriginal.VerifyOWOnlySend = false;
                    objADJobOriginal.VerifyOWOnlyTX = false;
                }
                if (objUpdateJob.Editrbt == "TitleVerifiedBox")
                {
                    objADJobOriginal.TitleVerifiedBox = true;
                    objADJobOriginal.PrelimBox = false;
                    objADJobOriginal.VerifyJob = false;
                    objADJobOriginal.SendAsIsBox = false;
                    objADJobOriginal.VerifyOWOnly = false;
                    objADJobOriginal.VerifyOWOnlySend = false;
                    objADJobOriginal.VerifyOWOnlyTX = false;
                }
                if (objUpdateJob.Editrbt == "SendAsIsBox")
                {
                    objADJobOriginal.SendAsIsBox = true;
                    objADJobOriginal.PrelimBox = false;
                    objADJobOriginal.VerifyJob = false;
                    objADJobOriginal.TitleVerifiedBox = false;
                    objADJobOriginal.VerifyOWOnly = false;
                    objADJobOriginal.VerifyOWOnlySend = false;
                    objADJobOriginal.VerifyOWOnlyTX = false;
                }
                if (objUpdateJob.Editrbt == "VerifyOWOnly")
                {
                    objADJobOriginal.VerifyOWOnly = true;
                    objADJobOriginal.PrelimBox = false;
                    objADJobOriginal.VerifyJob = false;
                    objADJobOriginal.TitleVerifiedBox = false;
                    objADJobOriginal.SendAsIsBox = false;
                    objADJobOriginal.VerifyOWOnlySend = false;
                    objADJobOriginal.VerifyOWOnlyTX = false;
                }
                if (objUpdateJob.Editrbt == "VerifyOWOnlySend")
                {
                    objADJobOriginal.VerifyOWOnlySend = true;
                    objADJobOriginal.PrelimBox = false;
                    objADJobOriginal.VerifyJob = false;
                    objADJobOriginal.TitleVerifiedBox = false;
                    objADJobOriginal.SendAsIsBox = false;
                    objADJobOriginal.VerifyOWOnly = false;
                    objADJobOriginal.VerifyOWOnlyTX = false;
                }
                if (objUpdateJob.Editrbt == "VerifyOWOnlyTX")
                {
                    objADJobOriginal.VerifyOWOnlyTX = true;
                    objADJobOriginal.PrelimBox = false;
                    objADJobOriginal.VerifyJob = false;
                    objADJobOriginal.TitleVerifiedBox = false;
                    objADJobOriginal.SendAsIsBox = false;
                    objADJobOriginal.VerifyOWOnly = false;
                    objADJobOriginal.VerifyOWOnlySend = false;
                }

                if (objUpdateJob.obj_JobEdit.PublicJob == true)
                {
                    objADJobOriginal.PublicJob = true;
                    objADJobOriginal.PrivateJob = false;
                    objADJobOriginal.ResidentialBox = false;
                    objADJobOriginal.FederalJob = false;
                }
                if (objUpdateJob.obj_JobEdit.PrivateJob == true)
                {
                    objADJobOriginal.PrivateJob = true;
                    objADJobOriginal.PublicJob = false;
                    objADJobOriginal.ResidentialBox = false;
                    objADJobOriginal.FederalJob = false;
                }
                if (objUpdateJob.obj_JobEdit.ResidentialBox == true)
                {
                    objADJobOriginal.ResidentialBox = true;
                    objADJobOriginal.PrivateJob = false;
                    objADJobOriginal.PublicJob = false;
                    objADJobOriginal.FederalJob = false;
                }
                if (objUpdateJob.obj_JobEdit.FederalJob == true)
                {
                    objADJobOriginal.FederalJob = true;
                    objADJobOriginal.PrivateJob = false;
                    objADJobOriginal.PublicJob = false;
                    objADJobOriginal.ResidentialBox = false;
                    
                }

                if (string.IsNullOrEmpty(Convert.ToString(objUpdatedADJob.EstBalance)) == false)
                {
                    objADJobOriginal.EstBalance = objUpdatedADJob.EstBalance;
                }

                if (objUpdatedADJob.PONum == "" || objUpdatedADJob.PONum == null)
                {
                }
                else
                {
                    objADJobOriginal.PONum = objUpdatedADJob.PONum.Trim();
                }
                if (objUpdatedADJob.JobName == "" || objUpdatedADJob.JobName == null)
                {
                }
                else
                {
                    objADJobOriginal.JobName = objUpdatedADJob.JobName.Trim();
                }
                if (objUpdatedADJob.JobAdd1 == "" || objUpdatedADJob.JobAdd1 == null)
                {
                }
                else
                {
                    objADJobOriginal.JobAdd1 = objUpdatedADJob.JobAdd1.Trim();
                }
                if (objUpdatedADJob.JobAdd2 == "" || objUpdatedADJob.JobAdd2 == null)
                {
                    objADJobOriginal.JobAdd2 = objUpdatedADJob.JobAdd2.Trim();
                }
                else
                {
                    objADJobOriginal.JobAdd2 = objUpdatedADJob.JobAdd2.Trim();
                }
                if (objUpdatedADJob.JobCity == "" || objUpdatedADJob.JobCity == null)
                {
                }
                else
                {
                    objADJobOriginal.JobCity = objUpdatedADJob.JobCity.Trim();
                }
                if (objUpdatedADJob.JobState == "" || objUpdatedADJob.JobState == null)
                {
                }
                else
                {
                    objADJobOriginal.JobState = objUpdatedADJob.JobState.Trim();
                }
                if (objUpdatedADJob.JobZip == "" || objUpdatedADJob.JobZip == null)
                {
                }
                else
                {
                    objADJobOriginal.JobZip = objUpdatedADJob.JobZip.Trim();
                }
                if (objUpdatedADJob.JobCounty == "" || objUpdatedADJob.JobCounty == null)
                {
                }
                else
                {
                    objADJobOriginal.JobCounty = objUpdatedADJob.JobCounty.Trim();
                }
                if (objUpdatedADJob.APNNum == "" || objUpdatedADJob.APNNum == null)
                {
                }
                else
                {
                    objADJobOriginal.APNNum = objUpdatedADJob.APNNum.Trim();
                }
                if (objUpdatedADJob.CountyRecorderNum == "" || objUpdatedADJob.CountyRecorderNum == null)
                {
                }
                else
                {
                    objADJobOriginal.CountyRecorderNum = objUpdatedADJob.CountyRecorderNum.Trim();
                }
                if (objUpdatedADJob.FolioNum == "" || objUpdatedADJob.FolioNum == null)
                {
                }
                else
                {
                    objADJobOriginal.FolioNum = objUpdatedADJob.FolioNum.Trim();
                }
                if (objUpdatedADJob.CustJobNum == "" || objUpdatedADJob.CustJobNum == null)
                {
                }
                else
                {
                    objADJobOriginal.CustJobNum = objUpdatedADJob.CustJobNum.Trim();
                }
                if (objUpdatedADJob.RbtJobType.ToString() == "")
                {
                }
                else
                {
                    objADJobOriginal.RbtJobType = objUpdatedADJob.RbtJobType;
                }
                if (objUpdatedADJob.DeskNumId == 0 || objUpdatedADJob.DeskNumId == null)
                {
                }
                else
                {
                    objADJobOriginal.DeskNumId = objUpdatedADJob.DeskNumId;
                }
                if (objUpdatedADJob.RevDate.ToString() == "" || objUpdatedADJob.RevDate == null)
                {
                }
                else
                {
                    objADJobOriginal.RevDate = objUpdatedADJob.RevDate;
                }


                AutoMapper.Mapper.CreateMap<ADJob, Job>();
                Job myentity;
                myentity = AutoMapper.Mapper.Map<Job>(objADJobOriginal);
                //myentity = AutoMapper.Mapper.Map<Job>(objUpdatedADJob);

                ITable tblI = myentity;
                DBO.Provider.Update(ref tblI);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool UpdateEditCheckBoxes(ADJob objUpdatedADJob)
        {
            try
            {
                ADJob objADJobOriginal = ReadJob(objUpdatedADJob.Id);

                objADJobOriginal.LastUpdatedById = objUpdatedADJob.LastUpdatedById;
                objADJobOriginal.LastUpdatedBy = objUpdatedADJob.LastUpdatedBy;
                objADJobOriginal.LastUpdatedOn = objUpdatedADJob.LastUpdatedOn;

                objADJobOriginal.RushOrder = objUpdatedADJob.RushOrder;
                objADJobOriginal.PublicJob = objUpdatedADJob.PublicJob;
                objADJobOriginal.ResidentialBox = objUpdatedADJob.ResidentialBox;
                objADJobOriginal.FederalJob = objUpdatedADJob.FederalJob;
                objADJobOriginal.NCInfo = objUpdatedADJob.NCInfo;
                objADJobOriginal.RushOrderVerify = objUpdatedADJob.RushOrderVerify;
                objADJobOriginal.GreenCard = objUpdatedADJob.GreenCard;
                objADJobOriginal.TXSubSub60 = objUpdatedADJob.TXSubSub60;
                objADJobOriginal.TXSub90 = objUpdatedADJob.TXSub90;
                objADJobOriginal.JobAlertBox = objUpdatedADJob.JobAlertBox;
                objADJobOriginal.ManagerReviewed = objUpdatedADJob.ManagerReviewed;
                objADJobOriginal.NOCompBox = objUpdatedADJob.NOCompBox;

                objADJobOriginal.OwnerSourceCust = objUpdatedADJob.OwnerSourceCust;
                objADJobOriginal.OwnerSourceCustFax = objUpdatedADJob.OwnerSourceCustFax;
                objADJobOriginal.OwnerSourceAccurint = objUpdatedADJob.OwnerSourceAccurint;
                objADJobOriginal.OwnerSourceAccessor = objUpdatedADJob.OwnerSourceAccessor;
                objADJobOriginal.OwnerSourceGC = objUpdatedADJob.OwnerSourceGC;
                objADJobOriginal.OwnerSourceGCFax = objUpdatedADJob.OwnerSourceGCFax;
                objADJobOriginal.OwnerSourceClient = objUpdatedADJob.OwnerSourceClient;

                AutoMapper.Mapper.CreateMap<ADJob, Job>();
                Job myentity;
                myentity = AutoMapper.Mapper.Map<Job>(objADJobOriginal);
                //myentity = AutoMapper.Mapper.Map<Job>(objUpdatedADJob);

                ITable tblI = myentity;
                DBO.Provider.Update(ref tblI);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public static DataTable GetvwJobInfoList(string JobId)
        {
            try
            {
                string sql = "Select * from vwJobInfo where JobId = " + JobId;
                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }
        public static bool UpdateChangeClientCodeInfo(ChangeClientCodeVM ObjUpdateChangeClientCodeVM)
        {
            try
            {

                var mysproc = new uspbo_Jobs_SwitchClient();
                mysproc.JobId = Convert.ToInt32(ObjUpdateChangeClientCodeVM.JobId);
                mysproc.ClientId = Convert.ToInt32(ObjUpdateChangeClientCodeVM.objvwJobsList.ClientIds);
                //mysproc.ClientId = Convert.ToInt32(Obj_ADJob.ClientIds);
                DBO.Provider.ExecNonQuery(mysproc);

                ADJob ObjJob = ReadJob(Convert.ToInt32(ObjUpdateChangeClientCodeVM.JobId));

                if (ObjJob.ClientId != mysproc.ClientId)
                {
                    ObjUpdateChangeClientCodeVM.Message = "Can't change Client this job has been billed";
                }

                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }

        public static DataTable GetChangeJobInfoList(string JobId)
        {
            try
            {
                string sql = "Select * from vwJobInfo where JobId = " + JobId;
                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        public static int UpdateJob(ChangeJobInfoVM objChangeJobInfo)
        {
            try
            {
                ADJob objADJobOriginal = ReadJob(objChangeJobInfo.objADJob.Id);

                objADJobOriginal.JointCk = objChangeJobInfo.chkJointCk;
                objADJobOriginal.GreenCard = objChangeJobInfo.chkGreencard;
                objADJobOriginal.IsJobView = objChangeJobInfo.chkIsJobview;

                if (objChangeJobInfo.chkStartDate == true)
                {
                    objADJobOriginal.StartDate = Convert.ToDateTime(objChangeJobInfo.DateTimePicker1.ToShortDateString());
                }

                if (objChangeJobInfo.chkBranchNum == true)
                {
                    objADJobOriginal.BranchNum = objChangeJobInfo.txtBranchNum;
                }

                if (objChangeJobInfo.chkDeleteVerifiedDate == true)
                {
                    objADJobOriginal.VerifiedDate = Convert.ToDateTime(null);
                }

                if (objChangeJobInfo.chkDeleteTODesk == true)
                {
                    objADJobOriginal.TODeskNum = "";
                    objADJobOriginal.ToDeskNumId = 0;
                   
                }

                AutoMapper.Mapper.CreateMap<ADJob, Job>();
                Job myentity;
                myentity = AutoMapper.Mapper.Map<Job>(objADJobOriginal);
                //myentity = AutoMapper.Mapper.Map<Job>(objUpdatedADJob);

                ITable tblI = myentity;
                DBO.Provider.Update(ref tblI);

                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }


        public static int SaveJob_ChangeJobInfo_1(ChangeJobInfoVM objChangeJobInfo)
        {
            //ChangeJobInfoVM objChangeJobInfoVM = new ChangeJobInfoVM();
            try
            {
                var MYSPROC = new uspbo_Jobs_DeleteNoticeHistory();
                MYSPROC.jobId = Convert.ToInt32(objChangeJobInfo.JobId);
                MYSPROC.DateCreated = objChangeJobInfo.DateTimePicker2.ToShortDateString();
                DBO.Provider.ExecNonQuery(MYSPROC);

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int SaveJob_ChangeJobInfo2(string JobId)
        {
            Job objJob = new Job();
            try
            {
                var MYSPROC = new uspbo_Jobview_UpdateDeadlineDates();
                MYSPROC.JobId = Convert.ToString(objJob.Id);
                DBO.Provider.ExecNonQuery(MYSPROC);
                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int SaveJob_ChangeJobInfo_2ElseCon(string JobId)
        {
            Job objJob = new Job();
            try
            {
                var MYSPROC = new uspbo_LiensDayEnd_UpdateDeadlineDates();
                MYSPROC.ExpireDays = 0;
                MYSPROC.JobId = Convert.ToString(objJob.Id);
                DBO.Provider.ExecNonQuery(MYSPROC);
                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int UpdatePublicException_Action(ChangeDateRequestedVM objChangeDateRequestedVM)
        {

            try
            {
                string sOldType = "";
                /// myentity = new Job();
                //ITable myentityItable = myentity;
                //DBO.Provider.Read(objChangeDateRequestedVM.JobId.ToString(), ref myentityItable);
                //myentity = (Job)myentityItable;
                //AutoMapper.Mapper.CreateMap<Job, ADJob>();
                //// return (AutoMapper.Mapper.Map<ADJob>(myentity));

                //  CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                ////  ChangeDateRequestedVM objADJobOriginal = ReadJob(Convert.ToInt32(objChangeDateRequestedVM.JobId));

                //  TableView myview = DBO.Provider.GetTableView(objADJobOriginal.ToString());
                //  // DBO.Provider.Read(objChangeDateRequestedVM.JobId, ref myentityItable);
                //  DBO.Provider.Read(myview.get_RowItem("JobId").ToString(), ref myentityItable);
                //  if (objADJobOriginal.PrivateJob == true)
                //  {
                //      sOldType = "Private Job";
                //  }
                //  if (objADJobOriginal.ResidentialBox == true)
                //  {
                //      sOldType = "Residential Job";
                //  }

                //  LogChanges(objADJobOriginal.Id, sOldType);
                //  objADJobOriginal.PublicJob = true;
                //  if (objADJobOriginal.SendAsIsBox == true && objADJobOriginal.PlacementSource == "WEB")
                //  {
                //      objADJobOriginal.NCInfo = true;
                //  }

                //  DBO.Provider.Update(ref myentityItable);
                //  DBO.Provider.Read(myview.get_RowItem("JobId").ToString(), ref myentityItable);


                //  //  myview.get_RowItem("Id") = myentity.PublicJob;

                //  //    ListGrid.Rows(myview.RowIndex).Cells(0).Value = myjob.PublicJob;
                //  //ListGrid.Rows(myview.RowIndex).Cells(4).Value = myjob.NCInfo;


                //  myview.MoveNext();

                return 1;
            }

            catch (Exception ex)
            {
                return 0;
            }

        }

        public static int LogChanges(int aJobId, string sOldType)
        {
            var mysproc = new uspbo_Jobs_UpdateChangeLog();
            mysproc.JobId = aJobId;
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            mysproc.UserId = user.Id;
            mysproc.UserName = user.UserName.Substring(0, 50).ToUpper();
            mysproc.ChangeType = 0;
            mysproc.ChangeFrom = sOldType.Trim() + " ";
            mysproc.ChangeTo = "Public Job";
            DBO.Provider.ExecNonQuery(mysproc);
            return 1;
        }


        public static int ProcessActionCode(string ActionCodesId, string JobId, string StatusCodeId)
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var MYSPROC = new CRF.BLL.CRFDB.SPROCS.uspbo_Jobs_UpdateActions();
                MYSPROC.JobId = Convert.ToInt32(JobId);
                MYSPROC.UserId = user.Id;
                MYSPROC.UserName = user.UserName;

                string sSql = "Select * from JobActions where Id In(" + ActionCodesId + ")";

                TableView myview = DBO.Provider.GetTableView(sSql);
                if (myview.Count == 0)
                {
                    return 0;
                }
                else
                {
                    DataTable dt = myview.ToTable();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        MYSPROC.ActionIds = dt.Rows[i]["Id"].ToString();
                        if (Convert.ToBoolean(dt.Rows[i]["IsDropReview"]) == true && Convert.ToBoolean(dt.Rows[i]["IsChangeStatus"]) == false)
                        {
                            //  var myCollectionStatus = new CollectionStatus();
                            CollectionStatus myCollectionStatus = new CollectionStatus();
                            ITable myentityItable = myCollectionStatus;
                            DBO.Provider.Read(StatusCodeId, ref myentityItable);
                            myCollectionStatus = (CollectionStatus)myentityItable;
                            if (myCollectionStatus.IsDropContact == false)
                            {
                                return 1;
                            }
                        }
                        DBO.Provider.ExecNonQuery(MYSPROC);
                    }
                    return 2;
                }
            }
            catch (Exception ex)
            {
                return -1;
            }
        }




        public static bool UpdateJobMiscInfo(ADJob objUpdatedADJob)
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                ADJob objADJobOriginal = new ADJob();
                if (objUpdatedADJob == null || objUpdatedADJob.Id == 0)
                {
                     objADJobOriginal = ReadJob(0);
                }
                else
                {
                     objADJobOriginal = ReadJob(objUpdatedADJob.Id);
                }

                objADJobOriginal.SpecialInstruction = objUpdatedADJob.SpecialInstruction;
                objADJobOriginal.Note = objUpdatedADJob.Note;
                objADJobOriginal.AZLotAllocate = objUpdatedADJob.AZLotAllocate;
                objADJobOriginal.EquipRate = objUpdatedADJob.EquipRate;
                objADJobOriginal.NOCDate = objUpdatedADJob.NOCDate;
                objADJobOriginal.EquipRental = objUpdatedADJob.EquipRental;
                objADJobOriginal.NOCDocNum = objUpdatedADJob.NOCDocNum;
                objADJobOriginal.RANum = objUpdatedADJob.RANum;
                objADJobOriginal.NOCRecDate = objUpdatedADJob.NOCRecDate;
                objADJobOriginal.BuildingPermitNum = objUpdatedADJob.BuildingPermitNum;
                objADJobOriginal.JobCompletionDate = objUpdatedADJob.JobCompletionDate;
                objADJobOriginal.MaterialDescription = objUpdatedADJob.MaterialDescription;
                objADJobOriginal.MaterialPrice = objUpdatedADJob.MaterialPrice;
                objADJobOriginal.LastUpdatedById = user.Id;
                objADJobOriginal.LastUpdatedBy = user.UserName;
                objADJobOriginal.LastUpdatedOn = DateTime.Now;

                Client myClient = new Client();
                ADClient ObjADClient =  ADClient.ReadClientByClientId(Convert.ToInt32(objUpdatedADJob.ClientId));

                // 11/10/2016
                // 3/12/2019 Added logic for jobview job
                // 3/27/2019 Added Client.NoRentalInfo flag for Jobview clients without Rental info
        
                if(objADJobOriginal.IsJobView == true && ObjADClient.NoRentalInfo == false)
                {

                    var mysproc = new uspbo_Jobview_UpdateDeadlineDates();
                    mysproc.JobId = Convert.ToString(objADJobOriginal.Id);
                    DBO.Provider.ExecNonQuery(mysproc);
                }
                else
                {
                    var mysproc = new uspbo_LiensDayEnd_UpdateDeadlineDates();
                    mysproc.ExpireDays = 0;
                    mysproc.JobId = Convert.ToString(objADJobOriginal.Id);
                    DBO.Provider.ExecNonQuery(mysproc);
                }


                AutoMapper.Mapper.CreateMap<ADJob, Job>();
                Job myentity;
                myentity = AutoMapper.Mapper.Map<Job>(objADJobOriginal);
                //myentity = AutoMapper.Mapper.Map<Job>(objUpdatedADJob);

                ITable tblI = myentity;
                DBO.Provider.Update(ref tblI);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }



        #endregion



        #region Local Methods

        public static ADJob GetJob(DataTable dt)
        {
            List<ADJob> JobList = (from DataRow dr in dt.Rows
                                   select new ADJob()
                                   {
                                       Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                       BatchId = ((dr["BatchId"] != DBNull.Value ? Convert.ToInt32(dr["BatchId"]) : 0)),
                                       ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
                                       JobName = ((dr["JobName"] != DBNull.Value ? dr["JobName"].ToString() : "")),
                                       JobAdd1 = ((dr["JobAdd1"] != DBNull.Value ? dr["JobAdd1"].ToString() : "")),
                                       JobAdd2 = ((dr["JobAdd2"] != DBNull.Value ? dr["JobAdd2"].ToString() : "")),
                                       JobCity = ((dr["JobCity"] != DBNull.Value ? dr["JobCity"].ToString() : "")),
                                       JobState = ((dr["JobState"] != DBNull.Value ? dr["JobState"].ToString() : "")),
                                       JobZip = ((dr["JobZip"] != DBNull.Value ? dr["JobZip"].ToString() : "")),
                                       JobNum = ((dr["JobNum"] != DBNull.Value ? dr["JobNum"].ToString() : "")),
                                       RefNum = ((dr["RefNum"] != DBNull.Value ? dr["RefNum"].ToString() : "")),
                                       APNNum = ((dr["APNNum"] != DBNull.Value ? dr["APNNum"].ToString() : "")),
                                       EstBalance = ((dr["EstBalance"] != DBNull.Value ? Convert.ToDecimal(dr["EstBalance"]) : 0)),
                                       DateAssigned = ((dr["DateAssigned"] != DBNull.Value ? Convert.ToDateTime(dr["DateAssigned"]) : DateTime.MinValue)),
                                       StartDate = ((dr["StartDate"] != DBNull.Value ? Convert.ToDateTime(dr["StartDate"]) : DateTime.MinValue)),
                                       RevDate = ((dr["RevDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevDate"]) : DateTime.MinValue)),
                                       CanDate = ((dr["CanDate"] != DBNull.Value ? Convert.ToDateTime(dr["CanDate"]) : DateTime.MinValue)),
                                       EndDate = ((dr["EndDate"] != DBNull.Value ? Convert.ToDateTime(dr["EndDate"]) : DateTime.MinValue)),
                                       FileDate = ((dr["FileDate"] != DBNull.Value ? Convert.ToDateTime(dr["FileDate"]) : DateTime.MinValue)),
                                       LienDate = ((dr["LienDate"] != DBNull.Value ? Convert.ToDateTime(dr["ResendDate"]) : DateTime.MinValue)),
                                       ResendDate = ((dr["ResendDate"] != DBNull.Value ? Convert.ToDateTime(dr["NoticeSent"]) : DateTime.MinValue)),
                                       LienAssignDate = ((dr["LienAssignDate"] != DBNull.Value ? Convert.ToDateTime(dr["LienAssignDate"]) : DateTime.MinValue)),
                                       AmendedDate = ((dr["AmendedDate"] != DBNull.Value ? Convert.ToDateTime(dr["AmendedDate"]) : DateTime.MinValue)),
                                       VerifiedDate = ((dr["VerifiedDate"] != DBNull.Value ? Convert.ToDateTime(dr["VerifiedDate"]) : DateTime.MinValue)),
                                       NoticeSent = ((dr["NoticeSent"] != DBNull.Value ? Convert.ToDateTime(dr["NoticeSent"]) : DateTime.MinValue)),
                                       BookNum = ((dr["BookNum"] != DBNull.Value ? dr["BookNum"].ToString() : "")),
                                       PageNum = ((dr["PageNum"] != DBNull.Value ? dr["PageNum"].ToString() : "")),
                                       PONum = ((dr["PONum"] != DBNull.Value ? dr["PONum"].ToString() : "")),
                                       StatusCode = ((dr["StatusCode"] != DBNull.Value ? dr["StatusCode"].ToString() : "")),
                                       DeskNum = ((dr["DeskNum"] != DBNull.Value ? dr["DeskNum"].ToString() : "")),
                                       DeskNumId = ((dr["DeskNumId"] != DBNull.Value ? Convert.ToInt32(dr["DeskNumId"]) : 0)),
                                       StatusCodeId = ((dr["StatusCodeId"] != DBNull.Value ? Convert.ToInt32(dr["StatusCodeId"]) : 0)),
                                       ToDeskNumId = ((dr["ToDeskNumId"] != DBNull.Value ? Convert.ToInt32(dr["ToDeskNumId"]) : 0)),
                                       TitleDeskNumId = ((dr["TitleDeskNumId"] != DBNull.Value ? Convert.ToInt32(dr["TitleDeskNumId"]) : 0)),
                                       TODeskNum = ((dr["TODeskNum"] != DBNull.Value ? dr["TODeskNum"].ToString() : "")),
                                       JobCounty = ((dr["JobCounty"] != DBNull.Value ? dr["JobCounty"].ToString() : "")),
                                       BondNum = ((dr["BondNum"] != DBNull.Value ? dr["BondNum"].ToString() : "")),
                                       RecorderNum = ((dr["RecorderNum"] != DBNull.Value ? dr["RecorderNum"].ToString() : "")),
                                       SpecialInstruction = ((dr["SpecialInstruction"] != DBNull.Value ? dr["SpecialInstruction"].ToString() : "")),
                                       FolioNum = ((dr["FolioNum"] != DBNull.Value ? dr["FolioNum"].ToString() : "")),
                                       ReleaseDocNum = ((dr["ReleaseDocNum"] != DBNull.Value ? dr["ReleaseDocNum"].ToString() : "")),
                                       TitleDeskNum = ((dr["TitleDeskNum"] != DBNull.Value ? dr["TitleDeskNum"].ToString() : "")),
                                       Note = ((dr["Note"] != DBNull.Value ? dr["Note"].ToString() : "")),
                                       ReleaseFileDate = ((dr["ReleaseFileDate"] != DBNull.Value ? Convert.ToDateTime(dr["ReleaseFileDate"]) : DateTime.MinValue)),
                                       CreateDate = ((dr["CreateDate"] != DBNull.Value ? Convert.ToDateTime(dr["CreateDate"]) : DateTime.MinValue)),
                                       PropOwner = ((dr["PropOwner"] != DBNull.Value ? dr["PropOwner"].ToString() : "")),
                                       LenderName = ((dr["LenderName"] != DBNull.Value ? dr["LenderName"].ToString() : "")),
                                       MatchingKey = ((dr["MatchingKey"] != DBNull.Value ? dr["MatchingKey"].ToString() : "")),
                                       PrelimBox = ((dr["PrelimBox"] != DBNull.Value ? Convert.ToBoolean(dr["PrelimBox"]) : false)),
                                       RushOrder = ((dr["RushOrder"] != DBNull.Value ? Convert.ToBoolean(dr["RushOrder"]) : false)),
                                       VerifyJob = ((dr["VerifyJob"] != DBNull.Value ? Convert.ToBoolean(dr["VerifyJob"]) : false)),
                                       PODBox = ((dr["PODBox"] != DBNull.Value ? Convert.ToBoolean(dr["PODBox"]) : false)),
                                       FederalJob = ((dr["FederalJob"] != DBNull.Value ? Convert.ToBoolean(dr["FederalJob"]) : false)),
                                       ResidentialBox = ((dr["ResidentialBox"] != DBNull.Value ? Convert.ToBoolean(dr["ResidentialBox"]) : false)),
                                       GreenCard = ((dr["GreenCard"] != DBNull.Value ? Convert.ToBoolean(dr["GreenCard"]) : false)),
                                       MechanicLien = ((dr["MechanicLien"] != DBNull.Value ? Convert.ToBoolean(dr["MechanicLien"]) : false)),
                                       PublicJob = ((dr["PublicJob"] != DBNull.Value ? Convert.ToBoolean(dr["PublicJob"]) : false)),
                                       BatchJobId = ((dr["BatchJobId"] != DBNull.Value ? Convert.ToInt32(dr["BatchJobId"]) : 0)),
                                       BadAddressFlag = ((dr["BadAddressFlag"] != DBNull.Value ? Convert.ToInt32(dr["BadAddressFlag"]) : 0)),
                                       LienAmt = ((dr["LienAmt"] != DBNull.Value ? Convert.ToDecimal(dr["LienAmt"]) : 0)),
                                       RecordingFee = ((dr["RecordingFee"] != DBNull.Value ? Convert.ToDecimal(dr["RecordingFee"]) : 0)),
                                       RushOrderML = ((dr["RushOrderML"] != DBNull.Value ? Convert.ToBoolean(dr["RushOrderML"]) : false)),
                                       CanOrder = ((dr["CanOrder"] != DBNull.Value ? Convert.ToBoolean(dr["CanOrder"]) : false)),
                                       StopNoticeBox = ((dr["StopNoticeBox"] != DBNull.Value ? Convert.ToBoolean(dr["StopNoticeBox"]) : false)),
                                       LienRelease = ((dr["LienRelease"] != DBNull.Value ? Convert.ToBoolean(dr["LienRelease"]) : false)),
                                       SuretyBox = ((dr["SuretyBox"] != DBNull.Value ? Convert.ToBoolean(dr["SuretyBox"]) : false)),
                                       PaymentBond = ((dr["PaymentBond"] != DBNull.Value ? Convert.ToBoolean(dr["PaymentBond"]) : false)),
                                       ResendBox = ((dr["ResendBox"] != DBNull.Value ? Convert.ToBoolean(dr["ResendBox"]) : false)),
                                       JointCk = ((dr["JointCk"] != DBNull.Value ? Convert.ToBoolean(dr["JointCk"]) : false)),
                                       CustJobNum = ((dr["CustJobNum"] != DBNull.Value ? dr["CustJobNum"].ToString() : "")),
                                       ProblemMsg = ((dr["ProblemMsg"] != DBNull.Value ? dr["ProblemMsg"].ToString() : "")),
                                       GCJobNum = ((dr["GCJobNum"] != DBNull.Value ? dr["GCJobNum"].ToString() : "")),
                                       BranchNum = ((dr["BranchNum"] != DBNull.Value ? dr["BranchNum"].ToString() : "")),

                                       ReleaseRush = ((dr["ReleaseRush"] != DBNull.Value ? Convert.ToBoolean(dr["ReleaseRush"]) : false)),
                                       MechLienRush = ((dr["MechLienRush"] != DBNull.Value ? Convert.ToBoolean(dr["MechLienRush"]) : false)),
                                       LienExtBox = ((dr["LienExtBox"] != DBNull.Value ? Convert.ToBoolean(dr["LienExtBox"]) : false)),
                                       ResendOwner = ((dr["ResendOwner"] != DBNull.Value ? Convert.ToBoolean(dr["ResendOwner"]) : false)),
                                       ResendGC = ((dr["ResendGC"] != DBNull.Value ? Convert.ToBoolean(dr["ResendGC"]) : false)),
                                       ResendLender = ((dr["ResendLender"] != DBNull.Value ? Convert.ToBoolean(dr["ResendLender"]) : false)),
                                       ResendCust = ((dr["ResendCust"] != DBNull.Value ? Convert.ToBoolean(dr["ResendCust"]) : false)),
                                       BackupBox = ((dr["BackupBox"] != DBNull.Value ? Convert.ToBoolean(dr["BackupBox"]) : false)),
                                       NOIBox = ((dr["NOIBox"] != DBNull.Value ? Convert.ToBoolean(dr["NOIBox"]) : false)),
                                       AmendedBox = ((dr["AmendedBox"] != DBNull.Value ? Convert.ToBoolean(dr["AmendedBox"]) : false)),
                                       ProbRequest2 = ((dr["ProbRequest2"] != DBNull.Value ? Convert.ToBoolean(dr["ProbRequest2"]) : false)),
                                       ProbRequest3 = ((dr["ProbRequest3"] != DBNull.Value ? Convert.ToBoolean(dr["ProbRequest3"]) : false)),
                                       TXSub90 = ((dr["TXSub90"] != DBNull.Value ? Convert.ToBoolean(dr["TXSub90"]) : false)),
                                       TXSubSub60 = ((dr["TXSubSub60"] != DBNull.Value ? Convert.ToBoolean(dr["TXSubSub60"]) : false)),
                                       BondReleaseBox = ((dr["BondReleaseBox"] != DBNull.Value ? Convert.ToBoolean(dr["BondReleaseBox"]) : false)),
                                       SNReleaseBox = ((dr["SNReleaseBox"] != DBNull.Value ? Convert.ToBoolean(dr["SNReleaseBox"]) : false)),
                                       TitleVerifiedBox = ((dr["TitleVerifiedBox"] != DBNull.Value ? Convert.ToBoolean(dr["TitleVerifiedBox"]) : false)),
                                       SameAsGC = ((dr["SameAsGC"] != DBNull.Value ? Convert.ToBoolean(dr["SameAsGC"]) : false)),
                                       FATHit = ((dr["FATHit"] != DBNull.Value ? Convert.ToBoolean(dr["FATHit"]) : false)),
                                       SendAsIsBox = ((dr["SendAsIsBox"] != DBNull.Value ? Convert.ToBoolean(dr["SendAsIsBox"]) : false)),
                                       SameAsCust = ((dr["SameAsCust"] != DBNull.Value ? Convert.ToBoolean(dr["SameAsCust"]) : false)),
                                       NCInfo = ((dr["NCInfo"] != DBNull.Value ? Convert.ToBoolean(dr["NCInfo"]) : false)),
                                       PrivateJob = ((dr["PrivateJob"] != DBNull.Value ? Convert.ToBoolean(dr["PrivateJob"]) : false)),
                                       SameAsOwner = ((dr["SameAsOwner"] != DBNull.Value ? Convert.ToBoolean(dr["SameAsOwner"]) : false)),
                                       ReleaseDate = ((dr["ReleaseDate"] != DBNull.Value ? Convert.ToDateTime(dr["ReleaseDate"]) : DateTime.MinValue)),
                                       BondDate = ((dr["BondDate"] != DBNull.Value ? Convert.ToDateTime(dr["BondDate"]) : DateTime.MinValue)),
                                       PODDate = ((dr["PODDate"] != DBNull.Value ? Convert.ToDateTime(dr["PODDate"]) : DateTime.MinValue)),
                                       LienExtDate = ((dr["LienExtDate"] != DBNull.Value ? Convert.ToDateTime(dr["LienExtDate"]) : DateTime.MinValue)),
                                       SNDate = ((dr["SNDate"] != DBNull.Value ? Convert.ToDateTime(dr["SNDate"]) : DateTime.MinValue)),
                                       NOIDate = ((dr["NOIDate"] != DBNull.Value ? Convert.ToDateTime(dr["NOIDate"]) : DateTime.MinValue)),
                                       BondReleaseDate = ((dr["BondReleaseDate"] != DBNull.Value ? Convert.ToDateTime(dr["BondReleaseDate"]) : DateTime.MinValue)),
                                       SNReleaseDate = ((dr["SNReleaseDate"] != DBNull.Value ? Convert.ToDateTime(dr["SNReleaseDate"]) : DateTime.MinValue)),
                                       NOIAssignDate = ((dr["NOIAssignDate"] != DBNull.Value ? Convert.ToDateTime(dr["NOIAssignDate"]) : DateTime.MinValue)),
                                       SNAssignDate = ((dr["SNAssignDate"] != DBNull.Value ? Convert.ToDateTime(dr["SNAssignDate"]) : DateTime.MinValue)),
                                       BondAssignDate = ((dr["BondAssignDate"] != DBNull.Value ? Convert.ToDateTime(dr["BondAssignDate"]) : DateTime.MinValue)),
                                       SNReleaseAssign = ((dr["SNReleaseAssign"] != DBNull.Value ? Convert.ToDateTime(dr["SNReleaseAssign"]) : DateTime.MinValue)),
                                       BCReleaseAssign = ((dr["BCReleaseAssign"] != DBNull.Value ? Convert.ToDateTime(dr["BCReleaseAssign"]) : DateTime.MinValue)),
                                       LienReleaseAssign = ((dr["LienReleaseAssign"] != DBNull.Value ? Convert.ToDateTime(dr["LienReleaseAssign"]) : DateTime.MinValue)),
                                       LEAssignDate = ((dr["LEAssignDate"] != DBNull.Value ? Convert.ToDateTime(dr["LEAssignDate"]) : DateTime.MinValue)),
                                       LienDDD = ((dr["LienDDD"] != DBNull.Value ? Convert.ToDateTime(dr["LienDDD"]) : DateTime.MinValue)),
                                       BondDDD = ((dr["BondDDD"] != DBNull.Value ? Convert.ToDateTime(dr["BondDDD"]) : DateTime.MinValue)),
                                       SNDDD = ((dr["SNDDD"] != DBNull.Value ? Convert.ToDateTime(dr["SNDDD"]) : DateTime.MinValue)),
                                       FATDate = ((dr["FATDate"] != DBNull.Value ? Convert.ToDateTime(dr["FATDate"]) : DateTime.MinValue)),
                                       LastNoticePrintDate = ((dr["LastNoticePrintDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastNoticePrintDate"]) : DateTime.MinValue)),
                                       SubmittedOn = ((dr["SubmittedOn"] != DBNull.Value ? Convert.ToDateTime(dr["SubmittedOn"]) : DateTime.MinValue)),
                                       WebCRSLienNum = ((dr["WebCRSLienNum"] != DBNull.Value ? Convert.ToInt32(dr["WebCRSLienNum"]) : 0)),
                                       CustId = ((dr["CustId"] != DBNull.Value ? Convert.ToInt32(dr["CustId"]) : 0)),
                                       GenId = ((dr["GenId"] != DBNull.Value ? Convert.ToInt32(dr["GenId"]) : 0)),
                                       OwnrId = ((dr["OwnrId"] != DBNull.Value ? Convert.ToInt32(dr["OwnrId"]) : 0)),
                                       LenderId = ((dr["LenderId"] != DBNull.Value ? Convert.ToInt32(dr["LenderId"]) : 0)),
                                       DesigneeId = ((dr["DesigneeId"] != DBNull.Value ? Convert.ToInt32(dr["DesigneeId"]) : 0)),
                                       BatchJobSiteId = ((dr["BatchJobSiteId"] != DBNull.Value ? Convert.ToInt32(dr["BatchJobSiteId"]) : 0)),
                                       ParentJobId = ((dr["ParentJobId"] != DBNull.Value ? Convert.ToInt32(dr["ParentJobId"]) : 0)),
                                       JVDeskNumId = ((dr["JVDeskNumId"] != DBNull.Value ? Convert.ToInt32(dr["JVDeskNumId"]) : 0)),
                                       JVStatusCodeId = ((dr["JVStatusCodeId"] != DBNull.Value ? Convert.ToInt32(dr["JVStatusCodeId"]) : 0)),
                                       SubmittedByUserId = ((dr["SubmittedByUserId"] != DBNull.Value ? Convert.ToInt32(dr["SubmittedByUserId"]) : 0)),
                                       LastUpdatedById = ((dr["LastUpdatedById"] != DBNull.Value ? Convert.ToInt32(dr["LastUpdatedById"]) : 0)),
                                       NTOClientId = ((dr["NTOClientId"] != DBNull.Value ? Convert.ToInt32(dr["NTOClientId"]) : 0)),
                                       CountyRecorderNum = ((dr["CountyRecorderNum"] != DBNull.Value ? dr["CountyRecorderNum"].ToString() : "")),
                                       EquipRate = ((dr["EquipRate"] != DBNull.Value ? dr["EquipRate"].ToString() : "")),
                                       EquipRental = ((dr["EquipRental"] != DBNull.Value ? dr["EquipRental"].ToString() : "")),
                                       CustCertNum = ((dr["CustCertNum"] != DBNull.Value ? dr["CustCertNum"].ToString() : "")),
                                       GCCertNum = ((dr["GCCertNum"] != DBNull.Value ? dr["GCCertNum"].ToString() : "")),
                                       OwnerCertNum = ((dr["OwnerCertNum"] != DBNull.Value ? dr["OwnerCertNum"].ToString() : "")),
                                       LenderCertNum = ((dr["LenderCertNum"] != DBNull.Value ? dr["LenderCertNum"].ToString() : "")),
                                       AZLotAllocate = ((dr["AZLotAllocate"] != DBNull.Value ? dr["AZLotAllocate"].ToString() : "")),
                                       PlacementSource = ((dr["PlacementSource"] != DBNull.Value ? dr["PlacementSource"].ToString() : "")),
                                       SubmittedBy = ((dr["SubmittedBy"] != DBNull.Value ? dr["SubmittedBy"].ToString() : "")),
                                       LastUpdatedBy = ((dr["LastUpdatedBy"] != DBNull.Value ? dr["LastUpdatedBy"].ToString() : "")),
                                       RANum = ((dr["RANum"] != DBNull.Value ? dr["RANum"].ToString() : "")),
                                       OrigJobAdd1 = ((dr["OrigJobAdd1"] != DBNull.Value ? dr["OrigJobAdd1"].ToString() : "")),
                                       BuildingPermitNum = ((dr["BuildingPermitNum"] != DBNull.Value ? dr["BuildingPermitNum"].ToString() : "")),
                                       JobAlertBox = ((dr["JobAlertBox"] != DBNull.Value ? Convert.ToBoolean(dr["JobAlertBox"]) : false)),
                                       CancelML = ((dr["CancelML"] != DBNull.Value ? Convert.ToBoolean(dr["CancelML"]) : false)),
                                       FaxReceived = ((dr["FaxReceived"] != DBNull.Value ? Convert.ToBoolean(dr["FaxReceived"]) : false)),
                                       OwnerSourceFAT = ((dr["OwnerSourceFAT"] != DBNull.Value ? Convert.ToBoolean(dr["OwnerSourceFAT"]) : false)),
                                       OwnerSourceAccurint = ((dr["OwnerSourceAccurint"] != DBNull.Value ? Convert.ToBoolean(dr["OwnerSourceAccurint"]) : false)),
                                       OwnerSourceCust = ((dr["OwnerSourceCust"] != DBNull.Value ? Convert.ToBoolean(dr["OwnerSourceCust"]) : false)),
                                       OwnerSourceCustFax = ((dr["OwnerSourceCustFax"] != DBNull.Value ? Convert.ToBoolean(dr["OwnerSourceCustFax"]) : false)),
                                       OwnerSourceGC = ((dr["OwnerSourceGC"] != DBNull.Value ? Convert.ToBoolean(dr["OwnerSourceGC"]) : false)),
                                       OwnerSourceGCFax = ((dr["OwnerSourceGCFax"] != DBNull.Value ? Convert.ToBoolean(dr["OwnerSourceGCFax"]) : false)),
                                       OwnerSourceAccessor = ((dr["OwnerSourceAccessor"] != DBNull.Value ? Convert.ToBoolean(dr["OwnerSourceAccessor"]) : false)),
                                       OwnerSourceClient = ((dr["OwnerSourceClient"] != DBNull.Value ? Convert.ToBoolean(dr["OwnerSourceClient"]) : false)),
                                       MultipleOwnerBox = ((dr["MultipleOwnerBox"] != DBNull.Value ? Convert.ToBoolean(dr["MultipleOwnerBox"]) : false)),
                                       ProbRequest1 = ((dr["ProbRequest1"] != DBNull.Value ? Convert.ToBoolean(dr["ProbRequest1"]) : false)),
                                       ManagerReviewed = ((dr["ManagerReviewed"] != DBNull.Value ? Convert.ToBoolean(dr["ManagerReviewed"]) : false)),
                                       IsBillableLegalDesc = ((dr["IsBillableLegalDesc"] != DBNull.Value ? Convert.ToBoolean(dr["IsBillableLegalDesc"]) : false)),
                                       NOCompBox = ((dr["NOCompBox"] != DBNull.Value ? Convert.ToBoolean(dr["NOCompBox"]) : false)),
                                       IsDataEntryFee = ((dr["IsDataEntryFee"] != DBNull.Value ? Convert.ToBoolean(dr["IsDataEntryFee"]) : false)),

                                       LastUpdatedOn = ((dr["LastUpdatedOn"] != DBNull.Value ? Convert.ToDateTime(dr["LastUpdatedOn"]) : DateTime.MinValue)),
                                       NOCDate = ((dr["NOCDate"] != DBNull.Value ? Convert.ToDateTime(dr["NOCDate"]) : DateTime.MinValue)),
                                       JobCompletionDate = ((dr["JobCompletionDate"] != DBNull.Value ? Convert.ToDateTime(dr["JobCompletionDate"]) : DateTime.MinValue)),
                                       LienDeadlineDate = ((dr["LienDeadlineDate"] != DBNull.Value ? Convert.ToDateTime(dr["LienDeadlineDate"]) : DateTime.MinValue)),
                                       BondDeadlineDate = ((dr["BondDeadlineDate"] != DBNull.Value ? Convert.ToDateTime(dr["BondDeadlineDate"]) : DateTime.MinValue)),
                                       SNDeadlineDate = ((dr["SNDeadlineDate"] != DBNull.Value ? Convert.ToDateTime(dr["SNDeadlineDate"]) : DateTime.MinValue)),
                                       NOIDeadlineDate = ((dr["NOIDeadlineDate"] != DBNull.Value ? Convert.ToDateTime(dr["NOIDeadlineDate"]) : DateTime.MinValue)),
                                       NOCRecDate = ((dr["NOCRecDate"] != DBNull.Value ? Convert.ToDateTime(dr["NOCRecDate"]) : DateTime.MinValue)),
                                       PIFDate = ((dr["PIFDate"] != DBNull.Value ? Convert.ToDateTime(dr["PIFDate"]) : DateTime.MinValue)),
                                       CloseDate = ((dr["CloseDate"] != DBNull.Value ? Convert.ToDateTime(dr["CloseDate"]) : DateTime.MinValue)),
                                       NoticeDeadlineDate = ((dr["NoticeDeadlineDate"] != DBNull.Value ? Convert.ToDateTime(dr["NoticeDeadlineDate"]) : DateTime.MinValue)),
                                       ForeclosureDeadlineDate = ((dr["ForeclosureDeadlineDate"] != DBNull.Value ? Convert.ToDateTime(dr["ForeclosureDeadlineDate"]) : DateTime.MinValue)),
                                       BondSuitDeadlineDate = ((dr["BondSuitDeadlineDate"] != DBNull.Value ? Convert.ToDateTime(dr["BondSuitDeadlineDate"]) : DateTime.MinValue)),
                                       ForeclosureDate = ((dr["ForeclosureDate"] != DBNull.Value ? Convert.ToDateTime(dr["ForeclosureDate"]) : DateTime.MinValue)),
                                       BondSuitDate = ((dr["BondSuitDate"] != DBNull.Value ? Convert.ToDateTime(dr["BondSuitDate"]) : DateTime.MinValue)),

                                       JVDeskNum = ((dr["JVDeskNum"] != DBNull.Value ? dr["JVDeskNum"].ToString() : "")),
                                       JVStatusCode = ((dr["JVStatusCode"] != DBNull.Value ? dr["JVStatusCode"].ToString() : "")),
                                       MaterialDescription = ((dr["MaterialDescription"] != DBNull.Value ? dr["MaterialDescription"].ToString() : "")),
                                       NOCDocNum = ((dr["NOCDocNum"] != DBNull.Value ? dr["NOCDocNum"].ToString() : "")),
                                       FilterKey = ((dr["FilterKey"] != DBNull.Value ? dr["FilterKey"].ToString() : "")),
                                       CollectorName = ((dr["CollectorName"] != DBNull.Value ? dr["CollectorName"].ToString() : "")),
                                       CollectorPhoneNo = ((dr["CollectorPhoneNo"] != DBNull.Value ? dr["CollectorPhoneNo"].ToString() : "")),

                                       VerifyOWOnly = ((dr["VerifyOWOnly"] != DBNull.Value ? Convert.ToBoolean(dr["VerifyOWOnly"]) : false)),
                                       VerifyOWOnlySend = ((dr["VerifyOWOnlySend"] != DBNull.Value ? Convert.ToBoolean(dr["VerifyOWOnlySend"]) : false)),
                                       VerifyOWOnlyTX = ((dr["VerifyOWOnlyTX"] != DBNull.Value ? Convert.ToBoolean(dr["VerifyOWOnlyTX"]) : false)),
                                       IsJobView = ((dr["IsJobView"] != DBNull.Value ? Convert.ToBoolean(dr["IsJobView"]) : false)),
                                       JobViewService = ((dr["JobViewService"] != DBNull.Value ? Convert.ToBoolean(dr["JobViewService"]) : false)),
                                       IsReviewNote = ((dr["IsReviewNote"] != DBNull.Value ? Convert.ToBoolean(dr["IsReviewNote"]) : false)),
                                       NoNTO = ((dr["NoNTO"] != DBNull.Value ? Convert.ToBoolean(dr["NoNTO"]) : false)),

                                       MaterialPrice = ((dr["MaterialPrice"] != DBNull.Value ? Convert.ToDecimal(dr["MaterialPrice"]) : 0)),
                                       JobBalance = ((dr["JobBalance"] != DBNull.Value ? Convert.ToDecimal(dr["JobBalance"]) : 0)),
                                       JobOrigBalance = ((dr["JobOrigBalance"] != DBNull.Value ? Convert.ToDecimal(dr["JobOrigBalance"]) : 0)),
                                       MergedOrigBalance = ((dr["MergedOrigBalance"] != DBNull.Value ? Convert.ToDecimal(dr["MergedOrigBalance"]) : 0)),
                                       MergedJobBalance = ((dr["MergedJobBalance"] != DBNull.Value ? Convert.ToDecimal(dr["MergedJobBalance"]) : 0)),
                                       MergedEstBalance = ((dr["MergedEstBalance"] != DBNull.Value ? Convert.ToDecimal(dr["MergedEstBalance"]) : 0)),
                                       RbtJobType = ((dr["RbtJobType"] != DBNull.Value ? Convert.ToInt32(dr["RbtJobType"]) : 0)),

                                   }).ToList();

            return JobList.First();


        }

        //ProcessJVTNNoticeRequest
        public static int ProcessJVTNNoticeRequest()
        {
            try
            {
                var mysprocAutoRequest = new uspbo_JobView_TNNoticeRequest();
                mysprocAutoRequest.JobState = "TN";
                DBO.Provider.ExecNonQuery(mysprocAutoRequest);

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        //ProcessJVTXNoticeRequest
        public static int ProcessJVTXNoticeRequest()
        {
            try
            {
                var mysprocAutoRequest = new uspbo_JobView_TXNoticeRequest();
                mysprocAutoRequest.JobState = "TX";
                DBO.Provider.ExecNonQuery(mysprocAutoRequest);

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        //PrelimNotices GetNoticesByDateRequested
        public static DataTable GetNoticesByDateRequested()
        {
            try
            {
                var mysproc = new uspbo_PrelimNotices_GetNoticesByDateRequested();

                mysproc.RunType = 2;
                DBO.Provider.ExecNonQuery(mysproc);

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(mysproc);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new DataTable());
            }
        }


        //Process JV TX Extract Letters
        public static int ProcessJVTXExtractLetters(JVTXExtractLettersVM objJVTXExtractLettersVM)
        {
            try
            {
                TableView MYVIEW;

                var mysproc = new uspbo_PrelimNotices_GetNoticesByDateRequested();
                mysproc.RunType = 0;
                mysproc.IsTXNotice = true;
                mysproc.ThruDate = objJVTXExtractLettersVM.ThruDate;
                mysproc.JobId = objJVTXExtractLettersVM.JobId;
                mysproc.IsJobview = true;

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                MYVIEW = GetRequestedPrelimNotices(user, mysproc, objJVTXExtractLettersVM.chkAssignCerts);

                mysproc.RunType = 2;
                MYVIEW = DBO.Provider.GetTableView(mysproc);

                return MYVIEW.Count;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return -1;
            }
        }

        //Process JV TX Extract Letters
        public static int ProcessNoticeCycleExtractLetters(NoticeCycleExtractVM objNoticeCycleExtractVM)
        {
            try
            {
                TableView MYVIEW;

                var mysproc = new uspbo_PrelimNotices_GetNoticesByDateRequested();
                mysproc.RunType = 0;
                mysproc.ThruDate = objNoticeCycleExtractVM.ThruDate;

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                MYVIEW = GetNoticeCycleExtractRequestedPrelimNotices(user, mysproc, objNoticeCycleExtractVM.chkAssignCerts);

                mysproc.RunType = 2;
                MYVIEW = DBO.Provider.GetTableView(mysproc);

                return MYVIEW.Count;
            }
            catch (Exception ex)
            {
                if (ex.Message == "USPSCertNum TABLE EMPTY.")
                {
                    return -1;
                }
                else if (ex.Message == "ERROR READING JOBNOTICEHISTORY.")
                {
                    return -2;
                }
                else if (ex.Message == "MAXIMUM NUMBER OF CERT CODES REACHED.")
                {
                    return -3;
                }
                else if (ex.Message == "NOT ENOUGH DIGITS FOR THE SERVICE TYPE CODE.")
                {
                    return -4;
                }
                else
                {
                    return -0;
                }
               
            }
        }

        public static TableView GetRequestedPrelimNotices(CRF.BLL.Users.CurrentUser auser, uspbo_PrelimNotices_GetNoticesByDateRequested asproc, bool ageneratecrt)
        {
            string mydir1 = auser.OutputFolder + "\\TEXASNOTICES\\";
            if (System.IO.Directory.Exists(mydir1) == false)
            {
                System.IO.Directory.CreateDirectory(mydir1);
            }

            if (asproc.RunType == 0)
            {
                CRF.BLL.Common.FileOps.DirectoryClean(mydir1, DateTime.MaxValue);
            }

            TableView aview = DBO.Provider.GetTableView(asproc);
            DataTable dt = aview.ToTable();
            if (ageneratecrt == true)
            {
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        if (Convert.ToInt32(row["DebtAddressTypeId"]) == 1)
                        {
                            string mynoticeid = Convert.ToString(row["JobNoticeHistoryId"]);
                            GetNextCERT(mynoticeid);
                        }
                    }
                }
            }

            asproc.RunType = 1;
            aview = DBO.Provider.GetTableView(asproc);
            aview.MoveFirst();
            string myspreadsheetpath = CRF.BLL.Common.FileOps.GetFileName(ref auser, "CERTFILE", ".CSV", true);
            myspreadsheetpath = CRF.BLL.CRFView.CRFView.GetFilePath(ref auser, "CERTFIEDEXTRACT1", "JVTXCERTIFIED.CSV");

            CRF.BLL.Export.ExportColumns mycols = new CRF.BLL.Export.ExportColumns();

            mycols.Add("SEQNO");
            mycols.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.JobId);
            mycols.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.CoverName);
            mycols.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.CoverAdd1);
            mycols.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.CoverAdd2);
            mycols.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.CoverCity);
            mycols.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.CoverState);
            mycols.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.CoverZip);
            mycols.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.BarCode);
            mycols.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.ClientCode);
            mycols.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.Greencard);

            DataView MyDataView = (DataView)aview;

            CRF.BLL.Export.Export.ExportToCSV(ref MyDataView, ref myspreadsheetpath, ref mycols, false);

            aview.MoveFirst();

            myspreadsheetpath = CRF.BLL.CRFView.CRFView.GetFilePath(ref auser, "CERTIFIEDEXTRACT", "JVTXCERTIFIED.CSV");
            myspreadsheetpath = CRF.BLL.CRFView.CRFView.GetFileName(ref auser, "CERTIFIEDEXTRACT", "JVTXCERTIFIED", ".CSV");

            MyDataView = (DataView)aview;
            CRF.BLL.Export.Export.ExportToCSV(ref MyDataView, ref myspreadsheetpath, ref mycols, false);

            return aview;
        }

        public static TableView GetNoticeCycleExtractRequestedPrelimNotices(CRF.BLL.Users.CurrentUser auser, uspbo_PrelimNotices_GetNoticesByDateRequested asproc, bool ageneratecrt)
        {
            string mydir1 = auser.OutputFolder + "\\PRELIMNOTICES\\";
            if (System.IO.Directory.Exists(mydir1) == false)
            {
                System.IO.Directory.CreateDirectory(mydir1);
            }

            //if (asproc.RunType == 0)
            if (asproc.RunType == 2)
            {
                CRF.BLL.Common.FileOps.DirectoryClean(mydir1, DateTime.MaxValue);
            }

            TableView aview = DBO.Provider.GetTableView(asproc);
            DataTable dt = aview.ToTable();
            if (ageneratecrt == true)
            {
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        //if (Convert.ToInt32(row["DebtAddressTypeId"]) == 1)
                        //{
                            string mynoticeid = Convert.ToString(row["JobNoticeHistoryId"]);
                            GetNextCERT(mynoticeid);
                        //}
                    }
                }
            }

            asproc.RunType = 1;
            aview = DBO.Provider.GetTableView(asproc);
            aview.MoveFirst();
            string myspreadsheetpath = CRF.BLL.Common.FileOps.GetFileName(ref auser, "CERTFILE", ".CSV", true);
            myspreadsheetpath = CRF.BLL.CRFView.CRFView.GetFilePath(ref auser, "CERTFIEDEXTRACT1", "CERTIFIED.CSV");

            CRF.BLL.Export.ExportColumns mycols = new CRF.BLL.Export.ExportColumns();

            mycols.Add("SEQNO");
            mycols.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.JobId);
            mycols.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.CoverName);
            mycols.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.CoverAdd1);
            mycols.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.CoverAdd2);
            mycols.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.CoverCity);
            mycols.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.CoverState);
            mycols.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.CoverZip);
            mycols.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.BarCode);
            mycols.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.ClientCode);
            mycols.Add(CRF.BLL.CRFDB.VIEWS.vwJobNoticeRequestHistory.ColumnNames.Greencard);

            DataView MyDataView = (DataView)aview;

            CRF.BLL.Export.Export.ExportToCSV(ref MyDataView, ref myspreadsheetpath, ref mycols, false);

            aview.MoveFirst();

            //myspreadsheetpath = CRF.BLL.CRFView.CRFView.GetFilePath(ref auser, "CERTIFIEDEXTRACT", "JVTXCERTIFIED.CSV");
            myspreadsheetpath = CRF.BLL.CRFView.CRFView.GetFileName(ref auser, "CERTIFIEDEXTRACT", "CERTIFIED", ".CSV");

            MyDataView = (DataView)aview;
            CRF.BLL.Export.Export.ExportToCSV(ref MyDataView, ref myspreadsheetpath, ref mycols, false);

            return aview;
        }

        public static int GetNextCERT(string anoticeid)
        {
            //try
            //{
                //CHECK IF THIS NOTICE ALREADY HAS A CERT
                USPSCertNum myusscodes = new USPSCertNum();
            AutoMapper.Mapper.CreateMap<ADUSPSCertNum, USPSCertNum>();
            ITable tblUSPSCertNum;
            HDS.DAL.COMMON.KeyValue li = new HDS.DAL.COMMON.KeyValue();
            ADUSPSCertNum objADUSPSCertNumOriginal = ADUSPSCertNum.ReadUSPSCertNum(1);
            myusscodes = AutoMapper.Mapper.Map<USPSCertNum>(objADUSPSCertNumOriginal);
            if (myusscodes.Id < 1)
            {
                throw new Exception("USPSCertNum TABLE EMPTY.");
            }

            JobNoticeHistory myhistory = new JobNoticeHistory();
            AutoMapper.Mapper.CreateMap<ADJobNoticeHistory, JobNoticeHistory>();
            ITable tblJobNoticeHistory;
            ADJobNoticeHistory objADJobNoticeHistoryOriginal = ADJobNoticeHistory.ReadJobNoticeHistory(Convert.ToInt32(anoticeid));
            myhistory = AutoMapper.Mapper.Map<JobNoticeHistory>(objADJobNoticeHistoryOriginal);
            if (myhistory.Id != Convert.ToInt32(anoticeid))
            {
                throw new Exception("ERROR READING JOBNOTICEHISTORY.");
            }

            myhistory.CertNum = "";

            string strAI;
            string strServiceTypeCode;
            string strSeqNo;
            string strMailerId;
            long lngMaxSeqNo;
            object[,] vntCode = new object[2, 22];
            int x;
            string strCode;
            int y = 0, z = 0;
            string strTmp;
            for (x = 0; x < 22; x++)
            {
                vntCode[0, x] = -(x + 1) + 23;
            }

            string str;

            str = "000000000" + myusscodes.ApplicationIdentifier;
            strAI = str.Substring(str.Length - 2, 2);

            str = "000000000" + myusscodes.ServiceTypeCode;
            strServiceTypeCode = str.Substring(str.Length - 3, 3);

            str = "000000" + myusscodes.MailerId;
            strMailerId = str.Substring(str.Length - 6, 6);


            str = "0000000000" + myusscodes.SeqNo;
            strSeqNo = str.Substring(str.Length - 10, 10);

            lngMaxSeqNo = Convert.ToInt64(myusscodes.MaxSeqNo);

            if (Convert.ToInt64(strSeqNo) > lngMaxSeqNo)
            {
                throw new Exception("MAXIMUM NUMBER OF CERT CODES REACHED.");
            }

            strCode = strAI + strServiceTypeCode + strMailerId + strSeqNo;

            //Make sure that we have 21 Digits
            if (strCode.Length != 21)
            {
                throw new Exception("NOT ENOUGH DIGITS FOR THE SERVICE TYPE CODE.");
            }

            //Add the Code to the Array
            for (x = 0; x < 21; x++)
            {
                vntCode[1, x] = strCode.Substring(x, 1);
            }

            //Now we'll add up the even codes
            for (x = 0; x < 21; x++)
            {
                switch (x)
                {
                    case 0:
                    case 2:
                    case 4:
                    case 6:
                    case 8:
                    case 10:
                    case 12:
                    case 14:
                    case 16:
                    case 18:
                    case 20:
                        y = y + Convert.ToInt32(vntCode[1, x]);
                        break;
                }
            }

            y = y * 3;

            //Now we'll add up the odd codes
            for (x = 0; x < 21; x++)
            {
                switch (x)
                {
                    case 1:
                    case 3:
                    case 5:
                    case 7:
                    case 9:
                    case 11:
                    case 13:
                    case 15:
                    case 17:
                    case 19:
                        z = z + Convert.ToInt32(vntCode[1, x]);
                        break;
                }
            }

            z = z + y;

            //Get the right most number
            str = Convert.ToString(z);
            z = Convert.ToInt32(str.Substring(str.Length - 1, 1));
            //The Check digit is the number that added to z = 10

            if (z == 0)
            {
                z = 0;
            }
            else
            {
                z = 10 - z;
            }

            strTmp = strCode + z;

            //string s1 = strTmp.Substring(0, 4);
            //string s2 = strTmp.Substring(4, 4);
            //string s3 = strTmp.Substring(8, 4);
            ////string s4 = strTmp.Substring(12, 4);
            //string s5 = strTmp.Substring(12, 4);
            //string s6 = strTmp.Substring(16, 4);
            //string s7 = strTmp.Substring(20, 2);

            myhistory.CertNum = strTmp.Substring(0, 4) + " " + strTmp.Substring(4, 4) + " " + strTmp.Substring(8, 4) + " " + strTmp.Substring(12, 4) + " " + strTmp.Substring(16, 4) + " " + strTmp.Substring(20, 2);
            tblJobNoticeHistory = (ITable)myhistory;
            DBO.Provider.Update(ref tblJobNoticeHistory);
             
            myusscodes.SeqNo = (Convert.ToInt32(myusscodes.SeqNo) + 1).ToString();
            tblUSPSCertNum = (ITable)myusscodes;
            DBO.Provider.Update(ref tblUSPSCertNum);
            return 1;
        }


        //Request JV TX Close Cycle
        public static int RequestJVTXCloseCycle()
        {
            try
            {
                var mysproc = new uspbo_PrelimNotices_GetNoticesByDateRequested();
                mysproc.RunType = 3;
                DBO.Provider.ExecNonQuery(mysproc);

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        //PrelimNotices GetTXNoticesByDateRequested
        public static DataTable GetTXNoticesByDateRequested()
        {
            try
            {
                var mysproc = new uspbo_PrelimNotices_GetNoticesByDateRequested();
                mysproc.RunType = 2;
                mysproc.IsTXNotice = true;
                DBO.Provider.ExecNonQuery(mysproc);

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(mysproc);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new DataTable());
            }
        }

        //Variables for JV TX Print Letters
        static string mMajorGroup = string.Empty;
        static string mPrintGroup = string.Empty;
        static bool mPrintLog = false;

        static CRF.BLL.COMMON.PrintMode mPrintMode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
        static TableView MYVIEW;
        static StandardReport myreport = new StandardReport();

        // static CRF.BLL.Users.CurrentUser user = new CRF.BLL.Users.CurrentUser();
        static CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
        static CRF.BLL.Liens.Reports.JobPrelimNotice mynotice = new CRF.BLL.Liens.Reports.JobPrelimNotice(user);
        static string mydir;
        static string mydir1;
        static string mydir2;
        static string mydir3;
        static string myseq;
        static string MYPRINTERNAME = "";

        //Process JV TX Print Letters 
        public static string PrintJVTXLetters(JVTXPrintLettersVM objJVTXPrintLettersVM)
        {
            if (objJVTXPrintLettersVM.RbtPrintAction == "Print All")
            {
                objJVTXPrintLettersVM.RbtPrintAction = "1";
            }
            if (objJVTXPrintLettersVM.RbtPrintAction == "Print Remaning")
            {
                objJVTXPrintLettersVM.RbtPrintAction = "2";
            }
            if (objJVTXPrintLettersVM.RbtPrintAction == "Reprint Job")
            {
                objJVTXPrintLettersVM.RbtPrintAction = "3";
            }


            string path = user.SettingsFolder;
            //string ReportName = "Acc-Liens.flxr";
            string RunType = "2";
            //string JobId = objJVTXPrintLettersVM.JobId.ToString();


            string ParameterList = "JobId" + ',' + "RunType" + ',' + "chkNoMailLog" + ',' + "ReportDestination" + ','+"chkSendToPrinter" + ',' + "chkPrintToPdf";
            string ParameterValues = objJVTXPrintLettersVM.JobId.ToString() + ',' + RunType + ',' + objJVTXPrintLettersVM.chkNoMailLog + ',' + objJVTXPrintLettersVM.RbtPrintAction+','+ objJVTXPrintLettersVM.chkSendToPrinter+','+objJVTXPrintLettersVM.chkPrintToPdf;

            // ITable tblPrintReportQueue;
            //string RecentClientId;
            //Client recent = new Client();
            AutoMapper.Mapper.CreateMap<JVTXPrintLettersVM, PrintReportQueue>();
            //ObjSource.IsBranch = true;
            PrintReportQueue objPrintReportQueue = new PrintReportQueue();
            objPrintReportQueue = AutoMapper.Mapper.Map<PrintReportQueue>(objJVTXPrintLettersVM);
            objPrintReportQueue.RequestedDate = DateTime.Now;
            objPrintReportQueue.ReportName = "PrelimNotice.xml";
            objPrintReportQueue.ParameterList = ParameterList;
            objPrintReportQueue.ParameterValues = ParameterValues;
            //objPrintReportQueue.ChkCsvReport = objJVTXPrintLettersVM.chkCsv;
            objPrintReportQueue.ReportStoragePath = path;
            objPrintReportQueue.ReportType = objJVTXPrintLettersVM.RbtPrintAction;
            objPrintReportQueue.UserName = user.UserName;
            objPrintReportQueue.UserId = user.Id;
            objPrintReportQueue.UserEmail = user.Email;
            objPrintReportQueue.TabValue = "JVTXNoticeRequestPrintLetters";
            ITable tblPrintReportQueue = (ITable)objPrintReportQueue;
            DBO.Provider.Create(ref tblPrintReportQueue);



            try
            {
                uspbo_PrelimNotices_GetNoticesByDateRequested mysproc = new uspbo_PrelimNotices_GetNoticesByDateRequested();

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();


                mPrintMode = CRF.BLL.COMMON.PrintMode.PrintToPDF;

                if (objJVTXPrintLettersVM.RbtPrintAction == "Reprint Job")
                {
                    mysproc.JobId = objJVTXPrintLettersVM.JobId;
                }
                mysproc.RunType = 2;

                DataSet MYDS = CRF.BLL.Providers.DBO.Provider.GetDataSet(mysproc);
                MYVIEW = DBO.Provider.GetTableView(mysproc);
                DataTable dt = MYDS.Tables[0];

                if (MYDS.Tables[0].Rows.Count == 0)
                {
                    return "";
                }

                

                //mydir = user.OutputFolder + "\\TEXASNOTICES";


                //if (System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(mydir)))
                //{
                //    System.IO.Directory.Delete(System.IO.Path.GetDirectoryName(mydir), true);

                //}
                //System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(mydir));


                //mydir1 = mydir + "\\TEXASSINGLE";
                //if (System.IO.Directory.Exists(mydir1) == false)
                //{
                //    System.IO.Directory.CreateDirectory(mydir1);
                //}

                //if (dt.Rows.Count > 0)
                //{
                //    foreach (DataRow row in dt.Rows)
                //    {
                //        myseq = Convert.ToString(dt.Rows.IndexOf(row)) + 1;
                //        string MYNOTICEID = Convert.ToString(row["JobNoticeHistoryId"]);
                //        string MYJOBID = Convert.ToString(row["JobId"]);
                //        string MYMAJORGROUP = Convert.ToString(row["PrintMajorGroup"]);
                //        string MYGROUP = Convert.ToString(row["PrintGroup"]);
                //        bool MYPRINTLOG = false;
                //        if (Convert.ToString(row["PrintMailingLog"]) != "")
                //        {
                //            MYPRINTLOG = Convert.ToBoolean(row["PrintMailingLog"]);
                //        }



                //        if (objJVTXPrintLettersVM.chkNoMailLog == false)
                //        {
                //            PRINTMAILLOG(MYMAJORGROUP, MYGROUP, MYPRINTLOG, objJVTXPrintLettersVM);
                //        }
                //        mydir3 = mydir;
                //        mydir3 += "GROUP" + MYMAJORGROUP + MYGROUP + "\\";

                //        PRINTNOTICE(MYJOBID, MYNOTICEID, MYMAJORGROUP, MYGROUP, objJVTXPrintLettersVM);
                //    }
                //}

                //if (objJVTXPrintLettersVM.chkNoMailLog == false && MYVIEW.Count > 0)
                //{
                //    PRINTMAILLOG("ZZ", "ZZ", true, objJVTXPrintLettersVM);
                //}

                ////1 /5/2015 Added the Non-US Addr mailing log
                //TableView MYVIEW2 = DBO.Provider.GetTableView(mysproc);
                //DataTable dt2 = MYDS.Tables[2];
                //PrintNonUSLOG(user, MYVIEW2, mPrintMode, objJVTXPrintLettersVM);

                //TableView MYVIEW1 = DBO.Provider.GetTableView(mysproc);
                //DataTable dt1 = MYDS.Tables[1];
                //PrintGroup99LOG(user, MYVIEW1, mPrintMode, objJVTXPrintLettersVM);


                // return MYVIEW.Count;
                //return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return "error";
            }
            return "1";
        }

        public static string PrintTXNoticeLetters(JVTXPrintLettersVM objJVTXPrintLettersVM)
        {
            string path = user.SettingsFolder;
            //string ReportName = "Acc-Liens.flxr";
            string RunType = "2";
            //string JobId = objJVTXPrintLettersVM.JobId.ToString();
            if (objJVTXPrintLettersVM.RbtPrintAction == "Print All")
            {
                objJVTXPrintLettersVM.RbtPrintAction = "1";
            }
            if (objJVTXPrintLettersVM.RbtPrintAction == "Print Remaning")
            {
                objJVTXPrintLettersVM.RbtPrintAction = "2";
            }
            if (objJVTXPrintLettersVM.RbtPrintAction == "Reprint Job")
            {
                objJVTXPrintLettersVM.RbtPrintAction = "3";
            }
            string ParameterList = "JobId" + ',' + "RunType" + ',' + "chkNoMailLog" + "ReportDestination";
            string ParameterValues = objJVTXPrintLettersVM.JobId.ToString() + ',' + RunType + ',' + objJVTXPrintLettersVM.chkNoMailLog + ',' + objJVTXPrintLettersVM.RbtPrintAction;

            // ITable tblPrintReportQueue;
            //string RecentClientId;
            //Client recent = new Client();
            AutoMapper.Mapper.CreateMap<JVTXPrintLettersVM, PrintReportQueue>();
            //ObjSource.IsBranch = true;
            PrintReportQueue objPrintReportQueue = new PrintReportQueue();
            objPrintReportQueue = AutoMapper.Mapper.Map<PrintReportQueue>(objJVTXPrintLettersVM);
            objPrintReportQueue.RequestedDate = DateTime.Now;
            objPrintReportQueue.ReportName = "PrelimNotice.xml";
            objPrintReportQueue.TabValue = "TXNoticeCyclePrintLetters";
            objPrintReportQueue.ParameterList = ParameterList;
            objPrintReportQueue.ParameterValues = ParameterValues;
            //objPrintReportQueue.ChkCsvReport = objJVTXPrintLettersVM.chkCsv;
            objPrintReportQueue.ReportStoragePath = path;
            //objPrintReportQueue.ReportType = objJVTXPrintLettersVM.RbtPrintOption;
            objPrintReportQueue.UserName = user.UserName;
            objPrintReportQueue.UserId = user.Id;
            objPrintReportQueue.UserEmail = user.Email;
            ITable tblPrintReportQueue = (ITable)objPrintReportQueue;
            DBO.Provider.Create(ref tblPrintReportQueue);



            try
            {
                uspbo_PrelimNotices_GetNoticesByDateRequested mysproc = new uspbo_PrelimNotices_GetNoticesByDateRequested();

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();


                mPrintMode = CRF.BLL.COMMON.PrintMode.PrintToPDF;

                if (objJVTXPrintLettersVM.RbtPrintAction == "Reprint Job")
                {
                    mysproc.JobId = objJVTXPrintLettersVM.JobId;
                }
                mysproc.RunType = 2;

                DataSet MYDS = CRF.BLL.Providers.DBO.Provider.GetDataSet(mysproc);
                MYVIEW = DBO.Provider.GetTableView(mysproc);
                DataTable dt = MYDS.Tables[0];
                if (MYDS.Tables[0].Rows.Count == 0)
                {
                    return "";
                }

                //mydir = user.OutputFolder + "\\TEXASNOTICES";

                //if (System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(mydir)))
                //{
                //    System.IO.Directory.Delete(System.IO.Path.GetDirectoryName(mydir), true);

                //}
                //System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(mydir));


                //mydir1 = mydir + "\\TEXASSINGLE";
                //if (System.IO.Directory.Exists(mydir1) == false)
                //{
                //    System.IO.Directory.CreateDirectory(mydir1);
                //}

                //if (dt.Rows.Count > 0)
                //{
                //    foreach (DataRow row in dt.Rows)
                //    {
                //        myseq = Convert.ToString(dt.Rows.IndexOf(row)) + 1;
                //        string MYNOTICEID = Convert.ToString(row["JobNoticeHistoryId"]);
                //        string MYJOBID = Convert.ToString(row["JobId"]);
                //        string MYMAJORGROUP = Convert.ToString(row["PrintMajorGroup"]);
                //        string MYGROUP = Convert.ToString(row["PrintGroup"]);
                //        bool MYPRINTLOG = false;
                //        if (Convert.ToString(row["PrintMailingLog"]) != "")
                //        {
                //            MYPRINTLOG = Convert.ToBoolean(row["PrintMailingLog"]);
                //        }



                //        if (objJVTXPrintLettersVM.chkNoMailLog == false)
                //        {
                //            PRINTMAILLOG(MYMAJORGROUP, MYGROUP, MYPRINTLOG, objJVTXPrintLettersVM);
                //        }
                //        mydir3 = mydir;
                //        mydir3 += "GROUP" + MYMAJORGROUP + MYGROUP + "\\";

                //        PRINTNOTICE(MYJOBID, MYNOTICEID, MYMAJORGROUP, MYGROUP, objJVTXPrintLettersVM);
                //    }
                //}

                //if (objJVTXPrintLettersVM.chkNoMailLog == false && MYVIEW.Count > 0)
                //{
                //    PRINTMAILLOG("ZZ", "ZZ", true, objJVTXPrintLettersVM);
                //}

                ////1 /5/2015 Added the Non-US Addr mailing log
                //TableView MYVIEW2 = DBO.Provider.GetTableView(mysproc);
                //DataTable dt2 = MYDS.Tables[2];
                //PrintNonUSLOG(user, MYVIEW2, mPrintMode, objJVTXPrintLettersVM);

                //TableView MYVIEW1 = DBO.Provider.GetTableView(mysproc);
                //DataTable dt1 = MYDS.Tables[1];
                //PrintGroup99LOG(user, MYVIEW1, mPrintMode, objJVTXPrintLettersVM);


                //return MYVIEW.Count;
                return "1";
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return "-1";
            }
            return "1";
        }

        //Process Notice Cycle Print Letters 
        public static int PrintNoticeCycleLetters(JVTXPrintLettersVM objJVTXPrintLettersVM)
        {

            try
            {

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                string path = user.SettingsFolder;
                //string ReportName = "Acc-Liens.flxr";
                string RunType = "2";
                string JobId = objJVTXPrintLettersVM.JobId.ToString();

                if (objJVTXPrintLettersVM.RbtPrintAction == "Print All")
                {
                    objJVTXPrintLettersVM.RbtPrintAction = "1";
                }
                if (objJVTXPrintLettersVM.RbtPrintAction == "Print Remaning")
                {
                    objJVTXPrintLettersVM.RbtPrintAction = "2";
                }
                if (objJVTXPrintLettersVM.RbtPrintAction == "Reprint Job")
                {
                    objJVTXPrintLettersVM.RbtPrintAction = "3";
                }
                string ParameterList = "JobId" + ',' + "RunType" + ',' + "chkNoMailLog" + ',' + "ReportDestination" + ',' + "chkSendToPrinter" + ',' + "chkPrintToPdf";
                string ParameterValues = objJVTXPrintLettersVM.JobId.ToString() + ',' + RunType + ',' + objJVTXPrintLettersVM.chkNoMailLog + ',' + objJVTXPrintLettersVM.RbtPrintAction + ',' + objJVTXPrintLettersVM.chkSendToPrinter + ',' + objJVTXPrintLettersVM.chkPrintToPdf;
                //string ParameterList = "JobId" + ',' + "RunType" + ',' + "chkNoMailLog" + "ReportDestination";
                //string ParameterValues = objJVTXPrintLettersVM.JobId.ToString() + ',' + RunType + ',' + objJVTXPrintLettersVM.chkNoMailLog + ',' + objJVTXPrintLettersVM.RbtPrintAction;

                //ITable tblPrintReportQueue;
                string RecentClientId;
                Client recent = new Client();
                AutoMapper.Mapper.CreateMap<JVTXPrintLettersVM, PrintReportQueue>();
                //ObjSource.IsBranch = true;
                PrintReportQueue objPrintReportQueue = new PrintReportQueue();
                objPrintReportQueue = AutoMapper.Mapper.Map<PrintReportQueue>(objJVTXPrintLettersVM);
                objPrintReportQueue.RequestedDate = DateTime.Now;
                objPrintReportQueue.TabValue = "NoticeCyclePrintLetters";
                objPrintReportQueue.ReportName = "PrelimNotice.xml";;
                objPrintReportQueue.ParameterList = ParameterList;
                objPrintReportQueue.ParameterValues = ParameterValues;
                //objPrintReportQueue.ChkCsvReport = objJVTXPrintLettersVM.chkCsv;
                objPrintReportQueue.ReportStoragePath = path;
                objPrintReportQueue.ReportType = objJVTXPrintLettersVM.RbtPrintAction;
                objPrintReportQueue.UserName = user.UserName;
                objPrintReportQueue.UserEmail = user.Email;
                objPrintReportQueue.UserId = user.Id;
                ITable tblPrintReportQueue = (ITable)objPrintReportQueue;
                DBO.Provider.Create(ref tblPrintReportQueue);

                uspbo_PrelimNotices_GetNoticesByDateRequested mysproc = new uspbo_PrelimNotices_GetNoticesByDateRequested();

                //CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();


                mPrintMode = CRF.BLL.COMMON.PrintMode.PrintToPDF;

                if (objJVTXPrintLettersVM.RbtPrintAction == "Reprint Job")
                {
                    mysproc.JobId = objJVTXPrintLettersVM.JobId;
                }
                mysproc.RunType = 2;

                DataSet MYDS = CRF.BLL.Providers.DBO.Provider.GetDataSet(mysproc);
                MYVIEW = DBO.Provider.GetTableView(mysproc);
                DataTable dt = MYDS.Tables[0];

                if (MYDS.Tables[0].Rows.Count == 0)
                {
                    return 1;
                }
                //mydir = user.OutputFolder + "\\PRELIMNOTICES";

                //if (System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(mydir)))
                //{
                //    System.IO.Directory.Delete(System.IO.Path.GetDirectoryName(mydir), true);

                //}
                //System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(mydir));


                //mydir1 = mydir + "\\SINGLE";
                //if (System.IO.Directory.Exists(mydir1) == false)
                //{
                //    System.IO.Directory.CreateDirectory(mydir1);
                //}


                ////if (objJVTXPrintLettersVM.chkNoMailLog == false)
                ////{

                ////        string S = "PrintMajorGroup = '" + objJVTXPrintLettersVM.MajorGroup + "' And ";
                ////         S += " PrintGroup = '" + objJVTXPrintLettersVM.PrintGroup + "'";
                ////    MYVIEW.RowFilter = S;
                ////}

                //if (dt.Rows.Count > 0)
                //{
                //    foreach (DataRow row in dt.Rows)
                //    {
                //        myseq = Convert.ToString(dt.Rows.IndexOf(row)) + 1;
                //        string MYNOTICEID = Convert.ToString(row["JobNoticeHistoryId"]);
                //        string MYJOBID = Convert.ToString(row["JobId"]);
                //        string MYMAJORGROUP = Convert.ToString(row["PrintMajorGroup"]);
                //        string MYGROUP = Convert.ToString(row["PrintGroup"]);
                //        bool MYPRINTLOG = false;
                //        if (Convert.ToString(row["PrintMailingLog"]) != "")
                //        {
                //            MYPRINTLOG = Convert.ToBoolean(row["PrintMailingLog"]);
                //        }



                //        if (objJVTXPrintLettersVM.chkNoMailLog == false)
                //        {
                //            PRINTMAILLOG(MYMAJORGROUP, MYGROUP, MYPRINTLOG, objJVTXPrintLettersVM);
                //        }
                //        mydir3 = mydir;
                //        mydir3 += "GROUP" + MYMAJORGROUP + MYGROUP + "\\";

                //        NoticeCyclePRINTNOTICE(MYJOBID, MYNOTICEID, objJVTXPrintLettersVM);
                //    }
                //}



                //if (objJVTXPrintLettersVM.chkNoMailLog == false && MYVIEW.Count > 0)
                //{
                //    PRINTMAILLOG("ZZ", "ZZ", true, objJVTXPrintLettersVM);
                //}

                ////1 /5/2015 Added the Non-US Addr mailing log
                //TableView MYVIEW2 = DBO.Provider.GetTableView(mysproc);
                //DataTable dt2 = MYDS.Tables[2];
                //PrintNonUSLOG(user, MYVIEW2, mPrintMode, objJVTXPrintLettersVM);

                //TableView MYVIEW1 = DBO.Provider.GetTableView(mysproc);
                //DataTable dt1 = MYDS.Tables[1];
                //PrintGroup99LOG(user, MYVIEW1, mPrintMode, objJVTXPrintLettersVM);


                //return MYVIEW.Count;
                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return -1;
            }
        }





        public static int PrintTXLetters(JVTXPrintLettersVM objJVTXPrintLettersVM)
        {
            try
            {
                uspbo_PrelimNotices_GetNoticesByDateRequested mysproc = new uspbo_PrelimNotices_GetNoticesByDateRequested();

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();


                mPrintMode = CRF.BLL.COMMON.PrintMode.PrintToPDF;

                if (objJVTXPrintLettersVM.RbtPrintAction == "Reprint Job")
                {
                    mysproc.JobId = objJVTXPrintLettersVM.JobId;
                }
                mysproc.RunType = 2;

                DataSet MYDS = CRF.BLL.Providers.DBO.Provider.GetDataSet(mysproc);
                MYVIEW = DBO.Provider.GetTableView(mysproc);
                DataTable dt = MYDS.Tables[0];


                mydir = user.OutputFolder + "\\TEXASNOTICES";

                if (System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(mydir)))
                {
                    System.IO.Directory.Delete(System.IO.Path.GetDirectoryName(mydir), true);

                }
                System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(mydir));


                mydir1 = mydir + "\\TEXASSINGLE";
                if (System.IO.Directory.Exists(mydir1) == false)
                {
                    System.IO.Directory.CreateDirectory(mydir1);
                }

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        myseq = Convert.ToString(dt.Rows.IndexOf(row)) + 1;
                        string MYNOTICEID = Convert.ToString(row["JobNoticeHistoryId"]);
                        string MYJOBID = Convert.ToString(row["JobId"]);
                        string MYMAJORGROUP = Convert.ToString(row["PrintMajorGroup"]);
                        string MYGROUP = Convert.ToString(row["PrintGroup"]);
                        bool MYPRINTLOG = false;
                        if (Convert.ToString(row["PrintMailingLog"]) != "")
                        {
                            MYPRINTLOG = Convert.ToBoolean(row["PrintMailingLog"]);
                        }



                        if (objJVTXPrintLettersVM.chkNoMailLog == false)
                        {
                            PRINTMAILLOG(MYMAJORGROUP, MYGROUP, MYPRINTLOG, objJVTXPrintLettersVM);
                        }
                        mydir3 = mydir;
                        mydir3 += "GROUP" + MYMAJORGROUP + MYGROUP + "\\";

                        TXPRINTNOTICE(MYJOBID, MYNOTICEID, MYMAJORGROUP, MYGROUP, objJVTXPrintLettersVM);
                    }
                }

                if (objJVTXPrintLettersVM.chkNoMailLog == false && MYVIEW.Count > 0)
                {
                    PRINTMAILLOG("ZZ", "ZZ", true, objJVTXPrintLettersVM);
                }

                //1 /5/2015 Added the Non-US Addr mailing log
                TableView MYVIEW2 = DBO.Provider.GetTableView(mysproc);
                DataTable dt2 = MYDS.Tables[2];
                PrintNonUSLOG(user, MYVIEW2, mPrintMode, objJVTXPrintLettersVM);

                TableView MYVIEW1 = DBO.Provider.GetTableView(mysproc);
                DataTable dt1 = MYDS.Tables[1];
                PrintGroup99LOG(user, MYVIEW1, mPrintMode, objJVTXPrintLettersVM);


                return MYVIEW.Count;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return -1;
            }
        }

        ////Process Notice Cycle Print Letters 
        //public static int PrintNoticeCycleLetters(JVTXPrintLettersVM objJVTXPrintLettersVM)
        //{
        //    try
        //    {
        //        uspbo_PrelimNotices_GetNoticesByDateRequested mysproc = new uspbo_PrelimNotices_GetNoticesByDateRequested();

        //        CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();


        //        mPrintMode = CRF.BLL.COMMON.PrintMode.PrintToPDF;

        //        if (objJVTXPrintLettersVM.RbtPrintAction == "Reprint Job")
        //        {
        //            mysproc.JobId = objJVTXPrintLettersVM.JobId;
        //        }
        //        mysproc.RunType = 2;

        //        DataSet MYDS = CRF.BLL.Providers.DBO.Provider.GetDataSet(mysproc);
        //        MYVIEW = DBO.Provider.GetTableView(mysproc);
        //        DataTable dt = MYDS.Tables[0];


        //        mydir = user.OutputFolder + "\\PRELIMNOTICES";

        //        if (System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(mydir)))
        //        {
        //            System.IO.Directory.Delete(System.IO.Path.GetDirectoryName(mydir), true);

        //        }
        //        System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(mydir));


        //        mydir1 = mydir + "\\SINGLE";
        //        if (System.IO.Directory.Exists(mydir1) == false)
        //        {
        //            System.IO.Directory.CreateDirectory(mydir1);
        //        }


        //        //if (objJVTXPrintLettersVM.chkNoMailLog == false)
        //        //{

        //        //        string S = "PrintMajorGroup = '" + objJVTXPrintLettersVM.MajorGroup + "' And ";
        //        //         S += " PrintGroup = '" + objJVTXPrintLettersVM.PrintGroup + "'";
        //        //    MYVIEW.RowFilter = S;
        //        //}

        //        if (dt.Rows.Count > 0)
        //        {
        //            foreach (DataRow row in dt.Rows)
        //            {
        //                myseq = Convert.ToString(dt.Rows.IndexOf(row)) + 1;
        //                string MYNOTICEID = Convert.ToString(row["JobNoticeHistoryId"]);
        //                string MYJOBID = Convert.ToString(row["JobId"]);
        //                string MYMAJORGROUP = Convert.ToString(row["PrintMajorGroup"]);
        //                string MYGROUP = Convert.ToString(row["PrintGroup"]);
        //                bool MYPRINTLOG = false;
        //                if (Convert.ToString(row["PrintMailingLog"]) != "")
        //                {
        //                    MYPRINTLOG = Convert.ToBoolean(row["PrintMailingLog"]);
        //                }



        //                if (objJVTXPrintLettersVM.chkNoMailLog == false)
        //                {
        //                    PRINTMAILLOG(MYMAJORGROUP, MYGROUP, MYPRINTLOG, objJVTXPrintLettersVM);
        //                }
        //                mydir3 = mydir;
        //                mydir3 += "GROUP" + MYMAJORGROUP + MYGROUP + "\\";

        //               NoticeCyclePRINTNOTICE(MYJOBID, MYNOTICEID,objJVTXPrintLettersVM);
        //            }
        //        }



        //        if (objJVTXPrintLettersVM.chkNoMailLog == false && MYVIEW.Count > 0)
        //        {
        //            PRINTMAILLOG("ZZ", "ZZ", true, objJVTXPrintLettersVM);
        //        }

        //        //1 /5/2015 Added the Non-US Addr mailing log
        //        TableView MYVIEW2 = DBO.Provider.GetTableView(mysproc);
        //        DataTable dt2 = MYDS.Tables[2];
        //        PrintNonUSLOG(user, MYVIEW2, mPrintMode, objJVTXPrintLettersVM);

        //        TableView MYVIEW1 = DBO.Provider.GetTableView(mysproc);
        //        DataTable dt1 = MYDS.Tables[1];
        //        PrintGroup99LOG(user, MYVIEW1, mPrintMode, objJVTXPrintLettersVM);


        //        return MYVIEW.Count;
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLog.SendErrorToText(ex);
        //        return -1;
        //    }
        //}

        private static string PrintGroup99LOG(CRF.BLL.Users.CurrentUser auser, TableView MYVIEW, PrintMode aprintmode, JVTXPrintLettersVM objJVTXPrintLettersVM)
        {
            try
            {
                string myreportname = "PrelimException";
                string myreportprefix = "ERRORS";

                myreport.ReportDefPath = auser.SettingsFolder + "\\Reports\\LienReports.xml";
                myreport.OutputFolder = mydir;
                myreport.ReportName = myreportname;
                myreport.DataView = MYVIEW;
                myreport.PrintMode = aprintmode;
                myreport.Sort = "";
                myreport.FilePrefix = myreportprefix;
                myreport.RenderReport(false);

                if (objJVTXPrintLettersVM.chkSendToPrinter == true)
                {
                    PDFLIB.PrintPDF(MYPRINTERNAME, myreport.ReportFileName);
                }
                else
                {
                    PDFLIB.ViewPDF(myreport.ReportFileName);
                }

                return myreport.ReportFileName;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return "";
            }

        }
        private static string PrintNonUSLOG(CRF.BLL.Users.CurrentUser auser, TableView MYVIEW, PrintMode aprintmode, JVTXPrintLettersVM objJVTXPrintLettersVM)
        {
            try
            {

                string myreportname = "MailingLogNonUS";
                string myreportprefix = "ZZZZZZ";

                myreport.ReportDefPath = auser.SettingsFolder + "\\Reports\\PrelimNotice.xml";
                myreport.OutputFolder = mydir;
                myreport.ReportName = myreportname;
                myreport.DataView = MYVIEW;
                myreport.PrintMode = aprintmode;
                myreport.Sort = "";
                myreport.FilePrefix = myreportprefix;
                myreport.RenderReport(false);

                if (objJVTXPrintLettersVM.chkSendToPrinter == true)
                {
                    PDFLIB.PrintPDF(MYPRINTERNAME, myreport.ReportFileName);
                }
                else
                {
                    PDFLIB.ViewPDF(myreport.ReportFileName);
                }

                return myreport.ReportFileName;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return "";
            }
        }

        private static bool PRINTNOTICE(string MYJOBID, string MYNOTICEID, string MYMAJORGROUP, string MYGROUP, JVTXPrintLettersVM objJVTXPrintLettersVM)
        {
            try
            {
                ADJobNoticeHistory objADJobNoticeHistoryOriginal = ADJobNoticeHistory.ReadJobNoticeHistory(Convert.ToInt32(MYNOTICEID));

                if (objJVTXPrintLettersVM.RbtPrintAction == "Reprint Job")
                {
                    if (Convert.ToInt32(MYJOBID) != objJVTXPrintLettersVM.JobId)
                    {
                        return true;
                    }
                }
                return PrintPrelimNotice(user, MYNOTICEID, mPrintMode, objJVTXPrintLettersVM);
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }


        private static bool NoticeCyclePRINTNOTICE(string MYJOBID, string MYNOTICEID, JVTXPrintLettersVM objJVTXPrintLettersVM)
        {
            try
            {
                ADJobNoticeHistory objADJobNoticeHistoryOriginal = ADJobNoticeHistory.ReadJobNoticeHistory(Convert.ToInt32(MYNOTICEID));

                if (objJVTXPrintLettersVM.RbtPrintAction == "Reprint Job")
                {
                    if (Convert.ToInt32(MYJOBID) != objJVTXPrintLettersVM.JobId)
                    {
                        return true;
                    }
                }
                return PrintPrelimNotice(user, MYNOTICEID, mPrintMode, objJVTXPrintLettersVM);
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }

        private static bool TXPRINTNOTICE(string MYJOBID, string MYNOTICEID, string MYMAJORGROUP, string MYGROUP, JVTXPrintLettersVM objJVTXPrintLettersVM)
        {
            try
            {
                ADJobNoticeHistory objADJobNoticeHistoryOriginal = ADJobNoticeHistory.ReadJobNoticeHistory(Convert.ToInt32(MYNOTICEID));

                if (objJVTXPrintLettersVM.RbtPrintAction == "Reprint Job")
                {
                    if (Convert.ToInt32(MYJOBID) != objJVTXPrintLettersVM.JobId)
                    {
                        return true;
                    }
                }
                return TXPrintPrelimNotice(user, MYNOTICEID, mPrintMode, objJVTXPrintLettersVM);
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }

        private static bool TXPrintPrelimNotice(CRF.BLL.Users.CurrentUser auser, string anoticeid, PrintMode aprintmode, JVTXPrintLettersVM objJVTXPrintLettersVM)
        {
            try
            {
                JobNoticeRequestForms myform = new JobNoticeRequestForms();
                ITable tblI = myform;

                var mysproc = new uspbo_PrelimNotices_GetNoticeInfo();
                mysproc.JobNoticeHistoryId = Convert.ToInt32(anoticeid);
                DataSet myds = CRF.BLL.Providers.DBO.Provider.GetDataSet(mysproc);


                DataTable dtmyview = myds.Tables[0];
                DataTable dtmycover = myds.Tables[0];
                DataTable dtmyview3 = myds.Tables[2];

                DataView myview = dtmyview.DefaultView;
                DataView mycover = dtmycover.DefaultView;
                DataView myview3 = dtmyview3.DefaultView;

                DateTime DatePrinted = Convert.ToDateTime(dtmyview.Rows[0]["DatePrinted"]);
                string mydate = DatePrinted.ToString("yyyyMMdd");

                TableView myforms = new TableView(myds.Tables[1]);


                DataTable dtmyforms = myds.Tables[1];



                ArrayList myfiles = new ArrayList();

                if (Convert.ToBoolean(dtmyview.Rows[0]["IsCustomReportFile"]) == true)
                {
                    string sfilepath = auser.SettingsFolder + "\\Reports\\" + Convert.ToString(dtmyview.Rows[0]["CustomReportFile"]);
                    if (System.IO.File.Exists(sfilepath))
                    {
                        myreport.ReportDefPath = auser.SettingsFolder + "\\Reports\\" + Convert.ToString(dtmyview.Rows[0]["CustomReportFile"]);
                        mynotice.ReportDefPath = auser.SettingsFolder + "\\Reports\\" + Convert.ToString(dtmyview.Rows[0]["CustomReportFile"]);
                    }
                    else
                    {
                        myreport.ReportDefPath = auser.SettingsFolder + "\\Reports\\PrelimNotice.xml";
                        mynotice.ReportDefPath = auser.SettingsFolder + "\\Reports\\PrelimNotice.xml";
                    }
                }
                else
                {
                    myreport.ReportDefPath = auser.SettingsFolder + "\\Reports\\PrelimNotice.xml";
                    mynotice.ReportDefPath = auser.SettingsFolder + "\\Reports\\PrelimNotice.xml";
                }

                myreport.OutputFolder = mydir1;
                myreport.UserName = auser.UserName;

                mynotice.OutputFolder = mydir1;
                mynotice.UserName = auser.UserName;
                mynotice.PrintMode = aprintmode;

                myforms.Sort = "SeqNo";
                string myfilename = "";
                myforms.Sort = "SEQNO ASC";
                int i;
                string myformseq;


                while (myforms.EOF())
                {

                    myforms.FillEntity(ref tblI);

                    myreport.ReportName = myform.ReportName;
                    myreport.DataView = myforms;
                    myreport.PrintMode = aprintmode;
                    myreport.Sort = "";

                    i = 10 - myform.SeqNo;
                    myformseq = i.ToString();
                    myfilename = Convert.ToString(dtmyview.Rows[0]["PrintMajorGroup"]) + Convert.ToString(dtmyview.Rows[0]["PrintMajorGroup"]);
                    myfilename += Convert.ToString(dtmyview.Rows[0]["PrintMajorGroup"]) + Convert.ToString(dtmyview.Rows[0]["PrintGroup"]);
                    string str1;
                    str1 = "00000" + myseq;

                    myfilename = str1.Substring(1, 6) + "-" + myformseq;



                    myfilename += "-" + Convert.ToString(dtmyview.Rows[0]["ClientCode"]);
                    myfilename += "-" + Convert.ToString(dtmyview.Rows[0]["JobId"]);
                    myfilename += "-" + Convert.ToString(dtmyview.Rows[0]["JobNoticeHistoryId"]);
                    myfilename += "-" + (10 - Convert.ToInt64(dtmyforms.Rows[0]["SeqNo"]));

                    myreport.FilePrefix = myfilename;

                    switch (myform.SeqNo)
                    {
                        case 1:
                            myreport.DataView = mycover;
                            myreport.RenderReport(false);
                            myfiles.Add(myreport.ReportFileName);
                            break;

                        case 2:
                            TableView MYVIEW = DBO.Provider.GetTableView(mysproc);
                            mynotice.ExecuteC1Report(MYVIEW, myfilename);
                            myfiles.Add(mynotice.ReportFileName);
                            break;

                        case 4:
                            myreport.DataView = myview3;
                            myreport.RenderReport(false);
                            myfiles.Add(myreport.ReportFileName);
                            break;

                        case 5:
                            // skip statement logic is done below
                            // ---- statement logic
                            try
                            {
                                var myds1 = new uspbo_PrelimNotices_GetTexasStatement();
                                // var myspJV = new uspbo_PrelimNotices_GetTexasStatement();
                                myds1.JobNoticeHistoryId = anoticeid;
                                DataSet mydsJV = CRF.BLL.Providers.DBO.Provider.GetDataSet(myds1);
                                DataTable mydtJV = mydsJV.Tables[0];
                                DataView mydviewds1 = mydtJV.DefaultView;
                                if (mydsJV.Tables[0].Rows.Count == 1)
                                {
                                    CRF.BLL.Users.CurrentUser myuser = new CRF.BLL.Users.CurrentUser();
                                    myuser.UserName = auser.UserName;
                                    myuser.OutputFolder = mydir1;
                                    myuser.SettingsFolder = auser.SettingsFolder;
                                    string myfile = CRF.BLL.COMMON.Reporting.PrintDataReport(myuser, "TexasStatement.DRL", "Statement", mydviewds1, aprintmode, myfilename, "", "");
                                    myfiles.Add(myfile);
                                }
                            }
                            catch (Exception ex)
                            {
                                string err = ex.Message;
                                ErrorLog.SendErrorToText(ex);
                                return false;
                            }

                            break;
                        default:
                            myreport.DataView = myview;
                            myreport.RenderReport(false);
                            myfiles.Add(myreport.ReportFileName);
                            break;
                    }

                    myforms.MoveNext();

                }

                myfilename = Convert.ToString(dtmyview.Rows[0]["CertNum"]);
                string str;
                str = "00000" + myseq;


                if (myfilename.Length > 2)
                {
                    myfilename = str.Substring(6) + "-" + myfilename.Replace(" ", "-");

                }
                else
                {
                    myfilename = str.Substring(6);

                }

                string MYMAJORGROUP = Convert.ToString(dtmyview.Rows[0]["PrintMajorGroup"]);
                string MYGROUP = Convert.ToString(dtmyview.Rows[0]["PrintGroup"]);

                myfilename += "-" + Convert.ToString(dtmyview.Rows[0]["ClientCode"]);
                myfilename += "-" + Convert.ToString(dtmyview.Rows[0]["JobId"]);
                myfilename += "-" + Convert.ToString(dtmyview.Rows[0]["JobNoticeHistoryId"]);
                mydir3 = mydir;
                mydir3 += "GROUP" + MYMAJORGROUP + MYGROUP + "\\";

                string s = mydir3 + myfilename + ".pdf";

                if (System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(mydir3)) == false)
                {
                    System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(mydir3));
                }

                Combine(myfiles, s, objJVTXPrintLettersVM);

                string myjobid = Convert.ToString(dtmyview.Rows[0]["JobId"]);

                CopyToImageFolder(s, myjobid, anoticeid);

                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }

        private static bool PrintPrelimNotice(CRF.BLL.Users.CurrentUser auser, string anoticeid, PrintMode aprintmode, JVTXPrintLettersVM objJVTXPrintLettersVM)
        {
            try
            {
                JobNoticeRequestForms myform = new JobNoticeRequestForms();
                ITable tblI = myform;

                var mysproc = new uspbo_PrelimNotices_GetNoticeInfo();
                mysproc.JobNoticeHistoryId = Convert.ToInt32(anoticeid);
                DataSet myds = CRF.BLL.Providers.DBO.Provider.GetDataSet(mysproc);


                DataTable dtmyview = myds.Tables[0];
                DataTable dtmycover = myds.Tables[0];
                DataTable dtmyview3 = myds.Tables[2];

                DataView myview = dtmyview.DefaultView;
                DataView mycover = dtmycover.DefaultView;
                DataView myview3 = dtmyview3.DefaultView;

                DateTime DatePrinted = Convert.ToDateTime(dtmyview.Rows[0]["DatePrinted"]);
                string mydate = DatePrinted.ToString("yyyyMMdd");

                TableView myforms = new TableView(myds.Tables[1]);


                DataTable dtmyforms = myds.Tables[1];



                ArrayList myfiles = new ArrayList();

                if (Convert.ToBoolean(dtmyview.Rows[0]["IsCustomReportFile"]) == true)
                {
                    string sfilepath = auser.SettingsFolder + "\\Reports\\" + Convert.ToString(dtmyview.Rows[0]["CustomReportFile"]);
                    if (System.IO.File.Exists(sfilepath))
                    {
                        myreport.ReportDefPath = auser.SettingsFolder + "\\Reports\\" + Convert.ToString(dtmyview.Rows[0]["CustomReportFile"]);
                        mynotice.ReportDefPath = auser.SettingsFolder + "\\Reports\\" + Convert.ToString(dtmyview.Rows[0]["CustomReportFile"]);
                    }
                    else
                    {
                        myreport.ReportDefPath = auser.SettingsFolder + "\\Reports\\PrelimNotice.xml";
                        mynotice.ReportDefPath = auser.SettingsFolder + "\\Reports\\PrelimNotice.xml";
                    }
                }
                else
                {
                    myreport.ReportDefPath = auser.SettingsFolder + "\\Reports\\PrelimNotice.xml";
                    mynotice.ReportDefPath = auser.SettingsFolder + "\\Reports\\PrelimNotice.xml";
                }

                myreport.OutputFolder = mydir1;
                myreport.UserName = auser.UserName;

                mynotice.OutputFolder = mydir1;
                mynotice.UserName = auser.UserName;
                mynotice.PrintMode = aprintmode;

                myforms.Sort = "SeqNo";
                string myfilename = "";
                myforms.Sort = "SEQNO ASC";
                int i;
                string myformseq;


                while (myforms.EOF())
                {

                    myforms.FillEntity(ref tblI);

                    myreport.ReportName = myform.ReportName;
                    myreport.DataView = myforms;
                    myreport.PrintMode = aprintmode;
                    myreport.Sort = "";

                    i = 10 - myform.SeqNo;
                    myformseq = i.ToString();
                    myfilename = Convert.ToString(dtmyview.Rows[0]["PrintMajorGroup"]) + Convert.ToString(dtmyview.Rows[0]["PrintMajorGroup"]);
                    myfilename += Convert.ToString(dtmyview.Rows[0]["PrintMajorGroup"]) + Convert.ToString(dtmyview.Rows[0]["PrintGroup"]);
                    string str1;
                    str1 = "00000" + myseq;

                    myfilename = str1.Substring(1, 6) + "-" + myformseq;



                    myfilename += "-" + Convert.ToString(dtmyview.Rows[0]["ClientCode"]);
                    myfilename += "-" + Convert.ToString(dtmyview.Rows[0]["JobId"]);
                    myfilename += "-" + Convert.ToString(dtmyview.Rows[0]["JobNoticeHistoryId"]);
                    myfilename += "-" + (10 - Convert.ToInt64(dtmyforms.Rows[0]["SeqNo"]));

                    myreport.FilePrefix = myfilename;

                    switch (myform.SeqNo)
                    {
                        case 1:
                            myreport.DataView = mycover;
                            myreport.RenderReport(false);
                            myfiles.Add(myreport.ReportFileName);
                            break;

                        case 2:
                            TableView MYVIEW = DBO.Provider.GetTableView(mysproc);
                            mynotice.ExecuteC1Report(MYVIEW, myfilename);
                            myfiles.Add(mynotice.ReportFileName);
                            break;

                        case 4:
                            myreport.DataView = myview3;
                            myreport.RenderReport(false);
                            myfiles.Add(myreport.ReportFileName);
                            break;

                        case 5:
                            // skip statement logic is done below
                            // ---- statement logic
                            try
                            {

                                var myspJV = new uspbo_PrelimNotices_GetTexasStatementJV();
                                myspJV.JobNoticeHistoryId = anoticeid;
                                DataSet mydsJV = CRF.BLL.Providers.DBO.Provider.GetDataSet(myspJV);
                                DataTable mydtJV = mydsJV.Tables[0];
                                DataView myviewJV = mydtJV.DefaultView;
                                if (myviewJV.Count > 0)
                                {
                                    CRF.BLL.Users.CurrentUser myuser = new CRF.BLL.Users.CurrentUser();
                                    myuser.UserName = auser.UserName;
                                    myuser.OutputFolder = mydir1;
                                    myuser.SettingsFolder = auser.SettingsFolder;
                                    string myfile = CRF.BLL.COMMON.Reporting.PrintC1Report(myuser, "PrelimNotice.xml", "TXStateOfAcct", myviewJV, aprintmode, myfilename, "", "");
                                    myfiles.Add(myfile);
                                }
                            }
                            catch (Exception ex)
                            {
                                string err = ex.Message;
                                ErrorLog.SendErrorToText(ex);
                                return false;
                            }

                            break;
                        default:
                            myreport.DataView = myview;
                            myreport.RenderReport(false);
                            myfiles.Add(myreport.ReportFileName);
                            break;
                    }

                    myforms.MoveNext();

                }

                myfilename = Convert.ToString(dtmyview.Rows[0]["CertNum"]);
                string str;
                str = "00000" + myseq;


                if (myfilename.Length > 2)
                {
                    myfilename = str.Substring(6) + "-" + myfilename.Replace(" ", "-");

                }
                else
                {
                    myfilename = str.Substring(6);

                }

                string MYMAJORGROUP = Convert.ToString(dtmyview.Rows[0]["PrintMajorGroup"]);
                string MYGROUP = Convert.ToString(dtmyview.Rows[0]["PrintGroup"]);

                myfilename += "-" + Convert.ToString(dtmyview.Rows[0]["ClientCode"]);
                myfilename += "-" + Convert.ToString(dtmyview.Rows[0]["JobId"]);
                myfilename += "-" + Convert.ToString(dtmyview.Rows[0]["JobNoticeHistoryId"]);
                mydir3 = mydir;
                mydir3 += "GROUP" + MYMAJORGROUP + MYGROUP + "\\";

                string s = mydir3 + myfilename + ".pdf";

                if (System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(mydir3)) == false)
                {
                    System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(mydir3));
                }

                Combine(myfiles, s, objJVTXPrintLettersVM);

                string myjobid = Convert.ToString(dtmyview.Rows[0]["JobId"]);

                CopyToImageFolder(s, myjobid, anoticeid);

                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }

        private static bool CopyToImageFolder(string afile, string ajobid, string anoticeid)
        {
            try
            {
                string myfilename = "";
                myfilename = "";
                myfilename = ajobid;
                myfilename += "-" + anoticeid + ".PDF";

                string mydest = CRF.BLL.CRFView.CRFView.GetKeyValue("JOBNOTICEIMAGEPATH") + myfilename;

                // for testing locally
                //mydest = "d:\images\" + myfilename;

                if (System.IO.Directory.Exists(CRF.BLL.CRFView.CRFView.GetKeyValue("JOBNOTICEIMAGEPATH")) == false)
                {
                    //    MsgBox("Image folder not found, Please Correct " & vbCrLf & CRFView.GetKeyValue("JOBNOTICEIMAGEPATH"), MsgBoxStyle.Critical)
                    return false;
                }

                if (System.IO.File.Exists(mydest) == true)
                {
                    System.IO.File.Delete(mydest);
                }
                System.IO.File.Copy(afile, mydest);

                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }

        private static bool Combine(ArrayList afiles, string adest, JVTXPrintLettersVM objJVTXPrintLettersVM)
        {

            try
            {
                if (System.IO.Directory.Exists("TEMP\\"))
                {
                    System.IO.Directory.Delete("TEMP\\", true);
                }

                System.IO.Directory.CreateDirectory("TEMP\\");

                if (System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(adest)) == false)
                {
                    System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(adest));
                }

                foreach (string FILE in afiles)
                {
                    string OUTFILE = "TEMP\\" + System.IO.Path.GetFileName(FILE);
                    if (System.IO.File.Exists(FILE) == true)
                    {
                        System.IO.File.Copy(FILE, OUTFILE);
                    }
                }

                string S = HttpContext.Current.Server.MapPath("~") + "\\PDFTK  " + "TEMP\\*.PDF CAT OUTPUT " + "TEMP\\ONE.PDF";

                System.IO.File.Copy("TEMP\\one.PDF", adest);

                if (objJVTXPrintLettersVM.chkSendToPrinter == true)
                {
                    System.Threading.Thread.Sleep(1300);
                    PDFLIB.PrintPDF(MYPRINTERNAME, adest);
                }
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }

        private static bool PRINTMAILLOG(string AMAJORGROUP, string APRINTGROUP, bool APRINTLOG, JVTXPrintLettersVM objJVTXPrintLettersVM)
        {
            try
            {
                if (mMajorGroup.Length < 1)
                {
                    mMajorGroup = AMAJORGROUP;
                    mPrintLog = APRINTLOG;
                }

                if (mPrintGroup.Length < 1)
                {
                    mPrintGroup = APRINTGROUP;
                    mPrintLog = APRINTLOG;
                }

                if (mMajorGroup == AMAJORGROUP && mPrintGroup == APRINTGROUP)
                {
                    return true;
                }

                if (mPrintLog == false)
                {
                    mMajorGroup = AMAJORGROUP;
                    mPrintGroup = APRINTGROUP;
                    mPrintLog = APRINTLOG;
                    return true;
                }

                var mysproc = new uspbo_PrelimNotices_GetNoticesByDateRequested();
                mysproc.RunType = 2;
                mysproc.PrintMajorGroup = mMajorGroup;
                mysproc.PrintGroup = mPrintGroup;

                PrintMailLog1(mysproc, "", mPrintMode, objJVTXPrintLettersVM);

                mMajorGroup = AMAJORGROUP;
                mPrintGroup = APRINTGROUP;
                mPrintLog = APRINTLOG;
                return true;

            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }

        }

        private static string PrintMailLog1(uspbo_PrelimNotices_GetNoticesByDateRequested asproc, string areportprefix, PrintMode aprintmode, JVTXPrintLettersVM objJVTXPrintLettersVM)
        {
            try
            {
                string mydir3 = mydir + "GROUP" + asproc.PrintMajorGroup + asproc.PrintGroup + "\\";
                if (System.IO.Directory.Exists(mydir3) == false)
                {
                    System.IO.Directory.CreateDirectory(mydir3);
                }

                DataSet MYDS = CRF.BLL.Providers.DBO.Provider.GetDataSet(asproc);
                TableView myview = CRF.BLL.Providers.DBO.Provider.GetTableView(asproc);
                DataTable dt = MYDS.Tables[3];
                string myreportdef = "PrelimNotice.xml";
                string myreportname = "MailingLog";
                string myreportprefix = "ZZZZZZ";
                myreport.OutputFolder = mydir3;
                myreport.ReportName = myreportname;
                myreport.DataView = myview;
                myreport.PrintMode = aprintmode;
                myreport.Sort = "";
                myreport.FilePrefix = myreportprefix;
                myreport.RenderReport(false);

                if (objJVTXPrintLettersVM.chkSendToPrinter == true)
                {
                    PDFLIB.PrintPDF(MYPRINTERNAME, myreport.ReportFileName);
                }

                return myreport.ReportFileName;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return "";
            }
        }


        private static bool CombineReverse(ArrayList afiles, string ajobid, string anoticeid, string adest, JVTXPrintLettersVM objJVTXPrintLettersVM)
        {

            try
            {

                string myfilename = "";
                myfilename = ajobid;
                myfilename += "-" + anoticeid + ".PDF";
                string mydest = CRF.BLL.CRFView.CRFView.GetKeyValue("JOBNOTICEIMAGEPATH") + myfilename;


                if (System.IO.Directory.Exists("TEMP\\"))
                {
                    System.IO.Directory.Delete("TEMP\\", true);
                }

                System.IO.Directory.CreateDirectory("TEMP\\");

                if (System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(adest)) == false)
                {
                    System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(adest));
                }

                foreach (string FILE in afiles)
                {
                    string OUTFILE = "TEMP\\" + System.IO.Path.GetFileName(FILE);
                    if (System.IO.File.Exists(FILE) == true)
                    {
                        System.IO.File.Copy(FILE, OUTFILE);
                    }
                }

                string S = HttpContext.Current.Server.MapPath("~") + "\\PDFTK  " + "TEMP\\*.PDF CAT OUTPUT " + "TEMP\\ONE.PDF";

                System.IO.File.Copy("TEMP\\one.PDF", adest);

                if (objJVTXPrintLettersVM.chkSendToPrinter == true)
                {
                    System.Threading.Thread.Sleep(1300);
                    PDFLIB.PrintPDF(MYPRINTERNAME, adest);
                }
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }

        public static DataTable GetNoticesCycleByDateRequested()
        {
            try
            {
                var mysproc = new uspbo_PrelimNotices_GetNoticesByDateRequested();
                mysproc.RunType = 3;
                DBO.Provider.ExecNonQuery(mysproc);

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(mysproc);
                DataTable dt = EntityList.ToTable();
                return (dt);

            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new DataTable());
            }
        }

        public static int SaveDebtAccount_DeskAssignment_Desk(DeskAssignmentVM objDeskAssignmentVM)
        {
            try
            {
                ADJob objADJob = new ADJob();
                AutoMapper.Mapper.CreateMap<ADJob, Job>();
                ITable tblJob;
                Job objJob;

                if (!string.IsNullOrEmpty(objDeskAssignmentVM.JobId))
                {
                    ADJob objADJobOriginal = ReadJob(Convert.ToInt32(objDeskAssignmentVM.JobId));


                    if (objDeskAssignmentVM.objADJob.DeskNumId.ToString() == "" || objDeskAssignmentVM.objADJob.DeskNumId == 0)
                    {
                    }
                    else
                    {
                        objADJobOriginal.DeskNumId = objDeskAssignmentVM.objADJob.DeskNumId;


                        objJob = AutoMapper.Mapper.Map<Job>(objADJobOriginal);
                        tblJob = (ITable)objJob;
                        DBO.Provider.Update(ref tblJob);
                    }
                }
                else
                {
                    objJob = AutoMapper.Mapper.Map<Job>(objADJob);
                    tblJob = (ITable)objJob;
                    DBO.Provider.Create(ref tblJob);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }


        public static int SaveDebtAccount_DeskAssignment_Action(DeskAssignmentVM objDeskAssignmentVM)
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                var Sp_OBJ = new uspbo_Jobs_CreateActionHistory();
                Sp_OBJ.JobId = Convert.ToInt32(objDeskAssignmentVM.JobId);
                Sp_OBJ.ActionId = objDeskAssignmentVM.objADJob.StatusCodeId;
                Sp_OBJ.UserId = user.Id;
                Sp_OBJ.UserName = user.UserName;
                DBO.Provider.ExecNonQuery(Sp_OBJ);
                return 1;
            }

            catch (Exception ex)
            {
                return 0;
            }

        }
        #endregion

        public static int SaveOtherNotices (EditOtherNoticesVM objEditOtherNoticesVM)
        {
            try
            {

            ADJob objADJobOriginal = ReadJob(objEditOtherNoticesVM.objJob.Id);
            ADJobLegalParties objJobLegalParties = ADJobLegalParties.ReadJobLegalParties(objADJobOriginal.LenderId);
            objEditOtherNoticesVM.objADClient = ADClient.GetClientDetails(objEditOtherNoticesVM.objJob.ClientId);
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            objEditOtherNoticesVM.objJob.LastUpdatedById = user.Id;
            objEditOtherNoticesVM.objJob.LastUpdatedBy = user.UserName;
            objEditOtherNoticesVM.objJob.LastUpdatedOn = DateTime.Now;

            AutoMapper.Mapper.CreateMap<ADJob, Job>();
            ITable tblJob;
            Job ObjJob;
            objADJobOriginal.LastUpdatedById = user.Id;
            objADJobOriginal.LastUpdatedBy = user.UserName;
            objADJobOriginal.LastUpdatedOn = DateTime.Now;
                objADJobOriginal.LienAssignDate = Convert.ToDateTime(objEditOtherNoticesVM.objJob.LienAssignDate.ToShortDateString());
                objADJobOriginal.LienAmt = objEditOtherNoticesVM.objJob.LienAmt;
                objADJobOriginal.LienExtDate = objEditOtherNoticesVM.objJob.LienExtDate;
                objADJobOriginal.RecorderNum = objEditOtherNoticesVM.objJob.RecorderNum;
                objADJobOriginal.EndDate = objEditOtherNoticesVM.objJob.EndDate;
                objADJobOriginal.FileDate = objEditOtherNoticesVM.objJob.FileDate;
                objADJobOriginal.BondAssignDate = objEditOtherNoticesVM.objJob.BondAssignDate;
                objADJobOriginal.BondNum = objEditOtherNoticesVM.objJob.BondNum;
                objADJobOriginal.BondDDD = objEditOtherNoticesVM.objJob.BondDDD;
                objADJobOriginal.LienReleaseAssign = objEditOtherNoticesVM.objJob.LienReleaseAssign;
                objADJobOriginal.PageNum = objEditOtherNoticesVM.objJob.PageNum;
                objADJobOriginal.ReleaseFileDate = objEditOtherNoticesVM.objJob.ReleaseFileDate;
                objADJobOriginal.BookNum = objEditOtherNoticesVM.objJob.BookNum;
                objADJobOriginal.ReleaseDocNum = objEditOtherNoticesVM.objJob.ReleaseDocNum;
                objADJobOriginal.SNAssignDate = objEditOtherNoticesVM.objJob.SNAssignDate;
                objADJobOriginal.SNDDD = objEditOtherNoticesVM.objJob.SNDDD;
                objADJobOriginal.NOIAssignDate = objEditOtherNoticesVM.objJob.NOIAssignDate;
            ObjJob = AutoMapper.Mapper.Map<Job>(objADJobOriginal);
            tblJob = (ITable)ObjJob;
            DBO.Provider.Update(ref tblJob);

            // Save BondNum to lender JobLegalParties
            if (ObjJob.LenderId > 0 && DBNull.Value.Equals(objEditOtherNoticesVM.objJob.BondNum) == false)
            {
                AutoMapper.Mapper.CreateMap<ADJobLegalParties, JobLegalParties>();
                ITable tblLegalParty;
                JobLegalParties objJobLegalParty;
                objJobLegalParties.RefNum = objEditOtherNoticesVM.objJob.BondNum;
                objJobLegalParty = AutoMapper.Mapper.Map<JobLegalParties>(objJobLegalParties);

                tblLegalParty = objJobLegalParty;

                DBO.Provider.Update(ref tblLegalParty);
            }
             if(ObjJob.IsJobView == true && objEditOtherNoticesVM.objADClient.NoRentalInfo == false)
                {
                    var mysproc = new uspbo_Jobview_UpdateDeadlineDates();
                    mysproc.JobId = objEditOtherNoticesVM.objJob.Id.ToString();
                    DBO.Provider.ExecNonQuery(mysproc);
                }
                else
                {
                    var mysproc = new uspbo_LiensDayEnd_UpdateDeadlineDates();
                    mysproc.ExpireDays = 0;
                    mysproc.JobId = objEditOtherNoticesVM.objJob.Id.ToString();
                    DBO.Provider.ExecNonQuery(mysproc);
                }

          
            
            return 1;

            }
            catch(Exception ex)
            {
                return 0;
            }
        }
        
       
        public static string SendFax (string typecode,int JobId)
        {
            try { 
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
           
            //PrinterSettings settings = new PrinterSettings();
            //string MYNAME = settings.PrinterName;
            string pdfPath = null;
            var mysproc = new uspbo_Jobs_CreateFaxHistory();
            mysproc.UserId = user.Id;
            mysproc.UserName = user.UserName;
            mysproc.JobId = JobId;
            JobVM ObjJobVM = JobInfo.LoadJobInfo(JobId.ToString());
          
                 DataView myview = ObjJobVM.jobdt.DefaultView;

            //CRF.BLL.CRFView.CRFView.SetDefaultPrinter(CRF.BLL.CRFView.CRFView.GetKeyValue("FAXPRINTER"));
            string myreport;
            switch (typecode)
            {
                case "PCU":
                    if(ObjJobVM.ObjvwJobList.JobState == "TX")
                    {
                        myreport = "FaxCustAutoTX";
                    }
                    else
                    {
                        myreport = "FaxCustAuto";
                    }
                        // CRF.BLL.COMMON.Reporting.PrintC1Report(user, "LienReports.xml", myreport, myview, CRF.BLL.COMMON.PrintMode.PrintDirect, "", "", "");
                        pdfPath = Reporting.PrintC1Report(user, "LienReports.xml", myreport, myview, PrintMode.PrintToPDF, "", "", "");
                        mysproc.GCfAX = 0;
                       mysproc.CustFax = 1;
                    DBO.Provider.ExecNonQuery(mysproc);

                    break;
                case "PGC":
                    if (ObjJobVM.ObjvwJobList.JobState == "TX")
                    {
                        myreport = "FaxGenAutoTX";
                    }
                    else
                    {
                        myreport = "FaxGenAuto";
                    }
                    //CRF.BLL.COMMON.Reporting.PrintC1Report(user, "LienReports.xml", myreport, myview, CRF.BLL.COMMON.PrintMode.PrintDirect, "", "", "");
                    pdfPath = Reporting.PrintC1Report(user, "LienReports.xml", myreport, myview, PrintMode.PrintToPDF, "", "", "");
                    mysproc.CustFax = 0;
                    mysproc.GCfAX = 1;
                    DBO.Provider.ExecNonQuery(mysproc);

                    break;

            }
                //CRF.BLL.CRFView.CRFView.SetDefaultPrinter(MYNAME);
                return pdfPath;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return "";
            }

           
        }
    }
}
