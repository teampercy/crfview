﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard
{
    public class Filter
    {
        #region Properties
        
        public string JobId { get; set; }
        public string ClientCode { get; set; }
        public string JobNum { get; set; }

        public DateTime ReviewDate { get; set; }

        public DateTime DateAssigned { get; set; }
        public DateTime DateNoticeSent { get; set; }
        public DateTime CanDate { get; set; }
        public string StatusCode { get; set; }
        public string DeskNum { get; set; }
        public string TODeskNum { get; set; }
        public string PONUm { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime LienAssigned { get; set; }
        public DateTime LienSent { get; set; }
        public DateTime NOIDate { get; set; }
        public DateTime VerifiedDate { get; set; }
        public string BranchNumber { get; set; }
        public string JobCity { get; set; }
        public string JobCounty { get; set; }
        public string JobState { get; set; }
        public string JobName { get; set; }
        public string JobAdd1 { get; set; }

        public string PropertyType { get; set; }
        public string StatusGroup { get; set; }

        public string Customer { get; set; }
        public string CustomerRef { get; set; }
        public string GeneralContractor { get; set; }
        public string PropertyOwner { get; set; }
        public string LenderName { get; set; }
        public string BondNum { get; set; }
        public string EstBalance { get; set; }
        public string MyProperty { get; set; }

        public bool chkWildcard { get; set; }
        public bool chkPropertyType { get; set; }
        public bool chkWildCardName { get; set; }
        public bool chkIncludeClosed { get; set; }
        public bool chkPendingNotices { get; set; }
        public bool chkReviewNotes { get; set; }
        public bool chkUnassigned { get; set; }
        public bool chkNotManagerReviewed { get; set; }
        public bool chkTXNotReviewed { get; set; }
        public bool chkWildcardAddr { get; set; }
        public bool chkWildcardCust { get; set; }
        public bool chkWildcardGC { get; set; }
        public bool chkWildcardOwner { get; set; }
        public bool chkWildcardLender { get; set; }
        public bool chkReviewDate { get; set; }
        public bool chkStatusGroup { get; set; }

        public bool IsWebNoteList = false;
        public bool IsJVWebNoteList = false;
        public bool IsTXReviewList = false;

        #endregion
    }
}
