﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.Providers;
using CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.Verifiers.WorkCard
{
   public class ADJobNoticePrelimRequest
    {
        #region Properties
        public int Id { get; set; }
        public int JobId { get; set; }
        public int SubmittedByUserId { get; set; }
        public string SubmittedBy { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DateProcessed { get; set; }
        public bool IsProcessed { get; set; }
        public bool PrelimBox { get; set; }
        public bool AmendedBox { get; set; }
        public bool ResendBox { get; set; }
        public bool RushBox { get; set; }
        public int RequestId { get; set; }
        public int BillableEventId { get; set; }
        public int JobActionHistoryId { get; set; }
        public string TypeCode { get; set; }
        public string LegalPartyName { get; set; }

        #endregion
        #region CRUD
        public  static List<ADJobNoticePrelimRequest> GetJobNoticePrelimRequestList(DataTable dt)
        {
            try
            {
                List<ADJobNoticePrelimRequest> list = (from DataRow dr in dt.Rows
                                                       select new ADJobNoticePrelimRequest()
                                                       {
                                                           SubmittedBy = ((dr["SubmittedBy"] != DBNull.Value ? dr["SubmittedBy"].ToString():"")),
                                                           TypeCode = ((dr["TypeCode"] != DBNull.Value ? dr["TypeCode"].ToString() : "")),
                                                           LegalPartyName = ((dr["LegalPartyName"] != DBNull.Value ? (dr["LegalPartyName"]).ToString():"")),
                                                           ResendBox = ((dr["ResendBox"] != DBNull.Value ? Convert.ToBoolean(dr["ResendBox"]) : false)),
                                                           AmendedBox = ((dr["AmendedBox"] != DBNull.Value ? Convert.ToBoolean(dr["AmendedBox"]) : false)),
                                                           RushBox = ((dr["RushBox"] != DBNull.Value ? Convert.ToBoolean(dr["RushBox"]) : false)),
                                                            JobId = ((dr["JobId"] != DBNull.Value ? Convert.ToInt32(dr["JobId"]) : 0)),
                                                       }).ToList();
                return list;
            }
            catch (Exception ex)
            {
                return (new List<ADJobNoticePrelimRequest>());
            }
        }
        public static int savePrelimNotice(IList<NoticeEditVM> noticeList, int JobId, bool RushBox, string RbtNoticeType,bool panelVal)
        {
            try
            {
                NoticeEditVM objNoticeEditVM = new NoticeEditVM();
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                var sproc = new uspbo_Jobs_CreateNoticeRequest();
                sproc.JobId = JobId;
                sproc.UserId = user.Id;
                sproc.UserName = user.UserName;
                sproc.NoticeType = 1;
               
                sproc.SecondaryIds = (-1).ToString();
             
                
                 if(panelVal == true)
                {
                    if(RbtNoticeType == "Amended Notice")
                    {
                        sproc.AmendedBox = true;
                    }
                    else
                    {
                        sproc.AmendedBox = false;
                    }
                    
                    
                     sproc.PrelimBox = false;
                   
                    if (RbtNoticeType == "Resend")
                    {
                        sproc.ResendBox  = true;
                    }
                    else
                    {
                        sproc.ResendBox = false;
                    }
                    
                }
                else
                {
                    sproc.AmendedBox = false;
                    sproc.PrelimBox = true;
                    sproc.ResendBox = false;

                }
                sproc.RushBox = RushBox;
                sproc.MainCust = false;
                sproc.MainGC = false;
                sproc.MainLender = false;
                sproc.MainOwner = false;
                sproc.MainDesignee = false;

                for (var i = 0; i < noticeList.Count; i++)
                {



                    if (noticeList[i].ischeck == true)
                    {
                        
                        
                        var typeCode = noticeList[i].TypeCode.Replace("\n", "").Trim();
                        var IsPrimary = noticeList[i].IsPrimary.Replace("\n", "").Trim();
                        //&& noticeList[i].IsPrimary == "YES"
                        if (typeCode == "CU" && IsPrimary == "YES")
                        {
                            sproc.MainCust = true;
                        }

                        if (typeCode == "GC" && IsPrimary == "YES")
                        {
                            sproc.MainGC = true;
                        }

                        if (typeCode == "LE" && IsPrimary == "YES")
                        {
                            sproc.MainLender = true;
                        }

                        if ((typeCode == "OW" || IsPrimary == "ML AGENT") && (noticeList[i].IsPrimary == "YES"))
                        {
                            sproc.MainOwner = true;
                        }

                        if (typeCode == "DE" && IsPrimary == "YES")
                        {
                            sproc.MainDesignee = true;
                        }

                        if (IsPrimary == "NO")
                        {
                            if (sproc.SecondaryIds.Length > 0)
                            {
                                sproc.SecondaryIds += ",";
                            }
                            sproc.SecondaryIds += noticeList[i].Id.ToString() + ",";
                             
                        }

                    }

                }

                if ((sproc.MainCust == false) && (sproc.MainGC == false) && (sproc.MainLender == false) && (sproc.MainOwner == false) && (sproc.MainDesignee == false) && (sproc.SecondaryIds == (-1).ToString()))
                {
                    return 0;
                }

                DBO.Provider.ExecNonQuery(sproc);
                

                return 1;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        
              public static bool DeletePrelimNotice(string JobId)
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                var sproc = new uspbo_Jobs_DeleteNoticePrelimRequest();
                sproc.JobId = Convert.ToInt32(JobId);
                sproc.UserId = user.Id;
                sproc.UserName = user.UserName;
                DBO.Provider.ExecNonQuery(sproc);
                return true;
            }
              catch (Exception ex)
            {
                return false;
            }
        }
        #endregion
    }
}
