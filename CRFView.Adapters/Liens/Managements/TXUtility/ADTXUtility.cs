﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.Providers;
using CRFView.Adapters.Liens.Managements.TXUtility.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADTXUtility
    {

        #region Properties

        #endregion

        public static int GetTXUtility(TXUtilityVM objTXUtilityVM)
        {
            try
            {
                var result = "";
                var mysproc = new uspbo_JobView_TXCleanup();

                if (objTXUtilityVM.RbtBtn == "optUtility1")
                {
                    mysproc.UtilityType = 1;
                }
                else
                {
                    mysproc.UtilityType = 2;
                }

                DBO.Provider.ExecNonQuery(mysproc);
                result = "Processed Succesfully";

                return 1;

            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

    }
}
