﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.Managements.TXUtility.ViewModels
{
    public class TXUtilityVM
    {
        public string RbtBtn { get; set; }
        public string optUtility1 { get; set; }
        public string optUtility2 { get; set; }
        public string Message { get; set; }
    }
}
