﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Web.Mvc;

namespace CRFView.Adapters.Liens.Managements.NCInfoCheck.ViewModels
{
    public class NCInfoCheckVM
    {
        public NCInfoCheckVM()
        {
            ObjNCInfoCheck = new ADvmJobInfo();

            NCInfoCheckList = new List<ADvmJobInfo>();
            AllClientIdList = CommonBindList.GetClientIdList();

        }

        public String ByJobState { get; set; }
        public System.DateTime DateTimePicker1 { get; set; }
        public System.DateTime DateTimePicker2 { get; set; }
        public int JobId { get; set; }
        public ADvmJobInfo ObjNCInfoCheck { get; set; }
        public DataTable dtNCInfoCheckList { get; set; }

        public List<ADvmJobInfo> NCInfoCheckList { get; set; }
        public bool chkAllClient { get; set; }
        public string ddlAllClientId { get; set; }
        public IEnumerable<SelectListItem> AllClientIdList { get; set; }
        public bool chkNCInfo { get; set; }
        public string check { get; set; }
    }
}
