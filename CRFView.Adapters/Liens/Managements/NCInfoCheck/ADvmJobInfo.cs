﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRFView.Adapters.Liens.Managements.NCInfoCheck.ViewModels;
using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using Ionic.Zip;
using System.IO;
using CRF.BLL.CRFDB.VIEWS;
using CRFView.Adapters.Accounting.AccountingLiens.ClientViewAccessInvoice.ViewModels;

namespace CRFView.Adapters
{
    public class ADvmJobInfo
    {
        #region Properties
        public int Id { get; set; }
        public int ClientId { get; set; }
        public int JobId { get; set; }
        public bool NCInfo { get; set; }
        public string BranchNumber { get; set; }
        public string ClientCode { get; set; }
        public string ClientName { get; set; }
        public string JobNum { get; set; }
        public string JobName { get; set; }
        public string JobAdd1 { get; set; }
        public string JobAdd2 { get; set; }
        public string JobCity { get; set; }
        public string JobState { get; set; }
        public string JobZip { get; set; }
        public string JobCounty { get; set; }
        public string LaborType { get; set; }
        public DateTime StartDate { get; set; }
        public string PrelimDate { get; set; }
        public string EquipRental { get; set; }
        public bool PublicJob { get; set; }
        public bool JointCk { get; set; }
        public bool SuretyBox { get; set; }
        public decimal EstBalance { get; set; }
        public DateTime DateAssigned { get; set; }
        public String StatusCode { get; set; }
        public String DeskNum { get; set; }
        public DateTime NoticeSent { get; set; }
        public DateTime RevDate { get; set; }
        //public DateTime CanDate { get; set; }
        //public DateTime EndDate { get; set; }
        //public DateTime FileDate { get; set; }
        //public DateTime LienDate { get; set; }
        //public DateTime ResendDate { get; set; }
        //public DateTime LienAssignDate { get; set; }
        //public DateTime AmendedDate { get; set; }
        public DateTime VerifiedDate { get; set; }
        public String CustName { get; set; }
        public String GCName { get; set; }
        public String OwnerName { get; set; }
        
        public String LenderName { get; set; }
        //public string BookNum { get; set; }
        //public string PageNum { get; set; }
        //public string PONum { get; set; }
        #endregion Properties


        public static ADvmJobInfo ReadJob(int JobID)
        {
            Job myentity = new Job();
            ITable myentityItable = myentity;
            DBO.Provider.Read(JobID.ToString(), ref myentityItable);
            myentity = (Job)myentityItable;
            AutoMapper.Mapper.CreateMap<Job, ADvmJobInfo>();
            return (AutoMapper.Mapper.Map<ADvmJobInfo>(myentity));
        }

        public static DataTable GetNCInfoCheckDt(NCInfoCheckVM objNCInfoCheckVM)
        {
            try
            {
                var sp_obj = new uspbo_LiensDayEnd_GetNCInfo();


                sp_obj.FromDate = objNCInfoCheckVM.DateTimePicker1.ToShortDateString();
                sp_obj.ThruDate = objNCInfoCheckVM.DateTimePicker2.ToShortDateString();

                if (objNCInfoCheckVM.chkAllClient == false)
                {
                    sp_obj.ClientId = Convert.ToInt32(objNCInfoCheckVM.ddlAllClientId);
                }
                else
                {
                    sp_obj.ClientId = 0;
                }
                //if ((objNCInfoCheckVM.ObjNCInfoCheck.ClientId) > 0)
                //{
                //    sp_obj.ClientId = Convert.ToInt32(objNCInfoCheckVM.ObjNCInfoCheck.ClientId);
                //}
                //else
                //{
                //    sp_obj.ClientId = 0;
                //}

                if (!string.IsNullOrEmpty(objNCInfoCheckVM.ByJobState))
                {
                    sp_obj.JobState = Convert.ToString(objNCInfoCheckVM.ByJobState);
                }
                else
                {
                    sp_obj.JobState = null;
                }


                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sp_obj);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        public static List<ADvmJobInfo> GetNCInfoCheckList(DataTable dt)
        {
            List<ADvmJobInfo> List = (from DataRow dr in dt.Rows
                                                select new ADvmJobInfo()

                                                {
                                                    NCInfo = ((dr["NCInfo"] != DBNull.Value ? Convert.ToBoolean(dr["NCInfo"]) : false)),
                                                    JobId = ((dr["JobId"] != DBNull.Value ? Convert.ToInt32(dr["JobId"]) : Int32.MinValue )),
                                                    JobNum = ((dr["JobNum"] != DBNull.Value ? dr["JobNum"].ToString() : "")),
                                                    JobName = ((dr["JobName"] != DBNull.Value ? dr["JobName"].ToString() : "")),
                                                    JobAdd1 = ((dr["JobAdd1"] != DBNull.Value ? dr["JobAdd1"].ToString() : "")),
                                                    JobAdd2 = ((dr["JobAdd2"] != DBNull.Value ? dr["JobAdd2"].ToString() : "")),
                                                    JobCity = ((dr["JobCity"] != DBNull.Value ? dr["JobCity"].ToString() : "")),
                                                    JobState = ((dr["JobState"] != DBNull.Value ? dr["JobState"].ToString() : "")),
                                                    JobZip = ((dr["JobZip"] != DBNull.Value ? dr["JobZip"].ToString() : "")),
                                                    EstBalance = ((dr["EstBalance"] != DBNull.Value ? Convert.ToDecimal(dr["EstBalance"]) : 0)),
                                                    ClientName = ((dr["ClientName"] != DBNull.Value ? dr["ClientName"].ToString() : "")),
                                                    ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                                    DeskNum = ((dr["DeskNum"] != DBNull.Value ? dr["DeskNum"].ToString() : "")),
                                                    StatusCode = ((dr["StatusCode"] != DBNull.Value ? dr["StatusCode"].ToString() : "")),
                                                    DateAssigned = ((dr["DateAssigned"] != DBNull.Value ? Convert.ToDateTime(dr["DateAssigned"]) : DateTime.MinValue)),
                                                    NoticeSent = ((dr["NoticeSent"] != DBNull.Value ? Convert.ToDateTime(dr["NoticeSent"]) : DateTime.MinValue)),
                                                    RevDate = ((dr["RevDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevDate"]) : DateTime.MinValue)),
                                                    
                                                }).ToList();

            return List;
        }

        public static DataTable LoadInfo(string JobId)
        {
            try
            {
                string sql = "Select * from vwJobInfo where JobId = " + JobId;
                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        public static int UpdateJob(NCInfoCheckVM ObjNCInfoCheck)
        {
            try
            {
                ADvmJobInfo objADJobOriginal = ReadJob(ObjNCInfoCheck.ObjNCInfoCheck.Id);

                objADJobOriginal.NCInfo = ObjNCInfoCheck.chkNCInfo;
                //objADJobOriginal.GreenCard = objChangeJobInfo.chkGreencard;
                //objADJobOriginal.IsJobView = objChangeJobInfo.chkIsJobview;

                //if (objChangeJobInfo.chkStartDate == true)
                //{
                //    objADJobOriginal.StartDate = Convert.ToDateTime(objChangeJobInfo.DateTimePicker1.ToShortDateString());
                //}

                //if (objChangeJobInfo.chkBranchNum == true)
                //{
                //    objADJobOriginal.BranchNum = objChangeJobInfo.txtBranchNum;
                //}

                //if (objChangeJobInfo.chkDeleteVerifiedDate == true)
                //{
                //    objADJobOriginal.VerifiedDate = Convert.ToDateTime(null);
                //}

                AutoMapper.Mapper.CreateMap<ADvmJobInfo, Job>();
                Job myentity;
                myentity = AutoMapper.Mapper.Map<Job>(objADJobOriginal);
                //myentity = AutoMapper.Mapper.Map<Job>(objUpdatedADJob);

                ITable tblI = myentity;
                DBO.Provider.Update(ref tblI);

                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static string ClientViewAccessInvoice(ClientViewAccessInvoiceVM  objClientViewAccessInvoiceVM)
        {
            string pdfPath = null;
            string sFromDate = null;
            string sThruDate = null;

            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var Sp_OBJ = new uspbo_Accounting_GetClientViewLienInvoice();

                //string sql = "Select * from vwCollectionReportsList Where ReportId = " + Convert.ToInt32(objCollectionReportsVM.ddlReportId);
                TableView mytableview = DBO.Provider.GetTableView(Sp_OBJ);

           

                Sp_OBJ.ClientId = objClientViewAccessInvoiceVM.ClientId;
                Sp_OBJ.FROMDATE = objClientViewAccessInvoiceVM.InvoiceFromDate.ToShortDateString();
                Sp_OBJ.THRUDATE = objClientViewAccessInvoiceVM.InvoiceThruDate.ToShortDateString();
                sFromDate = objClientViewAccessInvoiceVM.InvoiceFromDate.ToShortDateString();
                sThruDate = objClientViewAccessInvoiceVM.InvoiceThruDate.ToShortDateString();



                //TableView myview = DBO.Provider.GetTableView(Sp_OBJ);

                TableView myview = CRF.BLL.Providers.DBO.Provider.GetTableView(Sp_OBJ);
                DataSet ds = DBO.Provider.GetDataSet(Sp_OBJ);
               //string abc = ds.Tables[0].Rows[0].Field<DateTime>("DateCreated").ToString();
                // ds += ds.Tables[0].Rows[0]["DateCreated"].ToString("DD/MM/YYYY");
                DataTable mytbl = ds.Tables[0];

                if(mytbl.Rows.Count > 0)
                {
                    string  myreport = " ";

                    string myprefix = Convert.ToString(mytbl.Rows[0]["ClientCode"]);

                   CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;
                    if (objClientViewAccessInvoiceVM.RbtPrintOption == "Send To PDF")
                    {
                        mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                    }
                    // pdfPath = Globals.RenderC1Report(user, mytbl, myreportfilename, myreportname, mymode, "", sFromDate, sThruDate);
                    pdfPath = Globals.RenderC1Report(user, mytbl, "ACC-Liens.xml", "CV Access Invoice", mymode, myprefix);
                }

            
                  

            
            }

            catch (Exception ex)
            {
                return "";
            }
            return pdfPath;
        }

    }
}
