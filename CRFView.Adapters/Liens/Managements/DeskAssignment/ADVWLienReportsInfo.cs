﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.Providers;
using CRFView.Adapters.Collections.Management.DeskAssignment.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.Managements.DeskAssignment
{
    public  class ADVWLienReportsInfo
    {
        #region Properties
        public bool Select { get; set; }
        public string JobId { get; set; }
        public string ClientCode { get; set; }
        public string JobName { get; set; }
        public string JobAdd1 { get; set; }
        public string JobCity { get; set; }
        public string JobState { get; set; }
        public string DeskNum { get; set; }
        public string StatusCode { get; set; }
        public System.DateTime DateAssigned { get; set; }
        public System.DateTime RevDate { get; set; }
        public decimal EstBalance { get; set; }
        public decimal JobBalance { get; set; }
        public System.DateTime StartDate { get; set; }
        public string BranchNumber { get; set; }
        public string ClientCustomer { get; set; }
        public string GeneralContractor { get; set; }
        public string OwnerName { get; set; }
        public bool Document { get; set; }
        public System.DateTime NoticeDeadlineDate { get; set; }
        public bool LateNotice { get; set; }
       
        #endregion
        public static DataTable GetAccountDetailDt(DeskAssignmentVM objDeskAssignmentVM)
        {
            try
            {
                var sp_obj = new uspbo_LiensDayEnd_MassActionAndDeskChange();
  

                   sp_obj.ActionType = 1;
                if (objDeskAssignmentVM.chkAllClients == false)
                {
                    sp_obj.ClientId = Convert.ToInt32(objDeskAssignmentVM.ddlAllClientId);
                }
                else
                {
                    sp_obj.ClientId = 0;
                }
                if (!string.IsNullOrEmpty(objDeskAssignmentVM.JobState))
                {
                    sp_obj.JobState = Convert.ToString(objDeskAssignmentVM.JobState);
                }
                else
                {
                    sp_obj.JobState = null;
                }


                sp_obj.FromDate = objDeskAssignmentVM.AssignedFrom.ToShortDateString();
                sp_obj.ThruDate = objDeskAssignmentVM.AssignedThru.ToShortDateString();

                if (objDeskAssignmentVM.chkAllDesks == false)
                {
                    sp_obj.DeskNumId = Convert.ToInt32(objDeskAssignmentVM.ddlAllDeskId);
                }
                else
                {
                    sp_obj.DeskNumId = 0;
                }

                if (objDeskAssignmentVM.chkAllStatus == false)
                {
                    sp_obj.StatusCodeId = Convert.ToInt32(objDeskAssignmentVM.ddlAllStatusId);
                }
                else
                {
                    sp_obj.StatusCodeId = 0;
                }

                if (objDeskAssignmentVM.chkAllStatusGroup == false)
                {
                    sp_obj.StatusCodeId = Convert.ToInt32(objDeskAssignmentVM.ddlAllStatusGroupId);
                }
                else
                {
                    sp_obj.StatusCodeId = 0;
                }
                //sp_obj.ClientGroup = objDeskAssignmentVM.ClientGroup.ToString();


                if (!string.IsNullOrEmpty(objDeskAssignmentVM.ClientGroup))
                {
                    sp_obj.ClientGroup = Convert.ToString(objDeskAssignmentVM.ClientGroup);
                }
                else
                {
                    sp_obj.ClientGroup = null;
                }
                //if (!string.IsNullOrEmpty(objDeskAssignmentVM.ClientCustomer))
                //{
                //    sp_obj.ClientCustomer = Convert.ToString(objDeskAssignmentVM.ClientCustomer);
                //}
                //else
                //{
                //    sp_obj.ClientCustomer = null;
                //}
                //if (!string.IsNullOrEmpty(objDeskAssignmentVM.GeneralContractor))
                //{
                //    sp_obj.GeneralContractor = Convert.ToString(objDeskAssignmentVM.GeneralContractor);
                //}
                //else
                //{
                //    sp_obj.GeneralContractor = null;
                //}
                if (!string.IsNullOrEmpty(objDeskAssignmentVM.Jobname))
                {
                    sp_obj.Jobname = Convert.ToString(objDeskAssignmentVM.Jobname);
                }
                else
                {
                    sp_obj.Jobname = null;
                }
                ////if (objDeskAssignmentVM.BalanceRange != null )
                ////{
                ////    sp_obj.BalanceRange = Convert.ToInt32(objDeskAssignmentVM.BalanceRange);
                ////}
                ////else
                ////{
                ////    sp_obj.BalanceRange = 0;
                ////}
                //if (!string.IsNullOrEmpty(objDeskAssignmentVM.FromBalance))
                //{
                //    sp_obj.FromBalance = Convert.ToString(objDeskAssignmentVM.FromBalance);
                //}
                //else
                //{
                //    sp_obj.FromBalance = null;
                //}
                //if (!string.IsNullOrEmpty(objDeskAssignmentVM.ThruBalance))
                //{
                //    sp_obj.ThruBalance = Convert.ToString(objDeskAssignmentVM.ThruBalance);
                //}
                //else
                //{
                //    sp_obj.ThruBalance = null;
                //}
                //sp_obj.StartFromDate = objDeskAssignmentVM.StartFromDate.ToShortDateString();
                //sp_obj.StartThruDate = objDeskAssignmentVM.StartDateThru.ToShortDateString();
                //sp_obj.NDFromDate = objDeskAssignmentVM.NoticeDeadline.ToShortDateString();
                //sp_obj.NDThruDate = objDeskAssignmentVM.NDDateThru.ToShortDateString();

                //if (!string.IsNullOrEmpty(objDeskAssignmentVM.JobAdd1))
                //{
                //    sp_obj.JobAdd1 = Convert.ToString(objDeskAssignmentVM.JobAdd1);
                //}
                //else
                //{
                //    sp_obj.JobAdd1 = null;
                //}
                //if (!string.IsNullOrEmpty(objDeskAssignmentVM.OwnerName))
                //{
                //    sp_obj.OwnerName = Convert.ToString(objDeskAssignmentVM.OwnerName);
                //}
                //else
                //{
                //    sp_obj.OwnerName = null;
                //}


                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sp_obj);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        public static List<ADVWLienReportsInfo> GetDetailList(DataTable dt)
        {
            List<ADVWLienReportsInfo> List = (from DataRow dr in dt.Rows
                                           select new ADVWLienReportsInfo()
                                           {
                                               Select = ((dr["Select"] != DBNull.Value ? Convert.ToBoolean(dr["Select"]) : false)),
                                               JobId = ((dr["JobId"] != DBNull.Value ? dr["JobId"].ToString() : "")),
                                               ClientCode = ((dr["Client"] != DBNull.Value ? dr["Client"].ToString() : "")),
                                               JobName = ((dr["JobName"] != DBNull.Value ? dr["JobName"].ToString() : "")),
                                               JobAdd1 = ((dr["JobAdd1"] != DBNull.Value ? dr["JobAdd1"].ToString() : "")),
                                               JobCity = ((dr["JobCity"] != DBNull.Value ? dr["JobCity"].ToString() : "")),
                                               JobState = ((dr["JobState"] != DBNull.Value ? dr["JobState"].ToString() : "")),
                                               DeskNum = ((dr["Desk"] != DBNull.Value ? dr["Desk"].ToString() : "")),
                                               StatusCode = ((dr["Status"] != DBNull.Value ? dr["Status"].ToString() : "")),
                                               DateAssigned = ((dr["DateAssigned"] != DBNull.Value ? Convert.ToDateTime(dr["DateAssigned"]) : DateTime.MinValue)),
                                               RevDate = ((dr["RevDate"] != DBNull.Value ? Convert.ToDateTime(dr["RevDate"]) : DateTime.MinValue)),
                                               EstBalance = ((dr["EstBalance"] != DBNull.Value ? Convert.ToDecimal(dr["EstBalance"]) : 0)),
                                               JobBalance = ((dr["JobBalance"] != DBNull.Value ? Convert.ToDecimal(dr["JobBalance"]) : 0)),
                                               StartDate = ((dr["StartDate"] != DBNull.Value ? Convert.ToDateTime(dr["StartDate"]) : DateTime.MinValue)),
                                               BranchNumber = ((dr["Branch"] != DBNull.Value ? dr["Branch"].ToString() : "")),
                                               ClientCustomer = ((dr["ClientCustomer"] != DBNull.Value ? dr["ClientCustomer"].ToString() : "")),
                                               GeneralContractor = ((dr["GeneralContractor"] != DBNull.Value ? dr["GeneralContractor"].ToString() : "")),
                                               OwnerName = ((dr["OwnerName"] != DBNull.Value ? dr["OwnerName"].ToString() : "")),
                                               Document = ((dr["Document"] != DBNull.Value ? Convert.ToBoolean(dr["Document"]) : false)),
                                               NoticeDeadlineDate = ((dr["NoticeDeadlineDate"] != DBNull.Value ? Convert.ToDateTime(dr["NoticeDeadlineDate"]) : DateTime.MinValue)),
                                               LateNotice = ((dr["LateNotice"] != DBNull.Value ? Convert.ToBoolean(dr["LateNotice"]) : false)),

                                           }).ToList();
            return List;
        }

        public static DataTable GetLienReportsInfoByJobId(int JobId)
        {
            string sql = "SELECT * FROM vwLienReportsInfo With (Nolock) WHERE JobId =" + JobId;

            var EntityList = DBO.Provider.GetTableView(sql);
            DataTable dt = EntityList.ToTable();
            return dt;
        }
    }
}
