﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using CRF.BLL.Providers;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADvwJobPlacementInfo
    {
        #region Properties

        public int BatchId { get; set; }
        public string JobName { get; set; }
        public string JobAdd1 { get; set; }
        public string JobAdd2 { get; set; }
        public string JobCity { get; set; }
        public string JobState { get; set; }
        public string JobZip { get; set; }
        public string JobNum { get; set; }
        public string GCRefNum { get; set; }
        public string GCName { get; set; }
        public string GCContactName { get; set; }
        public string GCAdd1 { get; set; }
        public string GCAdd2 { get; set; }
        public string GCCity { get; set; }
        public string GCState { get; set; }
        public string GCZip { get; set; }
        public string GCPhone1 { get; set; }
        public string GCPhone2 { get; set; }
        public string GCFax { get; set; }
        public string OwnrName  { get; set; }
        public string OwnrAdd1  { get; set; }
        public string OwnrAdd2  { get; set; }
        public string OwnrCity  { get; set; }
        public string OwnrState { get; set; }
        public string OwnrZip   { get; set; }
        public string OwnrPhone1 { get; set; }
        public string OwnrPhone2 { get; set; }
        public string OwnrFax    { get; set; }
        public string LenderName { get; set; }
        public string LenderAdd1 { get; set; }
        public string LenderAdd2 { get; set; }
        public string LenderCity { get; set; }
        public string LenderState { get; set; }
        public string LenderZip  { get; set; }
        public string LenderPhone1 { get; set; }
        public string LenderPhone2 { get; set; }
        public string LenderFax { get; set; }
        public string BondNum { get; set; }
        public bool RushOrderML { get; set; }
        public bool SameAsGC   { get; set; }
        public bool SameAsCust { get; set; }
        public bool IsError    { get; set; }
        public string ErrorDescription { get; set; }
        public int SubmittedByUserId { get; set; }
        public string PlacementSource { get; set; }
        public DateTime SubmittedOn { get; set; }
        public int JobActionId { get; set; }
        public bool SuretyBox { get; set; }
        public string ClientCode { get; set; }
        public int JobId { get; set; }
        public int BatchJobId { get; set; }
        public string SubmittedBy { get; set; }
        public bool RushOrder { get; set; }
        public bool OwnerSourceGC { get; set; }
        public bool MLAgent { get; set; }
        public int Id { get; set; }

        #endregion

        #region CRUD

        public static DataTable GetViewPlacementInfoList(string JobId)
        {
            try
            {
                string sql = "Select * from vwJobPlacementInfo Where JobId = " + JobId;
                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        public static DataTable JobActionsList(string JobActionId)
        {
            try
            {
                string sql2 = "Select Description from JobActions Where Id = " + JobActionId;
                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sql2);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

    


        #endregion


    }
}
