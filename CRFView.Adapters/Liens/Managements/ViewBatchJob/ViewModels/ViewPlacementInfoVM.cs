﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Liens.Managements.ViewBatchJob.ViewModels
{
    public class ViewPlacementInfoVM
    {
        public ViewPlacementInfoVM()
        {
            ObjADvwJobPlacementInfo = new ADvwJobPlacementInfo();
            BatchJobLegalPartiesList = new List<ADBatchJobLegalParties>();
        }

        public ADvwJobPlacementInfo ObjADvwJobPlacementInfo { get; set; }
        //public DataTable dtBatchJobLegalPartiesList { get; set; }
        public List<ADBatchJobLegalParties> BatchJobLegalPartiesList { get; set; }

        public string ByJobId { get; set; }
        public string Message { get; set; }
        public string JobType { get; set; }
        public int mybatchjobid { get; set; }
        public string value { get; set; }
      
    }
}
