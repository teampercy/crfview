﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Liens.Managements.ChangeCustInfo.ViewModels
{
    public class ChangeCustInfoVM
    {

        public ChangeCustInfoVM()
            {
            ChangeClientCodeList = CommonBindList.GetChangeClientCodeList();
            ClientCustomerList = new List<ADClientCustomer>();

        }
        public IEnumerable<SelectListItem> ChangeClientCodeList { get; set; }
        public int ClientCode { get; set; }
        public int ClientId { get; set; }
        public int Id { get; set; }
      
        public String RefNum { get; set; }
        public string RdbBtn { get; set; }

        
        public string cboCustName { get; set; }
        public DataTable dtChangeCustInfoList { get; set; }
        public List<ADClientCustomer> ClientCustomerList { get; set; }
        public ADClientCustomer ObjChangeCustInfo { get; set; }
    }
}
