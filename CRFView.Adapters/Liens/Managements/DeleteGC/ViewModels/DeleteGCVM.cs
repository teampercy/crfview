﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
namespace CRFView.Adapters.Liens.Managements.DeleteGC.ViewModels
{
    public class DeleteGCVM
    {
        public  DeleteGCVM()
        {
            ChangeClientCodeList = CommonBindList.GetChangeClientCodeList();
            DeleteGCList = new List<ADClientGeneralContractor>();
        }
        public IEnumerable<SelectListItem> ChangeClientCodeList { get; set; }
        public string value { get; set; } //custom Property
        public int ClientCode { get; set; }
        public int ClientId { get; set; }
        public int Id { get; set; }

        
        public String RefNum { get; set; }
        public String GeneralContractor { get; set; }
        public DataTable dtDeleteGCList { get; set; }
        public List<ADClientGeneralContractor> DeleteGCList { get; set; }
        public ADClientGeneralContractor ObjDeleteGC { get; set; }
        public String message { get; set; }
    }
}
