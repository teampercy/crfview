﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using Ionic.Zip;
using System.IO;
using CRF.BLL.CRFDB.VIEWS;
using System.Data;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Liens.Managements.DeleteGC.ViewModels;
using CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels;
using System.Runtime.InteropServices;

namespace CRFView.Adapters
{
    public class ADClientGeneralContractor
    {
        #region Properties
        public string GenId { get; set; }
     
        public int ClientId { get; set; }
        public String RefNum { get; set; }
       
        public String GeneralContractor { get; set; }

        public String ContactName { get; set; }
        public String DelGCName { get; set; }
        public String Id { get; set; }
        public String AddressLine1 { get; set; }
        public String AddressLine2 { get; set; }
        public String City { get; set; }
        public String State { get; set; }
        public String PostalCode { get; set; }
        public String Country { get; set; }
        public String Telephone1 { get; set; }
        public String Telephone2 { get; set; }
        public String Fax { get; set; }

        public string ContactEmail { get; set; }

        public string ContactEmail2 { get; set; }


        [Required(ErrorMessage = "Please enter your email address")]
       // [DataType(DataType.EmailAddress)]
        //[Display(Name = "Email address")]
        [MaxLength(50)]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}", ErrorMessage = "Please enter correct email")]
        public String Email { get; set; }
        public String GCLicNum { get; set; }
        public String SpecialInstruction { get; set; }
        public bool IsNonUSAddr { get; set; }
        public string MatchingKey { get; set; }
        public bool NoCallGC { get; set; }
        public bool GCAllSend { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public int BatchJobId { get; set; }
        public int BatchId { get; set; }
        public bool NoJointCk { get; set; }
        public int BatchJobLegalPartyId { get; set; }
        public bool DeleteFlag { get; set; }
        public bool NoEmail { get; set; }
        public bool NoFax { get; set; }
        public String DelRefNum { get; set; }
        public String DelGenId { get; set; }
        public String DelAdd1 { get; set; }
        public String DelAdd2 { get; set; }
        public String DelCity { get; set; }
        public String DelState { get; set; }
        public String DelZip { get; set; }

        public String RepGCName { get; set; }
        public String RepGenId { get; set; }
        public String RepRefNum { get; set; }
        public String RepAdd1 { get; set; }
        public String RepAdd2 { get; set; }
        public String RepCity { get; set; }
        public String RepState { get; set; }
        public String RepZip { get; set; }
        public string Name { get; set; }
        
        public string Address { get; set; }
        #endregion

        public static ADClientGeneralContractor ReadClientGeneralContractor(int Id)
        {
            ClientGeneralContractor myentity = new ClientGeneralContractor();
            ITable myentityItable = myentity;
            DBO.Provider.Read(Id.ToString(), ref myentityItable);
            myentity = (ClientGeneralContractor)myentityItable;
            AutoMapper.Mapper.CreateMap<ClientGeneralContractor, ADClientGeneralContractor>();
            return (AutoMapper.Mapper.Map<ADClientGeneralContractor>(myentity));
        }

        public static DataTable GetDeleteGCDt(DeleteGCVM objDeleteGCVM)
        {
            try
            {
                var sp_obj = new uspbo_Jobs_GetGeneralContractorList();
                sp_obj.ClientId = objDeleteGCVM.ClientCode;

                //sp_obj.Name = objDeleteGCVM.GeneralContractor.Trim() + "%";
                //sp_obj.Name = objDeleteGCVM.GeneralContractor.ToString();

                sp_obj.Name = $"{objDeleteGCVM.GeneralContractor}%";

                //if (!string.IsNullOrEmpty(objDeleteGCVM.RefNum))
                //{
                //    sp_obj.Reference = Convert.ToString(objDeleteGCVM.RefNum);
                //}
                //else
                //{
                //    sp_obj.RefNum = null;
                //}
                //if ((objDeleteGCVM.ClientId > 0))
                //{
                //    sp_obj.ClientId = Convert.ToInt32(objDeleteGCVM.ClientId);
                //}
                //else
                //{
                //    sp_obj.ClientId = 0;
                //}

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sp_obj);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        public static ADClientGeneralContractor GetGCList(DataTable dt)
        {
            List<ADClientGeneralContractor> List = (from DataRow dr in dt.Rows
                                           select new ADClientGeneralContractor()

                                           {
                                               GenId = ((dr["GenId"] != DBNull.Value ? dr["GenId"].ToString() : "")),
                                               ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : Int32.MinValue)),
                                              RefNum = ((dr["RefNum"] != DBNull.Value ? dr["RefNum"].ToString() : "")),
                                              GeneralContractor = ((dr["GeneralContractor"] != DBNull.Value ? dr["GeneralContractor"].ToString() : "")),
                                              ContactName = ((dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : "")),
                                              // DelGCName = ((dr["DelGCName"] != DBNull.Value ? dr["DelGCName"].ToString() : "")),
                                               AddressLine1 = ((dr["AddressLine1"] != DBNull.Value ? dr["AddressLine1"].ToString() : "")),
                                               AddressLine2 = ((dr["AddressLine2"] != DBNull.Value ? dr["AddressLine2"].ToString() : "")),
                                               City = ((dr["City"] != DBNull.Value ? dr["City"].ToString() : "")),
                                               State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),
                                               PostalCode = ((dr["PostalCode"] != DBNull.Value ? dr["PostalCode"].ToString() : "")),
                                               Country = ((dr["Country"] != DBNull.Value ? dr["Country"].ToString() : "")),
                                               Telephone1 = ((dr["Telephone1"] != DBNull.Value ? dr["Telephone1"].ToString() : "")),
                                               Fax = ((dr["Fax"] != DBNull.Value ? dr["Fax"].ToString() : "")),
                                               Email = ((dr["Email"] != DBNull.Value ? dr["Email"].ToString() : "")),
                                               GCLicNum = ((dr["GCLicNum"] != DBNull.Value ? dr["GCLicNum"].ToString() : "")),
                                               SpecialInstruction = ((dr["SpecialInstruction"] != DBNull.Value ? dr["SpecialInstruction"].ToString() : "")),
                                               IsNonUSAddr = ((dr["IsNonUSAddr"] != DBNull.Value ? Convert.ToBoolean(dr["IsNonUSAddr"]) : false)),
                                               NoCallGC = ((dr["NoCallGC"] != DBNull.Value ? Convert.ToBoolean(dr["NoCallGC"]) : false)),
                                               GCAllSend = ((dr["GCAllSend"] != DBNull.Value ? Convert.ToBoolean(dr["GCAllSend"]) : false)),
                                               NoEmail = ((dr["NoEmail"] != DBNull.Value ? Convert.ToBoolean(dr["NoEmail"]) : false)),
                                               NoFax = ((dr["NoFax"] != DBNull.Value ? Convert.ToBoolean(dr["NoFax"]) : false)),
                                               MatchingKey = ((dr["MatchingKey"] != DBNull.Value ? dr["MatchingKey"].ToString() : "")),
                                               CreatedOn = ((dr["CreatedOn"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedOn"]) : DateTime.MinValue)),
                                               CreatedBy = ((dr["CreatedBy"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBy"]) : 0)),
                                               BatchJobId = ((dr["BatchJobId"] != DBNull.Value ? Convert.ToInt32(dr["BatchJobId"]) : 0)),
                                               BatchId = ((dr["BatchId"] != DBNull.Value ? Convert.ToInt32(dr["BatchId"]) : 0)),
                                               BatchJobLegalPartyId = ((dr["BatchJobLegalPartyId"] != DBNull.Value ? Convert.ToInt32(dr["BatchJobLegalPartyId"]) : 0)),
                                               DeleteFlag = ((dr["DeleteFlag"] != DBNull.Value ? Convert.ToBoolean(dr["DeleteFlag"]) : false))
                                               //DelAdd1 = ((dr["DelAdd1"] != DBNull.Value ? dr["DelAdd1"].ToString() : "")),
                                               //DelAdd2 = ((dr["DelAdd2"] != DBNull.Value ? dr["DelAdd2"].ToString() : "")),
                                               //DelCity = ((dr["DelCity"] != DBNull.Value ? dr["DelCity"].ToString() : "")),
                                               //DelState = ((dr["DelState"] != DBNull.Value ? dr["DelState"].ToString() : "")),
                                               //DelZip = ((dr["DelZip"] != DBNull.Value ? dr["DelZip"].ToString() : "")),
                                               // RepGCName = ((dr["RepGCName"] != DBNull.Value ? dr["RepGCName"].ToString() : "")),
                                               // RepGenId = ((dr["RepGenId"] != DBNull.Value ? dr["RepGenId"].ToString() : "")),
                                               // RepRefNum = ((dr["RepRefNum"] != DBNull.Value ? dr["RepRefNum"].ToString() : "")),
                                               // RepAdd1 = ((dr["RepAdd1"] != DBNull.Value ? dr["RepAdd1"].ToString() : "")),
                                               //RepAdd2 = ((dr["RepAdd2"] != DBNull.Value ? dr["RepAdd2"].ToString() : "")),
                                               //RepCity = ((dr["RepCity"] != DBNull.Value ? dr["RepCity"].ToString() : "")),
                                               //RepState = ((dr["RepState"] != DBNull.Value ? dr["RepState"].ToString() : "")),
                                               //RepZip = ((dr["RepZip"] != DBNull.Value ? dr["RepZip"].ToString() : "")),

                                           }).ToList();

            return List.FirstOrDefault();
        }
        public static List<ADClientGeneralContractor> GetDeleteGCList(DataTable dt)
        {
            List<ADClientGeneralContractor> List = (from DataRow dr in dt.Rows
                                                select new ADClientGeneralContractor()

                                                {
                                                    ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32 (dr["ClientId"]) : Int32.MinValue)),
                                                    //DateRequested = ((dr["Name"] != DBNull.Value ? Convert.ToDateTime(dr["Name"]) : DateTime.MinValue)),
                                                    //ChangeTo = ((dr["ChangeTo"] != DBNull.Value ? Convert.ToDateTime(dr["ChangeTo"]) : DateTime.MinValue)),
                                                    Id = ((dr["Id"] != DBNull.Value ? dr["Id"].ToString() : "")),
                                                    GeneralContractor = ((dr["Name"] != DBNull.Value ? dr["Name"].ToString() : "")),
                                                   
                                                    //JobNum = ((dr["JobNum"] != DBNull.Value ? dr["JobNum"].ToString() : "")),
                                                    //DateAssigned = ((dr["DateAssigned"] != DBNull.Value ? Convert.ToDateTime(dr["DateAssigned"]) : DateTime.MinValue)),
                                                    //EstBalance = ((dr["EstBalance"] != DBNull.Value ? Convert.ToDecimal(dr["EstBalance"]) : 0)),
                                                    //StatusCode = ((dr["StatusCode"] != DBNull.Value ? dr["StatusCode"].ToString() : "")),
                                                    //DeskNum = ((dr["DeskNum"] != DBNull.Value ? dr["DeskNum"].ToString() : "")),
                                                    //ClientName = ((dr["ClientName"] != DBNull.Value ? dr["ClientName"].ToString() : "")),
                                                    //IsProcessed = ((dr["IsProcessed"] != DBNull.Value ? Convert.ToBoolean(dr["IsProcessed"]) : false)),

                                                }).ToList();

            return List;
        }

        public static int Delete_GC(DeleteGCVM objDeleteGCVM)
        {
            try
            {
                var result = "";
                var mysproc = new uspbo_LiensDayEnd_DeleteGC();

                if(Convert.ToInt32(objDeleteGCVM.ObjDeleteGC.RepGenId) > 0 && Convert.ToInt32(objDeleteGCVM.ObjDeleteGC.DelGenId) > 0)
                { 

                mysproc.ClientId = Convert.ToInt32(objDeleteGCVM.ClientId);
                mysproc.GenId = Convert.ToInt32(objDeleteGCVM.ObjDeleteGC.DelGenId);
                mysproc.NewGenId = Convert.ToInt32(objDeleteGCVM.ObjDeleteGC.RepGenId);

                DBO.Provider.ExecNonQuery(mysproc);
                result = "Completed";

                return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }


        public static DataTable GetGCListDt(string searchInitial, string ClientId,string RdbBtnVal)
        {
            try
            {
                var sp_obj = new uspbo_Jobs_GetGCList();

                sp_obj.ClientId = Convert.ToInt32(ClientId);
                if (RdbBtnVal == "N")
                {
                    sp_obj.Name = $"{searchInitial}%";
                }
                else
                {
                    sp_obj.Address = $"{searchInitial}%";
                }

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sp_obj);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        public static List<string> GetGCLookUpList1(DataTable dt)
        {

            var yy = new List<string>();
            foreach (DataRow dr in dt.Rows)
            {
                string name = dr["name"].ToString();
                //string Ref = dr["Ref"].ToString();
                string Address = dr["Address"].ToString();
                string Id = dr["Id"].ToString();


                yy.Add(name + "|" + Address + "|" + Id);
            }
            //yy.Add(dr["name"].ToString());
            //yy.Add(dr["Address"].ToString());


            return yy;
        }
        public static List<ADClientGeneralContractor> GetGCLookUpList(DataTable dt)
        {
            //DataTable dt = GetCustomerLookUpDt(objDeleteCustomerVM);

            List<ADClientGeneralContractor> List = (from DataRow dr in dt.Rows
                                           select new ADClientGeneralContractor()
                                           {
                                               //ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
                                               Id = ((dr["Id"] != DBNull.Value ? dr["Id"].ToString() : "")),
                                               Name = ((dr["Name"] != DBNull.Value ? dr["Name"].ToString() : "")),
                                              // Ref = ((dr["Ref"] != DBNull.Value ? dr["Ref"].ToString() : "")),
                                               Address = ((dr["Address"] != DBNull.Value ? dr["Address"].ToString() : "")),
                                           }).ToList();

            return List;
        }

        public static int SaveGC(GCEditVM objGCEditVM,int GenId,bool IsSameAsCustomerCheckedvisible)
        {
            try
            {
                objGCEditVM.obj_JobEdit = ADJob.ReadJob(Convert.ToInt32(objGCEditVM.JobId));
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                bool value = false;
                bool IsSameAsCustChecked = false;
                if (objGCEditVM.obj_JobEdit.GenId == 0)
                {
                    value = true;
                }
                ADClientGeneralContractor objADClientGeneralContractor = new ADClientGeneralContractor();
                //ADClientGeneralContractor objADClientGeneralContractor = ReadClientGeneralContractor(CustId);
                AutoMapper.Mapper.CreateMap<ADClientGeneralContractor, ClientGeneralContractor>();
                ClientGeneralContractor myentity;
                myentity = AutoMapper.Mapper.Map<ClientGeneralContractor>(objADClientGeneralContractor);
                ITable tblI = myentity;


                myentity.ClientId = Convert.ToInt32(objGCEditVM.ClientId);
                myentity.GeneralContractor = objGCEditVM.objADClientGeneralContractor.GeneralContractor;
                myentity.ContactName = objGCEditVM.objADClientGeneralContractor.ContactName;
                myentity.GCLicNum = objGCEditVM.objADClientGeneralContractor.GCLicNum;
                myentity.AddressLine1 = objGCEditVM.objADClientGeneralContractor.AddressLine1;
                myentity.AddressLine2 = objGCEditVM.objADClientGeneralContractor.AddressLine2;
                myentity.City = objGCEditVM.objADClientGeneralContractor.City;
                myentity.State = objGCEditVM.objADClientGeneralContractor.State;
                myentity.PostalCode = objGCEditVM.objADClientGeneralContractor.PostalCode;
                myentity.Telephone1 = objGCEditVM.objADClientGeneralContractor.Telephone1;
                myentity.Fax = objGCEditVM.objADClientGeneralContractor.Fax;
                myentity.Email = objGCEditVM.objADClientGeneralContractor.Email;
                myentity.SpecialInstruction = objGCEditVM.objADClientGeneralContractor.SpecialInstruction;


                if (objGCEditVM.objADClientGeneralContractor.NoCallGC == true)
                {
                    //objADClientGeneralContractor.NoCallGC = objGCEditVM.objADClientGeneralContractor.NoCallGC;
                    myentity.NoCallGC = objGCEditVM.objADClientGeneralContractor.NoCallGC;

                }
                else
                {
                    //objADClientGeneralContractor.NoCallGC = false;
                    myentity.NoCallGC = false;
                }

                if (objGCEditVM.objADClientGeneralContractor.NoEmail == true)
                {
                    //objADClientGeneralContractor.NoEmail = objGCEditVM.objADClientGeneralContractor.NoEmail;
                    myentity.NoEmail = objGCEditVM.objADClientGeneralContractor.NoEmail;

                }
                else
                {
                    //objADClientGeneralContractor.NoEmail = false;
                    myentity.NoEmail = false;
                }

                if (objGCEditVM.objADClientGeneralContractor.NoFax == true)
                {
                    //objADClientGeneralContractor.NoFax = objGCEditVM.objADClientGeneralContractor.NoFax;
                    myentity.NoFax = objGCEditVM.objADClientGeneralContractor.NoFax;
                }
                else
                {
                    //objADClientGeneralContractor.NoFax = false;
                    myentity.NoFax = false;
                }


                if (objGCEditVM.objADClientGeneralContractor.GCAllSend == true)
                {
                    //objADClientGeneralContractor.GCAllSend = objGCEditVM.objADClientGeneralContractor.GCAllSend;
                    myentity.GCAllSend = objGCEditVM.objADClientGeneralContractor.GCAllSend;
                }
                else
                {
                    //objADClientGeneralContractor.GCAllSend = false;
                    myentity.GCAllSend = false;
                }

                if (objGCEditVM.objADClientGeneralContractor.IsNonUSAddr == true)
                {
                    //objADClientGeneralContractor.GCAllSend = objGCEditVM.objADClientGeneralContractor.GCAllSend;
                    myentity.IsNonUSAddr = objGCEditVM.objADClientGeneralContractor.IsNonUSAddr;
                }
                else
                {
                    //objADClientGeneralContractor.GCAllSend = false;
                    myentity.IsNonUSAddr = false;
                }





                var myacc = new Job();
                ITable IJob = myacc;
                DBO.Provider.Read(objGCEditVM.JobId.ToString(), ref IJob);
                 myacc = (Job)IJob;
                
                IJob = myacc;

                if (GenId < 1 && myacc.GenId > 1)
                {
                    objGCEditVM.IsPrimary = false;
                }

                if (myacc.GenId < 1)
                {
                    objGCEditVM.IsPrimary = true;
                }

                var myLegalParties = new JobLegalParties();

                ITable ILegalParties = myLegalParties;
                DBO.Provider.Read(myacc.LenderId.ToString(), ref ILegalParties);
                myLegalParties.Id = myacc.LenderId;
                myLegalParties.JobId = Convert.ToInt32(objGCEditVM.JobId);
                myLegalParties.IsPrimary = false;
                myLegalParties.TypeCode = "GC";
                ILegalParties = myLegalParties;

                if(IsSameAsCustChecked == true)
                {

                    int iNewGenId;
                    var sp_obj = new uspbo_Jobs_CreateGeneralContractor();


                    sp_obj.ClientId = Convert.ToInt32(objGCEditVM.JobId);
                    sp_obj.GeneralContractor = objGCEditVM.objADClientGeneralContractor.GeneralContractor;
                    sp_obj.ContactName = objGCEditVM.objADClientGeneralContractor.ContactName;
                   // sp_obj.GCLicNum = objGCEditVM.objADClientGeneralContractor.GCLicNum;
                    sp_obj.AddressLine1 = objGCEditVM.objADClientGeneralContractor.AddressLine1;
                    sp_obj.AddressLine2 = objGCEditVM.objADClientGeneralContractor.AddressLine2;
                    sp_obj.City = objGCEditVM.objADClientGeneralContractor.City;
                    sp_obj.State = objGCEditVM.objADClientGeneralContractor.State;
                    sp_obj.PostalCode = objGCEditVM.objADClientGeneralContractor.PostalCode;
                    sp_obj.Telephone1 = objGCEditVM.objADClientGeneralContractor.Telephone1;
                    sp_obj.Fax = objGCEditVM.objADClientGeneralContractor.Fax;
                    sp_obj.Email = objGCEditVM.objADClientGeneralContractor.Email;
                    sp_obj.SpecialInstruction = objGCEditVM.objADClientGeneralContractor.SpecialInstruction;
                    sp_obj.CreatedBy = user.Id;
                    sp_obj.GCAllSend = objGCEditVM.objADClientGeneralContractor.GCAllSend;
                    sp_obj.NoCallGC = objGCEditVM.objADClientGeneralContractor.NoCallGC;

                    var myview = DBO.Provider.GetTableView(sp_obj);
                    myview.FillEntity(ref tblI);
                        iNewGenId = myentity.GenId;
                    LogGCChanges(objGCEditVM.obj_JobEdit.GenId, myentity, value);

                }

               
                
                    // DBO.Provider.Create(ref tblI);
               
                else if(IsSameAsCustChecked == false && GenId < 1)
                {
                    LogGCChanges(objGCEditVM.obj_JobEdit.GenId, myentity, value);
                    DBO.Provider.Create(ref tblI);
                }

                else
                {

                    myentity.GenId = GenId;
                    LogGCChanges(objGCEditVM.obj_JobEdit.GenId, myentity, value);

                    DBO.Provider.Update(ref tblI);

                }

                if(objGCEditVM.IsPrimary == true)
                {
                    myacc.GenId = myentity.GenId; 
                   //if (objGCEditVM.MakeSameAsCustomerchkbox == true)
                    if (IsSameAsCustomerCheckedvisible == true)
                    {
                        //myacc.SameAsCust = objGCEditVM.MakeSameAsCustomerchked;
                        myacc.SameAsCust = objGCEditVM.IsSameAsCustomerChecked;
                        myacc.IsGCUnverified = objGCEditVM.IsGCUnverified;
                    }
                    DBO.Provider.Update(ref IJob);
                }

                else
                {
                    myLegalParties.SecondaryId = myentity.GenId;
                    if (myLegalParties.Id > 0)
                    {
                        DBO.Provider.Update(ref ILegalParties);
                    }
                    else
                    {
                        DBO.Provider.Create(ref ILegalParties);
                     }
                }

               

                //DBO.Provider.Update(ref tblI);

                return 1;


            }

            catch (Exception ex)
            {
                return 0;
            }
        }

        public static void LogGCChanges(int GenId, ClientGeneralContractor myentity, bool IsCreate, [Optional] bool IsDelete)
        {
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

            GCEditVM objGCEditVM = new GCEditVM();
            ADClientCustomer objADClientCustomer = ADClientCustomer.ReadClientCustomer(myentity.ClientId);
            var mysproc = new CRF.BLL.CRFDB.SPROCS.uspbo_Jobs_UpdateChangeLog();

            mysproc.JobId = Convert.ToInt32(objGCEditVM.JobId);
            mysproc.UserId = user.Id;
            mysproc.UserName = user.UserName;
            mysproc.CustId = myentity.GenId;

            mysproc.ChangeType = 2;

            if (myentity.GenId > 0)
            {

                mysproc.ChangeFrom = myentity.GeneralContractor.Trim() + "~";
                mysproc.ChangeFrom += myentity.AddressLine1.Trim() + "~";
                mysproc.ChangeFrom += myentity.AddressLine2.Trim() + "~";
                mysproc.ChangeFrom += myentity.City.Trim() + "~";
                mysproc.ChangeFrom += myentity.State.Trim() + "~";
                mysproc.ChangeFrom += myentity.PostalCode.Trim() + "~";
                mysproc.ChangeFrom += myentity.Email.Trim() + "~";
               // mysproc.ChangeFrom += "NOSEND:(" + myentity.CustNoSend + ")" + "~";
                mysproc.ChangeFrom += "ALLSEND:(" + myentity.GCAllSend + ")" + "~";
                mysproc.ChangeFrom += "NOCALL:(" + myentity.NoCallGC + ")" + "~";
               // mysproc.ChangeFrom += "NONTO:(" + myentity.NoNTO + ")" + "~"; ;

            }

            if(IsDelete == true)
            {
                mysproc.ChangeTo = "DELETED";
            mysproc.ChangeTo = myentity.GeneralContractor.Trim() + "~";
            mysproc.ChangeTo += myentity.AddressLine1.Trim() + "~";
            mysproc.ChangeTo += myentity.AddressLine2.Trim() + "~";
            mysproc.ChangeTo += myentity.City.Trim() + "~";
            mysproc.ChangeTo += myentity.State.Trim() + "~";
            mysproc.ChangeTo += myentity.PostalCode.Trim() + "~";
            mysproc.ChangeTo += myentity.Email.Trim() + "~";
            //mysproc.ChangeTo += "NOSEND:(" + myentity.CustNoSend + ")" + "~";
            mysproc.ChangeTo += "ALLSEND:(" + myentity.GCAllSend + ")" + "~";
            mysproc.ChangeTo += "NOCALL:(" + myentity.NoCallGC + ")" + "~";
                // mysproc.ChangeTo += "NONTO:(" + myentity.NoNTO + ")" + "~";
            }

            if (mysproc.ChangeFrom != mysproc.ChangeTo)
            {
                DBO.Provider.ExecNonQuery(mysproc);
            }

        }

        public static int DeleteGC(GCEditVM objGCEditVM, int GenId)
        {
            try
            {
                bool value = false;
                objGCEditVM.objADClientGeneralContractor = ReadClientGeneralContractor(GenId);
                ClientGeneralContractor myentity = new ClientGeneralContractor();
                var myLegalParties = new JobLegalParties();

                ITable ILegalParties = myLegalParties;
               

                ILegalParties = myLegalParties;

                if (myLegalParties.Id > 0)
                {

                    DBO.Provider.Delete(ref ILegalParties);
                }


                if (GenId == 0)
                {
                    value = true;
                }
                
                LogGCChanges(GenId, myentity, value,true);

                if(objGCEditVM.IsPrimary == true)
                { 
                var myacc = new Job();

                ITable IJob = myacc;
                DBO.Provider.Read(objGCEditVM.JobId, ref IJob);
                myacc.GenId = 0;
                myacc.SameAsCust = false;
                IJob = myacc;
                DBO.Provider.Update(ref IJob);
                }
                //DBO.Provider.Update(ref IJob);
                // ADJobLegalParties obj_ADJobLegalParties = ADJobLegalParties.ReadJobLegalParties(objCustomerEditVM.obj_JobEdit.LenderId);







                return 1;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }

    }
}
