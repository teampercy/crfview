﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Liens.Managements.ChangeClient.ViewModels
{
    public class ChangeClientCodeVM
   
    {
        public  ChangeClientCodeVM()
        {
            objvwJobsList = new ADvwJobsList();
            ChangeClientCodeList = CommonBindList.GetChangeClientCodeList();
        }
        public IEnumerable<SelectListItem> ChangeClientCodeList { get; set; }
        public ADJob ObjJobEdit { get; set; }
        public string Message { get; set; }
        public string MessageDiff { get; set; }
        public string JobId { get; set; }
        public ADvwJobsList objvwJobsList { get; set; }

    }
}
