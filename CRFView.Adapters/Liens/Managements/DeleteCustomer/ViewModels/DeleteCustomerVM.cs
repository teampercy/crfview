﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Liens.Managements.DeleteCustomer.ViewModels
{
    public class DeleteCustomerVM
    {
        public DeleteCustomerVM()
        {
            objADClient = new ADClient();
            ClientCustomerList = new List<ADClientCustomer>();
            ChangeClientCodeList = CommonBindList.GetChangeClientCodeList();
           
        }

        public IEnumerable<SelectListItem> ChangeClientCodeList { get; set; }
        public ADClient objADClient { get; set; }
        public ADClientCustomer objADClientCustomer { get; set; }
        public DataTable dtCustomerLoupList { get; set; }

        public List<ADClientCustomer> ClientCustomerList { get; set; }

        //custom Property

        public string ClientId { get; set; }
        public int CustId { get; set; }
        public int Delvalue { get; set; }
        public int Repvalue { get; set; }

        public int DelCustId { get; set; }
        public string DelCustName { get; set; }
        public string DelRefNum { get; set; }
        public string DelAddr1 { get; set; }
        public string DelAddr2 { get; set; }
        public string DelCity { get; set; }
        public string DelState { get; set; }
        public string DelZip { get; set; }

        public int RepCustId { get; set; }
        public string RepCustName { get; set; }
        public string RepRefNum { get; set; }
        public string RepAddr1 { get; set; }
        public string RepAddr2 { get; set; }
        public string RepCity { get; set; }
        public string RepState { get; set; }
        public string RepZip { get; set; }

        public string RdbBtn { get; set; }
        
        public string cboCustName { get; set; }
        public string Message { get; set; }

    }
}
