﻿using CRF.BLL.COMMON;
using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using CRFView.Adapters.Liens.Managements.ChangeCustInfo.ViewModels;
using CRFView.Adapters.Liens.Managements.DeleteCustomer.ViewModels;
using CRFView.Adapters.Liens.Verifiers.WorkCard;
using CRFView.Adapters.Liens.Verifiers.WorkCard.ViewModels;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADClientCustomer
    {
        #region Properties

        public int CustId { get; set; }
        public int ClientId { get; set; }
        public string RefNum { get; set; }
        public string ClientCustomer { get; set; }
        public string ContactName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string Country { get; set; }
        public string Telephone1 { get; set; }
        public string Telephone2 { get; set; }
        public string Fax { get; set; }
        public string MatchingKey { get; set; }
        public int CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string Email { get; set; }
        public string SpecialInstruction { get; set; }
        public bool CustNoSend { get; set; }
        public bool NoCallCust { get; set; }
        public bool CustAllSend { get; set; }
        public bool NoNTO { get; set; }
        public DateTime UpdateDate { get; set; }
        public int BatchJobId { get; set; }
        public int BatchId { get; set; }
        public bool NoJointCk { get; set; }
        public bool IsNonUSAddr { get; set; }
        public bool DeleteFlag { get; set; }
        public bool NoCallsCustGc { get; set; }
        public int BatchCustId { get; set; }
        public string NARPAcctNo { get; set; }
        public string Status { get; set; }
        public string BranchNum { get; set; }
        public string SmanNum { get; set; }
        public string ParentCompany { get; set; }
        public int UpdateBy { get; set; }
        public int RiskRating { get; set; }
        public DateTime RiskRatingUpdateDate { get; set; }
        public bool CustPublic { get; set; }
        public bool CustNotConst { get; set; }
        public bool NoFax { get; set; }
        public bool NoEmail { get; set; }
        public string CreditScore { get; set; }
        public string CreditMgrCode { get; set; }
        public DateTime OpenDate { get; set; }

        // custom Property
        public string Id { get; set; }
        public string Name { get; set; }
        public string Ref { get; set; }
        public string Address { get; set; }
        public string Website { get; set; }

        #endregion

        public static ADClientCustomer ReadClientCustomer(int Id)
        {
            ClientCustomer myentity = new ClientCustomer();
            ITable myentityItable = myentity;
            DBO.Provider.Read(Id.ToString(), ref myentityItable);
            myentity = (ClientCustomer)myentityItable;
            AutoMapper.Mapper.CreateMap<ClientCustomer, ADClientCustomer>();
            return (AutoMapper.Mapper.Map<ADClientCustomer>(myentity));
        }


        public static DataTable GetCustomerLookUpDt(DeleteCustomerVM objDeleteCustomerVM)
        {
            try
            {
                var sp_obj = new uspbo_Jobs_GetCustomerList();

                sp_obj.ClientId = Convert.ToInt32(objDeleteCustomerVM.ClientId);

                if (objDeleteCustomerVM.RdbBtn == "R")
                {
                    sp_obj.Reference = $"{objDeleteCustomerVM.cboCustName}%";
                }
                else
                {
                    sp_obj.Name = $"{objDeleteCustomerVM.cboCustName}%";
                }


                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sp_obj);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }


        public static DataTable GetCustomerListDt(string searchInitial,string ClientId,string RdbBtnVal)
  {
            try
            {
                var sp_obj = new uspbo_Jobs_GetCUList();
                //var sp_obj = new uspbo_Jobs_GetCUListTest();
                //uspbo_Jobs_GetCUList_bckup_09_23_2021

                sp_obj.ClientId = Convert.ToInt32(ClientId);
                if (RdbBtnVal == "N")
                {
                    sp_obj.Name = $"{searchInitial}%";

                }
                else
                {
                    sp_obj.Add = $"{searchInitial}%";
                }

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sp_obj);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        public static List<ADClientCustomer> GetCustomerLookUpList(DataTable dt)
        {
            //DataTable dt = GetCustomerLookUpDt(objDeleteCustomerVM);

            List<ADClientCustomer> List = (from DataRow dr in dt.Rows
                                           select new ADClientCustomer()
                                           {
                                               ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
                                               Id = ((dr["Id"] != DBNull.Value ? dr["Id"].ToString() : "")),
                                               Name = ((dr["Name"] != DBNull.Value ? dr["Name"].ToString() : "")),
                                               Ref = ((dr["Ref"] != DBNull.Value ? dr["Ref"].ToString() : "")),
                                               Address = ((dr["Address"] != DBNull.Value ? dr["Address"].ToString() : "")),
                                           }).ToList();

            return List;
        }
        public static List<string> GetCustomerLookUpList1(DataTable dt)
       {
           
            var yy = new List<string>();
            foreach (DataRow dr in dt.Rows)
            {
                string name = dr["name"].ToString();
                string Ref = dr["Ref"].ToString();
                string Address = dr["Address"].ToString();
                string Id = dr["Id"].ToString();
                

                yy.Add(name + "|" + Ref + "|" + Address + "|" + Id);
            }
            //yy.Add(dr["name"].ToString());
                //yy.Add(dr["Address"].ToString());

           
            return yy;
        }
        public static ADClientCustomer GetClientCustomerList(DataTable dt)
        {
            
            List<ADClientCustomer> List = (from DataRow dr in dt.Rows
                                           select new ADClientCustomer()
                                           {
                                               CustId = ((dr["CustId"] != DBNull.Value ? Convert.ToInt32(dr["CustId"]) : 0)),
                                               ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
                                               RefNum = ((dr["RefNum"] != DBNull.Value ? dr["RefNum"].ToString() : "")),
                                               ClientCustomer = ((dr["ClientCustomer"] != DBNull.Value ? dr["ClientCustomer"].ToString() : "")),
                                               ContactName = ((dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : "")),
                                               AddressLine1 = ((dr["AddressLine1"] != DBNull.Value ? dr["AddressLine1"].ToString() : "")),
                                               AddressLine2 = ((dr["AddressLine2"] != DBNull.Value ? dr["AddressLine2"].ToString() : "")),
                                               City = ((dr["City"] != DBNull.Value ? dr["City"].ToString() : "")),
                                               State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),
                                               PostalCode = ((dr["PostalCode"] != DBNull.Value ? dr["PostalCode"].ToString() : "")),
                                               Country = ((dr["Country"] != DBNull.Value ? dr["Country"].ToString() : "")),
                                               Telephone1 = ((dr["Telephone1"] != DBNull.Value ? dr["Telephone1"].ToString() : "")),
                                               Telephone2 = ((dr["Telephone2"] != DBNull.Value ? dr["Telephone2"].ToString() : "")),
                                               Fax = ((dr["Fax"] != DBNull.Value ? dr["Fax"].ToString() : "")),
                                               MatchingKey = ((dr["MatchingKey"] != DBNull.Value ? dr["MatchingKey"].ToString() : "")),
                                               CreatedOn = ((dr["CreatedOn"] != DBNull.Value ? Convert.ToDateTime(dr["CreatedOn"]) : DateTime.MinValue)),
                                               CreatedBy = ((dr["CreatedBy"] != DBNull.Value ? Convert.ToInt32(dr["CreatedBy"]) : 0)),
                                               Email = ((dr["Email"] != DBNull.Value ? dr["Email"].ToString() : "")),
                                               SpecialInstruction = ((dr["SpecialInstruction"] != DBNull.Value ? dr["SpecialInstruction"].ToString() : "")),
                                               CustNoSend = ((dr["CustNoSend"] != DBNull.Value ? Convert.ToBoolean(dr["CustNoSend"]) : false)),
                                               CustAllSend = ((dr["CustAllSend"] != DBNull.Value ? Convert.ToBoolean(dr["CustAllSend"]) : false)),
                                               NoNTO = ((dr["NoNTO"] != DBNull.Value ? Convert.ToBoolean(dr["NoNTO"]) : false)),
                                               UpdateDate = ((dr["UpdateDate"] != DBNull.Value ? Convert.ToDateTime(dr["UpdateDate"]) : DateTime.MinValue)),
                                               BatchJobId = ((dr["BatchJobId"] != DBNull.Value ? Convert.ToInt32(dr["BatchJobId"]) : 0)),
                                               BatchId = ((dr["BatchId"] != DBNull.Value ? Convert.ToInt32(dr["BatchId"]) : 0)),
                                               NoJointCk = ((dr["NoJointCk"] != DBNull.Value ? Convert.ToBoolean(dr["NoJointCk"]) : false)),
                                               IsNonUSAddr = ((dr["IsNonUSAddr"] != DBNull.Value ? Convert.ToBoolean(dr["IsNonUSAddr"]) : false)),
                                               DeleteFlag = ((dr["DeleteFlag"] != DBNull.Value ? Convert.ToBoolean(dr["DeleteFlag"]) : false)),
                                               NoCallsCustGc = ((dr["NoCallsCustGc"] != DBNull.Value ? Convert.ToBoolean(dr["NoCallsCustGc"]) : false)),
                                               BatchCustId = ((dr["BatchCustId"] != DBNull.Value ? Convert.ToInt32(dr["BatchCustId"]) : 0)),
                                               NARPAcctNo = ((dr["NARPAcctNo"] != DBNull.Value ? dr["NARPAcctNo"].ToString() : "")),
                                               Status = ((dr["Status"] != DBNull.Value ? dr["Status"].ToString() : "")),
                                               BranchNum = ((dr["BranchNum"] != DBNull.Value ? dr["BranchNum"].ToString() : "")),
                                               SmanNum = ((dr["SmanNum"] != DBNull.Value ? dr["SmanNum"].ToString() : "")),
                                               ParentCompany = ((dr["ParentCompany"] != DBNull.Value ? dr["ParentCompany"].ToString() : "")),
                                               UpdateBy = ((dr["UpdateBy"] != DBNull.Value ? Convert.ToInt32(dr["UpdateBy"]) : 0)),
                                               RiskRating = ((dr["RiskRating"] != DBNull.Value ? Convert.ToInt32(dr["RiskRating"]) : 0)),
                                               RiskRatingUpdateDate = ((dr["RiskRatingUpdateDate"] != DBNull.Value ? Convert.ToDateTime(dr["RiskRatingUpdateDate"]) : DateTime.MinValue)),
                                               CustPublic = ((dr["CustPublic"] != DBNull.Value ? Convert.ToBoolean(dr["CustPublic"]) : false)),
                                               CustNotConst = ((dr["CustNotConst"] != DBNull.Value ? Convert.ToBoolean(dr["CustNotConst"]) : false)),
                                               NoFax = ((dr["NoFax"] != DBNull.Value ? Convert.ToBoolean(dr["NoFax"]) : false)),
                                               NoEmail = ((dr["NoEmail"] != DBNull.Value ? Convert.ToBoolean(dr["NoEmail"]) : false)),
                                               CreditScore = ((dr["CreditScore"] != DBNull.Value ? dr["CreditScore"].ToString() : "")),
                                               CreditMgrCode = ((dr["CreditMgrCode"] != DBNull.Value ? dr["CreditMgrCode"].ToString() : "")),
                                               OpenDate = ((dr["OpenDate"] != DBNull.Value ? Convert.ToDateTime(dr["OpenDate"]) : DateTime.MinValue)),

                                           }).ToList();

            return List.First();
        }

        public static int LoadRepCustomerDetails(string CustId)
        {
            try
            {

                DeleteCustomerVM objDeleteCustomerVM = new DeleteCustomerVM();

                ClientCustomer objClientCustomer = new ClientCustomer();
                objDeleteCustomerVM.objADClientCustomer = ADClientCustomer.ReadClientCustomer(objDeleteCustomerVM.objADClientCustomer.CustId);


                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }


        public static int GetDetails_DeleteCustomer(DeleteCustomerVM objDeleteCustomerVM)
        {
            try
            {
                var result = "";
                var mysproc = new uspbo_LiensDayEnd_DeleteCustomer();
                if (objDeleteCustomerVM.RepCustId > 0 && objDeleteCustomerVM.DelCustId > 0)
                {
                    mysproc.ClientId = Convert.ToInt32(objDeleteCustomerVM.ClientId);
                    mysproc.CustId = Convert.ToInt32(objDeleteCustomerVM.DelCustId);
                    mysproc.NewCustId = Convert.ToInt32(objDeleteCustomerVM.RepCustId);

                    DBO.Provider.ExecNonQuery(mysproc);
                    result = "Completed";

                    return 1;
                }
                else
                {
                    return 0;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }
        public static DataTable GetCustomerLookUpDt(ChangeCustInfoVM objChangeCustInfoVM)
        {
            try
            {
                var sp_obj = new uspbo_Jobs_GetCustomerList();

                sp_obj.ClientId = Convert.ToInt32(objChangeCustInfoVM.ClientCode);

                if (objChangeCustInfoVM.RdbBtn == "R")
                {
                    sp_obj.Reference = $"{objChangeCustInfoVM.cboCustName}%";
                }
                else
                {
                   
                    sp_obj.Name = $"{objChangeCustInfoVM.cboCustName}%";
                }


                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sp_obj);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        public static List<ADClientCustomer> GetChangecUstInfoLookUpList(DataTable dt)
        {
            //DataTable dt = GetCustomerLookUpDt(objDeleteCustomerVM);

            List<ADClientCustomer> List = (from DataRow dr in dt.Rows
                                           select new ADClientCustomer()
                                           {
                                               ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
                                               Id = ((dr["Id"] != DBNull.Value ? dr["Id"].ToString() : "")),
                                               Name = ((dr["Name"] != DBNull.Value ? dr["Name"].ToString() : "")),
                                               Ref = ((dr["Ref"] != DBNull.Value ? dr["Ref"].ToString() : "")),
                                               Address = ((dr["Address"] != DBNull.Value ? dr["Address"].ToString() : "")),
                                           }).ToList();

            return List;
        }

        public static int UpdateCustomer(ChangeCustInfoVM ObjChangeCustInfoVM)
        {
            try
            {
                ADClientCustomer objADJobOriginal = ReadClientCustomer(ObjChangeCustInfoVM.ObjChangeCustInfo.CustId);

                objADJobOriginal.ClientId = ObjChangeCustInfoVM.ClientCode;
               
                    if (Convert.ToInt32(objADJobOriginal.CustId) > 0)
                    {
                    objADJobOriginal.ClientCustomer = ObjChangeCustInfoVM.ObjChangeCustInfo.ClientCustomer;
                    objADJobOriginal.RefNum = ObjChangeCustInfoVM.ObjChangeCustInfo.RefNum;
                    objADJobOriginal.ContactName = ObjChangeCustInfoVM.ObjChangeCustInfo.ContactName;
                    objADJobOriginal.AddressLine1 = ObjChangeCustInfoVM.ObjChangeCustInfo.AddressLine1;
                    objADJobOriginal.AddressLine2 = ObjChangeCustInfoVM.ObjChangeCustInfo.AddressLine2;
                    objADJobOriginal.City = ObjChangeCustInfoVM.ObjChangeCustInfo.City;
                    objADJobOriginal.State = ObjChangeCustInfoVM.ObjChangeCustInfo.State;
                    objADJobOriginal.PostalCode = ObjChangeCustInfoVM.ObjChangeCustInfo.PostalCode;
                    objADJobOriginal.Telephone1 = ObjChangeCustInfoVM.ObjChangeCustInfo.Telephone1;
                    objADJobOriginal.Fax = ObjChangeCustInfoVM.ObjChangeCustInfo.Fax;
                    objADJobOriginal.Email = ObjChangeCustInfoVM.ObjChangeCustInfo.Email;


                    if (ObjChangeCustInfoVM.ObjChangeCustInfo.NoCallCust == true)
                    {
                        objADJobOriginal.NoCallCust = ObjChangeCustInfoVM.ObjChangeCustInfo.NoCallCust;
                    }
                    else
                    {
                        objADJobOriginal.NoCallCust = false;
                    }

                    if (ObjChangeCustInfoVM.ObjChangeCustInfo.CustAllSend == true)
                    {
                        objADJobOriginal.CustAllSend = ObjChangeCustInfoVM.ObjChangeCustInfo.CustAllSend;
                    }
                    else
                    {
                        objADJobOriginal.CustAllSend = false;
                    }

                    if (ObjChangeCustInfoVM.ObjChangeCustInfo.CustNoSend == true)
                    {
                        objADJobOriginal.CustNoSend = ObjChangeCustInfoVM.ObjChangeCustInfo.CustNoSend;
                    }
                    else
                    {
                        objADJobOriginal.CustNoSend = false;
                    }


                    if (ObjChangeCustInfoVM.ObjChangeCustInfo.NoJointCk == true)
                    {
                        objADJobOriginal.NoJointCk = ObjChangeCustInfoVM.ObjChangeCustInfo.NoJointCk;
                    }
                    else
                    {
                        objADJobOriginal.NoJointCk = false;
                    }

                    if (ObjChangeCustInfoVM.ObjChangeCustInfo.NoNTO == true)
                    {
                        objADJobOriginal.NoNTO = ObjChangeCustInfoVM.ObjChangeCustInfo.NoNTO;
                    }
                    else
                    {
                        objADJobOriginal.NoNTO = false;
                    }

                    AutoMapper.Mapper.CreateMap<ADClientCustomer, ClientCustomer>();
                    ClientCustomer myentity;
                    myentity = AutoMapper.Mapper.Map<ClientCustomer>(objADJobOriginal);
                   

                    ITable tblI = myentity;
                    DBO.Provider.Update(ref tblI);

                    return 1;
                }
                else
                {
                    return 0;
                }
            
            }
            
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static int SaveCustomer(CustomerEditVM objCustomerEditVM,int CustId)
        {
            try
            {
                ADClientCustomer objADJobOriginal = ReadClientCustomer(CustId);

                // objADJobOriginal.ClientId = objCustomerEditVM.obj_ClientCustomer.ClientCode;

                // if (Convert.ToInt32(objADJobOriginal.CustId) > 0)
                objADJobOriginal.ClientId = Convert.ToInt32(objCustomerEditVM.ClientId);
                objADJobOriginal.ClientCustomer = objCustomerEditVM.obj_ClientCustomer.ClientCustomer;
                    objADJobOriginal.RefNum = objCustomerEditVM.obj_ClientCustomer.RefNum;
                    objADJobOriginal.ContactName = objCustomerEditVM.obj_ClientCustomer.ContactName;
                    objADJobOriginal.AddressLine1 = objCustomerEditVM.obj_ClientCustomer.AddressLine1;
                    objADJobOriginal.AddressLine2 = objCustomerEditVM.obj_ClientCustomer.AddressLine2;
                    objADJobOriginal.City = objCustomerEditVM.obj_ClientCustomer.City;
                    objADJobOriginal.State = objCustomerEditVM.obj_ClientCustomer.State;
                    objADJobOriginal.PostalCode = objCustomerEditVM.obj_ClientCustomer.PostalCode;
                    objADJobOriginal.Telephone1 = objCustomerEditVM.obj_ClientCustomer.Telephone1;
                    objADJobOriginal.Fax = objCustomerEditVM.obj_ClientCustomer.Fax;
                    objADJobOriginal.Email = objCustomerEditVM.obj_ClientCustomer.Email;
                    objADJobOriginal.Website = objCustomerEditVM.obj_ClientCustomer.Website;
                    objADJobOriginal.CreditScore = objCustomerEditVM.obj_ClientCustomer.CreditScore;


                if (objCustomerEditVM.obj_ClientCustomer.NoCallCust == true)
                    {
                        objADJobOriginal.NoCallCust = objCustomerEditVM.obj_ClientCustomer.NoCallCust;
                    }
                    else
                    {
                        objADJobOriginal.NoCallCust = false;
                    }

                    if (objCustomerEditVM.obj_ClientCustomer.CustAllSend == true)
                    {
                        objADJobOriginal.CustAllSend = objCustomerEditVM.obj_ClientCustomer.CustAllSend;
                    }
                    else
                    {
                        objADJobOriginal.CustAllSend = false;
                    }

                    if (objCustomerEditVM.obj_ClientCustomer.CustNoSend == true)
                    {
                        objADJobOriginal.CustNoSend = objCustomerEditVM.obj_ClientCustomer.CustNoSend;
                    }
                    else
                    {
                        objADJobOriginal.CustNoSend = false;
                    }


                    if (objCustomerEditVM.obj_ClientCustomer.NoJointCk == true)
                    {
                        objADJobOriginal.NoJointCk = objCustomerEditVM.obj_ClientCustomer.NoJointCk;
                    }
                    else
                    {
                        objADJobOriginal.NoJointCk = false;
                    }

                    if (objCustomerEditVM.obj_ClientCustomer.NoNTO == true)
                    {
                        objADJobOriginal.NoNTO = objCustomerEditVM.obj_ClientCustomer.NoNTO;
                    }
                    else
                    {
                        objADJobOriginal.NoNTO = false;
                    }

                    AutoMapper.Mapper.CreateMap<ADClientCustomer, ClientCustomer>();
                    ClientCustomer myentity;
                    myentity = AutoMapper.Mapper.Map<ClientCustomer>(objADJobOriginal);
                    ITable tblI = myentity;

                    var myacc = new Job();

                    ITable IJob = myacc;
                    DBO.Provider.Read(objCustomerEditVM.JobId.ToString(), ref IJob);
                    IJob = myacc;

                    var myLegalParties = new JobLegalParties();

                    ITable ILegalParties = myLegalParties;
                    DBO.Provider.Read(myacc.LenderId.ToString(), ref ILegalParties);
                    myLegalParties.Id = myacc.LenderId;
                    myLegalParties.JobId = Convert.ToInt32(objCustomerEditVM.JobId);
                    myLegalParties.IsPrimary = false;
                    myLegalParties.TypeCode = "CU";
                    ILegalParties = myLegalParties;


                    if (CustId < 1 && myacc.CustId > 1)
                    {
                        objCustomerEditVM.IsPrimary = false;
                    }

                    

                    if (CustId < 1)
                    {
                        LogCustChanges(myacc.CustId, myentity, false, objCustomerEditVM.JobId);
                        DBO.Provider.Create(ref tblI);
                    }
                     else 
                    {
                    myentity.CustId = CustId;
                        LogCustChanges(myacc.CustId, myentity, true, objCustomerEditVM.JobId);
                        DBO.Provider.Update(ref tblI);
                    }

                    if (objCustomerEditVM.IsPrimary == true)
                    {
                        objCustomerEditVM.obj_JobEdit = ADJob.ReadJob(Convert.ToInt32(objCustomerEditVM.JobId));
                        objCustomerEditVM.obj_JobEdit.CustId = myentity.CustId;


                        DBO.Provider.Update(ref IJob);
                    }

                    else
                    {
                    
                    myLegalParties.SecondaryId = myentity.CustId;
                        if(myLegalParties.Id > 0)
                        {
                            DBO.Provider.Update(ref ILegalParties);
                        }
                    
                        else
                        {
                            DBO.Provider.Create(ref ILegalParties);
   
                        }
                   
                    }

                        //DBO.Provider.Update(ref tblI);

                        return 4;
                
              
            }

            catch (Exception ex)
            {
                return 0;
            }
        }

        public static void LogCustChanges(int CustId, ClientCustomer myentity,bool IsCreate,string JobId)
        {
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

            CustomerEditVM objCustomerEditVM = new CustomerEditVM();
            ADClientCustomer objADClientCustomer = ADClientCustomer.ReadClientCustomer(myentity.ClientId);
            var mysproc = new CRF.BLL.CRFDB.SPROCS.uspbo_Jobs_UpdateChangeLog();

            mysproc.JobId = Convert.ToInt32(objCustomerEditVM.JobId);
            mysproc.UserId = user.Id;
            mysproc.UserName = user.UserName;
            mysproc.CustId = myentity.CustId;

            mysproc.ChangeType = 1;

            if (myentity.CustId > 0)
            {
              
                mysproc.ChangeFrom = myentity.ClientCustomer.Trim() + "~";
                mysproc.ChangeFrom += myentity.AddressLine1.Trim() + "~";
                mysproc.ChangeFrom += myentity.AddressLine2.Trim() + "~";
                mysproc.ChangeFrom += myentity.City.Trim() + "~";
                mysproc.ChangeFrom += myentity.State.Trim() + "~";
                mysproc.ChangeFrom += myentity.PostalCode.Trim() + "~";
                mysproc.ChangeFrom += myentity.Email.Trim() + "~";
                mysproc.ChangeFrom += "NOSEND:(" + myentity.CustNoSend + ")" + "~";
                mysproc.ChangeFrom += "ALLSEND:(" + myentity.CustAllSend + ")" + "~";
                mysproc.ChangeFrom += "NOCALL:(" + myentity.NoCallCust + ")" + "~";
                 mysproc.ChangeFrom += "NONTO:(" + myentity.NoNTO + ")" +"~"; ;

            }

            mysproc.ChangeTo = myentity.ClientCustomer.Trim() +"~";
            mysproc.ChangeTo += myentity.AddressLine1.Trim() +"~";
            mysproc.ChangeTo += myentity.AddressLine2.Trim() + "~";
            mysproc.ChangeTo += myentity.City.Trim() + "~";
            mysproc.ChangeTo += myentity.State.Trim() + "~";
            mysproc.ChangeTo += myentity.PostalCode.Trim() + "~";
            mysproc.ChangeTo += myentity.Email.Trim() + "~";
            mysproc.ChangeTo += "NOSEND:(" + myentity.CustNoSend + ")" + "~";
            mysproc.ChangeTo += "ALLSEND:(" + myentity.CustAllSend + ")" + "~";
            mysproc.ChangeTo += "NOCALL:(" + myentity.NoCallCust + ")" + "~";
            mysproc.ChangeTo += "NONTO:(" + myentity.NoNTO + ")" + "~";

            if(mysproc.ChangeFrom != mysproc.ChangeTo)
            {
                DBO.Provider.ExecNonQuery(mysproc);
            }

        }

        public static int DeleteCustomer(CustomerEditVM objCustomerEditVM, int CustId)
        {
            try
            {
                var myacc = new Job();

                ITable IJob = myacc;
                DBO.Provider.Read(objCustomerEditVM.JobId, ref IJob);
                myacc.CustId = 0;
                IJob = myacc;
                //DBO.Provider.Update(ref IJob);
                // ADJobLegalParties obj_ADJobLegalParties = ADJobLegalParties.ReadJobLegalParties(objCustomerEditVM.obj_JobEdit.LenderId);

                var myLegalParties = new JobLegalParties();

                ITable ILegalParties = myLegalParties;
                DBO.Provider.Read(myacc.LenderId.ToString(), ref ILegalParties);
               
                ILegalParties = myLegalParties;

                if(myLegalParties.Id > 0)
                {
                  
                    DBO.Provider.Delete(ref ILegalParties);
                }

                if(objCustomerEditVM.IsPrimary == true)
                {

                    //var myacc = new Job();

                    //ITable IJob = myacc;
                    //DBO.Provider.Read(objCustomerEditVM.obj_JobEdit.Id.ToString(), ref IJob);
                    myacc.CustId = 0;
                    IJob = myacc;
                    DBO.Provider.Update(ref IJob);

                }
                return 1;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static ADClientCustomer ReadClientCustomerByClientId(int ClientId)
        {
            ClientCustomer myentity = new ClientCustomer();
            ITable myentityItable = myentity;
            DBO.Provider.Read(ClientId.ToString(), ref myentityItable);
            myentity = (ClientCustomer)myentityItable;
            AutoMapper.Mapper.CreateMap<ClientCustomer, ADClientCustomer>();
            return (AutoMapper.Mapper.Map<ADClientCustomer>(myentity));
        }
        public static ClientVM EmailPageSetup(string TypeCode, int JobId, ClientVM OBJClientVM)
        {
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            
            JobVM  ObjJobVM = JobInfo.LoadJobInfo(JobId.ToString());
            string myreport;
            OBJClientVM = PageSetup(TypeCode, JobId);
            if (TypeCode == "CU")
            {
                if(ObjJobVM.ObjvwJobList.JobState == "TX")
                {
                    myreport = "FaxCustAutoTX";
                }
                else
                {
                    myreport = "FaxCustAuto";
                }

                if (ObjJobVM.jobdt != null && ObjJobVM.jobdt.Rows.Count <= 0)
                {
                    ObjJobVM.jobdt = null;
                }
                OBJClientVM.To = ObjJobVM.obj_ClientCustomer.Email;

                OBJClientVM.path = Reporting.PrintC1Report(user, "LIENREPORTS.xml", myreport, ObjJobVM.jobdt.DefaultView, PrintMode.PrintToPDF, "", "", "");
                //OBJClientVM.path = "";

            }
            else if (TypeCode == "GC")
            {
                if (ObjJobVM.ObjvwJobList.JobState == "TX")
                {
                    myreport = "FaxGenAutoTX";
                }
                else
                {
                    myreport = "FaxGenAuto";
                }
                OBJClientVM.To = ObjJobVM.objADClientGeneralContractor.Email;
                OBJClientVM.path = Reporting.PrintC1Report(user, "LIENREPORTS.xml", myreport, ObjJobVM.jobdt.DefaultView, PrintMode.PrintToPDF, "", "", "");


            }
            else
            {
                string sql = "SELECT * FROM vwLienReportsInfo With (Nolock) WHERE JobId =" + JobId;

                var MYVIEW = DBO.Provider.GetTableView(sql);
                OBJClientVM.path = Reporting.PrintC1Report(user, "LIENREPORTS.xml", "JobInfo", MYVIEW, PrintMode.PrintToPDF, "", "", "");
                //Dim s As String = BLL.COMMON.Reporting.PrintC1Report(CRFView.GetCurrentUser, "LIENREPORTS.xml", myreport, MYVIEW, BLL.COMMON.PrintMode.PrintToPDF, "", "", "")
                var ClientLienInfo = ObjJobVM.dtClientInfo;
                if (ClientLienInfo.Rows.Count > 0)
                {
                    foreach (DataRow dr in ClientLienInfo.Rows)
                    {
                        OBJClientVM.To = dr["Email"].ToString();
                    }
                }

            }

            
            return OBJClientVM;
        }

        public static ClientVM PageSetup(string STypeCode, int JobId)
        {
            
            string sBody = "";
            string s = "";
            JobVM ObjJobVM;
            ObjJobVM = JobInfo.LoadJobInfo(JobId.ToString());
            var Job = ObjJobVM.jobdt;
            foreach (DataRow dr in ObjJobVM.jobdt.Rows)
            {
                // ObjJobVM.
                if (STypeCode == "CU")
                {
                    s = "Job Information Request: " + dr["ClientName"] + ", " + dr["JobName"] + ", CRF#: " + dr["JobId"] + ", JOB#: " + dr["JobNum"] + ", PO#: " + dr["PONum"];
                }
                else if (STypeCode == "GC")
                {
                    s = "Job Information Request:" + dr["ClientName"] + "," + dr["JobName"] + ", CRF#: " + dr["JobId"] + ", JOB#: " + dr["JobNum"] + ", SUBCONTRACTOR: " + dr["CustName"] + ", PO#:" + dr["PONum"];
                }
                else
                {
                    s = dr["ClientCode"] + "," + " " + dr["JobName"] + ", CRF#: " + dr["JobId"] + ", JOB#: " + dr["JobNum"] + ", CUSTNAME: " + dr["CustName"] + ", CUSTREF#: " + dr["CustRefNum"] + ", PO#: " + dr["PONum"] + ", RA#: " + dr["RANum"];



                }
                sBody =  dr["ClientName"] + " has supplied Material, Labor or Equipment to " + dr["CustName"] + " for the project listed below. We are " + "\n";
                sBody += "reaching out to you because they have contracted with us to make sure their records for the job listed " + "\n";
                sBody += "below are accurate and complete.";
                sBody += "\n\n";
                sBody += "Job Name: " + dr["JobName"] + "\n";
                sBody += "Job Address: " + dr["JobAdd1"] + dr["JobAdd2"] + ", " + dr["JobCity"] + ", " + dr["JobState"] + dr["JobZip"];
                sBody += "\n\n";
                sBody += "If you have a job sheet for this project, please send it to us. If not, please confirm the job address, " + "\n";
                sBody += "provide us with the name and address of the Property Owner and the Prime Contractor. Please provide " + "\n";
                sBody += "Surety if there is a Payment Bond or Lender, if applicable, for this project.";
                sBody += "\n\n";
                string EmpPhNum = dr["EmpPhNum"].ToString();
                sBody += "If you have any questions you can call us at " + "(" + EmpPhNum.Substring(0,3) + ")" + " " + EmpPhNum.Substring(3, 3) + "-" + EmpPhNum.Substring(6, 4)  + ", or you can simply reply to this email with " + "\n";
                //sBody += "If you have any questions you can call us at " + "(" + dr["EmpPhNum"] + ")" + ", or you can simply reply to this email with " + "\n";
                sBody += "the above information.";
                sBody += "\n\n";
                sBody += "Thank you";




            }
            string Subject = s;



            ClientVM clientVM = new ClientVM();
            clientVM.Subject = s;
            clientVM.Message = sBody;
            clientVM.ListaDJobAttachments = ADJobAttachments.GetJobAttachmentsByJobId(JobId.ToString());





            return clientVM;
        }

        public static ClientVM EmailPageSetup(CRF.BLL.Users.CurrentUser user, DataTable dtClientInfo, int JobId)
        {
            ClientVM clientVM = new ClientVM();
            try
            {

                clientVM = PageSetup(JobId);
                if (dtClientInfo != null)
                {
                    clientVM.AttachmentPath = Reporting.PrintC1Report(user, "LIENREPORTS.xml", "JobInfo", dtClientInfo.DefaultView, PrintMode.PrintToPDF, "", "", "");

                }
                else
                {
                    //DataView myview;
                    //clientVM.AttachmentPath = Reporting.PrintC1Report(user, "LIENREPORTS.xml", "JobInfo", myview, PrintMode.PrintToPDF, "", "", "");

                }

            }
            catch (Exception ex)
            {

            }
            return clientVM;
        }
        public static ClientVM PageSetup(int JobId)
        {
            ClientVM clientVM = new ClientVM();
            string STypeCode = "";
            string sBody = "";
            string s = "";
            JobVM ObjJobVM;
            ObjJobVM = JobInfo.LoadJobInfo(JobId.ToString());
            var Job = ObjJobVM.jobdt;
            var ClientLienInfo = ObjJobVM.dtClientInfo;
            if (ClientLienInfo != null)
            {
                foreach (DataRow dr in ClientLienInfo.Rows)
                {
                    clientVM.To = dr["Email"].ToString();
                }
            }
            foreach (DataRow dr in ObjJobVM.jobdt.Rows)
            {
                // ObjJobVM.
                if (STypeCode == "CU")
                {
                    s = "Job Information Request:  " + dr["JobName"] + ", CRF#: " + dr["JobId"] + ", JOB#: " + dr["JobNum"] + ", PO#: " + dr["PONum"];
                }
                else if (STypeCode == "GC")
                {
                    s = "Job Information Request:" + dr["ClientName"] + dr["JobName"] + ", CRF#: " + dr["JobId"] + ", JOB#: " + dr["JobNum"] + ", SUBCONTRACTOR: " + dr["CustName"] + ", PO#:" + dr["PONum"];
                }
                else
                {
                    s = dr["ClientCode"] + "," + dr["JobName"] + ", CRF#:" + dr["JobId"] + ", JOB#: " + dr["JobNum"] + ", CUSTNAME: " + dr["CustName"] + ", CUSTREF#: " + dr["CustRefNum"] + ", PO#: " + dr["PONum"] + ", RA#:" + dr["RANum"];

                }
                sBody = dr["ClientName"] + " has supplied Material, Labor or Equipment to " + dr["CustName"] + " for the project listed below. We are " + "<br />";
                sBody += "reaching out to you because they have contracted with us to make sure their records for the job listed " + "<br />";
                sBody += "below are accurate and complete." + "<br />";
                sBody += "<br />";
                sBody += "Job Name: " + dr["JobName"] + "<br />";
                sBody += "Job Address: " + dr["JobAdd1"] + dr["JobAdd2"] + ", " + dr["JobCity"] + ", " + dr["JobState"] + dr["JobZip"] + "<br />";
                sBody += "<br />";
                sBody += "If you have a job sheet for this project, please send it to us. If not, please confirm the job address, " + "<br />";
                sBody += "provide us with the name and address of the Property Owner and the Prime Contractor. Please provide " + "<br />";
                sBody += "Surety if there is a Payment Bond or Lender, if applicable, for this project." + "<br />";
                sBody += "<br />";
                sBody += "If you have any questions you can call us at " + dr["EmpPhNum"] + ", or you can simply reply to this email with " + "<br />";
                sBody += "the above information." + "<br />";
                sBody += "<br />";
                sBody += "Thank you";


            }
            string Subject = s;


            clientVM.Subject = s;
            clientVM.Message = sBody;
            clientVM.ListaDJobAttachments = ADJobAttachments.GetJobAttachmentsByJobId(JobId.ToString());



            return clientVM;
        }
    }

}
