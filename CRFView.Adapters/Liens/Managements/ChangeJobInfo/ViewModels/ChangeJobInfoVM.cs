﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Liens.Managements.ChangeJobInfo.ViewModels
{
    public class ChangeJobInfoVM
    {
        public ChangeJobInfoVM()
        {
            objvwJobsList = new ADvwJobsList();
        }

        public ADvwJobsList objvwJobsList { get; set; }
        public string JobId { get; set; }
        public string Message { get; set; }
        public  ADStateInfo objStateInfo { get; set; }
        public ADJob objADJob { get; set; }
        public ADClient objADClient { get; set; }

        public bool chkGreencard { get; set; }
        public bool chkJointCk { get; set; }
        public bool chkIsJobview { get; set; }
        public bool chkDeleteTODesk { get; set; }
        public bool myPrivateJob { get; set; }
        public bool myResidentialBox { get; set; }
        public bool myPublicBox { get; set; }
        public bool chkStartDate { get; set; }
        public bool chkBranchNum { get; set; }
        public string txtBranchNum { get; set; }
        public bool chkDeleteVerifiedDate { get; set; }
        public bool chkDeleteNoticeHistory { get; set; }

        public DateTime DateTimePicker1 { get; set; }
        public DateTime DateTimePicker2 { get; set; }
    }
}
