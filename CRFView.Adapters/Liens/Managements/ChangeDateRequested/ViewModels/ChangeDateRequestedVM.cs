﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Liens.Managements.ChangeDateRequested.ViewModels
{
    public class ChangeDateRequestedVM
    {
        public ChangeDateRequestedVM()
        {
            ObjChangeDateRequested = new ADvwJobPrelimRequests();
          
           ChangeDateRequestedList = new List<ADvwJobPrelimRequests>();

        }

        public ChangeDateRequestedVM(string JobId)
        {
            ObjChangeDateRequested = new ADvwJobPrelimRequests();
        }
        public String JobId { get; set; }
        public DateTime DateRequested { get; set; }
        public DateTime ChangeTo { get; set; }
        public bool IsProcessed { get; set; }
       // public bool PrivateJob { get; set; }

        public bool PublicJob { get; set; }
        public System.DateTime DateTimePicker1 { get; set; }
        public System.DateTime DateTimePicker2 { get; set; }

        public DataTable dtChangeDateRequestedList { get; set; }
       
        public List<ADvwJobPrelimRequests> ChangeDateRequestedList { get; set; }
       
        public ADDebtAccountCreditReport ObjDebtAccountIndividualCreditReport { get; set; }

        public ADvwJobPrelimRequests ObjChangeDateRequested { get; set; }
      

        public ADvwJobPrelimRequests objvwJobPrelimRequests { get; set; }
      
    }

}
