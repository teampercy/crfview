﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRFView.Adapters.Liens.Managements.ChangeDateRequested.ViewModels;
using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using Ionic.Zip;
using System.IO;
using CRF.BLL.CRFDB.VIEWS;
using CRF.BLL.CRFView;
using System.Web.UI.WebControls;
using AutoMapper.Internal;
using CRFView.Adapters.Liens.DayEnds.PublicException.ViewModels;


namespace CRFView.Adapters
{
    public class ADvwJobPrelimRequests
    {

       
        #region Properties
        public String JobId { get; set; }
        public System.DateTime DateRequested { get; set; }
        public String ClientCode { get; set; }
        public String JobName { get; set; }
        public String JobNum { get; set; }
        public System.DateTime DateAssigned { get; set; }
        public decimal EstBalance { get; set; }
        public String StatusCode { get; set; }
        public String DeskNum { get; set; }
        public String ClientName { get; set; }
        public bool IsProcessed { get; set; }
        public String CustName { get; set; }
        public String GCName { get; set; }
        public String OwnerName { get; set; }
        public System.DateTime StartDate { get; set; }
        public String LenderName { get; set; }
        public String JobAdd1 { get; set; }
        public String JobState { get; set; }
        public String JobCity { get; set; }
        public string BranchNumber { get; set; }
        public String JobZip { get; set; }
        public System.DateTime VerifiedDate { get; set; }

        static TableView myview;
        // dataGridViewCellStyle m_SelectedStyle = new DataGridViewCellStyle();
        static int m_SelectedRow = 0;
        #endregion




        public static DataTable GetChangeDateRequestedDt(ChangeDateRequestedVM objChangeDateRequestedVM)
        {
            try
            {
                var sp_obj = new uspbo_LiensDayEnd_GetJobNoticePrelimRequest();

                
                 sp_obj.FromDate = objChangeDateRequestedVM.DateTimePicker1.ToShortDateString();

               

                if (!string.IsNullOrEmpty(objChangeDateRequestedVM.JobId))
                {
                    sp_obj.JobId = Convert.ToInt32(objChangeDateRequestedVM.JobId);
                }
                else
                {
                    sp_obj.JobId = 0;
                }

                //DBO.GetTableView 
                 myview = DBO.Provider.GetTableView(sp_obj);
                DataTable dt = myview.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        public static List<ADvwJobPrelimRequests> GetChangeDateRequestedList(DataTable dt)
        {
            List<ADvwJobPrelimRequests> List = (from DataRow dr in dt.Rows
                                                select new ADvwJobPrelimRequests()

                                                {
                                                    JobId = ((dr["JobId"] != DBNull.Value ? dr["JobId"].ToString() : "")),
                                                    DateRequested = ((dr["DateRequested"] != DBNull.Value ? Convert.ToDateTime(dr["DateRequested"]) : DateTime.MinValue)),
                                                    //ChangeTo = ((dr["ChangeTo"] != DBNull.Value ? Convert.ToDateTime(dr["ChangeTo"]) : DateTime.MinValue)),
                                                    ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                                    JobName = ((dr["JobName"] != DBNull.Value ? dr["JobName"].ToString() : "")),
                                                    JobNum = ((dr["JobNum"] != DBNull.Value ? dr["JobNum"].ToString() : "")),
                                                    DateAssigned = ((dr["DateAssigned"] != DBNull.Value ? Convert.ToDateTime(dr["DateAssigned"]) : DateTime.MinValue)),
                                                    EstBalance = ((dr["EstBalance"] != DBNull.Value ? Convert.ToDecimal(dr["EstBalance"]) : 0)),
                                                    StatusCode = ((dr["StatusCode"] != DBNull.Value ? dr["StatusCode"].ToString() : "")),
                                                    DeskNum = ((dr["DeskNum"] != DBNull.Value ? dr["DeskNum"].ToString() : "")),
                                                    ClientName = ((dr["ClientName"] != DBNull.Value ? dr["ClientName"].ToString() : "")),
                                                    IsProcessed = ((dr["IsProcessed"] != DBNull.Value ? Convert.ToBoolean(dr["IsProcessed"]) : false)),

                                                }).ToList();
           
            return List;
        }


       
         public static DataTable LoadInfo(string JobId)
        {
            try
            {
                if (myview.Count < 1)
                {

                }
                if (myview.RowIndex <= 0)
                {
                    myview.MoveFirst();
                }

                if (myview.RowIndex >= myview.Count)
                {
                    myview.MoveLast();
                }
                string sql = "Select * from vwJobInfo where JobId = " + JobId;
                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }
        public static int UpdateChangeDateRequested_Action(ChangeDateRequestedVM objChangeDateRequestedVM)
        {

            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                var sp_obj = new uspbo_LiensUtility_ChangePrelimDateRequested();

                sp_obj.DateRequested = objChangeDateRequestedVM.DateTimePicker2.ToShortDateString();
                sp_obj.JobId = Convert.ToInt32(objChangeDateRequestedVM.JobId);

                DBO.Provider.ExecNonQuery(sp_obj);
                return 1;
            }

            catch (Exception ex)
            {
                return 0;
            }
             }

            //public static int UpdatePublicException_Action(ChangeDateRequestedVM objChangeDateRequestedVM)
            //{

            //    try
            //    {
            //        string sOldType = "";

            //        CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            //        Job myentity = new Job();
            //        ITable myentityItable = myentity;

            //        TableView myview = DBO.Provider.GetTableView(myentityItable.ToString());
            //        // DBO.Provider.Read(objChangeDateRequestedVM.JobId, ref myentityItable);
            //        DBO.Provider.Read(myview.get_RowItem("JobId").ToString(), ref myentityItable);
            //        if (myentity.PrivateJob == true)
            //        {
            //            sOldType = "Private Job";
            //        }
            //        if (myentity.ResidentialBox == true)
            //        {
            //            sOldType = "Residential Job";
            //        }

            //        LogChanges(myentity.Id, sOldType);
            //        myentity.PublicJob = true;
            //        if (myentity.SendAsIsBox == true && myentity.PlacementSource == "WEB")
            //        {
            //            myentity.NCInfo = true;
            //        }

            //        DBO.Provider.Update(ref myentityItable);
            //        DBO.Provider.Read(myview.get_RowItem("JobId").ToString(), ref myentityItable);


            //        //  myview.get_RowItem("Id") = myentity.PublicJob;

            //        //    ListGrid.Rows(myview.RowIndex).Cells(0).Value = myjob.PublicJob;
            //        //ListGrid.Rows(myview.RowIndex).Cells(4).Value = myjob.NCInfo;


            //        myview.MoveNext();

            //        return 1;
            //    }

            //    catch (Exception ex)
            //    {
            //        return 0;
            //    }

            //}

            //public static int LogChanges(int aJobId, string sOldType)
            //{
            //    var mysproc = new uspbo_Jobs_UpdateChangeLog();
            //    mysproc.JobId = aJobId;
            //    CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            //    mysproc.UserId = user.Id;
            //    mysproc.UserName = user.UserName.Substring(0, 50).ToUpper();
            //    mysproc.ChangeType = 0;
            //    mysproc.ChangeFrom = sOldType.Trim() + " ";
            //    mysproc.ChangeTo = "Public Job";
            //    DBO.Provider.ExecNonQuery(mysproc);
            //    return 1;
            //}



        }
}
