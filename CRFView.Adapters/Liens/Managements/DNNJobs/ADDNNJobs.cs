﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.Providers;
using CRFView.Adapters.Liens.Managements.DNNJobs.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADDNNJobs
    {
        #region Properties

        #endregion


        public static int GetJobView_DNNJobs(DNNJobsVM objDNNJobsVM)
        {
            try
            {
                var result = "";
                var mysproc = new uspbo_JobView_DNNJobs();
         
                DBO.Provider.ExecNonQuery(mysproc);
                result = "Processed Succesfully";

                return 1;
  
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

    }
}
