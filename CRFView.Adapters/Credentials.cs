﻿using System.ComponentModel.DataAnnotations;
using CRF.BLL.Users;
using System.Web;

namespace CRFView.Adapters
{
    public class Credentials 
    {
        #region Properties
        [Required(ErrorMessage = "UserName is required")]
            public string UserName { set; get; }

            [Required(ErrorMessage = "Password is Required")]
            public string Password { set; get; }
        #endregion

        #region Functions
       
        UserLogin objUserLogin=new UserLogin();
        CurrentUser objCurrentUser = new CurrentUser();

        public bool GetLoginDetails()
        {
            objUserLogin.SprocParams.LoginCode = UserName;
            objUserLogin.SprocParams.Password = Password;

            if (objUserLogin.InternalUserLogin())
            {
                AutoMapper.Mapper.CreateMap<UserLogin, ADInternalUser>();
                HttpContext.Current.Session["UserLogin"] = AutoMapper.Mapper.Map<ADInternalUser>(objUserLogin);
              
                return true;
            }
            return false;
        }



        #endregion
    }
}