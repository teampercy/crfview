﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;


namespace CRFView.Adapters
{
    public class ADJobViewDesks
    {
        #region Properties
        public int DeskId { get; set; }
        public string DeskNum { get; set; }
        public string DeskName { get; set; }
        public bool IsManager { get; set; }
        #endregion

        #region CRUD Operations

        JobViewDesks myentity;
        //List of JobViewDesks
        public List<ADJobViewDesks> ListJobViewDesks()
        {
          
            myentity = new JobViewDesks();
            TableView EntityList;
            try
            {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADJobViewDesks> CAList = (from DataRow dr in dt.Rows
                                             select new ADJobViewDesks()
                                             {
                                                 DeskId = Convert.ToInt16(dr["DeskId"]),
                                                 DeskNum = Convert.ToString(dr["DeskNum"]),
                                                 DeskName = Convert.ToString(dr["DeskName"])
                                             }).ToList();

                return CAList;
            }
            catch (Exception ex)
            {
                return (new List<ADJobViewDesks>());
            }
        }

        //Read Details from database
        public ADJobViewDesks ReadJobViewDesks(int id)
        {
            JobViewDesks myentity = new JobViewDesks();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (JobViewDesks)myentityItable;
            AutoMapper.Mapper.CreateMap<JobViewDesks, ADJobViewDesks>();
            return (AutoMapper.Mapper.Map<ADJobViewDesks>(myentity));
        }

        //Save(Create or Update) JobViewDesks Details
        public bool SaveJobViewDesks(ADJobViewDesks updatedJobViewDesks)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADJobViewDesks, JobViewDesks>();
                ITable tblJobViewDesks;
                if (updatedJobViewDesks.DeskId != 0)
                {
                    ADJobViewDesks loadJobViewDesks = ReadJobViewDesks(updatedJobViewDesks.DeskId);
                    loadJobViewDesks.DeskNum = updatedJobViewDesks.DeskNum;
                    loadJobViewDesks.DeskName = updatedJobViewDesks.DeskName;
                    JobViewDesks objJobViewDesks = AutoMapper.Mapper.Map<JobViewDesks>(loadJobViewDesks);
                     tblJobViewDesks = objJobViewDesks;
                    DBO.Provider.Update(ref tblJobViewDesks);
                }
                else
                {
                    JobViewDesks objJobViewDesks = AutoMapper.Mapper.Map<JobViewDesks>(updatedJobViewDesks);
                    tblJobViewDesks = objJobViewDesks;
                    DBO.Provider.Create(ref tblJobViewDesks);
                }
            }
            catch(Exception ex)
            {
                return false;
            }
            return true;
        }
        #endregion
    }
}