﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADClientEDIReference
    {
        #region Properties
        public int Id { get; set; }
        public int ClientInterfaceId { get; set; }
        public string InputField { get; set; }
        public int ClientId { get; set; }
        //Additional property required in list from client table
        public string ClientCode { get; set; }
        public string ClientName { get; set; }
        public string InterfaceName { get; set; }
        public List<CommonBindList> ClientList { get; set; }
        public List<CommonBindList> ClientInterfaceList { get; set; }
        #endregion

        #region CRUD Operations
        //List of ClientInterface
        public List<ADClientEDIReference> ListClientEDIReference()
        {
            TableView EntityList;
            try {
                CRF.BLL.Configuration.ClientEDIReference.IClientEDIReferenceProvider objIClientEDIReference = new CRF.BLL.Configuration.ClientEDIReference.SQLClientEDIReferenceProvider();
                EntityList = objIClientEDIReference.GetClientEDIReferenceList();
                DataTable dt = EntityList.ToTable();
                List<ADClientEDIReference> CIList = (from DataRow dr in dt.Rows
                          select new ADClientEDIReference()
                          {
                              Id= Convert.ToInt16(dr["Id"]),
                              InterfaceName = Convert.ToString(dr["InterfaceName"]),
                             InputField = Convert.ToString(dr["InputField"]),
                              ClientCode = Convert.ToString(dr["ClientCode"]),
                              ClientName = Convert.ToString(dr["ClientName"])                            

                          }).ToList();

                return CIList;
            }
            catch(Exception ex)
            {
                return (new List<ADClientEDIReference>());
            }
        }

        //Read Details from database
        public ADClientEDIReference ReadClientEDIReference(int id)
        {
            ClientInterfaceReference myentity = new ClientInterfaceReference();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (ClientInterfaceReference)myentityItable;
            AutoMapper.Mapper.CreateMap<ClientInterfaceReference, ADClientEDIReference>();
            return (AutoMapper.Mapper.Map<ADClientEDIReference>(myentity));
        }

        ////Save(Create or Update) ClientInterface Details
        public void SaveClientEDIReference(ADClientEDIReference updatedClientEDIReference)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADClientEDIReference, ClientInterfaceReference>();
                ITable tblClientEDIReference;
                if (updatedClientEDIReference.Id != 0)
                {
                    ADClientEDIReference loadClientEDIReference = ReadClientEDIReference(updatedClientEDIReference.Id);
                    loadClientEDIReference.ClientId = updatedClientEDIReference.ClientId;
                    loadClientEDIReference.ClientInterfaceId = updatedClientEDIReference.ClientInterfaceId;
                    loadClientEDIReference.InputField = updatedClientEDIReference.InputField;
                    ClientInterfaceReference objClientReference = AutoMapper.Mapper.Map<ClientInterfaceReference>(loadClientEDIReference);
                   
                    tblClientEDIReference = (ITable)objClientReference;
                    DBO.Provider.Update(ref tblClientEDIReference);
                }
                else
                {
                    ClientInterfaceReference objClientReference = AutoMapper.Mapper.Map<ClientInterfaceReference>(updatedClientEDIReference);
                    tblClientEDIReference = (ITable)objClientReference;
                    DBO.Provider.Create(ref tblClientEDIReference);

                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

    
    }
}