﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADClientReportsDefinations
    {
        #region Properties
        public int PKID { get; set; }
        public string taskname { get; set; }
        public string title { get; set; }
        public string description { get; set; }
        public string emailto { get; set; }
        public int expiredays { get; set; }
        public string type { get; set; }
        public string assemblyfile { get; set; }
        public string parameters { get; set; }
        public DateTime lastrundate { get; set; }
        public DateTime nextrundate { get; set; }
        public string frequency { get; set; }
        public int number { get; set; }
        public int runfromhour { get; set; }
        public int runuptohour { get; set; }
        public bool isdisabled { get; set; }
        public string lastmessage { get; set; }
        public int status { get; set; }
        public DateTime starttime { get; set; }
        public DateTime endtime { get; set; }
        #endregion

        #region CRUD Operations
        //List of Report
        public List<ADClientReportsDefinations> ListClientReportsDefinations()
        {
            Report myentity = new Report();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADClientReportsDefinations> CRDList = (from DataRow dr in dt.Rows
                          select new ADClientReportsDefinations()
                          {
                              PKID = Convert.ToInt16(dr["PKID"]),
                              taskname = Convert.ToString(dr["taskname"]),
                              description = Convert.ToString(dr["description"]),
                              lastrundate= dr["lastrundate"] != DBNull.Value ? Convert.ToDateTime(dr["lastrundate"]) : DateTime.MinValue,
                              nextrundate =dr["nextrundate"] != DBNull.Value ? Convert.ToDateTime(dr["nextrundate"]) : DateTime.MinValue,
                              isdisabled = Convert.ToBoolean(dr["isdisabled"]),
                              lastmessage = Convert.ToString(dr["lastmessage"]),

                          }).ToList();

                return CRDList;
            }
            catch(Exception ex)
            {
                return (new List<ADClientReportsDefinations>());
            }
        }

        //Read Details from database
        public ADClientReportsDefinations ReadClientReportsDefinations(int id)
        {
            Report myentity = new Report();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (Report)myentityItable;
            AutoMapper.Mapper.CreateMap<Report, ADClientReportsDefinations>();
            return (AutoMapper.Mapper.Map<ADClientReportsDefinations>(myentity));
        }

        //Save(Create or Update) Report Details
        public void SaveClientReportsDefinations(ADClientReportsDefinations updatedClientReportDef)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADClientReportsDefinations, Report>();
                ITable tblClientReportDef;
                if (updatedClientReportDef.PKID != 0)
                {
                    ADClientReportsDefinations loadClientReportDef = ReadClientReportsDefinations(updatedClientReportDef.PKID);
                    loadClientReportDef.taskname = updatedClientReportDef.taskname;
                    loadClientReportDef.title = updatedClientReportDef.title;
                    loadClientReportDef.description = updatedClientReportDef.description;
                    loadClientReportDef.assemblyfile = updatedClientReportDef.assemblyfile;
                    loadClientReportDef.type = updatedClientReportDef.type;
                    loadClientReportDef.lastrundate = updatedClientReportDef.lastrundate;
                    loadClientReportDef.nextrundate = updatedClientReportDef.nextrundate;
                    loadClientReportDef.frequency = updatedClientReportDef.frequency;
                    loadClientReportDef.number = updatedClientReportDef.number;
                    loadClientReportDef.runfromhour = updatedClientReportDef.runfromhour;
                    loadClientReportDef.runuptohour = updatedClientReportDef.runuptohour;
                    loadClientReportDef.isdisabled = updatedClientReportDef.isdisabled;
                    Report objreport= AutoMapper.Mapper.Map<Report>(loadClientReportDef);
                    tblClientReportDef = objreport;
                    DBO.Provider.Update(ref tblClientReportDef);
                }
                else
                {
                    Report objreport = AutoMapper.Mapper.Map<Report>(updatedClientReportDef);
                    tblClientReportDef = objreport;
                    DBO.Provider.Create(ref tblClientReportDef);

                }
            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion
    }
}
