﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADReportCategory
    {
        #region Properties
        public int Id { get; set; }
        public string ReportCategory { get; set; }
        #endregion

        #region CRUD Operations
        //List of ReportCategories
        public List<ADReportCategory> ListReportCategories()
        {
            ReportCatagories myentity = new ReportCatagories();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADReportCategory> RFList = new List<ADReportCategory>();
                RFList = (from DataRow dr in dt.Rows
                          select new ADReportCategory()
                          {
                               Id = Convert.ToInt16(dr["Id"]),
                              ReportCategory = Convert.ToString(dr["ReportCategory"]),
                           
                          }).ToList();

                return RFList;
            }
            catch(Exception ex)
            {
                return (new List<ADReportCategory>());
            }
        }

        //Read Details from database
        public ADReportCategory ReadReportCategories(int id)
        {
            ReportCatagories myentity = new ReportCatagories();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (ReportCatagories)myentityItable;
            AutoMapper.Mapper.CreateMap<ReportCatagories, ADReportCategory>();
            return (AutoMapper.Mapper.Map<ADReportCategory>(myentity));
        }

        //Save(Create or Update) ReportCategories Details
        public void SaveReportCategories(ADReportCategory updatedReportCategories)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADReportCategory, ReportCatagories>();
                ITable tblReportCategories;
                if (updatedReportCategories.Id != 0)
                {
                    ADReportCategory loadReportCategories = ReadReportCategories(updatedReportCategories.Id);
                    loadReportCategories.ReportCategory = updatedReportCategories.ReportCategory;
                    ReportCatagories objReportcategory = AutoMapper.Mapper.Map<ReportCatagories>(loadReportCategories);
                    tblReportCategories = (ITable)objReportcategory;
                    DBO.Provider.Update(ref tblReportCategories);
                }
                else
                {
                    ReportCatagories objReportcategory = AutoMapper.Mapper.Map<ReportCatagories>(updatedReportCategories);
                    tblReportCategories = objReportcategory;
                    DBO.Provider.Create(ref tblReportCategories);

                }
            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion
    }
}