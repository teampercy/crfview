﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;


namespace CRFView.Adapters
{
    public class ADAssociations
    {
        #region Properties
        public int Id { get; set; }
        public string AssociationName { get; set; }
        #endregion

        #region CRUD Operations

        ClientAssociation myentity;
        //List of ClientAssociation
        public List<ADAssociations> ListClientAssociation()
        {
          
            myentity = new ClientAssociation();
            TableView EntityList;
            try
            {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADAssociations> CAList = (from DataRow dr in dt.Rows
                                             select new ADAssociations()
                                             {
                                                 Id = Convert.ToInt16(dr["Id"]),
                                                 AssociationName = Convert.ToString(dr["AssociationName"])

                                             }).ToList();

                return CAList;
            }
            catch (Exception ex)
            {
                return (new List<ADAssociations>());
            }
        }

        //Read Details from database
        public ADAssociations ReadClientAssociation(int id)
        {
            ClientAssociation myentity = new ClientAssociation();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (ClientAssociation)myentityItable;
            AutoMapper.Mapper.CreateMap<ClientAssociation, ADAssociations>();
            return (AutoMapper.Mapper.Map<ADAssociations>(myentity));
        }

        //Save(Create or Update) ClientAssociation Details
        public void SaveClientAssociation(ADAssociations updatedClientAssociation)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADAssociations, ClientAssociation>();
                ITable tblClientAssociation;
                if (updatedClientAssociation.Id != 0)
                {
                    ADAssociations loadClientAssociation = ReadClientAssociation(updatedClientAssociation.Id);
                    loadClientAssociation.AssociationName = updatedClientAssociation.AssociationName;
                    ClientAssociation objClientAssociation = AutoMapper.Mapper.Map<ClientAssociation>(loadClientAssociation);
                     tblClientAssociation = (ITable)objClientAssociation;
                    DBO.Provider.Update(ref tblClientAssociation);
                }
                else
                {
                    ClientAssociation objClientAssociation = AutoMapper.Mapper.Map<ClientAssociation>(updatedClientAssociation);
                    tblClientAssociation = (ITable)objClientAssociation;
                    DBO.Provider.Create(ref tblClientAssociation);
                }
            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion
    }
}