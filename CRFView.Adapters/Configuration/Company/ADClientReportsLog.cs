﻿using System;
using System.Collections.Generic;
using System.Linq;
using HDS.DAL.COMMON;
using System.Data;
using CRF.BLL.Configuration.ClientReportLog;

namespace CRFView.Adapters
{
    public class ADClientReportsLog
    {

        #region Properties
        public int PKID { get; set; }
        public int reportid { get; set; }
        public string batchid { get; set; }
        public string taskname { get; set; }
        public DateTime startdate { get; set; }
        public DateTime enddate { get; set; }
        public int status { get; set; }
        public string message { get; set; }
        public int clientid { get; set; }
        public string clientcode { get; set; }
        public string subject { get; set; }
        public string body { get; set; }
        public string keyvalues { get; set; }
        public DateTime dateviewed { get; set; }
        public int viewedbyuserid { get; set; }
        public bool isemailed { get; set; }
        public DateTime emaildate { get; set; }
        public int totitems { get; set; }
        public int lastitem { get; set; }
        public int altkeyid { get; set; }
        public int BranchClientId { get; set; }
        #endregion

        #region CRUD Operations
        //List of Report_History
        public List<ADClientReportsLog> ListClientReportLog(string FilterValue, string FilterDate)
        { 
            try
            {
                IClientReportLogProvider objIClientReportLog = new SQLClientReportLogProvider();
                TableView EntityList = objIClientReportLog.GetClientReportLogList(FilterValue, FilterDate);
                DataTable dt = EntityList.ToTable();
                List<ADClientReportsLog> RHList = (from DataRow dr in dt.Rows
                                                 select new ADClientReportsLog()
                                                 {
                                                     PKID = Convert.ToInt32(dr["PKID"]),
                                                     taskname = Convert.ToString(dr["taskname"]),
                                                     batchid = Convert.ToString(dr["batchid"]),
                                                     clientcode = Convert.ToString(dr["clientcode"]),
                                                     startdate = dr["startdate"] != DBNull.Value ? Convert.ToDateTime(dr["startdate"]) : DateTime.MinValue,
                                                     emaildate = dr["emaildate"] != DBNull.Value ? Convert.ToDateTime(dr["emaildate"]) : DateTime.MinValue,
                                                     dateviewed = dr["dateviewed"] != DBNull.Value ? Convert.ToDateTime(dr["dateviewed"]) : DateTime.MinValue,
                                                     status = Convert.ToInt32(dr["status"]),
                                                     message = Convert.ToString(dr["message"])
                                                 }).ToList();

                return RHList;
            }
            catch (Exception ex)
            {
                return (new List<ADClientReportsLog>());
            }
        }

        #endregion
    }
}