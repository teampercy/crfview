﻿using System;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.Providers;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADCompanyInfo
    {
        #region Properties
        public int AgencyId { get; set; }
        public string Agency { get; set; }
        public string AgencyName { get; set; }
        public string AgencyAttention { get; set; }
        public string AgencyAddr1 { get; set; }
        public string AgencyAddr2 { get; set; }
        public string AgencyAddr3 { get; set; }
        public string AgencyPhoneNo { get; set; }
        public string AgencyEmail { get; set; }
        public string AgencyFax { get; set; }
        public int NextFileNumber { get; set; }
        public int MaxReviewDays { get; set; }
        public bool SupressInBound { get; set; }
        public string CBRInnovisNo { get; set; }
        public string CBREquifaxNo { get; set; }
        public string CBRExpirianNo { get; set; }
        public string CBRTransUnionNo { get; set; }
        public string CBRReporterName { get; set; }
        public string CBRReporterAddr { get; set; }
        public string CBRReporterPhone { get; set; }
        public byte[] AgencyLogo { get; set; }
        public string AgencyRptHeader { get; set; }
        public string AgencyRptFooter { get; set; }
        public int NBPrelimActionId { get; set; }
        public int NBVerifyOnlyActionId { get; set; }
        public int NBVerifiedAsIsActionId { get; set; }
        public int NBMechLienActionId { get; set; }
        public int NBStopNoticeActionId { get; set; }
        public int NBLienReleaseActionId { get; set; }
        public int NBBondClaimActionId { get; set; }
        public string SMTPMAILSERVER { get; set; }
        public string SUPPORTEMAIL { get; set; }
        public string CUSTOMERSERVICEEMAIL { get; set; }
        public string SALESEMAIL { get; set; }
        public string SMTPUSERID { get; set; }
        public string SMTPPASSWORD { get; set; }
        public int NBSendAsIsActionId { get; set; }
        public int NBVerifyOnlyTXActionId { get; set; }
        public int NBVerifiedAsIsTXActionId { get; set; }
        public byte[] AgencyClientLogo { get; set; }
        public int NBNOCActionId { get; set; }
        public int MiscInvoiceNum { get; set; }
        public int NBVerifyOWOnlyActionId { get; set; }
        public int NBVerifyOWOnlySendActionId { get; set; }
        public int NBVerifyOWOnlyTXActionId { get; set; }
        #endregion

        #region CRUD Operations

        //Read Details from database
        public ADCompanyInfo ReadCompanyInfo(int id)
        {
            Agency myentity = new Agency();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (Agency)myentityItable;
            AutoMapper.Mapper.CreateMap<Agency, ADCompanyInfo>();
            return (AutoMapper.Mapper.Map<ADCompanyInfo>(myentity));
        }

        //Read Details from database
        public static ADCompanyInfo ReadCompanyInfoById(int id)
        {
            Agency myentity = new Agency();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (Agency)myentityItable;
            AutoMapper.Mapper.CreateMap<Agency, ADCompanyInfo>();
            return (AutoMapper.Mapper.Map<ADCompanyInfo>(myentity));
        }

        //Save(Create or Update) Agency Details
        public void SaveCompanyInfo(ADCompanyInfo updatedAgency)
        {
            try
            {
                ADCompanyInfo loadAgency = ReadCompanyInfo(1);
                loadAgency.AgencyLogo = updatedAgency.AgencyLogo;
                loadAgency.AgencyClientLogo = updatedAgency.AgencyClientLogo;
                loadAgency.NBPrelimActionId = updatedAgency.NBPrelimActionId;
                loadAgency.NBVerifyOnlyActionId = updatedAgency.NBVerifyOnlyActionId;
                loadAgency.NBVerifyOnlyTXActionId = updatedAgency.NBVerifyOnlyTXActionId;
                loadAgency.NBVerifiedAsIsActionId = updatedAgency.NBVerifiedAsIsActionId;
                loadAgency.NBVerifiedAsIsTXActionId = updatedAgency.NBVerifiedAsIsTXActionId;
                loadAgency.NBMechLienActionId = updatedAgency.NBMechLienActionId;
                loadAgency.NBStopNoticeActionId = updatedAgency.NBStopNoticeActionId;
                loadAgency.NBLienReleaseActionId = updatedAgency.NBLienReleaseActionId;
                loadAgency.NBBondClaimActionId = updatedAgency.NBBondClaimActionId;
                loadAgency.NBNOCActionId = updatedAgency.NBNOCActionId;
                loadAgency.NBVerifyOWOnlyActionId = updatedAgency.NBVerifyOWOnlyActionId;
                loadAgency.NBVerifyOWOnlySendActionId = updatedAgency.NBVerifyOWOnlySendActionId;
                loadAgency.NBVerifyOWOnlyTXActionId = updatedAgency.NBVerifyOWOnlyTXActionId;

                AutoMapper.Mapper.CreateMap<ADCompanyInfo, Agency>();
                Agency objAgency = AutoMapper.Mapper.Map<Agency>(updatedAgency);
                ITable tblAgency = objAgency;
                DBO.Provider.Update(ref tblAgency);
            }
            catch (Exception ex)
            {

            }
        }
        
        #endregion
    }
}