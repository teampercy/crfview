﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADSchedulerTasks
    {
        #region properties

        public int PKID { get; set; }
        public string prefix { get; set; }
        public DateTime lastrundate { get; set; }
        public DateTime nextrundate { get; set; }
        public int dateinterval { get; set; }
        public int number { get; set; }
        public bool isdisabled { get; set; }
        public string internaluserlist { get; set; }
        public string clientuserlist { get; set; }
        public int lastresult { get; set; }
        public string lastmessage { get; set; }
        public int taskdefid { get; set; }
        public string parameters { get; set; }
        public int runfromhour { get; set; }
        public int runuptohour { get; set; }
        public bool isvisible { get; set; }
        public bool isweb { get; set; }
        public string description { get; set; }
        public string subject { get; set; }
        public string ccemail { get; set; }
        public string type { get; set; }
        public string assemblyfile { get; set; }
        public string taskname { get; set; }
        public List<CommonBindList> TaskNamesList { get; set; }
        public List<CommonBindList> InternalUsersList { get; set; }
        public List<CommonBindList> ClientUsersList { get; set; }
        public List<CommonBindList> SelectedInternalUsersList { get; set; }
        public List<CommonBindList> SelectedClientUsersList { get; set; }
        #endregion

        #region CRUD Operations


        //List of SchedulerTasks
        public List<ADSchedulerTasks> ListSchedulerTasks()
        { 
            TableView EntityList;
            try {
                Tasks_Task myentity = new Tasks_Task();
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADSchedulerTasks> STList =(from DataRow dr in dt.Rows
                          select new ADSchedulerTasks()
                          {
                              PKID = Convert.ToInt16(dr["PKID"]),
                              taskname = Convert.ToString(dr["taskname"]),
                               isdisabled= Convert.ToBoolean(dr["isdisabled"]),
                              lastrundate = ((dr["lastrundate"] != DBNull.Value ? Convert.ToDateTime(dr["lastrundate"]) : DateTime.MinValue)),
                              nextrundate = ((dr["nextrundate"] != DBNull.Value ? Convert.ToDateTime(dr["nextrundate"]) : DateTime.MinValue)),
                              lastmessage = Convert.ToString(dr["lastmessage"])

                          }).ToList();

                return STList;
            }
            catch(Exception ex)
            {
                return (new List<ADSchedulerTasks>());
            }
        }

        //Read Details from database
        public ADSchedulerTasks ReadSchedulerTasks(int id)
        {
            Tasks_Task myentity = new Tasks_Task();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (Tasks_Task)myentityItable;
            AutoMapper.Mapper.CreateMap<Tasks_Task, ADSchedulerTasks>();
            return (AutoMapper.Mapper.Map<ADSchedulerTasks>(myentity));
        }

        ////Save(Create or Update) Tasks_Task Details
        public void SaveSchedulerTasks(ADSchedulerTasks updatedSchedulerTasks)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADSchedulerTasks, Tasks_Task>();
                ITable  tblSchedulerTasks;
                if (updatedSchedulerTasks.PKID != 0)
                {
                    ADSchedulerTasks loadSchedulerTasks = ReadSchedulerTasks(updatedSchedulerTasks.PKID);
                    loadSchedulerTasks.taskname = updatedSchedulerTasks.taskname;
                    loadSchedulerTasks.taskdefid = updatedSchedulerTasks.taskdefid;
                    loadSchedulerTasks.lastrundate = updatedSchedulerTasks.lastrundate;
                    loadSchedulerTasks.nextrundate = updatedSchedulerTasks.nextrundate;
                    loadSchedulerTasks.dateinterval = updatedSchedulerTasks.dateinterval;
                    loadSchedulerTasks.runfromhour = updatedSchedulerTasks.runfromhour;
                    loadSchedulerTasks.runuptohour = updatedSchedulerTasks.runuptohour;
                    loadSchedulerTasks.isdisabled = updatedSchedulerTasks.isdisabled;
                    loadSchedulerTasks.number = updatedSchedulerTasks.number;
                    loadSchedulerTasks.description = updatedSchedulerTasks.description;
                    loadSchedulerTasks.prefix = updatedSchedulerTasks.prefix;
                    loadSchedulerTasks.subject = updatedSchedulerTasks.subject;
                    loadSchedulerTasks.internaluserlist = updatedSchedulerTasks.internaluserlist;
                    loadSchedulerTasks.clientuserlist = updatedSchedulerTasks.clientuserlist;
                    Tasks_Task objTasks_Task = AutoMapper.Mapper.Map<Tasks_Task>(loadSchedulerTasks);
                    tblSchedulerTasks = (ITable)objTasks_Task;
                    DBO.Provider.Update(ref tblSchedulerTasks);
                }
                else
                {
                    Tasks_Task objTasks_Task = AutoMapper.Mapper.Map<Tasks_Task>(updatedSchedulerTasks);
                    tblSchedulerTasks = (ITable)objTasks_Task;
                    DBO.Provider.Create(ref tblSchedulerTasks);

                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

       
    }
}