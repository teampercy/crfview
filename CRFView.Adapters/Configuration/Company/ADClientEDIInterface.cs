﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADClientEDIInterface
    {
        #region Properties
        public int Id { get; set; }
        public int ClientId { get; set; }
        public string InterfaceName { get; set; }
        public string InterfaceType { get; set; }
        public string InterfaceDescr { get; set; }
        public int ContractId { get; set; }
        public string EDIInfo { get; set; }
       public string ClientCode { get; set; }
        public List<CommonBindList> ClientList { get; set; }
        public List<CommonBindList> ClientContractList { get; set; }
        #endregion

        #region CRUD Operations
        //List of ClientInterface
        public List<ADClientEDIInterface> ListClientEDIInterface()
        {
            
            TableView EntityList;
            try {
                CRF.BLL.Configuration.ClientEDIInterFace.IClientEDIInterfaceProvider objIClientInterface = new CRF.BLL.Configuration.ClientEDIInterFace.SQLClientEDIInterfaceProvider();
                EntityList = objIClientInterface.GetClientEDIInterfaceList();
                DataTable dt = EntityList.ToTable();
                List<ADClientEDIInterface> CIList = (from DataRow dr in dt.Rows
                          select new ADClientEDIInterface()
                          {
                              Id = Convert.ToInt16(dr["Id"]),
                             ClientCode = Convert.ToString(dr["clientcode"]),
                              InterfaceType = Convert.ToString(dr["InterfaceType"]),
                              InterfaceName = Convert.ToString(dr["InterfaceName"]),
                              InterfaceDescr = Convert.ToString(dr["InterfaceDescr"])

                          }).ToList();

                return CIList;
            }
            catch(Exception ex)
            {
                return (new List<ADClientEDIInterface>());
            }
        }

        //Read Details from database
        public ADClientEDIInterface ReadClientEDIInterface(int id)
        {
            ClientInterface myentity = new ClientInterface();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (ClientInterface)myentityItable;
            AutoMapper.Mapper.CreateMap<ClientInterface, ADClientEDIInterface>();
            return (AutoMapper.Mapper.Map<ADClientEDIInterface>(myentity));
        }

        ////Save(Create or Update) ClientInterface Details
        public void SaveClientEDIInterface(ADClientEDIInterface updatedClientEDIInterface)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADClientEDIInterface, ClientInterface>();
                ITable tblClientEDIInterface;
                if (updatedClientEDIInterface.Id != 0)
                {
                    ADClientEDIInterface loadClientEDIInterface = ReadClientEDIInterface(updatedClientEDIInterface.Id);
                    loadClientEDIInterface.ClientId = updatedClientEDIInterface.ClientId;
                    loadClientEDIInterface.InterfaceType = updatedClientEDIInterface.InterfaceType;
                    loadClientEDIInterface.InterfaceName = updatedClientEDIInterface.InterfaceName;
                    loadClientEDIInterface.InterfaceDescr = updatedClientEDIInterface.InterfaceDescr;
                    loadClientEDIInterface.EDIInfo = updatedClientEDIInterface.EDIInfo;
                    loadClientEDIInterface.ContractId = updatedClientEDIInterface.ContractId;
                    ClientInterface objClientInterface = AutoMapper.Mapper.Map<ClientInterface>(loadClientEDIInterface);
                    tblClientEDIInterface = (ITable)objClientInterface;
                    DBO.Provider.Update(ref tblClientEDIInterface);
                }
                else
                {
                    ClientInterface objClientInterface = AutoMapper.Mapper.Map<ClientInterface>(updatedClientEDIInterface);
                    tblClientEDIInterface = (ITable)objClientInterface;
                    DBO.Provider.Create(ref tblClientEDIInterface);

                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion

       
    }
}