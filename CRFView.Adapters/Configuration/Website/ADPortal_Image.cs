﻿using System;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.Providers;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADPortal_Image
    {
        #region Properties
        public int PKID { get; set; }
        public byte[] Image { get; set; }
        public string UniqueId { get; set; }
        #endregion

        #region CRUD Operations

        //Read Details from database
        public ADPortal_Image ReadImage(int id)
        {
            Portal_Image myentity = new Portal_Image();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (Portal_Image)myentityItable;
            AutoMapper.Mapper.CreateMap<Portal_Image, ADPortal_Image>();
            return (AutoMapper.Mapper.Map<ADPortal_Image>(myentity));
        }

        ////Save(Create or Update) Portal_Image Details
        public int SaveImage(ADPortal_Image updatedImage)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADPortal_Image, Portal_Image>();
                ITable tblImage;
                Portal_Image objImage;
                if (updatedImage.PKID != 0)
                {
                    ADPortal_Image loadImage = ReadImage(updatedImage.PKID);
                    loadImage.Image = updatedImage.Image;
                    loadImage.UniqueId = updatedImage.UniqueId;
                   objImage = AutoMapper.Mapper.Map<Portal_Image>(loadImage);
                    tblImage = (ITable)objImage;
                    DBO.Provider.Update(ref tblImage);
                }
                else
                {
                   objImage = AutoMapper.Mapper.Map<Portal_Image>(updatedImage);
                    tblImage = (ITable)objImage;
                    DBO.Provider.Create(ref tblImage);
                }
                objImage = (Portal_Image)tblImage;

                return objImage.PKID;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }
        #endregion
    }
}