﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADPortal_Employee
    {
        #region Properties
        public int PKID { get; set; }
        public string fullname { get; set; }
        public string email { get; set; }
        public string addr { get; set; }
        public string city { get; set; }
        public string state { get; set; }
        public string zip { get; set; }
        public string country { get; set; }
        public string phone { get; set; }
        public string fax { get; set; }
        public string description { get; set; }
        public string imageurl { get; set; }
        public string imagealttext { get; set; }
        public int imageid { get; set; }
        public bool isvisible { get; set; }
        public string title { get; set; }
        public DateTime profiledate { get; set; }
        public bool IsMarketing { get; set; }
        public int DisplayOrder { get; set; }
        //additional property for view
        public byte[] image { get; set; }
        public string uniqueid { get; set; }
        #endregion

        #region CRUD Operations
        //List of Portal_Employee
        public List<ADPortal_Employee> ListEmployees()
        {
            Portal_Employee EntityView = new Portal_Employee();
            TableView EntityList;
            try
            {
                Query query = new Query(EntityView);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADPortal_Employee> CIList = (from DataRow dr in dt.Rows
                                           select new ADPortal_Employee()
                                           {
                                               PKID = Convert.ToInt32(dr["PKID"]),
                                               fullname = Convert.ToString(dr["fullname"]),
                                               title = Convert.ToString(dr["title"]),
                                               isvisible = dr["isvisible"]!=DBNull.Value?Convert.ToBoolean(dr["isvisible"]):false,
                                               IsMarketing = dr["IsMarketing"] != DBNull.Value ? Convert.ToBoolean(dr["IsMarketing"]) : false,
                                               DisplayOrder = dr["DisplayOrder"] != DBNull.Value ? Convert.ToInt32(dr["DisplayOrder"]) : 0,
                                           }).ToList();
                return CIList;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<ADPortal_Employee>());
            }
        }

        //Read Details from database
        public ADPortal_Employee ReadEmployees(int id)
        {
            Portal_Employee myentity = new Portal_Employee();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (Portal_Employee)myentityItable;
            AutoMapper.Mapper.CreateMap<Portal_Employee, ADPortal_Employee>();
            ADPortal_Employee objADPortal_Employee = AutoMapper.Mapper.Map<ADPortal_Employee>(myentity);
            ADPortal_Image objADPortal_Image = new ADPortal_Image();
            objADPortal_Image = objADPortal_Image.ReadImage(objADPortal_Employee.imageid);
            objADPortal_Employee.image = objADPortal_Image.Image;
            objADPortal_Employee.uniqueid = objADPortal_Image.UniqueId;
            return (objADPortal_Employee);
        }

        ////Save(Create or Update) Portal_Employee Details
   
        public bool SaveEmployees(ADPortal_Employee updatedEmployees)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADPortal_Employee, Portal_Employee>();
                ITable tblEmployees;
                Portal_Employee objEmployees;

                ADPortal_Image objPortal_Image = new ADPortal_Image();
                objPortal_Image.PKID = updatedEmployees.imageid;
                objPortal_Image.Image = updatedEmployees.image;
                objPortal_Image.UniqueId = DBO.GetUniqueId();
                updatedEmployees.imageid= objPortal_Image.SaveImage(objPortal_Image);

                if (updatedEmployees.PKID != 0)
                {
                    ADPortal_Employee loadEmployees = ReadEmployees(updatedEmployees.PKID);
                    loadEmployees.fullname = updatedEmployees.fullname;
                    loadEmployees.title = updatedEmployees.title;
                    loadEmployees.phone = updatedEmployees.phone;
                    loadEmployees.fax = updatedEmployees.fax;
                    loadEmployees.email = updatedEmployees.email;
                    loadEmployees.description = updatedEmployees.description;
                    loadEmployees.DisplayOrder = updatedEmployees.DisplayOrder;
                    loadEmployees.isvisible = updatedEmployees.isvisible;
                    loadEmployees.IsMarketing = updatedEmployees.IsMarketing;
                    loadEmployees.imageid = updatedEmployees.imageid;
                    objEmployees = AutoMapper.Mapper.Map<Portal_Employee>(loadEmployees);
                    tblEmployees = (ITable)objEmployees;
                    DBO.Provider.Update(ref tblEmployees);
                }
                else
                {
                   objEmployees = AutoMapper.Mapper.Map<Portal_Employee>(updatedEmployees);
                    tblEmployees = (ITable)objEmployees;
                    DBO.Provider.Create(ref tblEmployees);
                }
                    
                           
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }
        #endregion
    }
}