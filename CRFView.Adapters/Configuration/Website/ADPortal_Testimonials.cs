﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADPortal_Testimonials
    {
        #region Properties
        public int PKID { get; set; }
        public string testimonial { get; set; }
        public string testimonialby { get; set; }
        public string FullTestimonial { get; set; }
        #endregion

        #region CRUD Operations
        //List of Portal_Testimonials
        public List<ADPortal_Testimonials> ListTestimonials()
        {
            Portal_Testimonials EntityView = new Portal_Testimonials();
            TableView EntityList;
            try
            {
                Query query = new Query(EntityView);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADPortal_Testimonials> CIList = (from DataRow dr in dt.Rows
                                           select new ADPortal_Testimonials()
                                           {
                                               PKID = Convert.ToInt32(dr["PKID"]),
                                               testimonial = Convert.ToString(dr["testimonial"]),
                                               testimonialby = Convert.ToString(dr["testimonialby"])
                                           }).ToList();
                return CIList;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<ADPortal_Testimonials>());
            }
        }

        //Read Details from database
        public ADPortal_Testimonials ReadTestimonials(int id)
        {
            Portal_Testimonials myentity = new Portal_Testimonials();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (Portal_Testimonials)myentityItable;
            AutoMapper.Mapper.CreateMap<Portal_Testimonials, ADPortal_Testimonials>();
            return (AutoMapper.Mapper.Map<ADPortal_Testimonials>(myentity));
        }

        ////Save(Create or Update) Portal_Testimonials Details
        
        public bool SaveTestimonials(ADPortal_Testimonials updatedTestimonials)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADPortal_Testimonials, Portal_Testimonials>();
                ITable tblTestimonials;
                Portal_Testimonials objTestimonials;
                if (updatedTestimonials.PKID != 0)
                {
                    ADPortal_Testimonials loadTestimonials = ReadTestimonials(updatedTestimonials.PKID);
                    loadTestimonials.testimonial = updatedTestimonials.testimonial;
                    loadTestimonials.testimonialby = updatedTestimonials.testimonialby;
                   objTestimonials = AutoMapper.Mapper.Map<Portal_Testimonials>(loadTestimonials);
                    tblTestimonials = (ITable)objTestimonials;
                    DBO.Provider.Update(ref tblTestimonials);
                }
                else
                {
                   objTestimonials = AutoMapper.Mapper.Map<Portal_Testimonials>(updatedTestimonials);
                    tblTestimonials = (ITable)objTestimonials;
                    DBO.Provider.Create(ref tblTestimonials);
                }        
                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }
        #endregion
    }
}