﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using CRF.BLL.CRFDB.VIEWS;
using CRFView.Adapters.Liens.Reports.LienReports.ViewModels;
using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL;

namespace CRFView.Adapters
{
    public class ADLienReports
    {
        #region Properties
        public int ReportId { get; set; }
        public string ReportName { get; set; }
        public string Description { get; set; }
        public int ReportFileId { get; set; }
        public int ReportCategoryId { get; set; }
        public string PrintFrom { get; set; }
        public string CustomSPName { get; set; }
        //addition properties for view
        public string ReportCategory { get; set; }
        public string ReportFileName { get; set; }
        #endregion

        #region CRUD Operations
        //List of LienReports
        public List<ADLienReports> ListLienReports()
        {
            vwLienReportsList EntityView = new vwLienReportsList();
            TableView EntityList;
            try
            {
                Query query = new Query(EntityView);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADLienReports> CIList = (from DataRow dr in dt.Rows
                                           select new ADLienReports()
                                           {
                                               ReportId = Convert.ToInt32(dr["ReportId"]),
                                               ReportName = Convert.ToString(dr["ReportName"]),
                                               Description = Convert.ToString(dr["Description"]),
                                               ReportCategory = Convert.ToString(dr["ReportCategory"]),
                                               PrintFrom = Convert.ToString(dr["PrintFrom"]),
                                               CustomSPName = Convert.ToString(dr["CustomSPName"]),
                                               ReportFileName = Convert.ToString(dr["ReportFileName"])
                                           }).ToList();
                return CIList;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<ADLienReports>());
            }
        }

        //Read Details from database
        public ADLienReports ReadLienReports(int id)
        {
            LienReports myentity = new LienReports();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (LienReports)myentityItable;
            AutoMapper.Mapper.CreateMap<LienReports, ADLienReports>();
            return (AutoMapper.Mapper.Map<ADLienReports>(myentity));
        }

        ////Save(Create or Update) LienReports Details
        public bool SaveLienReports(ADLienReports updatedLienReports)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADLienReports, LienReports>();
                ITable tblLienReports;
                if (updatedLienReports.ReportId != 0)
                {
                    ADLienReports loadLienReports = ReadLienReports(updatedLienReports.ReportId);
                    loadLienReports.ReportName = updatedLienReports.ReportName;
                    loadLienReports.Description = updatedLienReports.Description;
                    loadLienReports.ReportCategoryId = updatedLienReports.ReportCategoryId;
                    loadLienReports.PrintFrom = updatedLienReports.PrintFrom;
                    loadLienReports.ReportFileId = updatedLienReports.ReportFileId;
                    loadLienReports.CustomSPName = updatedLienReports.CustomSPName;
                    LienReports objLienReports = AutoMapper.Mapper.Map<LienReports>(loadLienReports);
                    tblLienReports = (ITable)objLienReports;
                    DBO.Provider.Update(ref tblLienReports);
                    return true;
                }
                else
                {
                    LienReports objLienReports = AutoMapper.Mapper.Map<LienReports>(updatedLienReports);
                    tblLienReports = (ITable)objLienReports;
                    DBO.Provider.Create(ref tblLienReports);
                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }


        //Print Letters PDF
        public static string PrintLienReports(LienReportsVM ObjLienReportsVM)
        {
            string pdfPath = null;
            string sFromDate = null;
            string sThruDate = null;

            try
            {

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var Sp_OBJ = new uspbo_Reports_GetLienReportsInfo();

                string sql = "Select * from vwLienReportsList Where ReportId = " + Convert.ToInt32(ObjLienReportsVM.ddlReportId);
                TableView mytableview = DBO.Provider.GetTableView(sql);
                DataSet myds = DBO.Provider.GetDataSet(sql);
                DataTable mydt = myds.Tables[0];

                if(ObjLienReportsVM.chkAllClients == false)
                {
                    Sp_OBJ.ClientId = ObjLienReportsVM.ddlClientId;
                }

                if(ObjLienReportsVM.chkAllJobIds == false)
                {
                    Sp_OBJ.JobId = ObjLienReportsVM.JobId;
                }

                if(ObjLienReportsVM.chkAllDesks == false)
                {
                    Sp_OBJ.DeskNum = ObjLienReportsVM.ddlDeskId;
                }

                if (ObjLienReportsVM.chkAllDesksGroup == false)
                {
                    Sp_OBJ.DeskGroupName = ObjLienReportsVM.ddlDeskGroupId;
                }

                if (ObjLienReportsVM.chkAllSman == false)
                {
                    Sp_OBJ.SmanNum = ObjLienReportsVM.ddlSalesTeamId;
                }

                if (ObjLienReportsVM.chkByAssignDate == true)
                {
                    Sp_OBJ.AssignDateFROM = ObjLienReportsVM.AssignDateFrom.ToShortDateString();
                    Sp_OBJ.AssignDateTO = ObjLienReportsVM.AssignDateThru.ToShortDateString();
                    sFromDate = ObjLienReportsVM.AssignDateFrom.ToShortDateString();
                    sThruDate = ObjLienReportsVM.AssignDateThru.ToShortDateString();
                }

                if(ObjLienReportsVM. chkByLienAssignDate == true)
                {
                    Sp_OBJ.LienAssignDateFROM = ObjLienReportsVM.LienAssignDateFrom.ToShortDateString();
                    Sp_OBJ.LienAssignDateTO = ObjLienReportsVM.LienAssignDateThru.ToShortDateString();
                    sFromDate = ObjLienReportsVM.LienAssignDateFrom.ToShortDateString();
                    sThruDate = ObjLienReportsVM.LienAssignDateThru.ToShortDateString();
                }

                if (ObjLienReportsVM.chkByNoticeSentDate == true)
                {
                    Sp_OBJ.LienAssignDateFROM = ObjLienReportsVM.LienAssignDateFrom.ToShortDateString();
                    Sp_OBJ.LienAssignDateTO = ObjLienReportsVM.LienAssignDateThru.ToShortDateString();
                    sFromDate = ObjLienReportsVM.LienAssignDateFrom.ToShortDateString();
                    sThruDate = ObjLienReportsVM.LienAssignDateThru.ToShortDateString();
                }

                if (ObjLienReportsVM.chkByLienDate == true)
                {
                    Sp_OBJ.LienDateFROM = ObjLienReportsVM.LienDateFrom.ToShortDateString();
                    Sp_OBJ.LienDateTO = ObjLienReportsVM.LienDateThru.ToShortDateString();
                    sFromDate = ObjLienReportsVM.LienDateFrom.ToShortDateString();
                    sThruDate = ObjLienReportsVM.LienDateThru.ToShortDateString();
                }

                if(ObjLienReportsVM.chkByTrxDate == true)
                {
                    Sp_OBJ.TrxDateFROM = ObjLienReportsVM.TrxDateFrom.ToShortDateString();
                    Sp_OBJ.TrxDateTO = ObjLienReportsVM.TrxDateThru.ToShortDateString();
                    sFromDate = ObjLienReportsVM.TrxDateFrom.ToShortDateString();
                    sThruDate = ObjLienReportsVM.TrxDateThru.ToShortDateString();
                }

                if(ObjLienReportsVM.chkAllStatus == false)
                {

                    //Sp_OBJ.JobStatus = ObjLienReportsVM.
                    //mysproc.JobStatus = GetCheckedStatusList()
                    //mysproc.JobStatus = Mid(mysproc.JobStatus, 1, Len(mysproc.JobStatus) - 1)
                }

                if(ObjLienReportsVM.chkAllStatusGroup == false)
                {
                    Sp_OBJ.StatusGroup = ObjLienReportsVM.JobStatusGroupId;
                }

                if(ObjLienReportsVM.chkByJobState == false)
                {
                    Sp_OBJ.JobState = ObjLienReportsVM.JobState;
                }    

                Sp_OBJ.CustomSPName = Convert.ToString(mydt.Rows[0]["CustomSPName"]);
                Sp_OBJ.ReportName = Convert.ToString(mydt.Rows[0]["ReportName"]);

                TableView myview = DBO.Provider.GetTableView(Sp_OBJ);

                DataSet ds = DBO.Provider.GetDataSet(Sp_OBJ);

                DataTable mytbl = ds.Tables[0];

                CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;
                if (ObjLienReportsVM.RbtPrintOption == "Send To PDF")
                {
                    mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                }

                string myreportfilename = Convert.ToString(mydt.Rows[0]["ReportFileName"]);
                string myreportname = Convert.ToString(mydt.Rows[0]["ReportName"]);

                if (mytbl.Rows.Count > 0)
                {
                    pdfPath = Globals.RenderC1Report(user, mytbl, myreportfilename, myreportname, mymode, "", sFromDate, sThruDate);
                }

            }

            catch (Exception ex)
            {
                return "";
            }
            return pdfPath;
        }



        private static string ProduceCSV(DataView aview, string areportname)
        {
            CRF.BLL.Export.ExportColumns mycols = new CRF.BLL.Export.ExportColumns();
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            string sfilepath = CRF.BLL.CRFView.CRFView.GetFileName(ref user, "Management", areportname, ".CSV");
            string fileName = CRF.BLL.Export.Export.ExportToCSV(ref aview, ref sfilepath, ref mycols, true);
            if (!string.IsNullOrEmpty(fileName))
            {
                return fileName;
            }
            else
            {
                return "";
            }
        }




        #endregion
    }
}