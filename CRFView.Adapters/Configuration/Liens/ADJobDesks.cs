﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADJobDesks
    {
        #region Properties
        public int DeskId { get; set; }
        public string DeskNum { get; set; }
        public string Title { get; set; }
        public string PhoneNum { get; set; }
        public string FaxNum { get; set; }
        public string DeskName { get; set; }
        public int DeskGrpId { get; set; }
        public int InventoryThreshold { get; set; }
        public int LastInventoryCount { get; set; }
        public string Email { get; set; }
        public int DailyGoal { get; set; }
        #endregion

        #region CRUD Operations
        //List of JobDesks
        public List<ADJobDesks> ListJobDesks()
        {
            JobDesks EntityView = new JobDesks();
            TableView EntityList;
            try
            {

                Query query = new Query(EntityView);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADJobDesks> CIList = (from DataRow dr in dt.Rows
                                           select new ADJobDesks()
                                           {
                                               DeskId = Convert.ToInt32(dr["DeskId"]),
                                               DeskNum = Convert.ToString(dr["DeskNum"]),
                                               DeskName = Convert.ToString(dr["DeskName"]),
                                               Title = Convert.ToString(dr["Title"]),
                                               PhoneNum = Convert.ToString(dr["PhoneNum"]),
                                               FaxNum = Convert.ToString(dr["FaxNum"]),
                                               Email = Convert.ToString(dr["Email"]),
                                               InventoryThreshold = dr["InventoryThreshold"] != DBNull.Value ? Convert.ToInt32(dr["InventoryThreshold"]) : 0,
                                               LastInventoryCount = dr["LastInventoryCount"] != DBNull.Value ? Convert.ToInt32(dr["LastInventoryCount"]) : 0
                                           }).ToList();

                return CIList;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<ADJobDesks>());
            }
        }

        //Read Details from database
        public ADJobDesks ReadJobDesks(int id)
        {
            JobDesks myentity = new JobDesks();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (JobDesks)myentityItable;
            AutoMapper.Mapper.CreateMap<JobDesks, ADJobDesks>();
            return (AutoMapper.Mapper.Map<ADJobDesks>(myentity));
        }

        ////Save(Create or Update) JobDesks Details
        public bool SaveJobDesks(ADJobDesks updatedJobDesks)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADJobDesks, JobDesks>();
                ITable tblJobDesks;
                if (updatedJobDesks.DeskId != 0)
                {
                    ADJobDesks loadJobDesks = ReadJobDesks(updatedJobDesks.DeskId);
                    loadJobDesks.DeskNum = updatedJobDesks.DeskNum;
                    loadJobDesks.Title = updatedJobDesks.Title;
                    loadJobDesks.PhoneNum = updatedJobDesks.PhoneNum;
                    loadJobDesks.FaxNum = updatedJobDesks.FaxNum;
                    loadJobDesks.DeskName = updatedJobDesks.DeskName;
                    loadJobDesks.DeskGrpId = updatedJobDesks.DeskGrpId;
                    loadJobDesks.InventoryThreshold = updatedJobDesks.InventoryThreshold;
                    loadJobDesks.LastInventoryCount = updatedJobDesks.LastInventoryCount;
                    loadJobDesks.Email = updatedJobDesks.Email;
                    loadJobDesks.DailyGoal = updatedJobDesks.DailyGoal;
                    JobDesks objJobDesks = AutoMapper.Mapper.Map<JobDesks>(loadJobDesks);
                    tblJobDesks = (ITable)objJobDesks;
                    DBO.Provider.Update(ref tblJobDesks);
                    return true;
                }
                else
                {
                    JobDesks objJobDesks = AutoMapper.Mapper.Map<JobDesks>(updatedJobDesks);
                    tblJobDesks = (ITable)objJobDesks;
                    DBO.Provider.Create(ref tblJobDesks);
                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }
        #endregion


    }
}