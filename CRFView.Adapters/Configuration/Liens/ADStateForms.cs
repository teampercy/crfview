﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using CRF.BLL.CRFDB.VIEWS;

namespace CRFView.Adapters
{
    public class ADStateForms
    {
        #region Properties
        public int Id { get; set; }
        public string StateCode { get; set; }
        public string FormCode { get; set; }
        public string NoticeCode { get; set; }
        public string Description { get; set; }
        public string FormType { get; set; }
        public bool IsPublic { get; set; }
        public bool IsPrivate { get; set; }
        public bool IsResidential { get; set; }
        public bool IsRental { get; set; }
        public bool IsOvernight { get; set; }
        public bool IsTX60Day { get; set; }
        public bool IsTX90Day { get; set; }
        public bool IsNonStat { get; set; }
        public bool IsFederal { get; set; }
        public string WaiverSigRTF { get; set; }
        public bool IsPIFPrompt { get; set; }
        public bool IsWaiverUpdated { get; set; }
        public DateTime WaiverUpdateDate { get; set; }
        //other properties for view
        public string JobType { get; set; }
        #endregion

        #region CRUD Operations
        //List of StateForms
        public List<ADStateForms> ListStateForms()
        {
            StateForms EntityView = new StateForms();
            TableView EntityList;
            try
            {
                Query query = new Query(EntityView);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADStateForms> CIList = (from DataRow dr in dt.Rows
                                           select new ADStateForms()
                                           {
                                               Id = Convert.ToInt32(dr["Id"]),
                                               StateCode = Convert.ToString(dr["StateCode"]),
                                               FormType = Convert.ToString(dr["FormType"]),
                                               FormCode = Convert.ToString(dr["FormCode"]),
                                               Description = Convert.ToString(dr["Description"]),
                                               IsPublic = dr["IsPublic"]!= DBNull.Value?Convert.ToBoolean(dr["IsPublic"]):false,
                                               IsPrivate = dr["IsPrivate"] != DBNull.Value ? Convert.ToBoolean(dr["IsPrivate"]) : false,
                                               IsResidential = dr["IsResidential"] != DBNull.Value ? Convert.ToBoolean(dr["IsResidential"]) : false,
                                               IsFederal = dr["IsFederal"] != DBNull.Value ? Convert.ToBoolean(dr["IsFederal"]) : false,
                                               IsRental = dr["IsRental"] != DBNull.Value ? Convert.ToBoolean(dr["IsRental"]) : false,
                                               IsNonStat = dr["IsNonStat"] != DBNull.Value ? Convert.ToBoolean(dr["IsNonStat"]) : false,
                                               IsTX60Day = dr["IsTX60Day"] != DBNull.Value ? Convert.ToBoolean(dr["IsTX60Day"]) : false,
                                               IsTX90Day = dr["IsTX90Day"] != DBNull.Value ? Convert.ToBoolean(dr["IsTX90Day"]) : false,
                                               IsWaiverUpdated = dr["IsWaiverUpdated"] != DBNull.Value ? Convert.ToBoolean(dr["IsWaiverUpdated"]) : false,
                                               WaiverUpdateDate = dr["WaiverUpdateDate"] != DBNull.Value ? Convert.ToDateTime(dr["WaiverUpdateDate"]) : DateTime.MinValue,
                                           }).ToList();
                return CIList;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<ADStateForms>());
            }
        }

        //Read Details from database
        public ADStateForms ReadStateForms(int id)
        {
            StateForms myentity = new StateForms();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (StateForms)myentityItable;
            AutoMapper.Mapper.CreateMap<StateForms, ADStateForms>();
            return (AutoMapper.Mapper.Map<ADStateForms>(myentity));
        }

        ////Save(Create or Update) StateForms Details
        public bool SaveStateForms(ADStateForms updatedStateForms)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADStateForms, StateForms>();
                ITable tblStateForms;
                if (updatedStateForms.Id != 0)
                {
                    ADStateForms loadStateForms = ReadStateForms(updatedStateForms.Id);
                    loadStateForms.StateCode = updatedStateForms.StateCode;
                    loadStateForms.FormCode = updatedStateForms.FormCode;
                    loadStateForms.NoticeCode = updatedStateForms.NoticeCode;
                    loadStateForms.Description = updatedStateForms.Description;
                    loadStateForms.FormType = updatedStateForms.FormType;
                    loadStateForms.WaiverSigRTF = updatedStateForms.WaiverSigRTF;
                    loadStateForms.IsPublic = updatedStateForms.IsPublic;
                    loadStateForms.IsPrivate = updatedStateForms.IsPrivate;
                    loadStateForms.IsResidential = updatedStateForms.IsResidential;
                    loadStateForms.IsFederal = updatedStateForms.IsFederal;
                    loadStateForms.IsRental = updatedStateForms.IsRental;
                    loadStateForms.IsOvernight = updatedStateForms.IsOvernight;
                    loadStateForms.IsTX60Day = updatedStateForms.IsTX60Day;
                    loadStateForms.IsTX90Day = updatedStateForms.IsTX90Day;
                    loadStateForms.IsNonStat = updatedStateForms.IsNonStat;
                    loadStateForms.IsPIFPrompt = updatedStateForms.IsPIFPrompt;
                    loadStateForms.IsWaiverUpdated = updatedStateForms.IsWaiverUpdated ;
                    loadStateForms.WaiverUpdateDate = updatedStateForms.WaiverUpdateDate;
                    StateForms objStateForms = AutoMapper.Mapper.Map<StateForms>(loadStateForms);
                    tblStateForms = (ITable)objStateForms;
                    DBO.Provider.Update(ref tblStateForms);
                    return true;
                }
                else
                {
                    StateForms objStateForms = AutoMapper.Mapper.Map<StateForms>(updatedStateForms);
                    tblStateForms = (ITable)objStateForms;
                    DBO.Provider.Create(ref tblStateForms);
                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }
        #endregion
    }
}