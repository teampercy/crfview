﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using CRF.BLL.CRFDB.VIEWS;

namespace CRFView.Adapters
{
    public class ADJobNoticeGroup
    {
        #region Properties
        public int Id { get; set; }
        public string PrintGroup { get; set; }
        public bool IsTXNotice { get; set; }
        public int PostageRateId { get; set; }
        public bool IsGreencard { get; set; }
        public bool PrintMailingLog { get; set; }
        public bool IsCertifiedMail { get; set; }
        public bool IsMailingLogPostage { get; set; }
        // additional property for view
        public string PostageRate { get; set; }
        #endregion

        #region CRUD Operations
        //List of JobNoticeGroup
        public List<ADJobNoticeGroup> ListJobNoticeGroup()
        {
            vwNoticePrintGroupList EntityView = new vwNoticePrintGroupList();
            TableView EntityList;
            try
            {

                Query query = new Query(EntityView);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADJobNoticeGroup> CIList = (from DataRow dr in dt.Rows
                                           select new ADJobNoticeGroup()
                                           {
                                               Id = Convert.ToInt32(dr["Id"]),
                                               PrintGroup = Convert.ToString(dr["PrintGroup"]),
                                               IsTXNotice = dr["IsTXNotice"]!=DBNull.Value?Convert.ToBoolean(dr["IsTXNotice"]):false,
                                               PrintMailingLog = dr["PrintMailingLog"] != DBNull.Value ? Convert.ToBoolean(dr["PrintMailingLog"]) : false,
                                               IsCertifiedMail = dr["IsCertifiedMail"] != DBNull.Value ? Convert.ToBoolean(dr["IsCertifiedMail"]) : false,
                                               IsGreencard = dr["IsGreencard"] != DBNull.Value ? Convert.ToBoolean(dr["IsGreencard"]) : false,
                                               IsMailingLogPostage = dr["IsMailingLogPostage"] != DBNull.Value ? Convert.ToBoolean(dr["IsMailingLogPostage"]) : false,
                                               PostageRate = Convert.ToString(dr["PostageRate"])
                                           }).ToList();

                return CIList;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<ADJobNoticeGroup>());
            }
        }

        //Read Details from database
        public ADJobNoticeGroup ReadJobNoticeGroup(int id)
        {
            JobNoticeGroup myentity = new JobNoticeGroup();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (JobNoticeGroup)myentityItable;
            AutoMapper.Mapper.CreateMap<JobNoticeGroup, ADJobNoticeGroup>();
            return (AutoMapper.Mapper.Map<ADJobNoticeGroup>(myentity));
        }

        ////Save(Create or Update) JobNoticeGroup Details
        public bool SaveJobNoticeGroup(ADJobNoticeGroup updatedJobNoticeGroup)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADJobNoticeGroup, JobNoticeGroup>();
                ITable tblJobNoticeGroup;
                if (updatedJobNoticeGroup.Id != 0)
                {
                    JobNoticeGroup objJobNoticeGroup = AutoMapper.Mapper.Map<JobNoticeGroup>(updatedJobNoticeGroup);
                    tblJobNoticeGroup = (ITable)objJobNoticeGroup;
                    DBO.Provider.Update(ref tblJobNoticeGroup);
                    return true;
                }
                else
                {
                    JobNoticeGroup objJobNoticeGroup = AutoMapper.Mapper.Map<JobNoticeGroup>(updatedJobNoticeGroup);
                    tblJobNoticeGroup = (ITable)objJobNoticeGroup;
                    DBO.Provider.Create(ref tblJobNoticeGroup);
                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }
        #endregion


    }
}