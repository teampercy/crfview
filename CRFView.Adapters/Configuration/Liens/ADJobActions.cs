﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using CRF.BLL.CRFDB.VIEWS;

namespace CRFView.Adapters
{
    public class ADJobActions
    {
        #region Properties
        public int Id { get; set; }
        public string Description { get; set; }
        public int NextCall { get; set; }
        public bool IsAll { get; set; }
        public bool IsClient { get; set; }
        public bool IsVerifier { get; set; }
        public bool IsLienVerifier { get; set; }
        public bool IsDataEntry { get; set; }
        public bool NotifyClient { get; set; }
        public bool IsChangeStatus { get; set; }
        public bool IsChangeDesk { get; set; }
        public int NewStatusId { get; set; }
        public int NewDeskId { get; set; }
        public int EventTypeId { get; set; }
        public bool IsBillableEvent { get; set; }
        public bool IsDropReview { get; set; }
        public bool IsNotation { get; set; }
        public string HistoryNote { get; set; }
        public bool IsSystem { get; set; }
        public string CategoryType { get; set; }
        public bool PublicJobBox { get; set; }
        public bool FederalJobBox { get; set; }
        public bool CustRecBox { get; set; }
        public bool CustFaxRecBox { get; set; }
        public bool GCRecBox { get; set; }
        public bool GCFaxRecBox { get; set; }
        public bool AccessorBox { get; set; }
        public bool ClientRecBox { get; set; }
        public bool OnlineRecBox { get; set; }
        public bool CustChangeBox { get; set; }
        public bool GCChangeBox { get; set; }
        public bool OpenItem1Box { get; set; }
        public bool OpenItem2Box { get; set; }
        public bool OPenItem3Box { get; set; }
        public bool CustAutoFaxBox { get; set; }
        public bool GCAutoFaxBox { get; set; }
        public bool JobChangeBox { get; set; }
        public bool LenderChangeBox { get; set; }
        public bool OwnerChangeBox { get; set; }
        public bool CancelLienRequestBox { get; set; }
        public bool CancelLienReleaseBox { get; set; }
        public bool CancelBondClaimBox { get; set; }
        public bool CancelBondReleaseBox { get; set; }
        public bool CancelStopNoticeBox { get; set; }
        public bool CancelStopReleaseBox { get; set; }
        public bool CancelNTOBox { get; set; }
        public bool CancelNOIBox { get; set; }
        public bool CancelLienExtensionBox { get; set; }
        public bool PromptActionQty { get; set; }
        public bool ValidateForNoticeSent { get; set; }
        public bool DesigneeChangeBox { get; set; }
        public bool IsTXJobAction { get; set; }
        public bool DeskChangeBox { get; set; }
        public bool IsMailReturnAction { get; set; }
        public bool NOCBox { get; set; }
        public bool IsReviewed { get; set; }
        public bool IsMailDeliveredAction { get; set; }
        public bool IsJobView { get; set; }
        public bool IsChangeJVStatus { get; set; }
        public int NewJobViewStatusId { get; set; }
        public bool IsChangeJVDesk { get; set; }
        public int NewJobViewDeskId { get; set; }
        public bool NCInfoBox { get; set; }
        //Additional properties for view
        public string DeskNum { get; set; }
        public string JobStatus { get; set; }
        public string JobViewDesk { get; set; }
        public string JobviewStatus { get; set; }
        public string ActionType { get; set; }
        #endregion

        #region CRUD Operations

        JobActions myentity;
        //List of JobActions
        public List<ADJobActions> ListJobActions()
        {

            vwJobActionsList EntityView = new vwJobActionsList();
            TableView EntityList;
            try
            {
                Query query = new Query(EntityView);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADJobActions> List = (from DataRow dr in dt.Rows
                                             select new ADJobActions()
                                             {
                                                 Id = Convert.ToInt16(dr["Id"]),
                                                 Description = Convert.ToString(dr["Description"]),
                                                 IsJobView = dr["IsJobView"] != DBNull.Value ? Convert.ToBoolean(dr["IsJobView"]):false,
                                                 IsSystem = dr["IsSystem"] != DBNull.Value ? Convert.ToBoolean(dr["IsSystem"]) : false,
                                                 IsClient = dr["IsClient"] != DBNull.Value ? Convert.ToBoolean(dr["IsClient"]) : false,
                                                 IsLienVerifier = dr["IsLienVerifier"] != DBNull.Value ? Convert.ToBoolean(dr["IsLienVerifier"]) : false,
                                                 IsVerifier = dr["IsVerifier"] != DBNull.Value ? Convert.ToBoolean(dr["IsVerifier"]) : false,
                                                 IsDataEntry = dr["IsDataEntry"] != DBNull.Value ? Convert.ToBoolean(dr["IsDataEntry"]) : false,
                                                 IsTXJobAction = dr["IsTXJobAction"] != DBNull.Value ? Convert.ToBoolean(dr["IsTXJobAction"]) : false,
                                                 IsDropReview = dr["IsDropReview"] != DBNull.Value ? Convert.ToBoolean(dr["IsDropReview"]) : false,
                                                 IsNotation = dr["IsNotation"] != DBNull.Value ? Convert.ToBoolean(dr["IsNotation"]) : false,
                                                 DeskNum = Convert.ToString(dr["DeskNum"]),
                                                 JobStatus = Convert.ToString(dr["JobStatus"]),
                                                 NextCall = dr["NextCall"]!= DBNull.Value ? Convert.ToInt32(dr["NextCall"]):0,
                                                 JobViewDesk = Convert.ToString(dr["JobViewDesk"]),
                                                 JobviewStatus = Convert.ToString(dr["JobviewStatus"]),
                                                 ValidateForNoticeSent = dr["ValidateForNoticeSent"] != DBNull.Value ? Convert.ToBoolean(dr["ValidateForNoticeSent"]) : false
                                             }).ToList();

                return List;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<ADJobActions>());
            }
        }

        //Read Details from database
        public ADJobActions ReadJobActions(int id)
        {
                JobActions myentity = new JobActions();
                ITable myentityItable = myentity;
                DBO.Provider.Read(id.ToString(), ref myentityItable);
                myentity = (JobActions)myentityItable;
                AutoMapper.Mapper.CreateMap<JobActions, ADJobActions>();
                return (AutoMapper.Mapper.Map<ADJobActions>(myentity));
        }

        //Read Details from database
        public static ADJobActions ReadJobAction(int id)
        {
            JobActions myentity = new JobActions();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (JobActions)myentityItable;
            AutoMapper.Mapper.CreateMap<JobActions, ADJobActions>();
            return (AutoMapper.Mapper.Map<ADJobActions>(myentity));
        }

        //Save(Create or Update) JobActions Details
        public bool SaveJobActions(ADJobActions updatedJobActions)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADJobActions, JobActions>();
                ITable tblJobActions;
                if (updatedJobActions.ActionType == "IsVerifier")
                {
                    updatedJobActions.IsVerifier=true;
                }
                else if ( updatedJobActions.ActionType == "IsLienVerifier")
                {
                    updatedJobActions.IsLienVerifier = true;
                }
                else if (updatedJobActions.ActionType == "IsDataEntry")
                {
                    updatedJobActions.IsDataEntry = true;
                }
                else if ( updatedJobActions.ActionType == "IsClient")
                {
                    updatedJobActions.IsClient = true;
                }
                else if (updatedJobActions.ActionType== "IsSystem")
                {
                    updatedJobActions.IsSystem = true;
                }
                else if (updatedJobActions.ActionType == "IsJobView")
                {
                    updatedJobActions.IsJobView = true;
                }
                else { }
                if(updatedJobActions.IsChangeDesk==false)
                {
                    updatedJobActions.NewDeskId = 0;
                }
                if (updatedJobActions.IsChangeStatus == false)
                {
                    updatedJobActions.NewStatusId = 0;
                }
                if (updatedJobActions.IsChangeJVDesk == false)
                {
                    updatedJobActions.NewJobViewDeskId = 0;
                }
                if (updatedJobActions.IsChangeJVStatus == false)
                {
                    updatedJobActions.NewJobViewStatusId = 0;
                }
                if (updatedJobActions.IsVerifier == false)
                {
                    updatedJobActions.CategoryType = "";
                }
                if(updatedJobActions.IsDataEntry==false)
                {
                    updatedJobActions.IsTXJobAction = false;
                }
                if (updatedJobActions.Id != 0)
                {
                    ADJobActions loadJobActions = ReadJobActions(updatedJobActions.Id);
                    loadJobActions.Description = updatedJobActions.Description;
                    loadJobActions.IsVerifier = updatedJobActions.IsVerifier;
                    loadJobActions.IsLienVerifier = updatedJobActions.IsLienVerifier;
                    loadJobActions.IsDataEntry = updatedJobActions.IsDataEntry;
                    loadJobActions.IsClient = updatedJobActions.IsClient;
                    loadJobActions.IsSystem = updatedJobActions.IsSystem;
                    loadJobActions.IsJobView = updatedJobActions.IsJobView;
                    //-----------
                    loadJobActions.IsBillableEvent = updatedJobActions.IsBillableEvent;
                    loadJobActions.NextCall = updatedJobActions.NextCall;
                    loadJobActions.IsChangeStatus = updatedJobActions.IsChangeStatus;
                    loadJobActions.IsChangeDesk = updatedJobActions.IsChangeDesk;
                    loadJobActions.IsDropReview = updatedJobActions.IsDropReview;
                    loadJobActions.ValidateForNoticeSent = updatedJobActions.ValidateForNoticeSent;
                    loadJobActions.IsMailDeliveredAction = updatedJobActions.IsMailDeliveredAction;
                    loadJobActions.IsMailReturnAction = updatedJobActions.IsMailReturnAction;
                    loadJobActions.IsChangeJVStatus = updatedJobActions.IsChangeJVStatus;
                    loadJobActions.IsChangeJVDesk = updatedJobActions.IsChangeJVDesk;
                    //-------------------
                    loadJobActions.EventTypeId = updatedJobActions.EventTypeId;
                    loadJobActions.NewStatusId = updatedJobActions.NewStatusId;
                    loadJobActions.NewDeskId = updatedJobActions.NewDeskId;
                    loadJobActions.NewJobViewStatusId = updatedJobActions.NewJobViewStatusId;
                    loadJobActions.NewJobViewDeskId = updatedJobActions.NewJobViewDeskId;
                    loadJobActions.CategoryType = updatedJobActions.CategoryType;
                    loadJobActions.IsTXJobAction = updatedJobActions.IsTXJobAction;
                    //-----------
                    loadJobActions.PublicJobBox = updatedJobActions.PublicJobBox;
                    loadJobActions.FederalJobBox = updatedJobActions.FederalJobBox;
                    loadJobActions.ClientRecBox = updatedJobActions.ClientRecBox;
                    loadJobActions.NOCBox = updatedJobActions.NOCBox;
                    loadJobActions.IsReviewed = updatedJobActions.IsReviewed;
                    loadJobActions.CustRecBox = updatedJobActions.CustRecBox;
                    loadJobActions.CustFaxRecBox = updatedJobActions.CustFaxRecBox;
                    loadJobActions.CustAutoFaxBox = updatedJobActions.CustAutoFaxBox;
                    loadJobActions.CustChangeBox = updatedJobActions.CustChangeBox;
                    loadJobActions.GCRecBox = updatedJobActions.GCRecBox;
                    loadJobActions.GCFaxRecBox = updatedJobActions.GCFaxRecBox;
                    loadJobActions.GCAutoFaxBox = updatedJobActions.GCAutoFaxBox;
                    loadJobActions.GCChangeBox = updatedJobActions.GCChangeBox;
                    loadJobActions.AccessorBox = updatedJobActions.AccessorBox;
                    loadJobActions.OnlineRecBox = updatedJobActions.OnlineRecBox;
                    loadJobActions.JobChangeBox = updatedJobActions.JobChangeBox;
                    loadJobActions.LenderChangeBox = updatedJobActions.LenderChangeBox;
                    loadJobActions.OwnerChangeBox = updatedJobActions.OwnerChangeBox;
                    loadJobActions.DesigneeChangeBox = updatedJobActions.DesigneeChangeBox;
                    loadJobActions.DeskChangeBox = updatedJobActions.DeskChangeBox;
                    loadJobActions.IsNotation = updatedJobActions.IsNotation;
                    loadJobActions.HistoryNote = updatedJobActions.HistoryNote;
                    loadJobActions.CancelLienRequestBox = updatedJobActions.CancelLienRequestBox;
                    loadJobActions.CancelLienReleaseBox = updatedJobActions.CancelLienReleaseBox;
                    loadJobActions.CancelLienExtensionBox = updatedJobActions.CancelLienExtensionBox;
                    loadJobActions.CancelNOIBox = updatedJobActions.CancelNOIBox;
                    loadJobActions.CancelBondClaimBox = updatedJobActions.CancelBondClaimBox;
                    loadJobActions.CancelBondReleaseBox = updatedJobActions.CancelBondReleaseBox;
                    loadJobActions.CancelStopNoticeBox = updatedJobActions.CancelStopNoticeBox;
                    loadJobActions.CancelStopReleaseBox = updatedJobActions.CancelStopReleaseBox;
                    loadJobActions.CancelNTOBox = updatedJobActions.CancelNTOBox;
                    JobActions objJobActions= AutoMapper.Mapper.Map<JobActions>(loadJobActions);
                     tblJobActions= objJobActions;
                    DBO.Provider.Update(ref tblJobActions);
                }
                else
                {
                    JobActions objJobActions= AutoMapper.Mapper.Map<JobActions>(updatedJobActions);
                    tblJobActions= objJobActions;
                    DBO.Provider.Create(ref tblJobActions);
                }
            }
            catch(Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
            return true;
        }
        #endregion
    }
}