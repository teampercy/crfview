﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using CRF.BLL.CRFDB.VIEWS;

namespace CRFView.Adapters
{
    public class ADJobDeskRules
    {
        #region Properties
        public int Id { get; set; }
        public string Description { get; set; }
        public decimal FromBalance { get; set; }
        public decimal ThruBalance { get; set; }
        public int DeskNumId { get; set; }
        //additional property for view
        public string DeskNum { get; set; }
        #endregion

        #region CRUD Operations
        //List of JobDeskRules
        public List<ADJobDeskRules> ListJobDeskRules()
        {
            vwJobDeskRuleList EntityView = new vwJobDeskRuleList();
            TableView EntityList;
            try
            {

                Query query = new Query(EntityView);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADJobDeskRules> CIList = (from DataRow dr in dt.Rows
                                           select new ADJobDeskRules()
                                           {
                                               Id = Convert.ToInt32(dr["Id"]),
                                               Description = Convert.ToString(dr["Description"]),
                                               FromBalance = dr["FromBalance"]!=DBNull.Value?Convert.ToDecimal(dr["FromBalance"]):0,
                                               ThruBalance = dr["ThruBalance"] != DBNull.Value ? Convert.ToDecimal(dr["ThruBalance"]) : 0,
                                               DeskNum = Convert.ToString(dr["DeskNum"]),
                                               }).ToList();

                return CIList;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<ADJobDeskRules>());
            }
        }

        //Read Details from database
        public ADJobDeskRules ReadJobDeskRules(int id)
        {
            JobDeskRules myentity = new JobDeskRules();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (JobDeskRules)myentityItable;
            AutoMapper.Mapper.CreateMap<JobDeskRules, ADJobDeskRules>();
            return (AutoMapper.Mapper.Map<ADJobDeskRules>(myentity));
        }

        ////Save(Create or Update) JobDeskRules Details
        public bool SaveJobDeskRules(ADJobDeskRules updatedJobDeskRules)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADJobDeskRules, JobDeskRules>();
                ITable tblJobDeskRules;
                if (updatedJobDeskRules.Id != 0)
                {
                    ADJobDeskRules loadJobDeskRules = ReadJobDeskRules(updatedJobDeskRules.Id);
                    loadJobDeskRules.Description = updatedJobDeskRules.Description;
                    loadJobDeskRules.FromBalance = updatedJobDeskRules.FromBalance;
                    loadJobDeskRules.ThruBalance = updatedJobDeskRules.ThruBalance;
                    loadJobDeskRules.DeskNumId = updatedJobDeskRules.DeskNumId;
                    JobDeskRules objJobDeskRules = AutoMapper.Mapper.Map<JobDeskRules>(loadJobDeskRules);
                    tblJobDeskRules = (ITable)objJobDeskRules;
                    DBO.Provider.Update(ref tblJobDeskRules);
                    return true;
                }
                else
                {
                    JobDeskRules objJobDeskRules = AutoMapper.Mapper.Map<JobDeskRules>(updatedJobDeskRules);
                    tblJobDeskRules = (ITable)objJobDeskRules;
                    DBO.Provider.Create(ref tblJobDeskRules);
                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }
        #endregion


    }
}