﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADJobDeskGroup
    {
        #region Properties
        public int Id { get; set; }
        public string DeskGroupName { get; set; }
        #endregion

        #region CRUD Operations
        //List of JobDeskGroup
        public List<ADJobDeskGroup> ListJobDeskGroup()
        {
            JobDeskGroup EntityView = new JobDeskGroup();
            TableView EntityList;
            try
            {

                Query query = new Query(EntityView);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADJobDeskGroup> CIList = (from DataRow dr in dt.Rows
                                           select new ADJobDeskGroup()
                                           {
                                               Id = Convert.ToInt32(dr["Id"]),
                                               DeskGroupName = Convert.ToString(dr["DeskGroupName"])
                                                 }).ToList();

                return CIList;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<ADJobDeskGroup>());
            }
        }

        //Read Details from database
        public ADJobDeskGroup ReadJobDeskGroup(int id)
        {
            JobDeskGroup myentity = new JobDeskGroup();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (JobDeskGroup)myentityItable;
            AutoMapper.Mapper.CreateMap<JobDeskGroup, ADJobDeskGroup>();
            return (AutoMapper.Mapper.Map<ADJobDeskGroup>(myentity));
        }

        ////Save(Create or Update) JobDeskGroup Details
        public bool SaveJobDeskGroup(ADJobDeskGroup updatedJobDeskGroup)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADJobDeskGroup, JobDeskGroup>();
                ITable tblJobDeskGroup;
                if (updatedJobDeskGroup.Id != 0)
                {
                    JobDeskGroup objJobDeskGroup = AutoMapper.Mapper.Map<JobDeskGroup>(updatedJobDeskGroup);
                    tblJobDeskGroup = (ITable)objJobDeskGroup;
                    DBO.Provider.Update(ref tblJobDeskGroup);
                    return true;
                }
                else
                {
                    JobDeskGroup objJobDeskGroup = AutoMapper.Mapper.Map<JobDeskGroup>(updatedJobDeskGroup);
                    tblJobDeskGroup = (ITable)objJobDeskGroup;
                    DBO.Provider.Create(ref tblJobDeskGroup);
                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }
        #endregion


    }
}