﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADJobStatusGroup
    {
        #region Properties
        public int Id { get; set; }
        public string StatusGroupDescription { get; set; }
        public int StatusGroupType { get; set; }

        #endregion

        #region CRUD Operations
        //List of JobStatusGroup
        public List<ADJobStatusGroup> ListJobStatusGroup()
        {
            JobStatusGroup EntityView = new JobStatusGroup();
            TableView EntityList;
            try
            {
                Query query = new Query(EntityView);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADJobStatusGroup> CIList = (from DataRow dr in dt.Rows
                                           select new ADJobStatusGroup()
                                           {
                                               Id = Convert.ToInt32(dr["Id"]),
                                               StatusGroupDescription = Convert.ToString(dr["StatusGroupDescription"]),
                                              
                                           }).ToList();
                return CIList;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return (new List<ADJobStatusGroup>());
            }
        }

        //Read Details from database
        public ADJobStatusGroup ReadJobStatusGroup(int id)
        {
            JobStatusGroup myentity = new JobStatusGroup();
            ITable myentityItable = (ITable)myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (JobStatusGroup)myentityItable;
            AutoMapper.Mapper.CreateMap<JobStatusGroup, ADJobStatusGroup>();
            return (AutoMapper.Mapper.Map<ADJobStatusGroup>(myentity));
        }

        ////Save(Create or Update) JobStatusGroup Details
        public bool SaveJobStatusGroup(ADJobStatusGroup updatedJobStatusGroup)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADJobStatusGroup, JobStatusGroup>();
                ITable tblJobStatusGroup;
                if (updatedJobStatusGroup.Id != 0)
                {
                    ADJobStatusGroup loadJobStatusGroup = ReadJobStatusGroup(updatedJobStatusGroup.Id);
                    loadJobStatusGroup.StatusGroupDescription = updatedJobStatusGroup.StatusGroupDescription;
                    JobStatusGroup objJobStatusGroup = AutoMapper.Mapper.Map<JobStatusGroup>(loadJobStatusGroup);
                    tblJobStatusGroup = (ITable)objJobStatusGroup;
                    DBO.Provider.Update(ref tblJobStatusGroup);
                    return true;
                }
                else
                {
                    JobStatusGroup objJobStatusGroup = AutoMapper.Mapper.Map<JobStatusGroup>(updatedJobStatusGroup);
                    tblJobStatusGroup = (ITable)objJobStatusGroup;
                    DBO.Provider.Create(ref tblJobStatusGroup);
                    return true;
                }
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }
        #endregion
    }
}