﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADCustomText
    {
        #region Properties
        public int PKID { get; set; }
        public string Description { get; set; }
        public string CustomText { get; set; }
        public string LetterType { get; set; }
        public string LetterCode { get; set; }

        #endregion

        #region CRUD Operations
        //List of CollectionCustomText
        public List<ADCustomText> ListCustomText()
        {
            CollectionCustomText myentity = new CollectionCustomText();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADCustomText> CTList =  (from DataRow dr in dt.Rows
                          select new ADCustomText()
                          {
                              PKID = Convert.ToInt32(dr["PKID"]),
                              Description = Convert.ToString(dr["Description"]),
                              LetterType = Convert.ToString(dr["LetterType"]),
                              LetterCode = Convert.ToString(dr["LetterCode"]),
                              CustomText = Convert.ToString(dr["CustomText"])
                          }).ToList();

                return CTList;
            }
            catch(Exception ex)
            {
                return (new List<ADCustomText>());
            }
        }

        //Read Details from database
        public ADCustomText ReadCustomText(int id)
        {
            CollectionCustomText myentity = new CollectionCustomText();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (CollectionCustomText)myentityItable;
            AutoMapper.Mapper.CreateMap<CollectionCustomText, ADCustomText>();
            return (AutoMapper.Mapper.Map<ADCustomText>(myentity));
        }

        //Save(Create or Update) CollectionCustomText Details
        public void SaveCustomText(ADCustomText updatedCustomText)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADCustomText, CollectionCustomText>();
                ITable tblCustomText;
       
                if (updatedCustomText.PKID != 0)
                {
                    ADCustomText loadCustomText = ReadCustomText(updatedCustomText.PKID);
                    loadCustomText.Description = updatedCustomText.Description;
                    loadCustomText.LetterType = updatedCustomText.LetterType;
                    loadCustomText.LetterCode = updatedCustomText.LetterCode;
                    loadCustomText.CustomText = updatedCustomText.CustomText;
                    CollectionCustomText objCustomText = AutoMapper.Mapper.Map<CollectionCustomText>(loadCustomText);
                    tblCustomText = objCustomText;
                    DBO.Provider.Update(ref tblCustomText);
                }
                else
                {
                    CollectionCustomText objCustomText = AutoMapper.Mapper.Map<CollectionCustomText>(updatedCustomText);
                    tblCustomText = objCustomText;
                    DBO.Provider.Create(ref tblCustomText);

                }
            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion
    }
}