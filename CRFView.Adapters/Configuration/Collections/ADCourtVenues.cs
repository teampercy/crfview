﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADCourtVenues
    {
        #region Properties
        public int PKID { get; set; }
        public string CourtId { get; set; }
        public string Venue { get; set; }
        public string VenueAdd { get; set; }
        public string VenueCity { get; set; }
        public string VenueState { get; set; }
        public string VenueZip { get; set; }
        public string VenuePhone { get; set; }
        public string VenueWeb { get; set; }

        #endregion

        #region CRUD Operations
        //List of CourtHouse
        public List<ADCourtVenues> ListCourtVenues()
        {
            CourtHouse myentity = new CourtHouse();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADCourtVenues> CTList =  (from DataRow dr in dt.Rows
                          select new ADCourtVenues()
                          {
                              PKID = Convert.ToInt32(dr["PKID"]),
                              CourtId = Convert.ToString(dr["CourtId"]),
                              Venue = Convert.ToString(dr["Venue"])
                          }).ToList();

                return CTList;
            }
            catch(Exception ex)
            {
                return (new List<ADCourtVenues>());
            }
        }

        //Read Details from database
        public ADCourtVenues ReadCourtVenues(int id)
        {
            CourtHouse myentity = new CourtHouse();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (CourtHouse)myentityItable;
            AutoMapper.Mapper.CreateMap<CourtHouse, ADCourtVenues>();
            return (AutoMapper.Mapper.Map<ADCourtVenues>(myentity));
        }

        //Save(Create or Update) CourtHouse Details
        public void SaveCourtVenues(ADCourtVenues updatedCourtVenues)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADCourtVenues, CourtHouse>();
                ITable tblCourtVenues;
       
                if (updatedCourtVenues.PKID != 0)
                {
                    ADCourtVenues loadCourtVenues = ReadCourtVenues(updatedCourtVenues.PKID);
                    loadCourtVenues.CourtId = updatedCourtVenues.CourtId;
                    loadCourtVenues.Venue = updatedCourtVenues.Venue;
                    loadCourtVenues.VenueAdd = updatedCourtVenues.VenueAdd;
                    loadCourtVenues.VenueCity = updatedCourtVenues.VenueCity;
                    loadCourtVenues.VenueState = updatedCourtVenues.VenueState;
                    loadCourtVenues.VenueZip = updatedCourtVenues.VenueZip;
                    loadCourtVenues.VenuePhone = updatedCourtVenues.VenuePhone;
                    loadCourtVenues.VenueWeb = updatedCourtVenues.VenueWeb;
                    CourtHouse objCourtVenues = AutoMapper.Mapper.Map<CourtHouse>(loadCourtVenues);
                    tblCourtVenues = objCourtVenues;
                    DBO.Provider.Update(ref tblCourtVenues);
                }
                else
                {
                    CourtHouse objCourtVenues = AutoMapper.Mapper.Map<CourtHouse>(updatedCourtVenues);
                    tblCourtVenues = objCourtVenues;
                    DBO.Provider.Create(ref tblCourtVenues);

                }
            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion
    }
}