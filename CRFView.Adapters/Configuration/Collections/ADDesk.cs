﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using CRF.BLL;

namespace CRFView.Adapters
{
    public class ADDesk
    {
        #region Properties
        public int LocationId { get; set; }
        public string Location { get; set; }
        public string LocationDescr { get; set; }
        public string DebtorAlias { get; set; }
        public string ClientAlias { get; set; }
        public string LocationTitle { get; set; }
        public string LocationInitials { get; set; }
        public string LocationEmail { get; set; }
        public string LocationPhone { get; set; }
        public string LocationFax { get; set; }
        public bool IsDropContact { get; set; }
        public bool IsDropLetter { get; set; }
        public byte[] Signature { get; set; }
        public int PrimaryOwnerId { get; set; }
        public string TestFileNumber { get; set; }
        public string Certification { get; set; }
        public bool IsInactive { get; set; }
        public byte[] CollectorPhoto { get; set; }

        //addition properties for view
        public string LocationUsersList { get; set; }

        #endregion

        #region CRUD Operations
        //List of Location
        public List<ADDesk> ListDesk()
        {
            Location myentity = new Location();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADDesk> CLList = (from DataRow dr in dt.Rows
                                       select new ADDesk()
                                       {
                                           LocationId = Convert.ToInt32(dr["LocationId"]),
                                           Location = Convert.ToString(dr["Location"]),
                                           LocationDescr = Convert.ToString(dr["LocationDescr"]),
                                           IsInactive = dr["IsInactive"] != DBNull.Value ? Convert.ToBoolean(dr["IsInactive"]) : false
                          }).ToList();

                return CLList;
            }
            catch(Exception ex)
            {
                return (new List<ADDesk>());
            }
        }

        //Read Details from database
        public ADDesk ReadDesk(int id)
        {
            Location myentity = new Location();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (Location)myentityItable;
            AutoMapper.Mapper.CreateMap<Location, ADDesk>();
            return (AutoMapper.Mapper.Map<ADDesk>(myentity));
        }

        //Save(Create or Update) Location Details
        public void SaveDesk(ADDesk updatedDesk)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADDesk, Location>();
                ITable tblDesk;
       
                if (updatedDesk.LocationId != 0)
                {
                    ADDesk loadDesk = ReadDesk(updatedDesk.LocationId);
                    loadDesk.Location = updatedDesk.Location;
                    loadDesk.LocationInitials = updatedDesk.LocationInitials;
                    loadDesk.LocationTitle = updatedDesk.LocationTitle;
                    loadDesk.DebtorAlias = updatedDesk.DebtorAlias;
                    loadDesk.ClientAlias = updatedDesk.ClientAlias;
                    loadDesk.LocationDescr = updatedDesk.LocationDescr;
                    loadDesk.LocationPhone = updatedDesk.LocationPhone;
                    loadDesk.LocationEmail = updatedDesk.LocationEmail;
                    loadDesk.PrimaryOwnerId = updatedDesk.PrimaryOwnerId;
                    loadDesk.Certification = updatedDesk.Certification;
                    loadDesk.IsDropContact = updatedDesk.IsDropContact;
                    loadDesk.IsDropLetter = updatedDesk.IsDropLetter;
                    loadDesk.IsInactive = updatedDesk.IsInactive;
                    loadDesk.Signature = updatedDesk.Signature;
                    loadDesk.CollectorPhoto = updatedDesk.CollectorPhoto;
                    Location objDesk = AutoMapper.Mapper.Map<Location>(loadDesk);
                    tblDesk = objDesk;
                    DBO.Provider.Update(ref tblDesk);
                }
                else
                {
                    Location objDesk = AutoMapper.Mapper.Map<Location>(updatedDesk);
                    tblDesk = objDesk;
                    DBO.Provider.Create(ref tblDesk);
                    updatedDesk.LocationId = Convert.ToInt32(Queries.GetLatestLocationId());
                }
                if (updatedDesk.LocationId !=0)
                {
                    CRF.BLL.CRFDB.SPROCS.cview_Security_UpdateDeskUsers mysproc = new CRF.BLL.CRFDB.SPROCS.cview_Security_UpdateDeskUsers();
                    mysproc.Id = updatedDesk.LocationId.ToString();
                    mysproc.List = updatedDesk.LocationUsersList;
                    DBO.Provider.ExecNonQuery(mysproc);
                }


            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion
    }
}