﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADContactPlans
    {
        #region Properties
        public int ContactPlanId { get; set; }
        public string ContactPlan { get; set; }
        public int InitialCallDelay { get; set; }
        public bool HasPhoneCalls { get; set; }
        public bool HasLetterSeries { get; set; }
        public int Duration { get; set; }
        public int Letter1Days { get; set; }
        public int Letter2Days { get; set; }
        public int Letter3Days { get; set; }
        public int Letter4Days { get; set; }
        public int Letter5Days { get; set; }
        public int Letter6Days { get; set; }
        public string ContactPlanCode { get; set; }
        public int DefaultStatusId { get; set; }
        public int DefaultLocationId { get; set; }
        public decimal Letter1Price { get; set; }
        public decimal Letter2Price { get; set; }
        public decimal Letter3Price { get; set; }
        public decimal Letter4Price { get; set; }
        public decimal Letter5Price { get; set; }
        public decimal Letter6Price { get; set; }
        public string Letter1Code { get; set; }
        public string Letter2Code { get; set; }
        public string Letter3Code { get; set; }
        public string Letter4Code { get; set; }
        public string Letter5Code { get; set; }
        public string Letter6Code { get; set; }
        public bool IsTenDayContactPlan { get; set; }
        #endregion

        #region CRUD Operations
        //List of ContactPlan
        public List<ADContactPlans> ListContactPlans()
        {
            ContactPlan myentity = new ContactPlan();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADContactPlans> CTList =  (from DataRow dr in dt.Rows
                          select new ADContactPlans()
                          {
                              ContactPlanId = Convert.ToInt32(dr["ContactPlanId"]),
                              ContactPlanCode = Convert.ToString(dr["ContactPlanCode"]),
                              ContactPlan = Convert.ToString(dr["ContactPlan"]),
                              IsTenDayContactPlan = (dr["IsTenDayContactPlan"]!= DBNull.Value ? Convert.ToBoolean(dr["IsTenDayContactPlan"]) :false)
                          }).ToList();

                return CTList;
            }
            catch(Exception ex)
            {
                return (new List<ADContactPlans>());
            }
        }

        //Read Details from database
        public ADContactPlans ReadContactPlans(int id)
        {
            ContactPlan myentity = new ContactPlan();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (ContactPlan)myentityItable;
            AutoMapper.Mapper.CreateMap<ContactPlan, ADContactPlans>();
            return (AutoMapper.Mapper.Map<ADContactPlans>(myentity));
        }

        //Save(Create or Update) ContactPlan Details
        public void SaveContactPlans(ADContactPlans updatedContactPlans)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADContactPlans, ContactPlan>();
                ITable tblContactPlans;
                if (updatedContactPlans.ContactPlanId != 0)
                {
                    ADContactPlans loadContactPlans = ReadContactPlans(updatedContactPlans.ContactPlanId);
                    loadContactPlans.DefaultLocationId = updatedContactPlans.DefaultLocationId;
                    loadContactPlans.DefaultStatusId = updatedContactPlans.DefaultStatusId;
                    loadContactPlans.Letter6Code = updatedContactPlans.Letter6Code;
                    loadContactPlans.Letter5Code = updatedContactPlans.Letter5Code;
                    loadContactPlans.Letter4Code = updatedContactPlans.Letter4Code;
                    loadContactPlans.Letter3Code = updatedContactPlans.Letter3Code;
                    loadContactPlans.Letter2Code = updatedContactPlans.Letter2Code;
                    loadContactPlans.Letter1Code = updatedContactPlans.Letter1Code;
                    loadContactPlans.Letter6Price = updatedContactPlans.Letter6Price;
                    loadContactPlans.Letter5Price = updatedContactPlans.Letter5Price;
                    loadContactPlans.Letter4Price = updatedContactPlans.Letter4Price;
                    loadContactPlans.Letter3Price = updatedContactPlans.Letter3Price;
                    loadContactPlans.Letter2Price = updatedContactPlans.Letter2Price;
                    loadContactPlans.Letter1Price = updatedContactPlans.Letter1Price;
                    loadContactPlans.Letter6Days = updatedContactPlans.Letter6Days;
                    loadContactPlans.Letter5Days = updatedContactPlans.Letter5Days;
                    loadContactPlans.Letter4Days = updatedContactPlans.Letter4Days;
                    loadContactPlans.Letter3Days = updatedContactPlans.Letter3Days;
                    loadContactPlans.Letter2Days = updatedContactPlans.Letter2Days;
                    loadContactPlans.Letter1Days = updatedContactPlans.Letter1Days;
                    loadContactPlans.InitialCallDelay = updatedContactPlans.InitialCallDelay;
                    loadContactPlans.HasLetterSeries = updatedContactPlans.HasLetterSeries;
                    loadContactPlans.HasPhoneCalls = updatedContactPlans.HasPhoneCalls;
                    loadContactPlans.ContactPlan = updatedContactPlans.ContactPlan;
                    loadContactPlans.ContactPlanCode = updatedContactPlans.ContactPlanCode;
                    loadContactPlans.IsTenDayContactPlan = updatedContactPlans.IsTenDayContactPlan;
                    ContactPlan objContactPlans = AutoMapper.Mapper.Map<ContactPlan>(loadContactPlans);
                    tblContactPlans = objContactPlans;
                    DBO.Provider.Update(ref tblContactPlans);
                }
                else
                {
                    ContactPlan objContactPlans = AutoMapper.Mapper.Map<ContactPlan>(updatedContactPlans);
                    tblContactPlans = objContactPlans;
                    DBO.Provider.Create(ref tblContactPlans);

                }
            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion
    }
}