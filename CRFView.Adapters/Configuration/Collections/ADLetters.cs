﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADLetters
    {
        #region Properties
        public int PKID { get; set; }
        public string LetterCode { get; set; }
        public string LetterType { get; set; }
        public string LetterDescr { get; set; }
        public string LetterCat { get; set; }
        public int StatusDays { get; set; }
        public int ReviewDays { get; set; }
        public int LegalDays { get; set; }
        public bool IsInactive { get; set; }
        public bool GenerateInvoiceNum { get; set; }
        public string DebtorId { get; set; }
        #endregion

        #region CRUD Operations
        //List of CollectionLetters
        public List<ADLetters> ListLetters()
        {
            CollectionLetters myentity = new CollectionLetters();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADLetters> CTList =  (from DataRow dr in dt.Rows
                          select new ADLetters()
                          {
                              PKID = Convert.ToInt32(dr["PKID"]),
                              LetterCode = Convert.ToString(dr["LetterCode"]),
                              LetterType = Convert.ToString(dr["LetterType"]),
                              LetterDescr = Convert.ToString(dr["LetterDescr"]),
                              ReviewDays = (dr["ReviewDays"] != DBNull.Value ? Convert.ToInt32(dr["ReviewDays"]):0),
                              StatusDays = (dr["StatusDays"] != DBNull.Value ? Convert.ToInt32(dr["StatusDays"]):0),
                              GenerateInvoiceNum = (dr["GenerateInvoiceNum"]!=DBNull.Value?Convert.ToBoolean(dr["GenerateInvoiceNum"]):false),
                              IsInactive = (dr["IsInactive"] != DBNull.Value ? Convert.ToBoolean(dr["IsInactive"]):false)
                          }).ToList();

                return CTList;
            }
            catch(Exception ex)
            {
                return (new List<ADLetters>());
            }
        }

        //Read Details from database
        public ADLetters ReadLetters(int id)
        {
            CollectionLetters myentity = new CollectionLetters();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (CollectionLetters)myentityItable;
            AutoMapper.Mapper.CreateMap<CollectionLetters, ADLetters>();
            return (AutoMapper.Mapper.Map<ADLetters>(myentity));
        }

        //Save(Create or Update) CollectionLetters Details
        public bool SaveLetters(ADLetters updatedLetters)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADLetters, CollectionLetters>();
                ITable tblLetters;
       
                if (updatedLetters.PKID != 0)
                {
                    ADLetters loadLetters = ReadLetters(updatedLetters.PKID);
                    loadLetters.LetterDescr = updatedLetters.LetterDescr;
                    loadLetters.LetterType = updatedLetters.LetterType;
                    loadLetters.LetterCode = updatedLetters.LetterCode;
                    loadLetters.IsInactive = updatedLetters.IsInactive;
                    loadLetters.StatusDays = updatedLetters.StatusDays;
                    loadLetters.ReviewDays = updatedLetters.ReviewDays;
                    loadLetters.GenerateInvoiceNum = updatedLetters.GenerateInvoiceNum;                
                    CollectionLetters objLetters = AutoMapper.Mapper.Map<CollectionLetters>(loadLetters);
                    tblLetters = objLetters;
                    DBO.Provider.Update(ref tblLetters);
                }
                else
                {
                    CollectionLetters objLetters = AutoMapper.Mapper.Map<CollectionLetters>(updatedLetters);
                    tblLetters = objLetters;
                    DBO.Provider.Create(ref tblLetters);

                }
            }
            catch(Exception ex)
            {
                return false;
            }
            return true;
        }

        public string PrintLetter(int id,string letterCode)
        {
           CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
           string pdfPath= CollectionsCollectors.PrintCollectionLetter(user, id, "CollectionLetters.XML", letterCode, HDS.Reporting.COMMON.PrintMode.PrintToPDF, "", "", "", "InvoiceDetailSub");
           return pdfPath;
        }

        //Read Details from database
        public static ADLetters ReadCollectionLetters(int id)
        {
            CollectionLetters myentity = new CollectionLetters();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (CollectionLetters)myentityItable;
            AutoMapper.Mapper.CreateMap<CollectionLetters, ADLetters>();
            return (AutoMapper.Mapper.Map<ADLetters>(myentity));
        }
        #endregion
    }
}