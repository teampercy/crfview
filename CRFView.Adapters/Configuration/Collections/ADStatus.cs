﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using CRF.BLL.CRFDB.VIEWS;

namespace CRFView.Adapters
{
    public class ADStatus
    {
        #region Properties
        public int CollectionStatusId { get; set; }
        public string CollectionStatus { get; set; }
        public string CollectionStatusDescr { get; set; }
        public int StatusGroup { get; set; }
        public bool IsDropContact { get; set; }
        public bool IsDropLetter { get; set; }

        //additional property for View
        public string StatusGroupDescription { get; set; }
        #endregion

        #region CRUD Operations
        //List of CollectionStatus
        public List<ADStatus> ListStatus()
        {
            vwCollectionStatus myentity = new vwCollectionStatus();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADStatus> CTList =  (from DataRow dr in dt.Rows
                          select new ADStatus()
                          {
                              CollectionStatusId = Convert.ToInt32(dr["CollectionStatusId"]),
                              CollectionStatus = Convert.ToString(dr["CollectionStatus"]),
                              CollectionStatusDescr = Convert.ToString(dr["CollectionStatusDescr"]),
                              StatusGroupDescription = Convert.ToString(dr["StatusGroupDescription"])
                          }).ToList();

                return CTList;
            }
            catch(Exception ex)
            {
                return (new List<ADStatus>());
            }
        }

        //Read Details from database
        public ADStatus ReadStatus(int id)
        {
            CollectionStatus myentity = new CollectionStatus();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (CollectionStatus)myentityItable;
            AutoMapper.Mapper.CreateMap<CollectionStatus, ADStatus>();
            return (AutoMapper.Mapper.Map<ADStatus>(myentity));
        }

        //Save(Create or Update) CollectionStatus Details
        public void SaveStatus(ADStatus updatedStatus)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADStatus, CollectionStatus>();
                ITable tblStatus;
       
                if (updatedStatus.CollectionStatusId != 0)
                {
                    ADStatus loadStatus = ReadStatus(updatedStatus.CollectionStatusId);
                    loadStatus.CollectionStatus = updatedStatus.CollectionStatus;
                    loadStatus.CollectionStatusDescr = updatedStatus.CollectionStatusDescr;
                    loadStatus.StatusGroup = updatedStatus.StatusGroup;
                    CollectionStatus objStatus = AutoMapper.Mapper.Map<CollectionStatus>(loadStatus);
                    tblStatus = objStatus;
                    DBO.Provider.Update(ref tblStatus);
                }
                else
                {
                    CollectionStatus objStatus = AutoMapper.Mapper.Map<CollectionStatus>(updatedStatus);
                    tblStatus = objStatus;
                    DBO.Provider.Create(ref tblStatus);

                }
            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion
    }
}