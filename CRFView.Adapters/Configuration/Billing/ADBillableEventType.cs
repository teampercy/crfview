﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;

namespace CRFView.Adapters
{
    public class ADBillableEventType
    {
        #region Properties
        public int Id { get; set; }
       
        public string BillableEventName { get; set; }
        public string BillableEventCode { get; set; }
        public bool IsVolumeBased { get; set; }
        public bool IsMajor { get; set; }
        public bool IsPercentageBased { get; set; }
        public bool IsPostagePlusEvent { get; set; }
        public int DisplayOrder { get; set; }
        public bool IsNonNoticeItem { get; set; }
        public int RevenueColumn { get; set; }
        public decimal StandardPrice { get; set; }
        public bool IsCommissionable { get; set; }
        #endregion

        #region CRUD Operations
        //List of BillableEventType
        public List<ADBillableEventType> ListBillableEventType()
        {
            BillableEventType myentity = new BillableEventType();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADBillableEventType> CTList =  (from DataRow dr in dt.Rows
                          select new ADBillableEventType()
                          {
                              Id = Convert.ToInt32(dr["Id"]),
                              BillableEventName = Convert.ToString(dr["BillableEventName"]),
                              BillableEventCode = Convert.ToString(dr["BillableEventCode"]),
                              DisplayOrder = Convert.ToInt32(dr["DisplayOrder"]),
                              IsVolumeBased = ((dr["IsVolumeBased"] != DBNull.Value ? Convert.ToBoolean(dr["IsVolumeBased"]) : false)),
                              IsNonNoticeItem = ((dr["IsNonNoticeItem"] != DBNull.Value ? Convert.ToBoolean(dr["IsNonNoticeItem"]) : false)),
                              IsCommissionable = ((dr["IsCommissionable"] != DBNull.Value ? Convert.ToBoolean(dr["IsCommissionable"]) : false))

                          }).ToList();

                return CTList;
            }
            catch(Exception ex)
            {
                return (new List<ADBillableEventType>());
            }
        }

        //Read Details from database
        public ADBillableEventType ReadBillableEventType(int id)
        {
            BillableEventType myentity = new BillableEventType();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (BillableEventType)myentityItable;
            AutoMapper.Mapper.CreateMap<BillableEventType, ADBillableEventType>();
            return (AutoMapper.Mapper.Map<ADBillableEventType>(myentity));
        }

        //Save(Create or Update) BillableEventType Details
        public void SaveBillableEventType(ADBillableEventType updatedBillableEventType)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADBillableEventType, BillableEventType>();
                ITable tblBillableEventType;
                if (updatedBillableEventType.Id != 0)
                {
                    ADBillableEventType loadBillableEventType = ReadBillableEventType(updatedBillableEventType.Id);
                    loadBillableEventType.BillableEventName = updatedBillableEventType.BillableEventName;
                    loadBillableEventType.BillableEventCode = updatedBillableEventType.BillableEventCode;
                    loadBillableEventType.IsVolumeBased = updatedBillableEventType.IsVolumeBased;
                    loadBillableEventType.IsMajor = updatedBillableEventType.IsMajor;
                    loadBillableEventType.IsPercentageBased = updatedBillableEventType.IsPercentageBased;
                    loadBillableEventType.IsPostagePlusEvent = updatedBillableEventType.IsPostagePlusEvent;
                    loadBillableEventType.DisplayOrder = updatedBillableEventType.DisplayOrder;
                    loadBillableEventType.IsNonNoticeItem = updatedBillableEventType.IsNonNoticeItem;
                    loadBillableEventType.RevenueColumn = updatedBillableEventType.RevenueColumn;
                    loadBillableEventType.StandardPrice = updatedBillableEventType.StandardPrice;
                    loadBillableEventType.IsCommissionable = updatedBillableEventType.IsCommissionable;
                    BillableEventType objBillableEventType = AutoMapper.Mapper.Map<BillableEventType>(loadBillableEventType);
                    tblBillableEventType = objBillableEventType;
                    DBO.Provider.Update(ref tblBillableEventType);
                }
                else
                {
                    BillableEventType objBillableEventType = AutoMapper.Mapper.Map<BillableEventType>(updatedBillableEventType);
                    tblBillableEventType = objBillableEventType;
                    DBO.Provider.Create(ref tblBillableEventType);
                }
            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion
    }
}