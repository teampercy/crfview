﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADLedgerTypes
    {
        #region Properties
        public int LedgerTypeId { get; set; }
        public string LedgerType { get; set; }
        public string Description { get; set; }
        public bool ToAgency { get; set; }
        public bool ToClient { get; set; }
        public bool WriteToNotes { get; set; }
        public bool OmitFromStmt { get; set; }
        public bool OmitFromCashrpt { get; set; }
        public bool NoCommission { get; set; }
        public bool NoTax { get; set; }
        public string ApplyToCols { get; set; }
        public bool NoCollectorId { get; set; }
        public bool NoSmanId { get; set; }
        public bool NoCommRate { get; set; }
        public bool NoAutoDistribution { get; set; }
        public string NewStatus { get; set; }
        public bool CreditCardrpt { get; set; }
        public bool NSFrpt { get; set; }
        #endregion

        #region CRUD Operations
        //List of LedgerTypes
        public List<ADLedgerTypes> ListLedgerTypes()
        {
            LedgerType myentity = new LedgerType();
            TableView EntityList;
            try
            {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADLedgerTypes> CTList = (from DataRow dr in dt.Rows
                                              select new ADLedgerTypes()
                                              {
                                                  LedgerTypeId = Convert.ToInt32(dr["LedgerTypeId"]),
                                                  LedgerType = Convert.ToString(dr["LedgerType"]),
                                                  Description = Convert.ToString(dr["Description"])
                                              }).ToList();

                return CTList;
            }
            catch (Exception ex)
            {
                return (new List<ADLedgerTypes>());
            }
        }

        //Read Details from database
        public ADLedgerTypes ReadLedgerTypes(int id)
        {

            LedgerType myentity = new LedgerType();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (LedgerType)myentityItable;
            AutoMapper.Mapper.CreateMap<LedgerType, ADLedgerTypes>();
            return (AutoMapper.Mapper.Map<ADLedgerTypes>(myentity));
        }

        //Save(Create or Update) LedgerTypes Details
        public void SaveLedgerTypes(ADLedgerTypes updatedLedgerTypes)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADLedgerTypes, LedgerType>();
                ITable tblLedgerTypes;
                if (updatedLedgerTypes.LedgerTypeId != 0)
                {
                    ADLedgerTypes loadLedgerTypes = ReadLedgerTypes(updatedLedgerTypes.LedgerTypeId);
                    loadLedgerTypes.LedgerType = updatedLedgerTypes.LedgerType;
                    loadLedgerTypes.Description = updatedLedgerTypes.Description;
                    LedgerType objLedgerTypes = AutoMapper.Mapper.Map<LedgerType>(loadLedgerTypes);
                    tblLedgerTypes = objLedgerTypes;
                    DBO.Provider.Update(ref tblLedgerTypes);
                }
                else
                {
                    LedgerType objLedgerTypes = AutoMapper.Mapper.Map<LedgerType>(updatedLedgerTypes);
                    tblLedgerTypes = objLedgerTypes;
                    DBO.Provider.Create(ref tblLedgerTypes);

                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
    }
}