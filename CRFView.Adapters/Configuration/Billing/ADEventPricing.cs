﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using CRF.BLL.CRFDB.VIEWS;
using System.ComponentModel.DataAnnotations;

namespace CRFView.Adapters
{
    public class ADEventPricing
    {
        #region Properties
        public int Id { get; set; }
        public int PriceCodeId { get; set; }
        public int BillableEventId { get; set; }
        public decimal Price { get; set; }
        public int QtyFrom { get; set; }
        public int QtyThru { get; set; }
        public bool IsVolumeBased { get; set; }
        public decimal PricePercentage { get; set; }
        public bool IsDefaultPrice { get; set; }
        public string PriceCodeName { get; set; }
        #endregion

        #region CRUD Operations
        //List of BillableEventPricing
        public List<ADEventPricing> ListEventPricing()
        {
            vwBillableEventPricingList myentity = new vwBillableEventPricingList();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADEventPricing> CTList =  (from DataRow dr in dt.Rows
                          select new ADEventPricing()
                          {
                              Id = Convert.ToInt32(dr["Id"]),
                              PriceCodeName = Convert.ToString(dr["PriceCodeName"]),
                              QtyFrom = ((dr["QtyFrom"] != DBNull.Value ? Convert.ToInt32(dr["QtyFrom"]) : 0)),
                              QtyThru = ((dr["QtyThru"] != DBNull.Value ? Convert.ToInt32(dr["QtyThru"]) : 0)),
                              IsDefaultPrice = ((dr["IsDefaultPrice"] != DBNull.Value ? Convert.ToBoolean(dr["IsDefaultPrice"]) : false)),
                              Price = ((dr["Price"] != DBNull.Value ? Convert.ToDecimal(dr["Price"]) : 0))
                          }).ToList();

                return CTList;
            }
            catch(Exception ex)
            {
                return (new List<ADEventPricing>());
            }
        }

        //Read Details from database
        public ADEventPricing ReadEventPricing(int id)
        {
            BillableEventPricing myentity = new BillableEventPricing();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (BillableEventPricing)myentityItable;
            AutoMapper.Mapper.CreateMap<BillableEventPricing, ADEventPricing>();
            return (AutoMapper.Mapper.Map<ADEventPricing>(myentity));
        }

        //Save(Create or Update) BillableEventPricing Details
        public void SaveEventPricing(ADEventPricing updatedEventPricing)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADEventPricing, BillableEventPricing>();
                ITable tblEventPricing;
                if (updatedEventPricing.Id != 0)
                {
                    ADEventPricing loadEventPricing = ReadEventPricing(updatedEventPricing.Id);
                    loadEventPricing.PriceCodeId = updatedEventPricing.PriceCodeId;
                    loadEventPricing.QtyFrom = updatedEventPricing.QtyFrom;
                    loadEventPricing.QtyThru = updatedEventPricing.QtyThru;
                    loadEventPricing.IsDefaultPrice = updatedEventPricing.IsDefaultPrice;
                    loadEventPricing.Price = updatedEventPricing.Price;
                    BillableEventPricing objEventPricing = AutoMapper.Mapper.Map<BillableEventPricing>(loadEventPricing);
                    tblEventPricing = objEventPricing;
                    DBO.Provider.Update(ref tblEventPricing);
                }
                else
                {
                    BillableEventPricing objEventPricing = AutoMapper.Mapper.Map<BillableEventPricing>(updatedEventPricing);
                    tblEventPricing = objEventPricing;
                    DBO.Provider.Create(ref tblEventPricing);

                }
            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion
    }
}