﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters
{
    public class ADInvoiceTypes
    {
        #region Properties
        public int FinDocTypeId { get; set; }
        public string FinDocTypeName { get; set; }
        public string FinDocTypeCode { get; set; }
        public bool BranchBreak { get; set; }
        public int InvoiceBreak { get; set; }
        public bool CreateCSVInvoice { get; set; }
        public bool IsAvailableInColl { get; set; }
        #endregion

        #region CRUD Operations
        //List of InvoiceTypes
        public List<ADInvoiceTypes> ListInvoiceTypes()
        {
            FinDocType myentity = new FinDocType();
            TableView EntityList;
            try {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADInvoiceTypes> CTList =  (from DataRow dr in dt.Rows
                          select new ADInvoiceTypes()
                          {
                              FinDocTypeId = Convert.ToInt32(dr["FinDocTypeId"]),
                              FinDocTypeName = Convert.ToString(dr["FinDocTypeName"]),
                              BranchBreak = (dr["BranchBreak"] != DBNull.Value ? Convert.ToBoolean(dr["BranchBreak"]) : false),
                              IsAvailableInColl = (dr["IsAvailableInColl"] != DBNull.Value ? Convert.ToBoolean(dr["IsAvailableInColl"]) : false),
                          }).ToList();

                return CTList;
            }
            catch(Exception ex)
            {
                return (new List<ADInvoiceTypes>());
            }
        }

        //Read Details from database
        public ADInvoiceTypes ReadInvoiceTypes(int id)
        {

            FinDocType myentity = new FinDocType();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (FinDocType)myentityItable;
            AutoMapper.Mapper.CreateMap<FinDocType, ADInvoiceTypes>();
            return (AutoMapper.Mapper.Map<ADInvoiceTypes>(myentity));
        }

        //Save(Create or Update) InvoiceTypes Details
        public void SaveInvoiceTypes(ADInvoiceTypes updatedInvoiceTypes)
        {
            try {
                AutoMapper.Mapper.CreateMap<ADInvoiceTypes, FinDocType>();
                ITable tblInvoiceTypes;
                if (updatedInvoiceTypes.FinDocTypeId != 0)
                {
                    ADInvoiceTypes loadADInvoiceTypes = ReadInvoiceTypes(updatedInvoiceTypes.FinDocTypeId);
                    loadADInvoiceTypes.FinDocTypeName = updatedInvoiceTypes.FinDocTypeName;
                    loadADInvoiceTypes.BranchBreak = updatedInvoiceTypes.BranchBreak;
                    loadADInvoiceTypes.IsAvailableInColl = updatedInvoiceTypes.IsAvailableInColl;
                    FinDocType objInvoiceTypes = AutoMapper.Mapper.Map<FinDocType>(loadADInvoiceTypes);
                    tblInvoiceTypes = objInvoiceTypes;
                    DBO.Provider.Update(ref tblInvoiceTypes);
                }
                else
                {
                    FinDocType objInvoiceTypes = AutoMapper.Mapper.Map<FinDocType>(updatedInvoiceTypes);
                    tblInvoiceTypes = objInvoiceTypes;
                    DBO.Provider.Create(ref tblInvoiceTypes);
                }
            }
            catch(Exception ex)
            {
      
            }
        }
        #endregion
    }
}