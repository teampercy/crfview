﻿using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Accounting.AccountingCollections.PrintInvoice.ViewModels
{
    public class PrintInvoiceVM
    {
        public PrintInvoiceVM()
        {

            GetClientList = CommonBindList.GetClientIdList();
            InvoiceCycleList = CommonBindList.GetInvoiceCycleList();
            
        }

        public IEnumerable<SelectListItem> GetClientList { get; set; }
        public IEnumerable<SelectListItem> InvoiceCycleList { get; set; }
        public ADVWCLIENTCOLLFINDOCEMAIL objADvwClientCollFinDocEmail { get;set; }
        public bool chkAllClient { get; set; }
        public string ClientId { get; set; }
        public string RbtPrintOption { get; set; }
        public string InvoiceCycleId { get; set; }

        public string RbtPrintOptionEmail { get; set; }
        public bool chkInvoiceOnly { get; set; }
        public bool chkExcludeInvoice { get; set; }
    }
}
