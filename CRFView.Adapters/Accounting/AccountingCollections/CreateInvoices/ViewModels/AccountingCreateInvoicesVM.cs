﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Accounting.AccountingCollections.CreateInvoices.ViewModels
{
    public class AccountingCreateInvoicesVM
    {
        public AccountingCreateInvoicesVM()
        {

            objAdAccountingCreateInvoices = new AdAccountingCreateInvoices();
            GetClientList = CommonBindList.GetClientIdList();
            InvoiceCycleList = CommonBindList.GetInvoiceCycleList();

        }
        public IEnumerable<SelectListItem> GetClientList { get; set; }
        public IEnumerable<SelectListItem> InvoiceCycleList { get; set; }
        public AdAccountingCreateInvoices objAdAccountingCreateInvoices { get; set; }
        public string ClientId { get; set; }
        public int Id { get; set; }
        public bool chkAllClient { get; set; }
        public string FromDate { get; set; }
        public string ThruDate { get; set; }
        public string InvoiceCycle { get; set; }



    }
}
