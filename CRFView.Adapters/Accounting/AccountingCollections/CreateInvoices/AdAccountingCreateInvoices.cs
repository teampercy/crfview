﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.Providers;
using CRFView.Adapters.Accounting.AccountingLiens.DeleteInvoiceRun.ViewModels;
using CRFView.Adapters.Accounting.AccountingCollections.CreateInvoices.ViewModels;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Accounting.AccountingCollections.CreateInvoices
{
    public class AdAccountingCreateInvoices
    {
        #region Properties
        public string ClientId { get; set; }
        public string FromDate { get; set; }
        public string ThruDate { get; set; }
        public string InvoiceCycle { get; set; }
        #endregion

        #region CRUD       
        public static bool TestMethod(AdAccountingCreateInvoices obj_AdCreateInvoice)
        {
            try
            {
                var objCreateInvoice = new cview_Accounting_CreateInvoice();
                objCreateInvoice.ClientId = obj_AdCreateInvoice.ClientId;
                objCreateInvoice.FROMDATE = obj_AdCreateInvoice.FromDate;
                objCreateInvoice.THRUDATE = obj_AdCreateInvoice.ThruDate;
                objCreateInvoice.INVOICECYCLE = obj_AdCreateInvoice.InvoiceCycle;

                DBO.Provider.ExecNonQuery(objCreateInvoice);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public static int SaveINvoice(AccountingCreateInvoicesVM objAccountingCreateInvoicesVM)
        {
            try
            {
                
                var sp_obj = new uspbo_Accounting_CreateLienInvoice();

              

                sp_obj.FROMDATE = objAccountingCreateInvoicesVM.FromDate.ToString();
                sp_obj.THRUDATE = objAccountingCreateInvoicesVM.ThruDate.ToString();
                sp_obj.InvoiceCycle = objAccountingCreateInvoicesVM.Id.ToString();

                DBO.Provider.ExecNonQuery(sp_obj);


                
                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int DeleteINvoice(AccountingDeleteInvoicesVM objAccountingDeleteInvoicesVM)
        {
            try
            {

                var sp_obj = new uspbo_Accounting_UndoInvoiceRun();



                sp_obj.InvoiceFrom = objAccountingDeleteInvoicesVM.FromDate.ToString();
                sp_obj.InvoiceTo = objAccountingDeleteInvoicesVM.ThruDate.ToString();
                sp_obj.InvoiceCycle = objAccountingDeleteInvoicesVM.Id.ToString();

                DBO.Provider.ExecNonQuery(sp_obj);



                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }
        public static int CreateNewINvoice(AccountingCreateInvoicesVM objAccountingCreateInvoicesVM)
        {
            try
            {

                var sp_obj = new cview_Accounting_CreateInvoice();



                sp_obj.FROMDATE = objAccountingCreateInvoicesVM.FromDate.ToString();
                sp_obj.THRUDATE = objAccountingCreateInvoicesVM.ThruDate.ToString();
                sp_obj.INVOICECYCLE = objAccountingCreateInvoicesVM.Id.ToString();
                if (objAccountingCreateInvoicesVM.chkAllClient == false)
                {
                    sp_obj.ClientId = objAccountingCreateInvoicesVM.ClientId;
                }

                DBO.Provider.ExecNonQuery(sp_obj);

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }
        #endregion
    }
}
