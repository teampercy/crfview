﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Accounting.AccountingCollections.AuditList.AuditListVM
{
    public class AuditOptionsVM
    {

        public AuditOptionsVM()
        {
            InvoiceCycleList = CommonBindList.GetInvoiceCycleList();
        }
        #region Properties
        public string ReportType { get; set; }
            public string PrintOptions { get; set; }
            public string ClientFilter { get; set; }
            public bool ChkInvoiceCycle { get; set; }
            public bool ChkRemitDate { get; set; }
            public bool ChkTrxDate { get; set; }
            public bool ChkDebtorState { get; set; }   
            public int ClientId { get; set; }
            public string TrxFromDate { get; set; }
            public string TrxThruDate { get; set; }
        //public DateTime RemitFromDate { get; set; }
        //public DateTime RemitThruDate { get; set; }
        public string RemitFromDate { get; set; }
        public string RemitThruDate { get; set; }
        public string InvoiceCycle { get; set; }
            public string DebtorState { get; set; }
            public string AllOrIndvdClient { get; set; }
            public IEnumerable<SelectListItem> ServiceCodes { get; set; }
            public IEnumerable<SelectListItem> InvoiceCycleList { get; set; } 
            public IEnumerable<SelectListItem> States { get; set; }      
        #endregion
    }
}
