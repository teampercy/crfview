﻿using CRF.BLL;
using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.CRFDB.VIEWS;
using CRF.BLL.Providers;
using CRFView.Adapters.Accounting.AccountingCollections.PaymentEntry.PaymentEntryVM;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Accounting.AccountingCollections.PaymentEntry
{
    public static class AdAccountLedger
    {
        #region CRUD

        public static bool CheckAccountExists(string AccountId)
        {
            try
            {
                var sp_obj = new cview_Accounts_GetAccountInfo();
                sp_obj.AccountId = AccountId;
                var resultDs = DBO.Provider.GetDataSet(sp_obj);
                DataTable dtAccount = resultDs.Tables[0];
                if (dtAccount.Rows.Count == 1)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch(Exception ex)
            {
                return false;
            }
        } 

        public static ADvwAccountList GetAccountDetail (string AccountId)
        {
            var sp_obj = new cview_Accounts_GetAccountInfo();
            sp_obj.AccountId = AccountId;
            var resultDs = DBO.Provider.GetDataSet(sp_obj);
            DataTable dtAccount = resultDs.Tables[0];
            List<ADvwAccountList> ObjAccount = new List<ADvwAccountList>();
            if (dtAccount.Rows.Count >= 1)
            {
                ObjAccount = (from DataRow dr in dtAccount.Rows
                              select new ADvwAccountList()
                              {
                                  FileNumber = ((dr["FileNumber"] != DBNull.Value ? dr["FileNumber"].ToString() : "")),
                                  ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                  ReferalDate = ((dr["ReferalDate"] != DBNull.Value ? Convert.ToDateTime(dr["ReferalDate"]) : DateTime.MinValue)),
                                  TotAsgAmt = ((dr["TotAsgAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotAsgAmt"]) : 0)),
                                  AccountNo = (dr["AccountNo"] != DBNull.Value ? dr["AccountNo"].ToString() : ""),
                                  LastServiceDate = (dr["LastServiceDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastServiceDate"]) : DateTime.MinValue),
                                  TotCollFeeAmt = ((dr["TotCollFeeAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotCollFeeAmt"]) : 0)),
                                  DebtorName = ((dr["DebtorName"] != DBNull.Value ? dr["DebtorName"].ToString() : "")),
                                  LastPaymentDate = ((dr["LastPaymentDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastPaymentDate"]) : DateTime.MinValue)),
                                  TotAdjAmt = ((dr["TotAdjAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotAdjAmt"]) : 0)),
                                  ContactName = ((dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : "")),
                                  LastTrustDate = ((dr["LastTrustDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastTrustDate"]) : DateTime.MinValue)),
                                  TotPmtAmt = ((dr["TotPmtAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotPmtAmt"]) : 0)),
                                  AddressLine1 = ((dr["AddressLine1"] != DBNull.Value ? dr["AddressLine1"].ToString() : "")),
                                  CloseDate = ((dr["CloseDate"] != DBNull.Value ? Convert.ToDateTime(dr["CloseDate"]) : DateTime.MinValue)),
                                  TotBalAmt = ((dr["TotBalAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotBalAmt"]) : 0)),
                                  AddressLine2 = ((dr["AddressLine2"] != DBNull.Value ? dr["AddressLine2"].ToString() : "")),
                                  NextContactDate = ((dr["NextContactDate"] != DBNull.Value ? Convert.ToDateTime(dr["NextContactDate"]) : DateTime.MinValue)),
                                  TotAttyExpAmt = ((dr["TotAttyExpAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotAttyExpAmt"]) : 0)),
                                  City = ((dr["City"] != DBNull.Value ? dr["City"].ToString() : "")),
                                  State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),
                                  PostalCode = ((dr["PostalCode"] != DBNull.Value ? dr["PostalCode"].ToString() : "")),
                                  PhoneNo = ((dr["PhoneNo"] != DBNull.Value ? dr["PhoneNo"].ToString() : "")),
                                  PhoneNoExt = ((dr["PhoneNoExt"] != DBNull.Value ? dr["PhoneNoExt"].ToString() : "")),
                                  CellPhone = ((dr["CellPhone"] != DBNull.Value ? dr["CellPhone"].ToString() : "")),
                                  Fax = ((dr["Fax"] != DBNull.Value ? dr["Fax"].ToString() : "")),
                                  Email = ((dr["Email"] != DBNull.Value ? dr["Email"].ToString() : "")),
                                  TaxId = ((dr["TaxId"] != DBNull.Value ? dr["TaxId"].ToString() : "")),
                                  FeeCode = ((dr["FeeCode"] != DBNull.Value ? dr["FeeCode"].ToString() : "")),
                                  CollectionStatus = ((dr["CollectionStatus"] != DBNull.Value ? dr["CollectionStatus"].ToString() : "")),
                                  Location = ((dr["Location"] != DBNull.Value ? dr["Location"].ToString() : "")),
                                  Age = ((dr["Age"] != DBNull.Value ? Convert.ToInt32(dr["Age"]) : 0)),
                                  DebtAccountNote = ((dr["DebtAccountNote"] != DBNull.Value ? dr["DebtAccountNote"].ToString() : "")),
                                  TotalDue = ((dr["TotalDue"] != DBNull.Value ? Convert.ToDecimal(dr["TotalDue"]) : 0)),
                                  TotCostExpAmt = ((dr["TotCostExpAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotCostExpAmt"]) : 0)),
                                  TotCostRcvAmt = ((dr["TotCostRcvAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotCostRcvAmt"]) : 0)),
                                  TotIntAmt = ((dr["TotIntAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotIntAmt"]) : 0)),
                                  TotIntRcvAmt = ((dr["TotIntRcvAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotIntRcvAmt"]) : 0)),

                              }).ToList();

            }
            return ObjAccount.First();
        }

        public static List<LedgerVM> GetLedgerDetail(string AccountId)
        {
            var sp_obj = new cview_Accounts_GetAccountInfo();
            sp_obj.AccountId = AccountId;
            var resultDs = DBO.Provider.GetDataSet(sp_obj);
            DataTable dtLedger = resultDs.Tables[1];
            dtLedger.DefaultView.Sort = "TrxDate DESC";
            List<LedgerVM> ObjLedger = new List<LedgerVM>();
            if (dtLedger.Rows.Count >= 1)
            {
                ObjLedger = (from DataRow dr in dtLedger.Rows
                              select new LedgerVM()
                              {
                                  DebtAccountLedgerId = ((dr["DebtAccountLedgerId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAccountLedgerId"]) : 0)),
                                  DebtorAccountId = ((dr["DebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAccountId"]) : 0)),
                                  TrxDate = ((dr["TrxDate"] != DBNull.Value ? Convert.ToDateTime(dr["TrxDate"]).ToShortDateString() : "")),
                                  RemitDate = ((dr["RemitDate"] != DBNull.Value ? Convert.ToDateTime(dr["RemitDate"]).ToShortDateString() : "")),
                                  LedgerType = ((dr["LedgerType"] != DBNull.Value ? dr["LedgerType"].ToString() : "")),
                                  TrxAmt = ((dr["TrxAmt"] != DBNull.Value ? dr["TrxAmt"].ToString() : "")),
                                  AsgAmt = ((dr["AsgAmt"] != DBNull.Value ? dr["AsgAmt"].ToString() : "")),
                                  AdjAmt = ((dr["AdjAmt"] != DBNull.Value ? dr["AdjAmt"].ToString() : "")),
                                  AsgRcvAmt = ((dr["AsgRcvAmt"] != DBNull.Value ? dr["AsgRcvAmt"].ToString() : "")),
                                  FeeRcvAmt = ((dr["FeeRcvAmt"] != DBNull.Value ? dr["FeeRcvAmt"].ToString() : "")),
                                  IntRcvAmt = ((dr["IntRcvAmt"] != DBNull.Value ? dr["IntRcvAmt"].ToString() : "")),
                                  CostRcvAmt = ((dr["CostRcvAmt"] != DBNull.Value ? dr["CostRcvAmt"].ToString() : "")),
                                  AttyRcvAmt = ((dr["AttyRcvAmt"] != DBNull.Value ? dr["AttyRcvAmt"].ToString() : "")),
                                  AttyOwedAmt = ((dr["AttyOwedAmt"] != DBNull.Value ? dr["AttyOwedAmt"].ToString() : "")),
                                  CostRcvAmtOnPIF = ((dr["CostRcvAmtOnPIF"] != DBNull.Value ? dr["CostRcvAmtOnPIF"].ToString() : "")),
                                  OvrPmtAmt = ((dr["OvrPmtAmt"] != DBNull.Value ? dr["OvrPmtAmt"].ToString() : "")),
                                  RebateAmt = ((dr["RebateAmt"] != DBNull.Value ? dr["RebateAmt"].ToString() : "")),
                                  Rate = ((dr["Rate"] != DBNull.Value ? dr["Rate"].ToString() : "")),
                                  ExchangeRate = ((dr["ExchangeRate"] != DBNull.Value ? dr["ExchangeRate"].ToString() : "")),
                                  PmtAmt = ((dr["PmtAmt"] != DBNull.Value ? dr["PmtAmt"].ToString() : "")),
                                  FeeAmt = ((dr["FeeAmt"] != DBNull.Value ? dr["FeeAmt"].ToString() : "")),
                                  PrincDueAmt = ((dr["PrincDueAmt"] != DBNull.Value ? dr["PrincDueAmt"].ToString() : "")),
                                  IsAdvanceCC = ((dr["IsAdvanceCC"] != DBNull.Value ? dr["IsAdvanceCC"].ToString() : "")),
                                  InvoicedDate = ((dr["InvoicedDate"] != DBNull.Value ? Convert.ToDateTime(dr["InvoicedDate"]).ToShortDateString() : "")),
                                  SubLedgerType = ((dr["SubLedgerType"] != DBNull.Value ? dr["SubLedgerType"].ToString() : "")),
                                  CollectionStatus = ((dr["CollectionStatus"] != DBNull.Value ? dr["CollectionStatus"].ToString() : "")),
                                  DeskNum = ((dr["DeskNum"] != DBNull.Value ? dr["DeskNum"].ToString() : "")),

                              }).ToList();

            }
            return ObjLedger;
        }

        //public static int SaveNewAdjustment(NewAdjustmentVM ObjNewAdjustmentVM)
        //{
        //    try
        //    {


        //        AutoMapper.Mapper.CreateMap<AdDedtAccountLedger, DebtAccountLedger>();
        //        ITable tblAdDedtAccountLedger;
        //        DebtAccountLedger objDedtAccountLedger;


        //           AdDedtAccountLedger objAdDedtAccountLedger = new AdDedtAccountLedger();

        //           objAdDedtAccountLedger.DebtAccountId = ObjNewAdjustmentVM.DebtAccountId;
        //           objAdDedtAccountLedger.EnteredByUserId = Globals.CurrentUser.Id;
        //           objAdDedtAccountLedger.SubLedgerTypeId = ObjNewAdjustmentVM.SubLedgerTypeId;

        //           var mytable = new SubLedgerType();
        //           ITable IAcc = mytable;
        //           DBO.Provider.Read(Convert.ToString(ObjNewAdjustmentVM.SubLedgerTypeId), ref IAcc);
        //           mytable = (SubLedgerType)IAcc;

        //           objAdDedtAccountLedger.SubLedgerType = mytable.SubLedgerType;
        //           objAdDedtAccountLedger.TrxDate = ObjNewAdjustmentVM.TrxDate;
        //           objAdDedtAccountLedger.RemitDate = ObjNewAdjustmentVM.TrxDate;
        //           objAdDedtAccountLedger.ReceiptDate = ObjNewAdjustmentVM.TrxDate;
        //           objAdDedtAccountLedger.RemitDate = null;


        //           objAdDedtAccountLedger.TrxAmt = 0;
        //           objAdDedtAccountLedger.AdjAmt = Convert.ToDecimal(ObjNewAdjustmentVM.distAdjAmt);
        //           objAdDedtAccountLedger.PmtAmt = 0;
        //           objAdDedtAccountLedger.Rate = 0;
        //           objAdDedtAccountLedger.FeeAmt = 0;

        //           objAdDedtAccountLedger.ReceiptDate = ObjNewAdjustmentVM.TrxDate;
        //           objAdDedtAccountLedger.IsRemitted = false;
        //           // objAdDedtAccountLedger.InvoicedDate = "";


        //           if (objAdDedtAccountLedger.DebtAccountId < 1)
        //           {

        //               objDedtAccountLedger = AutoMapper.Mapper.Map<DebtAccountLedger>(objAdDedtAccountLedger);
        //               tblAdDedtAccountLedger = (ITable)objDedtAccountLedger;
        //               DBO.Provider.Create(ref tblAdDedtAccountLedger);
        //           }

        //           else
        //           {

        //               objDedtAccountLedger = AutoMapper.Mapper.Map<DebtAccountLedger>(objAdDedtAccountLedger);
        //               tblAdDedtAccountLedger = (ITable)objDedtAccountLedger;
        //               DBO.Provider.Update(ref tblAdDedtAccountLedger);
        //           }


        //           //If.DebtAccountLedgerId < 1 Then
        //           //    DBO.Create(myitem)
        //           //Else
        //           //    DBO.Update(myitem)
        //           //End If




        //           //var sp_obj = new cview_Accounting_VoidInvoice();

        //           //sp_obj.ClientFinDocId = ObjAccountingVoidInvoicesVM.FinDocId;

        //           //DBO.Provider.ExecNonQuery(sp_obj);
        //           //result = "Completed";

        //           return 1;
        //       }
        //       catch (Exception ex)
        //       {
        //           ErrorLog.SendErrorToText(ex);
        //           return 0;
        //       }
        //}






        #endregion

        #region Local Methods
        #endregion
    }
}
