﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Accounting.AccountingCollections.PaymentEntry.PaymentEntryVM
{
    public class NewPaymentVM
    {

        public NewPaymentVM()
        {
            Locationlist = CommonBindList.GetLocationDetails();
        }
        public string rbtVal { get; set; }
        public string rbtStatementStatus { get; set; }
        //public bool rbtPMTA { get; set; }

        //public bool rbtPMTC { get; set; }

        public string currPrincDue { get; set; }

        public string distAsgRcvAmt { get; set; }

        public string newPrincDue { get; set; }

        public string currFeeDue { get; set; }

        public string distFeeRcv { get; set; }

        public string newFeeDue { get; set; }

        public string currCostDue { get; set; }

        public string distCostAmt { get; set; }

        public string newCostDue { get; set; }

        public string currIntDue { get; set; }

        public string DistIntAmt { get; set; }

        public string newIntDue { get; set; }

        public string distAttyRcv { get; set; }

        public string AttOwedAmt { get; set; }

        public string CostRcvAmtOnPIF { get; set; }

        public string OvrPmtAmt { get; set; }

        public string RebateAmt { get; set; }

        public string NewBalance { get; set; }

        public string applied { get; set; }

        public string undistributed { get; set; }

        public string feerate { get; set; }

        public string billable { get; set; }

        public string feeearned { get; set; }

        //public bool rbtPAY { get; set; }

        //public bool rbtPCF { get; set; }

        //public bool rbtPIF { get; set; }

        //public bool rbtPPF { get; set; }

        //public bool rbtPPO { get; set; }

        //public bool rbtPTD { get; set; }

        //public bool rbtLIF { get; set; }

        //public bool rbtLPP { get; set; }

        //public bool rbtSIF { get; set; }

        //public bool rbtCC { get; set; }

        //public bool rbtINT { get; set; }

        //public bool rbtNSF { get; set; }

        public int DebtAccountId { get; set; }
        public int DebtAccountLedgerId { get; set; }

        public int Id { get; set; }
        public IEnumerable<SelectListItem> Locationlist { get; set; }

        public AdDedtAccountLedger objAdDedtAccountLedger { get; set; }

        public decimal[,] m_amt1 { get; set; }
        public string Location { get; set; }
        public bool EditNewPayment { get; set; }
        public bool ShowExchangeRate { get; set; }
        public bool showRemitDate { get; set; }
        public bool IsAutodistribute { get; set; }







    }
}
