﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Accounting.AccountingCollections.PaymentEntry.PaymentEntryVM
{
    public class LedgerVM
    {
        #region Properties

        public int DebtAccountLedgerId { get; set; }

        
        public IEnumerable<string> DebtAccountId { get; set; }
        public int DebtorAccountId { get; set; }
        public string TrxDate { get; set; }
        public string RemitDate { get; set; }
        public string LedgerType { get; set; }
        public string TrxAmt { get; set; }
        public string AsgAmt { get; set; }
        public string AdjAmt { get; set; }
        public string AsgRcvAmt { get; set; }
        public string FeeRcvAmt { get; set; }
        public string IntRcvAmt { get; set; }
        public string CostRcvAmt { get; set; }
        public string AttyRcvAmt { get; set; }
        public string AttyOwedAmt { get; set; }
        public string CostRcvAmtOnPIF { get; set; }
        public string OvrPmtAmt { get; set; }
        public string RebateAmt { get; set; }
        public string Rate { get; set; }
        public string ExchangeRate { get; set; }
        public string PmtAmt { get; set; }
        public string FeeAmt { get; set; }
        public string PrincDueAmt { get; set; }
        public string IsAdvanceCC { get; set; }
        public string InvoicedDate { get; set; }
        public string SubLedgerType { get; set; }
        public string CollectionStatus { get; set; }
        public string DeskNum { get; set; }

        public AdDedtAccountLedger ObjAdDedtAccountLedger { get; set; }



        #endregion

        #region CRUD

        #endregion
    }
}
