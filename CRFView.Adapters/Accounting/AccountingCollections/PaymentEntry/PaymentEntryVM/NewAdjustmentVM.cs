﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Accounting.AccountingCollections.PaymentEntry.PaymentEntryVM
{
    public class NewAdjustmentVM
    {

        public NewAdjustmentVM()
        {
            SubLedgerTypelist = CommonBindList.GetSubLedgerType();
        }

        public string RemainingBal { get; set; }

        public DateTime TrxDate { get; set; }

        public int SubLedgerTypeId { get; set; }

        public string distAdjAmt { get; set; }

        public int DebtAccountId { get; set; }
        public int DebtAccountLedgerId { get; set; }
        public bool EditNewAdjustment { get; set; }
        public bool btnAdjustmentDelete{ get; set; }

        public IEnumerable<SelectListItem> SubLedgerTypelist { get; set; }

        public AdDedtAccountLedger objAdDedtAccountLedger { get; set; }

    }
}
