﻿
using AutoMapper.Internal;
using CRF.BLL;
using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.CRFDB.VIEWS;
using CRF.BLL.Providers;
using CRFView.Adapters.Accounting.AccountingCollections.PaymentEntry.PaymentEntryVM;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.UI.WebControls;

namespace CRFView.Adapters.Accounting.AccountingCollections.PaymentEntry
{
    public class AdDedtAccountLedger
    {
        #region Properties
        public int DebtAccountLedgerId { get; set; }
        public int DebtAccountId { get; set; }
        public int ClientFinDocId { get; set; }
        public int BatchId { get; set; }
        public int LedgerTypeId { get; set; }
        public string LedgerType { get; set; }
        public string CollectionStatus { get; set; }
        public string TrxCode { get; set; }
        public System.DateTime TrxDate { get; set; }
        public System.DateTime ReceiptDate { get; set; }
        public System.DateTime? RemitDate { get; set; }
        public System.DateTime? InvoicedDate { get; set; }
        public decimal TrxAmt { get; set; }
        public decimal AsgAmt { get; set; }
        public decimal AsgRcvAmt { get; set; }
        public decimal FeeRcvAmt { get; set; }
        public decimal AdjAmt { get; set; }
        public decimal IntRcvAmt { get; set; }
        public decimal CostRcvAmt { get; set; }
        public decimal AttyRcvAmt { get; set; }
        public decimal Rate { get; set; }
        public decimal PmtAmt { get; set; }
        public decimal BillableAmt { get; set; }
        public decimal IntSharedAmt { get; set; }
        public decimal FeeAmt { get; set; }
        public decimal BilledAmt { get; set; }
        public decimal RemitAmt { get; set; }
        public decimal OvrPmtAmt { get; set; }
        public decimal AttyFeeAmt { get; set; }
        public decimal RcvByAgencyAmt { get; set; }
        public decimal RcvByClientAmt { get; set; }
        public decimal FeeByAgencyAmt { get; set; }
        public decimal FeeByClientAmt { get; set; }
        public decimal DueFromClientAmt { get; set; }
        public decimal RemitToClientAmt { get; set; }
        public decimal PrincDueAmt { get; set; }
        public decimal TotalDueAmt { get; set; }
        public bool IsRemitted { get; set; }
        public bool IsRcvdbyClient { get; set; }
        public bool IsOmitFromStmt { get; set; }
        public bool IsOmitFromCashRpt { get; set; }
        public bool IsInterestShared { get; set; }
        public bool IsOnHold { get; set; }
        public bool IsNSF { get; set; }
        public string Reference { get; set; }
        public string LedgerNote { get; set; }
        public int DeskNumId { get; set; }
        public string DeskNum { get; set; }
        public string StmtGrouping { get; set; }
        public int EnteredByUserId { get; set; }
        public string EnteredByUserCode { get; set; }
        public decimal AttyOwedAmt { get; set; }
        public bool IsConverted { get; set; }
        public string InvoiceBreak { get; set; }
        public decimal CollectFeeAmt { get; set; }
        public decimal FeeByAgencyAmtCF { get; set; }
        public decimal FeeByClientAmtCF { get; set; }
        public int AltKeyId { get; set; }
        public string SubLedgerType { get; set; }
        public bool IsAdvanceCC { get; set; }
        public int SubLedgerTypeId { get; set; }
        public decimal ExchangeRate { get; set; }
        public decimal CostRcvAmtOnPIF { get; set; }
        public string BankName { get; set; }
        public string BankAcctNum { get; set; }
        public string CheckMaker { get; set; }
        public decimal IntShareRate { get; set; }
        public decimal RebateAmt { get; set; }
        public decimal TotBalAmt { get; set; }


        //custom prop

        public decimal[,] m_amt = new decimal[4, 30];

        //public decimal[,] Amt { get => m_amt; set => m_amt = value; }






        #endregion

        #region CRUD

        //Read Details from database
        public static AdDedtAccountLedger ReadDedtAccountLedger(int id)
        {
            DebtAccountLedger myentity = new DebtAccountLedger();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (DebtAccountLedger)myentityItable;
            AutoMapper.Mapper.CreateMap<DebtAccountLedger, AdDedtAccountLedger>();
            return (AutoMapper.Mapper.Map<AdDedtAccountLedger>(myentity));
        }





        public static NewAdjustmentVM LoadNewAdjustmentDetails(string FileNumber, AdDedtAccountLedger objAdDedtAccountLedger, string TotBalAmt)
        {
            NewAdjustmentVM objNewAdjustmentVM = new NewAdjustmentVM();

            objNewAdjustmentVM.DebtAccountId = Convert.ToInt32(FileNumber);
            objNewAdjustmentVM.RemainingBal = TotBalAmt;
            objNewAdjustmentVM.distAdjAmt = objAdDedtAccountLedger.AdjAmt.ToString();
            objNewAdjustmentVM.SubLedgerTypeId = objAdDedtAccountLedger.SubLedgerTypeId;
            objNewAdjustmentVM.TrxDate = objAdDedtAccountLedger.TrxDate;
            objNewAdjustmentVM.btnAdjustmentDelete = true;
            if (objAdDedtAccountLedger.DebtAccountLedgerId < 1)
            {
                objNewAdjustmentVM.btnAdjustmentDelete = false;
                objNewAdjustmentVM.SubLedgerTypeId = -1;
            }
          
            // SetUpPage()

            return objNewAdjustmentVM;

        }

        public static NewPaymentVM LoadNewPayment(string DebtAccountId,string location,int DebtAccountLedgerId,bool IsEdit)
        {

            decimal[,] m_amt = new decimal[4, 30];
            
            var myacc = new DebtAccount();
            ITable IAcc = myacc;
            DBO.Provider.Read(DebtAccountId, ref IAcc);
            myacc = (DebtAccount)IAcc;
            var myds = new DataSet();
            var mysproc = new cview_Accounts_GetAccountInfo();
            mysproc.AccountId = DebtAccountId;
            myds = DBO.Provider.GetDataSet(mysproc);
            DataTable dt = myds.Tables[0];
            NewPaymentVM ObjNewPaymentVM = new NewPaymentVM();
            ObjNewPaymentVM.m_amt1 = new decimal[4, 30];
            vwAccountList MYACCOUNT = new vwAccountList();

            TableView vwAccount = new TableView(myds.Tables[0]);
            TableView vwDebtAccountLedger = new TableView(myds.Tables[1]);
            DataTable dtcontract = myds.Tables[5];
            DebtAccountLedger myledger = new DebtAccountLedger();
            if (IsEdit == true)
            {
               
                //myledger = ReadDedtAccountLedger(DebtAccountLedgerId);
                ITable a = myledger;
                CRF.BLL.Globals.DBO.Read(DebtAccountLedgerId.ToString(), ref a);
                myledger = (DebtAccountLedger)a;
               // DAL.Provider.DAL.Read(myaccountinfo.AccountView.RowItem("ContractId"), mycontract)
                // Globals.DBO.Read(myview.RowItem("DebtAccountLedgerId"), myledger)
            }
            else
            {
               // DebtAccountLedger myledger = new DebtAccountLedger();

                myledger.DebtAccountId = Convert.ToInt32(DebtAccountId);
                myledger.LedgerType = "PMTA";
                myledger.ReceiptDate = DateTime.Today;
                myledger.DeskNum = location;
            }
           
            //int id = Convert.ToInt32(vwDebtAccountLedger.get_RowItem("DebtAccountLedgerId"));

           // AdDedtAccountLedger objDebtAccountLedger = new AdDedtAccountLedger();
           // objDebtAccountLedger = ReadDedtAccountLedger(id);
           ObjNewPaymentVM.objAdDedtAccountLedger = ReadDedtAccountLedger(0);

            //


            //DebtAccountLedger objDebtAccountLedger = new DebtAccountLedger();
            //ITable myentityItable1 = objDebtAccountLedger;
            //DBO.Provider.Read(Convert.ToString(vwDebtAccountLedger.get_RowItem("DebtAccountLedgerId")), ref myentityItable1);
            //objDebtAccountLedger = (DebtAccountLedger)myentityItable1;
            //AutoMapper.Mapper.CreateMap<DebtAccountLedger, ADDebtAccountLedger>();
            //ADDebtAccountLedger tempADDebtAccountLedger = AutoMapper.Mapper.Map<ADDebtAccountLedger>(objDebtAccountLedger);


            ClientContract mycontract = new ClientContract();
            ITable myentityItable2 = mycontract;
            DBO.Provider.Read(Convert.ToString(vwAccount.get_RowItem("ContractId")), ref myentityItable2);
            mycontract = (ClientContract)myentityItable2;
            AutoMapper.Mapper.CreateMap<ClientContract, ADClientContract>();
            ADClientContract temp = AutoMapper.Mapper.Map<ADClientContract>(mycontract);

            //lblExchange.Visible = True
            ObjNewPaymentVM.ShowExchangeRate = true;
            ObjNewPaymentVM.objAdDedtAccountLedger.ExchangeRate = myledger.ExchangeRate;

            if(mycontract.IsIntlClient == false)
            {
                ObjNewPaymentVM.ShowExchangeRate = false;
                ObjNewPaymentVM.objAdDedtAccountLedger.ExchangeRate = 1;
                //ObjNewPaymentVM.objAdDedtAccountLedger.ExchangeRate = 1;
            }

            //if (Convert.ToInt32( vwDebtAccountLedger.get_RowItem("DebtAccountLedgerId")) < 1)
            if (myledger.DebtAccountLedgerId < 1)
            {
                if (dtcontract.Rows.Count == 0)
                {
                    ObjNewPaymentVM.objAdDedtAccountLedger.IsAdvanceCC = false;
                    ObjNewPaymentVM.objAdDedtAccountLedger.DeskNumId = 0;

                }
                else
                {
                    ObjNewPaymentVM.objAdDedtAccountLedger.IsAdvanceCC = Convert.ToBoolean(dtcontract.Rows[0]["AdvanceCC"]);
                    ObjNewPaymentVM.objAdDedtAccountLedger.DeskNumId = Convert.ToInt32(vwAccount.get_RowItem("locationId"));

                }
                if (myledger.LedgerType == "PMTA")
                {
                    myledger.RemitDate = DateTime.Now.AddDays(14) ;
                }
                else
                {
                    myledger.RemitDate = DateTime.Today;
                }

                ObjNewPaymentVM.objAdDedtAccountLedger.ReceiptDate = DateTime.Today;
            }
            else
            {
                ObjNewPaymentVM.objAdDedtAccountLedger.IsAdvanceCC = myledger.IsAdvanceCC;
                ObjNewPaymentVM.objAdDedtAccountLedger.DeskNumId = myledger.DeskNumId;
                ObjNewPaymentVM.objAdDedtAccountLedger.ReceiptDate = myledger.TrxDate;
            }

            ObjNewPaymentVM.objAdDedtAccountLedger.IsOmitFromStmt = myledger.IsOmitFromStmt;
            

            if (myledger.IsOmitFromStmt == true)
            {
                ObjNewPaymentVM.showRemitDate = false;
            }
            else if(myledger.RemitDate == DateTime.MinValue)
            {
                ObjNewPaymentVM.showRemitDate = true;
                //Me.RemitDate.CustomFormat = " "
                ObjNewPaymentVM.objAdDedtAccountLedger.RemitDate = null;
            }
            else if (myledger.RemitDate != DateTime.MinValue)
            {
                ObjNewPaymentVM.showRemitDate = true;
                ObjNewPaymentVM.objAdDedtAccountLedger.RemitDate = myledger.RemitDate;
            }
            else
            {
                ObjNewPaymentVM.showRemitDate = false; 
            }



            ////ObjNewPaymentVM.objAdDedtAccountLedger.BankName = objDebtAccountLedger.BankName;
            ////ObjNewPaymentVM.objAdDedtAccountLedger.BankAcctNum = objDebtAccountLedger.BankAcctNum;
            ////ObjNewPaymentVM.objAdDedtAccountLedger.CheckMaker = objDebtAccountLedger.CheckMaker;

            AdDedtAccountLedger objAdDedtAccountLedger = new AdDedtAccountLedger();

            //objAdDedtAccountLedger.m_amt[1, 1] = (MYACCOUNT.TotAsgAmt + MYACCOUNT.TotAdjAmt - MYACCOUNT.TotAsgRcvAmt) + objDebtAccountLedger.AsgRcvAmt;
            ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[1, 1] = Convert.ToDecimal( vwAccount.get_RowItem("TotAsgAmt")) + Convert.ToDecimal( vwAccount.get_RowItem("TotAdjAmt")) - Convert.ToDecimal(vwAccount.get_RowItem("TotAsgRcvAmt")) + myledger.AsgRcvAmt;
            ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[1, 2] = Convert.ToDecimal(vwAccount.get_RowItem("totcollfeeamt")) - Convert.ToDecimal(vwAccount.get_RowItem("totcollfeercvamt")) + myledger.FeeRcvAmt;
            ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[1, 3] = Convert.ToDecimal(vwAccount.get_RowItem("totlegalexpamt")) - Convert.ToDecimal(vwAccount.get_RowItem("totcostrcvamt")) + myledger.CostRcvAmt;
            ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[1, 4] = Convert.ToDecimal(vwAccount.get_RowItem("totintamt")) - Convert.ToDecimal(vwAccount.get_RowItem("totintrcvamt")) + myledger.IntRcvAmt;
            //ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[1, 5] = Convert.ToDecimal(vwAccount.get_RowItem("totattyexpamt")) - Convert.ToDecimal(vwAccount.get_RowItem("totattyrcvamt")) + myledger.AttyRcvAmt;

            ObjNewPaymentVM.m_amt1[1, 1] = Convert.ToDecimal(vwAccount.get_RowItem("TotAsgAmt")) + Convert.ToDecimal(vwAccount.get_RowItem("TotAdjAmt")) - Convert.ToDecimal(vwAccount.get_RowItem("TotAsgRcvAmt")) + myledger.AsgRcvAmt;
            ObjNewPaymentVM.m_amt1[1, 2] = Convert.ToDecimal(vwAccount.get_RowItem("totcollfeeamt")) - Convert.ToDecimal(vwAccount.get_RowItem("totcollfeercvamt")) + myledger.FeeRcvAmt;
            ObjNewPaymentVM.m_amt1[1, 3] = Convert.ToDecimal(vwAccount.get_RowItem("totlegalexpamt")) - Convert.ToDecimal(vwAccount.get_RowItem("totcostrcvamt")) + myledger.CostRcvAmt;
            ObjNewPaymentVM.m_amt1[1, 4] = Convert.ToDecimal(vwAccount.get_RowItem("totintamt")) - Convert.ToDecimal(vwAccount.get_RowItem("totintrcvamt")) + myledger.IntRcvAmt;
            ObjNewPaymentVM.m_amt1[1, 5] = Convert.ToDecimal(vwAccount.get_RowItem("totattyexpamt")) - Convert.ToDecimal(vwAccount.get_RowItem("totattyrcvamt")) + myledger.AttyRcvAmt;

            if (Convert.ToBoolean(vwAccount.get_RowItem("IsSmallClaims")) == true)
            {
                string mysql = "Select * from DebtAccountSmallclaims where DebtAccountId = " + DebtAccountId;
                TableView myview = DBO.Provider.GetTableView(mysql);

                if(myview.Count > 0 && Convert.ToInt32(myview.get_RowItem("JudgmentAmt")) > 0)
                {
                    ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[1, 1] = Convert.ToDecimal(myview.get_RowItem("JudgmentAmt")) - Convert.ToDecimal(myview.get_RowItem("Credits"));
                    ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[1, 3] = Convert.ToDecimal(myview.get_RowItem("Costs")) + Convert.ToDecimal(myview.get_RowItem("PostCosts"));
                    //m_amt(1, 4) = (myview.RowItem("SubTotal2") * 0.1 / 365) * (DateDiff(DateInterval.Day, myview.RowItem("JudgmentDate"), Today))
                    decimal SubTotal2 = (Convert.ToDecimal(myview.get_RowItem("SubTotal2")));
                    int d = (DateTime.Compare(Convert.ToDateTime(myview.get_RowItem("JudgmentDate")), DateTime.Today));
                    DateTime JudgmentDate = Convert.ToDateTime(myview.get_RowItem("JudgmentDate"));
                    TimeSpan final = DateTime.Today.Subtract(JudgmentDate);
                    int x = (int)final.TotalDays;
                    double tot = Convert.ToDouble(SubTotal2) * (0.1) / 365 ;
                    ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[1, 4] = Convert.ToDecimal(tot) * x;


                }

                //var CollectionActionsView = DBO.Provider.GetTableView(mysql);
                //DataTable dt = CollectionActionsView.ToTable();
            }

            ObjNewPaymentVM.currPrincDue = Convert.ToString(ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[1, 1]);
            ObjNewPaymentVM.currFeeDue = Convert.ToString(ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[1, 2]);
            ObjNewPaymentVM.currCostDue = Convert.ToString(ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[1, 3]);
            ObjNewPaymentVM.currIntDue = Convert.ToString(ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[1, 4]);

            ObjNewPaymentVM.distAsgRcvAmt = Convert.ToString(myledger.AsgRcvAmt);
            ObjNewPaymentVM.distFeeRcv =  Convert.ToString(myledger.FeeRcvAmt);
            ObjNewPaymentVM.DistIntAmt =  Convert.ToString(myledger.IntRcvAmt);
            ObjNewPaymentVM.distCostAmt = Convert.ToString(myledger.CostRcvAmt); 
            ObjNewPaymentVM.distAttyRcv = Convert.ToString(myledger.AttyRcvAmt);
            ObjNewPaymentVM.AttOwedAmt = Convert.ToString(myledger.AttyOwedAmt);
            ObjNewPaymentVM.CostRcvAmtOnPIF = Convert.ToString(myledger.CostRcvAmtOnPIF);
            ObjNewPaymentVM.OvrPmtAmt = Convert.ToString(myledger.OvrPmtAmt);
            ObjNewPaymentVM.RebateAmt = Convert.ToString(myledger.RebateAmt);

            ObjNewPaymentVM.objAdDedtAccountLedger.TrxAmt = myledger.TrxAmt;
            ObjNewPaymentVM.feeearned = Convert.ToString(myledger.FeeAmt);

           ObjNewPaymentVM.objAdDedtAccountLedger.Reference = myledger.Reference;
            ObjNewPaymentVM.objAdDedtAccountLedger.IsOmitFromStmt = myledger.IsOmitFromStmt;

            if(myledger.LedgerType == "PMTC")
            {
                ObjNewPaymentVM.rbtVal = "rbtPMTC";
            }
            else
            {
                ObjNewPaymentVM.rbtVal = "rbtPMTA";
            }

            ObjNewPaymentVM.rbtStatementStatus = "rbtPAY";


            switch (myledger.CollectionStatus)
            {
                case "PAY":
                    ObjNewPaymentVM.rbtStatementStatus = "rbtPAY";
                    break;

                case "LIF":
                    ObjNewPaymentVM.rbtStatementStatus = "rbtLIF";
                    break;

                case "PIF":
                    ObjNewPaymentVM.rbtStatementStatus = "rbtPIF";
                    break;

                case "LPP":
                    ObjNewPaymentVM.rbtStatementStatus = "rbtLPP";
                    break;

                case "SIF":
                    ObjNewPaymentVM.rbtStatementStatus = "rbtSIF";
                    break;

                case "PCF":
                    ObjNewPaymentVM.rbtStatementStatus = "rbtPCF";
                    break;

                case "PPF":
                    ObjNewPaymentVM.rbtStatementStatus = "rbtPPF";
                    break;

                case "PPO":
                    ObjNewPaymentVM.rbtStatementStatus = "rbtPPO";
                    break;

                case "PTD":
                    ObjNewPaymentVM.rbtStatementStatus = "rbtPTD";
                    break;

                case "INT":
                    ObjNewPaymentVM.rbtStatementStatus = "rbtINT";
                    break;

                case "CC":
                    ObjNewPaymentVM.rbtStatementStatus = "rbtCC";
                    break;

                case "NSF":
                    ObjNewPaymentVM.rbtStatementStatus = "rbtNSF";
                    break;

            }


            TotalDistributions(ObjNewPaymentVM);
            ObjNewPaymentVM.Id = objAdDedtAccountLedger.DebtAccountLedgerId;


            /////TotalDistributions

            //ObjNewPaymentVM.newPrincDue = (Convert.ToDouble(ObjNewPaymentVM.currPrincDue) - Convert.ToDouble(ObjNewPaymentVM.distAsgRcvAmt)).ToString();
            //ObjNewPaymentVM.newFeeDue = (Convert.ToDouble(ObjNewPaymentVM.currFeeDue) - Convert.ToDouble(ObjNewPaymentVM.distFeeRcv)).ToString();
            //ObjNewPaymentVM.newCostDue = (Convert.ToDouble(ObjNewPaymentVM.currCostDue) - Convert.ToDouble(ObjNewPaymentVM.distCostAmt)).ToString();
            //ObjNewPaymentVM.newIntDue = (Convert.ToDouble(ObjNewPaymentVM.currIntDue) - Convert.ToDouble(ObjNewPaymentVM.DistIntAmt)).ToString();

            //ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 1] = Convert.ToDecimal(ObjNewPaymentVM.distAsgRcvAmt);
            //ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 2] = Convert.ToDecimal(ObjNewPaymentVM.distFeeRcv);
            //ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 3] = Convert.ToDecimal(ObjNewPaymentVM.distCostAmt);
            //ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 4] = Convert.ToDecimal(ObjNewPaymentVM.DistIntAmt);
            //ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 5] = Convert.ToDecimal(ObjNewPaymentVM.distAttyRcv);
            //ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 6] = Convert.ToDecimal(ObjNewPaymentVM.AttOwedAmt);
            //ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 7] = Convert.ToDecimal(ObjNewPaymentVM.CostRcvAmtOnPIF);
            //ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 8] = Convert.ToDecimal(ObjNewPaymentVM.OvrPmtAmt);



            //objAdDedtAccountLedger.m_amt[3, 1] = ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[1, 1] - ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 1];
            //objAdDedtAccountLedger.m_amt[3, 2] = ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[1, 2] - ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 2];
            //objAdDedtAccountLedger.m_amt[3, 3] = ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[1, 3] - ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 3];
            //objAdDedtAccountLedger.m_amt[3, 4] = ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[1, 4] - ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 4];
            //objAdDedtAccountLedger.m_amt[3, 10] = objAdDedtAccountLedger.m_amt[3, 1] + objAdDedtAccountLedger.m_amt[3, 2] + objAdDedtAccountLedger.m_amt[3, 3] + objAdDedtAccountLedger.m_amt[3, 4];

            //objAdDedtAccountLedger.m_amt[2, 10] = ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 1] + ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 2] + ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 3] + ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 4] + ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 7] + ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 8];
            //objAdDedtAccountLedger.m_amt[3, 11] = ObjNewPaymentVM.objAdDedtAccountLedger.TrxAmt - ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 10];

            //ObjNewPaymentVM.applied = Convert.ToString(ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 10]);
            //ObjNewPaymentVM.NewBalance = Convert.ToString(objAdDedtAccountLedger.m_amt[3, 10]);
            //ObjNewPaymentVM.undistributed =  Convert.ToString(ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[3, 11]);
            //ObjNewPaymentVM.billable = Convert.ToString(objAdDedtAccountLedger.m_amt[2, 1]) + Convert.ToString(objAdDedtAccountLedger.m_amt[2, 2]);

            //if (ObjNewPaymentVM.billable != "00" && ObjNewPaymentVM.feeearned != "0.00")
            //{
            //    decimal MFEE = Convert.ToDecimal(ObjNewPaymentVM.feeearned);
            //    decimal MBILL = Convert.ToDecimal(ObjNewPaymentVM.billable);
            //    decimal MRATE = (MFEE / MBILL) * 100;
            //    ObjNewPaymentVM.feerate = Convert.ToString(MRATE);
            //    ObjNewPaymentVM.billable = Convert.ToString(MBILL);
            //}
            //if (ObjNewPaymentVM.feerate == null)
            //{
            //    ObjNewPaymentVM.feerate = "0";
            //}
            //decimal billableVal;
            ////11 / 29 / 11
            //decimal feerate = Convert.ToDecimal(ObjNewPaymentVM.feerate);
            //decimal.TryParse(ObjNewPaymentVM.billable, out billableVal);

            //Decimal a = feerate * billableVal;
            //decimal s = a / 100;
            //ObjNewPaymentVM.feeearned = s.ToString();
            //ObjNewPaymentVM.billable = billableVal.ToString();
            ////11 / 29 / 11



            ////ObjNewPaymentVM.feeearned = (Convert.ToDouble(ObjNewPaymentVM.feerate) * Convert.ToDouble(ObjNewPaymentVM.billable).ToString() / 100);

            ////Me.undistributed.ErrorMessage = ""

            //if (Convert.ToInt32(ObjNewPaymentVM.undistributed) != 0)
            //{
            //    //Me.undistributed.IsValid = False
            //    //Me.undistributed.Validate()
            //}



            return ObjNewPaymentVM;

        }
        public static AdDedtAccountLedger EditDetails(string DebtAccountId)
        {
            AdDedtAccountLedger objDebtAccountLedger = new AdDedtAccountLedger();
            

            var mysproc = new cview_Accounts_GetAccountInfo();
            mysproc.AccountId = DebtAccountId;
            var myds = new DataSet();
            myds = DBO.Provider.GetDataSet(mysproc);
            DataTable dt = myds.Tables[0];
            NewPaymentVM ObjNewPaymentVM = new NewPaymentVM();
            ObjNewPaymentVM.m_amt1 = new decimal[4, 30];
            vwAccountList MYACCOUNT = new vwAccountList();

            TableView vwAccount = new TableView(myds.Tables[0]);

            objDebtAccountLedger.TotBalAmt = Convert.ToDecimal(vwAccount.get_RowItem("TotBalAmt"));

            return objDebtAccountLedger;
        }

        public static NewPaymentVM AutoDistribution(NewPaymentVM ObjNewPaymentVM,int myaccountid, decimal TrxAmt,string Location,int DebtAccountLedgerId, bool IsAutodistribute)
        {
           
            ObjNewPaymentVM = LoadNewPayment(myaccountid.ToString(), Location, DebtAccountLedgerId,IsAutodistribute);
            decimal[,] m_amt = new decimal[4, 30];
            AdDedtAccountLedger objAdDedtAccountLedger = new AdDedtAccountLedger();

            ObjNewPaymentVM.m_amt1[2, 1] = 0;
            ObjNewPaymentVM.m_amt1[2, 2] = 0;
            ObjNewPaymentVM.m_amt1[2, 3] = 0;
            ObjNewPaymentVM.m_amt1[2, 4] = 0;
            ObjNewPaymentVM.m_amt1[2, 5] = 0;
            ObjNewPaymentVM.m_amt1[2, 6] = 0;
            ObjNewPaymentVM.m_amt1[2, 7] = 0;
            ObjNewPaymentVM.m_amt1[2, 8] = 0;

            int i = 0;
            decimal m_trxamt = TrxAmt;
            decimal x = TrxAmt;
            for ( i = 1; i <= 4; i++)
            {
                if (ObjNewPaymentVM.m_amt1[1, i] > 0)
                {
                    if ((ObjNewPaymentVM.m_amt1[1, i] <= x) && (x != 0.00m))
                    {
                        ObjNewPaymentVM.m_amt1[2, i] = ObjNewPaymentVM.m_amt1[1, i];
                        x = x - ObjNewPaymentVM.m_amt1[2, i];
                    }
                    else
                    {
                        ObjNewPaymentVM.m_amt1[2, i] = x;
                        x = x - ObjNewPaymentVM.m_amt1[2, i];
                    }
                }
            }

            decimal cBillableAmt;
            cBillableAmt = ObjNewPaymentVM.m_amt1[2, 1] + ObjNewPaymentVM.m_amt1[2, 2];

            string mysql = "Select dbo.fn_Debt_GetCommAmt(" + myaccountid + ", " + cBillableAmt + ") as CommAmt from DebtAccount Where debtaccountid = " + myaccountid;
            //TableView myview = DBO.Provider.GetTableView(mysql);
            DataSet myview = DBO.Provider.GetDataSet(mysql);
            DataTable dtmyview = myview.Tables[0];
            decimal mCommAmt = Convert.ToDecimal(dtmyview.Rows[0]["CommAmt"]);
            ObjNewPaymentVM.feeearned = mCommAmt.ToString();

            ObjNewPaymentVM.distAsgRcvAmt = ObjNewPaymentVM.m_amt1[2, 1].ToString();
            ObjNewPaymentVM.distFeeRcv = ObjNewPaymentVM.m_amt1[2, 2].ToString();
            ObjNewPaymentVM.distCostAmt = ObjNewPaymentVM.m_amt1[2, 3].ToString();
            ObjNewPaymentVM.DistIntAmt = ObjNewPaymentVM.m_amt1[2, 4].ToString();

            TotalDistributions(ObjNewPaymentVM);

            return ObjNewPaymentVM;
        }

        public static NewPaymentVM TotalDistributions(NewPaymentVM ObjNewPaymentVM)
        {

            ///TotalDistributions
            AdDedtAccountLedger objAdDedtAccountLedger = new AdDedtAccountLedger();
             

            ObjNewPaymentVM.newPrincDue = (Convert.ToDouble(ObjNewPaymentVM.currPrincDue) - Convert.ToDouble(ObjNewPaymentVM.distAsgRcvAmt)).ToString();
            ObjNewPaymentVM.newFeeDue = (Convert.ToDouble(ObjNewPaymentVM.currFeeDue) - Convert.ToDouble(ObjNewPaymentVM.distFeeRcv)).ToString();
            ObjNewPaymentVM.newCostDue = (Convert.ToDouble(ObjNewPaymentVM.currCostDue) - Convert.ToDouble(ObjNewPaymentVM.distCostAmt)).ToString();
            ObjNewPaymentVM.newIntDue = (Convert.ToDouble(ObjNewPaymentVM.currIntDue) - Convert.ToDouble(ObjNewPaymentVM.DistIntAmt)).ToString();

            objAdDedtAccountLedger.m_amt[2, 1] = Convert.ToDecimal(ObjNewPaymentVM.distAsgRcvAmt);
            objAdDedtAccountLedger.m_amt[2, 2] = Convert.ToDecimal(ObjNewPaymentVM.distFeeRcv);
            objAdDedtAccountLedger.m_amt[2, 3] = Convert.ToDecimal(ObjNewPaymentVM.distCostAmt);
            objAdDedtAccountLedger.m_amt[2, 4] = Convert.ToDecimal(ObjNewPaymentVM.DistIntAmt);
            objAdDedtAccountLedger.m_amt[2, 5] = Convert.ToDecimal(ObjNewPaymentVM.distAttyRcv);
            objAdDedtAccountLedger.m_amt[2, 6] = Convert.ToDecimal(ObjNewPaymentVM.AttOwedAmt);
            objAdDedtAccountLedger.m_amt[2, 7] = Convert.ToDecimal(ObjNewPaymentVM.CostRcvAmtOnPIF);
            objAdDedtAccountLedger.m_amt[2, 8] = Convert.ToDecimal(ObjNewPaymentVM.OvrPmtAmt);


            objAdDedtAccountLedger.m_amt[3, 1] = ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[1, 1] - objAdDedtAccountLedger.m_amt[2, 1];
            objAdDedtAccountLedger.m_amt[3, 2] = ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[1, 2] - objAdDedtAccountLedger.m_amt[2, 2];
            objAdDedtAccountLedger.m_amt[3, 3] = ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[1, 3] - objAdDedtAccountLedger.m_amt[2, 3];
            objAdDedtAccountLedger.m_amt[3, 4] = ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[1, 4] - objAdDedtAccountLedger.m_amt[2, 4];
            objAdDedtAccountLedger.m_amt[3, 10] = objAdDedtAccountLedger.m_amt[3, 1] + objAdDedtAccountLedger.m_amt[3, 2] + objAdDedtAccountLedger.m_amt[3, 3] + objAdDedtAccountLedger.m_amt[3, 4];

            objAdDedtAccountLedger.m_amt[2, 10] = ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 1] + ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 2] + ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 3] + ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 4] + ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 7] + ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 8];
            objAdDedtAccountLedger.m_amt[3, 11] = ObjNewPaymentVM.objAdDedtAccountLedger.TrxAmt - ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 10];

            ObjNewPaymentVM.applied = Convert.ToString(ObjNewPaymentVM.objAdDedtAccountLedger.m_amt[2, 10]);
            ObjNewPaymentVM.NewBalance = Convert.ToString(objAdDedtAccountLedger.m_amt[3, 10]);
            ObjNewPaymentVM.undistributed = Convert.ToString(objAdDedtAccountLedger.m_amt[3, 11]);
            ObjNewPaymentVM.billable = Convert.ToString(objAdDedtAccountLedger.m_amt[2, 1]) + Convert.ToString(objAdDedtAccountLedger.m_amt[2, 2]);
            decimal billable = objAdDedtAccountLedger.m_amt[2, 1] + objAdDedtAccountLedger.m_amt[2, 2];
            ObjNewPaymentVM.billable = billable.ToString();
            decimal feeearned = Convert.ToDecimal(ObjNewPaymentVM.feeearned);
            decimal MRATE = 0;
            if ((billable != 0) && (feeearned != 0))
            {
                decimal MFEE = Convert.ToDecimal(ObjNewPaymentVM.feeearned);
                decimal MBILL = Convert.ToDecimal(ObjNewPaymentVM.billable);
                 MRATE = (MFEE / MBILL) * 100;
                ObjNewPaymentVM.feerate = Convert.ToString(MRATE);
            }
            decimal k = MRATE * billable / 100;
            ObjNewPaymentVM.feeearned = k.ToString();
            
           
            if (ObjNewPaymentVM.undistributed != "0")
            {
                //ObjNewPaymentVM.undistributed.IsValid = false;
                //ObjNewPaymentVM.undistributed.Validate()
            }
            else
            {
                //ObjNewPaymentVM.undistributed.IsValid = False
            }
            return ObjNewPaymentVM;

        }


        public static int DeleteAdjustment(int DebtAccountLedgerId)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<AdDedtAccountLedger, DebtAccountLedger>();
                ITable tblDedtAccountLedger;
                
                DebtAccountLedger objDebtAccountLedger;



                AdDedtAccountLedger objAdDedtAccountLedger = ReadDedtAccountLedger(DebtAccountLedgerId);
                objDebtAccountLedger = AutoMapper.Mapper.Map<DebtAccountLedger>(objAdDedtAccountLedger);
                tblDedtAccountLedger = (ITable)objDebtAccountLedger;
                DBO.Provider.Delete(ref tblDedtAccountLedger);
                return 1;

            }
            catch (Exception ex)
            {
                return 0;
            }

        }

        public static int NsfORReverse(AdDedtAccountLedger objAdDedtAccountLedger, bool IsNSF, bool IsReverse)
        {
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

            var mysproc = new cview_Accounts_GetAccountInfo();
            mysproc.AccountId = objAdDedtAccountLedger.DebtAccountId.ToString();
           DataSet myds = DBO.Provider.GetDataSet(mysproc);
            DataTable dt = myds.Tables[0];
            AutoMapper.Mapper.CreateMap<AdDedtAccountLedger, DebtAccountLedger>();
            ITable tblDebtAccountLedger;
            DebtAccountLedger objDebtAccountLedger;


            AdDedtAccountLedger OBJAdDedtAccountLedger = AdDedtAccountLedger.ReadDedtAccountLedger(0);
            OBJAdDedtAccountLedger.RemitDate = null;
            OBJAdDedtAccountLedger.IsOmitFromStmt = true;
            objDebtAccountLedger = AutoMapper.Mapper.Map<DebtAccountLedger>(OBJAdDedtAccountLedger);

            tblDebtAccountLedger = (ITable)objDebtAccountLedger;
            DBO.Provider.Update(ref tblDebtAccountLedger);

            OBJAdDedtAccountLedger.DebtAccountId = objAdDedtAccountLedger.DebtAccountId;
            OBJAdDedtAccountLedger.EnteredByUserId = user.Id;
            OBJAdDedtAccountLedger.TrxDate = DateTime.Today;
            OBJAdDedtAccountLedger.TrxAmt = objAdDedtAccountLedger.TrxAmt * -1;
            OBJAdDedtAccountLedger.LedgerType = objAdDedtAccountLedger.LedgerType;
            OBJAdDedtAccountLedger.RemitDate = null;
            OBJAdDedtAccountLedger.IsOmitFromStmt = true;
            OBJAdDedtAccountLedger.AsgAmt = 0;
            OBJAdDedtAccountLedger.AsgRcvAmt = objAdDedtAccountLedger.AsgRcvAmt * -1;
            OBJAdDedtAccountLedger.FeeRcvAmt = objAdDedtAccountLedger.FeeRcvAmt * -1;
            OBJAdDedtAccountLedger.IntRcvAmt = objAdDedtAccountLedger.IntRcvAmt * -1;
            OBJAdDedtAccountLedger.CostRcvAmt = objAdDedtAccountLedger.CostRcvAmt * -1;
            OBJAdDedtAccountLedger.AttyRcvAmt = objAdDedtAccountLedger.AttyRcvAmt * -1;
            OBJAdDedtAccountLedger.AttyOwedAmt = objAdDedtAccountLedger.AttyOwedAmt * -1;
            OBJAdDedtAccountLedger.Rate = objAdDedtAccountLedger.Rate;
            OBJAdDedtAccountLedger.FeeAmt = objAdDedtAccountLedger.FeeAmt * -1;
           

            ADClientContract mycontract = new ADClientContract();
            mycontract = ADClientContract.ReadClientContractType(Convert.ToInt32(dt.Rows[0]["ContractId"]));
            
            if(mycontract.IsInterestSharedAsPrinc == true){
                OBJAdDedtAccountLedger.IntSharedAmt = (objAdDedtAccountLedger.IntRcvAmt * OBJAdDedtAccountLedger.Rate) / 100;
                OBJAdDedtAccountLedger.IntShareRate = objAdDedtAccountLedger.Rate;
            }
            else if (mycontract.IsInterestShared == true)
            {
                OBJAdDedtAccountLedger.IntShareRate = mycontract.InterestShareRate;
                OBJAdDedtAccountLedger.IntSharedAmt = (objAdDedtAccountLedger.IntRcvAmt * OBJAdDedtAccountLedger.IntShareRate) / 100;
                
            }
            else
            {
                OBJAdDedtAccountLedger.IntShareRate = 100.0m;
                OBJAdDedtAccountLedger.IntSharedAmt = objAdDedtAccountLedger.IntRcvAmt;
            }
            OBJAdDedtAccountLedger.IntSharedAmt = OBJAdDedtAccountLedger.IntSharedAmt * 1; ;
            OBJAdDedtAccountLedger.ExchangeRate = objAdDedtAccountLedger.ExchangeRate;
            OBJAdDedtAccountLedger.ReceiptDate = objAdDedtAccountLedger.ReceiptDate;
            OBJAdDedtAccountLedger.IsRemitted = false;
            OBJAdDedtAccountLedger.InvoicedDate = null;
            if (IsNSF == true)
            {
                OBJAdDedtAccountLedger.CollectionStatus = "NSF";
                OBJAdDedtAccountLedger.IsNSF = true;
            }
            else if(IsReverse == true)
            {
                OBJAdDedtAccountLedger.CollectionStatus = "REV";
              
            }
            OBJAdDedtAccountLedger.DeskNum = objAdDedtAccountLedger.DeskNum;
            OBJAdDedtAccountLedger.DeskNumId = objAdDedtAccountLedger.DeskNumId;
            OBJAdDedtAccountLedger.LedgerType = OBJAdDedtAccountLedger.LedgerType;

            objDebtAccountLedger = AutoMapper.Mapper.Map<DebtAccountLedger>(OBJAdDedtAccountLedger);

            tblDebtAccountLedger = (ITable)objDebtAccountLedger;
            DBO.Provider.Create(ref tblDebtAccountLedger);
            return 1;
        }
        #endregion



    }
}
