﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Accounting.AccountingCollections.DeleteInvoiceRuns.ViewModels
{
    public class AccountingDeleteInvoiceRunVM
    {

        public AccountingDeleteInvoiceRunVM()
        {
            // ClientCustomerList = new List<ADClientCustomer>();
            InvoiceCycleList = CommonBindList.GetInvoiceCycleList();

        }

        public IEnumerable<SelectListItem> InvoiceCycleList { get; set; }
        public int Id { get; set; }
        public string FromDate { get; set; }
        public string ThruDate { get; set; }
        public string InvoiceCycle { get; set; }



    }
}
