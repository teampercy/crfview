﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.Providers;
using CRFView.Adapters.Accounting.AccountingCollections.DeleteInvoiceRuns.ViewModels;
using CRFView.Adapters.Accounting.AccountingCollections.VoidInvoices.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADClientCollFinDocItem
    {
        #region Properties
        public int Id { get; set; }
        public int ClientFinDocId { get; set; }
        public string PriceCode { get; set; }
        public decimal Qty { get; set; }
        public decimal Price { get; set; }
        public string Description { get; set; }
        public decimal UnitPrice { get; set; }
        public decimal PmtAmt { get; set; }
        public decimal FeeAmt { get; set; }
        public decimal InterestShareAmt { get; set; }
        public decimal CourtCostAmt { get; set; }
        public decimal AttyFeeAmt { get; set; }
        public decimal RcvByAgencyAmt { get; set; }
        public decimal RcvByClientAmt { get; set; }
        public decimal FeeByAgencyAmt { get; set; }
        public decimal FeeByClientAmt { get; set; }
        public decimal DueFromClientAmt { get; set; }
        public decimal RemitToClientAmt { get; set; }

        #endregion

        #region CRUD

        public static int SaveVoidInvoices(AccountingVoidInvoicesVM ObjAccountingVoidInvoicesVM)
        {
            try
            {
                var result = "";

                var sp_obj = new cview_Accounting_VoidInvoice();

                sp_obj.ClientFinDocId = ObjAccountingVoidInvoicesVM.FinDocId;

                DBO.Provider.ExecNonQuery(sp_obj);
                result = "Completed";

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }


        public static int OkDeleteInvoiceRuns (AccountingDeleteInvoiceRunVM ObjAccountingDeleteInvoiceRunVM)
        {
            try
            {
                var result = "";

                var sp_obj = new cview_Accounting_UndoInvoiceRun();

                sp_obj.InvoiceFrom = ObjAccountingDeleteInvoiceRunVM.FromDate;
                sp_obj.InvoiceTo = ObjAccountingDeleteInvoiceRunVM.ThruDate;
                sp_obj.InvoiceCycle = ObjAccountingDeleteInvoiceRunVM.Id.ToString().Substring(0, 1);
                






                //Dim mysproc As New SPROCS.cview_Accounting_UndoInvoiceRun
                //With mysproc
                //    .InvoiceFrom = ProcessDialog.DateTimePicker1.Value.ToShortDateString
                //    .InvoiceTo = ProcessDialog.DateTimePicker2.Value.ToShortDateString
                //    .InvoiceCycle = Left(ProcessDialog.ExtendedDropDown1.Text, 1)
                //End With
                //Globals.DBO.ExecNonQuery(mysproc)


                DBO.Provider.ExecNonQuery(sp_obj);
                result = "Completed";

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }


        #endregion


    }
}
