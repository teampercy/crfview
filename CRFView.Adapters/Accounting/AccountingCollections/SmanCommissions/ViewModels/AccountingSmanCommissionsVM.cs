﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Accounting.AccountingCollections.SmanCommissions.ViewModels
{
    public class AccountingSmanCommissionsVM
    {

        public AccountingSmanCommissionsVM()
        {
            GetTeamList = CommonBindList.GetSalesTeamList();
        }

        public IEnumerable<SelectListItem> GetTeamList { get; set; }
        public string TeamId { get; set; }
        public bool chkAllSman { get; set; }

        public string DateTimePicker1 { get; set; }
        public string DateTimePicker2 { get; set; }
        public string RbtPrintOption { get; set; }
        public string Rbtbtn1 { get; set; }

    }
}
