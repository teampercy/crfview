﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Accounting.AccountingLiens.InvoiceRecaps.ViewModels
{
    public class InvoiceRecapsVM
    {


        public InvoiceRecapsVM()
        {
            InvoiceCycleList = CommonBindList.GetInvoiceCycleList();
        }

        public IEnumerable<SelectListItem> InvoiceCycleList { get; set; }
        public string FinDocCycle { get; set; }
        public string DateTimePicker1 { get; set; }
        public string DateTimePicker2 { get; set; }

        public string RbtPrintOption { get; set; }

    }
}
