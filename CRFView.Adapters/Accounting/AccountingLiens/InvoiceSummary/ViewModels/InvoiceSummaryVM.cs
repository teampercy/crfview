﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Accounting.AccountingLiens.InvoiceSummary.ViewModels
{
    public class InvoiceSummaryVM
    {

        public string DateTimePicker1 { get; set; }
        public string DateTimePicker2 { get; set; }

        public string RbtPrintOption { get; set; }

    }
}
