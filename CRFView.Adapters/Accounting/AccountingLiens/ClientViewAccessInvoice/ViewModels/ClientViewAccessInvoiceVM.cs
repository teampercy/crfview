﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Accounting.AccountingLiens.ClientViewAccessInvoice.ViewModels
{
    public class ClientViewAccessInvoiceVM
    {

        public ClientViewAccessInvoiceVM()
        {
         
            GetClientList = CommonBindList.GetClientIdList();
        }
        public IEnumerable<SelectListItem> GetClientList { get; set; }

        public ADvmJobInfo objADvmJobInfo { get; set; }
        public DateTime InvoiceFromDate { get; set; }
        public DateTime InvoiceThruDate { get; set; }
        public string ClientId { get; set; }
        public bool chkAllClient { get; set; }
        public string RbtPrintOption { get; set; }

    }
}
