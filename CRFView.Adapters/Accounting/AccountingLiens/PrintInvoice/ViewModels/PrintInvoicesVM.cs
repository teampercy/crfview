﻿using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Accounting.AccountingLiens.PrintInvoice.ViewModels
{
    public class PrintInvoicesVM
    {
        public PrintInvoicesVM()
        {
            InvoiceCycleList = CommonBindList.GetInvoiceCycleList();
            //GetTeamList = CommonBindList.GetSalesTeamList();
            GetClientList = CommonBindList.GetChangeClientCodeList();
        }

        

        public IEnumerable<SelectListItem> InvoiceCycleList { get; set; }


        public IEnumerable<SelectListItem> GetClientList { get; set; }
        //public IEnumerable<SelectListItem> GetTeamList { get; set; }
        public ADClientFinDoc objADClientFinDoc { get; set; }
        public string DateTimePicker1 { get; set; }
        public string DateTimePicker2 { get; set; }
        public string ClientId { get; set; }
        public string TeamId { get; set; }
        public bool chkAllSman { get; set; }
        public bool chkAllClient { get; set; }
        public string RbtPrintOption { get; set; }
        public bool chkCsv { get; set; }
        public string InvoiceCycleId { get; set; }

    }
}
