﻿using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Accounting.AccountingLiens.SmanCommission.ViewModels
{
    public class SmanCommissionVM
    {

        public SmanCommissionVM()
        {
            GetTeamList = CommonBindList.GetSalesTeamList();
            GetClientList = CommonBindList.GetClientIdList();
        }

        public IEnumerable<SelectListItem> GetClientList { get; set; }
        public IEnumerable<SelectListItem> GetTeamList { get; set; }
        public ADClientFinDoc objADClientFinDoc { get; set; }
        public string ClientId { get; set; }
        public string TeamId { get; set; }
        public bool chkAllSman { get; set; }
        public bool chkAllClient { get; set; }
        public string RbtPrintOption { get; set; }

    }
}
