﻿using CRFView.Adapters.Accounting.AccountingCollections.CreateInvoices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;


namespace CRFView.Adapters.Accounting.AccountingLiens.DeleteInvoiceRun.ViewModels
{
    public class AccountingDeleteInvoicesVM
    {
        public AccountingDeleteInvoicesVM()
        {
            objAdAccountingCreateInvoices = new AdAccountingCreateInvoices();
            // ClientCustomerList = new List<ADClientCustomer>();
            InvoiceCycleList = CommonBindList.GetInvoiceCycleList();

        }

        public IEnumerable<SelectListItem> InvoiceCycleList { get; set; }
        public AdAccountingCreateInvoices objAdAccountingCreateInvoices { get; set; }
        public int Id { get; set; }
        public string FromDate { get; set; }
        public string ThruDate { get; set; }
        public string InvoiceCycle { get; set; }



    }
}
