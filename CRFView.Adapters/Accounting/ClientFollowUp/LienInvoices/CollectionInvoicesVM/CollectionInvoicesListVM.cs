﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices.CollectionInvoicesVM
{
    public class CollectionInvoicesListVM
    {
        public SearchDivVM ObjSearchDiv { get; set; }
        public List<ADVWCLIENTCOLLFINDOCEMAIL> CollectionInvoices { get; set; }
    }
}
