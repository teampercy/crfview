﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using CRF.BLL.Clients;
using CRFView.Adapters.Clients.Client_Management;
using System.IO;
using System.Text.RegularExpressions;
using CRFView.Adapters.Accounting.AccountingLiens.ViewModels;
using CRFView.Adapters.Accounting.AccountingLiens.InvoiceSummary.ViewModels;
using CRF.BLL;
using CRFView.Adapters.Accounting.AccountingLiens.LienRevenue.ViewModels;
using CRFView.Adapters.Accounting.AccountingLiens.InvoiceRecaps.ViewModels;
using CRFView.Adapters.Accounting.AccountingLiens.SmanCommission.ViewModels;
using CRFView.Adapters.Accounting.AccountingLiens.PrintInvoice.ViewModels;
using System.Collections;
using static System.Net.Mime.MediaTypeNames;
using CRFView.Adapters.Accounting.AccountingCollections.PrintInvoice.ViewModels;

namespace CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices
{
    public class ADClientFinDoc
    {
        #region Properties
        public int Id { get; set; }
        public int BatchId { get; set; }
        public int ContractId { get; set; }
        public int FinDocTypeId { get; set; }
        public int FinDocStatusId { get; set; }
        public System.DateTime DocumentDate { get; set; }
        public string DocumentNo { get; set; }
        public string TrxPeriod { get; set; }
        public decimal TaxRate { get; set; }
        public decimal DiscRate { get; set; }
        public string Note { get; set; }
        public int TotQty { get; set; }
        public decimal TotalRevenue { get; set; }
        public decimal DocumentAmt { get; set; }
        public decimal TotRcvAmt { get; set; }
        public decimal TotAdjAmt { get; set; }
        public decimal TotalDue { get; set; }
        public string InvoiceName { get; set; }
        public string InvoiceAttn { get; set; }
        public string InvoiceAddr1 { get; set; }
        public string InvoiceAddr2 { get; set; }
        public string InvoiceCity { get; set; }
        public string InvoiceState { get; set; }
        public string InvoiceZip { get; set; }
        public string Reference { get; set; }
        public int CreatedBy { get; set; }
        public System.DateTime CreatedOn { get; set; }
        public int ModifiedBy { get; set; }
        public System.DateTime ModifiedOn { get; set; }
        public string ClientNumber { get; set; }
        public string SubBreak { get; set; }
        public string InvoiceContactTitle { get; set; }
        public string InvoiceEmail { get; set; }
        public string InvoicePeriod { get; set; }
        public System.DateTime InvoiceFromDate { get; set; }
        public System.DateTime InvoiceThruDate { get; set; }
        public string InvoiceCycle { get; set; }
        public bool IsEmailInvoice { get; set; }
        public int TaskHistoryId { get; set; }
        public bool IsPaid { get; set; }
        public int ClientId { get; set; }
        public int MainClientId { get; set; }
        public System.DateTime ReviewDate { get; set; }
        #endregion

        #region CRUD
        public static bool UpdatePaid(string Id)
        {
            try
            {

                //Dim myfindoc As New TABLES.ClientFinDoc
                //DAL.Provider.DAL.Read(myview.RowItem("ClientFinDocId"), myfindoc)
                //myfindoc.IsPaid = True
                //DAL.Provider.DAL.Update(myfindoc)
                var myfindoc = new ClientFinDoc();
                ITable ITable = myfindoc;
                HDS.DAL.Provider.DAL.Read(Id, ref ITable);
                myfindoc = (ClientFinDoc)ITable;
                myfindoc.IsPaid = true;
                ITable = myfindoc;
                HDS.DAL.Provider.DAL.Update(ref ITable);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool FlagUnpaid(string Id)
        {
            try
            {
                var myfindoc = new ClientFinDoc();
                ITable Imyfindoc = myfindoc;
                HDS.DAL.Provider.DAL.Read(Id, ref Imyfindoc);
                myfindoc = (ClientFinDoc)Imyfindoc;
                myfindoc.IsPaid = false;
                Imyfindoc = myfindoc;
                HDS.DAL.Provider.DAL.Update(ref Imyfindoc);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool ResendEmailUpdate(string Id, string PKID, string InvoiceEmail)
        {
            try
            {
                var myfindoc = new ClientFinDoc();
                ITable Imyfindoc = myfindoc;
                DBO.Provider.Read(Id, ref Imyfindoc);
                myfindoc = (ClientFinDoc)Imyfindoc;
                if (myfindoc.IsEmailInvoice == false)
                {
                    return false;
                }
                else
                {
                    myfindoc.InvoiceEmail = InvoiceEmail;
                    Imyfindoc = myfindoc;
                    DBO.Provider.Update(ref Imyfindoc);

                    //Dim mysql As String = "Update Report_HistoryRecipient"
                    //mysql += " Set email = '" & Me.invoiceemail.Text & "' "
                    //mysql += "where HistoryId ='" & myview.RowItem("HistoryId") & "' "
                    //DAL.Provider.DAL.ExecNonQuery(mysql)

                    var mysql = "update Report_HistoryRecipient ";
                    mysql += " Set email = '" + InvoiceEmail + "' ";
                    mysql += " where HistoryId ='" + PKID + "' ";
                    DBO.Provider.ExecNonQuery(mysql);

                    return true;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool UpdateClientFinDoc(string Id, bool ChkNewReview, bool ChkNewEmail,
                                            bool ChkDeleteReview, string TxtNewReviewDate, string TxtNewEmail)
        {
            try
            {
                var myfindoc = new ClientFinDoc();
                ITable ITable = myfindoc;
                HDS.DAL.Provider.DAL.Read(Id, ref ITable);
                myfindoc = (ClientFinDoc)ITable;
                if (ChkNewReview == true)
                {
                    myfindoc.ReviewDate = Convert.ToDateTime(TxtNewReviewDate);
                }
                if (ChkDeleteReview == true)
                {
                    myfindoc.ReviewDate = DateTime.MinValue;
                }
                if (ChkNewEmail == true)
                {
                    myfindoc.InvoiceEmail = TxtNewEmail;
                }
                ITable = myfindoc;
                HDS.DAL.Provider.DAL.Update(ref ITable);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public static int SaveVoidInvoice(VoidInvoiceVM objVoidInvoiceVM)
        {
            try
            {
                var result = "";

                //var sp_obj = new uspbo_Accounting_VoidClientFinDoc();
                var sp_obj = new uspbo_Accounting_VoidInvoice();

                sp_obj.ClientFinDocId = objVoidInvoiceVM.FinDocId;

                DBO.Provider.ExecNonQuery(sp_obj);
                result = "Completed";

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }


        public static string SaveInvoiceSummary(InvoiceSummaryVM objInvoiceSummaryVM)
        {
            string pdfPath = null;
            string sFromDate = null;
            string sThruDate = null;

            try
            {
                var result = "";

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var myds = new DataSet();
                var sp_obj = new uspbo_Accounting_GetInvoiceRecaps();
                sp_obj.DOCUMENTFrom = objInvoiceSummaryVM.DateTimePicker1;
                sp_obj.DOCUMENTTO = objInvoiceSummaryVM.DateTimePicker2;
                var myfilename = "Invoice Summary";
                // var myreportfilename = "Acc-Liens.xml";
                var myreportfilename = "Acc-Liens.xml";

                myds = CRF.BLL.Accounting.Liens.GetRecaps(user, sp_obj);

                myds.Tables[1].Columns.Add("ÏsCommissionable_1", typeof(string));            // add new column in myds.Tables[1]

                //foreach(DataRow row in myds.Tables[1].Rows)
                //{
                //   Convert.ToBoolean( row["IsCommisionable"]);
                //    {
                //        if(Convert.ToBoolean(row["IsCommisionable"]) == true)
                //        {
                //            row["IsCommisionable_1"] = "yes");
                //        }
                //    }
                //}


                foreach (DataRow row in myds.Tables[1].Rows)
                {

                    if (Convert.ToBoolean(row["IsCommissionable"]) == true)
                    {
                        row["ÏsCommissionable_1"] = "True";
                    }
                    else
                    {
                        row["ÏsCommissionable_1"] = "False";
                    }

                    //row["ÏsCommissionable_1"] = Convert.ToBoolean(row["IsCommissionable"]) == true ? "Yes" : "No";
                }

                myds.Tables[1].Columns.Remove("IsCommissionable");
                myds.Tables[1].Columns["ÏsCommissionable_1"].ColumnName = "IsCommissionable";



                var colType = myds.Tables[1].Columns["IsCommissionable"].DataType;

                CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;


                if (objInvoiceSummaryVM.RbtPrintOption == "optToPDF")
                {
                    mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                }

                TableView myview = new TableView(myds.Tables[1], myds.Tables[1].TableName);

                pdfPath = CRF.BLL.Accounting.Liens.ExportInvoiceSummary(user, ref myview, myfilename);
                //CRF.BLL.Accounting.Liens.PrintInvoiceSummary(user, myview, mymode);

                pdfPath = Globals.RenderC1Report(user, myds.Tables[1], myreportfilename, myfilename, mymode, "", sFromDate, sThruDate);



                DBO.Provider.ExecNonQuery(sp_obj);
                result = "Completed";

            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return "";
            }

            return pdfPath;
        }


        public static string PrintLienRevenue(LienRevenueVM objLienRevenueVM)
        {
            string pdfPath = null;
            string sFromDate = null;
            string sThruDate = null;

            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var Sp_OBJ = new uspbo_Accounting_GetLienRevenue();

                //string sql = "Select * from vwCollectionReportsList Where ReportId = " + Convert.ToInt32(objCollectionReportsVM.ddlReportId);
                TableView mytableview = DBO.Provider.GetTableView(Sp_OBJ);
                //DataSet myds = DBO.Provider.GetDataSet(sql);
                //DataTable mydt = myds.Tables[0];
                //Sp_OBJ.FromDate = objLienRevenueVM.objADClientFinDoc.InvoiceFromDate.ToShortDateString();
                //Sp_OBJ.ThruDate = objLienRevenueVM.objADClientFinDoc.InvoiceThruDate.ToShortDateString();

                if (objLienRevenueVM.chkAllClient == false)
                {
                    Sp_OBJ.ClientId = objLienRevenueVM.ClientId;
                }

                if (objLienRevenueVM.chkAllSman == false)
                {
                    Sp_OBJ.SmanNum = objLienRevenueVM.TeamId;
                }

                Sp_OBJ.FromDate = objLienRevenueVM.objADClientFinDoc.InvoiceFromDate.ToShortDateString();
                Sp_OBJ.ThruDate = objLienRevenueVM.objADClientFinDoc.InvoiceThruDate.ToShortDateString();
                sFromDate = objLienRevenueVM.objADClientFinDoc.InvoiceFromDate.ToShortDateString();
                sThruDate = objLienRevenueVM.objADClientFinDoc.InvoiceThruDate.ToShortDateString();

                TableView myview = CRF.BLL.Providers.DBO.Provider.GetTableView(Sp_OBJ);
                DataSet ds = DBO.Provider.GetDataSet(Sp_OBJ);
                DataTable mytbl = ds.Tables[0];


                if (objLienRevenueVM.RbtPrintOption == "Create CSV File")
                {
                    // mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                    string myfilename = "LienRevenue";
                    pdfPath = CRF.BLL.Accounting.Liens.ExportLienRevenue(user, ref myview, myfilename);
                }
                else
                {
                    CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;
                    if (objLienRevenueVM.RbtPrintOption == "Send To PDF")
                    {
                        mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                    }
                    string myreportfilename = Globals.CurrentUser.SettingsFolder + "LienReports.flxr";
                    string myreportname = "LienRevenueClientSum";

                   // pdfPath = Globals.RenderC1Report(user, mytbl, myreportfilename, myreportname, mymode, "", sFromDate, sThruDate);
                    pdfPath = CRF.BLL.LienView.Provider.PrintInvoice(user, mytbl, myreportfilename, myreportname, mymode, "", sFromDate, sThruDate);

                }
            }

            catch (Exception ex)
            {
                return "";
            }
            return pdfPath;
        }


        public static string SaveInvoiceRecap(InvoiceRecapsVM objInvoiceRecapsVM)
        {
            string pdfPath = null;
            string sFromDate = null;
            string sThruDate = null;

            try
            {
                var result = "";

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var myds = new DataSet();
                var sp_obj = new uspbo_Accounting_GetInvoiceRecaps();
                sp_obj.DOCUMENTFrom = objInvoiceRecapsVM.DateTimePicker1;
                sp_obj.DOCUMENTTO = objInvoiceRecapsVM.DateTimePicker2;
                sp_obj.INVOICECYCLE = Convert.ToString(objInvoiceRecapsVM.FinDocCycle);
                var myfilename = "Invoice Summary By Client";
                var myreportfilename = "Acc-Liens.xml";

                myds = CRF.BLL.Accounting.Liens.GetRecaps(user, sp_obj);



                CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;



                if (objInvoiceRecapsVM.RbtPrintOption == "optToPDF")
                {
                    mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                }

                //TableView myview = new TableView(myds.Tables[2], myds.Tables[2].TableName);

                // CRF.BLL.Accounting.Liens.PrintInvoiceSummaryByClient(user, myview, mymode);

                pdfPath = Globals.RenderC1Report(user, myds.Tables[2], myreportfilename, myfilename, mymode, "", sFromDate, sThruDate);



                DBO.Provider.ExecNonQuery(sp_obj);
                result = "Completed";

            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return "";
            }

            return pdfPath;
        }


        //public static string PrintSmanCommission(SmanCommissionVM objSmanCommissionVM)
        //{
        //    string pdfPath = null;
        //    string sFromDate = null;
        //    string sThruDate = null;

        //    try
        //    {
        //        CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

        //        var Sp_OBJ = new uspbo_Accounting_GetSmanComm();

        //        //string sql = "Select * from vwCollectionReportsList Where ReportId = " + Convert.ToInt32(objCollectionReportsVM.ddlReportId);
        //        TableView mytableview = DBO.Provider.GetTableView(Sp_OBJ);

        //        if (objSmanCommissionVM.chkAllClient == false)
        //        {
        //            Sp_OBJ.ClientId = objSmanCommissionVM.ClientId;
        //        }

        //        if (objSmanCommissionVM.chkAllSman == false)
        //        {
        //            Sp_OBJ.SmanNum = objSmanCommissionVM.TeamId;
        //        }

        //        Sp_OBJ.FromDate = objSmanCommissionVM.objADClientFinDoc.InvoiceFromDate.ToShortDateString();
        //        Sp_OBJ.ThruDate = objSmanCommissionVM.objADClientFinDoc.InvoiceThruDate.ToShortDateString();
        //        sFromDate = objSmanCommissionVM.objADClientFinDoc.InvoiceFromDate.ToShortDateString();
        //        sThruDate = objSmanCommissionVM.objADClientFinDoc.InvoiceThruDate.ToShortDateString();


        //        //Sp_OBJ.CustomSPName = Convert.ToString(mydt.Rows[0]["CustomSPName"]);
        //        //Sp_OBJ.ReportName = Convert.ToString(mydt.Rows[0]["ReportName"]);

        //        //TableView myview = DBO.Provider.GetTableView(Sp_OBJ);
        //        TableView myview = CRF.BLL.Providers.DBO.Provider.GetTableView(Sp_OBJ);
        //        DataSet ds = DBO.Provider.GetDataSet(Sp_OBJ);
        //        DataTable mytbl = ds.Tables[0];

        //        //TableView myview = CRF.BLL.Providers.DBO.Provider.GetTableView(Sp_OBJ);

        //        CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;
        //        if (objSmanCommissionVM.RbtPrintOption == "optToPDF")
        //        {
        //            mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
        //        }
        //        string myreportfilename = Globals.CurrentUser.SettingsFolder + "LienReports.xml";
        //        string myreportname = "SalesCommissionLien";


        //        if (objSmanCommissionVM.RbtPrintOption == "Create CSV File")
        //        {
        //            // mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
        //            // pdfPath = CRF.BLL.Accounting.Liens.Export.ExportLienRevenue(user, ref myview, myreportname);
        //            pdfPath = ADClientFinDoc.ProduceCSV(myview, myreportname);

        //        }
        //        else
        //        {

        //            pdfPath = Globals.RenderC1Report(user, mytbl, myreportfilename, myreportname, mymode, "", sFromDate, sThruDate);

        //        }
        //    }

        //    catch (Exception ex)
        //    {
        //        return "";
        //    }
        //    return pdfPath;
        //}


        //public static string PrintInvoicesDetails(PrintInvoiceVM objPrintInvoiceVM)
        //{
        //    string MYREPORTPATH = null;
        //    string sFromDate = null;
        //    string sThruDate = null;
        //    Boolean IsEmailRun ;
        //    object MYTASKDEF = new Tasks_Task();

        //    CRF.BLL.Reports.ClientReport myclrpt = new CRF.BLL.Reports.ClientReport();
        //    IsEmailRun = Convert.ToBoolean(objPrintInvoiceVM.RbtPrintOption);

        //    //CRFDB.Provider.GetItem(MYTASKDEF.TableObjectName, MYTASKDEF, "PREFIX", "COLLECTIONINVOICE")

        //    CRF.BLL.CRFDB.Provider.GetItem ((MYTASKDEF as Tasks_Task).TableObjectName, ref MYTASKDEF , "PREFIX", "COLLECTIONINVOICE");
        //    //Dim mysproc As New SPROCS.cview_Accounting_GetFinDocs
        //    CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

        //    var mybatchid = CRF.BLL.Reports.ClientReport.GetUniqueId();

        //    var Sp_OBJ = new uspbo_Accounting_GetClientFinDocs();

        //    Sp_OBJ.DOCUMENTFROM = objPrintInvoiceVM.objADvwClientCollFinDocEmail.DateTimePicker1;
        //    Sp_OBJ.DOCUMENTTO = objPrintInvoiceVM.DateTimePicker2;
        //    sFromDate = objPrintInvoiceVM.DateTimePicker1;
        //    sThruDate = objPrintInvoiceVM.DateTimePicker2;

        //    if (objPrintInvoiceVM.chkAllClient == false)
        //    {
        //        Sp_OBJ.CLIENTNUMBER = objPrintInvoiceVM.ClientId;
        //    }

        //    Sp_OBJ.INVOICECYCLE = objPrintInvoiceVM.InvoiceCycleId;


        //    TableView mytableview = DBO.Provider.GetTableView(Sp_OBJ);

        //    TableView myview = CRF.BLL.Providers.DBO.Provider.GetTableView(Sp_OBJ);
        //    DataSet myds = DBO.Provider.GetDataSet(Sp_OBJ);
        //    DataTable MYINVOICES = myds.Tables[0];

        //    DataView myview1 = new System.Data.DataView(myds.Tables[1]);
        //    DataView myviewNonLegal = new System.Data.DataView(myds.Tables[2]);
        //    DataView myviewLegal = new System.Data.DataView(myds.Tables[3]);
        //    DataView myviewCF = new System.Data.DataView(myds.Tables[4]);

        //    var sInvoiceType = "";
        //    bool bIsInvoiceOnly ;
        //    bool bIsExcludeInvoice;

        //    bIsInvoiceOnly  = objPrintInvoicesVM.chkInvoiceOnly;
        //    bIsExcludeInvoice = objPrintInvoicesVM



        //    //bIsInvoiceOnly = ProcessDialog.IsInvoiceOnly.Checked
        //    //    bIsExcludeInvoice = ProcessDialog.IsExcludeInvoice.Checked
        //    //    Dim mymode As BLL.Common.PrintMode = BLL.Common.PrintMode.PrintDirect
        //    //    If ProcessDialog.PrintOptions1.rbtToPDF.Checked Then
        //    //        mymode = BLL.Common.PrintMode.PrintToPDF
        //    //    End If
        //    //    mymode = GetPrintMode()


        //    return "";
        //}



        public static string PrintInvoicesDetails(PrintInvoicesVM objPrintInvoicesVM)
        {
            string MYREPORTPATH = null;
            string sFromDate = null;
           string sThruDate = null;
            string ClientId = "";


            //if (objPrintInvoicesVM.chkAllClient == true)
            //{
            //    objPrintInvoicesVM.ClientId = "0";
            //}
            if (objPrintInvoicesVM.chkAllClient == false)
            {
                 ClientId = objPrintInvoicesVM.ClientId;
            }
             else
            {
                ClientId = null;
            }

            if (objPrintInvoicesVM.RbtPrintOption == "optToPrinter")
            {
                objPrintInvoicesVM.RbtPrintOption = "1";
            }
            else if (objPrintInvoicesVM.RbtPrintOption == "optToPDF")
            {
                objPrintInvoicesVM.RbtPrintOption = "2";
            }
            else if (objPrintInvoicesVM.RbtPrintOption == "GenerateEmails")
            {
                objPrintInvoicesVM.RbtPrintOption = "3";
            }

            //if (objPrintInvoicesVM.chkCsv == true)
            //{
            //    objPrintInvoicesVM.chkCsv = true;
            //}


            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            string path = user.SettingsFolder;
            string ReportName = "Acc-Liens.flxr";


            string ParameterList = "ClientId" + ',' + "Invoicecycle" + ',' + "InvoiceFromDate" + ',' + "InvoiceToDate" + ',' + "ReportDestination";
            string ParameterValues = ClientId + ',' + objPrintInvoicesVM.InvoiceCycleId + ',' + objPrintInvoicesVM.DateTimePicker1 + ',' + objPrintInvoicesVM.DateTimePicker2 + ',' + objPrintInvoicesVM.RbtPrintOption;

            AutoMapper.Mapper.CreateMap<PrintInvoicesVM, PrintReportQueue>();
            //ObjSource.IsBranch = true;
            PrintReportQueue objPrintReportQueue = new PrintReportQueue();
            objPrintReportQueue = AutoMapper.Mapper.Map<PrintReportQueue>(objPrintInvoicesVM);
            objPrintReportQueue.RequestedDate = DateTime.Now;
            objPrintReportQueue.ReportName = ReportName;
            objPrintReportQueue.TabValue = "AccLiensPrintInvoice";
            objPrintReportQueue.ParameterList = ParameterList;
            objPrintReportQueue.ParameterValues = ParameterValues;
            objPrintReportQueue.ChkCsvReport = objPrintInvoicesVM.chkCsv;
            objPrintReportQueue.ReportStoragePath = path;
            objPrintReportQueue.ReportType = objPrintInvoicesVM.RbtPrintOption;
            objPrintReportQueue.UserName = user.UserName;
            objPrintReportQueue.UserId = user.Id;
            objPrintReportQueue.UserEmail = user.Email;
            ITable tblPrintReportQueue = (ITable)objPrintReportQueue;
            DBO.Provider.Create(ref tblPrintReportQueue);

            try
            {
                //CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var mybatchid = CRF.BLL.Reports.ClientReport.GetUniqueId();

                var Sp_OBJ = new uspbo_Accounting_GetClientFinDocs();

                Sp_OBJ.DOCUMENTFROM = objPrintInvoicesVM.DateTimePicker1;
                Sp_OBJ.DOCUMENTTO = objPrintInvoicesVM.DateTimePicker2;
                sFromDate = objPrintInvoicesVM.DateTimePicker1;
                sThruDate = objPrintInvoicesVM.DateTimePicker2;

                if (objPrintInvoicesVM.chkAllClient == false)
                {
                    Sp_OBJ.CLIENTNUMBER = objPrintInvoicesVM.ClientId;
                }

                Sp_OBJ.INVOICECYCLE = objPrintInvoicesVM.InvoiceCycleId;
                //if (objPrintInvoicesVM.InvoiceCycleId == 1)
                //{
                //    Sp_OBJ.INVOICECYCLE = "M";
                //}
                //if (objPrintInvoicesVM.InvoiceCycleId == 2)
                //{
                //    Sp_OBJ.INVOICECYCLE = "S";
                //}

                //if (objPrintInvoicesVM.InvoiceCycleId == 3)
                //{
                //    Sp_OBJ.INVOICECYCLE = "W";
                //}
                Sp_OBJ.INVOICECYCLE = objPrintInvoicesVM.InvoiceCycleId;
               // Sp_OBJ.SALESTEAM = "8";
                //Sp_OBJ.SERVICETEAM = "SF";

                TableView mytableview = DBO.Provider.GetTableView(Sp_OBJ);

                TableView myview = CRF.BLL.Providers.DBO.Provider.GetTableView(Sp_OBJ);
                DataSet ds = DBO.Provider.GetDataSet(Sp_OBJ);
                DataTable mytbl = ds.Tables[0];

                if (ds.Tables[0].Rows.Count == 0)
                {
                    return "error"; 
                }

                ////    //TableView myview = CRF.BLL.Providers.DBO.Provider.GetTableView(Sp_OBJ);

                ////    CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;
                ////    if (objPrintInvoicesVM.RbtPrintOption == "optToPDF")
                ////    {
                ////        mymode = mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                ////    }

                ////    if (objPrintInvoicesVM.RbtPrintOption == "GenerateEmails")
                ////    {
                ////        mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                ////    }

                ////    myview = CRF.BLL.Accounting.Liens.GetInvoices(user, Sp_OBJ, mymode);

                //foreach (DataRow row in mytbl.Rows)
                //{
                //    // string myreportname = "Liens Invoice";
                //    string myfilename = "";
                //    ClientFinDoc myfindoc = new ClientFinDoc();

                //    myfilename = row["ClientCode"] + "-" + row["FinDocId"];

                //    if (row["ClientCode"] != row["SubBreak"])
                //    {
                //        myfilename = row["ClientCode"] + "-" + row["SubBreak"] + "-" + row["FinDocId"];
                //    }

                //    var objfindoc = new ClientFinDoc();
                //    ITable Imyfindoc = objfindoc;
                //    DBO.Provider.Read(Convert.ToString(row["FinDocId"]), ref Imyfindoc);
                //    myfindoc = (ClientFinDoc)Imyfindoc;

                //    //  Create History
                //    //  If invoice by client, use clientid else use main clientid
                //    //  1/7/2019 Added logic to combine invoice and statement
                //    if (Convert.ToInt32(row["FinDocTypeId"]) == 5 || Convert.ToInt32(row["FinDocTypeId"]) == 6)
                //    {
                //        //     CreateReportHistory(myview.RowItem("ClientId"), myview.RowItem("ClientCode"))
                //        myclrpt = new CRF.BLL.Reports.ClientReport();
                //        //if (objPrintInvoicesVM.RbtPrintOption != "GenerateEmails")
                //        //{
                //        //    // return "";
                //        //}
                //        myclrpt = new CRF.BLL.Reports.ClientReport("LIEN-INVOICE", mybatchid, Convert.ToInt32(row["ClientId"]), Convert.ToString(row["ClientCode"]));
                //        myclrpt.UpdateStatus(Convert.ToInt32(row[CRF.BLL.CRFDB.VIEWS.vwClientFinDoc.ColumnNames.FinDocId]), 1, 1);

                //        // 3/2/2017 Added myfindoc.IsEmailInvoice = True 
                //        //if (objfindoc.IsEmailInvoice == false)
                //        //{
                //        //    return "";
                //        //}
                //        myclrpt.CreateRecipient(objfindoc.InvoiceEmail, objfindoc.InvoiceAttn);

                //    }
                //    else
                //    {
                //        //  CreateReportHistory(myview.RowItem("MainClientId"), myview.RowItem("ClientNumber"))
                //        myclrpt = new CRF.BLL.Reports.ClientReport();
                //        if (objPrintInvoicesVM.RbtPrintOption != "GenerateEmails")
                //        {
                //            // return "";
                //        }
                //        myclrpt = new CRF.BLL.Reports.ClientReport("LIEN-INVOICE", mybatchid, Convert.ToInt32(row["MainClientId"]), Convert.ToString(row["ClientNumber"]));
                //        myclrpt.UpdateStatus(Convert.ToInt32(row[CRF.BLL.CRFDB.VIEWS.vwClientFinDoc.ColumnNames.FinDocId]), 1, 1);

                //        // 3/2/2017 Added myfindoc.IsEmailInvoice = True 
                //        if (objfindoc.IsEmailInvoice == false)
                //        {
                //            // return null;
                //        }
                //        myclrpt.CreateRecipient(objfindoc.InvoiceEmail, objfindoc.InvoiceAttn);

                //    }

                //    //  Print invoice page
                //    MYREPORTPATH = CRF.BLL.Accounting.Liens.PrintInvoice(user, Convert.ToInt32(row["FinDocId"]), mymode, myfilename);

                //    if (Convert.ToBoolean(row["ExportToCSV"]) == true || objPrintInvoicesVM.chkCsv == true)
                //    {
                //        CreateAttachment("Invoice Summary (" + row["FinDocId"] + ")", MYREPORTPATH);
                //    }
                //    else
                //    {
                //        myfiles.Add(MYREPORTPATH);
                //    }

                //    //  Print statements
                //    if (Convert.ToBoolean(row["ExportToCSV"]) == true || objPrintInvoicesVM.chkCsv == true)
                //    {
                //        //   CSV output
                //        MYREPORTPATH = CRF.BLL.Accounting.Liens.ExportStatement(user, Convert.ToInt32(row["FinDocId"]), myfilename);
                //        CreateAttachment("Invoice Detail (" + row["FinDocId"] + ")", MYREPORTPATH);

                //        if (Convert.ToBoolean(row["UseBranchSummary"]) == true)
                //        {

                //            // Print Branch summary statement
                //            MYREPORTPATH = CRF.BLL.Accounting.Liens.ExportInvoiceBranchSummary(user, Convert.ToInt32(row["FinDocId"]), myfilename);
                //            MYREPORTPATH = CRF.BLL.Accounting.Liens.ExportInvoiceBranchDetail(user, Convert.ToInt32(row["FinDocId"]), myfilename);

                //        }
                //        else if (Convert.ToBoolean(row["UseClientSummary"]) == true)
                //        {
                //            //   Print Client summary statement
                //            MYREPORTPATH = CRF.BLL.Accounting.Liens.PrintInvoiceClientSummary(user, Convert.ToInt32(row["FinDocId"]), mymode, myfilename);
                //        }
                //    }
                //    else
                //    {

                //        //   Report
                //        if (Convert.ToBoolean(row["UseBranchSummary"]) == true)
                //        {
                //            // Print Branch summary statement
                //            MYREPORTPATH = CRF.BLL.Accounting.Liens.PrintInvoiceBranchSummary(user, Convert.ToInt32(row["Id"]), mymode, myfilename);
                //            MYREPORTPATH = CRF.BLL.Accounting.Liens.PrintInvoiceBranchDetail(user, Convert.ToInt32(row["Id"]), mymode, myfilename);
                //        }
                //        else if (Convert.ToBoolean(row[CRF.BLL.CRFDB.VIEWS.vwClientFinDoc.ColumnNames.UseClientSummary]) == true)
                //        {
                //            //  Print Client summary statement
                //            MYREPORTPATH = CRF.BLL.Accounting.Liens.PrintInvoiceClientSummary(user, Convert.ToInt32(row["FinDocId"]), mymode, myfilename);
                //        }
                //        else
                //        {
                //            //    Print standard statement
                //            if (Convert.ToBoolean(row["BranchBreak"]) == true)
                //            {
                //                MYREPORTPATH = CRF.BLL.Accounting.Liens.PrintInvoiceStatementByBranch(user, Convert.ToInt32(row["FinDocId"]), mymode, myfilename);
                //            }
                //            else
                //            {
                //                MYREPORTPATH = CRF.BLL.Accounting.Liens.PrintInvoiceStatement(user, Convert.ToInt32(row["FinDocId"]), mymode, myfilename);
                //            }
                //        }

                //    }

                //    //if (!(Convert.ToBoolean(row["ExportToCSV"]) == true || objPrintInvoicesVM.chkCsv == true))
                //    //{
                //    //    myfiles.Add(MYREPORTPATH);
                //    //    var sCombinedFilename = "";
                //    //    sCombinedFilename = user.OutputFolder + myfilename + "-LienInvoice-" + DateTime.Now.ToString("yyyyMMddhhmmss").Trim() + ".PDF";
                //    //     Combine(myfiles, sCombinedFilename);
                //    //    CreateAttachment("Lien Invoice (" + row["FinDocId"] + ")", sCombinedFilename);
                //    //    //   Cleanup separate PDFs
                //    //    string FILE = "";


                //    //    foreach (System.IO.File file in myfiles)
                //    //    {
                //    //        if (System.IO.File.Exists(Convert.ToString(file)) == true)
                //    //        {
                //    //            System.IO.File.Delete(Convert.ToString(file));
                //    //        }
                //    //    }


                //    //}

                //    CloseReportHistory(2, "");
                //    //DownloadReport(MYREPORTPATH);
                //}


            }

            catch (Exception ex)
            {
                return "";
            }
            return "1";
        }


        private static void CreateAttachment(string areportname, string afilepath)
        {
            PrintInvoicesVM objPrintInvoicesVM = new PrintInvoicesVM();
            ClientFinDoc myfindoc = new ClientFinDoc();
            CRF.BLL.Reports.ClientReport myclrpt = new CRF.BLL.Reports.ClientReport();

            if (myclrpt.History.PKID < 1 || objPrintInvoicesVM.RbtPrintOption != "GenerateEmails" || myfindoc.IsEmailInvoice == false)
            {
                return;
            }

            //   3/2/2017 Added myfindoc.IsEmailInvoice = True 
            if (System.IO.File.Exists(afilepath) == true && objPrintInvoicesVM.RbtPrintOption == "GenerateEmails" && myfindoc.IsEmailInvoice == true)
            {
                myclrpt.CreateAttachment(areportname, afilepath);
            }

            System.IO.File.Delete(afilepath);

        }

        private static void CreateReportHistory(int aclientid, string aclientcode)
        {

            PrintInvoicesVM objPrintInvoicesVM = new PrintInvoicesVM();
            CRF.BLL.Reports.ClientReport myclrpt = new CRF.BLL.Reports.ClientReport();
            var mybatchid = CRF.BLL.Reports.ClientReport.GetUniqueId();

            if (objPrintInvoicesVM.RbtPrintOption != "GenerateEmails")
            {
                return;
            }

            myclrpt = new CRF.BLL.Reports.ClientReport("LIEN-INVOICE", mybatchid, aclientid, aclientcode);




        }





        private static void CloseReportHistory(int status, string message)
        {
            PrintInvoicesVM objPrintInvoicesVM = new PrintInvoicesVM();
            CRF.BLL.Reports.ClientReport myclrpt = new CRF.BLL.Reports.ClientReport();

            if (myclrpt.History.PKID < 1)
            {
                return;
            }
            if (objPrintInvoicesVM.RbtPrintOption != "GenerateEmails")
            {
                return;
            }

            myclrpt.CloseHistory(status, message);
        }


        private static bool Combine(ArrayList afiles, string adest)
        {
            if (System.IO.Directory.Exists(("TEMP\\")))
            {
                System.IO.Directory.Delete("TEMP\\", true);
            }

            System.IO.Directory.CreateDirectory("TEMP\\");

            if (System.IO.Directory.Exists(System.IO.Path.GetDirectoryName(adest)) == false)
            {
                System.IO.Directory.CreateDirectory(System.IO.Path.GetDirectoryName(adest));
            }

            foreach (System.IO.File FILE in afiles)
            {
                var OUTFILE = "TEMP\\" + System.IO.Path.GetFileName(Convert.ToString(FILE));
                if (System.IO.File.Exists(Convert.ToString(FILE)) == true)
                {
                    System.IO.File.Copy(Convert.ToString(FILE), OUTFILE);
                }
            }

            return true;


        }




        private static string ProduceCSV(DataView aview, string areportname)
        {
            CRF.BLL.Export.ExportColumns mycols = new CRF.BLL.Export.ExportColumns();
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            string sfilepath = CRF.BLL.CRFView.CRFView.GetFileName(ref user, "Management", areportname, ".CSV");
            string fileName = CRF.BLL.Export.Export.ExportToCSV(ref aview, ref sfilepath, ref mycols, true);
            if (!string.IsNullOrEmpty(fileName))
            {
                return fileName;
            }
            else
            {
                return "";
            }
        }





        public static string PrintSmanCommission(SmanCommissionVM objSmanCommissionVM)
        {
            string pdfPath = null;
            string sFromDate = null;
            string sThruDate = null;

            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var Sp_OBJ = new uspbo_Accounting_GetSmanComm();

                //string sql = "Select * from vwCollectionReportsList Where ReportId = " + Convert.ToInt32(objCollectionReportsVM.ddlReportId);
                TableView mytableview = DBO.Provider.GetTableView(Sp_OBJ);

                if (objSmanCommissionVM.chkAllClient == false)
                {
                    Sp_OBJ.ClientId = objSmanCommissionVM.ClientId;
                }

                if (objSmanCommissionVM.chkAllSman == false)
                {
                    Sp_OBJ.SmanNum = objSmanCommissionVM.TeamId;
                }

                Sp_OBJ.FromDate = objSmanCommissionVM.objADClientFinDoc.InvoiceFromDate.ToShortDateString();
                Sp_OBJ.ThruDate = objSmanCommissionVM.objADClientFinDoc.InvoiceThruDate.ToShortDateString();
                sFromDate = objSmanCommissionVM.objADClientFinDoc.InvoiceFromDate.ToShortDateString();
                sThruDate = objSmanCommissionVM.objADClientFinDoc.InvoiceThruDate.ToShortDateString();


                //Sp_OBJ.CustomSPName = Convert.ToString(mydt.Rows[0]["CustomSPName"]);
                //Sp_OBJ.ReportName = Convert.ToString(mydt.Rows[0]["ReportName"]);

                //TableView myview = DBO.Provider.GetTableView(Sp_OBJ);
                TableView myview = CRF.BLL.Providers.DBO.Provider.GetTableView(Sp_OBJ);
                DataSet ds = DBO.Provider.GetDataSet(Sp_OBJ);
                DataTable mytbl = ds.Tables[0];

                //TableView myview = CRF.BLL.Providers.DBO.Provider.GetTableView(Sp_OBJ);

                CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;
                if (objSmanCommissionVM.RbtPrintOption == "optToPDF")
                {
                    mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                }
                string myreportfilename = Globals.CurrentUser.SettingsFolder + "LienReports.xml";
                string myreportname = "SalesCommissionLien";


                if (objSmanCommissionVM.RbtPrintOption == "Create CSV File")
                {
                    // mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                    // pdfPath = CRF.BLL.Accounting.Liens.Export.ExportLienRevenue(user, ref myview, myreportname);
                    pdfPath = ADClientFinDoc.ProduceCSV(myview, myreportname);

                }
                else
                {

                    pdfPath = Globals.RenderC1Report(user, mytbl, myreportfilename, myreportname, mymode, "", sFromDate, sThruDate);

                }
            }

            catch (Exception ex)
            {
                return "";
            }
            return pdfPath;
        }

        //private static void DownloadReport(string path)
        //{
        //    //Response.AppendHeader("Content-Disposition", "attachment; filename=" + objSearchDocuments.PhysicalName); Response.ContentType = "application/octet-stream";
        //    //string ShareName = System.Configuration.ConfigurationManager.AppSettings.Get("ServerLocationDocumentsFolder");
        //    //  NetworkCredential nc = new NetworkCredential(CommonRoutines.decryptString(ConfigurationSettingsReader.ServerLocationUsername), CommonRoutines.decryptString(ConfigurationSettingsReader.ServerLocationPassword));
        //    //  NetworkConnection ncn = new NetworkConnection(ShareName, nc);
        //    using (System.IO.FileStream fs = new System.IO.FileStream(path, System.IO.FileMode.Open, System.IO.FileAccess.Read))
        //    {
        //        byte[] currentFileBytes = new byte[fs.Length];
        //        fs.Read(currentFileBytes, 0, currentFileBytes.Length);
        //        fs.Close();
        //        Response.BinaryWrite(currentFileBytes);
        //        currentFileBytes = null;
        //        Response.Flush();
        //    }
        //    Response.End();
        //}







        #endregion

    }
}
