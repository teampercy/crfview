﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using CRF.BLL.Clients;
using CRFView.Adapters.Clients.Client_Management;
using System.IO;
using System.Text.RegularExpressions;
using CRFView.Adapters.Accounting.AccountingCollections.InvoiceRecap;
using CRF.BLL;

namespace CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices
{
    public class ADClientCollFinDoc
    {
        #region Properties
        public int Id { get; set; }
        public int BatchId { get; set; }
        public int ContractId { get; set; }
        public System.DateTime DocumentDate { get; set; }
        public string TrxPeriod { get; set; }
        public decimal TaxRate { get; set; }
        public decimal DiscRate { get; set; }
        public decimal PmtAmt { get; set; }
        public decimal FeeAmt { get; set; }
        public decimal InterestShareAmt { get; set; }
        public decimal CourtCostAmt { get; set; }
        public decimal AttyFeeAmt { get; set; }
        public decimal RcvByAgencyAmt { get; set; }
        public decimal RcvByClientAmt { get; set; }
        public decimal FeeByAgencyAmt { get; set; }
        public decimal FeeByClientAmt { get; set; }
        public decimal DueFromClientAmt { get; set; }
        public decimal RemitToClientAmt { get; set; }
        public decimal FeesEarnedAmt { get; set; }
        public decimal FeesDeductedAmt { get; set; }
        public decimal DocumentAmt { get; set; }
        public decimal TotRcvAmt { get; set; }
        public decimal TotAdjAmt { get; set; }
        public decimal TotalDue { get; set; }
        public string InvoiceName { get; set; }
        public string InvoiceAttn { get; set; }
        public string InvoiceAddr1 { get; set; }
        public string InvoiceAddr2 { get; set; }
        public string InvoiceCity { get; set; }
        public string InvoiceState { get; set; }
        public string InvoiceZip { get; set; }
        public string CheckNo { get; set; }
        public bool IsGrossRemit { get; set; }
        public bool IsBalanceDeduct { get; set; }
        public string Note { get; set; }
        public bool IsIndStmt { get; set; }
        public string StmtGrouping { get; set; }
        public System.DateTime InvoiceFromDate { get; set; }
        public System.DateTime InvoiceThruDate { get; set; }
        public bool IsIndPaidAgencyStmt { get; set; }
        public bool IsIndPaidClientStmt { get; set; }
        public string InvoiceCycle { get; set; }
        public bool IsSeparateStmt { get; set; }
        public string InvoiceBreak { get; set; }
        public int FinDocTypeId { get; set; }
        public string ClientNumber { get; set; }
        public string InvoicePeriod { get; set; }
        public string InvoiceEmail { get; set; }
        public bool IsEmailInvoice { get; set; }
        public int TaskHistoryId { get; set; }
        public bool IsPaid { get; set; }
        public bool IsInvoice { get; set; }
        public string vendornumber { get; set; }
        public System.DateTime ReviewDate { get; set; }
        public string InvoiceClientCode { get; set; }
        #endregion

        #region CRUD


        public static ADClientCollFinDoc ReadCollClientFinDoc(int id)
        {
            ClientCollFinDoc myentity = new ClientCollFinDoc();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (ClientCollFinDoc)myentityItable;
            AutoMapper.Mapper.CreateMap<ClientCollFinDoc, ADClientCollFinDoc>();
            return (AutoMapper.Mapper.Map<ADClientCollFinDoc>(myentity));
        }
        public static bool ResendEmailUpdate(string Id, string PKID, string InvoiceEmail)
        {
            try
            {

                var myfindoc = new ClientCollFinDoc();
                ITable Imyfindoc = myfindoc;
                DBO.Provider.Read(Id, ref Imyfindoc);
                myfindoc = (ClientCollFinDoc)Imyfindoc;
                if (myfindoc.IsEmailInvoice == false)
                {
                    return false;
                }
                else
                {
                    var mysql = "Update Report_HistoryRecipient ";
                    mysql += " Set email = '" + InvoiceEmail + "' ";
                    mysql += " where HistoryId ='" + PKID + "' ";
                    DBO.Provider.ExecNonQuery(mysql);

                    myfindoc.InvoiceEmail = InvoiceEmail;
                    Imyfindoc = myfindoc;
                    DBO.Provider.Update(ref Imyfindoc);

                    var myhistory = new Report_History();
                    ITable Imyhistory = myhistory;
                    DBO.Provider.Read(PKID,ref Imyhistory);
                    myhistory = (Report_History)Imyhistory;
                    myhistory.status = 3;
                    Imyhistory = myhistory;
                    DBO.Provider.Update(ref Imyhistory);


                    return true;
                }

            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool UpdatePaid(string Id)
        {
            try
            {
                var myfindoc = new ClientCollFinDoc();
                ITable ITable = myfindoc;
                HDS.DAL.Provider.DAL.Read(Id, ref ITable);
                myfindoc = (ClientCollFinDoc)ITable;
                myfindoc.IsPaid = true;
                ITable = myfindoc;
                HDS.DAL.Provider.DAL.Update(ref ITable);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool FlagUnpaid(string Id)
        {
            try
            {
                var myfindoc = new ClientCollFinDoc();
                ITable Imyfindoc = myfindoc;
                HDS.DAL.Provider.DAL.Read(Id, ref Imyfindoc);
                myfindoc = (ClientCollFinDoc)Imyfindoc;
                myfindoc.IsPaid = false;
                Imyfindoc = myfindoc;
                HDS.DAL.Provider.DAL.Update(ref Imyfindoc);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public static bool UpdateViewed(string PKID)
        {
            try
            {
                var myhistory = new Report_History();
                ITable Imyhistory = myhistory;
                HDS.DAL.Provider.DAL.Read(PKID, ref Imyhistory);
                myhistory = (Report_History)Imyhistory;
                myhistory.dateviewed = DateTime.Now;
                Imyhistory = myhistory;
                DBO.Provider.Update(ref Imyhistory);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool UpdateClientFinDoc(string Id, bool ChkNewReview, bool ChkNewEmail,
                                            bool ChkDeleteReview, string TxtNewReviewDate, string TxtNewEmail)
        {
            try
            {
                var myfindoc = new ClientCollFinDoc();
                ITable ITable = myfindoc;
                HDS.DAL.Provider.DAL.Read(Id, ref ITable);
                myfindoc = (ClientCollFinDoc)ITable;
                if (ChkNewReview == true)
                {
                    myfindoc.ReviewDate = Convert.ToDateTime(TxtNewReviewDate);
                }
                if (ChkDeleteReview == true)
                {
                    myfindoc.ReviewDate = DateTime.MinValue;
                }
                if (ChkNewEmail == true)
                {
                    myfindoc.InvoiceEmail = TxtNewEmail;
                }
                ITable = myfindoc;
                HDS.DAL.Provider.DAL.Update(ref ITable);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public static string SaveInvoiceRecapsPrint(AccountingInvoiceRecapsVM objAccountingInvoiceRecapsVM)
        {
            string pdfPath = null;
            string sFromDate = null;
            string sThruDate = null;

            try
            {
                var result = "";

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var myds = new DataSet();
                var sp_obj = new cview_Accounting_GetFinDocs();
                sp_obj.DOCUMENTFROM = objAccountingInvoiceRecapsVM.DateTimePicker1;
                sp_obj.DOCUMENTTO = objAccountingInvoiceRecapsVM.DateTimePicker2;
                //sp_obj.INVOICECYCLE = Convert.ToString(objInvoiceRecapsVM.FinDocCycle);
                //myds = CRF.BLL.Accounting.Liens.GetRecaps(user, sp_obj);

                DataSet ds = DBO.Provider.GetDataSet(sp_obj);
               // DataTable mytbl = ds.Tables[0];

                var myfilename = "TrustInvoiceRecap";
                var myreportfilename = "CollectViewReports.xml";

                CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;

                if (objAccountingInvoiceRecapsVM.RbtPrintOption == "optToPDF")
                {
                    mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                }

                 pdfPath = Globals.RenderC1Report(user, ds.Tables[0], myreportfilename, myfilename, mymode, "", sFromDate, sThruDate);

                DBO.Provider.ExecNonQuery(sp_obj);
                result = "Report Processed Succesfully";

            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return "";
            }

            return pdfPath;
        }

        #endregion
    }
}
