﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices.LienInvoicesVM;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices
{
   public class ADClientFinDocLedger
    {
        #region Properties
        public int Id { get; set; }
        public int ClientFinDocId { get; set; }
        public System.DateTime LedgerDate { get; set; }
        public decimal BalAmt { get; set; }
        public decimal RcvAmt { get; set; }
        public decimal AdjAmt { get; set; }
        public string Reference { get; set; }
        public string Note { get; set; }
        public string CreatedBy { get; set; }
        public DateTime CreatedOn { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string PmtStatus { get; set; }
        public bool IsCheck { get; set; }
        public bool IsCreditCard { get; set; }
        public bool IsACH { get; set; }
        public bool IsAdjustment { get; set; }
        public DateTime PmtDate { get; set; }
        public string CCFeeAmt { get; set; }
        #endregion

        #region CRUD

        public static ADClientFinDocLedger ReadClientFinDocLedger(int id)
        {
            ClientFinDocLedger myentity = new ClientFinDocLedger();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (ClientFinDocLedger)myentityItable;
            AutoMapper.Mapper.CreateMap<ClientFinDocLedger, ADClientFinDocLedger>();
            return (AutoMapper.Mapper.Map<ADClientFinDocLedger>(myentity));
        }

        public static bool SavePaymentORAdjustment(LienInvoiceDetailsVM objLienInvoiceDetailsVM)
        {
            try
            {
               
                ADClientFinDocLedger OBJADClientFinDocLedger = ADClientFinDocLedger.ReadClientFinDocLedger(objLienInvoiceDetailsVM.ClntFinId);

                AutoMapper.Mapper.CreateMap<ADClientFinDocLedger, ClientFinDocLedger>();
              
                ITable tbClientFinDocLedger;

                string r = objLienInvoiceDetailsVM.LienRcvAmt.ToString();
                OBJADClientFinDocLedger.RcvAmt = Convert.ToDecimal(r.Replace(@"$", ""));

                string a = objLienInvoiceDetailsVM.LienAdjAmt.ToString();
                OBJADClientFinDocLedger.AdjAmt = Convert.ToDecimal(a.Replace(@"$", ""));

                //OBJADClientFinDocLedger.AdjAmt = objLienInvoiceDetailsVM.objADClientFinDocLedger.AdjAmt.Replace(@"$", ""); 
                OBJADClientFinDocLedger.CCFeeAmt = objLienInvoiceDetailsVM.objADClientFinDocLedger.CCFeeAmt.Replace(@"$", "");
                OBJADClientFinDocLedger.Note = objLienInvoiceDetailsVM.objADClientFinDocLedger.Note;

                if (objLienInvoiceDetailsVM.RdbBtn == "Adjustment")
                {
                    OBJADClientFinDocLedger.IsAdjustment = true;
                }
                if (objLienInvoiceDetailsVM.PTypeRdbBtn == "Check")
                {
                    OBJADClientFinDocLedger.IsCheck = true;
                }
                if (objLienInvoiceDetailsVM.PTypeRdbBtn == "CreditCard")
                {
                    OBJADClientFinDocLedger.IsCreditCard =  true;
                }
                if (objLienInvoiceDetailsVM.PTypeRdbBtn == "ACH")
                {
                    OBJADClientFinDocLedger.IsACH = true;
                }
                
              
                if (objLienInvoiceDetailsVM.ClntFinId != 0)
                {
                    OBJADClientFinDocLedger.Id = objLienInvoiceDetailsVM.ClntFinId;
                    OBJADClientFinDocLedger.ClientFinDocId = objLienInvoiceDetailsVM.objADClientFinDocLedger.ClientFinDocId;
                    OBJADClientFinDocLedger.LedgerDate = objLienInvoiceDetailsVM.objADClientFinDocLedger.LedgerDate;
                     string p = objLienInvoiceDetailsVM.LienBalAmt.ToString();
                    OBJADClientFinDocLedger.BalAmt = Convert.ToDecimal(p.Replace(@"$", ""));
                    ClientFinDocLedger objClientFinDocLedger = AutoMapper.Mapper.Map<ClientFinDocLedger>(OBJADClientFinDocLedger);
                    tbClientFinDocLedger = objClientFinDocLedger;
                    DBO.Provider.Update(ref tbClientFinDocLedger);
                    return true;
                }
                else {

                    OBJADClientFinDocLedger.ClientFinDocId = objLienInvoiceDetailsVM.ObjClientFinDoc.Id;
                    OBJADClientFinDocLedger.LedgerDate = objLienInvoiceDetailsVM.ObjClientFinDoc.DocumentDate;
                    string s = objLienInvoiceDetailsVM.ObjClientFinDoc.DocumentAmt.ToString();
                    OBJADClientFinDocLedger.BalAmt = 0.00m;
                    ClientFinDocLedger objClientFinDocLedger = AutoMapper.Mapper.Map<ClientFinDocLedger>(OBJADClientFinDocLedger);
                    tbClientFinDocLedger = objClientFinDocLedger;
                    DBO.Provider.Create(ref tbClientFinDocLedger);

                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion
    }
}
