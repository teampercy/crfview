﻿using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices.LienInvoicesVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices.LienInvoicesVM
{
    public class LienInvoicesListVM
    {
        public SearchDivVM ObjSearchDiv { get; set; }
        public List<ADVWCLIENTFINDOCEMAIL> LienInvoices { get; set; }
    }
}
