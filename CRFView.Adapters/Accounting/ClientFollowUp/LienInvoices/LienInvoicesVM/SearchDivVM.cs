﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices.LienInvoicesVM
{
    public class SearchDivVM
    {
        #region Properties
        public bool All { get; set; }
        public bool NotViewed { get; set; }
        public bool Viewed { get; set; }
        public bool UnPaid { get; set; }
        public bool ReviewDate { get; set; }
        public string TxtSearach { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        #endregion

    }
}
