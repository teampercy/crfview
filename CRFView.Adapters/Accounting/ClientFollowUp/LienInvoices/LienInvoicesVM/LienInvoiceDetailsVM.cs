﻿using CRF.BLL.CRFDB.TABLES;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices.LienInvoicesVM;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices.LienInvoicesVM
{
    public class LienInvoiceDetailsVM
    {
        public List<ADvwIssues> Issues { get; set; }
        public List<ADreport_historyattachment> Attachments { get; set; }

        public List<ADClientFinDocLedger> Payments { get; set; }
        public Client ObjClient { get; set; }
        public Report_History ObjReportHistory { get; set; }
        public ClientFinDoc ObjClientFinDoc { get; set; }
        public ClientLienInfo ObjClientLienInfo { get; set; }

        public ADClientFinDocLedger objADClientFinDocLedger { get; set; }
        public bool ChkNewReview { get; set; } = false;
        public bool ChkDeleteReview { get; set; } = false;
        public bool ChkNewEmail { get; set; } = false;
        public string TxtNewEmail { get; set; }
        public string TxtNewReviewDate { get; set; }
        public string RdbBtn { get; set; }
        public string PTypeRdbBtn { get; set; }
        public int ClntFinId { get; set; }
        public int ClientFinDocId { get; set; }
        public string HistoryId { get; set; }
        public string LienBalAmt { get; set; }
        public string LienRcvAmt { get; set; }
        public string LienAdjAmt { get; set; }

    }
}
