﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices.CollectionInvoicesVM;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices
{
    public class ADClientCollFinDocLedger
    {

        #region Properties
        public int Id { get; set; }
        public int ClientFinDocId { get; set; }
        public System.DateTime LedgerDate { get; set; }
        public decimal BalAmt { get; set; }
        public decimal RcvAmt { get; set; }
        public decimal AdjAmt { get; set; }
        public string Reference { get; set; }
        public string Note { get; set; }
        public string EnteredBy { get; set; }
        public DateTime EntryDate { get; set; }
        public string ModifiedBy { get; set; }
        public DateTime ModifiedOn { get; set; }
        public string PmtStatus { get; set; }
        public bool IsCheck { get; set; }
        public bool IsCreditCard { get; set; }
        public bool IsACH { get; set; }
        public bool IsAdjustment { get; set; }
        public DateTime PmtDate { get; set; }
        public string CCFeeAmt { get; set; }
        #endregion

        #region CRUD

        public static ADClientCollFinDocLedger ReadCollClientFinDocLedger(int id)
        {
            ClientCollFinDocLedger myentity = new ClientCollFinDocLedger();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (ClientCollFinDocLedger)myentityItable;
            AutoMapper.Mapper.CreateMap<ClientCollFinDocLedger, ADClientCollFinDocLedger>();
            return (AutoMapper.Mapper.Map<ADClientCollFinDocLedger>(myentity));
        }

        public static bool SavePaymentORAdjustment(CollectionInvoiceDetailsVM objCollectionInvoiceDetailsVM)
        {
            try
            {

                ADClientCollFinDocLedger OBJADClientCollFinDocLedger = ADClientCollFinDocLedger.ReadCollClientFinDocLedger(objCollectionInvoiceDetailsVM.ClntCollFinId);

                ADClientCollFinDoc objClientCollFinDoc = ADClientCollFinDoc.ReadCollClientFinDoc(OBJADClientCollFinDocLedger.ClientFinDocId);

                AutoMapper.Mapper.CreateMap<ADClientCollFinDocLedger, ClientCollFinDocLedger>();

                ITable ClientCollFinDocLedger;

                string r = objCollectionInvoiceDetailsVM.CollRcvAmt.ToString();
                OBJADClientCollFinDocLedger.RcvAmt = Convert.ToDecimal(r.Replace(@"$", ""));

                string a = objCollectionInvoiceDetailsVM.CollAdjAmt.ToString();
                OBJADClientCollFinDocLedger.AdjAmt = Convert.ToDecimal(a.Replace(@"$", ""));

                //OBJADClientFinDocLedger.AdjAmt = objLienInvoiceDetailsVM.objADClientFinDocLedger.AdjAmt.Replace(@"$", ""); 
                OBJADClientCollFinDocLedger.CCFeeAmt = objCollectionInvoiceDetailsVM.objADClientCollFinDocLedger.CCFeeAmt.Replace(@"$", "");
                OBJADClientCollFinDocLedger.Note = objCollectionInvoiceDetailsVM.objADClientCollFinDocLedger.Note;

                if (objCollectionInvoiceDetailsVM.RdbBtn == "Adjustment")
                {
                    OBJADClientCollFinDocLedger.IsAdjustment = true;
                }
                if (objCollectionInvoiceDetailsVM.PTypeRdbBtn == "Check")
                {
                    OBJADClientCollFinDocLedger.IsCheck = true;
                }
                if (objCollectionInvoiceDetailsVM.PTypeRdbBtn == "CreditCard")
                {
                    OBJADClientCollFinDocLedger.IsCreditCard = true;
                }
                if (objCollectionInvoiceDetailsVM.PTypeRdbBtn == "ACH")
                {
                    OBJADClientCollFinDocLedger.IsACH = true;
                }


                if (objCollectionInvoiceDetailsVM.ClntCollFinId != 0)
                {
                    OBJADClientCollFinDocLedger.Id = objCollectionInvoiceDetailsVM.ClntCollFinId;
                    OBJADClientCollFinDocLedger.ClientFinDocId = objCollectionInvoiceDetailsVM.objADClientCollFinDocLedger.ClientFinDocId;
                    OBJADClientCollFinDocLedger.LedgerDate = objCollectionInvoiceDetailsVM.objADClientCollFinDocLedger.LedgerDate;
                    //string TstCollBalAmt = objCollectionInvoiceDetailsVM.CollBalAmt.ToString();
                    //string TstCollBalAmt1 = TstCollBalAmt.Replace(@"$", "");
                    //OBJADClientCollFinDocLedger.BalAmt = Convert.ToDecimal(TstCollBalAmt1.Replace(@"(", "").Replace(@")", ""));
                   
                    if (objCollectionInvoiceDetailsVM.CollBalAmt == "$0.00")
                    {
                        OBJADClientCollFinDocLedger.BalAmt = 0.00m;
                    }
                    else
                    {
                        OBJADClientCollFinDocLedger.BalAmt = objClientCollFinDoc.DocumentAmt;
                    }
                    ClientCollFinDocLedger objClientCollFinDocLedger = AutoMapper.Mapper.Map<ClientCollFinDocLedger>(OBJADClientCollFinDocLedger);
                    ClientCollFinDocLedger = objClientCollFinDocLedger;
                    DBO.Provider.Update(ref ClientCollFinDocLedger);
                    return true;
                }
                else
                {

                    OBJADClientCollFinDocLedger.ClientFinDocId = objCollectionInvoiceDetailsVM.ObjClientCollFinDoc.Id;
                    OBJADClientCollFinDocLedger.LedgerDate = objCollectionInvoiceDetailsVM.ObjClientCollFinDoc.DocumentDate;
                    string s = objCollectionInvoiceDetailsVM.ObjClientCollFinDoc.DocumentAmt.ToString();
                    OBJADClientCollFinDocLedger.BalAmt = 0.00m;
                    ClientCollFinDocLedger objClientCollFinDocLedger = AutoMapper.Mapper.Map<ClientCollFinDocLedger>(OBJADClientCollFinDocLedger);
                    ClientCollFinDocLedger = objClientCollFinDocLedger;
                    DBO.Provider.Create(ref ClientCollFinDocLedger);

                    return true;
                }
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        #endregion
    }
}
