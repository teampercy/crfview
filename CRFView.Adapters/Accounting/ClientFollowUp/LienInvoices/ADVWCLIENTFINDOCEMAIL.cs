﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using CRF.BLL.Clients;
using CRFView.Adapters.Clients.Client_Management;
using System.IO;
using System.Text.RegularExpressions;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices.LienInvoicesVM;

namespace CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices
{
    public class ADVWCLIENTFINDOCEMAIL
    {
        #region Properties
        public int reportid { get; set; }
        public string batchid { get; set; }
        public string taskname { get; set; }
        public System.DateTime startdate { get; set; }
        public System.DateTime enddate { get; set; }
        public int status { get; set; }
        public string message { get; set; }
        public System.DateTime dateviewed { get; set; }
        public System.DateTime emaildate { get; set; }
        public decimal DocumentAmt { get; set; }
        public decimal TotRcvAmt { get; set; }
        public decimal TotAdjAmt { get; set; }
        public decimal TotalDue { get; set; }
        public System.DateTime DocumentDate { get; set; }
        public string ClientCode { get; set; }
        public string BranchNumber { get; set; }
        public string ClientName { get; set; }
        public string PhoneNo { get; set; }
        public string InvoiceName { get; set; }
        public string InvoiceAttn { get; set; }
        public string InvoiceEmail { get; set; }
        public bool IsPaid { get; set; }
        public int ClientId { get; set; }
        public int ClientFinDocId { get; set; }
        public int HistoryId { get; set; }
        public bool IsBranch { get; set; }
        public System.DateTime ReviewDate { get; set; }
        public bool IsEmailInvoice { get; set; }
        public string InvoicePeriod { get; set; }
        public System.DateTime InvoiceFromDate { get; set; }
        public System.DateTime InvoiceThruDate { get; set; }
        #endregion

        #region CRUD
        public static List<ADVWCLIENTFINDOCEMAIL> ListADVWCLIENTFINDOCEMAIL()
        {
            ADVWCLIENTFINDOCEMAIL myentity = new ADVWCLIENTFINDOCEMAIL();
            TableView EntityList;
            
            try
            {
                string sql = "SELECT * FROM VWCLIENTFINDOCEMAIL where DocumentDate > DateAdd(d, -60, GetDate()) AND ( DateViewed Is Null )";
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADVWCLIENTFINDOCEMAIL> CTList = (from DataRow dr in dt.Rows
                                                    select new ADVWCLIENTFINDOCEMAIL()
                                                    {
                                                        ClientFinDocId = Convert.ToInt32(dr["ClientFinDocId"]),
                                                        ClientCode = Convert.ToString(dr["ClientCode"]),
                                                        ClientName = Convert.ToString(dr["ClientName"]),
                                                        DocumentDate = ((dr["DocumentDate"] != DBNull.Value ? Convert.ToDateTime(dr["DocumentDate"]) : DateTime.MinValue)),
                                                        IsEmailInvoice = ((dr["IsEmailInvoice"] != DBNull.Value ? Convert.ToBoolean(dr["IsEmailInvoice"]) : false)),
                                                        ReviewDate = ((dr["ReviewDate"] != DBNull.Value ? Convert.ToDateTime(dr["ReviewDate"]) : DateTime.MinValue)),
                                                        DocumentAmt = ((dr["DocumentAmt"] != DBNull.Value ? Convert.ToDecimal( dr["DocumentAmt"].ToString()) : 0)),
                                                        TotalDue = ((dr["TotalDue"] != DBNull.Value ? Convert.ToDecimal(dr["TotalDue"].ToString()) : 0)),
                                                        IsPaid = ((dr["IsPaid"] != DBNull.Value ? Convert.ToBoolean(dr["IsPaid"]) : false)),
                                                        emaildate = ((dr["emaildate"] != DBNull.Value ? Convert.ToDateTime(dr["emaildate"]) : DateTime.MinValue)),
                                                        dateviewed = ((dr["dateviewed"] != DBNull.Value ? Convert.ToDateTime(dr["dateviewed"]) : DateTime.MinValue)),
                                                        InvoiceEmail = Convert.ToString(dr["InvoiceEmail"]),
                                                        HistoryId = Convert.ToInt32(dr["HistoryId"])
                                                    }).ToList();

                return CTList;
            }
            catch (Exception ex)
            {
                return (new List<ADVWCLIENTFINDOCEMAIL>());
            }
        }

        public static List<ADVWCLIENTFINDOCEMAIL> FilterListADVWCLIENTFINDOCEMAIL(string RadioSelection, string FromDate, string ToDate, string TxtSearch)
        {
            ADVWCLIENTFINDOCEMAIL myentity = new ADVWCLIENTFINDOCEMAIL();
           var myfilter = new HDS.DAL.COMMON.FilterBuilder();
           TableView EntityList;
            try
            {
                string sql = "SELECT * FROM VWCLIENTFINDOCEMAIL";// ORDER BY CLIENTCODE";
                if(RadioSelection== "ReviewDate")
                {
                    sql += " WHERE ReviewDate >= '" + FromDate+"'";
                    sql += "  AND ReviewDate < '" + ToDate + "'";
                }
                else
                {
                    sql += " WHERE DocumentDate >= '"+  FromDate + "'";
                    sql += "  AND DocumentDate <= '" + ToDate + "'";
                }
                if(TxtSearch !="" && TxtSearch.Length>1)
                {
                    myfilter.Clear();
                    myfilter.SearchAnyWords("ClientCode", TxtSearch, "OR");
                    myfilter.SearchAnyWords("ClientName", TxtSearch, "OR");
                    myfilter.SearchAnyWords("InvoiceEmail", TxtSearch, "OR");
                    myfilter.SearchAnyWords("ClientFinDocId", TxtSearch, "");
                    var ad = myfilter.GetFilter();
                    sql += " And (" + myfilter.GetFilter() + ")";
                }
                if(RadioSelection == "NotViewed")
                {
                    sql += "  AND ( DateViewed Is Null ) ";


                }
                if (RadioSelection == "Viewed")
                {
                    sql += "  AND ( DateViewed Is Not Null ) ";
                }
                if (RadioSelection == "UnPaid")
                {
                    sql += "  AND ( IsPaid <>  1 ) ";
                }
                sql += "ORDER BY CLIENTCODE";
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADVWCLIENTFINDOCEMAIL> CTList = (from DataRow dr in dt.Rows
                                                      select new ADVWCLIENTFINDOCEMAIL()
                                                      {
                                                          ClientFinDocId = Convert.ToInt32(dr["ClientFinDocId"]),
                                                          ClientCode = Convert.ToString(dr["ClientCode"]),
                                                          ClientName = Convert.ToString(dr["ClientName"]),
                                                          DocumentDate = ((dr["DocumentDate"] != DBNull.Value ? Convert.ToDateTime(dr["DocumentDate"]) : DateTime.MinValue)),
                                                          IsEmailInvoice = ((dr["IsEmailInvoice"] != DBNull.Value ? Convert.ToBoolean(dr["IsEmailInvoice"]) : false)),
                                                          ReviewDate = ((dr["ReviewDate"] != DBNull.Value ? Convert.ToDateTime(dr["ReviewDate"]) : DateTime.MinValue)),
                                                          DocumentAmt = ((dr["DocumentAmt"] != DBNull.Value ? Convert.ToDecimal(dr["DocumentAmt"].ToString()) : 0)),
                                                          TotalDue = ((dr["TotalDue"] != DBNull.Value ? Convert.ToDecimal(dr["TotalDue"].ToString()) : 0)),
                                                          IsPaid = ((dr["IsPaid"] != DBNull.Value ? Convert.ToBoolean(dr["IsPaid"]) : false)),
                                                          emaildate = ((dr["emaildate"] != DBNull.Value ? Convert.ToDateTime(dr["emaildate"]) : DateTime.MinValue)),
                                                          dateviewed = ((dr["dateviewed"] != DBNull.Value ? Convert.ToDateTime(dr["dateviewed"]) : DateTime.MinValue)),
                                                          InvoiceEmail = Convert.ToString(dr["InvoiceEmail"]),
                                                          HistoryId = Convert.ToInt32(dr["HistoryId"])
                                                      }).ToList();
                return CTList;
            }
            catch (Exception ex)
            {
                return (new List<ADVWCLIENTFINDOCEMAIL>());
            }
        }
        
        public static LienInvoiceDetailsVM GetDetails(string ClientFinDocId,string HistoryId)
        {
            LienInvoiceDetailsVM ObjLienInvoiceDetailsVM = new LienInvoiceDetailsVM();
            var myclient = new Client();
            ITable myclientI = myclient;

            var myhistory = new Report_History();
            ITable myhistoryI = myhistory;

            var myfindoc = new ClientFinDoc();
            ITable myfindocI = myfindoc;

            HDS.DAL.Provider.DAL.Read(ClientFinDocId, ref myfindocI);
            myfindoc = (ClientFinDoc)myfindocI;

            HDS.DAL.Provider.DAL.Read(HistoryId, ref myhistoryI);
            myhistory = (Report_History)myhistoryI;
            HDS.DAL.Provider.DAL.Read(myfindoc.ClientId.ToString(),ref myclientI);
            myclient = (Client)myclientI;


            //Dim MYSQL As String = "Select top 1 * from clientlieninfo where clientId = '" & myfindoc.ClientId & "' "
            //Dim myds As DataSet = DAL.Provider.DAL.GetDataSet(MYSQL)
            //mylieninfo = New ClientLienInfo
            //DAL.Provider.DAL.FillEntity(myds.Tables(0).Rows(0), mylieninfo)

            var MYSQL = "select top 1 * from clientlieninfo where clientId = '"+ myfindoc.ClientId+"'";
            DataSet myds = HDS.DAL.Provider.DAL.GetDataSet(MYSQL);
            object mylieninfo = new ClientLienInfo(); 
            HDS.DAL.Provider.DAL.FillEntity(myds.Tables[0].Rows[0], ref mylieninfo);
            ClientLienInfo ObjClientLienInfo = (ClientLienInfo)mylieninfo;


            string MYSQL1 = "select * from vwIssues where clientid = "+ myfindoc.ClientId+" order by datecreated desc";
            TableView myTV1;
            myTV1 = DBO.Provider.GetTableView(MYSQL1);
            DataTable dt1 = myTV1.ToTable();
            List<ADvwIssues> IssueList = (from DataRow dr in dt1.Rows
                                                  select new ADvwIssues()
                                                  {
                                                      IssueDescriptionWithStrip = ((dr["IssueDescription"] != DBNull.Value ? CRF.BLL.CRFView.CRFView.GetText(dr["IssueDescription"].ToString()) : "")),
                                                      //IssueDescription = (Regex.Replace(dr["IssueDescription"] != DBNull.Value ? CRF.BLL.CRFView.CRFView.GetText1(dr["IssueDescription"].ToString()) : "".ToString().Replace("\n", "").Replace("\r", "").Replace("\\", "\\\\"), "<.*?>", string.Empty)),
                                                      IssueDescription = ((dr["IssueDescription"] != DBNull.Value ? Regex.Replace(dr["IssueDescription"].ToString().Replace("\n", "").Replace("\r", "").Replace("\\", "\\\\").Replace("@", ""), "<.*?>", string.Empty) : "")),
                                                      DateCreated = ((dr["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dr["DateCreated"]) : DateTime.MinValue)),
                                                      CreatedBy = Convert.ToString(dr["CreatedBy"]),
                                                      AssignedTo = Convert.ToString(dr["AssignedTo"]),
                                                      Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                                  }).ToList();

            string MYSQL2 = "select pkid, description, url from report_historyattachment where historyid = '"+ myhistory.PKID+"'";
            TableView myTV2;
            myTV2 = DBO.Provider.GetTableView(MYSQL2);
            DataTable dt2 = myTV2.ToTable();
            List<ADreport_historyattachment> AttachmentList = (from DataRow dr in dt2.Rows
                                                               select new ADreport_historyattachment()
                                                               {
                                                                   PKID = ((dr["PKID"] != DBNull.Value ? Convert.ToInt32(dr["PKID"]) : 0)),
                                                                   description = Convert.ToString(dr["description"]),
                                                                   url = Convert.ToString(dr["url"])
                                                               }
                                                               ).ToList();


            string MYSQL3 = "select * from ClientFinDocLedger where ClientFinDocId = '" + ClientFinDocId + "'";
            TableView myTV3;
            myTV3 = DBO.Provider.GetTableView(MYSQL3);
            DataTable dt3 = myTV3.ToTable();
            List<ADClientFinDocLedger> PaymentList = (from DataRow dr in dt3.Rows
                                                               select new ADClientFinDocLedger()
                                                               {
                                                                   Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                                                   ClientFinDocId = ((dr["ClientFinDocId"] != DBNull.Value ? Convert.ToInt32(dr["ClientFinDocId"]) : 0)),
                                                                   LedgerDate = ((dr["LedgerDate"] != DBNull.Value ? Convert.ToDateTime(dr["LedgerDate"]) : DateTime.MinValue)),
                                                                   BalAmt = ((dr["BalAmt"] != DBNull.Value ? Convert.ToDecimal(dr["BalAmt"]) : 0)),
                                                                   RcvAmt = ((dr["RcvAmt"] != DBNull.Value ? Convert.ToDecimal(dr["RcvAmt"]): 0)),
                                                                   AdjAmt = ((dr["AdjAmt"] != DBNull.Value ? Convert.ToDecimal(dr["AdjAmt"]): 0)),

                                                               }
                                                               ).ToList();
 
            ObjLienInvoiceDetailsVM.ObjClient = myclient;
            ObjLienInvoiceDetailsVM.ObjClientFinDoc = myfindoc;
            ObjLienInvoiceDetailsVM.ObjClientLienInfo = ObjClientLienInfo;
            ObjLienInvoiceDetailsVM.ObjReportHistory = myhistory;
            ObjLienInvoiceDetailsVM.Issues = IssueList;
            ObjLienInvoiceDetailsVM.Attachments = AttachmentList;
            ObjLienInvoiceDetailsVM.Payments = PaymentList;


            return ObjLienInvoiceDetailsVM;
        }
        #endregion

        #region Local Methods
        static string StripHTML(string inputString)
        {

          
            string temp1 = Regex.Replace(inputString, @"/\{\*?\\[^{}]+}|[{}]|\\\n?[A-Za-z]+\n?(?:-?\d+)?[ ]?/g", string.Empty);
            byte[] bytes = System.Text.Encoding.ASCII.GetBytes(inputString);
            string temp = System.Text.Encoding.GetEncoding("utf-8").GetString(bytes);
            
            // string temp = rtf;
            int cutLength = temp.Length;
            if (cutLength > 200)
            {
                cutLength = 200;
            }
            temp = temp.Substring(0, cutLength) + " ....";
            return temp;
        }
        
        #endregion

    }
}
