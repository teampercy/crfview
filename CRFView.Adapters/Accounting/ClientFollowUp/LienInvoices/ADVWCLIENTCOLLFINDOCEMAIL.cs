﻿//using CRF.BLL.ClientView.Collection;
using CRF.BLL;
using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using CRF.BLL.Reports;
using CRFView.Adapters.Accounting.AccountingCollections.PrintInvoice.ViewModels;
using CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices.CollectionInvoicesVM;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CRFView.Adapters.Accounting.ClientFollowUp.LienInvoices
{
    public class ADVWCLIENTCOLLFINDOCEMAIL
    {
        #region Properties
        public int BillingClientId { get; set; }
        public int ContractId { get; set; }
        public decimal TotRcvAmt { get; set; }
        public decimal TotAdjAmt { get; set; }
        public decimal TotalDue { get; set; }
        public string InvoiceName { get; set; }
        public string InvoiceAttn { get; set; }
        public string InvoiceAddr1 { get; set; }
        public string InvoiceAddr2 { get; set; }
        public string InvoiceCity { get; set; }
        public string InvoiceState { get; set; }
        public string InvoiceZip { get; set; }
        public string CheckNo { get; set; }
        public bool IsBalanceDeduct { get; set; }
        public string Note { get; set; }
        public int ClientFinDocId { get; set; }
        public decimal DocumentAmt { get; set; }
        public System.DateTime DocumentDate { get; set; }
        public string TrxPeriod { get; set; }
        public decimal TaxRate { get; set; }
        public decimal DiscRate { get; set; }
        public decimal PmtAmt { get; set; }
        public decimal FeeAmt { get; set; }
        public decimal InterestShareAmt { get; set; }
        public decimal CourtCostAmt { get; set; }
        public decimal AttyFeeAmt { get; set; }
        public decimal RcvByAgencyAmt { get; set; }
        public decimal RcvByClientAmt { get; set; }
        public decimal FeeByAgencyAmt { get; set; }
        public decimal FeeByClientAmt { get; set; }
        public decimal DueFromClientAmt { get; set; }
        public decimal RemitToClientAmt { get; set; }
        public decimal FeesEarnedAmt { get; set; }
        public decimal FeesDeductedAmt { get; set; }
        public bool IsGrossRemit { get; set; }
        public bool IsIndStmt { get; set; }
        public string StmtGrouping { get; set; }
        public bool IsIndPaidAgencyStmt { get; set; }
        public bool IsIndPaidClientStmt { get; set; }
        public System.DateTime InvoiceFromDate { get; set; }
        public System.DateTime InvoiceThruDate { get; set; }
        public bool IsSeparateStmt { get; set; }
        public string InvoiceCycle { get; set; }
        public string InvoiceBreak { get; set; }
        public int FinDocTypeId { get; set; }
        public string ClientNumber { get; set; }
        public string InvoicePeriod { get; set; }
        public bool IsInterestShared { get; set; }
        public bool IsShowAttyFeesOnStmt { get; set; }
        public bool IsSeparateLegalStmt { get; set; }
        public bool IsCustomInvoice { get; set; }
        public bool IsEmailInvoice { get; set; }
        public int TaskHistoryId { get; set; }
        public bool IsPaid { get; set; }
        public string InvoiceEmail { get; set; }
        public int ClientId { get; set; }
        public string ClientCode { get; set; }
        public string ClientName { get; set; }
        public int reportid { get; set; }
        public string batchid { get; set; }
        public string taskname { get; set; }
        public System.DateTime startdate { get; set; }
        public System.DateTime enddate { get; set; }
        public int status { get; set; }
        public string message { get; set; }
        public System.DateTime dateviewed { get; set; }
        public System.DateTime emaildate { get; set; }
        public int HistoryId { get; set; }
        public bool IsInvoice { get; set; }
        public string vendornumber { get; set; }
        public System.DateTime ReviewDate { get; set; }

        static  bool IsEmailRun = false;
       static ClientCollFinDoc myfindoc = new ClientCollFinDoc();
        static DataRow INVOICE;
       static CRF.BLL.Reports.ClientReport myreport = new ClientReport();
        static CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
        //Public Const  VariantType vbNull  = VariantType.Null;

        #endregion

        #region CRUD
        public static List<ADVWCLIENTCOLLFINDOCEMAIL> ListVWCLIENTCOLLFINDOCEMAIL()
        {
            ADVWCLIENTCOLLFINDOCEMAIL myentity = new ADVWCLIENTCOLLFINDOCEMAIL();
            TableView EntityList;
            try
            {
                string sql = "SELECT * FROM VWCLIENTCOLLFINDOCEMAIL where DocumentDate > DateAdd(d, -60, GetDate())AND ( DateViewed Is Null )";
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADVWCLIENTCOLLFINDOCEMAIL> CTList = (from DataRow dr in dt.Rows
                                                      select new ADVWCLIENTCOLLFINDOCEMAIL()
                                                      {
                                                          ClientFinDocId = Convert.ToInt32(dr["ClientFinDocId"]),
                                                          ClientCode = Convert.ToString(dr["ClientCode"]),
                                                          ClientName = Convert.ToString(dr["ClientName"]),
                                                          ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
                                                          DocumentDate = ((dr["DocumentDate"] != DBNull.Value ? Convert.ToDateTime(dr["DocumentDate"]) : DateTime.MinValue)),
                                                          IsEmailInvoice = ((dr["IsEmailInvoice"] != DBNull.Value ? Convert.ToBoolean(dr["IsEmailInvoice"]) : false)),
                                                          ReviewDate = ((dr["ReviewDate"] != DBNull.Value ? Convert.ToDateTime(dr["ReviewDate"]) : DateTime.MinValue)),
                                                          DocumentAmt = ((dr["DocumentAmt"] != DBNull.Value ? Convert.ToDecimal(dr["DocumentAmt"].ToString()) : 0)),
                                                          TotalDue = ((dr["TotalDue"] != DBNull.Value ? Convert.ToDecimal(dr["TotalDue"].ToString()) : 0)),
                                                          IsPaid = ((dr["IsPaid"] != DBNull.Value ? Convert.ToBoolean(dr["IsPaid"]) : false)),
                                                          emaildate = ((dr["emaildate"] != DBNull.Value ? Convert.ToDateTime(dr["emaildate"]) : DateTime.MinValue)),
                                                          dateviewed = ((dr["dateviewed"] != DBNull.Value ? Convert.ToDateTime(dr["dateviewed"]) : DateTime.MinValue)),
                                                          InvoiceEmail = Convert.ToString(dr["InvoiceEmail"]),
                                                          HistoryId = Convert.ToInt32(dr["HistoryId"])
                                                      }).ToList();

                return CTList;
            }
            catch (Exception ex)
            {
                return (new List<ADVWCLIENTCOLLFINDOCEMAIL>());
            }
        }

        public static List<ADVWCLIENTCOLLFINDOCEMAIL> FilterListADVWCLIENTFINDOCEMAIL(string RadioSelection,
                                                     string FromDate, string ToDate, string TxtSearch,
                                                     string chkRemit, string chkGross)
        {
            ADVWCLIENTCOLLFINDOCEMAIL myentity = new ADVWCLIENTCOLLFINDOCEMAIL();
            var myfilter = new HDS.DAL.COMMON.FilterBuilder();
            TableView EntityList;
            try
            {
                string sql = "SELECT * FROM VWCLIENTCOLLFINDOCEMAIL";// ORDER BY CLIENTCODE";
                if (RadioSelection == "ReviewDate")
                {
                    sql += " WHERE ReviewDate >= '" + FromDate + "'";
                    sql += "  AND ReviewDate < '" + ToDate + "'";
                }
                else
                {
                    sql += " WHERE DocumentDate >= '" + FromDate + "'";
                    sql += "  AND DocumentDate <= '" + ToDate + "'";
                }
                if (TxtSearch != "" && TxtSearch.Length > 1)
                {
                    myfilter.Clear();
                    myfilter.SearchAnyWords("ClientCode", TxtSearch, "OR");
                    myfilter.SearchAnyWords("ClientName", TxtSearch, "OR");
                    myfilter.SearchAnyWords("InvoiceEmail", TxtSearch, "OR");
                    myfilter.SearchAnyWords("ClientFinDocId", TxtSearch, "");
                    var ad = myfilter.GetFilter();
                    sql += " And (" + myfilter.GetFilter() + ")";
                }
                if (RadioSelection == "NotViewed")
                {
                    sql += "  AND ( DateViewed Is Null ) ";


                }
                if (RadioSelection == "Viewed")
                {
                    sql += "  AND ( DateViewed Is Not Null ) ";
                }


                if(chkRemit == "on")
                {
                    sql += "  AND ( IsInvoice = 0 and ISGROSSREMIT = 0) ";
                }
                if(chkGross == "on")
                {
                    sql += "  AND ( IsInvoice = 1 and ISGROSSREMIT = 1) ";
                }


                if (RadioSelection == "UnPaid")
                {
                    sql += "  AND ( IsPaid <>  1 ) ";
                }
                sql += "ORDER BY CLIENTCODE";
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADVWCLIENTCOLLFINDOCEMAIL> CTList = (from DataRow dr in dt.Rows
                                                      select new ADVWCLIENTCOLLFINDOCEMAIL()
                                                      {
                                                          ClientFinDocId = Convert.ToInt32(dr["ClientFinDocId"]),
                                                          ClientCode = Convert.ToString(dr["ClientCode"]),
                                                          ClientName = Convert.ToString(dr["ClientName"]),
                                                          ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
                                                          DocumentDate = ((dr["DocumentDate"] != DBNull.Value ? Convert.ToDateTime(dr["DocumentDate"]) : DateTime.MinValue)),
                                                          IsEmailInvoice = ((dr["IsEmailInvoice"] != DBNull.Value ? Convert.ToBoolean(dr["IsEmailInvoice"]) : false)),
                                                          ReviewDate = ((dr["ReviewDate"] != DBNull.Value ? Convert.ToDateTime(dr["ReviewDate"]) : DateTime.MinValue)),
                                                          DocumentAmt = ((dr["DocumentAmt"] != DBNull.Value ? Convert.ToDecimal(dr["DocumentAmt"].ToString()) : 0)),
                                                          TotalDue = ((dr["TotalDue"] != DBNull.Value ? Convert.ToDecimal(dr["TotalDue"].ToString()) : 0)),
                                                          IsPaid = ((dr["IsPaid"] != DBNull.Value ? Convert.ToBoolean(dr["IsPaid"]) : false)),
                                                          emaildate = ((dr["emaildate"] != DBNull.Value ? Convert.ToDateTime(dr["emaildate"]) : DateTime.MinValue)),
                                                          dateviewed = ((dr["dateviewed"] != DBNull.Value ? Convert.ToDateTime(dr["dateviewed"]) : DateTime.MinValue)),
                                                          InvoiceEmail = Convert.ToString(dr["InvoiceEmail"])
                                                      }).ToList();
                return CTList;
            }
            catch (Exception ex)
            {
                return (new List<ADVWCLIENTCOLLFINDOCEMAIL>());
            }
        }

        public static CollectionInvoiceDetailsVM GetDetails(string ClientId,string ClientFinDocId, string HistoryId)
        {
            CollectionInvoiceDetailsVM ObjCollInvoiceDetailsVM = new CollectionInvoiceDetailsVM();
            var myclient = new Client();
            ITable myclientI = myclient;

            var myhistory = new Report_History();
            ITable myhistoryI = myhistory;

            var myfindoc = new ClientCollFinDoc();
            ITable myfindocI = myfindoc;

            HDS.DAL.Provider.DAL.Read(ClientFinDocId, ref myfindocI);
            myfindoc = (ClientCollFinDoc)myfindocI;

            HDS.DAL.Provider.DAL.Read(HistoryId, ref myhistoryI);
            myhistory = (Report_History)myhistoryI;

            HDS.DAL.Provider.DAL.Read(ClientId, ref myclientI);
            myclient = (Client)myclientI;

            var MYSQL = "Select * from vwIssues where clientid ="+ ClientId+ " order by datecreated desc";
            var myds1 = HDS.DAL.Provider.DAL.GetDataSet(MYSQL);
            DataTable dt1 = myds1.Tables[0];
            List<ADvwIssues> IssueList = (from DataRow dr in dt1.Rows
                                          select new ADvwIssues()
                                          {
                                              IssueDescriptionWithStrip = ((dr["IssueDescription"] != DBNull.Value ? CRF.BLL.CRFView.CRFView.GetText(dr["IssueDescription"].ToString()) : "")),
                                              //IssueDescriptionWithStrip = ((dr["IssueDescription"] != DBNull.Value ? StripHTML(dr["IssueDescription"].ToString()) : "")),
                                              IssueDescription = ((dr["IssueDescription"] != DBNull.Value ? Regex.Replace(dr["IssueDescription"].ToString().Replace("\n", "").Replace("\r", "").Replace("\\", "\\\\").Replace("@",""), "<.*?>", string.Empty) : "")),
                                              //IssueDescription1 = Regex.Replace(IssueDescription, @"[\u0000-\u0008\u000A-\u001F\u0100-\uFFFF]", ""),
                                              DateCreated = ((dr["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dr["DateCreated"]) : DateTime.MinValue)),
                                              CreatedBy = Convert.ToString(dr["CreatedBy"]),
                                              AssignedTo = Convert.ToString(dr["AssignedTo"]),
                                              Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                          }).ToList();


            string MYSQL2 = "select pkid,description,url from report_historyattachment where historyid = " + myhistory.PKID + "";
            TableView myTV2;
            myTV2 = DBO.Provider.GetTableView(MYSQL2);
            DataTable dt2 = myTV2.ToTable();
            List<ADreport_historyattachment> AttachmentList = (from DataRow dr in dt2.Rows
                                                               select new ADreport_historyattachment()
                                                               {
                                                                   PKID = ((dr["PKID"] != DBNull.Value ? Convert.ToInt32(dr["PKID"]) : 0)),
                                                                   description = Convert.ToString(dr["description"]),
                                                                   url = Convert.ToString(dr["url"])
                                                               }
                                                               ).ToList();

            string MYSQL3 = "select * from ClientCollFinDocLedger where ClientFinDocId = '" + ClientFinDocId + "'";
            TableView myTV3;
            myTV3 = DBO.Provider.GetTableView(MYSQL3);
            DataTable dt3 = myTV3.ToTable();
            List<ADClientCollFinDocLedger> PaymentList = (from DataRow dr in dt3.Rows
                                                      select new ADClientCollFinDocLedger()
                                                      {
                                                          Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                                          ClientFinDocId = ((dr["ClientFinDocId"] != DBNull.Value ? Convert.ToInt32(dr["ClientFinDocId"]) : 0)),
                                                          LedgerDate = ((dr["LedgerDate"] != DBNull.Value ? Convert.ToDateTime(dr["LedgerDate"]) : DateTime.MinValue)),
                                                          BalAmt = ((dr["BalAmt"] != DBNull.Value ? Convert.ToDecimal(dr["BalAmt"]) : 0)),
                                                          RcvAmt = ((dr["RcvAmt"] != DBNull.Value ? Convert.ToDecimal(dr["RcvAmt"]) : 0)),
                                                          AdjAmt = ((dr["AdjAmt"] != DBNull.Value ? Convert.ToDecimal(dr["AdjAmt"]) : 0))

                                                      }
                                                               ).ToList();

            ObjCollInvoiceDetailsVM.ObjClient = myclient;
            ObjCollInvoiceDetailsVM.ObjClientCollFinDoc = myfindoc;
            ObjCollInvoiceDetailsVM.ObjReportHistory = myhistory;
            ObjCollInvoiceDetailsVM.Issues = IssueList;
            ObjCollInvoiceDetailsVM.Attachments = AttachmentList;
            ObjCollInvoiceDetailsVM.CollPayments = PaymentList;


            return ObjCollInvoiceDetailsVM;
        }
        #endregion

        #region Local Methods
        static string StripHTML(string inputString)
        {
            string temp = Regex.Replace(inputString, "<.*?>", string.Empty);
            int cutLength = temp.Length;
            if (cutLength > 200)
            {
                cutLength = 200;
            }
            temp = temp.Substring(0, cutLength) + " ....";
            return temp;
        }

        private static  CRF.BLL.COMMON.PrintMode  GetPrintMode(PrintInvoiceVM objPrintInvoiceVM)
        {
            CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;

            if (objPrintInvoiceVM.RbtPrintOption == "Send To PDF")
            {
                mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
            }
             if(IsEmailRun == true)
            {
                mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
            }
            return  mymode;
        }


       
             
        public static void CreateAttachment( string areportname,string afilepath)
        {
            if(IsEmailRun == false)
            {
                return;
            }
            if(System.IO.File.Exists(afilepath) == true && IsEmailRun == true)
            { 
                myreport.CreateAttachment(areportname, afilepath);
            }
            System.IO.File.Delete(afilepath);
        }
        public static void  CreateReportHistory(int clientid)
          {
             
               // IsEmailRun = false;
                    if (IsEmailRun ==false)
                       {
                            return;
                        }
          
                CRF.BLL.Reports.ClientReport myreport = new ClientReport();
                string MYBATCHID = "";
                var myfindoc = new ClientCollFinDoc();
                myreport = new  CRF.BLL.Reports.ClientReport ("COLL-INVOICE", MYBATCHID, clientid, myfindoc.ClientNumber);
                myreport.UpdateStatus(myfindoc.Id, 1, 1);
                     if (myfindoc.IsEmailInvoice == true)
                        {
                               myreport.CreateRecipient(myfindoc.InvoiceEmail, myfindoc.InvoiceName);
                        }
        
              }

        public static string CloseReportHistory(int status, string message)
        {
            if( IsEmailRun ==false)
            {
                return "";
            }
            if(myfindoc.IsEmailInvoice == false)
            {
                return "";
            }
            if (myreport.History.PKID<1)
            {
                return "";
            }
            myreport.CloseHistory(status, message);
            return "";
            
        }

  

        public static string PrintInvoiceOnly(string p_sStmtType, DataRow aStmtClient, DataView aStmtView, CRF.BLL.COMMON.PrintMode p_mymode, string sFromDate, string sThruDate)
        
        {
            ClientCollFinDoc myfindoc = new ClientCollFinDoc();
            PrintInvoiceVM objPrintInvoiceVM = new PrintInvoiceVM();


            string str;
            str = aStmtClient["StmtGrouping"].ToString();
            string strAI = str.Substring(0, 4);
            if (myfindoc.IsEmailInvoice == true && GetPrintMode(objPrintInvoiceVM) == CRF.BLL.COMMON.PrintMode.PrintDirect)
            {
                return  "";
            }
           // If myfindoc.IsEmailInvoice = True And GetPrintMode() = BLL.Common.PrintMode.PrintDirect Then Exit Sub
            string myreportpath = string.Empty;
            string myprefix = aStmtClient["ClientNumber"] + "-" + aStmtClient["ClientFinDocId"];

            //Select Case Left(aStmtClient("StmtGrouping"), 4)

            switch (Convert.ToString(aStmtClient["StmtGrouping"]).Substring(0, 4).ToUpper())
            {
                case "PMTC":
                    //Console.WriteLine("Excellent!");
                   // break;
                    if (Convert.ToBoolean(aStmtClient["IsIndPaidClientStmt"])  == true)
                    {
                        if((Convert.ToBoolean(aStmtClient["IsShowAttyFeesOnStmt"]) == true))
                        {
                            myreportpath = Globals.RenderC1Report(user, aStmtView.ToTable(), "CollectionInvoices.xml", "InvoiceAF", p_mymode, myprefix + (p_sStmtType == "-Legal" ? "-" + p_sStmtType : "") + "-" + strAI  + "-Ind");


                        }
                        
                            else
                        {
                            myreportpath = Globals.RenderC1Report(user,aStmtView.ToTable(), "CollectionInvoices.xml", "Invoice", p_mymode, myprefix + (p_sStmtType == "-Legal" ? "-" + p_sStmtType: "") + "-" + strAI + "-Ind");

                         //   aStmtView dv = ds.Tables[0].DefaultView;



                            myreportpath = Globals.RenderC1Report(user, aStmtView.ToTable() , "CollectionInvoices.xml", "Invoice", p_mymode, myprefix + (p_sStmtType == "- Legal" ? " - " + p_sStmtType: "") + " - " + strAI + " - Ind", sFromDate, sThruDate);
                        }
                      
                    }
                    else
                   
                    {
                        if ((Convert.ToBoolean(aStmtClient["IsShowAttyFeesOnStmt"]) == true))
                        {
                            myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "InvoiceAF", p_mymode, myprefix + (p_sStmtType == "-Legal" ? "-" + p_sStmtType : ""));
                            myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "Invoice", p_mymode, myprefix + (p_sStmtType == "-Legal" ? "-" + p_sStmtType : ""));

                        }

                        else
                        {
                            myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "Invoice", p_mymode, myprefix + (p_sStmtType == "-Legal" ? "-" + p_sStmtType: ""));

                        }

                    }
                    break;
            }

            if(System.IO.File.Exists(myreportpath) && IsEmailRun == true)
           
            {
                CreateAttachment("Collection Invoice (" + aStmtClient["ClientFinDocId"] + ")", myreportpath);
            }

            return myreportpath;

        }



        public static string PrintInvoice(PrintInvoiceVM objPrintInvoiceVM)
        {
       
          
            string result = "";
            string pdfPath = null;
            string sFromDate = null;
            string sThruDate = null;
            string MYBATCHID = null;
            object MYTASKDEF = new Tasks_Task();
            string ClientId = "";
           //string InvoiceFromDate = objPrintInvoiceVM.objADvwClientCollFinDocEmail.InvoiceFromDate.ToShortDateString();

            if (objPrintInvoiceVM.chkAllClient == false)
            {
                ClientId = objPrintInvoiceVM.ClientId;
            }
            else
            {
                ClientId = null;
            }

                if (objPrintInvoiceVM.RbtPrintOption == "Send To Printer")
                {
                    objPrintInvoiceVM.RbtPrintOption = "1";
                }
                else if (objPrintInvoiceVM.RbtPrintOption == "Send To PDF")
                {
                    objPrintInvoiceVM.RbtPrintOption = "2";
                }
                else if (objPrintInvoiceVM.RbtPrintOption == "Email")
                {
                    objPrintInvoiceVM.RbtPrintOption = "3";
                }
                else if (objPrintInvoiceVM.RbtPrintOption == "No Report")
                {
                    objPrintInvoiceVM.RbtPrintOption = "4";
                }

            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            string path = user.SettingsFolder;
            string ReportName = "CollectionInvoices.xml";

            string ParameterList = "ClientId" + ',' + "Invoicecycle" + ',' + "InvoiceFromDate" + ',' + "InvoiceToDate" + ',' + "ReportDestination" + ',' + "chkInvoiceOnly" + ',' + "chkExcludeInvoice";
            string ParameterValues = ClientId + ',' + objPrintInvoiceVM.InvoiceCycleId + ',' + objPrintInvoiceVM.objADvwClientCollFinDocEmail.InvoiceFromDate.ToShortDateString() + ',' + objPrintInvoiceVM.objADvwClientCollFinDocEmail.InvoiceThruDate.ToShortDateString() + ',' + objPrintInvoiceVM.RbtPrintOption + ',' + objPrintInvoiceVM.chkInvoiceOnly + ',' + objPrintInvoiceVM.chkExcludeInvoice;

            // ITable tblPrintReportQueue;
            //string RecentClientId;
            //Client recent = new Client();
            AutoMapper.Mapper.CreateMap<PrintInvoiceVM, PrintReportQueue>();
            //ObjSource.IsBranch = true;
            PrintReportQueue objPrintReportQueue = new PrintReportQueue();
            objPrintReportQueue = AutoMapper.Mapper.Map<PrintReportQueue>(objPrintInvoiceVM);
            objPrintReportQueue.RequestedDate = DateTime.Now;
            objPrintReportQueue.ReportName = ReportName;
            objPrintReportQueue.TabValue = "AccCollectionPrintInvoice";
            objPrintReportQueue.ParameterList = ParameterList;
            objPrintReportQueue.ParameterValues = ParameterValues;
            //objPrintReportQueue.ChkCsvReport = objPrintInvoiceVM.chkCsv;
            objPrintReportQueue.ReportStoragePath = path;
            objPrintReportQueue.ReportType = objPrintInvoiceVM.RbtPrintOption;
            objPrintReportQueue.UserName = user.UserName;
            objPrintReportQueue.UserEmail = user.Email;
            objPrintReportQueue.UserId = user.Id;
            ITable tblPrintReportQueue = (ITable)objPrintReportQueue;
            DBO.Provider.Create(ref tblPrintReportQueue);
            try
                {


                MYBATCHID = CRF.BLL.Reports.ClientReport.GetUniqueId();

                if(objPrintInvoiceVM.RbtPrintOption == "Email")
                {
                    IsEmailRun = true;
                }
                  
                //IsEmailRun =Convert.ToBoolean(objPrintInvoiceVM.RbtPrintOptionEmail);

                CRF.BLL.CRFDB.Provider.GetItem((MYTASKDEF as Tasks_Task).TableObjectName, ref MYTASKDEF, "PREFIX", "COLLECTIONINVOICE");

                //CRF.BLL.Reports.ClientReport MYBATCHID = new CRF.BLL.Reports.ClientReport();
                //IsEmailRun = Convert.ToBoolean(objPrintInvoiceVM.RbtPrintOption);


                //CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                    var Sp_OBJ = new cview_Accounting_GetFinDocs();

                    //TableView mytableview = DBO.Provider.GetTableView(Sp_OBJ);
                        
                   
                    //    Sp_OBJ.ClientId = objPrintInvoiceVM.ClientId;
                    Sp_OBJ.DOCUMENTFROM = objPrintInvoiceVM.objADvwClientCollFinDocEmail.InvoiceFromDate.ToShortDateString();
                    Sp_OBJ.DOCUMENTTO = objPrintInvoiceVM.objADvwClientCollFinDocEmail.InvoiceThruDate.ToShortDateString();
                    sFromDate = objPrintInvoiceVM.objADvwClientCollFinDocEmail.InvoiceFromDate.ToShortDateString();
                    sThruDate = objPrintInvoiceVM.objADvwClientCollFinDocEmail.InvoiceThruDate.ToShortDateString();
                //Sp_OBJ.INVOICECYCLE = objPrintInvoiceVM.Id.ToString();
                //if (objPrintInvoiceVM.Id == 1)
                //{
                //    Sp_OBJ.INVOICECYCLE = "M";
                //}
                //if (objPrintInvoiceVM.Id == 2)
                //{
                //    Sp_OBJ.INVOICECYCLE = "S";
                //}

                //if (objPrintInvoiceVM.Id == 3)
                //{
                //    Sp_OBJ.INVOICECYCLE = "W";
                //}
                Sp_OBJ.INVOICECYCLE = objPrintInvoiceVM.InvoiceCycleId;

                if (objPrintInvoiceVM.chkAllClient == false)
                {
                    Sp_OBJ.ClientId = objPrintInvoiceVM.ClientId;
                }
               

                //TableView myds = CRF.BLL.Providers.DBO.Provider.GetTableView(Sp_OBJ);
                //TableView myview1 = CRF.BLL.Providers.DBO.Provider.GetTableView(Sp_OBJ);
                    DataSet myds = DBO.Provider.GetDataSet(Sp_OBJ);

                if (myds.Tables[0].Rows.Count == 0)
                {
                    return "";
                }
                // Dim mydr As System.Data.DataRow
                //DataTable MYINVOICES = myds.Tables[0];

                //    TableView myview = new TableView(myds.Tables[1]);
                //    TableView myviewNonLegal = new TableView(myds.Tables[2]);
                //    TableView myviewLegal = new TableView(myds.Tables[3]);
                //    TableView myviewCF = new TableView(myds.Tables[4]);

             
                //    string sInvoiceType;
                //    bool bIsInvoiceOnly;
                //    bool bIsExcludeInvoice;


                ////bIsInvoiceOnly = ProcessDialog.IsInvoiceOnly.Checked
                ////bIsExcludeInvoice = ProcessDialog.IsExcludeInvoice.Checked

                //bIsInvoiceOnly = objPrintInvoiceVM.chkInvoiceOnly;
                //bIsExcludeInvoice = objPrintInvoiceVM.chkExcludeInvoice;

                //    CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;

                //    if (objPrintInvoiceVM.RbtPrintOption == "Send To PDF")
                //    {
                //        mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                //    }


                //    mymode = GetPrintMode(objPrintInvoiceVM);

           

                //    // Invoice Only
                //if (objPrintInvoiceVM.chkInvoiceOnly == true)
                //    {
                    
                //    foreach (DataRow mydr in myds.Tables[5].Rows)
                //        {
                //            DataRow INVOICE = mydr;


                //        //var objfindoc = new ClientFinDoc();
                //        //ITable Imyfindoc = objfindoc;
                //        //DBO.Provider.Read(Convert.ToString(row["FinDocId"]), ref Imyfindoc);
                //        //myfindoc = (ClientFinDoc)Imyfindoc;


                //        var objfindoc = new ClientCollFinDoc();
                //        ITable Imyfindoc = objfindoc;
                //        DBO.Provider.Read(Convert.ToString(INVOICE["ClientFinDocId"]), ref Imyfindoc);
                //        myfindoc = (ClientCollFinDoc)Imyfindoc;

                //        CreateReportHistory(Convert.ToInt32(INVOICE["BillingClientId"]));
                //        //  DBO.Provider.Read("ClientFinDocId", ref Imyfindoc);
                //        myview.RowFilter = "ClientFinDocId='" + INVOICE["ClientFinDocId"] + "'";
                //            myviewNonLegal.RowFilter = "ClientFinDocId='" + INVOICE["ClientFinDocId"] + "'";
                //            myviewLegal.RowFilter = "ClientFinDocId='" + INVOICE["ClientFinDocId"] + "'";
                //            myviewCF.RowFilter = "ClientFinDocId='" + INVOICE["ClientFinDocId"] + "'";

                //            if (Convert.ToBoolean(mydr["IsSeparateLegalStmt"]) == true)
                //            {

                //            pdfPath = PrintInvoiceOnly("Legal", mydr, myviewLegal, mymode, sFromDate, sThruDate);

                //            pdfPath = PrintInvoiceOnly(null, mydr, myviewNonLegal, mymode, sFromDate, sThruDate);


                //            }
                //            else
                //            {

                //            pdfPath = PrintInvoiceOnly(null, mydr, myview, mymode, sFromDate, sThruDate);
                //        }


                //            CloseReportHistory(2, "");
                //        }

                //    }
                //    else
                //    //All
                //    {
                //        foreach (DataRow mydr in myds.Tables[0].Rows)
                //        {
                //            DataRow INVOICE = mydr;
                //        //DataTable myfindoc = new CRF.BLL.CRFDB.TABLES.ClientCollFinDoc();
                //        var objfindoc = new ClientCollFinDoc();
                //        ITable Imyfindoc = objfindoc;
                //        DBO.Provider.Read(Convert.ToString(INVOICE["ClientFinDocId"]), ref Imyfindoc);
                //        myfindoc = (ClientCollFinDoc)Imyfindoc;

                //        //  DBO.Provider.Read("ClientFinDocId", ref Imyfindoc);
                //        myview.RowFilter = "ClientFinDocId='" + INVOICE["ClientFinDocId"] + "'";
                //        myviewNonLegal.RowFilter = "ClientFinDocId='" + INVOICE["ClientFinDocId"] + "'";
                //        myviewLegal.RowFilter = "ClientFinDocId='" + INVOICE["ClientFinDocId"] + "'";
                //        myviewCF.RowFilter = "ClientFinDocId='" + INVOICE["ClientFinDocId"] + "'";



                //        //var objfindoc = new ClientCollFinDoc();
                //        //    ITable Imyfindoc = objfindoc;
                //        //    DBO.Provider.Read("ClientFinDocId", ref Imyfindoc);
                //        //    myview.RowFilter = "ClientFinDocId='" + INVOICE["ClientFinDocId"] + "'";
                //        //    myviewNonLegal.RowFilter = "ClientFinDocId='" + INVOICE["ClientFinDocId"] + "'";
                //        //    myviewLegal.RowFilter = "ClientFinDocId='" + INVOICE["ClientFinDocId"] + "'";
                //        //    myviewCF.RowFilter = "ClientFinDocId='" + INVOICE["ClientFinDocId"] + "'";
                //        if (Convert.ToBoolean(mydr["IsGrossRemit"]) == true)
                //            {

                //                sInvoiceType = "GrossRemit";

                //            }
                //            else if (Convert.ToBoolean(mydr["IsSeparateStmt"]) == true)
                //            {

                //                sInvoiceType = "SeparateStmt";

                //            }
                //            else
                //            {
                //                sInvoiceType = "ComebineAndDeduct";
                //            }

                //            if (Convert.ToBoolean(mydr["IsCustomInvoice"]) == true)
                //            {
                //                PrintCustomStatement(sInvoiceType, mydr, myview, myviewCF, mymode);
                //            }

                //            else if (Convert.ToBoolean(mydr["IsSeparateLegalStmt"]) == true)
                //            {
                //            //  PrintStatement(sInvoiceType, "Legal", mydr, myviewLegal, bIsExcludeInvoice, mymode)
                //            pdfPath = PrintStatement(sInvoiceType, "Legal", mydr, myviewLegal, bIsExcludeInvoice, mymode);
                //            pdfPath = PrintStatement(sInvoiceType, null, mydr, myviewNonLegal, bIsExcludeInvoice, mymode);
                //            }
                //            else
                //            {
                //             pdfPath = PrintStatement(sInvoiceType, null, mydr, myview, bIsExcludeInvoice, mymode);
                //            }




                //       CloseReportHistory(2, "");

                //        }

                //    }
                //    result = "Processed Completed";

                //MYBATCHID2 OBJMYBATCHID2 = new MYBATCHID2();
              // MYBATCHID2 = CRF.BLL.Reports.ClientReport.GetUniqueId;
             


                }
      
  
            catch (Exception ex)
            {
                return "";
            }
            return "1";
        }
        private static string PrintCustomStatement (string p_sInvoiceType,DataRow aStmtClient,DataView aStmtView,DataView aStmtViewCF,CRF.BLL.COMMON.PrintMode p_mymode)
        {

            string str;
            str = aStmtClient["StmtGrouping"].ToString();
            string strAI = str.Substring(0, 4);
            PrintInvoiceVM objPrintInvoiceVM = new PrintInvoiceVM ();
            if ((Convert.ToBoolean(myfindoc.IsEmailInvoice) == true) && (GetPrintMode(objPrintInvoiceVM) == CRF.BLL.COMMON.PrintMode.PrintDirect))
          
            {
                return "";
            }
            string myprefix = aStmtClient["ClientNumber"] + "-" + aStmtClient["ClientFinDocId"];

            string myreportpath = "";
            if(Convert.ToString(aStmtClient["ClientNumber"]) == "NTS532")
                    {
                if((Convert.ToString(aStmtClient["StmtGrouping"]).Substring(0, 4).ToUpper()) == "PMTA")
                {
                 myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "RemittanceIntNoCF", p_mymode, myprefix + (p_sInvoiceType == "-Legal" ? "-" + p_sInvoiceType : "") + "-" + strAI + "-Ind");
                }
                else
                {
                  myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "InvoiceIntNoCF", p_mymode, myprefix + (p_sInvoiceType == "-Legal"? "-" + p_sInvoiceType : "") + "-" + strAI + "-Ind");

                }
                if ((Convert.ToString(aStmtClient["StmtGrouping"]).Substring(0, 4).ToUpper()) == "PMTA")
                {
                   myreportpath = Globals.RenderC1Report(user,aStmtViewCF.Table, "CollectionInvoices.xml", "RemittanceCF", p_mymode, myprefix + (p_sInvoiceType == "-Legal"? "-" + p_sInvoiceType : "") + "-" + strAI + "-Ind");
                    }

            }
            if(System.IO.File.Exists(myreportpath) && IsEmailRun == true)
            {
                if(myfindoc.IsEmailInvoice == true && IsEmailRun == true)
                {
                    myreport.CreateAttachment("Collection Invoice (" + INVOICE["ClientFinDocId"] + ")", myreportpath);
                }
            }
            return myreportpath;
        }
          
        private static string PrintStatement ( string p_sInvoiceType, string p_sStmtType, DataRow aStmtClient,DataView aStmtView, Boolean p_bIsExludeInvoice, CRF.BLL.COMMON.PrintMode p_mymode)
        {
            string str;
            str = aStmtClient["StmtGrouping"].ToString();
            string strAI = str.Substring(0, 4);
            PrintInvoiceVM objPrintInvoiceVM = new PrintInvoiceVM();
            if ((myfindoc.IsEmailInvoice == true) && GetPrintMode(objPrintInvoiceVM) == CRF.BLL.COMMON.PrintMode.PrintDirect)
            {
                return "";
            }
                string myreportpath = "";
                string myprefix = aStmtClient["ClientNumber"] + "-" + aStmtClient["ClientFinDocId"];
               

            switch (p_sInvoiceType)

            { 
                case "GrossRemit":
                //case "GrossRemit":
                        if (Convert.ToBoolean(aStmtClient["IsIntlClient"]) == true && Convert.ToBoolean(aStmtClient["IsInterestShared"]) == true || Convert.ToBoolean(aStmtClient["IsInterestSharedAsPrinc"] )== true)
                            {
                            myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "GrossRemitIntInternational", p_mymode, myprefix + (p_sStmtType == "-Legal" ? "-" + p_sStmtType: ""));



                              }
                        else if (Convert.ToBoolean(aStmtClient["IsIntlClient"]) == true && Convert.ToBoolean(aStmtClient["IsInterestShared"]) == false &&  Convert.ToBoolean(aStmtClient["IsInterestSharedAsPrinc"]) == false)

                        {
                            myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "GrossRemitInternational", p_mymode, myprefix + (p_sStmtType == "-Legal"? "-" + p_sStmtType: ""));
                         }
                        else if (Convert.ToBoolean(aStmtClient["IsIntlClient"]) == false && Convert.ToBoolean(aStmtClient["IsInterestShared"]) == true || Convert.ToBoolean(aStmtClient["IsInterestSharedAsPrinc"]) == true)

                        {
                            myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "GrossRemitInt", p_mymode, myprefix + (p_sStmtType == "-Legal" ? "-" + p_sStmtType: ""));
                        }
                        else if (Convert.ToBoolean(aStmtClient["IsShowAttyFeesOnStmt"]) == true)
                        {
                            myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "GrossRemitAF", p_mymode, myprefix + (p_sStmtType == "-Legal" ? "-" + p_sStmtType: ""));


                        }
                        else
                        {
                            myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "GrossRemit", p_mymode, myprefix + (p_sStmtType == "-Legal" ? "-" + p_sStmtType: ""));
                           }
                    break;

                    case "SeparateStmt":
                        switch (Convert.ToString(aStmtClient["StmtGrouping"]).Substring(0, 4).ToUpper())
                        {
                            case "PMTA":
                                //Console.WriteLine("Excellent!");
                                // break;
                                if (Convert.ToBoolean(aStmtClient["IsIndPaidAgencyStmt"]) == true)
                                {
                                    if ((Convert.ToBoolean(aStmtClient["IsShowAttyFeesOnStmt"]) == true))
                                    {
                                       myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "InvoiceAF", p_mymode, myprefix + (p_sStmtType == "-Legal" ? "-" + p_sStmtType: "") + "-" + strAI + "-Ind");

                                    }
                                    else if(Convert.ToBoolean(aStmtClient["IsIntlClient"]) == true)
                                    {
                                        myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "RemittanceInternational", p_mymode, myprefix + (p_sStmtType == "-Legal" ? "-" + p_sStmtType:"") + "-" + strAI + "-Ind");


                                    }

                                    else
                                    {
                                        myreportpath = Globals.RenderC1Report(user,aStmtView.Table, "CollectionInvoices.xml", "Remittance", p_mymode, myprefix + (p_sStmtType == "-Legal" ? "-" + p_sStmtType: "") + "-" + strAI + "-Ind");


                                    }

                                }
                                else

                                {
                                    if ((Convert.ToBoolean(aStmtClient["IsShowAttyFeesOnStmt"]) == true))
                                    {
                                        myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "RemittanceAF", p_mymode, myprefix + (p_sStmtType == "-Legal"? "-" + p_sStmtType: ""));
                                        }
                                    else if (Convert.ToBoolean(aStmtClient["IsInterestShared"]) == true || Convert.ToBoolean(aStmtClient["IsInterestSharedAsPrinc"]) == true)
                                    {
                                        myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "RemittanceInt", p_mymode, myprefix + (p_sStmtType == "-Legal" ?  "-" + p_sStmtType: ""));
                                      }

                                    else if (Convert.ToBoolean(aStmtClient["IsShowAttyFeesOnStmt"]) == true)
                                    {
                                   
                                    myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "RemittanceInternational", p_mymode, myprefix + (p_sStmtType == "-Legal" ? "-" + p_sStmtType: "") + "-" + strAI + "-Ind");
                        



                                }


                                    else
                                    {
                                        myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "Remittance", p_mymode, myprefix + (p_sStmtType == "-Legal" ? "-" + p_sStmtType: ""));

                                     }

                                }
 
                                break;
                                case "PMTC":

                                if(Convert.ToBoolean(aStmtClient["IsIndPaidClientStmt"]) == true && p_bIsExludeInvoice == false)
                                    {
                                    if(Convert.ToBoolean(aStmtClient["IsShowAttyFeesOnStmt"]) == true)
                                    {
                                        myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "InvoiceAF", p_mymode, myprefix + (p_sStmtType == "-Legal" ? "-" + p_sStmtType: "") + "-" + strAI + "-Ind");

                                     }
                                    else if (Convert.ToBoolean(aStmtClient["IsIntlClient"]) == true)
                                    {
                                        myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "InvoiceInternational", p_mymode, myprefix + (p_sStmtType == "-Legal" ? "-" + p_sStmtType: "") + "-" + strAI + "-Ind");


                                    }
                                    else
                                    {
                                        myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "Invoice", p_mymode, myprefix + (p_sStmtType == "-Legal" ? "-" + p_sStmtType: "") + "-" + strAI + "-Ind");


                                    }

                                }
                                else if(Convert.ToBoolean(aStmtClient["IsIndPaidClientStmt"]) == true && p_bIsExludeInvoice == false)
                                {
                                    if (Convert.ToBoolean(aStmtClient["IsShowAttyFeesOnStmt"]) == true)
                                    {

                                        myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "InvoiceAF", p_mymode, myprefix + (p_sStmtType == "-Legal" ?  "-" + p_sStmtType: ""));
                                    }
                                    else if (Convert.ToBoolean(aStmtClient["IsInterestShared"]) == true || Convert.ToBoolean(aStmtClient["IsInterestSharedAsPrinc"]) == true)
                                        { 
                                         myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "InvoiceInt", p_mymode, myprefix + (p_sStmtType == "-Legal" ?  "-" + p_sStmtType: ""));
                                        }



                                    else if(Convert.ToBoolean(aStmtClient["IsIntlClient"]) == true )
                                    {

                                        myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "InvoiceInternational", p_mymode, myprefix + (p_sStmtType == "-Legal" ? "-" + p_sStmtType: "") + "-" + (Convert.ToString(aStmtClient["StmtGrouping"]).Substring(0, 6).ToUpper() + "-Ind"));
                                                                                                                                                                                                                                                                                                                                    
                                      }
                                    else
                                        {

                                        myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "Invoice", p_mymode, myprefix + (p_sStmtType == "-Legal" ?  "-" + p_sStmtType: ""));

                                      }
                                }
                            break;

                            case "ComebineAndDeduct":
                             if (Convert.ToBoolean(aStmtClient["IsIntlClient"]) == true && Convert.ToBoolean(aStmtClient["IsInterestShared"]) == true || Convert.ToBoolean(aStmtClient["IsInterestSharedAsPrinc"]) == true)
                                    {
                                //myreportpath = Globals.RenderC1Report(aStmtView, "CollectionInvoices.xml", "CombineDeductIntInternational", p_mymode, myprefix + IIf(p_sStmtType = "Legal", "-" + p_sStmtType, ""));
                                myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "CombineDeductIntInternational", p_mymode, myprefix + (p_sStmtType == "Legal" ? "-" + p_sStmtType : ""));

                            }
                            else if (Convert.ToBoolean(aStmtClient["IsIntlClient"]) == true && Convert.ToBoolean(aStmtClient["IsInterestShared"]) == false && Convert.ToBoolean(aStmtClient["IsInterestSharedAsPrinc"]) == false)
                                {
                                    myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "CombineDeductInternational", p_mymode, myprefix + (p_sStmtType == "Legal" ? "-" + p_sStmtType: ""));
                                }
                                else if (Convert.ToBoolean(aStmtClient["IsIntlClient"]) == false && Convert.ToBoolean(aStmtClient["IsInterestShared"]) == true || Convert.ToBoolean(aStmtClient["IsInterestSharedAsPrinc"]) == true) 
                                {
                                    myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "CombineDeductInt", p_mymode, myprefix + (p_sStmtType == "Legal" ? "-" + p_sStmtType: ""));
                                 }

                                else
                                {
                                    myreportpath = Globals.RenderC1Report(user, aStmtView.Table, "CollectionInvoices.xml", "CombineDeduct", p_mymode, myprefix + (p_sStmtType == "Legal"? "-" + p_sStmtType: ""));
                                }
                            break;
                              
                        }
                    break;

                }


           // }
          
            if (System.IO.File.Exists(myreportpath) && IsEmailRun == true)
                {
                //CreateAttachment("Collection Invoice (" & INVOICE("ClientFinDocId") & ")", myreportpath)
                CreateAttachment("Collection Invoice (" + aStmtClient["ClientFinDocId"] + "]", myreportpath);
                  }
            //return "";

            return myreportpath;
        }
        #endregion
    }
}
