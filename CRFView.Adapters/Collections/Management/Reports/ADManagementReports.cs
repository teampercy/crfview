﻿using CRF.BLL;
using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using CRFView.Adapters.Collections.Management.Reports.ViewModels;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADManagementReports
    {
        #region CRUD

        //Print Letters CSV
        public static string PrintCSVManagementReports(ReportsVM objReportsVM)
        {
            string pdfPath = null;
            string sFromDate = null;
            string sThruDate = null;

            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var Sp_OBJ = new cview_Reports_CollectionManagement_CSV();

                string sql = "Select * from vwCollectionReportsList Where ReportId = " + Convert.ToInt32(objReportsVM.ddlReportId);
                TableView mytableview = DBO.Provider.GetTableView(sql);
                DataSet myds = DBO.Provider.GetDataSet(sql);
                DataTable mydt = myds.Tables[0];

                if (objReportsVM.chkAllClients == false)
                {
                    Sp_OBJ.ClientCode = objReportsVM.ClientNum;
                }
                else
                {
                    Sp_OBJ.ClientCode = "*";
                }

                if (objReportsVM.chkAllDesks == false)
                {
                    Sp_OBJ.DeskNum = objReportsVM.DeskNum;
                }
                else
                {
                    Sp_OBJ.DeskNum = "*";
                }

                if (objReportsVM.chkByAssignDate == true)
                {
                    Sp_OBJ.FromDate = objReportsVM.AssignDateFrom.ToShortDateString();
                    Sp_OBJ.ThruDate = objReportsVM.AssignDateThru.ToShortDateString();
                    sFromDate = objReportsVM.AssignDateFrom.ToShortDateString();
                    sThruDate = objReportsVM.AssignDateThru.ToShortDateString();
                }

                if (objReportsVM.chkByTrxDate == true)
                {
                    Sp_OBJ.FromDate = objReportsVM.TrxDateFrom.ToShortDateString();
                    Sp_OBJ.ThruDate = objReportsVM.TrxnDateThru.ToShortDateString();
                    sFromDate = objReportsVM.TrxDateFrom.ToShortDateString();
                    sThruDate = objReportsVM.TrxnDateThru.ToShortDateString();
                }

                if (objReportsVM.chkAllStatus == false)
                {
                    Sp_OBJ.StatusCode = objReportsVM.StatusCode;
                }
                else
                {
                    Sp_OBJ.StatusCode = "*";
                }

                Sp_OBJ.ReportName = Convert.ToString(mydt.Rows[0]["ReportName"]);
                TableView myview = DBO.Provider.GetTableView(Sp_OBJ);

                DataSet ds = DBO.Provider.GetDataSet(Sp_OBJ);

                DataTable mytbl = ds.Tables[0];

                CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;
                if (objReportsVM.RbtPrintOption == "Send To PDF")
                {
                    mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                }

                string myreportfilename = Convert.ToString(mydt.Rows[0]["ReportFileName"]);
                string myreportname = Convert.ToString(mydt.Rows[0]["ReportName"]);

                if (mytbl.Rows.Count > 0)
                {
                    if (objReportsVM.RbtPrintOption == "Send to CSV")
                    {
                        pdfPath = ProduceCSV(myview, myreportname);
                    }
                    else
                    {
                        pdfPath = Globals.RenderC1Report(user, mytbl, myreportfilename, myreportname, mymode, "", sFromDate, sThruDate);
                    }
                }
            }

            catch (Exception ex)
            {
                return "";
            }
            return pdfPath;
        }

        //Print Letters PDF
        public static string PrintPDFManagementReports(ReportsVM objReportsVM)
        {
            string pdfPath = null;
            string sFromDate = null;
            string sThruDate = null;
          
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

               var Sp_OBJ = new cview_Reports_CollectionManagement_PDF();

                string sql = "Select * from vwCollectionReportsList Where ReportId = " + Convert.ToInt32(objReportsVM.ddlReportId);
                TableView mytableview = DBO.Provider.GetTableView(sql);
                DataSet myds = DBO.Provider.GetDataSet(sql);
                DataTable mydt = myds.Tables[0];

                if (objReportsVM.chkAllClients == false)
                {
                    Sp_OBJ.ClientCode = objReportsVM.ClientNum;
                }
                else
                {
                    Sp_OBJ.ClientCode ="*";
                }

                if (objReportsVM.chkAllDesks == false)
                {
                    Sp_OBJ.DeskNum = objReportsVM.DeskNum;
                }
                else
                {
                    Sp_OBJ.DeskNum = "*";
                }
 
                if (objReportsVM.chkByAssignDate == true)
                {
                    Sp_OBJ.FromDate = objReportsVM.AssignDateFrom.ToShortDateString();
                        Sp_OBJ.ThruDate = objReportsVM.AssignDateThru.ToShortDateString();
                    sFromDate = objReportsVM.AssignDateFrom.ToShortDateString();
                    sThruDate = objReportsVM.AssignDateThru.ToShortDateString();
                }
 
                if (objReportsVM.chkByTrxDate == true)
                {
                    Sp_OBJ.FromDate = objReportsVM.TrxDateFrom.ToShortDateString();
                    Sp_OBJ.ThruDate = objReportsVM.TrxnDateThru.ToShortDateString();
                    sFromDate = objReportsVM.TrxDateFrom.ToShortDateString();
                    sThruDate = objReportsVM.TrxnDateThru.ToShortDateString();
                }

                if (objReportsVM.chkAllStatus == false)
                {
                    Sp_OBJ.StatusCode = objReportsVM.StatusCode;
                }
                else
                {
                    Sp_OBJ.StatusCode = "*";
                }

                Sp_OBJ.ReportName =Convert.ToString(mydt.Rows[0]["ReportName"]);
                TableView myview = DBO.Provider.GetTableView(Sp_OBJ);

                DataSet ds = DBO.Provider.GetDataSet(Sp_OBJ);
 
                DataTable mytbl = ds.Tables[0];

                CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;
                if (objReportsVM.RbtPrintOption == "Send To PDF")
                {
                    mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                }

                string myreportfilename = Convert.ToString(mydt.Rows[0]["ReportFileName"]);
                string myreportname = Convert.ToString(mydt.Rows[0]["ReportName"]);

                if(mytbl.Rows.Count > 0)
                {
                    if (objReportsVM.RbtPrintOption == "Send to CSV")
                    {
                        pdfPath = ProduceCSV(myview, myreportname);
                    }
                    else
                    {
                        pdfPath =   Globals.RenderC1Report(user, mytbl, myreportfilename, myreportname, mymode, "", sFromDate, sThruDate);
                    }
                }

            }

            catch (Exception ex)
            {
                return "";
            }
            return pdfPath;
        }
 
        private static string ProduceCSV(DataView aview, string areportname)
        {
            CRF.BLL.Export.ExportColumns mycols = new CRF.BLL.Export.ExportColumns();
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            string sfilepath = CRF.BLL.CRFView.CRFView.GetFileName(ref user, "Management", areportname, ".CSV");
            string fileName= CRF.BLL.Export.Export.ExportToCSV(ref aview, ref sfilepath, ref mycols, true);
            if(!string.IsNullOrEmpty(fileName))
            {
                return fileName;
            }
           else
            {
                return "";
            }
        }
        #endregion
    }
}
