﻿using CRFView.Adapters.Liens.Managements.DeskAssignment;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Collections.Management.DeskAssignment.ViewModels
{
    public class DeskAssignmentVM
    {
        public DeskAssignmentVM()
        {
            objDebtAccount = new ADDebtAccount();
            objADJob = new ADJob();
            AllClientIdList = CommonBindList.GetClientIdList();
            AllDeskIdList = CommonBindList.GetDeskIdList();
            AllStatusIdList = CommonBindList.GetStatusIdList();
            DeskAssignmentList = new List<ADvwAccountList>();
            LiensDeskAssignmentList = new List<ADVWLienReportsInfo>();
            AllStatusGroupIdList = CommonBindList.GetStatusGroupListId();

            NewDeskIdList = CommonBindList.GetNewDeskIdList();
            NewActionIdList = CommonBindList.GetNewActionIdList();
            LiensNewDeskIdList = CommonBindList.GetLiensNewDeskIdList();
            LiensNewActionIdList = CommonBindList.GetLiensNewActionIdList();
        }

        public string RbtActionChangeOption { get; set; }
        public DateTime AssignedFrom { get; set; }
        public DateTime AssignedThru { get; set; }
        public DateTime NoticeDeadline { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime NDDateFrom { get; set; }
        public DateTime NDDateThru { get; set; }
        public DateTime StartDateThru { get; set; }
        public DateTime StartFromDate { get; set; }
        public string DebtAccountId { get; set; }
        public string JobId { get; set; }
        public string ClientCustomer { get; set; }
        public string GeneralContractor { get; set; }
        public string OwnerName { get; set; }
        public string ClientGroup { get; set; }
        public string Jobname { get; set; }
        public string BalanceRange { get; set; }
        public string JobState { get; set; }
        public string JobAdd1 { get; set; }
        public string FromBalance { get; set; }
        public bool chkFromBalance { get; set; }
        public bool chkNotLateNoticeState { get; set; }
        public string ThruBalance { get; set; }



        public bool chkNoticeDeadline { get; set; }
        public bool chkStartDate { get; set; }
        public bool chkAllClients { get; set; }        
        public string ddlAllClientId { get; set; }
        public IEnumerable<SelectListItem> AllClientIdList { get; set; }

        public bool chkAllDesks { get; set; }
        public string ddlAllDeskId { get; set; }
        public IEnumerable<SelectListItem> AllDeskIdList { get; set; }

        public bool chkAllStatus { get; set; }
        public string ddlAllStatusId { get; set; }
        public IEnumerable<SelectListItem> AllStatusIdList { get; set; }

        public DataTable dtDeskAssignmentList { get; set; }

        public List<ADvwAccountList>  DeskAssignmentList { get; set; }
        public List<ADVWLienReportsInfo> LiensDeskAssignmentList { get; set; }

        public string ddlNewDeskId { get; set; }
        public IEnumerable<SelectListItem> NewDeskIdList { get; set; }

        public string ddlNewActionId { get; set; }
        public IEnumerable<SelectListItem> NewActionIdList { get; set; }

        public string ddlLiensNewDeskId { get; set; }
        public IEnumerable<SelectListItem> LiensNewDeskIdList { get; set; }

        public string ddlLiensNewActionId { get; set; }
        public IEnumerable<SelectListItem> LiensNewActionIdList { get; set; }

        public bool chkAllStatusGroup { get; set; }
        public string ddlAllStatusGroupId { get; set; }
        public IEnumerable<SelectListItem> AllStatusGroupIdList { get; set; }

        public ADDebtAccount objDebtAccount { get; set; }
        public ADJob objADJob { get; set; }

        public ADVWLienReportsInfo onjADVWLienReportsInfo { get; set; }

        public string RbtNewOption { get; set; }
    }
}
