﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Collections.Management.ChangeAccountInfo.ViewModels
{
    public class ChangeAccountInfoVM
    {
        public ChangeAccountInfoVM()
        {
            objDebtAccount = new ADvwAccountList();
            NewCommRateIdList = CommonBindList.GetNewCommRateIdList();
            NewClientIdList = CommonBindList.GetNewClientIdList();
        }
        
        public string Message { get; set; }
        public string DebtAccountId { get; set; }
        public bool chkNoCollectFee { get; set; }
        public bool chkClientTenDayContactPlan { get; set; }
        public bool chkTenDayContactPlan { get; set; }
        public ADvwAccountList objDebtAccount { get; set; }
        public string CollectionRateCode { get; set; }
        public ADCollectionRate objCollectionRate { get; set; }
        
        public bool chkDebtorNameTo { get; set; }
        public string NewDebtorName { get; set; }
        public bool chkAccountNo { get; set; }
        public string NewAccountNo { get; set; }
        public bool chkAssignDate { get; set; }
        public DateTime NewAssignDate { get; set; }
        public bool chkSpecialRate { get; set; }

        public string ddlNewCommRateId { get; set; }
        public IEnumerable<SelectListItem> NewCommRateIdList { get; set; }

        public string ddlNewClientId { get; set; }
        public IEnumerable<SelectListItem> NewClientIdList { get; set; }

        public bool chkLastChgDate { get; set; }
        public DateTime NewLastChgDate { get; set; }
        public bool chkIsNoCollectFee { get; set; }
        public bool chkLastPayDate { get; set; }
        public DateTime NewLastPayDate { get; set; }
        public bool chkClientNumber { get; set; }
        public bool chkAssignAmt { get; set; }
        public string NewAssignAmt { get; set; }
        public bool chkIsTenDayContactPlan { get; set; }
    
        public ADDebtAccountLedger objDebtAccountLedger { get; set; }
        public ADDebtAccount objDebtAccnt { get; set; }
        public ADDebt objDebt { get; set; }

    }
}
