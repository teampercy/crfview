﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADBatchDebtAccountInvoice
    {
        #region Properties
        public int BatchDebtAccountInvoiceId { get; set; }
        public int BatchId { get; set; }
        public int BatchDebtAccountId { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime InvoiceDate { get; set; }
        public decimal BalDue { get; set; }
        public DateTime DateCreated { get; set; }
        #endregion

    }
}
