﻿using CRF.BLL;
using CRF.BLL.Collections.DataEntry.ImportEDI;
using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.CRFDB.VIEWS;
using CRF.BLL.Providers;
using CRFView.Adapters.Collections.DataEntry.ImportEDI.ViewModels;
using CRFView.Adapters.Liens.DataEntry.ImportEDI.ViewModels;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADClientInterface
    {
        #region Properties
        public int Id { get; set; }
        public int ClientId { get; set; }
        public string InterfaceName { get; set; }
        public string InterfaceType { get; set; }
        public string InterfaceDescr { get; set; }
        public int ContractId { get; set; }
        public string EDIInfo { get; set; }
        #endregion

        #region CRUD
        //Read Details from database
        public static ADClientInterface ReadClientInterface(int id)
        {
            ClientInterface myentity = new ClientInterface();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (ClientInterface)myentityItable;
            AutoMapper.Mapper.CreateMap<ClientInterface, ADClientInterface>();
            return (AutoMapper.Mapper.Map<ADClientInterface>(myentity));
        }

        #endregion

        #region Collections Import EDI Methods
        public static string ImportEDI(Collections.DataEntry.ImportEDI.ViewModels.ImportEDIVM objImportEDIVM)
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                ADClientInterface objClientInterface = ReadClientInterface(Convert.ToInt32(objImportEDIVM.ddlClientInterface));

                ADBatch objADBatch = new ADBatch();
                objADBatch.BatchType = "COLL";
                objADBatch.BatchStatus = "OPEN";
                objADBatch.BatchDate = DateTime.Now;
                objADBatch.FileDate = DateTime.Now;
                objADBatch.SubmittedBy = user.UserName;
                objADBatch.SubmittedById = user.Id;

                AutoMapper.Mapper.CreateMap<ADBatch, Batch>();
                ITable tblBatch;
                Batch objBatch;
                objBatch = AutoMapper.Mapper.Map<Batch>(objADBatch);
                tblBatch = (ITable)objBatch;
                DBO.Provider.Create(ref tblBatch);

                string myreportname = objClientInterface.InterfaceName + "-" + objBatch.BatchId;

                string s = objImportEDIVM.FileName;
                StreamReader sr = new StreamReader(objImportEDIVM.FileName);
                int aBatchDebtAccountId = 0;
                int mycount = 0;
                bool myreturn = true;
                string myline;
                string myAccountNo = null;
                decimal myAsgAmt = 0;

                while (sr.Peek() > -1 && myreturn == true)
                {
                    myline = sr.ReadLine();
                    if (mycount > 0)
                    {
                        myreturn = ProcessLine(user, objImportEDIVM.FileName, Convert.ToString(objClientInterface.ClientId), objImportEDIVM.ddlClientInterface, objBatch, myline, objImportEDIVM.ClientBatch, aBatchDebtAccountId, myAccountNo, myAsgAmt);
                    }
                    mycount += 1;
                }

                TableView EntityList;
                string mysql = "Select * from vwBatchDebtAccount where BatchId = " + objBatch.BatchId;
                EntityList = DBO.Provider.GetTableView(mysql);
                string sReportName = null;
                if (EntityList.Count > 0)
                {
                    sReportName = GetAccountsSubmitted(user, EntityList);
                }
                else
                {
                    return "error";
                }
                return sReportName;
                //return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                //return 0;
                return "error";
            }
        }

        public static bool ProcessLine(CRF.BLL.Users.CurrentUser auser, string afilepath, string aclientid, string ainterfaceid, Batch abatch, string aline, string clientbatchref, int aBatchDebtAccountId, string aaccountno, decimal aasgamt)
        {
            ADClientInterface myinterface = ReadClientInterface(Convert.ToInt32(ainterfaceid));

            string sql = "Select * from vwClientCollectionServiceCode where clientid = " + aclientid;
            TableView myview = DBO.Provider.GetTableView(sql);

            if (myview.Count <= 0)
            {
                return true;
            }

            BatchDebtAccount myitem = new BatchDebtAccount();
            myitem.SubmittedByUserId = auser.Id;
            myitem.BatchId = abatch.BatchId;
            myitem.ReferalDate = DateTime.Now;
            myitem.ClientId = Convert.ToInt32(aclientid);
            myitem.ContractId = Convert.ToInt32(myview.get_RowItem("ContractId"));

            BatchDebtAccountInvoice myitemInvoice = new BatchDebtAccountInvoice();
            myitemInvoice.BatchId = abatch.BatchId;
            myitemInvoice.DateCreated = DateTime.Now;
            ITable tblBatchDebtAccount = null;
            ITable tblBatchDebtAccountInvoice = null;

            switch ((myinterface.InterfaceName).ToUpper())
            {
                case "AFP001":
                    myitem.ServiceType = clientbatchref;
                    if (CRF.BLL.Collections.DataEntry.ImportEDI.ImportUtils.ParseAFP001ImportLine(auser, aclientid, abatch, aline, ref myitem) == true)
                    {
                        if (myitem.AccountNo != "-1")
                        {
                            tblBatchDebtAccount = (ITable)myitem;
                            DBO.Provider.Create(ref tblBatchDebtAccount);
                        }
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "DISCUS":
                    myitem.ServiceType = clientbatchref;
                    if (CRF.BLL.Collections.DataEntry.ImportEDI.ImportUtils.ParseDISCUSImportLine(auser, aclientid, abatch, aline, ref myitem, ref myitemInvoice) == true)
                    {
                        if (aaccountno != myitem.AccountNo)
                        {
                            myitem.TotAsgAmt = myitemInvoice.BalDue;
                            tblBatchDebtAccount = (ITable)myitem;
                            DBO.Provider.Create(ref tblBatchDebtAccount);
                            aBatchDebtAccountId = myitem.BatchDebtAccountId;
                        }
                        if (aaccountno == myitem.AccountNo)
                        {
                            ADBatchDebtAccount objBatchDebtAccount = ADBatchDebtAccount.ReadBatchDebtAccounts(aBatchDebtAccountId);
                            myitem.TotAsgAmt = myitem.TotAsgAmt + myitemInvoice.BalDue;
                            tblBatchDebtAccount = (ITable)myitem;
                            DBO.Provider.Update(ref tblBatchDebtAccount);
                        }
                        myitemInvoice.BatchDebtAccountId = aBatchDebtAccountId;
                        tblBatchDebtAccountInvoice = (ITable)myitemInvoice;
                        DBO.Provider.Create(ref tblBatchDebtAccountInvoice);
                        aaccountno = myitem.AccountNo;
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "UPT713":
                    myitem.ServiceType = clientbatchref;
                    if (CRF.BLL.Collections.DataEntry.ImportEDI.ImportUtils.ParseUPT713ImportLine(auser, aclientid, abatch, aline, ref myitem, ref myitemInvoice) == true)
                    {
                        if (aaccountno != myitem.AccountNo)
                        {
                            myitem.TotAsgAmt = CRF.BLL.Collections.DataEntry.ImportEDI.ImportUtils.GetDecimal(myitemInvoice.BalDue);
                            tblBatchDebtAccount = (ITable)myitem;
                            DBO.Provider.Create(ref tblBatchDebtAccount);
                            aBatchDebtAccountId = myitem.BatchDebtAccountId;
                        }
                        if (aaccountno == myitem.AccountNo)
                        {
                            ADBatchDebtAccount objBatchDebtAccount = ADBatchDebtAccount.ReadBatchDebtAccounts(aBatchDebtAccountId);
                            decimal dAsgAmt, dBalDue;
                            dAsgAmt = CRF.BLL.Collections.DataEntry.ImportEDI.ImportUtils.GetDecimal(myitem.TotAsgAmt);
                            dBalDue = CRF.BLL.Collections.DataEntry.ImportEDI.ImportUtils.GetDecimal(myitemInvoice.BalDue);
                            dAsgAmt = dAsgAmt + dBalDue;
                            myitem.TotAsgAmt = dAsgAmt;
                            myitem.LastServiceDate = myitemInvoice.InvoiceDate;
                            tblBatchDebtAccount = (ITable)myitem;
                            DBO.Provider.Update(ref tblBatchDebtAccount);
                        }
                        myitemInvoice.BatchDebtAccountId = aBatchDebtAccountId;
                        tblBatchDebtAccountInvoice = (ITable)myitemInvoice;
                        DBO.Provider.Create(ref tblBatchDebtAccountInvoice);
                        aaccountno = myitem.AccountNo;
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "STP713":
                    myitem.ServiceType = clientbatchref;
                    if (CRF.BLL.Collections.DataEntry.ImportEDI.ImportUtils.ParseSTP713ImportLine(auser, aclientid, abatch, aline, ref myitem) == true)
                    {
                        tblBatchDebtAccount = (ITable)myitem;
                        DBO.Provider.Create(ref tblBatchDebtAccount);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "CSR312":
                    myitem.ServiceType = clientbatchref;
                    if (CRF.BLL.Collections.DataEntry.ImportEDI.ImportUtils.ParseCSR312ImportLine(auser, aclientid, abatch, aline, ref myitem) == true)
                    {
                        tblBatchDebtAccount = (ITable)myitem;
                        DBO.Provider.Create(ref tblBatchDebtAccount);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "DNI001":
                    if (CRF.BLL.Collections.DataEntry.ImportEDI.ImportUtils.ParseDNI001ImportLine(auser, aclientid, abatch, aline, ref myitem) == true)
                    {
                        tblBatchDebtAccount = (ITable)myitem;
                        DBO.Provider.Create(ref tblBatchDebtAccount);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "PLD818":
                    myitem.ServiceType = clientbatchref;
                    if (CRF.BLL.Collections.DataEntry.ImportEDI.ImportUtils.ParsePLD818ImportLine(auser, aclientid, abatch, aline, ref myitem) == true)
                    {
                        tblBatchDebtAccount = (ITable)myitem;
                        DBO.Provider.Create(ref tblBatchDebtAccount);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "SPM972":
                    myitem.ServiceType = clientbatchref;
                    if (CRF.BLL.Collections.DataEntry.ImportEDI.ImportUtils.ParseSPM972ImportLine(auser, aclientid, abatch, aline, ref myitem) == true)
                    {
                        tblBatchDebtAccount = (ITable)myitem;
                        DBO.Provider.Create(ref tblBatchDebtAccount);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "SPK713":
                    myitem.ServiceType = clientbatchref;
                    if (CRF.BLL.Collections.DataEntry.ImportEDI.ImportUtils.ParseSparksImportLine(auser, aclientid, abatch, aline, ref myitem) == true)
                    {
                        tblBatchDebtAccount = (ITable)myitem;
                        DBO.Provider.Create(ref tblBatchDebtAccount);
                        return true;
                    }
                    else
                    {
                        return false;
                    }


                case "SPG713":
                    myitem.ServiceType = clientbatchref;
                    if (CRF.BLL.Collections.DataEntry.ImportEDI.ImportUtils.ParseSparksImportLine(auser, aclientid, abatch, aline, ref myitem) == true)
                    {
                        tblBatchDebtAccount = (ITable)myitem;
                        DBO.Provider.Create(ref tblBatchDebtAccount);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "GEX713":
                    myitem.ServiceType = clientbatchref;
                    if (CRF.BLL.Collections.DataEntry.ImportEDI.ImportUtils.ParseGEX713ImportLine(auser, aclientid, abatch, aline, ref myitem) == true)
                    {
                        tblBatchDebtAccount = (ITable)myitem;
                        DBO.Provider.Create(ref tblBatchDebtAccount);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "GEX714":
                    myitem.ServiceType = clientbatchref;
                    if (CRF.BLL.Collections.DataEntry.ImportEDI.ImportUtils.ParseGEX714ImportLine(auser, aclientid, abatch, aline, ref myitem) == true)
                    {
                        tblBatchDebtAccount = (ITable)myitem;
                        DBO.Provider.Create(ref tblBatchDebtAccount);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "LANG":
                    myitem.ServiceType = clientbatchref;
                    if (CRF.BLL.Collections.DataEntry.ImportEDI.ImportUtils.ParseLANGImportLine(auser, aclientid, abatch, aline, ref myitem) == true)
                    {
                        tblBatchDebtAccount = (ITable)myitem;
                        DBO.Provider.Create(ref tblBatchDebtAccount);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "NIG002":
                    myitem.ServiceType = clientbatchref;
                    if (CRF.BLL.Collections.DataEntry.ImportEDI.ImportUtils.ParseNIG002ImportLine(auser, aclientid, abatch, aline, ref myitem) == true)
                    {
                        tblBatchDebtAccount = (ITable)myitem;
                        DBO.Provider.Create(ref tblBatchDebtAccount);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "ABS858":
                    myitem.ServiceType = clientbatchref;
                    if (CRF.BLL.Collections.DataEntry.ImportEDI.ImportUtils.ParseABS858ImportLine(auser, aclientid, abatch, aline, ref myitem) == true)
                    {
                        tblBatchDebtAccount = (ITable)myitem;
                        DBO.Provider.Create(ref tblBatchDebtAccount);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                default:
                    myitem.ServiceType = clientbatchref;
                    if (CRF.BLL.Collections.DataEntry.ImportEDI.ImportUtils.ParseStandardImportLine(auser, aclientid, abatch, aline, ref myitem) == true)
                    {
                        tblBatchDebtAccount = (ITable)myitem;
                        DBO.Provider.Create(ref tblBatchDebtAccount);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
            }

        }

        public static string GetAccountsSubmitted(CRF.BLL.Users.CurrentUser auser, TableView MyView)
        {
            string myspreadsheetpath = null;
            if (MyView.Count < 1)
            {
                return myspreadsheetpath;
            }
            myspreadsheetpath = CRF.BLL.Common.FileOps.GetFileName(ref auser, "AccounsSubmitted");
            CRF.BLL.Export.ExportColumns mycols = new CRF.BLL.Export.ExportColumns();

            mycols.Add(vwBatchDebtAccount.ColumnNames.ClientCode);
            mycols.Add(vwBatchDebtAccount.ColumnNames.AccountNo);
            mycols.Add(vwBatchDebtAccount.ColumnNames.DebtorName);
            mycols.Add(vwBatchDebtAccount.ColumnNames.ContactName);
            mycols.Add(vwBatchDebtAccount.ColumnNames.AddressLine1);
            mycols.Add(vwBatchDebtAccount.ColumnNames.AddressLine2);
            mycols.Add(vwBatchDebtAccount.ColumnNames.City);
            mycols.Add(vwBatchDebtAccount.ColumnNames.State);
            mycols.Add(vwBatchDebtAccount.ColumnNames.PostalCode);
            mycols.Add(vwBatchDebtAccount.ColumnNames.PhoneNo);
            mycols.Add(vwBatchDebtAccount.ColumnNames.PhoneNoExt);
            mycols.Add(vwBatchDebtAccount.ColumnNames.Cell);
            mycols.Add(vwBatchDebtAccount.ColumnNames.Fax);
            mycols.Add(vwBatchDebtAccount.ColumnNames.Email);
            mycols.Add(vwBatchDebtAccount.ColumnNames.ContactName_2);
            mycols.Add(vwBatchDebtAccount.ColumnNames.AddressLine1_2);
            mycols.Add(vwBatchDebtAccount.ColumnNames.AddressLine2_2);
            mycols.Add(vwBatchDebtAccount.ColumnNames.City_2);
            mycols.Add(vwBatchDebtAccount.ColumnNames.State_2);
            mycols.Add(vwBatchDebtAccount.ColumnNames.PostalCode_2);
            mycols.Add(vwBatchDebtAccount.ColumnNames.PhoneNo_2);
            mycols.Add(vwBatchDebtAccount.ColumnNames.PhoneNoExt_2);
            mycols.Add(vwBatchDebtAccount.ColumnNames.Cell_2);
            mycols.Add(vwBatchDebtAccount.ColumnNames.Fax_2);
            mycols.Add(vwBatchDebtAccount.ColumnNames.Email_2);
            mycols.Add(vwBatchDebtAccount.ColumnNames.ContactName_3);
            mycols.Add(vwBatchDebtAccount.ColumnNames.AddressLine1_3);
            mycols.Add(vwBatchDebtAccount.ColumnNames.AddressLine2_3);
            mycols.Add(vwBatchDebtAccount.ColumnNames.City_3);
            mycols.Add(vwBatchDebtAccount.ColumnNames.State_3);
            mycols.Add(vwBatchDebtAccount.ColumnNames.PostalCode_3);
            mycols.Add(vwBatchDebtAccount.ColumnNames.PhoneNo_3);
            mycols.Add(vwBatchDebtAccount.ColumnNames.PhoneNoExt_3);
            mycols.Add(vwBatchDebtAccount.ColumnNames.Cell_3);
            mycols.Add(vwBatchDebtAccount.ColumnNames.Fax_3);
            mycols.Add(vwBatchDebtAccount.ColumnNames.Email_3);
            mycols.Add(vwBatchDebtAccount.ColumnNames.ReferalDate);
            mycols.Add(vwBatchDebtAccount.ColumnNames.LastServiceDate);
            mycols.Add(vwBatchDebtAccount.ColumnNames.LastPaymentDate);
            mycols.Add(vwBatchDebtAccount.ColumnNames.TotAsgAmt);
            mycols.Add(vwBatchDebtAccount.ColumnNames.ClientAgentCode);
            mycols.Add(vwBatchDebtAccount.ColumnNames.TaxId);
            mycols.Add(vwBatchDebtAccount.ColumnNames.AccountNote);
            mycols.Add(vwBatchDebtAccount.ColumnNames.BranchNum);
            mycols.Add(vwBatchDebtAccount.ColumnNames.Location);
            mycols.Add(vwBatchDebtAccount.ColumnNames.CollectionStatus);
            mycols.Add(vwBatchDebtAccount.ColumnNames.CommRate);
            mycols.Add(vwBatchDebtAccount.ColumnNames.BatchId);
            mycols.Add(vwBatchDebtAccount.ColumnNames.BatchDebtAccountId);
            mycols.Add(vwBatchDebtAccount.ColumnNames.SubmittedByUserId);
            mycols.Add(vwBatchDebtAccount.ColumnNames.IsEstAudit);
            mycols.Add(vwBatchDebtAccount.ColumnNames.IsIntlAcct);
            mycols.Add(vwBatchDebtAccount.ColumnNames.IsJudgmentAcct);
            mycols.Add(vwBatchDebtAccount.ColumnNames.IsRapidRebate);
            mycols.Add(vwBatchDebtAccount.ColumnNames.IsSecondary);
            mycols.Add(vwBatchDebtAccount.ColumnNames.IsSkip);
            mycols.Add(vwBatchDebtAccount.ColumnNames.totcollfeeamt);
            mycols.Add(vwBatchDebtAccount.ColumnNames.AddrNote_2);
            mycols.Add(vwBatchDebtAccount.ColumnNames.AddrNote_3);
            mycols.Add(vwBatchDebtAccount.ColumnNames.ServiceType);
            mycols.Add(vwBatchDebtAccount.ColumnNames.ExchangeRate);
            mycols.Add(vwBatchDebtAccount.ColumnNames.BranchNum);
            mycols.Add(vwBatchDebtAccount.ColumnNames.FixedCollRateCode);
            mycols.Add(vwBatchDebtAccount.ColumnNames.ClientGroupNum);

            DataView MyDataView = (DataView)MyView;

            string fileName = CRF.BLL.Export.Export.ExportToCSV(ref MyDataView, ref myspreadsheetpath, ref mycols, true);

            return myspreadsheetpath;

        }


        #endregion


        #region Lien Import EDI Methods
        public static string LienImportEDI(Liens.DataEntry.ImportEDI.ViewModels.ImportEDIVM objImportEDIVM)
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                ADClientInterface objClientInterface = ReadClientInterface(Convert.ToInt32(objImportEDIVM.ddlClientInterface));

                ADBatch objADBatch = new ADBatch();
                objADBatch.BatchType = "LIEN";
                objADBatch.BatchStatus = "OPEN";
                objADBatch.BatchDate = DateTime.Now;
                objADBatch.FileDate = DateTime.Now;
                objADBatch.SubmittedBy = user.UserName;
                objADBatch.SubmittedById = user.Id;

                AutoMapper.Mapper.CreateMap<ADBatch, Batch>();
                ITable tblBatch;
                Batch objBatch;
                objBatch = AutoMapper.Mapper.Map<Batch>(objADBatch);
                tblBatch = (ITable)objBatch;
                DBO.Provider.Create(ref tblBatch);

                string myreportname = objClientInterface.InterfaceName + "-" + objBatch.BatchId;

                string s = objImportEDIVM.FileName;
                StreamReader sr = new StreamReader(objImportEDIVM.FileName);
                int mycount = 0;
                bool myreturn = true;
                string myline;

                while (sr.Peek() > -1 && myreturn == true)
                {
                    myline = sr.ReadLine();
                    if (myline.Length == 0)
                    {
                        myline = sr.ReadLine();
                    }
                    if (mycount > 0)
                    {
                        myreturn = LienProcessLine(user, objImportEDIVM.FileName, Convert.ToString(objClientInterface.ClientId), objImportEDIVM.ddlClientInterface, objBatch.BatchId, myline);
                    }
                    mycount += 1;
                }

                if ((objClientInterface.InterfaceName).ToUpper() == ("BCL831").ToUpper() || (objClientInterface.InterfaceName).ToUpper() == ("DMG").ToUpper())
                {
                    bool myfunc = true;
                    myfunc = HandleOwnerOLP(objBatch.BatchId);
                }

                if ((objClientInterface.InterfaceName).ToUpper() == ("USC626").ToUpper() || (objClientInterface.InterfaceName).ToUpper() == ("BEACON").ToUpper())
                {
                    bool myfunc = true;
                    myfunc = HandleLenderOLP(objBatch.BatchId);
                }

                if ((objClientInterface.InterfaceName).ToUpper() == ("EliteRoofing").ToUpper())
                {
                    bool myfunc = true;
                    myfunc = HandleEliteOLP(objBatch.BatchId);
                }

                if ((objClientInterface.InterfaceName).ToUpper() == ("Marco").ToUpper())
                {
                    var mysproc3 = new uspbo_LiensDataEntry_GetBatchJobsAmends();
                    mysproc3.BatchId = objBatch.BatchId;
                    TableView myview3 = DBO.Provider.GetTableView(mysproc3);
                    CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                    DataSet myds = DBO.Provider.GetDataSet(mysproc3);
                    DataTable mytbl = myds.Tables[0];
                    string sreport = null;
                    if (myds.Tables[0].Rows.Count > 0)
                    {
                        sreport = Globals.RenderC1Report(user, mytbl, "LienReports.xml", "BatchJobAmends", mymode);
                    }

                    //if (string.IsNullOrEmpty(sreport))
                    //{
                    var mysproc4 = new uspbo_LiensDataEntry_DeleteBatchJobAmends();
                    mysproc4.BatchId = objBatch.BatchId;
                    DBO.Provider.ExecNonQuery(mysproc4);
                    //}
                }

                string sReportName = null;

                var mysproc1 = new uspbo_LiensDataEntry_GetBatchJobsWithError();
                mysproc1.BatchId = objBatch.BatchId;
                TableView myerr = DBO.Provider.GetTableView(mysproc1);
                string MYFILENAME = objImportEDIVM.FileName + "_PROCESSED.CSV";
                DataView MyDataView = (DataView)myerr;
                CRF.BLL.Export.ExportColumns mycols = new CRF.BLL.Export.ExportColumns();
                sReportName = CRF.BLL.Export.Export.ExportToCSV(ref MyDataView, ref MYFILENAME, ref mycols, true);

                if (string.IsNullOrEmpty(MYFILENAME) == false)
                {
                }

                if (myerr.Count > 0)
                {
                    sReportName = PrintBatchList(user, CRF.BLL.COMMON.PrintMode.PrintToPDF, myerr);
                }

                return sReportName;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return "error";
            }
        }

        public static bool LienProcessLine(CRF.BLL.Users.CurrentUser auser, string afilepath, string aclientid, string ainterfaceid, int abatchid, string aline)
        {
            ADClientInterface myinterface = ReadClientInterface(Convert.ToInt32(ainterfaceid));

            BatchJob mybatchjob = new BatchJob();
            mybatchjob.SubmittedOn = DateTime.Now;
            mybatchjob.LastUpdateOn = DateTime.Now;
            ITable tblBatchJob = null;
            switch ((myinterface.InterfaceName).ToUpper())
            {
                case "HAN858":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseHAN858ImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "HAN502":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseHAN502ImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "HAN602":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseHAN602ImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "Hertz":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseHertzImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "URL281":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseURL281ImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "URL425":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseURL425ImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "BCL831":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseBCL831ImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "HZN480":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseHZN480ImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "SBRTX1":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseSBRTX1ImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "SRF714":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseSRF714ImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "Marco":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseMarcoImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "BCR510":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseBCR510ImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "NES":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseNESImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "RSG":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseRSGImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "Probuild":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseProbuildImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "GWB562":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseGWB562ImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "HCR510":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseHCR510ImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "USC626":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseUSC626ImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "HUB909":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseHUB909ImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "FBM":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseFBMImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "FBM253":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseFBM253ImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "SBI916":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseSBI916ImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "Slakey":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseSlakeyImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "BEACON":
                    int myrecordtype = CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseBeaconImportLine(auser, aclientid, abatchid, aline, ref mybatchjob, Convert.ToInt32(ainterfaceid));

                    if (myrecordtype == 1)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else if (myrecordtype == 2)
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "EliteRoofing":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseEliteRoofingImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "IES":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseIESImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);

                        int NewBatchJobId = mybatchjob.Id;
                        CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.HandleIESOLP(ref NewBatchJobId, aline);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "DMG":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseDMGImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "CTL770":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseCTL770ImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "CER925":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseCER925ImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "CED":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseCEDImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "TTE248":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseTTE248ImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "TPR567":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseTPR567ImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "TEST":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseTestImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                default:
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportUtils.ParseStandardImportLine(auser, aclientid, abatchid, aline, ref mybatchjob) == true)
                    {
                        tblBatchJob = (ITable)mybatchjob;
                        DBO.Provider.Create(ref tblBatchJob);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
            }

        }

        public static bool HandleOwnerOLP(int abatchid)
        {
            try
            {
                string sql = "select * from BatchJob where BatchId = " + abatchid;
                TableView myview = DBO.Provider.GetTableView(sql);
                DataTable dt = myview.ToTable();

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        if (Convert.ToString(row["DesigneeName"]).Length > 0)
                        {
                            ADBatchJobLegalParties objADBatchJobLegalParties = new ADBatchJobLegalParties();
                            AutoMapper.Mapper.CreateMap<ADBatchJobLegalParties, BatchJobLegalParties>();
                            ITable tblBatchJobLegalParties;
                            BatchJobLegalParties objBatchJobLegalParties;

                            objADBatchJobLegalParties.BatchJobId = Convert.ToInt32(row["Id"]);
                            objADBatchJobLegalParties.TypeCode = "OW";
                            objADBatchJobLegalParties.AddressName = Convert.ToString(row["DesigneeName"]);
                            objADBatchJobLegalParties.AddressLine1 = Convert.ToString(row["DesigneeAdd1"]);
                            objADBatchJobLegalParties.AddressLine2 = Convert.ToString(row["DesigneeAdd2"]);
                            objADBatchJobLegalParties.City = Convert.ToString(row["DesigneeCity"]);
                            objADBatchJobLegalParties.State = Convert.ToString(row["DesigneeState"]);
                            objADBatchJobLegalParties.PostalCode = Convert.ToString(row["DesigneeZip"]);
                            objADBatchJobLegalParties.Telephone1 = Convert.ToString(row["DesigneePhone1"]);
                            objADBatchJobLegalParties.IsPrimary = false;

                            objBatchJobLegalParties = AutoMapper.Mapper.Map<BatchJobLegalParties>(objADBatchJobLegalParties);
                            tblBatchJobLegalParties = (ITable)objBatchJobLegalParties;
                            DBO.Provider.Create(ref tblBatchJobLegalParties);

                            ADBatchJob objADADBatchJobOriginal = ADBatchJob.ReadBatchJob(Convert.ToInt32(row["Id"]));
                            AutoMapper.Mapper.CreateMap<ADBatchJob, BatchJob>();
                            ITable tblBatchJob;
                            BatchJob objBatchJob;

                            objADADBatchJobOriginal.DesigneeName = null;
                            objADADBatchJobOriginal.DesigneeAdd1 = null;
                            objADADBatchJobOriginal.DesigneeAdd2 = null;
                            objADADBatchJobOriginal.DesigneeCity = null;
                            objADADBatchJobOriginal.DesigneeState = null;
                            objADADBatchJobOriginal.DesigneeZip = null;
                            objADADBatchJobOriginal.DesigneePhone1 = null;
                            objADBatchJobLegalParties.AddressName = Convert.ToString(row["DesigneeName"]);

                            objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objADADBatchJobOriginal);
                            tblBatchJob = (ITable)objBatchJob;
                            DBO.Provider.Update(ref tblBatchJob);
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }

        public static bool HandleLenderOLP(int abatchid)
        {
            try
            {
                string sql = "select * from BatchJob where BatchId = " + abatchid;
                TableView myview = DBO.Provider.GetTableView(sql);
                DataTable dt = myview.ToTable();

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        if (Convert.ToString(row["DesigneeName"]).Length > 0)
                        {
                            ADBatchJobLegalParties objADBatchJobLegalParties = new ADBatchJobLegalParties();
                            AutoMapper.Mapper.CreateMap<ADBatchJobLegalParties, BatchJobLegalParties>();
                            ITable tblBatchJobLegalParties;
                            BatchJobLegalParties objBatchJobLegalParties;

                            objADBatchJobLegalParties.BatchJobId = Convert.ToInt32(row["Id"]);
                            objADBatchJobLegalParties.TypeCode = "LE";
                            objADBatchJobLegalParties.AddressName = Convert.ToString(row["DesigneeName"]);
                            objADBatchJobLegalParties.AddressLine1 = Convert.ToString(row["DesigneeAdd1"]);
                            objADBatchJobLegalParties.AddressLine2 = Convert.ToString(row["DesigneeAdd2"]);
                            objADBatchJobLegalParties.City = Convert.ToString(row["DesigneeCity"]);
                            objADBatchJobLegalParties.State = Convert.ToString(row["DesigneeState"]);
                            objADBatchJobLegalParties.PostalCode = Convert.ToString(row["DesigneeZip"]);
                            objADBatchJobLegalParties.Telephone1 = Convert.ToString(row["DesigneePhone1"]);
                            objADBatchJobLegalParties.IsPrimary = false;

                            objBatchJobLegalParties = AutoMapper.Mapper.Map<BatchJobLegalParties>(objADBatchJobLegalParties);
                            tblBatchJobLegalParties = (ITable)objBatchJobLegalParties;
                            DBO.Provider.Create(ref tblBatchJobLegalParties);

                            ADBatchJob objADADBatchJobOriginal = ADBatchJob.ReadBatchJob(Convert.ToInt32(row["Id"]));
                            AutoMapper.Mapper.CreateMap<ADBatchJob, BatchJob>();
                            ITable tblBatchJob;
                            BatchJob objBatchJob;

                            objADADBatchJobOriginal.DesigneeName = null;
                            objADADBatchJobOriginal.DesigneeAdd1 = null;
                            objADADBatchJobOriginal.DesigneeAdd2 = null;
                            objADADBatchJobOriginal.DesigneeCity = null;
                            objADADBatchJobOriginal.DesigneeState = null;
                            objADADBatchJobOriginal.DesigneeZip = null;
                            objADADBatchJobOriginal.DesigneePhone1 = null;
                            objADBatchJobLegalParties.AddressName = Convert.ToString(row["DesigneeName"]);

                            objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objADADBatchJobOriginal);
                            tblBatchJob = (ITable)objBatchJob;
                            DBO.Provider.Update(ref tblBatchJob);
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }


        public static bool HandleEliteOLP(int abatchid)
        {
            try
            {
                string sql = "select * from BatchJob where BatchId = " + abatchid;
                TableView myview = DBO.Provider.GetTableView(sql);
                DataTable dt = myview.ToTable();

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow row in dt.Rows)
                    {
                        if (Convert.ToString(row["FilterKey"]).Length > 0)
                        {
                            ADBatchJobLegalParties objADBatchJobLegalParties = new ADBatchJobLegalParties();
                            AutoMapper.Mapper.CreateMap<ADBatchJobLegalParties, BatchJobLegalParties>();
                            ITable tblBatchJobLegalParties;
                            BatchJobLegalParties objBatchJobLegalParties;

                            objADBatchJobLegalParties.BatchJobId = Convert.ToInt32(row["Id"]);
                            if (Convert.ToString(row["FilterKey"]) == "S")
                            {
                                objADBatchJobLegalParties.TypeCode = "LE";
                            }
                            else
                            {
                                objADBatchJobLegalParties.TypeCode = Convert.ToString(row["FilterKey"]);
                            }

                            objADBatchJobLegalParties.AddressName = Convert.ToString(row["DesigneeName"]);
                            objADBatchJobLegalParties.AddressLine1 = Convert.ToString(row["DesigneeAdd1"]);
                            objADBatchJobLegalParties.AddressLine2 = Convert.ToString(row["DesigneeAdd2"]);
                            objADBatchJobLegalParties.City = Convert.ToString(row["DesigneeCity"]);
                            objADBatchJobLegalParties.State = Convert.ToString(row["DesigneeState"]);
                            objADBatchJobLegalParties.PostalCode = Convert.ToString(row["DesigneeZip"]);
                            objADBatchJobLegalParties.Telephone1 = Convert.ToString(row["DesigneePhone1"]);
                            objADBatchJobLegalParties.IsPrimary = false;

                            objBatchJobLegalParties = AutoMapper.Mapper.Map<BatchJobLegalParties>(objADBatchJobLegalParties);
                            tblBatchJobLegalParties = (ITable)objBatchJobLegalParties;
                            DBO.Provider.Create(ref tblBatchJobLegalParties);

                            ADBatchJob objADADBatchJobOriginal = ADBatchJob.ReadBatchJob(Convert.ToInt32(row["Id"]));
                            AutoMapper.Mapper.CreateMap<ADBatchJob, BatchJob>();
                            ITable tblBatchJob;
                            BatchJob objBatchJob;

                            objADADBatchJobOriginal.FilterKey = null;
                            objADADBatchJobOriginal.DesigneeName = null;
                            objADADBatchJobOriginal.DesigneeAdd1 = null;
                            objADADBatchJobOriginal.DesigneeAdd2 = null;
                            objADADBatchJobOriginal.DesigneeCity = null;
                            objADADBatchJobOriginal.DesigneeState = null;
                            objADADBatchJobOriginal.DesigneeZip = null;
                            objADBatchJobLegalParties.AddressName = Convert.ToString(row["DesigneeName"]);

                            objBatchJob = AutoMapper.Mapper.Map<BatchJob>(objADADBatchJobOriginal);
                            tblBatchJob = (ITable)objBatchJob;
                            DBO.Provider.Update(ref tblBatchJob);
                        }
                    }
                }

                return true;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return false;
            }
        }

        public static string PrintBatchList(CRF.BLL.Users.CurrentUser auser, CRF.BLL.COMMON.PrintMode aprintmode, TableView aview)
        {
            CRF.BLL.COMMON.StandardReport myreport = new CRF.BLL.COMMON.StandardReport();
            myreport.ReportDefPath = auser.SettingsFolder + "\\Reports\\LienReports.xml";
            myreport.OutputFolder = auser.OutputFolder + "\\";
            myreport.UserName = auser.UserName;
            myreport.ReportName = "BatchJobList";
            myreport.TableIndex = 1;
            myreport.DataView = aview;
            myreport.PrintMode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
            myreport.Sort = "";
            myreport.RenderReport();

            return myreport.ReportFileName;
        }

        #endregion

        #region Lien Import JobView Methods
        public static string LienImportJobView(Liens.DataEntry.ImportJobView.ViewModels.ImportJobViewVM objImportJobViewVM)
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                ADClientInterface objClientInterface = ReadClientInterface(Convert.ToInt32(objImportJobViewVM.ddlClientInterface));

                ADBatch objADBatch = new ADBatch();
                objADBatch.BatchType = "JOBVIEW";
                objADBatch.BatchStatus = "OPEN";
                objADBatch.BatchDate = DateTime.Now;
                objADBatch.FileDate = DateTime.Now;
                objADBatch.SubmittedBy = user.UserName;
                objADBatch.SubmittedById = user.Id;

                AutoMapper.Mapper.CreateMap<ADBatch, Batch>();
                ITable tblBatch;
                Batch objBatch;
                objBatch = AutoMapper.Mapper.Map<Batch>(objADBatch);
                tblBatch = (ITable)objBatch;
                DBO.Provider.Create(ref tblBatch);

                string myreportname = objClientInterface.InterfaceName + "-" + objBatch.BatchId;

                string s = objImportJobViewVM.FileName;

                bool myresult = false;
                string sFilename = null;

                // Customer File (RMEFCMSHL.TXT)
                sFilename = objImportJobViewVM.FileName + "\\" + "RMEFCMSHL.DAT";
                if (File.Exists(sFilename))
                {
                    myresult = ProcessFile("Customer", sFilename, objImportJobViewVM.FileName, objBatch.BatchId, Convert.ToString(objClientInterface.ClientId), objImportJobViewVM.ddlClientInterface);
                }
                else
                {
                    myresult = false;
                }

                //Customer Analytic File (RMEFCANHL.TXT)
                sFilename = objImportJobViewVM.FileName + "\\" + "RMEFCANHL.DAT";
                if (File.Exists(sFilename))
                {
                    myresult = ProcessFile("CustomerAnalytic", sFilename, objImportJobViewVM.FileName, objBatch.BatchId, Convert.ToString(objClientInterface.ClientId), objImportJobViewVM.ddlClientInterface);
                }
                else
                {
                    myresult = false;
                }

                //Jobsite File (RMEFNJBHL.TXT)
                sFilename = objImportJobViewVM.FileName + "\\" + "RMEFNJBHL.DAT";
                if (File.Exists(sFilename))
                {
                    myresult = ProcessFile("Jobsite", sFilename, objImportJobViewVM.FileName, objBatch.BatchId, Convert.ToString(objClientInterface.ClientId), objImportJobViewVM.ddlClientInterface);
                }
                else
                {
                    myresult = false;
                }

                //**Resubmitted New Rental File (RMEFNRAHLR.TXT)
                sFilename = objImportJobViewVM.FileName + "\\" + "RMEFNRAHLR.DAT";
                if (File.Exists(sFilename))
                {
                    myresult = ProcessFile("NewRental", sFilename, objImportJobViewVM.FileName, objBatch.BatchId, Convert.ToString(objClientInterface.ClientId), objImportJobViewVM.ddlClientInterface);
                }
                else
                {
                    myresult = false;
                }

                //New Rental File (RMEFNRAHL.TXT)
                sFilename = objImportJobViewVM.FileName + "\\" + "RMEFNRAHL.DAT";
                if (File.Exists(sFilename))
                {
                    myresult = ProcessFile("NewRental", sFilename, objImportJobViewVM.FileName, objBatch.BatchId, Convert.ToString(objClientInterface.ClientId), objImportJobViewVM.ddlClientInterface);
                }
                else
                {
                    myresult = false;
                }

                //**Resubmitted Update Rental File (RMEFURAHLR.TXT)
                sFilename = objImportJobViewVM.FileName + "\\" + "RMEFURAHLR.DAT";
                if (File.Exists(sFilename))
                {
                    myresult = ProcessFile("UpdateRental", sFilename, objImportJobViewVM.FileName, objBatch.BatchId, Convert.ToString(objClientInterface.ClientId), objImportJobViewVM.ddlClientInterface);
                }
                else
                {
                    myresult = false;
                }

                //Update Rental File (RMEFURAHL.TXT)
                sFilename = objImportJobViewVM.FileName + "\\" + "RMEFURAHL.DAT";
                if (File.Exists(sFilename))
                {
                    myresult = ProcessFile("UpdateRental", sFilename, objImportJobViewVM.FileName, objBatch.BatchId, Convert.ToString(objClientInterface.ClientId), objImportJobViewVM.ddlClientInterface);
                }
                else
                {
                    myresult = false;
                }

                //**Resubmitted New Invoice File (RMEFNINHLR.TXT)
                sFilename = objImportJobViewVM.FileName + "\\" + "RMEFNINHLR.DAT";
                if (File.Exists(sFilename))
                {
                    myresult = ProcessFile("NewInvoice", sFilename, objImportJobViewVM.FileName, objBatch.BatchId, Convert.ToString(objClientInterface.ClientId), objImportJobViewVM.ddlClientInterface);
                }
                else
                {
                    myresult = false;
                }

                //New Invoice File (RMEFNINHL.TXT)
                sFilename = objImportJobViewVM.FileName + "\\" + "RMEFNINHL.DAT";
                if (File.Exists(sFilename))
                {
                    myresult = ProcessFile("NewInvoice", sFilename, objImportJobViewVM.FileName, objBatch.BatchId, Convert.ToString(objClientInterface.ClientId), objImportJobViewVM.ddlClientInterface);
                }
                else
                {
                    myresult = false;
                }

                //**Resubmitted Update Invoice File (RMEFUINHLR.TXT)
                sFilename = objImportJobViewVM.FileName + "\\" + "RMEFUINHLR.DAT";
                if (File.Exists(sFilename))
                {
                    myresult = ProcessFile("UpdateInvoice", sFilename, objImportJobViewVM.FileName, objBatch.BatchId, Convert.ToString(objClientInterface.ClientId), objImportJobViewVM.ddlClientInterface);
                }
                else
                {
                    myresult = false;
                }

                //Update Invoice File (RMEFUINHL.TXT)
                sFilename = objImportJobViewVM.FileName + "\\" + "RMEFUINHL.DAT";
                if (File.Exists(sFilename))
                {
                    myresult = ProcessFile("UpdateInvoice", sFilename, objImportJobViewVM.FileName, objBatch.BatchId, Convert.ToString(objClientInterface.ClientId), objImportJobViewVM.ddlClientInterface);
                }
                else
                {
                    myresult = false;
                }

                objBatch.BatchStatus = "POST";
                tblBatch = (ITable)objBatch;
                DBO.Provider.Create(ref tblBatch);

                string sReportName = null;
                sReportName = CreateExceptionList(objBatch.BatchId);

                return sReportName;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return "error";
            }
        }
        public static bool ProcessFile(string afiletype, string afilename, string afilepath, int abatchid, string ClientId, string ainterfaceid)
        {
            CRF.BLL.Users.CurrentUser auser = AuthenticationUtility.GetUser();

            StreamReader sr = new StreamReader(afilename);
            int mycount = 0;
            bool myreturn = true;
            string myline;
            myline = sr.ReadLine(); //Read header

            while (sr.Peek() > -1 && myreturn == true)
            {
                myline = sr.ReadLine();
                if (myline.Length == 0)
                {
                    myline = sr.ReadLine();
                }
                if (mycount > 0)
                {
                    myreturn = LienImportJobViewProcessLine(afiletype, auser, afilepath, ClientId, ainterfaceid, abatchid, myline);
                }
                mycount += 1;
            }
            return myreturn;
        }


        public static bool LienImportJobViewProcessLine(string afiletype, CRF.BLL.Users.CurrentUser auser, string afilepath, string aclientid, string ainterfaceid, int abatchid, string aline)
        {
            ADClientInterface myinterface = ReadClientInterface(Convert.ToInt32(ainterfaceid));

            BatchCustomer myCustomer = new BatchCustomer();
            BatchCustomerAnalytic myCustomerAnalytic = new BatchCustomerAnalytic();
            BatchJobSite mybatchjobsite = new BatchJobSite();
            BatchJobRental mybatchjobRental = new BatchJobRental();
            BatchJobInvoice mybatchjobInvoice = new BatchJobInvoice();

            ITable tblBatchCustomer = null;
            ITable tblBatchCustomerAnalytic = null;
            ITable tblBatchJobSite = null;
            ITable tblBatchJobRental = null;
            ITable tblBatchJobInvoice = null;

            myCustomer.DateCreated = DateTime.Now;
            myCustomerAnalytic.DateCreated = DateTime.Now;
            mybatchjobsite.DateCreated = DateTime.Now;
            mybatchjobRental.DateCreated = DateTime.Now;
            mybatchjobInvoice.DateCreated = DateTime.Now;

            myCustomer.BatchId = abatchid;
            myCustomerAnalytic.BatchId = abatchid;
            mybatchjobsite.BatchId = abatchid;
            mybatchjobRental.BatchId = abatchid;
            mybatchjobInvoice.BatchId = abatchid;

            myCustomerAnalytic.ClientId = Convert.ToInt32(aclientid);
            myCustomer.ClientId = Convert.ToInt32(aclientid);
            mybatchjobsite.ClientId = Convert.ToInt32(aclientid);
            mybatchjobRental.ClientId = Convert.ToInt32(aclientid);
            mybatchjobInvoice.ClientId = Convert.ToInt32(aclientid);


            switch ((afiletype).ToUpper())
            {
                case "CUSTOMER":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportFiles.ParseCustomer(auser, aclientid, abatchid, aline, ref myCustomer) == true)
                    {
                        tblBatchCustomer = (ITable)myCustomer;
                        DBO.Provider.Create(ref tblBatchCustomer);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "CUSTOMERANALYTIC":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportFiles.ParseCustomerAnalytic(auser, aclientid, abatchid, aline, ref myCustomerAnalytic) == true)
                    {
                        tblBatchCustomerAnalytic = (ITable)myCustomerAnalytic;
                        DBO.Provider.Create(ref tblBatchCustomerAnalytic);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "JOBSITE":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportFiles.ParseJobSite(auser, aclientid, abatchid, aline, ref mybatchjobsite) == true)
                    {
                        tblBatchJobSite = (ITable)mybatchjobsite;
                        DBO.Provider.Create(ref tblBatchJobSite);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "NEWRENTAL":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportFiles.ParseRental(auser, aclientid, abatchid, aline, ref mybatchjobRental) == true)
                    {
                        tblBatchJobRental = (ITable)mybatchjobRental;
                        DBO.Provider.Create(ref tblBatchJobRental);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "UPDATERENTAL":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportFiles.ParseRental(auser, aclientid, abatchid, aline, ref mybatchjobRental) == true)
                    {
                        tblBatchJobRental = (ITable)mybatchjobRental;
                        DBO.Provider.Create(ref tblBatchJobRental);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "NEWINVOICE":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportFiles.ParseInvoice(auser, aclientid, abatchid, aline, ref mybatchjobInvoice) == true)
                    {
                        tblBatchJobInvoice = (ITable)mybatchjobInvoice;
                        DBO.Provider.Create(ref tblBatchJobInvoice);
                        return true;
                    }
                    else
                    {
                        return false;
                    }

                case "UPDATEINVOICE":
                    if (CRF.BLL.Liens.DataEntry.ImportEDI.ImportFiles.ParseInvoice(auser, aclientid, abatchid, aline, ref mybatchjobInvoice) == true)
                    {
                        tblBatchJobInvoice = (ITable)mybatchjobInvoice;
                        DBO.Provider.Create(ref tblBatchJobInvoice);
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                default:
                    return false;
            }

        }

        public static string CreateExceptionList(int aBatchId)
        {
            string sReportName = null;
            CRF.BLL.Users.CurrentUser auser = AuthenticationUtility.GetUser();

            var mysproc1 = new uspbo_JobView_GetBatchJobsWithError();
            mysproc1.BatchId = aBatchId;
            TableView myerr = DBO.Provider.GetTableView(mysproc1);

            if (myerr.Count > 0)
            {
                sReportName = PrintJobviewExceptions(auser, CRF.BLL.COMMON.PrintMode.PrintToPDF, myerr);
                return sReportName;
            }
            else
            {
                return "error";
            }
        }

        public static string PrintJobviewExceptions(CRF.BLL.Users.CurrentUser auser, CRF.BLL.COMMON.PrintMode aprintmode, TableView aview)
        {
            CRF.BLL.COMMON.StandardReport myreport = new CRF.BLL.COMMON.StandardReport();
            myreport.ReportDefPath = auser.SettingsFolder + "\\Reports\\LienReports.xml";
            myreport.OutputFolder = auser.OutputFolder + "\\";
            myreport.UserName = auser.UserName;
            myreport.ReportName = "BatchJobViewExceptions";
            myreport.TableIndex = 1;
            myreport.DataView = aview;
            myreport.PrintMode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
            myreport.Sort = "";
            myreport.RenderReport();

            return myreport.ReportFileName;
        }


       //Collections EndDate Update Methods
        public static int EndDateUpdateEDI(Collections.DataEntry.ImportEDI.ViewModels.ImportEDIVM objImportEDIVM)
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                ADClientInterface objClientInterface = ReadClientInterface(Convert.ToInt32(objImportEDIVM.ddlClientInterface));
                string s = objImportEDIVM.FileName;
                StreamReader sr = new StreamReader(objImportEDIVM.FileName);
            
                int mycount = 0;
                int myreturn = 1;
                string myline;
              
                while (sr.Peek() > -1 && myreturn == 1)
                {
                    myline = sr.ReadLine();
                    if (mycount > 0)
                    {
                        
                        myreturn = EndDateProcessFile(user, objImportEDIVM.FileName,myline);
                    }
                    mycount += 1;
                }

        
                return 1;
                //return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                //return 0;
                return -1;
            }
        }

        public static int EndDateProcessFile(CRF.BLL.Users.CurrentUser auser, string afilepath,string aline)
        {
            //CRF.BLL.Users.CurrentUser auser = AuthenticationUtility.GetUser();
            try
            {
                ADJob objADJob = new ADJob();


                AutoMapper.Mapper.CreateMap<ADJob, Job>();
                ITable tblJob;
                Job objJob;
                objJob = AutoMapper.Mapper.Map<Job>(objADJob);
                tblJob = (ITable)objJob;
                DBO.Provider.Create(ref tblJob);

                ADClient objADClient = new ADClient();
                AutoMapper.Mapper.CreateMap<ADClient, Client>();
                ITable tblClient;
                Client objClient;
                objClient = AutoMapper.Mapper.Map<Client>(objADClient);
                tblClient = (ITable)objClient;
                DBO.Provider.Create(ref tblClient);

                string[] sfields = CRF.BLL.Collections.DataEntry.ImportEDI.ImportUtils.CSVParser(aline, ",");
                if (sfields.Length < 2)
                {
                    return 0;
                }
                if (sfields[0] == "")
                {
                    return 0;
                }
                HDS.DAL.Provider.DAL.Read(sfields[0], ref tblJob);
                objJob.EndDate = Convert.ToDateTime(sfields[1]);
                DBO.Provider.Update(ref tblJob);

                HDS.DAL.Provider.DAL.Read(objADJob.ClientId.ToString(), ref tblClient);



                if (objADJob.IsJobView == true && objClient.NoRentalInfo == false)
                {
                    var sp_obj = new uspbo_Jobview_UpdateDeadlineDates();
                    sp_obj.JobId = sfields[0];
                    DBO.Provider.ExecNonQuery(sp_obj);
                }
                else
                {
                    var SP_OBJ = new uspbo_LiensDayEnd_UpdateDeadlineDates();
                    SP_OBJ.ExpireDays = 0;
                    SP_OBJ.JobId = sfields[0];
                    DBO.Provider.ExecNonQuery(SP_OBJ);
                }


                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                //return 0;
                return -1;
            }
        }


     
      
        #endregion
    }
}
