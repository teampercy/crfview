﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.DataEntry.CreateWebBatch.ViewModels
{
    public class BatchWebPlacementsVM
    {
        public BatchWebPlacementsVM()
        {

        }

        public ADBatchDebtAccount ObjBatchDebtAccount { get; set; }

    }
}
