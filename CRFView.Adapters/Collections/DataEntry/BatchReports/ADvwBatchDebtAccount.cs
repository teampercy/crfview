﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.Providers;
using CRFView.Adapters.Collections.DataEntry.BatchReports.ViewModels;
using HDS.DAL.COMMON;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADvwBatchDebtAccount
    {


        //Print Collection Batch Schedule
        public string PrintBatchReport(BatchReportOptionsVM objBatchReportOptionsVM)
        {
            string pdfPath = null;
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                 
                var myds = new DataSet();
                var Sp_OBJ = new cview_Reports_GetAccountsInBatches();

                if (objBatchReportOptionsVM.chkAllClients == false)
                {
                    Sp_OBJ.ClientId = objBatchReportOptionsVM.ddlClientId;
                }

                if (objBatchReportOptionsVM.chkAllUsers == false)
                {
                    Sp_OBJ.SubmittedByUserId = objBatchReportOptionsVM.ddlUserId;
                }

                if (objBatchReportOptionsVM.chkByBatchDate == true)
                {
                    Sp_OBJ.BatchDateFROM = objBatchReportOptionsVM.FromBatchDate.ToShortDateString();
                    Sp_OBJ.BatchDateTO = objBatchReportOptionsVM.ThruBatchDate.ToShortDateString();
                    Sp_OBJ.SelectByBatchDate = true;
                }

                if (objBatchReportOptionsVM.chkByBatchNo == true)
                {
                    Sp_OBJ.BatchFROM = objBatchReportOptionsVM.FromBatchNo;
                    Sp_OBJ.BatchTO = objBatchReportOptionsVM.ThruBatchNo;
                    Sp_OBJ.SelectByBatch = true;
                }

                Sp_OBJ.BatchStatus = 0;                
                if(objBatchReportOptionsVM.RbtBatchStatus == "All Open Batches")
                {
                    Sp_OBJ.BatchStatus = 1;
                }
                if (objBatchReportOptionsVM.RbtBatchStatus == "Web Open")
                {
                    Sp_OBJ.BatchStatus = 2;
                }
                if (objBatchReportOptionsVM.RbtBatchStatus == "Internal Open")
                {
                    Sp_OBJ.BatchStatus = 3;
                }
                if (objBatchReportOptionsVM.RbtBatchStatus == "Released")
                {
                    Sp_OBJ.BatchStatus = 4;
                }
                if (objBatchReportOptionsVM.RbtBatchStatus == "Posted")
                {
                    Sp_OBJ.BatchStatus = 5;
                }

                CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;
                if(objBatchReportOptionsVM.RbtPrintOption == "Send To PDF")
                {
                    mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                }
              
                //TableView myview = DBO.Provider.GetTableView(Sp_OBJ);
                TableView myview = CRF.BLL.Providers.CollectionsDataEntry.GetNewAccountList(user, Sp_OBJ);



                pdfPath = CRF.BLL.Providers.CollectionsDataEntry.PrintBatchList(user, mymode, myview);
            }

            catch (Exception ex)
            {
                return "";
            }
            return pdfPath;
        }

        //Print Lien Batch Schedule
        public string PrintLienBatchReport(CRFView.Adapters.Liens.DataEntry.BatchReport.ViewModels.BatchReportOptionsVM objBatchReportOptionsVM)
        {
            string pdfPath = null;
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var myds = new DataSet();
                var Sp_OBJ = new uspbo_LiensDataEntry_GetJobsInBatches();

                if (objBatchReportOptionsVM.chkAllClients == false)
                {
                    Sp_OBJ.ClientId = objBatchReportOptionsVM.ddlClientId;
                }

                if (objBatchReportOptionsVM.chkAllUsers == false)
                {
                    Sp_OBJ.SubmittedByUserId = objBatchReportOptionsVM.ddlUserId;
                }

                if (objBatchReportOptionsVM.chkByBatchDate == true)
                {
                    Sp_OBJ.BatchDateFROM = objBatchReportOptionsVM.FromBatchDate.ToShortDateString();
                    Sp_OBJ.BatchDateTO = objBatchReportOptionsVM.ThruBatchDate.ToShortDateString();
                    Sp_OBJ.SelectByBatchDate = true;
                }

                if (objBatchReportOptionsVM.chkByBatchNo == true)
                {
                    Sp_OBJ.BatchFROM = objBatchReportOptionsVM.FromBatchNo;
                    Sp_OBJ.BatchTO = objBatchReportOptionsVM.ThruBatchNo;
                    Sp_OBJ.SelectByBatch = true;
                }

                Sp_OBJ.BatchStatus = 0;
                if (objBatchReportOptionsVM.RbtBatchStatus == "All Open Batches")
                {
                    Sp_OBJ.BatchStatus = 1;
                }
                if (objBatchReportOptionsVM.RbtBatchStatus == "Web Open")
                {
                    Sp_OBJ.BatchStatus = 2;
                }
                if (objBatchReportOptionsVM.RbtBatchStatus == "Internal Open")
                {
                    Sp_OBJ.BatchStatus = 3;
                }
                if (objBatchReportOptionsVM.RbtBatchStatus == "Released")
                {
                    Sp_OBJ.BatchStatus = 4;
                }
                if (objBatchReportOptionsVM.RbtBatchStatus == "Posted")
                {
                    Sp_OBJ.BatchStatus = 5;
                }

                CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;
                if (objBatchReportOptionsVM.RbtPrintOption == "Send To PDF")
                {
                    mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                }
          
                Sp_OBJ.ErrorsOnly = false;
                if(objBatchReportOptionsVM.chkErrors == true)
                {
                    Sp_OBJ.ErrorsOnly = true;
                }


               // TableView myview = DBO.Provider.GetTableView(Sp_OBJ);
                TableView myview = CRF.BLL.Providers.LiensDataEntry.GetJobsInBatches(user, Sp_OBJ);

                if(objBatchReportOptionsVM.chkBatchList == true)
                {
                    pdfPath = CRF.BLL.Providers.LiensDataEntry.PrintBatchList(user, mymode, myview);
                }

            }

            catch (Exception ex)
            {
                return "";
            }
            return pdfPath;
        }
    }
}
