﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Collections.DataEntry.BatchReports.ViewModels
{
    public class BatchReportOptionsVM
    {
        #region Properties
        public bool chkAllClients { get; set; }
        public bool chkAllUsers { get; set; }
        public bool chkByBatchDate { get; set; }
        public bool chkByBatchNo { get; set; }

        public DateTime FromBatchDate { get; set; }
        public DateTime ThruBatchDate { get; set; }

        public string FromBatchNo { get; set; }
        public string ThruBatchNo { get; set; }

        public string RbtBatchStatus { get; set; }
        public string RbtPrintOption { get; set; }

        public string ddlClientId { get; set; }
        public IEnumerable<SelectListItem> ClientIdList { get; set; }

        public string ddlUserId { get; set; }
        public IEnumerable<SelectListItem> UserIdList { get; set; }
        #endregion


        public BatchReportOptionsVM()
        {
            ClientIdList = CommonBindList.GetClientIdList();
            UserIdList = CommonBindList.GetUserIdList();
        }
    }
}
