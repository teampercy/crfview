﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Collections.DataEntry.DataEntry.ViewModels
{
    public class BatchDebtAccountVM
    {
        public BatchDebtAccountVM()
        {
            // BatchIdList = CommonBindList.GetBatchIdsList();
            BatchIdList = CommonBindList.GetBatchIdList();
            ServiceCodesList = CommonBindList.GetServiceCodesList();
            CommRateIdList = CommonBindList.GetCommRateIdList();
            obj_BatchDebtAccount = new ADBatchDebtAccount();
            obj_BatchDebtAccountEdit = new ADBatchDebtAccount();
        }

        public string ddlServiceCodes { get; set; }
        public List<CommonBindList> ServiceCodesList { get; set; }
        public ADBatchDebtAccount obj_BatchDebtAccount { get; set; }
        public ADBatchDebtAccount obj_BatchDebtAccountEdit { get; set; }
        public ADClient obj_Client { get; set; }
        public ADClientCollectionInfo obj_ClientCollectionInfo { get; set; }
        public ADClientContract obj_ClientContract { get; set; }
        public string ddlBatchId { get; set; }
        //public List<CommonBindList> BatchIdList { get; set; }
        public IEnumerable<SelectListItem> BatchIdList { get; set; }
        public string BatchDebtAccountId { get; set; }
        public string BatchDebtAccountIds { get; set; }

        public string UniqueId { get; set; }
        public string BatchId { get; set; }

        public string lblClientInfo { get; set; }
        public string ClientId { get; set; }
        public string lblMiscVar { get; set; }
        public string ContractId { get; set; }


        public bool BranchBox { get; set; }

        public bool chkRate { get; set; }
        public bool chkEstAudit { get; set; }
        public bool chkRapidRebate { get; set; }
        public bool chkInternational { get; set; }
        public bool chkSkip { get; set; }
        public bool chkJudgment { get; set; }
        public bool chkSecondary { get; set; }
        public bool chkNoCollectionFee { get; set; }
        public bool chkTenDayContactPlan { get; set; }

        public string ddlCommRateId { get; set; }
        public IEnumerable<SelectListItem> CommRateIdList { get; set; }

        public bool IsIntlClient { get; set; }
        public bool IsTenDayContactPlan { get; set; }
        
        public List<ADDebtAccountAttachment> obj_DebtAccountAttachment { get; set; }

    }
}
