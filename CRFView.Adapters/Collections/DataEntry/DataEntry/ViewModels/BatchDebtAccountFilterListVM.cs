﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRFView.Adapters;

namespace CRFView.Adapters.Collections.DataEntry.DataEntry.ViewModels
{
    public class BatchDebtAccountFilterListVM
    {
        public BatchDebtAccountFilterListVM()
        {

        }
        #region Properties

        #endregion

        #region CRUD
        public List<ADvwBatchDebtAccountFilterList> ObjBatchDebtAccount { get; set; }

        #endregion

    }
}
