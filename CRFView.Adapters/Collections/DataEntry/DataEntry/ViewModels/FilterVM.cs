﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Collections.DataEntry.DataEntry.ViewModels
{
    public class FilterVM
    {
        public FilterVM()
        {
            BatchIdList = CommonBindList.GetBatchIdList();
            ObjADBatch = new ADBatch();
            FilterObj = new Filter();
        }

        public Filter FilterObj { get; set; }
        public int Id { get; set; }
        public IEnumerable<SelectListItem> BatchIdList { get; set; }
        public ADBatch ObjADBatch { get; set; }
    }
}
