﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.DataEntry.DataEntry
{
    public class Filter
    {

        #region Properties
 
        public string DebtorName { get; set; }
        public string AccountNumber { get; set; }
        public string ddlBatchId { get; set; }

        #endregion

    }
}
