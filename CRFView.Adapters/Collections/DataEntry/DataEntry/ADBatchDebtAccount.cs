﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADBatchDebtAccount
    {

        #region Properties

        public int BatchDebtAccountId { get; set; }
        public int BatchId { get; set; }
        public int DebtId { get; set; }
        public int DebtAccountId { get; set; }
        public int ClientId { get; set; }
        public int ContractId { get; set; }
        public string AccountNo { get; set; }
        public string DebtorName { get; set; }
        public string ContactName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
        public string PhoneNo { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string TaxId { get; set; }
        public string ServiceType { get; set; }
        public decimal InterestRate { get; set; }
        public DateTime ReferalDate { get; set; }
        public DateTime LastServiceDate { get; set; }
        public DateTime LastPaymentDate { get; set; }
        public DateTime InterestFromDate { get; set; }
        public DateTime InterestThruDate { get; set; }
        public decimal TotAsgAmt { get; set; }
        public bool IsUpdate { get; set; }
        public bool IsInvalid { get; set; }
        public string ErrorDescription { get; set; }
        public int SubmittedByUserId { get; set; }
        public DateTime SubmittedOn { get; set; }
        public string ClientAgentCode { get; set; }
        public string BatchType { get; set; }
        public string ServiceCode { get; set; }
        public string AddrType_2 { get; set; }
        public string ContactName_2 { get; set; }
        public string AddressLine1_2 { get; set; }
        public string AddressLine2_2 { get; set; }
        public string City_2 { get; set; }
        public string State_2 { get; set; }
        public string PostalCode_2 { get; set; }
        public string County_2 { get; set; }
        public string Country_2 { get; set; }
        public string PhoneNo_2 { get; set; }
        public string Fax_2 { get; set; }
        public string Email_2 { get; set; }
        public string AddrNote_2 { get; set; }
        public string AddrType_3 { get; set; }
        public string ContactName_3 { get; set; }
        public string AddressLine1_3 { get; set; }
        public string AddressLine2_3 { get; set; }
        public string City_3 { get; set; }
        public string State_3 { get; set; }
        public string PostalCode_3 { get; set; }
        public string County_3 { get; set; }
        public string Country_3 { get; set; }
        public string PhoneNo_3 { get; set; }
        public string Fax_3 { get; set; }
        public string Email_3 { get; set; }
        public string AccountNote { get; set; }
        public decimal InsDeductAmt { get; set; }
        public string CollectionStatus { get; set; }
        public string ClaimNum { get; set; }
        public string FileNumber { get; set; }
        public string Priority { get; set; }
        public bool IsSkip { get; set; }
        public bool IsSecondary { get; set; }
        public bool IsJudgmentAcct { get; set; }
        public bool IsIntlAcct { get; set; }
        public string BranchNum { get; set; }
        public string PhoneNoExt { get; set; }
        public string Cell { get; set; }
        public int LocationId { get; set; }
        public string Location { get; set; }
        public int CollectionStatusId { get; set; }
        public int CommRateId { get; set; }
        public string CommRate { get; set; }
        public string PhoneNoExt_2 { get; set; }
        public string Cell_2 { get; set; }
        public string PhoneNoExt_3 { get; set; }
        public string Cell_3 { get; set; }
        public bool IsEstAudit { get; set; }
        public bool IsRapidRebate { get; set; }
        public string AddrNote_3 { get; set; }
        public decimal ExchangeRate { get; set; }
        public bool IsError { get; set; }
        public bool IsWarning { get; set; }
        public DateTime UpdatedOn { get; set; }
        public int ClientGroupNum { get; set; }
        public bool IsNoCollectFee { get; set; }
        public bool IsTenDayContactPlan { get; set; }
        public string PhoneNo2 { get; set; }
        public string PhoneNo2_Ext { get; set; }
        public string PhoneNo2_2 { get; set; }
        public string PhoneNo2_Ext_2 { get; set; }
        public string PhoneNo2_3 { get; set; }
        public string PhoneNo2_Ext_3 { get; set; }
        public decimal UsageFee { get; set; }
        public decimal EarlyTerminationFee { get; set; }
        public string ServiceState { get; set; }
        public string Note { get; set; }
        #endregion


        #region CRUD

        //Read Details from database
        public static ADBatchDebtAccount ReadBatchDebtAccounts(int id)
        {
            BatchDebtAccount myentity = new BatchDebtAccount();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (BatchDebtAccount)myentityItable;
            AutoMapper.Mapper.CreateMap<BatchDebtAccount, ADBatchDebtAccount> ();
            return (AutoMapper.Mapper.Map<ADBatchDebtAccount> (myentity));
        }


        public static int SaveBatchDebtAccount(ADBatchDebtAccount objUpdatedADBatchDebtAccount)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADBatchDebtAccount, BatchDebtAccount>();
                ITable tblBatchDebtAccount;
                BatchDebtAccount objBatchDebtAccount;

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                if (objUpdatedADBatchDebtAccount.BatchDebtAccountId != 0)
                {
                    ADBatchDebtAccount objADBatchDebtAccountOriginal = ReadBatchDebtAccounts(Convert.ToInt32(objUpdatedADBatchDebtAccount.BatchDebtAccountId));

                    if (objUpdatedADBatchDebtAccount.ClientId == 0)
                    {
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.ClientId = objUpdatedADBatchDebtAccount.ClientId;
                    }

                    if (objUpdatedADBatchDebtAccount.ContractId == 0)
                    {
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.ContractId = objUpdatedADBatchDebtAccount.ContractId;
                    }


                    #region Main Info 

                    if(!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.AccountNo))
                    {
                        objADBatchDebtAccountOriginal.AccountNo = objUpdatedADBatchDebtAccount.AccountNo;
                    }

                    if (objUpdatedADBatchDebtAccount.ReferalDate == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.ReferalDate = objUpdatedADBatchDebtAccount.ReferalDate;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.BranchNum))
                    {
                        objADBatchDebtAccountOriginal.BranchNum = objUpdatedADBatchDebtAccount.BranchNum;
                    }

                    if (objUpdatedADBatchDebtAccount.LastServiceDate == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.LastServiceDate = objUpdatedADBatchDebtAccount.LastServiceDate;
                    }
 
                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.DebtorName))
                    {
                        objADBatchDebtAccountOriginal.DebtorName = objUpdatedADBatchDebtAccount.DebtorName;
                    }

                    if (objUpdatedADBatchDebtAccount.LastPaymentDate == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.LastPaymentDate = objUpdatedADBatchDebtAccount.LastPaymentDate;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.ContactName))
                    {
                        objADBatchDebtAccountOriginal.ContactName = objUpdatedADBatchDebtAccount.ContactName;
                    }

                    if (objUpdatedADBatchDebtAccount.TotAsgAmt == 0)
                    {
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.TotAsgAmt = objUpdatedADBatchDebtAccount.TotAsgAmt;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.AddressLine1))
                    {
                        objADBatchDebtAccountOriginal.AddressLine1 = objUpdatedADBatchDebtAccount.AddressLine1;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.AddressLine2))
                    {
                        objADBatchDebtAccountOriginal.AddressLine2 = objUpdatedADBatchDebtAccount.AddressLine2;
                    }

                    if (objUpdatedADBatchDebtAccount.ExchangeRate == 0)
                    {
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.ExchangeRate = objUpdatedADBatchDebtAccount.ExchangeRate;
                    }


                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.City))
                    {
                        objADBatchDebtAccountOriginal.City = objUpdatedADBatchDebtAccount.City;
                    }


                    if (objUpdatedADBatchDebtAccount.CommRateId == 0)
                    {
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.CommRateId = objUpdatedADBatchDebtAccount.CommRateId;
                    }


                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.State))
                    {
                        objADBatchDebtAccountOriginal.State = objUpdatedADBatchDebtAccount.State;
                    }


                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.PostalCode))
                    {
                        objADBatchDebtAccountOriginal.PostalCode = objUpdatedADBatchDebtAccount.PostalCode;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.ClientAgentCode))
                    {
                        objADBatchDebtAccountOriginal.ClientAgentCode = objUpdatedADBatchDebtAccount.ClientAgentCode;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.PhoneNo))
                    {
                        objADBatchDebtAccountOriginal.PhoneNo = objUpdatedADBatchDebtAccount.PhoneNo;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.PhoneNoExt))
                    {
                        objADBatchDebtAccountOriginal.PhoneNoExt = objUpdatedADBatchDebtAccount.PhoneNoExt;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.AccountNote))
                    {
                        objADBatchDebtAccountOriginal.AccountNote = objUpdatedADBatchDebtAccount.AccountNote;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.PhoneNo2))
                    {
                        objADBatchDebtAccountOriginal.PhoneNo2 = objUpdatedADBatchDebtAccount.PhoneNo2;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.Cell))
                    {
                        objADBatchDebtAccountOriginal.Cell = objUpdatedADBatchDebtAccount.Cell;
                    }

                    if (objUpdatedADBatchDebtAccount.IsEstAudit == false)
                    {
                        objADBatchDebtAccountOriginal.IsEstAudit = false;
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.IsEstAudit = objUpdatedADBatchDebtAccount.IsEstAudit;
                    }

                    if (objUpdatedADBatchDebtAccount.IsRapidRebate == false)
                    {
                        objADBatchDebtAccountOriginal.IsRapidRebate = false;
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.IsRapidRebate = objUpdatedADBatchDebtAccount.IsRapidRebate;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.Fax))
                    {
                        objADBatchDebtAccountOriginal.Fax = objUpdatedADBatchDebtAccount.Fax;
                    }

                    if (objUpdatedADBatchDebtAccount.IsIntlAcct == false)
                    {
                        objADBatchDebtAccountOriginal.IsIntlAcct = false;
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.IsIntlAcct = objUpdatedADBatchDebtAccount.IsIntlAcct;
                    }

                    if (objUpdatedADBatchDebtAccount.IsSkip == false)
                    {
                        objADBatchDebtAccountOriginal.IsSkip = false;
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.IsSkip = objUpdatedADBatchDebtAccount.IsSkip;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.Email))
                    {
                        objADBatchDebtAccountOriginal.Email = objUpdatedADBatchDebtAccount.Email;
                    }

                    if (objUpdatedADBatchDebtAccount.IsJudgmentAcct == false)
                    {
                        objADBatchDebtAccountOriginal.IsJudgmentAcct = false;
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.IsJudgmentAcct = objUpdatedADBatchDebtAccount.IsJudgmentAcct;
                    }

                    if (objUpdatedADBatchDebtAccount.IsSecondary == false)
                    {
                        objADBatchDebtAccountOriginal.IsSecondary = false;
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.IsSecondary = objUpdatedADBatchDebtAccount.IsSecondary;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.TaxId))
                    {
                        objADBatchDebtAccountOriginal.TaxId = objUpdatedADBatchDebtAccount.TaxId;
                    }

                    if (objUpdatedADBatchDebtAccount.IsNoCollectFee == false)
                    {
                        objADBatchDebtAccountOriginal.IsNoCollectFee = false;
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.IsNoCollectFee = objUpdatedADBatchDebtAccount.IsNoCollectFee;
                    }

                    if (objUpdatedADBatchDebtAccount.IsTenDayContactPlan == false)
                    {
                        objADBatchDebtAccountOriginal.IsTenDayContactPlan = false;
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.IsTenDayContactPlan = objUpdatedADBatchDebtAccount.IsTenDayContactPlan;
                    }

                    #endregion

                    #region Additional Addresses
                    #region Address 2
                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.ContactName_2))
                    {
                        objADBatchDebtAccountOriginal.ContactName_2 = objUpdatedADBatchDebtAccount.ContactName_2;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.AddressLine1_2))
                    {
                        objADBatchDebtAccountOriginal.AddressLine1_2 = objUpdatedADBatchDebtAccount.AddressLine1_2;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.AddressLine2_2))
                    {
                        objADBatchDebtAccountOriginal.AddressLine2_2 = objUpdatedADBatchDebtAccount.AddressLine2_2;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.City_2))
                    {
                        objADBatchDebtAccountOriginal.City_2 = objUpdatedADBatchDebtAccount.City_2;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.State_2))
                    {
                        objADBatchDebtAccountOriginal.State_2 = objUpdatedADBatchDebtAccount.State_2;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.PostalCode_2))
                    {
                        objADBatchDebtAccountOriginal.PostalCode_2 = objUpdatedADBatchDebtAccount.PostalCode_2;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.PhoneNo_2))
                    {
                        objADBatchDebtAccountOriginal.PhoneNo_2 = objUpdatedADBatchDebtAccount.PhoneNo_2;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.PhoneNoExt_2))
                    {
                        objADBatchDebtAccountOriginal.PhoneNoExt_2 = objUpdatedADBatchDebtAccount.PhoneNoExt_2;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.Cell_2))
                    {
                        objADBatchDebtAccountOriginal.Cell_2 = objUpdatedADBatchDebtAccount.Cell_2;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.Email_2))
                    {
                        objADBatchDebtAccountOriginal.Email_2 = objUpdatedADBatchDebtAccount.Email_2;
                    }
                    #endregion

                    #region Address 3
                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.ContactName_3))
                    {
                        objADBatchDebtAccountOriginal.ContactName_3= objUpdatedADBatchDebtAccount.ContactName_3;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.AddressLine1_3))
                    {
                        objADBatchDebtAccountOriginal.AddressLine1_3 = objUpdatedADBatchDebtAccount.AddressLine1_3;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.AddressLine2_3))
                    {
                        objADBatchDebtAccountOriginal.AddressLine2_3 = objUpdatedADBatchDebtAccount.AddressLine2_3;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.City_3))
                    {
                        objADBatchDebtAccountOriginal.City_3= objUpdatedADBatchDebtAccount.City_3;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.State_3))
                    {
                        objADBatchDebtAccountOriginal.State_3= objUpdatedADBatchDebtAccount.State_3;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.PostalCode_3))
                    {
                        objADBatchDebtAccountOriginal.PostalCode_3 = objUpdatedADBatchDebtAccount.PostalCode_3;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.PhoneNo_3))
                    {
                        objADBatchDebtAccountOriginal.PhoneNo_3 = objUpdatedADBatchDebtAccount.PhoneNo_3;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.PhoneNoExt_3))
                    {
                        objADBatchDebtAccountOriginal.PhoneNoExt_3 = objUpdatedADBatchDebtAccount.PhoneNoExt_3;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.Cell_3))
                    {
                        objADBatchDebtAccountOriginal.Cell_3 = objUpdatedADBatchDebtAccount.Cell_3;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.Email_3))
                    {
                        objADBatchDebtAccountOriginal.Email_3 = objUpdatedADBatchDebtAccount.Email_3;
                    }
                    #endregion

                    #endregion

                    #region Documents


                    #endregion

                    objADBatchDebtAccountOriginal.UpdatedOn = DateTime.Now;
                    objBatchDebtAccount = AutoMapper.Mapper.Map<BatchDebtAccount>(objADBatchDebtAccountOriginal);
                    tblBatchDebtAccount = (ITable)objBatchDebtAccount;
                    DBO.Provider.Update(ref tblBatchDebtAccount);
                }
                else
                {
                    objUpdatedADBatchDebtAccount.UpdatedOn = DateTime.Now;
                    objUpdatedADBatchDebtAccount.SubmittedByUserId = user.Id;
                    objUpdatedADBatchDebtAccount.SubmittedOn = DateTime.Now;

                    objBatchDebtAccount = AutoMapper.Mapper.Map<BatchDebtAccount>(objUpdatedADBatchDebtAccount);
                    tblBatchDebtAccount = (ITable)objBatchDebtAccount;
                    DBO.Provider.Create(ref tblBatchDebtAccount); 
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static BatchDebtAccount SaveBatchDebtAccount(ADBatchDebtAccount objUpdatedADBatchDebtAccount, int i)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADBatchDebtAccount, BatchDebtAccount>();
                ITable tblBatchDebtAccount;
                BatchDebtAccount objBatchDebtAccount;

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                if (objUpdatedADBatchDebtAccount.BatchDebtAccountId != 0)
                {
                    ADBatchDebtAccount objADBatchDebtAccountOriginal = ReadBatchDebtAccounts(Convert.ToInt32(objUpdatedADBatchDebtAccount.BatchDebtAccountId));

                    if (objUpdatedADBatchDebtAccount.ClientId == 0)
                    {
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.ClientId = objUpdatedADBatchDebtAccount.ClientId;
                    }

                    if (objUpdatedADBatchDebtAccount.ContractId == 0)
                    {
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.ContractId = objUpdatedADBatchDebtAccount.ContractId;
                    }


                    #region Main Info 

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.AccountNo))
                    {
                        objADBatchDebtAccountOriginal.AccountNo = objUpdatedADBatchDebtAccount.AccountNo;
                    }

                    if (objUpdatedADBatchDebtAccount.ReferalDate == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.ReferalDate = objUpdatedADBatchDebtAccount.ReferalDate;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.BranchNum))
                    {
                        objADBatchDebtAccountOriginal.BranchNum = objUpdatedADBatchDebtAccount.BranchNum;
                    }

                    if (objUpdatedADBatchDebtAccount.LastServiceDate == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.LastServiceDate = objUpdatedADBatchDebtAccount.LastServiceDate;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.DebtorName))
                    {
                        objADBatchDebtAccountOriginal.DebtorName = objUpdatedADBatchDebtAccount.DebtorName;
                    }

                    if (objUpdatedADBatchDebtAccount.LastPaymentDate == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.LastPaymentDate = objUpdatedADBatchDebtAccount.LastPaymentDate;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.ContactName))
                    {
                        objADBatchDebtAccountOriginal.ContactName = objUpdatedADBatchDebtAccount.ContactName;
                    }

                    if (objUpdatedADBatchDebtAccount.TotAsgAmt == 0)
                    {
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.TotAsgAmt = objUpdatedADBatchDebtAccount.TotAsgAmt;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.AddressLine1))
                    {
                        objADBatchDebtAccountOriginal.AddressLine1 = objUpdatedADBatchDebtAccount.AddressLine1;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.AddressLine2))
                    {
                        objADBatchDebtAccountOriginal.AddressLine2 = objUpdatedADBatchDebtAccount.AddressLine2;
                    }

                    if (objUpdatedADBatchDebtAccount.ExchangeRate == 0)
                    {
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.ExchangeRate = objUpdatedADBatchDebtAccount.ExchangeRate;
                    }


                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.City))
                    {
                        objADBatchDebtAccountOriginal.City = objUpdatedADBatchDebtAccount.City;
                    }


                    if (objUpdatedADBatchDebtAccount.CommRateId == 0)
                    {
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.CommRateId = objUpdatedADBatchDebtAccount.CommRateId;
                    }


                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.State))
                    {
                        objADBatchDebtAccountOriginal.State = objUpdatedADBatchDebtAccount.State;
                    }


                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.PostalCode))
                    {
                        objADBatchDebtAccountOriginal.PostalCode = objUpdatedADBatchDebtAccount.PostalCode;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.ClientAgentCode))
                    {
                        objADBatchDebtAccountOriginal.ClientAgentCode = objUpdatedADBatchDebtAccount.ClientAgentCode;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.PhoneNo))
                    {
                        objADBatchDebtAccountOriginal.PhoneNo = objUpdatedADBatchDebtAccount.PhoneNo;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.PhoneNoExt))
                    {
                        objADBatchDebtAccountOriginal.PhoneNoExt = objUpdatedADBatchDebtAccount.PhoneNoExt;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.AccountNote))
                    {
                        objADBatchDebtAccountOriginal.AccountNote = objUpdatedADBatchDebtAccount.AccountNote;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.PhoneNo2))
                    {
                        objADBatchDebtAccountOriginal.PhoneNo2 = objUpdatedADBatchDebtAccount.PhoneNo2;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.Cell))
                    {
                        objADBatchDebtAccountOriginal.Cell = objUpdatedADBatchDebtAccount.Cell;
                    }

                    if (objUpdatedADBatchDebtAccount.IsEstAudit == false)
                    {
                        objADBatchDebtAccountOriginal.IsEstAudit = false;
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.IsEstAudit = objUpdatedADBatchDebtAccount.IsEstAudit;
                    }

                    if (objUpdatedADBatchDebtAccount.IsRapidRebate == false)
                    {
                        objADBatchDebtAccountOriginal.IsRapidRebate = false;
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.IsRapidRebate = objUpdatedADBatchDebtAccount.IsRapidRebate;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.Fax))
                    {
                        objADBatchDebtAccountOriginal.Fax = objUpdatedADBatchDebtAccount.Fax;
                    }

                    if (objUpdatedADBatchDebtAccount.IsIntlAcct == false)
                    {
                        objADBatchDebtAccountOriginal.IsIntlAcct = false;
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.IsIntlAcct = objUpdatedADBatchDebtAccount.IsIntlAcct;
                    }

                    if (objUpdatedADBatchDebtAccount.IsSkip == false)
                    {
                        objADBatchDebtAccountOriginal.IsSkip = false;
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.IsSkip = objUpdatedADBatchDebtAccount.IsSkip;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.Email))
                    {
                        objADBatchDebtAccountOriginal.Email = objUpdatedADBatchDebtAccount.Email;
                    }

                    if (objUpdatedADBatchDebtAccount.IsJudgmentAcct == false)
                    {
                        objADBatchDebtAccountOriginal.IsJudgmentAcct = false;
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.IsJudgmentAcct = objUpdatedADBatchDebtAccount.IsJudgmentAcct;
                    }

                    if (objUpdatedADBatchDebtAccount.IsSecondary == false)
                    {
                        objADBatchDebtAccountOriginal.IsSecondary = false;
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.IsSecondary = objUpdatedADBatchDebtAccount.IsSecondary;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.TaxId))
                    {
                        objADBatchDebtAccountOriginal.TaxId = objUpdatedADBatchDebtAccount.TaxId;
                    }

                    if (objUpdatedADBatchDebtAccount.IsNoCollectFee == false)
                    {
                        objADBatchDebtAccountOriginal.IsNoCollectFee = false;
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.IsNoCollectFee = objUpdatedADBatchDebtAccount.IsNoCollectFee;
                    }

                    if (objUpdatedADBatchDebtAccount.IsTenDayContactPlan == false)
                    {
                        objADBatchDebtAccountOriginal.IsTenDayContactPlan = false;
                    }
                    else
                    {
                        objADBatchDebtAccountOriginal.IsTenDayContactPlan = objUpdatedADBatchDebtAccount.IsTenDayContactPlan;
                    }

                    #endregion

                    #region Additional Addresses
                    #region Address 2
                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.ContactName_2))
                    {
                        objADBatchDebtAccountOriginal.ContactName_2 = objUpdatedADBatchDebtAccount.ContactName_2;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.AddressLine1_2))
                    {
                        objADBatchDebtAccountOriginal.AddressLine1_2 = objUpdatedADBatchDebtAccount.AddressLine1_2;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.AddressLine2_2))
                    {
                        objADBatchDebtAccountOriginal.AddressLine2_2 = objUpdatedADBatchDebtAccount.AddressLine2_2;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.City_2))
                    {
                        objADBatchDebtAccountOriginal.City_2 = objUpdatedADBatchDebtAccount.City_2;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.State_2))
                    {
                        objADBatchDebtAccountOriginal.State_2 = objUpdatedADBatchDebtAccount.State_2;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.PostalCode_2))
                    {
                        objADBatchDebtAccountOriginal.PostalCode_2 = objUpdatedADBatchDebtAccount.PostalCode_2;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.PhoneNo_2))
                    {
                        objADBatchDebtAccountOriginal.PhoneNo_2 = objUpdatedADBatchDebtAccount.PhoneNo_2;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.PhoneNoExt_2))
                    {
                        objADBatchDebtAccountOriginal.PhoneNoExt_2 = objUpdatedADBatchDebtAccount.PhoneNoExt_2;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.Cell_2))
                    {
                        objADBatchDebtAccountOriginal.Cell_2 = objUpdatedADBatchDebtAccount.Cell_2;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.Email_2))
                    {
                        objADBatchDebtAccountOriginal.Email_2 = objUpdatedADBatchDebtAccount.Email_2;
                    }
                    #endregion

                    #region Address 3
                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.ContactName_3))
                    {
                        objADBatchDebtAccountOriginal.ContactName_3 = objUpdatedADBatchDebtAccount.ContactName_3;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.AddressLine1_3))
                    {
                        objADBatchDebtAccountOriginal.AddressLine1_3 = objUpdatedADBatchDebtAccount.AddressLine1_3;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.AddressLine2_3))
                    {
                        objADBatchDebtAccountOriginal.AddressLine2_3 = objUpdatedADBatchDebtAccount.AddressLine2_3;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.City_3))
                    {
                        objADBatchDebtAccountOriginal.City_3 = objUpdatedADBatchDebtAccount.City_3;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.State_3))
                    {
                        objADBatchDebtAccountOriginal.State_3 = objUpdatedADBatchDebtAccount.State_3;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.PostalCode_3))
                    {
                        objADBatchDebtAccountOriginal.PostalCode_3 = objUpdatedADBatchDebtAccount.PostalCode_3;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.PhoneNo_3))
                    {
                        objADBatchDebtAccountOriginal.PhoneNo_3 = objUpdatedADBatchDebtAccount.PhoneNo_3;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.PhoneNoExt_3))
                    {
                        objADBatchDebtAccountOriginal.PhoneNoExt_3 = objUpdatedADBatchDebtAccount.PhoneNoExt_3;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.Cell_3))
                    {
                        objADBatchDebtAccountOriginal.Cell_3 = objUpdatedADBatchDebtAccount.Cell_3;
                    }

                    if (!string.IsNullOrEmpty(objUpdatedADBatchDebtAccount.Email_3))
                    {
                        objADBatchDebtAccountOriginal.Email_3 = objUpdatedADBatchDebtAccount.Email_3;
                    }
                    #endregion

                    #endregion

                    #region Documents


                    #endregion

                    objADBatchDebtAccountOriginal.UpdatedOn = DateTime.Now;
                    objBatchDebtAccount = AutoMapper.Mapper.Map<BatchDebtAccount>(objADBatchDebtAccountOriginal);
                    tblBatchDebtAccount = (ITable)objBatchDebtAccount;
                    DBO.Provider.Update(ref tblBatchDebtAccount);
                }
                else
                {
                    objUpdatedADBatchDebtAccount.UpdatedOn = DateTime.Now;
                    objUpdatedADBatchDebtAccount.SubmittedByUserId = user.Id;
                    objUpdatedADBatchDebtAccount.SubmittedOn = DateTime.Now;

                    objBatchDebtAccount = AutoMapper.Mapper.Map<BatchDebtAccount>(objUpdatedADBatchDebtAccount);
                    tblBatchDebtAccount = (ITable)objBatchDebtAccount;
                    DBO.Provider.Create(ref tblBatchDebtAccount);
                }

                return objBatchDebtAccount;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return null;
            }
        }


        public static DataTable GetBatchDebtAccountForWebPlacements()
        {
            try
            {
                string sql = "Select * from batchDebtAccount where BatchId = -1 and iserror = 0 ";

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        public static int CreateBatchWebPlacements()
        {
            try
            {
                    CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                    var MYSPROC = new cview_DataEntry_BatchWebPlacements();
                    MYSPROC.UserId = user.Id;
                    MYSPROC.UserName = user.UserName;
                    DBO.Provider.ExecNonQuery(MYSPROC);

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static string GetLastBatchDebtAccountId(ADBatchDebtAccount obj_BatchDebtAccount)
        {
            try
            {
                var sql = "SELECT top 1 BatchDebtAccountId FROM BatchDebtAccount where BatchId = " + obj_BatchDebtAccount.BatchId + " Order by BatchDebtAccountId desc";
                var BatchIdListView = DBO.Provider.GetTableView(sql);
                DataTable dt = BatchIdListView.ToTable();
                var BatchDebtAccountId = dt.Rows[0]["BatchDebtAccountId"];
                return BatchDebtAccountId.ToString();
            }
            catch
            {
                return null;
            }
        }

        public static int DeleteBatchDebtAccount(int BatchDebtAccountId,string batchId)
        {
            AutoMapper.Mapper.CreateMap<ADBatchDebtAccount, BatchDebtAccount>();
            ITable tblBatchDebtAccount;
            TableView EntityList;
            BatchDebtAccount objBatchDebtAccount;

            
             ADBatchDebtAccount objADBatchDebtAccount = ReadBatchDebtAccounts(BatchDebtAccountId);
            objBatchDebtAccount = AutoMapper.Mapper.Map<BatchDebtAccount>(objADBatchDebtAccount);
            tblBatchDebtAccount = (ITable)objBatchDebtAccount;
            DBO.Provider.Delete(ref tblBatchDebtAccount);

            string sql = "SELECT  * FROM VWBatchDebtAccount VW WHERE BatchId = " + batchId + "ORDER BY BATCHDEBTACCOUNTID DESC ";
            EntityList = DBO.Provider.GetTableView(sql);
            EntityList.AllowDelete = true;
            EntityList.Delete(EntityList.RowIndex);
             

            return 1;
        }

        #endregion
    }
}
