﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using CRFView.Adapters.Liens.DataEntry.PostBatch.ViewModels;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADBatch
    {
        #region Properties
        public int BatchId { get; set; }
        public DateTime BatchDate { get; set; }
        public DateTime FileDate { get; set; }
        public string BatchType { get; set; }
        public string BatchStatus { get; set; }
        public DateTime ProcessOn { get; set; }
        public DateTime TimeStarted { get; set; }
        public DateTime TimeEnded { get; set; }
        public int Count1 { get; set; }
        public int Count2 { get; set; }
        public int Count3 { get; set; }
        public int Count4 { get; set; }
        public int Count5 { get; set; }
        public int Count6 { get; set; }
        public int Count7 { get; set; }
        public int Count8 { get; set; }
        public int Count9 { get; set; }
        public int Count10 { get; set; }
        public int SubmittedById { get; set; }
        public DateTime ClosedDate { get; set; }
        public string SubmittedBy { get; set; }
        public string SelectionInfo { get; set; }
        #endregion

        #region CRUD
        //Read Details from database
        public static ADBatch ReadBatch(int id)
        {
            Batch myentity = new Batch();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (Batch)myentityItable;
            AutoMapper.Mapper.CreateMap<Batch, ADBatch>();
            return (AutoMapper.Mapper.Map<ADBatch>(myentity));
        }

        public static int SaveBatch(ADBatch objUpdatedBatch)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADBatch, Batch>();
                ITable tblBatch;
                Batch objBatch;

                if (objUpdatedBatch.BatchId != 0)
                {
                    ADBatch objBatchOriginal = ReadBatch(Convert.ToInt32(objUpdatedBatch.BatchId));

                    objBatch = AutoMapper.Mapper.Map<Batch>(objBatchOriginal);
                    tblBatch = (ITable)objBatch;
                    DBO.Provider.Update(ref tblBatch);
                }
                else
                {
                    objBatch = AutoMapper.Mapper.Map<Batch>(objUpdatedBatch);
                    tblBatch = (ITable)objBatch;
                    DBO.Provider.Create(ref tblBatch);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }


        public static int ReleaseBatch(string BatchId)
        {
            try
            {
                var myds = new DataSet();
                var mysproc = new cview_DataEntry_ReleaseBatch();
                mysproc.BatchId = Convert.ToInt32(BatchId);
                myds = DBO.Provider.GetDataSet(mysproc);

                ADBatch objBatch = ReadBatch(Convert.ToInt32(BatchId));

                if(objBatch.BatchStatus != "RELEASE")
                {
                    return 0;
                }
                else
                {
                    return 1;
                }

               
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }


        public static int ReleaseLienBatch(string BatchId)
        {
            try
            {
                var myds = new DataSet();
                var mysproc = new uspbo_LiensDataEntry_ReleaseBatch();
                mysproc.BatchId = Convert.ToInt32(BatchId);
                myds = DBO.Provider.GetDataSet(mysproc);

                ADBatch objBatch = ReadBatch(Convert.ToInt32(BatchId));

                if (objBatch.BatchStatus != "RELEASE")
                {
                    return 0;
                }
                else
                {
                    return 1;
                }


            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int DeleteBatch(string BatchId)
        {
            try
            {
                ITable IReport;
                Batch objBatch = new Batch();
                objBatch.BatchId = Convert.ToInt32(BatchId);
                IReport = objBatch;
                DBO.Provider.Delete(ref IReport);
                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }


        public static DataTable GetBatchForBatchPosts()
        {
            try
            {
                string sql = "Select * from batch where BatchType IN ( 'COLL', 'CLVW' ) AND BatchStatus = 'RELEASE' ";

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        public static int PostBatches()
        {
            try
            {
                var MYSPROC = new cview_DataEntry_PostBatches();
                DBO.Provider.ExecNonQuery(MYSPROC);

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static DataTable GetLienBatchForBatchPosts()
        {
            try
            {
                string sql = "Select * from batch where BatchType IN ( 'LIEN', 'LIENCV' ) AND BatchStatus = 'RELEASE' ";

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

    
        //Print Lien Batch Schedule
        public string PostLienBatches(PostBatchVM objPostBatchVM)
        {
            string pdfPath = null;
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                //user.SettingsFolder = user.SettingsFolder + "\\" ;

                var mysproc = new uspbo_LiensDataEntry_PostBatches();
                DBO.Provider.ExecNonQuery(mysproc);

                var mysproc2 = new uspbo_LiensDataEntry_GetErrorFromPostBatches();

                TableView myview = DBO.Provider.GetTableView(mysproc2);

                CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;

                if(myview.Count > 0)
                {
                    pdfPath = CRF.BLL.Providers.LiensDataEntry.PrintBatchList(user, mymode, myview);
                }
 
            }

            catch (Exception ex)
            {
                return "";
            }
            return pdfPath;
        }


        #endregion
    }
}
