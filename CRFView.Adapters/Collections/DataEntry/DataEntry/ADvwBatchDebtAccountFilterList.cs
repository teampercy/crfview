﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.Providers;
using HDS.DAL.COMMON;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADvwBatchDebtAccountFilterList
    {
        #region Properties

        public DateTime BatchDate { get; set; }
        public DateTime FileDate { get; set; }
        public string BatchStatus { get; set; }
        public DateTime ProcessOn { get; set; }
        public string ClientContactEmail { get; set; }
        public string ClientContactPhone { get; set; }
        public string ClientName { get; set; }
        public string ClientAddressLine1 { get; set; }
        public string ClientAddressLine2 { get; set; }
        public string ClientCity { get; set; }
        public string ClientState { get; set; }
        public string ClientPostalCode { get; set; }
        public string ClientEmail { get; set; }
        public string ClientPhoneNo { get; set; }
        public string ClientContactName { get; set; }
        public string ClientContactTitle { get; set; }
        public string ClientPhoneNoExt { get; set; }
        public string ClientContactSalutation { get; set; }
        public bool NewBusVar { get; set; }
        public bool Visa { get; set; }
        public bool CCMC { get; set; }
        public bool Amex { get; set; }
        public bool Discover { get; set; }
        public string ReportVars { get; set; }
        public string MscVars { get; set; }
        public string UserName { get; set; }
        public string BatchType { get; set; }
        public int BatchDebtAccountId { get; set; }
        public int BatchId { get; set; }
        public int DebtAccountId { get; set; }
        public int ClientId { get; set; }
        public int ContractId { get; set; }
        public string ClientCode { get; set; }
        public string AccountNo { get; set; }
        public string DebtorName { get; set; }
        public string ContactName { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNo { get; set; }
        public string Cell { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string TaxId { get; set; }
        public int LocationId { get; set; }
        public string Location { get; set; }
        public int CollectionStatusId { get; set; }
        public string CollectionStatus { get; set; }
        public int CommRateId { get; set; }
        public string CommRate { get; set; }
        public string ServiceType { get; set; }
        public decimal InterestRate { get; set; }
        public DateTime ReferalDate { get; set; }
        public DateTime LastServiceDate { get; set; }
        public DateTime LastPaymentDate { get; set; }
        public DateTime InterestFromDate { get; set; }
        public DateTime InterestThruDate { get; set; }
        public decimal TotAsgAmt { get; set; }
        public bool IsUpdate { get; set; }
        public bool sInvalid { get; set; }
        public string ErrorDescription { get; set; }
        public int SubmittedByUserId { get; set; }
        public DateTime SubmittedOn { get; set; }
        public string ClientAgentCode { get; set; }
        public string AddrType_2 { get; set; }
        public string ContactName_2 { get; set; }
        public string AddressLine1_2 { get; set; }
        public string AddressLine2_2 { get; set; }
        public string City_2 { get; set; }
        public string State_2 { get; set; }
        public string PostalCode_2 { get; set; }
        public string County_2 { get; set; }
        public string Country_2 { get; set; }
        public string PhoneNo_2 { get; set; }
        public string PhoneNoExt_2 { get; set; }
        public string Cell_2 { get; set; }
        public string Fax_2 { get; set; }
        public string Email_2 { get; set; }
        public string ContactName_3 { get; set; }
        public string AddressLine1_3 { get; set; }
        public string AddressLine2_3 { get; set; }
        public string City_3 { get; set; }
        public string State_3 { get; set; }
        public string PostalCode_3 { get; set; }
        public string PhoneNo_3 { get; set; }
        public string PhoneNoExt_3 { get; set; }
        public string Cell_3 { get; set; }
        public string Fax_3 { get; set; }
        public string Email_3 { get; set; }
        public string AccountNote { get; set; }
        public decimal InsDeductAmt { get; set; }
        public string ClaimNum { get; set; }
        public bool IsEstAudit { get; set; }
        public string FileNumber { get; set; }
        public bool IsSkip { get; set; }
        public bool IsSecondary { get; set; }
        public bool IsJudgmentAcct { get; set; }
        public bool IsIntlAcct { get; set; }
        public string ServiceCode { get; set; }
        public string BranchNum { get; set; }
        public string PhoneNoExt { get; set; }
        public string Priority { get; set; }
        public bool IsRapidRebate { get; set; }
        public decimal totcollfeeamt { get; set; }
        public string AddrNote_2 { get; set; }
        public string AddrNote_3 { get; set; }
        public decimal ExchangeRate { get; set; }
        public char FixedCollRateCode { get; set; }
        public bool IsError { get; set; }
        public int ClientGroupNum { get; set; }
        public bool IsTenDayContactPlan { get; set; }

        #endregion


        #region CRUD
        public static List<ADvwBatchDebtAccountFilterList> GetInitialList( string batchId)
        {
            try
            {
                TableView EntityList;
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                // string sql = "SELECT  * FROM VWBatchDebtAccount where BatchId = 0 ORDER BY BATCHDEBTACCOUNTID DESC";
                //string sql = "SELECT* FROM VWBatchDebtAccount where BatchId in (Select top(1) BatchId from Batch Where SubmittedById = '" + user.Id + "' and batchtype in ('COLL', 'CLVW') AND BatchStatus = 'OPEN' order by BatchId desc) ORDER BY BATCHDEBTACCOUNTID DESC";
                string sql = "SELECT  * FROM VWBatchDebtAccount VW WHERE BatchId = " + batchId + "ORDER BY BATCHDEBTACCOUNTID DESC ";
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADvwBatchDebtAccountFilterList> BatchDebtAccountList = (from DataRow dr in dt.Rows
                                                                             select new ADvwBatchDebtAccountFilterList()
                                                                             {
                                                                                 BatchId = Convert.ToInt32(dr["BatchId"]),
                                                                                 BatchDebtAccountId = Convert.ToInt32(dr["BatchDebtAccountId"]),
                                                                                 ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                                                                 AccountNo = ((dr["AccountNo"] != DBNull.Value ? dr["AccountNo"].ToString() : "")),
                                                                                 DebtorName = ((dr["DebtorName"] != DBNull.Value ? dr["DebtorName"].ToString() : "")),
                                                                                 AddressLine1 = ((dr["AddressLine1"] != DBNull.Value ? dr["AddressLine1"].ToString() : "")),
                                                                                 City = ((dr["City"] != DBNull.Value ? dr["City"].ToString() : "")),
                                                                                 State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),
                                                                                 PostalCode = ((dr["PostalCode"] != DBNull.Value ? dr["PostalCode"].ToString() : "")),
                                                                                 PhoneNo = ((dr["PhoneNo"] != DBNull.Value ? dr["PhoneNo"].ToString() : "")),
                                                                                 TotAsgAmt = ((dr["TotAsgAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotAsgAmt"]) : 0)),
                                                                                 IsError = ((dr["IsError"] != DBNull.Value ? Convert.ToBoolean(dr["IsError"]) : false)),
                                                                                ErrorDescription = ((dr["ErrorDescription"] != DBNull.Value ? dr["ErrorDescription"].ToString() : "")),
                                                           }).ToList();
                return BatchDebtAccountList;
            }
            catch (Exception ex)
            {
                return (new List<ADvwBatchDebtAccountFilterList>());
            }
        }


        public static List<ADvwBatchDebtAccountFilterList> GetFilteredList(Adapters.Collections.DataEntry.DataEntry.Filter FilterObj)
        { 
            try
            {
                TableView EntityList;

                string sql = "SELECT  * FROM VWBatchDebtAccount VW WHERE BatchId = " + FilterObj.ddlBatchId;
             
                if (!string.IsNullOrEmpty(FilterObj.AccountNumber))
                {
                    sql += " And AccountNo Like '" + FilterObj.AccountNumber.Trim() + "%'";
                }

                if (!string.IsNullOrEmpty(FilterObj.DebtorName))
                {
                    sql += " And DebtorName Like '" + FilterObj.DebtorName.Trim() + "%'";
                }

                sql += " ORDER BY BATCHDEBTACCOUNTID DESC ";

                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADvwBatchDebtAccountFilterList> BatchDebtAccountList = (from DataRow dr in dt.Rows
                                                                             select new ADvwBatchDebtAccountFilterList()
                                                                             {
                                                                                 BatchId = Convert.ToInt32(dr["BatchId"]),
                                                                                 BatchDebtAccountId = Convert.ToInt32(dr["BatchDebtAccountId"]),
                                                                                 ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                                                                 AccountNo = ((dr["AccountNo"] != DBNull.Value ? dr["AccountNo"].ToString() : "")),
                                                                                 DebtorName = ((dr["DebtorName"] != DBNull.Value ? dr["DebtorName"].ToString() : "")),
                                                                                 AddressLine1 = ((dr["AddressLine1"] != DBNull.Value ? dr["AddressLine1"].ToString() : "")),
                                                                                 City = ((dr["City"] != DBNull.Value ? dr["City"].ToString() : "")),
                                                                                 State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),
                                                                                 PostalCode = ((dr["PostalCode"] != DBNull.Value ? dr["PostalCode"].ToString() : "")),
                                                                                 PhoneNo = ((dr["PhoneNo"] != DBNull.Value ? dr["PhoneNo"].ToString() : "")),
                                                                                 TotAsgAmt = ((dr["TotAsgAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotAsgAmt"]) : 0)),
                                                                                 IsError = ((dr["IsError"] != DBNull.Value ? Convert.ToBoolean(dr["IsError"]) : false)),
                                                                                 ErrorDescription = ((dr["ErrorDescription"] != DBNull.Value ? dr["ErrorDescription"].ToString() : "")),
                                                                             }).ToList();
                return BatchDebtAccountList;
            }
            catch (Exception ex)
            {
                return (new List<ADvwBatchDebtAccountFilterList>());
            }
        }


        public static List<ADvwBatchDebtAccountFilterList> LoadDebtAccountListByBatchId(string BatchId)
        {
            try
            {
                TableView EntityList;

                string sql = "SELECT  * FROM VWBatchDebtAccount VW WHERE BatchId = " + BatchId;
                   sql += " ORDER BY BATCHDEBTACCOUNTID DESC ";

                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADvwBatchDebtAccountFilterList> BatchDebtAccountList = (from DataRow dr in dt.Rows
                                                                             select new ADvwBatchDebtAccountFilterList()
                                                                             {
                                                                                 BatchId = Convert.ToInt32(dr["BatchId"]),
                                                                                 BatchDebtAccountId = Convert.ToInt32(dr["BatchDebtAccountId"]),
                                                                                 ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                                                                 AccountNo = ((dr["AccountNo"] != DBNull.Value ? dr["AccountNo"].ToString() : "")),
                                                                                 DebtorName = ((dr["DebtorName"] != DBNull.Value ? dr["DebtorName"].ToString() : "")),
                                                                                 AddressLine1 = ((dr["AddressLine1"] != DBNull.Value ? dr["AddressLine1"].ToString() : "")),
                                                                                 City = ((dr["City"] != DBNull.Value ? dr["City"].ToString() : "")),
                                                                                 State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),
                                                                                 PostalCode = ((dr["PostalCode"] != DBNull.Value ? dr["PostalCode"].ToString() : "")),
                                                                                 PhoneNo = ((dr["PhoneNo"] != DBNull.Value ? dr["PhoneNo"].ToString() : "")),
                                                                                 TotAsgAmt = ((dr["TotAsgAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotAsgAmt"]) : 0)),
                                                                                 IsError = ((dr["IsError"] != DBNull.Value ? Convert.ToBoolean(dr["IsError"]) : false)),
                                                                                 ErrorDescription = ((dr["ErrorDescription"] != DBNull.Value ? dr["ErrorDescription"].ToString() : "")),
                                                                             }).ToList();
                return BatchDebtAccountList;
            }
            catch (Exception ex)
            {
                return (new List<ADvwBatchDebtAccountFilterList>());
            }
        }

        #endregion
    }
}
