﻿using CRF.BLL;
using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using CRFView.Adapters.Collections.Reports.CollectionReports.ViewModels;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADCollectionReports
    {
        #region CRUD
 
        //Print Letters PDF
        public static string PrintCollectionReports(CollectionReportsVM objCollectionReportsVM)
        {
            string pdfPath = null;
            string sFromDate = null;
            string sThruDate = null;

            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var Sp_OBJ = new cview_Reports_GetCollectionReports();

                string sql = "Select * from vwCollectionReportsList Where ReportId = " + Convert.ToInt32(objCollectionReportsVM.ddlReportId);
                TableView mytableview = DBO.Provider.GetTableView(sql);
                DataSet myds = DBO.Provider.GetDataSet(sql);
                DataTable mydt = myds.Tables[0];
 
                if (objCollectionReportsVM.chkAllClients == false)
                {
                    Sp_OBJ.ClientId = objCollectionReportsVM.ddlClientId ;
                }
              
                if (objCollectionReportsVM.chkAllIndustryTypes == false)
                {
                    Sp_OBJ.ClientTypeId = objCollectionReportsVM.ddlIndustryTypeId;
                }

                if (objCollectionReportsVM.chkAllDebtorIds == false)
                {
                    Sp_OBJ.DebtAccountId = objCollectionReportsVM.DebtorId;
                }

                if (objCollectionReportsVM.chkAllStates == false)
                {
                    Sp_OBJ.State = objCollectionReportsVM.State;
                }

                if (objCollectionReportsVM.chkAllDesks == false)
                {
                    Sp_OBJ.DeskNum = objCollectionReportsVM.ddlDeskId;
                }

                if (objCollectionReportsVM.chkAllSman == false)
                {
                    Sp_OBJ.SmanNum = objCollectionReportsVM.ddlSmanId;
                }
 
                if (objCollectionReportsVM.chkByAssignDate == true)
                {
                    Sp_OBJ.AssignDateFROM = objCollectionReportsVM.AssignDateFrom.ToShortDateString();
                    Sp_OBJ.AssignDateTO = objCollectionReportsVM.AssignDateThru.ToShortDateString();
                    sFromDate = objCollectionReportsVM.AssignDateFrom.ToShortDateString();
                    sThruDate = objCollectionReportsVM.AssignDateThru.ToShortDateString();
                }
 
                if (objCollectionReportsVM.chkByTrxDate == true)
                {
                    Sp_OBJ.TrxDateFROM = objCollectionReportsVM.TrxDateFrom.ToShortDateString();
                    Sp_OBJ.TrxDateTO = objCollectionReportsVM.TrxnDateThru.ToShortDateString();
                    sFromDate = objCollectionReportsVM.TrxDateFrom.ToShortDateString();
                    sThruDate = objCollectionReportsVM.TrxnDateThru.ToShortDateString();
                }
 
                if (objCollectionReportsVM.chkAllStatusGroups == false)
                {
                    Sp_OBJ.StatusGroup = objCollectionReportsVM.ddlStatusGroupId;
                } 

                if (objCollectionReportsVM.chkAllStatus == false)
                {
                    //mysproc.CollectionStatus = GetCheckedStatusList()
                    //mysproc.CollectionStatus = Mid(mysproc.CollectionStatus, 1, Len(mysproc.CollectionStatus) - 1)
                    Sp_OBJ.CollectionStatus = objCollectionReportsVM.ddlStatusId;
                }

                Sp_OBJ.CustomSPName = Convert.ToString(mydt.Rows[0]["CustomSPName"]);
                Sp_OBJ.ReportName = Convert.ToString(mydt.Rows[0]["ReportName"]);

                TableView myview = DBO.Provider.GetTableView(Sp_OBJ);

                DataSet ds = DBO.Provider.GetDataSet(Sp_OBJ);

                DataTable mytbl = ds.Tables[0];

                CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;
                if (objCollectionReportsVM.RbtPrintOption == "Send To PDF")
                {
                    mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                }

                string myreportfilename = Convert.ToString(mydt.Rows[0]["ReportFileName"]);
                string myreportname = Convert.ToString(mydt.Rows[0]["ReportName"]);

                if (mytbl.Rows.Count > 0)
                {
                        pdfPath = Globals.RenderC1Report(user, mytbl, myreportfilename, myreportname, mymode, "", sFromDate, sThruDate);
                }

            }

            catch (Exception ex)
            {
                return "";
            }
            return pdfPath;
        }

        private static string ProduceCSV(DataView aview, string areportname)
        {
            CRF.BLL.Export.ExportColumns mycols = new CRF.BLL.Export.ExportColumns();
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            string sfilepath = CRF.BLL.CRFView.CRFView.GetFileName(ref user, "Management", areportname, ".CSV");
            string fileName = CRF.BLL.Export.Export.ExportToCSV(ref aview, ref sfilepath, ref mycols, true);
            if (!string.IsNullOrEmpty(fileName))
            {
                return fileName;
            }
            else
            {
                return "";
            }
        }
        #endregion
    }
}
