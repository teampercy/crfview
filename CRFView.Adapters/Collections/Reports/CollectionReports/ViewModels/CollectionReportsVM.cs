﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Collections.Reports.CollectionReports.ViewModels
{
    public class CollectionReportsVM
    {
        public CollectionReportsVM()
        {
            objADvwCollectionReportsList = new ADvwCollectionReportsList();
            ClientIdList = CommonBindList.GetClientIdList();
            IndustryTypeIdList = CommonBindList.GetClientTypeList();
            DeskIdList = CommonBindList.GetDeskList();
            SmanIdList = CommonBindList.GetSalesTeamList();
            StatusGroupIdList = CommonBindList.GetCollectionStatusGroup();
            StatusIdList = CommonBindList.GetStatusIdList();
        }

        public string RbtPrintOption { get; set; }
        public string RbtReportCategoryOption { get; set; }
        

        public string ddlReportId { get; set; } 

        public string ddlClientId { get; set; }
        public IEnumerable<SelectListItem> ClientIdList { get; set; }
        public bool chkAllClients { get; set; }

        public string ddlIndustryTypeId { get; set; }
        public IEnumerable<SelectListItem> IndustryTypeIdList { get; set; }
        public bool chkAllIndustryTypes { get; set; }

        public bool chkAllDebtorIds { get; set; }
        public string DebtorId { get; set; }

        public bool chkAllStates { get; set; }
        public string State { get; set; }
        
        public bool chkAllDesks { get; set; }
        public IEnumerable<SelectListItem> DeskIdList { get; set; }
        public string ddlDeskId { get; set; }
        
        public bool chkAllSman { get; set; }
        public IEnumerable<SelectListItem> SmanIdList { get; set; }
        public string ddlSmanId { get; set; }


        public bool chkByAssignDate { get; set; }
        public bool chkByTrxDate { get; set; }

        public DateTime AssignDateFrom { get; set; }
        public DateTime AssignDateThru { get; set; }

        public DateTime TrxDateFrom { get; set; }
        public DateTime TrxnDateThru { get; set; }

        public bool chkAllStatusGroups { get; set; }
        public IEnumerable<SelectListItem> StatusGroupIdList { get; set; }
        public string ddlStatusGroupId { get; set; }

        public bool chkAllStatus { get; set; }
        public IEnumerable<SelectListItem> StatusIdList { get; set; }
        public string ddlStatusId { get; set; }

        public ADvwCollectionReportsList objADvwCollectionReportsList { get; set; }


    }
}
