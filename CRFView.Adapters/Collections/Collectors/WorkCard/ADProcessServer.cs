﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard
{
    public class ADProcessServer
    {
        #region Properties
        public int PKID { get; set; }
        public string ProcessServerNo { get; set; }
        public string MarshalOffice { get; set; }
        public string MarshalAdd { get; set; }
        public string MarshalCity { get; set; }
        public string MarshalState { get; set; }
        public string MarshalZip { get; set; }
        public string MarshalPhoneNo { get; set; }
        #endregion

        #region CRUD Operations
        //List of ProcessServer
        public List<ADProcessServer> ListProcessServers()
        {
            ProcessServer myentity = new ProcessServer();
            TableView EntityList;
            try
            {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADProcessServer> CTList = (from DataRow dr in dt.Rows
                                                 select new ADProcessServer()
                                                 {
                                                     PKID = Convert.ToInt32(dr["PKID"]),
                                                     ProcessServerNo = Convert.ToString(dr["ProcessServerNo"]),
                                                     MarshalOffice = Convert.ToString(dr["MarshalOffice"])
                                                 }).ToList();

                return CTList;
            }
            catch (Exception ex)
            {
                return (new List<ADProcessServer>());
            }
        }

        //Read Details from database
        public static ADProcessServer ReadProcessServers(int id)
        {
            ProcessServer myentity = new ProcessServer();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (ProcessServer)myentityItable;
            AutoMapper.Mapper.CreateMap<ProcessServer, ADProcessServer>();
            return (AutoMapper.Mapper.Map<ADProcessServer>(myentity));
        }

        //Save(Create or Update) ProcessServer Details
        public void SaveProcessServers(ADProcessServer updatedProcessServers)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADProcessServer, ProcessServer>();
                ITable tblProcessServers;

                if (updatedProcessServers.PKID != 0)
                {
                    ADProcessServer loadProcessServers = ReadProcessServers(updatedProcessServers.PKID);
                    loadProcessServers.ProcessServerNo = updatedProcessServers.ProcessServerNo;
                    loadProcessServers.MarshalAdd = updatedProcessServers.MarshalAdd;
                    loadProcessServers.MarshalOffice = updatedProcessServers.MarshalOffice;
                    loadProcessServers.MarshalCity = updatedProcessServers.MarshalCity;
                    loadProcessServers.MarshalState = updatedProcessServers.MarshalState;
                    loadProcessServers.MarshalZip = updatedProcessServers.MarshalZip;
                    loadProcessServers.MarshalPhoneNo = updatedProcessServers.MarshalPhoneNo;
                    ProcessServer objProcessServers = AutoMapper.Mapper.Map<ProcessServer>(loadProcessServers);
                    tblProcessServers = objProcessServers;
                    DBO.Provider.Update(ref tblProcessServers);
                }
                else
                {
                    ProcessServer objProcessServers = AutoMapper.Mapper.Map<ProcessServer>(updatedProcessServers);
                    tblProcessServers = objProcessServers;
                    DBO.Provider.Create(ref tblProcessServers);

                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
    }
}
