﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard
{
    public class ADDebtAddress
    {
        #region Properties
        public decimal DebtAddressId { get; set; }
        public int DebtId { get; set; }
        public int DebtAddressTypeId { get; set; }
        public bool IsMainAddress { get; set; }
        public bool BusinessFlag { get; set; }
        public string AddressName { get; set; }
        public string ContactName { get; set; }
        public string ContactTitle { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
        public string PhoneNo { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string Note { get; set; }
        public bool MailReturnFlag { get; set; }
        public bool BadAddressFlag { get; set; }
        public bool BadPhoneFlag { get; set; }
        public string AddressKey { get; set; }
        public string PhoneNo2 { get; set; }
        public int id { get; set; }
        public string TypeCode { get; set; }
        public string PhoneNoExt { get; set; }
        public string PhoneNo2Ext { get; set; }
        public string CellPhone { get; set; }
        public bool NoMailFlag { get; set; }
        public string SSN { get; set; }
        #endregion

        #region CRUD
        public static List<ADDebtAddress> GetAddressList(DataTable dt)
        {
            try
            {
                List<ADDebtAddress> AddressList = (from DataRow dr in dt.Rows
                                                                   select new ADDebtAddress()
                                                                   {
                                                                       DebtAddressId = ((dr["DebtAddressId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAddressId"].ToString()) : 0)),
                                                                       DebtId = ((dr["DebtId"] != DBNull.Value ? Convert.ToInt32(dr["DebtId"].ToString()) : 0)),
                                                                       DebtAddressTypeId = ((dr["DebtAddressTypeId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAddressTypeId"].ToString()) : 0)),
                                                                       IsMainAddress = ((dr["IsMainAddress"] != DBNull.Value ? Convert.ToBoolean(dr["IsMainAddress"]) : false)),
                                                                       BusinessFlag = ((dr["BusinessFlag"] != DBNull.Value ? Convert.ToBoolean(dr["BusinessFlag"]) : false)),
                                                                       AddressName = ((dr["AddressName"] != DBNull.Value ? dr["AddressName"].ToString() : "")),
                                                                       ContactName = ((dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : "")),
                                                                       ContactTitle = ((dr["ContactTitle"] != DBNull.Value ? dr["ContactTitle"].ToString() : "")),
                                                                       AddressLine1 = ((dr["AddressLine1"] != DBNull.Value ? dr["AddressLine1"].ToString() : "")),
                                                                       AddressLine2 = ((dr["AddressLine2"] != DBNull.Value ? dr["AddressLine2"].ToString() : "")),
                                                                       City = ((dr["City"] != DBNull.Value ? dr["City"].ToString() : "")),
                                                                       State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),
                                                                       PostalCode = ((dr["PostalCode"] != DBNull.Value ? dr["PostalCode"].ToString() : "")),
                                                                       County = ((dr["County"] != DBNull.Value ? dr["County"].ToString() : "")),
                                                                       Country = ((dr["Country"] != DBNull.Value ? dr["Country"].ToString() : "")),
                                                                       PhoneNo = ((dr["PhoneNo"] != DBNull.Value ? dr["PhoneNo"].ToString() : "")),
                                                                       Fax = ((dr["Fax"] != DBNull.Value ? dr["Fax"].ToString() : "")),
                                                                       Email = ((dr["Email"] != DBNull.Value ? dr["Email"].ToString() : "")),
                                                                       Note = ((dr["Note"] != DBNull.Value ? dr["Note"].ToString() : "")),
                                                                       MailReturnFlag = ((dr["MailReturnFlag"] != DBNull.Value ? Convert.ToBoolean(dr["MailReturnFlag"]) : false)),
                                                                       BadAddressFlag = ((dr["BadAddressFlag"] != DBNull.Value ? Convert.ToBoolean(dr["BadAddressFlag"]) : false)),
                                                                       BadPhoneFlag = ((dr["BadPhoneFlag"] != DBNull.Value ? Convert.ToBoolean(dr["BadPhoneFlag"]) : false)),
                                                                       AddressKey = ((dr["AddressKey"] != DBNull.Value ? dr["AddressKey"].ToString() : "")),
                                                                       PhoneNo2 = ((dr["PhoneNo2"] != DBNull.Value ? dr["PhoneNo2"].ToString() : "")),
                                                                       id = ((dr["id"] != DBNull.Value ? Convert.ToInt32(dr["id"].ToString()) : 0)),
                                                                       TypeCode = ((dr["TypeCode"] != DBNull.Value ? dr["TypeCode"].ToString() : "")),
                                                                       PhoneNoExt = ((dr["PhoneNoExt"] != DBNull.Value ? dr["PhoneNoExt"].ToString() : "")),
                                                                       PhoneNo2Ext = ((dr["PhoneNo2Ext"] != DBNull.Value ? dr["PhoneNo2Ext"].ToString() : "")),
                                                                       CellPhone = ((dr["CellPhone"] != DBNull.Value ? dr["CellPhone"].ToString() : "")),
                                                                       NoMailFlag = ((dr["NoMailFlag"] != DBNull.Value ? Convert.ToBoolean(dr["NoMailFlag"]) : false)),
                                                                       SSN = ((dr["SSN"] != DBNull.Value ? dr["SSN"].ToString() : "")),
                                                                   }).ToList();
                return AddressList;
            }
            catch (Exception ex)
            {
                return (new List<ADDebtAddress>());
            }
        }

        public static ADDebtAddress ReadAddress(int DebtAddressId)
        {
            //mysql = "Select Max(DebtAddressTypeId) as LastSeq from DebtAddress Where DebtId = " & aaccountid
            //myview = BLL.Providers.DBO.Provider.GetTableView(mysql)
            //Dim iSeq As Integer = myview.RowItem("LastSeq")
            //Me.DebtAddressTypeId.Value = iSeq + 1

            DebtAddress myentity = new DebtAddress();
            ITable myentityItable = myentity;
            DBO.Provider.Read(DebtAddressId.ToString(), ref myentityItable);
            myentity = (DebtAddress)myentityItable;
            AutoMapper.Mapper.CreateMap<DebtAddress, ADDebtAddress>();
            return (AutoMapper.Mapper.Map<ADDebtAddress>(myentity));
        }

        public static ADDebtAddress ReadAddressByDebtAccountId(string DebtAccountId)
        {
            try
            {
                string mysql = "select DebtAddress.* from DebtAddress join Debt on debt.debtid = debtAddress.debtid Join DebtAccount on debt.debtid = debtaccount.debtid where debtaddress.IsMainAddress = 1 and debtaccountid= '" + DebtAccountId + "'";
                //string mysql = "select DebtAddress.* from DebtAddress join Debt on debt.debtid = debtAddress.debtid Join DebtAccount on debt.debtid = debtaccount.debtid where debtaccountid= '" + DebtAccountId + "' order by DebtAddressTypeId desc";
                var AddressTableView = DBO.Provider.GetTableView(mysql);
                DataTable dt = AddressTableView.ToTable();
                return ConvertDataTableToClass(dt);
            }
            catch(Exception ex)
            {
                return new ADDebtAddress();
            }
        }

        public static ADDebtAddress UpdateAddressByDebtAccountId()
        {
            try
            {
                DebtAddress myentity = new DebtAddress();
                ITable myentityItable = myentity;
                DBO.Provider.Update(ref myentityItable);
                myentity = (DebtAddress)myentityItable;
                AutoMapper.Mapper.CreateMap<DebtAddress, ADDebtAddress>();
                return (AutoMapper.Mapper.Map<ADDebtAddress>(myentity));
            }
            catch (Exception ex)
            {
                return new ADDebtAddress();
            }
        }


        public static bool UpdateAddressEditView(ADDebtAddress ObjUpdatedADAddress)
        {
            try
            {
                ADDebtAddress ObjOriginalDebtAddress = ReadAddress(Convert.ToInt32(ObjUpdatedADAddress.DebtAddressId));

                if (ObjUpdatedADAddress.ContactName == "" || ObjUpdatedADAddress.ContactName == null)
                {
                }
                else
                {
                    ObjOriginalDebtAddress.ContactName = ObjUpdatedADAddress.ContactName.Trim();
                }

                if (ObjUpdatedADAddress.AddressLine1 == "" || ObjUpdatedADAddress.AddressLine1 == null)
                {
                }
                else
                {
                    ObjOriginalDebtAddress.AddressLine1 = ObjUpdatedADAddress.AddressLine1.Trim();
                }

                if (ObjUpdatedADAddress.AddressLine2 == "" || ObjUpdatedADAddress.AddressLine2 == null)
                {
                }
                else
                {
                    ObjOriginalDebtAddress.AddressLine2 = ObjUpdatedADAddress.AddressLine2.Trim();
                }

                if (ObjUpdatedADAddress.City == "" || ObjUpdatedADAddress.City == null)
                {
                }
                else
                {
                    ObjOriginalDebtAddress.City = ObjUpdatedADAddress.City.Trim();
                }

                if (ObjUpdatedADAddress.State == "" || ObjUpdatedADAddress.State == null)
                {
                }
                else
                {
                    ObjOriginalDebtAddress.State = ObjUpdatedADAddress.State.Trim();
                }

                if (ObjUpdatedADAddress.PostalCode == "" || ObjUpdatedADAddress.PostalCode == null)
                {
                }
                else
                {
                    ObjOriginalDebtAddress.PostalCode = ObjUpdatedADAddress.PostalCode.Trim();
                }

                if (ObjUpdatedADAddress.PhoneNo == "" || ObjUpdatedADAddress.PhoneNo == null)
                {
                }
                else
                {
                    ObjOriginalDebtAddress.PhoneNo = ObjUpdatedADAddress.PhoneNo.Trim();
                }

                if(ObjUpdatedADAddress.PhoneNoExt == "" || ObjUpdatedADAddress.PhoneNoExt == null)
                {
                }
                else
                {
                    ObjOriginalDebtAddress.PhoneNoExt = ObjUpdatedADAddress.PhoneNoExt.Trim();
                }

                if(ObjUpdatedADAddress.PhoneNo2 =="" || ObjUpdatedADAddress.PhoneNo2 == null)
                {
                }
                else
                {
                    ObjOriginalDebtAddress.PhoneNo2 = ObjUpdatedADAddress.PhoneNo2.Trim();
                }

                if(ObjUpdatedADAddress.Fax == "" || ObjUpdatedADAddress.Fax == null)
                {
                }
                else
                {
                    ObjOriginalDebtAddress.Fax = ObjUpdatedADAddress.Fax.Trim();
                }

                if(ObjUpdatedADAddress.CellPhone == "" || ObjUpdatedADAddress.CellPhone == null)
                {
                }
                else
                {
                    ObjOriginalDebtAddress.CellPhone = ObjUpdatedADAddress.CellPhone.Trim();
                }
        
                if(ObjUpdatedADAddress.Email == "" || ObjUpdatedADAddress.Email == null)
                {
                }            
                else
                {
                    ObjOriginalDebtAddress.Email = ObjUpdatedADAddress.Email.Trim();
                }

                AutoMapper.Mapper.CreateMap<ADDebtAddress, DebtAddress>();
                DebtAddress myentity = new DebtAddress();
                myentity = AutoMapper.Mapper.Map<DebtAddress>(ObjOriginalDebtAddress);
                ITable tblI;
                tblI = (ITable)myentity;
                DBO.Provider.Update(ref tblI);
                return true;
  
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public static int SaveDebtAccountAddress(ADDebtAddress objUpdatedADDebtAddress)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADDebtAddress, DebtAddress>();
                ITable tblDebtAccountAddress;
                DebtAddress objDebtAddress;

                if (objUpdatedADDebtAddress.DebtAddressId != 0)
                {
                    ADDebtAddress objADDebtAddressOriginal = ReadAddress(Convert.ToInt32(objUpdatedADDebtAddress.DebtAddressId));

                    if (objUpdatedADDebtAddress.ContactName == "" || objUpdatedADDebtAddress.ContactName == null)
                    {
                    }
                    else
                    {
                        objADDebtAddressOriginal.ContactName = objUpdatedADDebtAddress.ContactName.Trim();
                    }

                    if (objUpdatedADDebtAddress.AddressLine1 == "" || objUpdatedADDebtAddress.AddressLine1 == null)
                    {
                    }
                    else
                    {
                        objADDebtAddressOriginal.AddressLine1 = objUpdatedADDebtAddress.AddressLine1.Trim();
                    }

                    if (objUpdatedADDebtAddress.AddressLine2 == "" || objUpdatedADDebtAddress.AddressLine2 == null)
                    {
                    }
                    else
                    {
                        objADDebtAddressOriginal.AddressLine2 = objUpdatedADDebtAddress.AddressLine2.Trim();
                    }

                    if (objUpdatedADDebtAddress.City == "" || objUpdatedADDebtAddress.City == null)
                    {
                    }
                    else
                    {
                        objADDebtAddressOriginal.City = objUpdatedADDebtAddress.City.Trim();
                    }

                    if (objUpdatedADDebtAddress.State == "" || objUpdatedADDebtAddress.State == null)
                    {
                    }
                    else
                    {
                        objADDebtAddressOriginal.State = objUpdatedADDebtAddress.State.Trim();
                    }

                    if (objUpdatedADDebtAddress.PostalCode == "" || objUpdatedADDebtAddress.PostalCode == null)
                    {
                    }
                    else
                    {
                        objADDebtAddressOriginal.PostalCode = objUpdatedADDebtAddress.PostalCode.Trim();
                    }

                    if (objUpdatedADDebtAddress.PhoneNo == "" || objUpdatedADDebtAddress.PhoneNo == null)
                    {
                    }
                    else
                    {
                        objADDebtAddressOriginal.PhoneNo = objUpdatedADDebtAddress.PhoneNo.Trim();
                    }

                    if (objUpdatedADDebtAddress.PhoneNoExt == "" || objUpdatedADDebtAddress.PhoneNoExt == null)
                    {
                    }
                    else
                    {
                        objADDebtAddressOriginal.PhoneNoExt = objUpdatedADDebtAddress.PhoneNoExt.Trim();
                    }

                    if (objUpdatedADDebtAddress.PhoneNo2 == "" || objUpdatedADDebtAddress.PhoneNo2 == null)
                    {
                    }
                    else
                    {
                        objADDebtAddressOriginal.PhoneNo2 = objUpdatedADDebtAddress.PhoneNo2.Trim();
                    }

                    if (objUpdatedADDebtAddress.Fax == "" || objUpdatedADDebtAddress.Fax == null)
                    {
                    }
                    else
                    {
                        objADDebtAddressOriginal.Fax = objUpdatedADDebtAddress.Fax.Trim();
                    }

                    if (objUpdatedADDebtAddress.CellPhone == "" || objUpdatedADDebtAddress.CellPhone == null)
                    {
                    }
                    else
                    {
                        objADDebtAddressOriginal.CellPhone = objUpdatedADDebtAddress.CellPhone.Trim();
                    }

                    if (objUpdatedADDebtAddress.Email == "" || objUpdatedADDebtAddress.Email == null)
                    {
                    }
                    else
                    {
                        objADDebtAddressOriginal.Email = objUpdatedADDebtAddress.Email.Trim();
                    }

                    if (objUpdatedADDebtAddress.SSN == "" || objUpdatedADDebtAddress.SSN == null)
                    {
                    }
                    else
                    {
                        objADDebtAddressOriginal.SSN = objUpdatedADDebtAddress.SSN.Trim();
                    }

                    if (objUpdatedADDebtAddress.Note == "" || objUpdatedADDebtAddress.Note == null)
                    {
                    }
                    else
                    {
                        objADDebtAddressOriginal.Note = objUpdatedADDebtAddress.Note.Trim();
                    }

                    objDebtAddress = AutoMapper.Mapper.Map<DebtAddress>(objADDebtAddressOriginal);
                    tblDebtAccountAddress = (ITable)objDebtAddress;
                    DBO.Provider.Update(ref tblDebtAccountAddress);
                }
                else
                {
                    objDebtAddress = AutoMapper.Mapper.Map<DebtAddress>(objUpdatedADDebtAddress);
                    tblDebtAccountAddress = (ITable)objDebtAddress;
                    DBO.Provider.Create(ref tblDebtAccountAddress);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static DataTable GetDebtAccountAddressDtList(string DebtId)
        {
            try
            {
                string mysql = "select * from DebtAddress where debtid= "  + DebtId;

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(mysql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }


        #endregion

        #region Local Methods
        public static ADDebtAddress ConvertDataTableToClass(DataTable dt)
        {
            List<ADDebtAddress> AddressList = (from DataRow dr in dt.Rows
                                               select new ADDebtAddress()
                                               {
                                                   DebtAddressId = ((dr["DebtAddressId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAddressId"].ToString()) : 0)),
                                                   DebtId = ((dr["DebtId"] != DBNull.Value ? Convert.ToInt32(dr["DebtId"].ToString()) : 0)),
                                                   DebtAddressTypeId = ((dr["DebtAddressTypeId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAddressTypeId"].ToString()) : 0)),
                                                   IsMainAddress = ((dr["IsMainAddress"] != DBNull.Value ? Convert.ToBoolean(dr["IsMainAddress"]) : false)),
                                                   BusinessFlag = ((dr["BusinessFlag"] != DBNull.Value ? Convert.ToBoolean(dr["BusinessFlag"]) : false)),
                                                   AddressName = ((dr["AddressName"] != DBNull.Value ? dr["AddressName"].ToString() : "")),
                                                   ContactName = ((dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : "")),
                                                   ContactTitle = ((dr["ContactTitle"] != DBNull.Value ? dr["ContactTitle"].ToString() : "")),
                                                   AddressLine1 = ((dr["AddressLine1"] != DBNull.Value ? dr["AddressLine1"].ToString() : "")),
                                                   AddressLine2 = ((dr["AddressLine2"] != DBNull.Value ? dr["AddressLine2"].ToString() : "")),
                                                   City = ((dr["City"] != DBNull.Value ? dr["City"].ToString() : "")),
                                                   State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),
                                                   PostalCode = ((dr["PostalCode"] != DBNull.Value ? dr["PostalCode"].ToString() : "")),
                                                   County = ((dr["County"] != DBNull.Value ? dr["County"].ToString() : "")),
                                                   Country = ((dr["Country"] != DBNull.Value ? dr["Country"].ToString() : "")),
                                                   PhoneNo = ((dr["PhoneNo"] != DBNull.Value ? dr["PhoneNo"].ToString() : "")),
                                                   Fax = ((dr["Fax"] != DBNull.Value ? dr["Fax"].ToString() : "")),
                                                   Email = ((dr["Email"] != DBNull.Value ? dr["Email"].ToString() : "")),
                                                   Note = ((dr["Note"] != DBNull.Value ? dr["Note"].ToString() : "")),
                                                   MailReturnFlag = ((dr["MailReturnFlag"] != DBNull.Value ? Convert.ToBoolean(dr["MailReturnFlag"]) : false)),
                                                   BadAddressFlag = ((dr["BadAddressFlag"] != DBNull.Value ? Convert.ToBoolean(dr["BadAddressFlag"]) : false)),
                                                   BadPhoneFlag = ((dr["BadPhoneFlag"] != DBNull.Value ? Convert.ToBoolean(dr["BadPhoneFlag"]) : false)),
                                                   AddressKey = ((dr["AddressKey"] != DBNull.Value ? dr["AddressKey"].ToString() : "")),
                                                   PhoneNo2 = ((dr["PhoneNo2"] != DBNull.Value ? dr["PhoneNo2"].ToString() : "")),
                                                   id = ((dr["id"] != DBNull.Value ? Convert.ToInt32(dr["id"].ToString()) : 0)),
                                                   TypeCode = ((dr["TypeCode"] != DBNull.Value ? dr["TypeCode"].ToString() : "")),
                                                   PhoneNoExt = ((dr["PhoneNoExt"] != DBNull.Value ? dr["PhoneNoExt"].ToString() : "")),
                                                   PhoneNo2Ext = ((dr["PhoneNo2Ext"] != DBNull.Value ? dr["PhoneNo2Ext"].ToString() : "")),
                                                   CellPhone = ((dr["CellPhone"] != DBNull.Value ? dr["CellPhone"].ToString() : "")),
                                                   NoMailFlag = ((dr["NoMailFlag"] != DBNull.Value ? Convert.ToBoolean(dr["NoMailFlag"]) : false)),
                                                   SSN = ((dr["SSN"] != DBNull.Value ? dr["SSN"].ToString() : "")),
                                               }).ToList();
            return AddressList.First();
        }
        #endregion
    }
}
