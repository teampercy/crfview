﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using HDS.DAL.Providers;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Collections.Collectors.WorkCard.ViewModels;
using CRFView.Adapters.Clients.Client_Management;

namespace CRFView.Adapters.Collections.Collectors.WorkCard
{
    public class AccountInfo
    {
        #region Properties()
        #endregion

        #region CRUD
        public static DebtAccountVM LoadInfo(string DebtAccountId)
        {
            var myacc = new DebtAccount();
            ITable IAcc = myacc;
            DBO.Provider.Read(DebtAccountId, ref IAcc);
            myacc = (DebtAccount)IAcc;
            var myds = new DataSet();
            var mysproc = new cview_Accounts_GetAccountInfo();
            mysproc.AccountId = DebtAccountId;
            myds = DBO.Provider.GetDataSet(mysproc);
            DataTable dt = myds.Tables[0];
            DebtAccountVM Obj_DebtAccount = new DebtAccountVM();
            if (dt.Rows.Count < 1)
            {
                return null;
            }

            else
            {
                Obj_DebtAccount.obj_DebtAccount = GetDebtAccontInfo(dt);
            }

            string CRateId = null;
            if (Obj_DebtAccount.obj_DebtAccount.CommRateId > 0)
            {
                CRateId = Obj_DebtAccount.obj_DebtAccount.CommRateId.ToString();
            }
            else
            {
                CRateId = Obj_DebtAccount.obj_DebtAccount.CollectionRateId.ToString();
            }
            Obj_DebtAccount.obj_CollRate = ADCollectionRate.ReadCollectionRate(CRateId);

            Obj_DebtAccount.dtCollectionInfo = myds.Tables[12];
            Obj_DebtAccount.obj_CollectionInfo = ADClientCollectionInfo.GetClientCollectionInfo(Obj_DebtAccount.dtCollectionInfo);
            //Obj_DebtAccount.dtCollectionInfo = dtCollectionInfo;

            DataTable dtMisc = myds.Tables[10];
            Obj_DebtAccount.objMisc = ADDebtAccountMisc.GetMiscInfo(dtMisc);

            DataTable dtNotes = myds.Tables[2];
            DataView dvNotes = new DataView(dtNotes);
            dvNotes.Sort = "Id DESC";
            DataTable dtNotesDesc = dvNotes.ToTable();
            Obj_DebtAccount.ObjNotes = ADvwCollectionNotes.GetNotes(dtNotesDesc);

            DataTable dtActionHistory = myds.Tables[9];
            Obj_DebtAccount.ObjActionHistory = ADvwCollectionActionHistory.GetActionHistoryList(dtActionHistory);

            DataTable dtLedger = myds.Tables[1];
            Obj_DebtAccount.ObjLedger = ADLedgerGrid.GetLedgerList(dtLedger);


            DataTable dtAddress = myds.Tables[6];
            Obj_DebtAccount.ObjAddress = ADDebtAddress.GetAddressList(dtAddress);

            if(Obj_DebtAccount.obj_DebtAccount.IsNoCBR==true)
            {
                Obj_DebtAccount.ObjCreditReport = new List<ADDebtAccountCreditReport>();
            }
            else
            {
                string mysql = "Select * from DebtAccountCreditReport where DebtAccountId ="+ DebtAccountId;
                var CBRView = DBO.Provider.GetTableView(mysql);
                DataTable dtCBR = CBRView.ToTable();
                Obj_DebtAccount.ObjCreditReport = ADDebtAccountCreditReport.GetDebtAccontInfo(dtCBR);
            }

            DataTable dtDocuments = myds.Tables[3];
            DataView dvDocuments = new DataView(dtDocuments);
            dvDocuments.Sort = "DateCreated DESC";
            DataTable dtDocumentsDesc = dvDocuments.ToTable();
            Obj_DebtAccount.ObjDocuments = ADDebtAccountAttachment.GetDebtAttachments(dtDocumentsDesc);

            Obj_DebtAccount.LocationList = CommonBindList.GetLocationDebt();

            Obj_DebtAccount.obj_AccountLedgerSales = ADvwAccountLedgerSales.ReadvwAccountLedgerSales(DebtAccountId);

            DataTable dtLetters = myds.Tables[7];
            Obj_DebtAccount.ObjLetters = ADvwDebtAccountLetters.GetDebtAccountLettersList(dtLetters);

            DataTable dtLegalInfo = myds.Tables[8];
            Obj_DebtAccount.objLegalInfo = ADDebtAccountLegalInfo.GetDebtAccountLegalInfo(dtLegalInfo);

            Obj_DebtAccount.objAttorneyInfo = ADAttorneyInfo.ReadAttorneyInfo(Obj_DebtAccount.objLegalInfo.AttyId);

            Obj_DebtAccount.objLawListInfo = ADLawListInfo.ReadLawLists(Obj_DebtAccount.objLegalInfo.LawListId);


            DataTable dtDebtAccountSmallClaims = myds.Tables[11];
            Obj_DebtAccount.objDebtAccountSmallClaims = ADDebtAccountSmallClaims.GetDebtAccountSmallClaims(dtDebtAccountSmallClaims);

            Obj_DebtAccount.objCourtHouse = ADCourtHouse.ReadCourtHouse(Obj_DebtAccount.objDebtAccountSmallClaims.CourtId);

            Obj_DebtAccount.objProcessServer = ADProcessServer.ReadProcessServers(Obj_DebtAccount.objDebtAccountSmallClaims.ProcessServerId);

            return Obj_DebtAccount;
        }

        public static bool LogAccountChanges(EditDebtAccountVM objUpdateDebtAccount, CurrentUser CU_Obj)
        {
            try
            {
                ADDebt objAdDebt = ADDebt.ReadDebt(objUpdateDebtAccount.obj_DebtEdit.DebtId);
                ADDebtAddress objAddress = ADDebtAddress.ReadAddress(Convert.ToInt32(objUpdateDebtAccount.obj_DebtAddressEdit.DebtAddressId));
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var mysproc = new CRF.BLL.CRFDB.SPROCS.cview_Accounts_UpdateChangeLog();
                mysproc.DebtAccountId = objUpdateDebtAccount.obj_DebtAccountEdit.DebtAccountId;
                mysproc.UserId = user.Id;
                mysproc.UserName = user.UserName;
                mysproc.ChangeType = 0;

                mysproc.ChangeFrom = objAdDebt.DebtorName.Trim() + "~";
                mysproc.ChangeFrom = objAddress.ContactName.Trim() + "~";
                mysproc.ChangeFrom = objAddress.AddressLine1.Trim() + "~";
                mysproc.ChangeFrom = objAddress.City.Trim() + "~";
                mysproc.ChangeFrom = objAddress.State.Trim() + "~";
                mysproc.ChangeFrom = objAddress.PostalCode.Trim() + "~";
                mysproc.ChangeFrom = objAddress.PhoneNo.Trim();

                mysproc.ChangeTo = objUpdateDebtAccount.obj_DebtEdit.DebtorName.Trim() + "~";
                if (objUpdateDebtAccount.obj_DebtAddressEdit.ContactName == null || objUpdateDebtAccount.obj_DebtAddressEdit.ContactName == "")
                {
                    objUpdateDebtAccount.obj_DebtAddressEdit.ContactName = "";
                }
                mysproc.ChangeTo = objUpdateDebtAccount.obj_DebtAddressEdit.ContactName.Trim() + "~";
          
                if (objUpdateDebtAccount.obj_DebtAddressEdit.AddressLine1 == null || objUpdateDebtAccount.obj_DebtAddressEdit.AddressLine1 == "")
                {
                    objUpdateDebtAccount.obj_DebtAddressEdit.AddressLine1 = "";
                }
                mysproc.ChangeTo = objUpdateDebtAccount.obj_DebtAddressEdit.AddressLine1.Trim() + "~";

                if (objUpdateDebtAccount.obj_DebtAddressEdit.City == null || objUpdateDebtAccount.obj_DebtAddressEdit.City == "")
                {
                    objUpdateDebtAccount.obj_DebtAddressEdit.City = "";
                }
                mysproc.ChangeTo = objUpdateDebtAccount.obj_DebtAddressEdit.City.Trim() + "~";

                if (objUpdateDebtAccount.obj_DebtAddressEdit.State == null || objUpdateDebtAccount.obj_DebtAddressEdit.State == "")
                {
                    objUpdateDebtAccount.obj_DebtAddressEdit.State = "";
                }
                mysproc.ChangeTo = objUpdateDebtAccount.obj_DebtAddressEdit.State.Trim() + "~";

                if (objUpdateDebtAccount.obj_DebtAddressEdit.PostalCode == null || objUpdateDebtAccount.obj_DebtAddressEdit.PostalCode == "")
                {
                    objUpdateDebtAccount.obj_DebtAddressEdit.PostalCode = "";
                }
                mysproc.ChangeTo = objUpdateDebtAccount.obj_DebtAddressEdit.PostalCode.Trim() + "~";

                if (objUpdateDebtAccount.obj_DebtAddressEdit.PhoneNo == null || objUpdateDebtAccount.obj_DebtAddressEdit.PhoneNo == "")
                {
                    objUpdateDebtAccount.obj_DebtAddressEdit.PhoneNo = "";
                }
                mysproc.ChangeTo = objUpdateDebtAccount.obj_DebtAddressEdit.PhoneNo.Trim();

                if( mysproc.ChangeFrom != mysproc.ChangeTo)
                {
                    DBO.Provider.ExecNonQuery(mysproc);
                }

                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

      
        #endregion

        #region Local Methods
        public static ADvwAccountList GetDebtAccontInfo(DataTable dt)
        {
            List<ADvwAccountList> Account = (from DataRow dr in dt.Rows
                                                       select new ADvwAccountList()
                                                       {
                                                           DebtorName = ((dr["DebtorName"] != DBNull.Value ? dr["DebtorName"].ToString() : "")),
                                                           TaxId = ((dr["TaxId"] != DBNull.Value ? dr["TaxId"].ToString() : "")),
                                                           DriversLicense = ((dr["DriversLicense"] != DBNull.Value ? dr["DriversLicense"].ToString() : "")),
                                                           IsUnwillingToPay = ((dr["IsUnwillingToPay"] != DBNull.Value ? Convert.ToBoolean(dr["IsUnwillingToPay"]) : false)),
                                                           IsUnableToPay = ((dr["IsUnableToPay"] != DBNull.Value ? Convert.ToBoolean(dr["IsUnableToPay"]) : false)),
                                                           BirthDate = ((dr["BirthDate"] != DBNull.Value ? Convert.ToDateTime(dr["BirthDate"]) : DateTime.MinValue)),
                                                           HasReturnedChecks = ((dr["HasReturnedChecks"] != DBNull.Value ? Convert.ToBoolean(dr["HasReturnedChecks"]) : false)),
                                                           ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                                           ServiceDescr = ((dr["ServiceDescr"] != DBNull.Value ? dr["ServiceDescr"].ToString() : "")),
                                                           ClientName = ((dr["ClientName"] != DBNull.Value ? dr["ClientName"].ToString() : "")),
                                                           IsIndAck  = ((dr["IsIndAck"] != DBNull.Value ? Convert.ToBoolean(dr["IsIndAck"]) : false)),
                                                           IsRegAck = ((dr["IsRegAck"] != DBNull.Value ? Convert.ToBoolean(dr["IsRegAck"]) : false)),
                                                           IsCloseRpt = ((dr["IsCloseRpt"] != DBNull.Value ? Convert.ToBoolean(dr["IsCloseRpt"]) : false)),
                                                           IsMasterRpt = ((dr["IsMasterRpt"] != DBNull.Value ? Convert.ToBoolean(dr["IsMasterRpt"]) : false)),
                                                           IsPmtRpt = ((dr["IsPmtRpt"] != DBNull.Value ? Convert.ToBoolean(dr["IsPmtRpt"]) : false)),
                                                           ClientAddressLine1=((dr["ClientAddressLine1"] != DBNull.Value ? dr["ClientAddressLine1"].ToString() : "")),
                                                           ClientAddressLine2 = ((dr["ClientAddressLine2"] != DBNull.Value ? dr["ClientAddressLine2"].ToString() : "")),
                                                           ClientCity = ((dr["ClientCity"] != DBNull.Value ? dr["ClientCity"].ToString() : "")),
                                                           ClientState = ((dr["ClientState"] != DBNull.Value ? dr["ClientState"].ToString() : "")),
                                                           ClientPostalCode = ((dr["ClientPostalCode"] != DBNull.Value ? dr["ClientPostalCode"].ToString() : "")),
                                                           IsTrustAccount=((dr["IsTrustAccount"] != DBNull.Value ? Convert.ToBoolean(dr["IsTrustAccount"]) : false)),
                                                           IsRolloverList = ((dr["IsRolloverList"] != DBNull.Value ? Convert.ToBoolean(dr["IsRolloverList"]) : false)),
                                                           IsRollover = ((dr["IsRollover"] != DBNull.Value ? Convert.ToBoolean(dr["IsRollover"]) : false)),
                                                           ServiceCode = ((dr["ServiceCode"] != DBNull.Value ? dr["ServiceCode"].ToString() : "")),
                                                           DebtAccountId = ((dr["DebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAccountId"]) : 0)),
                                                           ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
                                                           ContractId = ((dr["ContractId"] != DBNull.Value ? Convert.ToInt32(dr["ContractId"]) : 0)),
                                                           DebtId = ((dr["DebtId"] != DBNull.Value ? Convert.ToInt32(dr["DebtId"]) : 0)),
                                                           FileNumber = ((dr["FileNumber"] != DBNull.Value ? dr["FileNumber"].ToString() : "")),
                                                           LocationId = ((dr["LocationId"] != DBNull.Value ? Convert.ToInt32(dr["LocationId"]) : 0)),
                                                           CollectionStatusId = ((dr["CollectionStatusId"] != DBNull.Value ? Convert.ToInt32(dr["CollectionStatusId"]) : 0)),
                                                           TempOwnerId = ((dr["TempOwnerId"] != DBNull.Value ? Convert.ToInt32(dr["TempOwnerId"]) : 0)),
                                                           TempOwner = ((dr["TempOwner"] != DBNull.Value ? dr["TempOwner"].ToString() : "")),
                                                           BranchNum = ((dr["BranchNum"] != DBNull.Value ? dr["BranchNum"].ToString() : "")),
                                                           ClientAgentCode = ((dr["ClientAgentCode"] != DBNull.Value ? dr["ClientAgentCode"].ToString() : "")),
                                                           Priority = ((dr["Priority"] != DBNull.Value ? dr["Priority"].ToString() : "")),
                                                           AccountNo = ((dr["AccountNo"] != DBNull.Value ? dr["AccountNo"].ToString() : "")),
                                                           CloseCode = ((dr["CloseCode"] != DBNull.Value ? dr["CloseCode"].ToString() : "")),
                                                           ClaimNum = ((dr["ClaimNum"] != DBNull.Value ? dr["ClaimNum"].ToString() : "")),
                                                           DebtAccountNote = ((dr["DebtAccountNote"] != DBNull.Value ? dr["DebtAccountNote"].ToString() : "")),
                                                           Age = ((dr["Age"] != DBNull.Value ? Convert.ToInt32(dr["Age"]) : 0)),
                                                           ReferalDate = ((dr["ReferalDate"] != DBNull.Value ? Convert.ToDateTime(dr["ReferalDate"]) : DateTime.MinValue)),
                                                           LastServiceDate = ((dr["LastServiceDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastServiceDate"]) : DateTime.MinValue)),
                                                           LastPaymentDate = ((dr["LastPaymentDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastPaymentDate"]) : DateTime.MinValue)),
                                                           LastUpdateDate = ((dr["LastUpdateDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastUpdateDate"]) : DateTime.MinValue)),
                                                           LastTrustDate = ((dr["LastTrustDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastTrustDate"]) : DateTime.MinValue)),
                                                           NextContactDate = ((dr["NextContactDate"] != DBNull.Value ? Convert.ToDateTime(dr["NextContactDate"]) : DateTime.MinValue)),
                                                           NextContactNo = ((dr["NextContactNo"] != DBNull.Value ? Convert.ToInt32(dr["NextContactNo"]) : 0)),
                                                           NextLetterDate = ((dr["NextLetterDate"] != DBNull.Value ? Convert.ToDateTime(dr["NextLetterDate"]) : DateTime.MinValue)),
                                                           NextLetterSeqNo = ((dr["NextLetterSeqNo"] != DBNull.Value ? Convert.ToInt32(dr["NextLetterSeqNo"]) : 0)),
                                                           NextStatusDate = ((dr["NextStatusDate"] != DBNull.Value ? Convert.ToDateTime(dr["NextStatusDate"]) : DateTime.MinValue)),
                                                           NextLegalDate = ((dr["NextLegalDate"] != DBNull.Value ? Convert.ToDateTime(dr["NextLegalDate"]) : DateTime.MinValue)),
                                                           InterestFromDate = ((dr["InterestFromDate"] != DBNull.Value ? Convert.ToDateTime(dr["InterestFromDate"]) : DateTime.MinValue)),
                                                           InterestThruDate = ((dr["InterestThruDate"] != DBNull.Value ? Convert.ToDateTime(dr["InterestThruDate"]) : DateTime.MinValue)),
                                                           CloseDate = ((dr["CloseDate"] != DBNull.Value ? Convert.ToDateTime(dr["CloseDate"]) : DateTime.MinValue)),
                                                           FirstReportDate = ((dr["FirstReportDate"] != DBNull.Value ? Convert.ToDateTime(dr["FirstReportDate"]) : DateTime.MinValue)),
                                                           LastReportDate = ((dr["LastReportDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastReportDate"]) : DateTime.MinValue)),
                                                           NextReportDate = ((dr["NextReportDate"] != DBNull.Value ? Convert.ToDateTime(dr["NextReportDate"]) : DateTime.MinValue)),
                                                           CreditReportStatus = ((dr["CreditReportStatus"] != DBNull.Value ? dr["CreditReportStatus"].ToString() : "")),
                                                           ClientLastViewDate = ((dr["ClientLastViewDate"] != DBNull.Value ? Convert.ToDateTime(dr["ClientLastViewDate"]) : DateTime.MinValue)),
                                                           ClientLastViewBy = ((dr["ClientLastViewBy"] != DBNull.Value ? Convert.ToInt32(dr["ClientLastViewBy"]) : 0)),
                                                           IsEstAudit = ((dr["IsEstAudit"] != DBNull.Value ? Convert.ToBoolean(dr["IsEstAudit"]) : false)),
                                                           IsSecondary = ((dr["IsSecondary"] != DBNull.Value ? Convert.ToBoolean(dr["IsSecondary"]) : false)),
                                                           IsSkip = ((dr["IsSkip"] != DBNull.Value ? Convert.ToBoolean(dr["IsSkip"]) : false)),
                                                           IsJudgmentAcct = ((dr["IsJudgmentAcct"] != DBNull.Value ? Convert.ToBoolean(dr["IsJudgmentAcct"]) : false)),
                                                           IsIntlAcct = ((dr["IsIntlAcct"] != DBNull.Value ? Convert.ToBoolean(dr["IsIntlAcct"]) : false)),
                                                           TempId = ((dr["TempId"] != DBNull.Value ? Convert.ToInt32(dr["TempId"]) : 0)),
                                                           CollectionStatus = ((dr["CollectionStatus"] != DBNull.Value ? dr["CollectionStatus"].ToString() : "")),
                                                           Location = ((dr["Location"] != DBNull.Value ? dr["Location"].ToString() : "")),
                                                           FeeCode = ((dr["FeeCode"] != DBNull.Value ? dr["FeeCode"].ToString() : "")),
                                                           CommRateId = ((dr["CommRateId"] != DBNull.Value ? Convert.ToInt32(dr["CommRateId"]) : 0)),
                                                           TotAsgAmt = ((dr["TotAsgAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotAsgAmt"]) : 0)),
                                                           TotAsgRcvAmt = ((dr["TotAsgRcvAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotAsgRcvAmt"]) : 0)),
                                                           TotCollFeeAmt = ((dr["TotCollFeeAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotCollFeeAmt"]) : 0)),
                                                           TotCollFeeRcvAmt = ((dr["TotCollFeeRcvAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotCollFeeRcvAmt"]) : 0)),
                                                           TotAdjAmt = ((dr["TotAdjAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotAdjAmt"]) : 0)),
                                                           TotPmtAmt = ((dr["TotPmtAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotPmtAmt"]) : 0)),
                                                           TotBalAmt = ((dr["TotBalAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotBalAmt"]) : 0)),
                                                           InterestRate = ((dr["InterestRate"] != DBNull.Value ? Convert.ToDecimal(dr["InterestRate"]) : 0)),
                                                           TotIntAmt = ((dr["TotIntAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotIntAmt"]) : 0)),
                                                           TotIntRcvAmt = ((dr["TotIntRcvAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotIntRcvAmt"]) : 0)),
                                                           TotCostExpAmt = ((dr["TotCostExpAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotCostExpAmt"]) : 0)),
                                                           TotCostRcvAmt = ((dr["TotCostRcvAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotCostRcvAmt"]) : 0)),
                                                           TotAttyExpAmt = ((dr["TotAttyExpAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotAttyExpAmt"]) : 0)),
                                                           TotAttyRcvAmt = ((dr["TotAttyRcvAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotAttyRcvAmt"]) : 0)),
                                                           TotalDue = ((dr["TotalDue"] != DBNull.Value ? Convert.ToDecimal(dr["TotalDue"]) : 0)),
                                                           IsMainAddress = ((dr["IsMainAddress"] != DBNull.Value ? Convert.ToBoolean(dr["IsMainAddress"]) : false)),
                                                           AddressName = ((dr["AddressName"] != DBNull.Value ? dr["AddressName"].ToString() : "")),
                                                           ContactName = ((dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : "")),
                                                           ContactTitle = ((dr["ContactTitle"] != DBNull.Value ? dr["ContactTitle"].ToString() : "")),
                                                           AddressLine1 = ((dr["AddressLine1"] != DBNull.Value ? dr["AddressLine1"].ToString() : "")),
                                                           AddressLine2 = ((dr["AddressLine2"] != DBNull.Value ? dr["AddressLine2"].ToString() : "")),
                                                           City = ((dr["City"] != DBNull.Value ? dr["City"].ToString() : "")),
                                                           State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),
                                                           PostalCode = ((dr["PostalCode"] != DBNull.Value ? dr["PostalCode"].ToString() : "")),
                                                           County = ((dr["County"] != DBNull.Value ? dr["County"].ToString() : "")),
                                                           Country = ((dr["Country"] != DBNull.Value ? dr["Country"].ToString() : "")),
                                                           PhoneNo = ((dr["PhoneNo"] != DBNull.Value ? dr["PhoneNo"].ToString() : "")),
                                                           Fax = ((dr["Fax"] != DBNull.Value ? dr["Fax"].ToString() : "")),
                                                           Email = ((dr["Email"] != DBNull.Value ? dr["Email"].ToString() : "")),
                                                           PhoneNo2 = ((dr["PhoneNo2"] != DBNull.Value ? dr["PhoneNo2"].ToString() : "")),
                                                           TypeCode = ((dr["TypeCode"] != DBNull.Value ? dr["TypeCode"].ToString() : "")),
                                                           PhoneNoExt = ((dr["PhoneNoExt"] != DBNull.Value ? dr["PhoneNoExt"].ToString() : "")),
                                                           PhoneNo2Ext = ((dr["PhoneNo2Ext"] != DBNull.Value ? dr["PhoneNo2Ext"].ToString() : "")),
                                                           CellPhone = ((dr["CellPhone"] != DBNull.Value ? dr["CellPhone"].ToString() : "")),
                                                           NoMailFlag = ((dr["NoMailFlag"] != DBNull.Value ? Convert.ToBoolean(dr["NoMailFlag"]) : false)),
                                                           MailReturnFlag = ((dr["MailReturnFlag"] != DBNull.Value ? Convert.ToBoolean(dr["MailReturnFlag"]) : false)),
                                                           BadAddressFlag = ((dr["BadAddressFlag"] != DBNull.Value ? Convert.ToBoolean(dr["BadAddressFlag"]) : false)),
                                                           BadPhoneFlag = ((dr["BadPhoneFlag"] != DBNull.Value ? Convert.ToBoolean(dr["BadPhoneFlag"]) : false)),
                                                           ClientContactName = ((dr["ClientContactName"] != DBNull.Value ? dr["ClientContactName"].ToString() : "")),
                                                           ClientContactEmail = ((dr["ClientContactEmail"] != DBNull.Value ? dr["ClientContactEmail"].ToString() : "")),
                                                           ClientContactPhone = ((dr["ClientContactPhone"] != DBNull.Value ? dr["ClientContactPhone"].ToString() : "")),
                                                           BatchId = ((dr["BatchId"] != DBNull.Value ? Convert.ToInt32(dr["BatchId"]) : 0)),
                                                           BatchDebtAccountId = ((dr["BatchDebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["BatchDebtAccountId"]) : 0)),
                                                           TotLegalExpAmt = ((dr["TotLegalExpAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotLegalExpAmt"]) : 0)),
                                                           TotRcvAmt = ((dr["TotRcvAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotRcvAmt"]) : 0)),
                                                           CollectionStatusDescr = ((dr["CollectionStatusDescr"] != DBNull.Value ? dr["CollectionStatusDescr"].ToString() : "")),
                                                           CollectorEmail = ((dr["CollectorEmail"] != DBNull.Value ? dr["CollectorEmail"].ToString() : "")),
                                                           ClientContactTitle = ((dr["ClientContactTitle"] != DBNull.Value ? dr["ClientContactTitle"].ToString() : "")),
                                                           ClientContactSalutation = ((dr["ClientContactSalutation"] != DBNull.Value ? dr["ClientContactSalutation"].ToString() : "")),
                                                           StatusGroup = ((dr["StatusGroup"] != DBNull.Value ? Convert.ToInt32(dr["StatusGroup"]) : 0)),
                                                           CollectorName = ((dr["CollectorName"] != DBNull.Value ? dr["CollectorName"].ToString() : "")),
                                                           SalesNum = ((dr["SalesNum"] != DBNull.Value ? dr["SalesNum"].ToString() : "")),
                                                           SalesName = ((dr["SalesName"] != DBNull.Value ? dr["SalesName"].ToString() : "")),
                                                           SetupDate = ((dr["SetupDate"] != DBNull.Value ? Convert.ToDateTime(dr["SetupDate"]) : DateTime.MinValue)),
                                                           IsPrecollect = ((dr["IsPrecollect"] != DBNull.Value ? Convert.ToBoolean(dr["IsPrecollect"]) : false)),
                                                           SalesTeamId = ((dr["SalesTeamId"] != DBNull.Value ? Convert.ToInt32(dr["SalesTeamId"]) : 0)),
                                                           TotAdjNoCFAmt = ((dr["TotAdjNoCFAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotAdjNoCFAmt"]) : 0)),
                                                           TotBalNoCFAmt = ((dr["TotBalNoCFAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotBalNoCFAmt"]) : 0)),
                                                           ClientContact = ((dr["ClientContact"] != DBNull.Value ? dr["ClientContact"].ToString() : "")),
                                                           ClientTitle = ((dr["ClientTitle"] != DBNull.Value ? dr["ClientTitle"].ToString() : "")),
                                                           ClientAs = ((dr["ClientAs"] != DBNull.Value ? dr["ClientAs"].ToString() : "")),
                                                           ClientLegalName = ((dr["ClientLegalName"] != DBNull.Value ? dr["ClientLegalName"].ToString() : "")),
                                                           ClientEntity = ((dr["ClientEntity"] != DBNull.Value ? dr["ClientEntity"].ToString() : "")),
                                                           ClientDBA = ((dr["ClientDBA"] != DBNull.Value ? dr["ClientDBA"].ToString() : "")),
                                                           // Signature = ((dr["Signature"] != DBNull.Value ? Convert.ToByte( dr["Signature"]) : )),
                                                           LegalAddr1 = ((dr["LegalAddr1"] != DBNull.Value ? dr["LegalAddr1"].ToString() : "")),
                                                           LegalAddr2 = ((dr["LegalAddr2"] != DBNull.Value ? dr["LegalAddr2"].ToString() : "")),
                                                           LegalCity = ((dr["LegalCity"] != DBNull.Value ? dr["LegalCity"].ToString() : "")),
                                                           LegalState = ((dr["LegalState"] != DBNull.Value ? dr["LegalState"].ToString() : "")),
                                                           LegalZip = ((dr["LegalZip"] != DBNull.Value ? dr["LegalZip"].ToString() : "")),
                                                           SCContactName = ((dr["SCContactName"] != DBNull.Value ? dr["SCContactName"].ToString() : "")),
                                                           SCContactTitle = ((dr["SCContactTitle"] != DBNull.Value ? dr["SCContactTitle"].ToString() : "")),
                                                           SCAddr1 = ((dr["SCAddr1"] != DBNull.Value ? dr["SCAddr1"].ToString() : "")),
                                                           SCAddr2 = ((dr["SCAddr2"] != DBNull.Value ? dr["SCAddr2"].ToString() : "")),
                                                           SCCity = ((dr["SCCity"] != DBNull.Value ? dr["SCCity"].ToString() : "")),
                                                           SCState = ((dr["SCState"] != DBNull.Value ? dr["SCState"].ToString() : "")),
                                                           SCZip = ((dr["SCZip"] != DBNull.Value ? dr["SCZip"].ToString() : "")),
                                                           SCPhone = ((dr["SCPhone"] != DBNull.Value ? dr["SCPhone"].ToString() : "")),
                                                           SCFax = ((dr["SCFax"] != DBNull.Value ? dr["SCFax"].ToString() : "")),
                                                           SCEmail = ((dr["SCEmail"] != DBNull.Value ? dr["SCEmail"].ToString() : "")),
                                                           FBN = ((dr["FBN"] != DBNull.Value ? dr["FBN"].ToString() : "")),
                                                           ExpireDate = ((dr["ExpireDate"] != DBNull.Value ? Convert.ToDateTime(dr["ExpireDate"]) : DateTime.MinValue)),
                                                           FBNCounty = ((dr["FBNCounty"] != DBNull.Value ? dr["FBNCounty"].ToString() : "")),
                                                           ClientPhone = ((dr["ClientPhone"] != DBNull.Value ? dr["ClientPhone"].ToString() : "")),
                                                           TotAsgAmtEx = ((dr["TotAsgAmtEx"] != DBNull.Value ? Convert.ToDecimal(dr["TotAsgAmtEx"]) : 0)),
                                                           CollectionRateId = ((dr["CollectionRateId"] != DBNull.Value ? Convert.ToInt32(dr["CollectionRateId"]) : 0)),
                                                           MiscVar = ((dr["MiscVar"] != DBNull.Value ? Convert.ToBoolean(dr["MiscVar"]) : false)),
                                                           IsRapidRebate = ((dr["IsRapidRebate"] != DBNull.Value ? Convert.ToBoolean(dr["IsRapidRebate"]) : false)),
                                                           IsForwarding = ((dr["IsForwarding"] != DBNull.Value ? Convert.ToBoolean(dr["IsForwarding"]) : false)),
                                                           IsSmallClaims = ((dr["IsSmallClaims"] != DBNull.Value ? Convert.ToBoolean(dr["IsSmallClaims"]) : false)),
                                                           ServiceType = ((dr["ServiceType"] != DBNull.Value ? dr["ServiceType"].ToString() : "")),
                                                           MatchingKey = ((dr["MatchingKey"] != DBNull.Value ? dr["MatchingKey"].ToString() : "")),
                                                           AddressKey = ((dr["AddressKey"] != DBNull.Value ? dr["AddressKey"].ToString() : "")),
                                                           SCReviewDate = ((dr["SCReviewDate"] != DBNull.Value ? Convert.ToDateTime(dr["SCReviewDate"]) : DateTime.MinValue)),
                                                           TrialDate = ((dr["TrialDate"] != DBNull.Value ? Convert.ToDateTime(dr["TrialDate"]) : DateTime.MinValue)),
                                                           ProcessServerId = ((dr["ProcessServerId"] != DBNull.Value ? Convert.ToInt32(dr["ProcessServerId"]) : 0)),
                                                           CourtId = ((dr["CourtId"] != DBNull.Value ? Convert.ToInt32(dr["CourtId"]) : 0)),
                                                           CaseNum = ((dr["CaseNum"] != DBNull.Value ? dr["CaseNum"].ToString() : "")),
                                                           EntityName1 = ((dr["EntityName1"] != DBNull.Value ? dr["EntityName1"].ToString() : "")),
                                                           IsDropContact = ((dr["IsDropContact"] != DBNull.Value ? Convert.ToBoolean(dr["IsDropContact"]) : false)),
                                                           AttyId = ((dr["AttyId"] != DBNull.Value ? Convert.ToInt32(dr["AttyId"]) : 0)),
                                                           IsNoCBR = ((dr["IsNoCBR"] != DBNull.Value ? Convert.ToBoolean(dr["IsNoCBR"]) : false)),
                                                           IsNoCollectFee = ((dr["IsNoCollectFee"] != DBNull.Value ? Convert.ToBoolean(dr["IsNoCollectFee"]) : false)),
                                                           IsCreditReport = ((dr["IsCreditReport"] != DBNull.Value ? Convert.ToBoolean(dr["IsCreditReport"]) : false)),
                                                           SuitFiledDate = ((dr["SuitFiledDate"] != DBNull.Value ? Convert.ToDateTime(dr["SuitFiledDate"]) : DateTime.MinValue)),
                                                           BKCaseNo = ((dr["BKCaseNo"] != DBNull.Value ? dr["BKCaseNo"].ToString() : "")),
                                                           DebtorBKName = ((dr["DebtorBKName"] != DBNull.Value ? dr["DebtorBKName"].ToString() : "")),
                                                           TotCollFeeRcvAmtEx = ((dr["TotCollFeeRcvAmtEx"] != DBNull.Value ? Convert.ToDecimal(dr["TotCollFeeRcvAmtEx"]) : 0)),
                                                           LegalSetupDate = ((dr["LegalSetupDate"] != DBNull.Value ? Convert.ToDateTime(dr["LegalSetupDate"]) : DateTime.MinValue)),
                                                           LegalAssignAmt = ((dr["LegalAssignAmt"] != DBNull.Value ? Convert.ToDecimal(dr["LegalRate"]) : 0)),
                                                           LegalRate = ((dr["LegalRate"] != DBNull.Value ? Convert.ToDecimal(dr["TotCollFeeRcvAmtEx"]) : 0)),
                                                           TotFeeAmt = ((dr["TotFeeAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotFeeAmt"]) : 0)),
                                                           TotAttyOwedAmt = ((dr["TotAttyOwedAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotAttyOwedAmt"]) : 0)),
                                                           ParentDebtAccountId = ((dr["ParentDebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["ParentDebtAccountId"]) : 0)),
                                                           SCVenue = ((dr["SCVenue"] != DBNull.Value ? dr["SCVenue"].ToString() : "")),
                                                           IsTenDayContactPlan = ((dr["IsTenDayContactPlan"] != DBNull.Value ? Convert.ToBoolean(dr["IsTenDayContactPlan"]) : false)),
                                                           CollectorPhone = ((dr["CollectorPhone"] != DBNull.Value ? dr["CollectorPhone"].ToString() : "")),
                                                           ClientFax = ((dr["ClientFax"] != DBNull.Value ? dr["ClientFax"].ToString() : "")),
                                                           IsIntlClient = ((dr["IsIntlClient"] != DBNull.Value ? Convert.ToBoolean(dr["IsIntlClient"]) : false)),
                                                           SCContactAs = ((dr["SCContactAs"] != DBNull.Value ? dr["SCContactAs"].ToString() : "")),
                                                           SIFAuthority = ((dr["SIFAuthority"] != DBNull.Value ? dr["SIFAuthority"].ToString() : "")),
                                                           ClientTypeName = ((dr["ClientTypeName"] != DBNull.Value ? dr["ClientTypeName"].ToString() : "")),
                                                           ClientTypeCode = ((dr["ClientTypeCode"] != DBNull.Value ? dr["ClientTypeCode"].ToString() : "")),
                                                           ClientTypeId = ((dr["ClientTypeId"] != DBNull.Value ? Convert.ToInt32(dr["ClientTypeId"]) : 0)),
                                                           CanAccountAmt = ((dr["CanAccountAmt"] != DBNull.Value ? Convert.ToDecimal(dr["CanAccountAmt"]) : 0)),
                                                           JudgmentDate = ((dr["JudgmentDate"] != DBNull.Value ? Convert.ToDateTime(dr["JudgmentDate"]) : DateTime.MinValue)),
                                                           FwdJudgmentDate = ((dr["FwdJudgmentDate"] != DBNull.Value ? Convert.ToDateTime(dr["FwdJudgmentDate"]) : DateTime.MinValue)),
                                                           SSN = ((dr["SSN"] != DBNull.Value ? dr["SSN"].ToString() : "")),
                                                           DebtAddressTypeId = ((dr["DebtAddressTypeId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAddressTypeId"]) : 0)),
                                                           DebtAddressId = ((dr["DebtAddressId"] != DBNull.Value ? Convert.ToDecimal(dr["DebtAddressId"]) : 0)),
                                                           BatchType = ((dr["BatchType"] != DBNull.Value ? dr["BatchType"].ToString() : "")),
                                                           SubmittedBy = ((dr["SubmittedBy"] != DBNull.Value ? dr["SubmittedBy"].ToString() : "")),
                                                           IsStatusGroup = ((dr["IsStatusGroup"] != DBNull.Value ? Convert.ToBoolean(dr["IsStatusGroup"]) : false)),
                                                           IsStatusRpt = ((dr["IsStatusRpt"] != DBNull.Value ? Convert.ToBoolean(dr["IsStatusRpt"]) : false)),
                                                           PaymentDueDate = ((dr["PaymentDueDate"] != DBNull.Value ? Convert.ToDateTime(dr["PaymentDueDate"]) : DateTime.MinValue)),
                                                           UsageFee = ((dr["UsageFee"] != DBNull.Value ? Convert.ToDecimal(dr["UsageFee"]) : 0)),
                                                           EarlyTerminationFee = ((dr["EarlyTerminationFee"] != DBNull.Value ? Convert.ToDecimal(dr["EarlyTerminationFee"]) : 0)),
                                                           ServiceState = ((dr["EarlyTerminationFee"] != DBNull.Value ? dr["EarlyTerminationFee"].ToString() : "")),
                                                       }).ToList();
            return Account.First();
        }
        #endregion
    }
}
