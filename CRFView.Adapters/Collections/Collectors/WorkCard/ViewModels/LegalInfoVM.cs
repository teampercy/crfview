﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Collections.Collectors.WorkCard.ViewModels
{
    public class LegalInfoVM
    {
        public LegalInfoVM()
        {
            objLegalInfoEdit = new ADDebtAccountLegalInfo();
            objAttorneyInfoEdit = new ADAttorneyInfo();
            AttyNoList = CommonBindList.GetAttyInfo();
            LawListCodeList = CommonBindList.GetLawListInfo();
        }
        public ADDebtAccountLegalInfo objLegalInfoEdit { get; set; }
        public string DebtAccountId { get; set; }
        public string PKID { get; set; }
        public string LawPKId { get; set; }
        
        public IEnumerable<SelectListItem> AttyNoList { get; set; }
        public string ddlAttyNo { get; set; }
        public string AttyNo { get; set; }

        public ADAttorneyInfo objAttorneyInfoEdit { get; set;}

        public decimal TotalCosts { get; set; }

        public IEnumerable<SelectListItem> LawListCodeList { get; set; }
        public string ddlLawListCode { get; set; }
        public string LawListCode { get; set; }
        
        public ADLawListInfo objLawListInfoEdit { get; set; }

        public bool chkSuitFiled { get; set; }
        public bool chkJudgmentDate { get; set; }

        public decimal LegalAssignAmt{ get; set; }

}
}
