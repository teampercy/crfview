﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Collections.Collectors.WorkCard.ViewModels
{
    public class CreditReportVM
    {
        public CreditReportVM()
        {
            ObjDebtAccountIndividualCreditReport = new ADDebtAccountCreditReport();
            ObjDebtAccountCommercialCreditReport = new ADDebtAccountCreditReport();
            AccountStatusList = CommonBindList.GetCreditReportStatus();
        }

        public CreditReportVM(string DebtAccountId)
        {
            ObjDebtAccountIndividualCreditReport = new ADDebtAccountCreditReport();
            ObjDebtAccountCommercialCreditReport = new ADDebtAccountCreditReport();
            SelectNamesList = CommonBindList.GetSelectNames(DebtAccountId);
            AccountStatusList = CommonBindList.GetCreditReportStatus();
        }

        public string ddlSelectNames { get; set; }
        public string DebtAddressId { get; set; }
        public string DebtAccountId { get; set; }
        public string Id { get; set; }
        public IEnumerable<SelectListItem> SelectNamesList { get; set; }
        public ADDebtAddress ObjAddress { get; set; }

        public string ddlAccountStatus { get; set; }
        public IEnumerable<SelectListItem> AccountStatusList { get; set; }
        public string AccountStatus { get; set; }

        public bool chkDispute { get; set; }
        public string RbtDisputeCode { get; set; }

        public ADDebtAccountCreditReport ObjDebtAccountIndividualCreditReport { get; set; }
        public ADDebtAccountCreditReport ObjDebtAccountCommercialCreditReport { get; set; }

        public string DebtAddressTypeId { get; set; }
    }
}
