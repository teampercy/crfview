﻿using CRF.BLL.COMMON;
using CRF.BLL.Providers;
using CRFView.Adapters.Clients.Client_Management;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Collections.Collectors.WorkCard.ViewModels
{
    public class LetterVM
    {
        public LetterVM()
        {
            LocationIdList = CommonBindList.GetLocationIdList();
        }

        public LetterVM(int DebtId)
        {
            string mysql = "Select *  from DebtAddress where BadAddressFlag = 0 and DebtId = "+ DebtId;
            var DebtAddressView = DBO.Provider.GetTableView(mysql);
            DataTable dtDebtAddress = DebtAddressView.ToTable();
            ObjDebtAccountAddressList = ADDebtAddress.GetAddressList(dtDebtAddress);

            LocationIdList = CommonBindList.GetLocationIdList();
        }

        public string DebtAccountId { get; set; }
        public string DebtId { get; set; }
        public string PKID { get; set; }

        public ADDebtAddress ObjDebtAddress { get; set; }
        public ADDebtAccountLetters ObjDebtAccountLetters { get; set; }
        public ADDebtAccount ObjDebtAccount { get; set; }
        public ADLetters ObjLetters { get; set; }        
        public ADCompanyInfo ObjCompanyInfo { get; set; }

        public string ddlLetterTypeId { get; set; }

        public List<ADDebtAddress> ObjDebtAccountAddressList { get; set; }

        public string LetterType { get; set; }
        public string RbtLetterType { get; set; }
        public string selectedDebtAddress { get; set; }


        public string StatusReportLetterType { get; set; }
        public string RbtStatusReportLetterType { get; set; }
        
        public string ddlStatusReportLetterTypeId { get; set; }
        public ADDebtAccountLetters ObjDebtAccountLetters_StatusReport { get; set; }

        public ADCustomText ObjCollectionCustomText { get; set; }

        public bool chkDesk { get; set; }
        
        public string ddlLocationId { get; set; }
        public IEnumerable<SelectListItem> LocationIdList { get; set; }

        public DataTable ObjDebtAccountLettersDt { get; set; }

        public ADClientCollectionInfo ObjClientCollectionInfo { get; set; }
        public ADDebtAccountNote ObjDebtAccountNote { get; set; }
        public string CustomText { get; set; }

        public DataTable GetLetterInfoDt { get; set; }

        public string PrintLetter(int DebtAccountId, int PKID, int AddressId, int LocationId)
        {
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            string pdfPath = null;
            pdfPath = CollectionsCollectors.PrintLetter(user, PKID,  AddressId, DebtAccountId,  LocationId);

            return pdfPath;
        }


    }
}
