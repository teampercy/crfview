﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Collections.Collectors.WorkCard.ViewModels
{
    public class FilterVM
    {
        public FilterVM()
        {
            CollectionStatusList = CommonBindList.GetCollectionStatuses();
            CollectionStatusGroupList = CommonBindList.GetCollectionStatusGroup();
            LocationList = CommonBindList.GetLocation();
            AttorneyList = CommonBindList.GetAttorneyInfo();
            CourtHouseList = CommonBindList.GetCourtHouse();
            ProcessServerList = CommonBindList.GetProcessServer();
        }
        public Filter FilterObj { get; set; }
        public IEnumerable<SelectListItem> LocationList { get; set; }
        public IEnumerable<SelectListItem> CollectionStatusList { get; set; }
        public IEnumerable<SelectListItem> CollectionStatusGroupList { get; set; }
        public IEnumerable<SelectListItem> AttorneyList { get; set; }
        public IEnumerable<SelectListItem> CourtHouseList { get; set; }
        public IEnumerable<SelectListItem> ProcessServerList { get; set; }
    }
}
