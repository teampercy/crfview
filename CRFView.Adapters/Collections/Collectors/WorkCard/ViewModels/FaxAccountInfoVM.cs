﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard.ViewModels
{
    public class FaxAccountInfoVM
    {
        public ADDebt obj_Debt { get; set; }
        public ADDebtAccount obj_DebtAccount { get; set; }
        public ADClient obj_Client { get; set; }
    
        public int DebtAccountId { get; set; }
        public string DBFaxContact { get; set; }
        public string CLFaxContact { get; set; }
     
    }
}
