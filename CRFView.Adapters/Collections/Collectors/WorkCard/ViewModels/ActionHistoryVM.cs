﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Collections.Collectors.WorkCard.ViewModels
{
    public class ActionHistoryVM
    {
        public ActionHistoryVM()
        {
            RbtActionType = "P"; //default P = Phone
            ActionHistoryCodesList = CommonBindList.GetActionHistoryCodes();
            ActionHistoryCodesList2 = CommonBindList.GetActionHistoryCodes2();
        }

        public IEnumerable<SelectListItem> ActionHistoryCodesList { get; set; }
        public List<CommonBindList> ActionHistoryCodesList2 { get; set; }
        public string ddlActionHistoryCodeId { get; set; }
        public string DebtAccountId { get; set; }
        public string CollectionStatusId { get; set; }
        public string SelectedActionHistoryCodesId { get; set; }
        public string RbtActionType { get; set; }

        public string hdntxtActionHistoryCodeId { get; set; }

        public string CityName { get; set; }
}
}
