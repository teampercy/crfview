﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard.ViewModels
{
    public class CostsSpentVM
    {
        public CostsSpentVM(int DebtAccountId)
        {
            string mysql = "select * from debtaccountlegalcost where ledgertype in ('ATTA','ATTC' ) AND debtaccountid = " + DebtAccountId;
            var CostsSpentView = DBO.Provider.GetTableView(mysql);
            DataTable dtCostsSpent = CostsSpentView.ToTable();
            ObjDebtAccountLegalCostList = ADDebtAccountLegalCost.GetDebtAccountLegalCost(dtCostsSpent);
        }

        public string DebtAccountId { get; set; }
        public List<ADDebtAccountLegalCost> ObjDebtAccountLegalCostList { get; set; }

        public DataTable ObjDebtAccountLegalCostATTCDt { get; set; }

        public DataTable ObjDebtAccountLegalCostATTADt { get; set; }

        public ADDebtAccountLegalCost ObjDebtAccountLegalCost { get; set; }

        public decimal CLCC { get; set; }
        public decimal CLSF { get; set; }
        public decimal AGCC { get; set; }
        public decimal AGSF { get; set; }
        public decimal TOTCL { get; set; }
        public decimal TOTAG { get; set; }
        public decimal TOTCOST { get; set; }

    }
}
