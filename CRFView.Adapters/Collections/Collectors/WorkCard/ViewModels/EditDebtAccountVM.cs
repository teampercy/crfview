﻿using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard.ViewModels
{
    public class EditDebtAccountVM
    {
        public ADDebtAccount obj_DebtAccountEdit { get; set; }
        public ADDebt obj_DebtEdit { get; set; }
        public ADDebtAddress obj_DebtAddressEdit { get; set; }


    }
}
