﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard.ViewModels
{
    public class LegalCostVM
    {
        public LegalCostVM()
        {

        }
        
        public ADDebtAccountLegalCost objDebtAccountLegalCost { get; set; }
        
        public ADDebtAccountLegalCost objDebtAccountLegalCostEdit { get; set; }

        public string DebtAccountId { get; set; }
        public string Id { get; set; }
    }
}
