﻿using CRFView.Adapters.Clients.Client_Management;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard.ViewModels
{
    public class ClientInfoVM
    {
        //public ADClient obj_Client { get; set; }
        
        public ADClientCollectionInfo obj_ClientCollectionInfo { get; set; }

    }
}
