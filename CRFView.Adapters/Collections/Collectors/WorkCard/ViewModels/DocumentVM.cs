﻿using CRFView.Adapters.Collections.Collectors.WorkCard;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace CRFView.Adapters.Collections.Collectors.WorkCard.ViewModels
{
    public class DocumentVM
    {
        public DocumentVM()
        {
            ObjDebtAccountAttachment = new ADDebtAccountAttachment();
            ObjDebtAccountAttachmentEdit = new ADDebtAccountAttachment();
            DocumentTypeList = CommonBindList.GetDocumentType();
        }

        public ADDebtAccountAttachment ObjDebtAccountAttachment { get; set; }
        public ADDebtAccountAttachment ObjDebtAccountAttachmentEdit { get; set; }

        public string ddlDocumentType { get; set; }
        public IEnumerable<SelectListItem> DocumentTypeList { get; set; }
        public string DocumentType { get; set; }

        public string DebtAccountId { get; set; }
        public string Id { get; set; }
    }
}
