﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard.ViewModels
{
    public class AddressVM
    {
        public ADDebtAddress ObjAddress { get; set; }

        public int obj_addressDebtId { get; set; }
        public int obj_addressTypeId { get; set; }
        public int obj_addressId { get; set; }

    }
}
