﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Collections.Collectors.WorkCard.ViewModels
{
    public class EditMoreInfoVM
    {
        public ADDebtAccount obj_DebtAccountMoreInfoEdit { get; set; }
        public IEnumerable<SelectListItem> LocationList { get; set; }

        public EditMoreInfoVM()
        {
            LocationList = CommonBindList.GetLocationDebt();
        }

    }
}
