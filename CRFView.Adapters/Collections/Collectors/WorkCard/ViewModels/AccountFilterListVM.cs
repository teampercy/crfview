﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Collections.Collectors.WorkCard.ViewModels
{
    public class AccountFilterListVM
    {
        #region Properties
        public List<ADvwAccountFilterList> ObjAccounts { get; set; }
        public string SortType { get; set; }
        public List<SelectListItem> SortTypeList = new List<SelectListItem>();

        public string SortBy { get; set; }
        public List<SelectListItem> SortByList = new List<SelectListItem>();

        #endregion

        public AccountFilterListVM()
        {
            populateLists();
        }


        public int listCount;
        //public int listCount;
        private void populateLists()
        {
            SortByList.Add(new SelectListItem { Text = "Debtor #", Value = "-1" });
            //SortByList.Add(new SelectListItem { Text = "Sort By", Value = "-1" });
            SortByList.Add(new SelectListItem { Text = "Debtor Name", Value = "1" });
            //SortByList.Add(new SelectListItem { Text = "Contact", Value = "2" });
            SortByList.Add(new SelectListItem { Text = "Debtor Address", Value = "3" });
            //SortByList.Add(new SelectListItem { Text = "Debtor Ref#", Value = "4" });
            SortByList.Add(new SelectListItem { Text = "Status Code", Value = "5" });
            SortByList.Add(new SelectListItem { Text = "Desk#", Value = "6" });
            SortByList.Add(new SelectListItem { Text = "To Desk#", Value = "7" });
            //SortByList.Add(new SelectListItem { Text = "Date Assigned", Value = "8" });
            SortByList.Add(new SelectListItem { Text = "Assign Date", Value = "8" });
            SortByList.Add(new SelectListItem { Text = "Review Date", Value = "9" });
            //SortByList.Add(new SelectListItem { Text = "Last Payment", Value = "10" });
           // SortByList.Add(new SelectListItem { Text = "Close Date", Value = "11" });
            SortByList.Add(new SelectListItem { Text = "Assignment Amt", Value = "12" });
            //SortByList.Add(new SelectListItem { Text = "Collect Fees", Value = "13" });
            //SortByList.Add(new SelectListItem { Text = "Interest", Value = "14" });
           // SortByList.Add(new SelectListItem { Text = "Payments", Value = "15" });
            SortByList.Add(new SelectListItem { Text = "Total Owned", Value = "16" });
            SortByList.Add(new SelectListItem { Text = "SC Rev Date", Value = "17" });
            //SortByList.Add(new SelectListItem { Text = "SC Trial Date", Value = "18" });
           // SortByList.Add(new SelectListItem { Text = "SC Judge Date", Value = "19" });
            //SortByList.Add(new SelectListItem { Text = "FWD Court Date", Value = "20" });
            //SortByList.Add(new SelectListItem { Text = "FWD Judge Date", Value = "21" });
            SortByList.Add(new SelectListItem { Text = "Debtor State ", Value = "22" });
            SortByList.Add(new SelectListItem { Text = "Client Code ", Value = "23" });

            SortTypeList.Add(new SelectListItem { Text = "Ascending", Value = "-1" });
            SortTypeList.Add(new SelectListItem { Text = "Descending", Value = "1"});
        }
         
        }
}
