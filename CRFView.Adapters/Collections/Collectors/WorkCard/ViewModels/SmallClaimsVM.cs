﻿using CRF.BLL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Collections.Collectors.WorkCard.ViewModels
{
    public class SmallClaimsVM
    {
        public SmallClaimsVM()
        {

        }

        public SmallClaimsVM(int DebtAccountId)
        {
            string mysql = "select * from debtaccountlegalcost where ledgertype in ('CRTA','CRTC') AND debtaccountid = " + DebtAccountId;
            var CostsSpentView = DBO.Provider.GetTableView(mysql);
            DataTable dtCostsSpent = CostsSpentView.ToTable();
            ObjDebtAccountSCCostList = ADDebtAccountLegalCost.GetDebtAccountLegalCost(dtCostsSpent);
        }

        public ADDebtAccountSmallClaims ObjDebtAccountSmallClaims { get; set; }
        public ADDebtAccountSmallClaims ObjDebtAccountSmallClaimsEdit { get; set; }

        public ADCourtHouse ObjCourtHouse  { get; set; }
        public ADCourtHouse ObjCourtHouseEdit { get; set; }

        public ADProcessServer ObjProcessServers { get; set; }
        public ADProcessServer ObjProcessServersEdit { get; set; }

        public DataTable ObjDebtAddressDt { get; set; }

        public string DebtAccountId { get; set; }
        public string State { get; set; }
        public string DebtId { get; set; }
        public string PKID { get; set; }
        
        public string ddlCourtId { get; set; }
        public IEnumerable<SelectListItem> CourtIdList { get; set; }
         

        public string ddlProcessServerId { get; set; }
        public IEnumerable<SelectListItem> ProcessServerIdList { get; set; }

        public string CourtId { get; set; }
        public string ProcessServerId { get; set; }

        public bool chkTrialDate { get; set; }
        public bool chkSuitFiled { get; set; }

        public string ClientCode { get; set; }
        public string ClientName { get; set; }
        public DateTime ReferalDate { get; set; }
        public string CollectionStatus { get; set; }
        public string DebtorName { get; set; }
        public string AccountNo { get; set; }
        public string Location { get; set; }

        public List<ADDebtAccountLegalCost> ObjDebtAccountSCCostList { get; set; }

        public DataTable ObjDebtAccountSCCostCRTADt { get; set; }

        public DataTable ObjDebtAccountSCCostCRTCDt { get; set; }

        public decimal CLFilingFee { get; set; }
        public decimal CLServiceFeeAmt { get; set; }
        public decimal CLWritFeeAmt { get; set; }
        public decimal CLLevyFeeAmt { get; set; }
        public decimal CLAbstractFeeAmt { get; set; }
        public decimal CLRecordingFeeAmt { get; set; }
        public decimal CLCostExpAmt { get; set; }

        public decimal AGFilingFee { get; set; }
        public decimal AGServiceFeeAmt { get; set; }
        public decimal AGWritFeeAmt { get; set; }
        public decimal AGLevyFeeAmt { get; set; }
        public decimal AGAbstractFeeAmt { get; set; }
        public decimal AGRecordingFeeAmt { get; set; }
        public decimal AGCostExpAmt { get; set; }
        
        public decimal TOTEXP { get; set; }

        public DataTable ObjDebtAccountSCPostPmtsDt { get; set; }
        public DataTable ObjDebtAccountSCPostCostsDt { get; set; }
        public DataTable ObjDebtAccountSCRecCostsDt { get; set; }
        public DataTable ObjDebtAccountSCCostsSpentDt { get; set; }

        public bool chkJudgmentDate { get; set; }       
    }
}
