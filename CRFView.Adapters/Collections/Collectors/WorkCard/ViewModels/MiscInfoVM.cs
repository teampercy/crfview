﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard.ViewModels
{
    public class MiscInfoVM
    {
        public MiscInfoVM()
        {
            objMiscEdit = new ADDebtAccountMisc();
        }
        public ADDebtAccountMisc objMiscEdit { get; set; }
        public string DebtAccountId { get; set; }
        public string PKID { get; set; }
        public string State { get; set; }
    }
}
