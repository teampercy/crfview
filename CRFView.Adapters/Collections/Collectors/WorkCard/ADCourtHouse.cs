﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard
{
    public class ADCourtHouse
    {
        #region Properties
        public int PKID { get; set; }
        public string CourtId { get; set; }
        public string Venue { get; set; }
        public string VenueAdd { get; set; }
        public string VenueCity { get; set; }
        public string VenueState { get; set; }
        public string VenueZip { get; set; }
        public string VenuePhone { get; set; }
        public string VenueWeb { get; set; }

        #endregion

        #region CRUD Operations
      
        //List of CourtHouse
        public List<ADCourtHouse> ListCourtHouse()
        {
            CourtHouse myentity = new CourtHouse();
            TableView EntityList;
            try
            {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADCourtHouse> CTList = (from DataRow dr in dt.Rows
                                              select new ADCourtHouse()
                                              {
                                                  PKID = Convert.ToInt32(dr["PKID"]),
                                                  CourtId = Convert.ToString(dr["CourtId"]),
                                                  Venue = Convert.ToString(dr["Venue"])
                                              }).ToList();

                return CTList;
            }
            catch (Exception ex)
            {
                return (new List<ADCourtHouse>());
            }
        }

        //Read Details from database
        public static ADCourtHouse ReadCourtHouse(int id)
        {
            CourtHouse myentity = new CourtHouse();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (CourtHouse)myentityItable;
            AutoMapper.Mapper.CreateMap<CourtHouse, ADCourtHouse>();
            return (AutoMapper.Mapper.Map<ADCourtHouse>(myentity));
        }

        //Save(Create or Update) CourtHouse Details
        public void SaveCourtHouse(ADCourtHouse updatedCourtHouse)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADCourtHouse, CourtHouse>();
                ITable tblCourtHouse;

                if (updatedCourtHouse.PKID != 0)
                {
                    ADCourtHouse loadCourtHouse = ReadCourtHouse(updatedCourtHouse.PKID);
                    loadCourtHouse.CourtId = updatedCourtHouse.CourtId;
                    loadCourtHouse.Venue = updatedCourtHouse.Venue;
                    loadCourtHouse.VenueAdd = updatedCourtHouse.VenueAdd;
                    loadCourtHouse.VenueCity = updatedCourtHouse.VenueCity;
                    loadCourtHouse.VenueState = updatedCourtHouse.VenueState;
                    loadCourtHouse.VenueZip = updatedCourtHouse.VenueZip;
                    loadCourtHouse.VenuePhone = updatedCourtHouse.VenuePhone;
                    loadCourtHouse.VenueWeb = updatedCourtHouse.VenueWeb;
                    CourtHouse objCourtHouse = AutoMapper.Mapper.Map<CourtHouse>(loadCourtHouse);
                    tblCourtHouse = objCourtHouse;
                    DBO.Provider.Update(ref tblCourtHouse);
                }
                else
                {
                    CourtHouse objCourtHouse = AutoMapper.Mapper.Map<CourtHouse>(updatedCourtHouse);
                    tblCourtHouse = objCourtHouse;
                    DBO.Provider.Create(ref tblCourtHouse);

                }
            }
            catch (Exception ex)
            {

            }
        }
        #endregion
    }
}
