﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADCollectionRate
    {
        public int Id { get; set; }
        public string CollectionRateCode { get; set; }
        public string CollectionRateName { get; set; }


        //Read CollectionRate From CRateId
        public static ADCollectionRate ReadCollectionRate(string CRateId)
        {
            try
            {
                ADCollectionRate Obj_CollectionRate = new ADCollectionRate();
                string MYSQL = "Select Top 1 * from CollectionRate where Id = " + CRateId;
                System.Data.DataSet ds = DBO.Provider.GetDataSet(MYSQL);
                DataTable dt = ds.Tables[0];

                if (dt.Rows.Count >= 1)
                {
                    string Id = dt.Rows[0]["Id"].ToString();
                    CollectionRate myentity = new CollectionRate();
                    ITable myentityItable = myentity;
                    DBO.Provider.Read(Id, ref myentityItable);
                    myentity = (CollectionRate)myentityItable;
                    AutoMapper.Mapper.CreateMap<CollectionRate, ADCollectionRate>();
                    Obj_CollectionRate = AutoMapper.Mapper.Map<ADCollectionRate>(myentity);
                }
                else
                {

                }
                return Obj_CollectionRate;
            }
            catch (Exception ex)
            {
                return new ADCollectionRate();
            }
        }
    }
}
