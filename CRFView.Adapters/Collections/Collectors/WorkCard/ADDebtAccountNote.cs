﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard
{
    public class ADDebtAccountNote
    {
        #region Properties
        public int DebtAccountNoteId { get; set; }
        public int DebtAccountId { get; set; }
        public int InBound { get; set; }
        public int OwnerId { get; set; }
        public string UserCode { get; set; }
        public System.DateTime NoteDate { get; set; }
        public string Note { get; set; }
        public int NoteTypeId { get; set; }
        #endregion

        #region CRUD
        public static ADDebtAccountNote ReadNote(int id)
        {
            DebtAccountNote myentity = new DebtAccountNote();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (DebtAccountNote)myentityItable;
            AutoMapper.Mapper.CreateMap<DebtAccountNote, ADDebtAccountNote>();
            return (AutoMapper.Mapper.Map<ADDebtAccountNote>(myentity));
        }


        public static int SaveDebtAccountNote(ADDebtAccountNote objUpdatedADDebtAccountNote)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADDebtAccountNote, DebtAccountNote>();
                ITable tblDebtAccountNote;
                DebtAccountNote objDebtAccountNote;

                if (objUpdatedADDebtAccountNote.DebtAccountNoteId != 0)
                {
                    ADDebtAccountNote objADDebtAccountNoteOriginal = ReadNote(objUpdatedADDebtAccountNote.DebtAccountNoteId);

                    if (objUpdatedADDebtAccountNote.Note.ToString() == "" || objUpdatedADDebtAccountNote.Note == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountNoteOriginal.Note = objUpdatedADDebtAccountNote.Note;
                    }

                    if (objUpdatedADDebtAccountNote.NoteTypeId.ToString() == "" || objUpdatedADDebtAccountNote.NoteTypeId == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountNoteOriginal.NoteTypeId = objUpdatedADDebtAccountNote.NoteTypeId;
                    }

                    objADDebtAccountNoteOriginal.NoteDate = DateTime.Now;
                    CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                    objADDebtAccountNoteOriginal.UserCode = user.UserName;
                    objADDebtAccountNoteOriginal.OwnerId = user.Id;

                   objDebtAccountNote = AutoMapper.Mapper.Map<DebtAccountNote>(objADDebtAccountNoteOriginal);
                    tblDebtAccountNote = (ITable)objDebtAccountNote;
                    DBO.Provider.Update(ref tblDebtAccountNote);
                }
                else
                {
                    objUpdatedADDebtAccountNote.NoteDate = DateTime.Now;

                    CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                    objUpdatedADDebtAccountNote.UserCode = user.UserName;
                    objUpdatedADDebtAccountNote.OwnerId = user.Id;

                    objDebtAccountNote = AutoMapper.Mapper.Map<DebtAccountNote>(objUpdatedADDebtAccountNote);
                    tblDebtAccountNote = (ITable)objDebtAccountNote;
                    DBO.Provider.Create(ref tblDebtAccountNote);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static bool DeleteDebtAccountNote(string DebtAccountNoteId)
        {
            try
            {
                ITable IReport;
                DebtAccountNote objDebtAccountNote = new DebtAccountNote();
                objDebtAccountNote.DebtAccountNoteId = Convert.ToInt32(DebtAccountNoteId);
                IReport = objDebtAccountNote;
                DBO.Provider.Delete(ref IReport);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }
        #endregion

    }
}
