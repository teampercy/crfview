﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;
using System.ComponentModel.DataAnnotations;
using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Clients.ClientManagementViewModels;
using CRF.BLL.Clients;
using CRFView.Adapters.Clients.Client_Management;
using System.IO;
using System.Text.RegularExpressions;
using CRFView.Adapters.Collections.DayEnd.MatchReport.ViewModels;
using CRF.BLL;
using Ionic.Zip;
using CRFView.Adapters.Collections.Collectors.WorkCard;

namespace CRFView.Adapters
{
    public class ADvwAccountFilterList
    {
        #region Properties
        public int DebtAccountId { get; set; }
        public string ClientCode { get; set; }
        public int ClientId { get; set; }
        public int LocationId { get; set; }
        public int CollectionStatusId { get; set; }
        public string Location { get; set; }
        public string CollectionStatus { get; set; }
        public int TempOwnerId { get; set; }
        public string TempOwner { get; set; }
        public string Priority { get; set; }
        public string AccountNo { get; set; }
        public System.DateTime ReferalDate { get; set; }
        public System.DateTime NextContactDate { get; set; }
        public System.DateTime NextStatusDate { get; set; }
        public System.DateTime CloseDate { get; set; }
        public decimal TotBalAmt { get; set; }
        public bool IsMainAddress { get; set; }
        public string ContactName { get; set; }
        public string AddressLine1 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string PhoneNo { get; set; }
        public string Fax { get; set; }
        public int AttyId { get; set; }
        public System.DateTime TrialDate { get; set; }
        public System.DateTime JudgmentDate { get; set; }
        public int CourtId { get; set; }
        public int ProcessServerId { get; set; }
        public string CaseNum { get; set; }
        public System.DateTime SCReviewDate { get; set; }
        public string DebtorBKName { get; set; }
        public string BKCaseNo { get; set; }
        public string EntityName1 { get; set; }
        public string DebtorName { get; set; }
        public string TaxId { get; set; }
        public System.DateTime FwdJudgmentDate { get; set; }
        public int DebtId { get; set; }
        public string SSN { get; set; }
        public string MatchingKey { get; set; }
        public string AddressKey { get; set; }
        public string BranchNum { get; set; }
        public string FileNumber { get; set; }
        public string StatusGroupDescription { get; set; }
        public int StatusGroupId { get; set; }

        /// <summary>
        public DateTime LastPaymentDate { get; set; }
        public decimal TotAsgAmt { get; set; }
        public decimal TotCollFeeAmt { get; set; }
        public decimal TotIntAmt { get; set; }
        public decimal TotPmtAmt { get; set; }
        public DateTime FwdCourtDate { get; set; }

        //code added by pooja on 07/17/20
        public int PreviousValue { get; set; }
        public int NextValue { get; set; }
        #endregion

        #region CRUD
        public static List<ADvwAccountFilterList> GetInitialList()
        {
            try
            {
                TableView EntityList;
                //var sp_obj = new cview_Accounts_GetAccountFilterList();
                var sp_obj = new cview_Accounts_GetAccountFilterList_Web();
                sp_obj.DebtAccountId = null;              
                EntityList = DBO.Provider.GetTableView(sp_obj);
                DataTable dt = EntityList.ToTable();
                List<ADvwAccountFilterList> AccountList = (from DataRow dr in dt.Rows
                                                           select new ADvwAccountFilterList()
                                                           {
                                                               ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                                               DebtorName = ((dr["DebtorName"] != DBNull.Value ? dr["DebtorName"].ToString() : "")),
                                                               DebtAccountId = Convert.ToInt32(dr["DebtAccountId"]),
                                                               NextContactDate = ((dr["NextContactDate"] != DBNull.Value ? Convert.ToDateTime(dr["NextContactDate"]) : DateTime.MinValue)),
                                                               ReferalDate = ((dr["ReferalDate"] != DBNull.Value ? Convert.ToDateTime(dr["ReferalDate"]) : DateTime.MinValue)),
                                                               NextStatusDate = ((dr["NextStatusDate"] != DBNull.Value ? Convert.ToDateTime(dr["NextStatusDate"]) : DateTime.MinValue)),
                                                               TotBalAmt = Convert.ToDecimal(dr["TotBalAmt"]),
                                                               CollectionStatus = ((dr["CollectionStatus"] != DBNull.Value ? dr["CollectionStatus"].ToString() : "")),
                                                               StatusGroupDescription = ((dr["StatusGroupDescription"] != DBNull.Value ? dr["StatusGroupDescription"].ToString() : "")),
                                                               Location = ((dr["Location"] != DBNull.Value ? dr["Location"].ToString() : "")),
                                                               TempOwner = ((dr["TempOwner"] != DBNull.Value ? dr["TempOwner"].ToString() : "")),
                                                               Priority = ((dr["Priority"] != DBNull.Value ? dr["Priority"].ToString() : "")),
                                                               AccountNo = ((dr["AccountNo"] != DBNull.Value ? dr["AccountNo"].ToString() : "")),
                                                               ContactName = ((dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : "")),
                                                               AddressLine1 = ((dr["AddressLine1"] != DBNull.Value ? dr["AddressLine1"].ToString() : "")),
                                                               State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),
                                                               PhoneNo = ((dr["PhoneNo"] != DBNull.Value ? dr["PhoneNo"].ToString() : "")),
                                                               Fax = ((dr["Fax"] != DBNull.Value ? dr["Fax"].ToString() : "")),
                                                               SSN = ((dr["SSN"] != DBNull.Value ? dr["SSN"].ToString() : "")),
                                                               CloseDate = ((dr["CloseDate"] != DBNull.Value ? Convert.ToDateTime(dr["CloseDate"]) : DateTime.MinValue)),
                                                               FwdJudgmentDate = ((dr["FwdJudgmentDate"] != DBNull.Value ? Convert.ToDateTime(dr["FwdJudgmentDate"]) : DateTime.MinValue)),
                                                               SCReviewDate = ((dr["SCReviewDate"] != DBNull.Value ? Convert.ToDateTime(dr["SCReviewDate"]) : DateTime.MinValue)),
                                                               JudgmentDate = ((dr["JudgmentDate"] != DBNull.Value ? Convert.ToDateTime(dr["JudgmentDate"]) : DateTime.MinValue)),
                                                               EntityName1 = ((dr["EntityName1"] != DBNull.Value ? dr["EntityName1"].ToString() : "")),
                                                               TrialDate = ((dr["TrialDate"] != DBNull.Value ? Convert.ToDateTime(dr["TrialDate"]) : DateTime.MinValue)),
                                                               CollectionStatusId = ((dr["CollectionStatusId"] != DBNull.Value ? Convert.ToInt32( dr["CollectionStatusId"]) : 0)),
                                                               TempOwnerId = ((dr["TempOwnerId"] != DBNull.Value ? Convert.ToInt32(dr["TempOwnerId"]) : 0)),
                                                               LastPaymentDate = ((dr["LastPaymentDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastPaymentDate"]) : DateTime.MinValue)),
                                                               TotAsgAmt = ((dr["TotAsgAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotAsgAmt"]) : 0)),
                                                               TotCollFeeAmt = ((dr["TotCollFeeAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotCollFeeAmt"]) : 0)),
                                                               TotIntAmt = ((dr["TotIntAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotIntAmt"]) : 0)),
                                                               TotPmtAmt = ((dr["TotPmtAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotPmtAmt"]) : 0)),
                                                               FwdCourtDate = ((dr["FwdCourtDate"] != DBNull.Value ? Convert.ToDateTime(dr["FwdCourtDate"]) : DateTime.MinValue)),
                                                               PostalCode = ((dr["PostalCode"] != DBNull.Value ? dr["PostalCode"].ToString() : "")),
                                                               ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
                                                               City = ((dr["City"] != DBNull.Value ? dr["City"].ToString() : "")),
                                                               //State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),

                                                              
                                                           }).ToList();
                return AccountList;
            }
            catch (Exception ex)
            {
                return (new List<ADvwAccountFilterList>());
            }
        }

        public static List<ADvwAccountFilterList> GetFilteredList(Adapters.Collections.Collectors.WorkCard.Filter FilterObj)
        {
            try
            {
                //cview_Accounts_GetAccountFilterList sp_obj = new cview_Accounts_GetAccountFilterList();
                cview_Accounts_GetAccountFilterList_Web sp_obj = new cview_Accounts_GetAccountFilterList_Web();
                //sp_obj.DebtAccountId = "-1";
                sp_obj.DebtAccountId =null;
                #region Fiter Options
                if (FilterObj.ServiceCode != null && FilterObj.ServiceCode.Length>0)
                {
                    sp_obj.SERVICECODE = "%" + FilterObj.ServiceCode.Trim() + "%";
                    sp_obj.DebtAccountId = "";
                }
                if (FilterObj.DebtAccountId != null && FilterObj.DebtAccountId.Length > 0)
                {
                    //sp_obj.DebtAccountId = "%" + FilterObj.DebtAccountId.Trim() + "%";
                    sp_obj.DebtAccountId =  FilterObj.DebtAccountId.Trim();
                }
                if (FilterObj.AccountNo != null && FilterObj.AccountNo.Length > 0)
                {
                    sp_obj.ACCOUNTNO = "%" + FilterObj.AccountNo.Trim() + "%";
                    sp_obj.DebtAccountId = "";
                }
                if (FilterObj.DebtorName != null && FilterObj.DebtorName.Length > 0)
                {
                    sp_obj.DebtorName = "%" + FilterObj.DebtorName.Trim() + "%";
                    sp_obj.DebtAccountId = "";
                }
                if (FilterObj.ContactName != null && FilterObj.ContactName.Length > 0)
                {
                    sp_obj.ContactName = "%" + FilterObj.ContactName.Trim() + "%";
                    sp_obj.DebtAccountId = "";
                    sp_obj.IsFetchByAddr = 1;
                }
                if (FilterObj.DebtorAddress != null && FilterObj.DebtorAddress.Length > 0)
                {
                    sp_obj.ADDRESSLINE1 = "%" + FilterObj.DebtorAddress.Trim() + "%";
                    sp_obj.DebtAccountId = "";
                    sp_obj.IsFetchByAddr = 1;
                }
                if (FilterObj.DebtorState != null && FilterObj.DebtorState.Length > 0)
                {
                    sp_obj.STATE = FilterObj.DebtorState.Trim();
                    sp_obj.DebtAccountId = "";
                    sp_obj.IsFetchByAddr = 1;
                }
                if (FilterObj.DebtorPhoneNo != null && FilterObj.DebtorPhoneNo.Length > 0)
                {
                    sp_obj.PHONENO = "%" + FilterObj.DebtorPhoneNo.Trim() + "%";
                    sp_obj.DebtAccountId = "";
                    sp_obj.IsFetchByAddr = 1;
                }
                if (FilterObj.DebtorFaxNo != null && FilterObj.DebtorFaxNo.Length > 0)
                {
                    sp_obj.FAX = "%" + FilterObj.DebtorFaxNo.Trim() + "%";
                    sp_obj.DebtAccountId = "";
                    sp_obj.IsFetchByAddr = 1;
                }
                if (FilterObj.TaxId != null && FilterObj.TaxId.Length > 0)
                {
                    sp_obj.TaxId = "%" + FilterObj.TaxId.Trim() + "%";
                    sp_obj.DebtAccountId = "";
                    sp_obj.IsFetchByAddr = 1;
                }
                if (FilterObj.Priority != null && FilterObj.Priority.Length > 0)
                {
                    sp_obj.Priority = "%" + FilterObj.Priority.Trim() + "%";
                    sp_obj.DebtAccountId = "";
                }
                if (FilterObj.CaseNum != null && FilterObj.CaseNum.Length > 0)
                {
                    sp_obj.CaseNum = "%" + FilterObj.CaseNum.Trim() + "%";
                    sp_obj.DebtAccountId = "";
                }
                if (FilterObj.EntityName1 != null && FilterObj.EntityName1.Length > 0)
                {
                    sp_obj.EntityName = "%" + FilterObj.EntityName1.Trim() + "%";
                    sp_obj.DebtAccountId = "";
                }
                if (FilterObj.DebtorBKName != null && FilterObj.DebtorBKName.Length > 0)
                {
                    sp_obj.DebtorBKName = "%" + FilterObj.DebtorBKName.Trim() + "%";
                    sp_obj.DebtAccountId = "";
                }
                if(FilterObj.chkStatus== true)
                {
                    sp_obj.CollectionStatusId = Convert.ToInt32(FilterObj.cboStatusCode);
                    sp_obj.DebtAccountId = "";
                }
                if (FilterObj.chkStatusType == true)
                {
                    sp_obj.StatusGroupId = Convert.ToInt32(FilterObj.cboStatusType);
                    sp_obj.DebtAccountId = "";
                }
                if (FilterObj.chkDesk == true)
                {
                    sp_obj.LocationId = Convert.ToInt32(FilterObj.cboDesk);
                    sp_obj.DebtAccountId = "";
                }
                if (FilterObj.chkToDesk == true)
                {
                    sp_obj.TempDeskId = Convert.ToInt32(FilterObj.cboToDesk);
                    sp_obj.DebtAccountId = "";
                }
                if (FilterObj.chkBalance == true)
                {
                    sp_obj.BalAmtFrom = FilterObj.BalanceFrom;
                    sp_obj.BalAmtTo = FilterObj.BalanceTo;
                    sp_obj.DebtAccountId = "";
                }
                if (FilterObj.chkAssigned == true)
                {
                    sp_obj.ReferalDateFrom = FilterObj.ReferalDateFrom.ToString("yyyy-MM-dd");
                    sp_obj.ReferalDateTo = FilterObj.ReferalDateTo.ToString("yyyy-MM-dd");
                    sp_obj.DebtAccountId = "";
                }
                if (FilterObj.chkReview == true)
                {
                    sp_obj.NextContactDateFrom = FilterObj.NextContactDateFrom.ToString("yyyy-MM-dd");
                    sp_obj.NextContactDateTo = FilterObj.NextContactDateTo.ToString("yyyy-MM-dd"); 
                    sp_obj.DebtAccountId = "";
                }
                if (FilterObj.chkJudgmentDate == true)
                {
                    sp_obj.FWDJudgmentDateFrom = FilterObj.FWDJudgmentDateFrom.ToString("yyyy-MM-dd"); 
                    sp_obj.FWDJudgmentDateTo = FilterObj.FWDJudgmentDateTo.ToString("yyyy-MM-dd"); 
                    sp_obj.DebtAccountId = "";
                }
                if (FilterObj.chkStatusDate == true)
                {
                    sp_obj.NextStatusDate = FilterObj.NextStatusDate.ToString("yyyy-MM-dd"); 
                    sp_obj.DebtAccountId = "";
                }
                if (FilterObj.chkSCReview == true)
                {
                    sp_obj.SCReviewDateFrom = FilterObj.SCReviewDateFrom.ToString("yyyy-MM-dd");
                    sp_obj.SCReviewDateTo = FilterObj.SCReviewDateTo.ToString("yyyy-MM-dd");
                    sp_obj.DebtAccountId = "";
                }
                if (FilterObj.chkSCJudgmentDate == true)
                {
                    sp_obj.SCJudgmentDateFrom = FilterObj.SCJudgmentDateFrom.ToString("yyyy-MM-dd"); 
                    sp_obj.SCJudgmentDateTo = FilterObj.SCJudgmentDateTo.ToString("yyyy-MM-dd"); 
                    sp_obj.DebtAccountId = "";
                }
                if (FilterObj.chkTrial == true)
                {
                    sp_obj.TrialDateTo = FilterObj.NextTrialDateTo.ToString("yyyy-MM-dd");
                    sp_obj.TrialDateFrom = FilterObj.NextTrialDateFrom.ToString("yyyy-MM-dd");
                    sp_obj.DebtAccountId = "";
                }
                if (FilterObj.chkFWDCourtDate == true)
                {
                    sp_obj.FWDCourtDateFrom = FilterObj.FWDCourtDateFrom.ToString("yyyy-MM-dd"); 
                    sp_obj.FWDCourtDateTo = FilterObj.FWDCourtDateTo.ToString("yyyy-MM-dd"); 
                    sp_obj.DebtAccountId = "";
                }
                if (FilterObj.ChkAtty == true)
                {
                    sp_obj.AttyId = Convert.ToInt32( FilterObj.cboAttyId);
                    sp_obj.DebtAccountId = "";
                }
                if (FilterObj.chkCourt == true)
                {
                    sp_obj.CourtId = Convert.ToInt32(FilterObj.cboCourtId);
                    sp_obj.DebtAccountId = "";
                }
                if (FilterObj.chkServer == true)
                {
                    sp_obj.ProcessServerId = Convert.ToInt32(FilterObj.cboProcessServerId);
                    sp_obj.DebtAccountId = "";
                }
                if (FilterObj.AttorneyRefNumber != null && FilterObj.AttorneyRefNumber.Length > 0)
                {
                    sp_obj.AttorneyRefNumber = "%" + FilterObj.AttorneyRefNumber.Trim() + "%";
                    sp_obj.DebtAccountId = "";
                }
                if (FilterObj.CourtMemo != null && FilterObj.CourtMemo.Length > 0)
                {
                    sp_obj.CourtMemo = "%" + FilterObj.CourtMemo.Trim() + "%";
                    sp_obj.DebtAccountId = "";
                }
                #endregion
                //DBO.GetTableView(mysearchinfo)
                var EntityList = DBO.Provider.GetTableView(sp_obj);
                DataTable dt = EntityList.ToTable();

                return (GetList(dt));
            }
            catch (Exception ex)
            {
                return (new List<ADvwAccountFilterList>());
            }
        }

        public static List<ADvwAccountFilterList> GetMatchedList(string AccountId)
        {
            try
            {
                cview_Accounts_GetAccountListMatches sp_obj = new cview_Accounts_GetAccountListMatches();
                sp_obj.AccountId = AccountId;

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sp_obj);
                DataTable dt = EntityList.ToTable();

                return (GetList(dt));
            }
            catch (Exception ex)
            {
                return (new List<ADvwAccountFilterList>());
            }
        }

        public static DataTable GetMatchedListByOrder(string AccountId, string Order)
        {
            try
            {
                cview_Accounts_ShowAccountByOrder sp_obj = new cview_Accounts_ShowAccountByOrder();
                sp_obj.DebtAccountId = AccountId;
                sp_obj.Order = Order;

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sp_obj);
                DataTable dt = EntityList.ToTable();

               

                return dt;
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }


        public static DataTable GetCommercialCreditReportList(string AccountId)
        {
            try
            {
                string sql = "Select * from DebtAccountCreditReport where ReportType = 'C' and DebtAccountId  = " + AccountId;

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new  DataTable());
            }
        }
        #endregion

        #region Local Methods
        public static List<ADvwAccountFilterList> GetList(DataTable dt)
        {
            List<ADvwAccountFilterList> AccountList = (from DataRow dr in dt.Rows
                                                       select new ADvwAccountFilterList()
                                                       {
                                                           ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                                           DebtorName = ((dr["DebtorName"] != DBNull.Value ? dr["DebtorName"].ToString() : "")),
                                                           DebtAccountId = Convert.ToInt32(dr["DebtAccountId"]),
                                                           NextContactDate = ((dr["NextContactDate"] != DBNull.Value ? Convert.ToDateTime(dr["NextContactDate"]) : DateTime.MinValue)),
                                                           ReferalDate = ((dr["ReferalDate"] != DBNull.Value ? Convert.ToDateTime(dr["ReferalDate"]) : DateTime.MinValue)),
                                                           NextStatusDate = ((dr["NextStatusDate"] != DBNull.Value ? Convert.ToDateTime(dr["NextStatusDate"]) : DateTime.MinValue)),
                                                           TotBalAmt = Convert.ToDecimal(dr["TotBalAmt"]),
                                                           CollectionStatus = ((dr["CollectionStatus"] != DBNull.Value ? dr["CollectionStatus"].ToString() : "")),
                                                           StatusGroupDescription = ((dr["StatusGroupDescription"] != DBNull.Value ? dr["StatusGroupDescription"].ToString() : "")),
                                                           Location = ((dr["Location"] != DBNull.Value ? dr["Location"].ToString() : "")),
                                                           TempOwner = ((dr["TempOwner"] != DBNull.Value ? dr["TempOwner"].ToString() : "")),
                                                           Priority = ((dr["Priority"] != DBNull.Value ? dr["Priority"].ToString() : "")),
                                                           AccountNo = ((dr["AccountNo"] != DBNull.Value ? dr["AccountNo"].ToString() : "")),
                                                           ContactName = ((dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : "")),
                                                           AddressLine1 = ((dr["AddressLine1"] != DBNull.Value ? dr["AddressLine1"].ToString() : "")),
                                                           State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),
                                                           PhoneNo = ((dr["PhoneNo"] != DBNull.Value ? dr["PhoneNo"].ToString() : "")),
                                                           Fax = ((dr["Fax"] != DBNull.Value ? dr["Fax"].ToString() : "")),
                                                           SSN = ((dr["SSN"] != DBNull.Value ? dr["SSN"].ToString() : "")),
                                                           CloseDate = ((dr["CloseDate"] != DBNull.Value ? Convert.ToDateTime(dr["CloseDate"]) : DateTime.MinValue)),
                                                           FwdJudgmentDate = ((dr["FwdJudgmentDate"] != DBNull.Value ? Convert.ToDateTime(dr["FwdJudgmentDate"]) : DateTime.MinValue)),
                                                           SCReviewDate = ((dr["SCReviewDate"] != DBNull.Value ? Convert.ToDateTime(dr["SCReviewDate"]) : DateTime.MinValue)),
                                                           JudgmentDate = ((dr["JudgmentDate"] != DBNull.Value ? Convert.ToDateTime(dr["JudgmentDate"]) : DateTime.MinValue)),
                                                           EntityName1 = ((dr["EntityName1"] != DBNull.Value ? dr["EntityName1"].ToString() : "")),
                                                           TrialDate = ((dr["TrialDate"] != DBNull.Value ? Convert.ToDateTime(dr["TrialDate"]) : DateTime.MinValue)),
                                                           CollectionStatusId = ((dr["CollectionStatusId"] != DBNull.Value ? Convert.ToInt32(dr["CollectionStatusId"]) : 0)),
                                                           TempOwnerId = ((dr["TempOwnerId"] != DBNull.Value ? Convert.ToInt32(dr["TempOwnerId"]) : 0)),
                                                           LastPaymentDate = ((dr["LastPaymentDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastPaymentDate"]) : DateTime.MinValue)),
                                                           TotAsgAmt = ((dr["TotAsgAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotAsgAmt"]) : 0)),
                                                           TotCollFeeAmt = ((dr["TotCollFeeAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotCollFeeAmt"]) : 0)),
                                                           TotIntAmt = ((dr["TotIntAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotIntAmt"]) : 0)),
                                                           TotPmtAmt = ((dr["TotPmtAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotPmtAmt"]) : 0)),
                                                           FwdCourtDate = ((dr["FwdCourtDate"] != DBNull.Value ? Convert.ToDateTime(dr["FwdCourtDate"]) : DateTime.MinValue)),
                                                           PostalCode = ((dr["PostalCode"] != DBNull.Value ? dr["PostalCode"].ToString() : "")),
                                                           City = ((dr["City"] != DBNull.Value ? dr["City"].ToString() : "")),
                                                           ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
                                                       }).ToList();
            return AccountList;
        }


        public static List<ADvwAccountFilterList> GetSortedList(string SortBy)
        {
            try
            {
                TableView EntityList;
                //var sp_obj = new cview_Accounts_GetAccountFilterList();
                var sp_obj = new cview_Accounts_GetAccountFilterList_Web();
                sp_obj.DebtAccountId = null;
                sp_obj.SortColType = SortBy;
                EntityList = DBO.Provider.GetTableView(sp_obj);
                DataTable dt = EntityList.ToTable();
                List<ADvwAccountFilterList> AccountList = (from DataRow dr in dt.Rows
                                                           select new ADvwAccountFilterList()
                                                           {
                                                               ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                                               DebtorName = ((dr["DebtorName"] != DBNull.Value ? dr["DebtorName"].ToString() : "")),
                                                               DebtAccountId = Convert.ToInt32(dr["DebtAccountId"]),
                                                               NextContactDate = ((dr["NextContactDate"] != DBNull.Value ? Convert.ToDateTime(dr["NextContactDate"]) : DateTime.MinValue)),
                                                               ReferalDate = ((dr["ReferalDate"] != DBNull.Value ? Convert.ToDateTime(dr["ReferalDate"]) : DateTime.MinValue)),
                                                               NextStatusDate = ((dr["NextStatusDate"] != DBNull.Value ? Convert.ToDateTime(dr["NextStatusDate"]) : DateTime.MinValue)),
                                                               TotBalAmt = Convert.ToDecimal(dr["TotBalAmt"]),
                                                               CollectionStatus = ((dr["CollectionStatus"] != DBNull.Value ? dr["CollectionStatus"].ToString() : "")),
                                                               StatusGroupDescription = ((dr["StatusGroupDescription"] != DBNull.Value ? dr["StatusGroupDescription"].ToString() : "")),
                                                               Location = ((dr["Location"] != DBNull.Value ? dr["Location"].ToString() : "")),
                                                               TempOwner = ((dr["TempOwner"] != DBNull.Value ? dr["TempOwner"].ToString() : "")),
                                                               Priority = ((dr["Priority"] != DBNull.Value ? dr["Priority"].ToString() : "")),
                                                               AccountNo = ((dr["AccountNo"] != DBNull.Value ? dr["AccountNo"].ToString() : "")),
                                                               ContactName = ((dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : "")),
                                                               AddressLine1 = ((dr["AddressLine1"] != DBNull.Value ? dr["AddressLine1"].ToString() : "")),
                                                               State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),
                                                               PhoneNo = ((dr["PhoneNo"] != DBNull.Value ? dr["PhoneNo"].ToString() : "")),
                                                               Fax = ((dr["Fax"] != DBNull.Value ? dr["Fax"].ToString() : "")),
                                                               SSN = ((dr["SSN"] != DBNull.Value ? dr["SSN"].ToString() : "")),
                                                               CloseDate = ((dr["CloseDate"] != DBNull.Value ? Convert.ToDateTime(dr["CloseDate"]) : DateTime.MinValue)),
                                                               FwdJudgmentDate = ((dr["FwdJudgmentDate"] != DBNull.Value ? Convert.ToDateTime(dr["FwdJudgmentDate"]) : DateTime.MinValue)),
                                                               SCReviewDate = ((dr["SCReviewDate"] != DBNull.Value ? Convert.ToDateTime(dr["SCReviewDate"]) : DateTime.MinValue)),
                                                               JudgmentDate = ((dr["JudgmentDate"] != DBNull.Value ? Convert.ToDateTime(dr["JudgmentDate"]) : DateTime.MinValue)),
                                                               EntityName1 = ((dr["EntityName1"] != DBNull.Value ? dr["EntityName1"].ToString() : "")),
                                                               TrialDate = ((dr["TrialDate"] != DBNull.Value ? Convert.ToDateTime(dr["TrialDate"]) : DateTime.MinValue)),
                                                               CollectionStatusId = ((dr["CollectionStatusId"] != DBNull.Value ? Convert.ToInt32(dr["CollectionStatusId"]) : 0)),
                                                               TempOwnerId = ((dr["TempOwnerId"] != DBNull.Value ? Convert.ToInt32(dr["TempOwnerId"]) : 0)),
                                                               LastPaymentDate = ((dr["LastPaymentDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastPaymentDate"]) : DateTime.MinValue)),
                                                               TotAsgAmt = ((dr["TotAsgAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotAsgAmt"]) : 0)),
                                                               TotCollFeeAmt = ((dr["TotCollFeeAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotCollFeeAmt"]) : 0)),
                                                               TotIntAmt = ((dr["TotIntAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotIntAmt"]) : 0)),
                                                               TotPmtAmt = ((dr["TotPmtAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotPmtAmt"]) : 0)),
                                                               FwdCourtDate = ((dr["FwdCourtDate"] != DBNull.Value ? Convert.ToDateTime(dr["FwdCourtDate"]) : DateTime.MinValue)),
                                                               PostalCode = ((dr["PostalCode"] != DBNull.Value ? dr["PostalCode"].ToString() : "")),
                                                               City = ((dr["City"] != DBNull.Value ? dr["City"].ToString() : "")),
                                                               ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
                                                           }).ToList();
                return AccountList;

            }
            catch (Exception ex)
            {
                return (new List<ADvwAccountFilterList>());
            }
        }

        //Print MatchReport
        public static string PrintMatchReport(MatchReportVM objPrintMatchReport)
        {
            string pdfPath = null;
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
       
                var Sp_OBJ = new cview_Processing_GetMatchList();
                Sp_OBJ.AssignDateFROM = objPrintMatchReport.FromDate.ToShortDateString();
                Sp_OBJ.AssignDateTO = objPrintMatchReport.ThruDate.ToShortDateString();

                string sFromDate, sThruDate;
                sFromDate = objPrintMatchReport.FromDate.ToShortDateString();
                sThruDate = objPrintMatchReport.ThruDate.ToShortDateString();

                 CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;
                if (objPrintMatchReport.RbtPrintOption == "Send To PDF")
                {
                    mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                }

                DataSet myds = DBO.Provider.GetDataSet(Sp_OBJ);

                TableView myview = DBO.Provider.GetTableView(Sp_OBJ);
 
                DataTable mytbl = myds.Tables[0];

                if (myds.Tables[0].Rows.Count > 0)
                {
                    pdfPath = Globals.RenderC1Report(user, mytbl, "CollectViewReports.xml", "MatchReport", mymode,"", sFromDate, sThruDate);
                }
               
            }

            catch (Exception ex)
            {
                return "";
            }
            return pdfPath;
        }

        public static List<ADvwAccountFilterList> GetAccountList(Filter FilterObj, string sortBy)
        {
            try
            {
                TableView EntityList;
                //var sp_obj = new cview_Accounts_GetAccountFilterList();
                var sp_obj = new cview_Accounts_GetAccountFilterList_Web();
                sp_obj.DebtAccountId = null;


                #region Fiter Options
                if (FilterObj != null)
                {
                    sp_obj.DebtAccountId = "-1";
                    if (FilterObj.ServiceCode != null && FilterObj.ServiceCode.Length > 0)
                    {
                        sp_obj.SERVICECODE = "%" + FilterObj.ServiceCode.Trim() + "%";
                        sp_obj.DebtAccountId = "";
                    }
                    if (FilterObj.DebtAccountId != null && FilterObj.DebtAccountId.Length > 0)
                    {
                        //sp_obj.DebtAccountId = "%" + FilterObj.DebtAccountId.Trim() + "%";
                        sp_obj.DebtAccountId = FilterObj.DebtAccountId.Trim();
                    }
                    if (FilterObj.AccountNo != null && FilterObj.AccountNo.Length > 0)
                    {
                        sp_obj.ACCOUNTNO = "%" + FilterObj.AccountNo.Trim() + "%";
                        sp_obj.DebtAccountId = "";
                    }
                    if (FilterObj.DebtorName != null && FilterObj.DebtorName.Length > 0)
                    {
                        sp_obj.DebtorName = "%" + FilterObj.DebtorName.Trim() + "%";
                        sp_obj.DebtAccountId = "";
                    }
                    if (FilterObj.ContactName != null && FilterObj.ContactName.Length > 0)
                    {
                        sp_obj.ContactName = "%" + FilterObj.ContactName.Trim() + "%";
                        sp_obj.DebtAccountId = "";
                        sp_obj.IsFetchByAddr = 1;
                    }
                    if (FilterObj.DebtorAddress != null && FilterObj.DebtorAddress.Length > 0)
                    {
                        sp_obj.ADDRESSLINE1 = "%" + FilterObj.DebtorAddress.Trim() + "%";
                        sp_obj.DebtAccountId = "";
                        sp_obj.IsFetchByAddr = 1;
                    }
                    if (FilterObj.DebtorState != null && FilterObj.DebtorState.Length > 0)
                    {
                        sp_obj.STATE = FilterObj.DebtorState.Trim();
                        sp_obj.DebtAccountId = "";
                        sp_obj.IsFetchByAddr = 1;
                    }
                    if (FilterObj.DebtorPhoneNo != null && FilterObj.DebtorPhoneNo.Length > 0)
                    {
                        sp_obj.PHONENO = "%" + FilterObj.DebtorPhoneNo.Trim() + "%";
                        sp_obj.DebtAccountId = "";
                        sp_obj.IsFetchByAddr = 1;
                    }
                    if (FilterObj.DebtorFaxNo != null && FilterObj.DebtorFaxNo.Length > 0)
                    {
                        sp_obj.FAX = "%" + FilterObj.DebtorFaxNo.Trim() + "%";
                        sp_obj.DebtAccountId = "";
                        sp_obj.IsFetchByAddr = 1;
                    }
                    if (FilterObj.TaxId != null && FilterObj.TaxId.Length > 0)
                    {
                        sp_obj.TaxId = "%" + FilterObj.TaxId.Trim() + "%";
                        sp_obj.DebtAccountId = "";
                        sp_obj.IsFetchByAddr = 1;
                    }
                    if (FilterObj.Priority != null && FilterObj.Priority.Length > 0)
                    {
                        sp_obj.Priority = "%" + FilterObj.Priority.Trim() + "%";
                        sp_obj.DebtAccountId = "";
                    }
                    if (FilterObj.CaseNum != null && FilterObj.CaseNum.Length > 0)
                    {
                        sp_obj.CaseNum = "%" + FilterObj.CaseNum.Trim() + "%";
                        sp_obj.DebtAccountId = "";
                    }
                    if (FilterObj.EntityName1 != null && FilterObj.EntityName1.Length > 0)
                    {
                        sp_obj.EntityName = "%" + FilterObj.EntityName1.Trim() + "%";
                        sp_obj.DebtAccountId = "";
                    }
                    if (FilterObj.DebtorBKName != null && FilterObj.DebtorBKName.Length > 0)
                    {
                        sp_obj.DebtorBKName = "%" + FilterObj.DebtorBKName.Trim() + "%";
                        sp_obj.DebtAccountId = "";
                    }
                    if (FilterObj.chkStatus == true)
                    {
                        sp_obj.CollectionStatusId = Convert.ToInt32(FilterObj.cboStatusCode);
                        sp_obj.DebtAccountId = "";
                    }
                    if (FilterObj.chkStatusType == true)
                    {
                        sp_obj.StatusGroupId = Convert.ToInt32(FilterObj.cboStatusType);
                        sp_obj.DebtAccountId = "";
                    }
                    if (FilterObj.chkDesk == true)
                    {
                        sp_obj.LocationId = Convert.ToInt32(FilterObj.cboDesk);
                        sp_obj.DebtAccountId = "";
                    }
                    if (FilterObj.chkToDesk == true)
                    {
                        sp_obj.TempDeskId = Convert.ToInt32(FilterObj.cboToDesk);
                        sp_obj.DebtAccountId = "";
                    }
                    if (FilterObj.chkBalance == true)
                    {
                        sp_obj.BalAmtFrom = FilterObj.BalanceFrom;
                        sp_obj.BalAmtTo = FilterObj.BalanceTo;
                        sp_obj.DebtAccountId = "";
                    }
                    if (FilterObj.chkAssigned == true)
                    {
                        sp_obj.ReferalDateFrom = FilterObj.ReferalDateFrom.ToString("yyyy-MM-dd");
                        sp_obj.ReferalDateTo = FilterObj.ReferalDateTo.ToString("yyyy-MM-dd");
                        sp_obj.DebtAccountId = "";
                    }
                    if (FilterObj.chkReview == true)
                    {
                        sp_obj.NextContactDateFrom = FilterObj.NextContactDateFrom.ToString("yyyy-MM-dd");
                        sp_obj.NextContactDateTo = FilterObj.NextContactDateTo.ToString("yyyy-MM-dd");
                        sp_obj.DebtAccountId = "";
                    }
                    if (FilterObj.chkJudgmentDate == true)
                    {
                        sp_obj.FWDJudgmentDateFrom = FilterObj.FWDJudgmentDateFrom.ToString("yyyy-MM-dd");
                        sp_obj.FWDJudgmentDateTo = FilterObj.FWDJudgmentDateTo.ToString("yyyy-MM-dd");
                        sp_obj.DebtAccountId = "";
                    }
                    if (FilterObj.chkStatusDate == true)
                    {
                        sp_obj.NextStatusDate = FilterObj.NextStatusDate.ToString("yyyy-MM-dd");
                        sp_obj.DebtAccountId = "";
                    }
                    if (FilterObj.chkSCReview == true)
                    {
                        sp_obj.SCReviewDateFrom = FilterObj.SCReviewDateFrom.ToString("yyyy-MM-dd");
                        sp_obj.SCReviewDateTo = FilterObj.SCReviewDateTo.ToString("yyyy-MM-dd");
                        sp_obj.DebtAccountId = "";
                    }
                    if (FilterObj.chkSCJudgmentDate == true)
                    {
                        sp_obj.SCJudgmentDateFrom = FilterObj.SCJudgmentDateFrom.ToString("yyyy-MM-dd");
                        sp_obj.SCJudgmentDateTo = FilterObj.SCJudgmentDateTo.ToString("yyyy-MM-dd");
                        sp_obj.DebtAccountId = "";
                    }
                    if (FilterObj.chkTrial == true)
                    {
                        sp_obj.TrialDateTo = FilterObj.NextTrialDateTo.ToString("yyyy-MM-dd");
                        sp_obj.TrialDateFrom = FilterObj.NextTrialDateFrom.ToString("yyyy-MM-dd");
                        sp_obj.DebtAccountId = "";
                    }
                    if (FilterObj.chkFWDCourtDate == true)
                    {
                        sp_obj.FWDCourtDateFrom = FilterObj.FWDCourtDateFrom.ToString("yyyy-MM-dd");
                        sp_obj.FWDCourtDateTo = FilterObj.FWDCourtDateTo.ToString("yyyy-MM-dd");
                        sp_obj.DebtAccountId = "";
                    }
                    if (FilterObj.ChkAtty == true)
                    {
                        sp_obj.AttyId = Convert.ToInt32(FilterObj.cboAttyId);
                        sp_obj.DebtAccountId = "";
                    }
                    if (FilterObj.chkCourt == true)
                    {
                        sp_obj.CourtId = Convert.ToInt32(FilterObj.cboCourtId);
                        sp_obj.DebtAccountId = "";
                    }
                    if (FilterObj.chkServer == true)
                    {
                        sp_obj.ProcessServerId = Convert.ToInt32(FilterObj.cboProcessServerId);
                        sp_obj.DebtAccountId = "";
                    }
                    if (FilterObj.AttorneyRefNumber != null && FilterObj.AttorneyRefNumber.Length > 0)
                    {
                        sp_obj.AttorneyRefNumber = "%" + FilterObj.AttorneyRefNumber.Trim() + "%";
                        sp_obj.DebtAccountId = "";
                    }
                    if (FilterObj.CourtMemo != null && FilterObj.CourtMemo.Length > 0)
                    {
                        sp_obj.CourtMemo = "%" + FilterObj.CourtMemo.Trim() + "%";
                        sp_obj.DebtAccountId = "";
                    }
                }

                if (string.IsNullOrEmpty(sortBy) == false)
                {
                    sp_obj.SortColType = sortBy;
                }

                #endregion


                EntityList = DBO.Provider.GetTableView(sp_obj);
                DataTable dt = EntityList.ToTable();
                List<ADvwAccountFilterList> AccountList = (from DataRow dr in dt.Rows
                                                           select new ADvwAccountFilterList()
                                                           {
                                                               ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                                               DebtorName = ((dr["DebtorName"] != DBNull.Value ? dr["DebtorName"].ToString() : "")),
                                                               DebtAccountId = Convert.ToInt32(dr["DebtAccountId"]),
                                                               NextContactDate = ((dr["NextContactDate"] != DBNull.Value ? Convert.ToDateTime(dr["NextContactDate"]) : DateTime.MinValue)),
                                                               ReferalDate = ((dr["ReferalDate"] != DBNull.Value ? Convert.ToDateTime(dr["ReferalDate"]) : DateTime.MinValue)),
                                                               NextStatusDate = ((dr["NextStatusDate"] != DBNull.Value ? Convert.ToDateTime(dr["NextStatusDate"]) : DateTime.MinValue)),
                                                               TotBalAmt = Convert.ToDecimal(dr["TotBalAmt"]),
                                                               CollectionStatus = ((dr["CollectionStatus"] != DBNull.Value ? dr["CollectionStatus"].ToString() : "")),
                                                               StatusGroupDescription = ((dr["StatusGroupDescription"] != DBNull.Value ? dr["StatusGroupDescription"].ToString() : "")),
                                                               Location = ((dr["Location"] != DBNull.Value ? dr["Location"].ToString() : "")),
                                                               TempOwner = ((dr["TempOwner"] != DBNull.Value ? dr["TempOwner"].ToString() : "")),
                                                               Priority = ((dr["Priority"] != DBNull.Value ? dr["Priority"].ToString() : "")),
                                                               AccountNo = ((dr["AccountNo"] != DBNull.Value ? dr["AccountNo"].ToString() : "")),
                                                               ContactName = ((dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : "")),
                                                               AddressLine1 = ((dr["AddressLine1"] != DBNull.Value ? dr["AddressLine1"].ToString() : "")),
                                                               State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),
                                                               PhoneNo = ((dr["PhoneNo"] != DBNull.Value ? dr["PhoneNo"].ToString() : "")),
                                                               Fax = ((dr["Fax"] != DBNull.Value ? dr["Fax"].ToString() : "")),
                                                               SSN = ((dr["SSN"] != DBNull.Value ? dr["SSN"].ToString() : "")),
                                                               CloseDate = ((dr["CloseDate"] != DBNull.Value ? Convert.ToDateTime(dr["CloseDate"]) : DateTime.MinValue)),
                                                               FwdJudgmentDate = ((dr["FwdJudgmentDate"] != DBNull.Value ? Convert.ToDateTime(dr["FwdJudgmentDate"]) : DateTime.MinValue)),
                                                               SCReviewDate = ((dr["SCReviewDate"] != DBNull.Value ? Convert.ToDateTime(dr["SCReviewDate"]) : DateTime.MinValue)),
                                                               JudgmentDate = ((dr["JudgmentDate"] != DBNull.Value ? Convert.ToDateTime(dr["JudgmentDate"]) : DateTime.MinValue)),
                                                               EntityName1 = ((dr["EntityName1"] != DBNull.Value ? dr["EntityName1"].ToString() : "")),
                                                               TrialDate = ((dr["TrialDate"] != DBNull.Value ? Convert.ToDateTime(dr["TrialDate"]) : DateTime.MinValue)),
                                                               CollectionStatusId = ((dr["CollectionStatusId"] != DBNull.Value ? Convert.ToInt32(dr["CollectionStatusId"]) : 0)),
                                                               TempOwnerId = ((dr["TempOwnerId"] != DBNull.Value ? Convert.ToInt32(dr["TempOwnerId"]) : 0)),
                                                               LastPaymentDate = ((dr["LastPaymentDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastPaymentDate"]) : DateTime.MinValue)),
                                                               TotAsgAmt = ((dr["TotAsgAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotAsgAmt"]) : 0)),
                                                               TotCollFeeAmt = ((dr["TotCollFeeAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotCollFeeAmt"]) : 0)),
                                                               TotIntAmt = ((dr["TotIntAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotIntAmt"]) : 0)),
                                                               TotPmtAmt = ((dr["TotPmtAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotPmtAmt"]) : 0)),
                                                               FwdCourtDate = ((dr["FwdCourtDate"] != DBNull.Value ? Convert.ToDateTime(dr["FwdCourtDate"]) : DateTime.MinValue)),
                                                               PostalCode = ((dr["PostalCode"] != DBNull.Value ? dr["PostalCode"].ToString() : "")),
                                                               ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
                                                               City = ((dr["City"] != DBNull.Value ? dr["City"].ToString() : "")),
                                                               //State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),
                                                               //code Added by pooja on 07/17/20
                                                             
                                                               //PreviousValue = Convert.ToInt32(dr["PreviousValue"]),
                                                               //NextValue = Convert.ToInt32(dr["NextValue"]),
                                                           }).ToList();
                return AccountList;
            }
            catch (Exception ex)
            {
                return (new List<ADvwAccountFilterList>());
            }
        }
        #endregion
    }
}
