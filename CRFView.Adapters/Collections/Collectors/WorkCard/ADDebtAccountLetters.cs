﻿using CRF.BLL;
using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using CRFView.Adapters.Collections.DayEnd.PrintLetters.ViewModels;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using Ionic.Zip;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace CRFView.Adapters
{
    public class ADDebtAccountLetters
    {
        #region Properties
        public int PKID { get; set; }
        public int DebtAccountId { get; set; }
        public int UserId { get; set; }
        public int LetterId { get; set; }
        public string LetterCode { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DatePrinted { get; set; }
        public string DocumentDescription { get; set; }
        public string DocumentPath { get; set; }
        public int ContractId { get; set; }
        public bool IsLetterSeries { get; set; }
        public bool IsProcessed { get; set; }
        public bool IsInvoiced { get; set; }
        public DateTime DateInvoiced { get; set; }
        public int SeqNO { get; set; }
        public  decimal Rate { get; set; }
        public int AddressId { get; set; }
        public string UserCode { get; set; }
        public string AddressType { get; set; }
        public   string MailedTo { get; set; }
        public int ClientFinDocId { get; set; }
        public int AddressTypeId { get; set; }
        public bool IsCustom { get; set; }
        public int CustomTextId { get; set; }
        public string CustomText { get; set; }
        public string CustomLetterCode { get; set; }
        public bool IsEmailed { get; set; }
        public string CustomLetterType { get; set; }
        public bool IsDayEndPrint { get; set; }
        public bool IsReprintd { get; set; }
        public DateTime RunFromDate { get; set; }
        public DateTime RunThruDate { get; set; }
        public int LocationId { get; set; }
        public bool IsSCInvoiceLetter { get; set; }
        public int SCInvoiceNum { get; set; }
        #endregion

        #region CRUD

        public static List<ADDebtAccountLetters> GetDebtAccountLettersList(DataTable dt)
        {
            try
            {
                List<ADDebtAccountLetters> LettersInfo = (from DataRow dr in dt.Rows
                                                            select new ADDebtAccountLetters()
                                                            {
                                                                PKID = ((dr["PKID"] != DBNull.Value ? Convert.ToInt32(dr["PKID"].ToString()) : 0)),
                                                                DebtAccountId = ((dr["DebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAccountId"].ToString()) : 0)),
                                                                UserId = ((dr["UserId"] != DBNull.Value ? Convert.ToInt32(dr["UserId"].ToString()) : 0)),
                                                                LetterId = ((dr["LetterId"] != DBNull.Value ? Convert.ToInt32(dr["LetterId"].ToString()) : 0)),
                                                                LetterCode = ((dr["LetterCode"] != DBNull.Value ? dr["LetterCode"].ToString() : "")),
                                                                DateCreated = ((dr["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dr["DateCreated"]) : DateTime.MinValue)),
                                                                DatePrinted = ((dr["DatePrinted"] != DBNull.Value ? Convert.ToDateTime(dr["DatePrinted"]) : DateTime.MinValue)),
                                                                DocumentDescription = ((dr["DocumentDescription"] != DBNull.Value ? dr["DocumentDescription"].ToString() : "")),
                                                                DocumentPath = ((dr["DocumentPath"] != DBNull.Value ? dr["DocumentPath"].ToString() : "")),
                                                                ContractId = ((dr["ContractId"] != DBNull.Value ? Convert.ToInt32(dr["ContractId"].ToString()) : 0)),
                                                                IsLetterSeries = ((dr["IsLetterSeries"] != DBNull.Value ? Convert.ToBoolean(dr["IsLetterSeries"]) : false)),
                                                                IsProcessed = ((dr["IsProcessed"] != DBNull.Value ? Convert.ToBoolean(dr["IsProcessed"]) : false)),
                                                                IsInvoiced = ((dr["IsInvoiced"] != DBNull.Value ? Convert.ToBoolean(dr["IsInvoiced"]) : false)),
                                                                DateInvoiced = ((dr["DateInvoiced"] != DBNull.Value ? Convert.ToDateTime(dr["DateInvoiced"]) : DateTime.MinValue)),
                                                                SeqNO = ((dr["SeqNO"] != DBNull.Value ? Convert.ToInt32(dr["SeqNO"].ToString()) : 0)),
                                                                Rate = ((dr["Rate"] != DBNull.Value ? Convert.ToDecimal(dr["Rate"].ToString()) : 0)),
                                                                AddressId = ((dr["AddressId"] != DBNull.Value ? Convert.ToInt32(dr["AddressId"].ToString()) : 0)),
                                                                UserCode = ((dr["UserCode"] != DBNull.Value ? dr["UserCode"].ToString() : "")),
                                                                AddressType = ((dr["AddressType"] != DBNull.Value ? dr["AddressType"].ToString() : "")),
                                                                MailedTo = ((dr["MailedTo"] != DBNull.Value ? dr["MailedTo"].ToString() : "")),
                                                                ClientFinDocId = ((dr["ClientFinDocId"] != DBNull.Value ? Convert.ToInt32(dr["ClientFinDocId"].ToString()) : 0)),
                                                                AddressTypeId = ((dr["AddressTypeId"] != DBNull.Value ? Convert.ToInt32(dr["AddressTypeId"].ToString()) : 0)),
                                                                IsCustom = ((dr["IsCustom"] != DBNull.Value ? Convert.ToBoolean(dr["IsCustom"]) : false)),
                                                                CustomTextId = ((dr["CustomTextId"] != DBNull.Value ? Convert.ToInt32(dr["CustomTextId"].ToString()) : 0)),
                                                                CustomText = ((dr["CustomText"] != DBNull.Value ? dr["CustomText"].ToString() : "")),
                                                                CustomLetterCode = ((dr["CustomLetterCode"] != DBNull.Value ? dr["CustomLetterCode"].ToString() : "")),
                                                                IsEmailed = ((dr["IsEmailed"] != DBNull.Value ? Convert.ToBoolean(dr["IsEmailed"]) : false)),
                                                                CustomLetterType = ((dr["CustomLetterType"] != DBNull.Value ? dr["CustomLetterType"].ToString() : "")),
                                                                IsDayEndPrint = ((dr["IsDayEndPrint"] != DBNull.Value ? Convert.ToBoolean(dr["IsDayEndPrint"]) : false)),
                                                                IsReprintd = ((dr["IsReprintd"] != DBNull.Value ? Convert.ToBoolean(dr["IsReprintd"]) : false)),
                                                                RunFromDate = ((dr["RunFromDate"] != DBNull.Value ? Convert.ToDateTime(dr["RunFromDate"]) : DateTime.MinValue)),
                                                                RunThruDate = ((dr["RunThruDate"] != DBNull.Value ? Convert.ToDateTime(dr["RunThruDate"]) : DateTime.MinValue)),
                                                                LocationId = ((dr["LocationId"] != DBNull.Value ? Convert.ToInt32(dr["LocationId"].ToString()) : 0)),
                                                                IsSCInvoiceLetter = ((dr["IsSCInvoiceLetter"] != DBNull.Value ? Convert.ToBoolean(dr["IsSCInvoiceLetter"]) : false)),
                                                                SCInvoiceNum = ((dr["SCInvoiceNum"] != DBNull.Value ? Convert.ToInt32(dr["SCInvoiceNum"].ToString()) : 0)),
    }).ToList();
                return LettersInfo;
            }
            catch (Exception ex)
            {
                return (new List<ADDebtAccountLetters>());
            }
        }

        //Read Details from database
        public static ADDebtAccountLetters ReadDebtAccountLetters(int id)
        {
            DebtAccountLetters myentity = new DebtAccountLetters();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (DebtAccountLetters)myentityItable;
            AutoMapper.Mapper.CreateMap<DebtAccountLetters, ADDebtAccountLetters>();
            return (AutoMapper.Mapper.Map<ADDebtAccountLetters>(myentity));
        }

        public static int SaveDebtAccountLetters(ADDebtAccountLetters objUpdatedADDebtAccountLetters)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADDebtAccountLetters, DebtAccountLetters>();
                ITable tblDebtAccountLetters;
                DebtAccountLetters objDebtAccountLetters;

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                if (objUpdatedADDebtAccountLetters.PKID != 0)
                {
                    ADDebtAccountLetters objADDebtAccountLettersOriginal = ReadDebtAccountLetters(Convert.ToInt32(objUpdatedADDebtAccountLetters.PKID));

                    if (objUpdatedADDebtAccountLetters.DebtAccountId == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLettersOriginal.DebtAccountId = objUpdatedADDebtAccountLetters.DebtAccountId;
                    }

                    if (objUpdatedADDebtAccountLetters.LetterId == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLettersOriginal.LetterId = objUpdatedADDebtAccountLetters.LetterId;
                    }

                    if (objUpdatedADDebtAccountLetters.LetterCode == "" || objUpdatedADDebtAccountLetters.LetterCode == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountLettersOriginal.LetterCode = objUpdatedADDebtAccountLetters.LetterCode.Trim();
                    }

                    if (objUpdatedADDebtAccountLetters.ContractId == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLettersOriginal.ContractId = objUpdatedADDebtAccountLetters.ContractId;
                    }

                    //objADDebtAccountLettersOriginal.DateCreated = DateTime.Now;

                    objADDebtAccountLettersOriginal.IsSCInvoiceLetter = objADDebtAccountLettersOriginal.IsSCInvoiceLetter;

                    if (objUpdatedADDebtAccountLetters.SCInvoiceNum == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLettersOriginal.SCInvoiceNum = objUpdatedADDebtAccountLetters.SCInvoiceNum;
                    }

                    if (objUpdatedADDebtAccountLetters.AddressId == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLettersOriginal.AddressId = objUpdatedADDebtAccountLetters.AddressId;
                    }

                    if (objUpdatedADDebtAccountLetters.AddressTypeId == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLettersOriginal.AddressTypeId = objUpdatedADDebtAccountLetters.AddressTypeId;
                    }

                    //if (objUpdatedADDebtAccountSmallClaims.AbstractDate == DateTime.MinValue)
                    //{
                    //}
                    //else
                    //{
                    //    objADDebtAccountSmallClaimsOriginal.AbstractDate = objUpdatedADDebtAccountSmallClaims.AbstractDate;
                    //}




                    objADDebtAccountLettersOriginal.UserId = user.Id;
                    objADDebtAccountLettersOriginal.UserCode = user.UserName;

                    objDebtAccountLetters = AutoMapper.Mapper.Map<DebtAccountLetters>(objADDebtAccountLettersOriginal);
                    tblDebtAccountLetters = (ITable)objDebtAccountLetters;
                    DBO.Provider.Update(ref tblDebtAccountLetters);
                }
                else
                {
                    objUpdatedADDebtAccountLetters.DateCreated = DateTime.Now;

                    objUpdatedADDebtAccountLetters.UserId = user.Id;
                    objUpdatedADDebtAccountLetters.UserCode = user.UserName;

                    objDebtAccountLetters = AutoMapper.Mapper.Map<DebtAccountLetters>(objUpdatedADDebtAccountLetters);
                    tblDebtAccountLetters = (ITable)objDebtAccountLetters;
                    DBO.Provider.Create(ref tblDebtAccountLetters);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int SaveDebtAccountLetters_StatusReport(ADDebtAccountLetters objUpdatedADDebtAccountLetters)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADDebtAccountLetters, DebtAccountLetters>();
                ITable tblDebtAccountLetters;
                DebtAccountLetters objDebtAccountLetters;

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                if (objUpdatedADDebtAccountLetters.PKID != 0)
                {
                    ADDebtAccountLetters objADDebtAccountLettersOriginal = ReadDebtAccountLetters(Convert.ToInt32(objUpdatedADDebtAccountLetters.PKID));

                    if (objUpdatedADDebtAccountLetters.DebtAccountId == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLettersOriginal.DebtAccountId = objUpdatedADDebtAccountLetters.DebtAccountId;
                    }

                    if (objUpdatedADDebtAccountLetters.LetterId == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLettersOriginal.LetterId = objUpdatedADDebtAccountLetters.LetterId;
                    }

                    if (objUpdatedADDebtAccountLetters.LetterCode == "" || objUpdatedADDebtAccountLetters.LetterCode == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountLettersOriginal.LetterCode = objUpdatedADDebtAccountLetters.LetterCode.Trim();
                    }

                    if (objUpdatedADDebtAccountLetters.ContractId == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLettersOriginal.ContractId = objUpdatedADDebtAccountLetters.ContractId;
                    }

                    //objADDebtAccountLettersOriginal.DateCreated = DateTime.Now;

                    objADDebtAccountLettersOriginal.IsSCInvoiceLetter = objADDebtAccountLettersOriginal.IsSCInvoiceLetter;

                    if (objUpdatedADDebtAccountLetters.SCInvoiceNum == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLettersOriginal.SCInvoiceNum = objUpdatedADDebtAccountLetters.SCInvoiceNum;
                    }

                    if (objUpdatedADDebtAccountLetters.AddressId == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLettersOriginal.AddressId = objUpdatedADDebtAccountLetters.AddressId;
                    }

                    if (objUpdatedADDebtAccountLetters.AddressTypeId == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLettersOriginal.AddressTypeId = objUpdatedADDebtAccountLetters.AddressTypeId;
                    }

                    if (objUpdatedADDebtAccountLetters.CustomText == "" || objUpdatedADDebtAccountLetters.CustomText == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountLettersOriginal.CustomText = objUpdatedADDebtAccountLetters.CustomText.Trim();
                    }

                    if (objUpdatedADDebtAccountLetters.LocationId == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLettersOriginal.LocationId = objUpdatedADDebtAccountLetters.LocationId;
                    }


                    if (objUpdatedADDebtAccountLetters.DatePrinted.ToString() == "" || objUpdatedADDebtAccountLetters.DatePrinted == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountLettersOriginal.DatePrinted = objUpdatedADDebtAccountLetters.DatePrinted;
                    }


                    objADDebtAccountLettersOriginal.UserId = user.Id;
                    objADDebtAccountLettersOriginal.UserCode = user.UserName;

                    objDebtAccountLetters = AutoMapper.Mapper.Map<DebtAccountLetters>(objADDebtAccountLettersOriginal);
                    tblDebtAccountLetters = (ITable)objDebtAccountLetters;
                    DBO.Provider.Update(ref tblDebtAccountLetters);
                }
                else
                {
                    objUpdatedADDebtAccountLetters.DateCreated = DateTime.Now;

                    objUpdatedADDebtAccountLetters.UserId = user.Id;
                    objUpdatedADDebtAccountLetters.UserCode = user.UserName;

                    objDebtAccountLetters = AutoMapper.Mapper.Map<DebtAccountLetters>(objUpdatedADDebtAccountLetters);
                    tblDebtAccountLetters = (ITable)objDebtAccountLetters;
                    DBO.Provider.Create(ref tblDebtAccountLetters);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }


        public static bool DeleteDebtAccountLetters(string DebtAccountLetterId)
        {
            try
            {
                ITable IReport;
                DebtAccountLetters objDebtAccountLetters = new DebtAccountLetters();
                objDebtAccountLetters.PKID = Convert.ToInt32(DebtAccountLetterId);
                IReport = objDebtAccountLetters;
                DBO.Provider.Delete(ref IReport);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static DataTable GetDebtAccountLettersDtList(string RbtStatusReportLetterType)
        {
            try
            {
                string sql = null;
                switch (RbtStatusReportLetterType)
                {
                    case "C":
                        sql = "select * from CollectionLetters where LetterCode = 'Status' and LetterType = 'C' and IsInactive = 0";
                        break;
                    case "D":
                        sql = "select * from CollectionLetters where LetterCode = 'Status (Debtor)' and LetterType = 'D' and IsInactive = 0";
                        break;
                    case "DA":
                        sql = "select * from CollectionLetters where LetterCode = 'Status (DebtorAtty)' and LetterType = 'DA' and IsInactive = 0";
                        break;
                    case "S":
                        sql = "select * from CollectionLetters where LetterCode = 'Status (Court)' and LetterType = 'S' and IsInactive = 0";
                        break;
                    case "CL":
                        sql = "select * from CollectionLetters where LetterCode = 'Status (Close)' and LetterType = 'CL' and IsInactive = 0";
                        break;
                    default:
                        sql = "select * from CollectionLetters where LetterCode = 'Status (Atty)' and LetterType = 'L' and IsInactive = 0";
                        break;
                }
                 
                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        public static DataTable GetLetterInfoDt(string DebtAccountId, string PKID)
        {
            try
            {
                cview_Processing_GetLetterInfo Sp_OBJ = new cview_Processing_GetLetterInfo();
                Sp_OBJ.DebtAccountId =  DebtAccountId;
                Sp_OBJ.DebtAccountLetterId = PKID;

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(Sp_OBJ);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        //Print Letters
        public static string PrintLetters(PrintLettersVM objPrintLettersVM)
        {
            string pdfPath = null;
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                string ZipLocation = user.OutputFolder + "\\DebtAccountPrintLetter-" + DateTime.Now.ToString("yyyyMMddhhmmss");


                if (objPrintLettersVM.RbtPrintOption == "Send To Printer")
                {
                    objPrintLettersVM.RbtPrintOption = "1";
                }
                else if (objPrintLettersVM.RbtPrintOption == "Send To PDF")
                {
                    objPrintLettersVM.RbtPrintOption = "2";
                }
                else if (objPrintLettersVM.RbtPrintOption == "No Report")
                {
                    objPrintLettersVM.RbtPrintOption = "3";
                }



                string path = user.SettingsFolder;
                string ReportName = "Acc-Liens.flxr";


                string ParameterList = "ClientId" + ',' + "chkReprint" + ',' + "InvoiceFromDate" + ',' + "InvoiceToDate" + ',' + "ReportDestination";
                string ParameterValues = objPrintLettersVM.ClientId + ',' + objPrintLettersVM.chkReprint + ',' + objPrintLettersVM.FromDate + ',' + objPrintLettersVM.FromDate + ',' + objPrintLettersVM.RbtPrintOption;

                AutoMapper.Mapper.CreateMap<PrintLettersVM, PrintReportQueue>();
                //ObjSource.IsBranch = true;
                PrintReportQueue objPrintReportQueue = new PrintReportQueue();
                objPrintReportQueue = AutoMapper.Mapper.Map<PrintReportQueue>(objPrintLettersVM);
                objPrintReportQueue.RequestedDate = DateTime.Today;
                objPrintReportQueue.ReportName = ReportName;
                objPrintReportQueue.ParameterList = ParameterList;
                objPrintReportQueue.ParameterValues = ParameterValues;
                //objPrintReportQueue.ChkCsvReport = objPrintLettersVM.chkReprint;
                objPrintReportQueue.ReportStoragePath = path;
                objPrintReportQueue.ReportType = objPrintLettersVM.RbtPrintOption;
                objPrintReportQueue.UserName = user.UserName;
                objPrintReportQueue.TabValue = "DayEndsPrintLetters";
                ITable tblPrintReportQueue = (ITable)objPrintReportQueue;
                DBO.Provider.Create(ref tblPrintReportQueue);

                if (System.IO.Directory.Exists(ZipLocation) == false)
                {
                    System.IO.Directory.CreateDirectory(ZipLocation);
                }

                user.OutputFolder = ZipLocation;

                var myds = new DataSet();
                var Sp_OBJ = new cview_Processing_GetLetters();
                Sp_OBJ.ClientId = objPrintLettersVM.ClientId;

                if (objPrintLettersVM.chkReprint == true)
                {
                    Sp_OBJ.DateFROM = objPrintLettersVM.FromDate.ToShortDateString();
                }
                else
                {
                    Sp_OBJ.DateFROM = objPrintLettersVM.FromDate.AddDays(-7).ToShortDateString();
                }

                Sp_OBJ.DateTO = objPrintLettersVM.ThruDate.ToShortDateString();

                DataTable mydt = DBO.Provider.GetDataSet(Sp_OBJ).Tables[0];
                TableView myview = DBO.Provider.GetTableView(Sp_OBJ);
                DataSet ds = DBO.Provider.GetDataSet(Sp_OBJ);
                DataTable mytbl = ds.Tables[0];

                if (ds.Tables[0].Rows.Count == 0)
                {
                    return "error";
                }







                //int myidx = 0;
                //CollectViewC1Letter myRPT = new CollectViewC1Letter();
                //string myreportfilename;
                //if (mydt.Rows.Count > 0)
                //{
                //    foreach (DataRow mydr in mydt.Rows)
                //    {
                //        myidx += 1;
                //        string myrptNAME = Convert.ToString(mydr["LetterCode"]);
                //        string mysubreportname = "InvoiceDetailSub";
                //        string myprefix = Convert.ToString(mydr["DEBTACCOUNTID"]) + "-" + Convert.ToString(mydr["PKID"]);
                //        AutoMapper.Mapper.CreateMap<ADDebtAccountLetters, DebtAccountLetters>();
                //        ITable tblDebtAccountLetters;
                //        DebtAccountLetters objDebtAccountLetters;
                //        ADDebtAccountLetters MYLETTER = ReadDebtAccountLetters(Convert.ToInt32(mydr["PKID"]));

                //        if (objPrintLettersVM.chkReprint == true || MYLETTER.DatePrinted == DateTime.MinValue)
                //        {
                //            var MYINFO = new cview_Processing_GetLetterInfo();
                //            MYINFO.DebtAccountId = Convert.ToString(MYLETTER.DebtAccountId);
                //            MYINFO.AddressId = Convert.ToString(MYLETTER.AddressId);
                //            MYINFO.DebtAccountLetterId = Convert.ToString(MYLETTER.PKID);

                //            var EntityList = DBO.Provider.GetTableView(MYINFO);
                //            DataTable dt = EntityList.ToTable();
                //            if (dt.Rows.Count > 0)
                //            {
                //          myreportfilename = myRPT.RenderLetter(user, DBO.Provider.GetTableView(MYINFO), "CollectionLetters.xml", myrptNAME, mymode, myprefix, "", "", mysubreportname);
                //            }

                //            MYLETTER.IsDayEndPrint = true;
                //            MYLETTER.RunFromDate = objPrintLettersVM.FromDate.AddDays(-7);
                //            MYLETTER.RunThruDate = objPrintLettersVM.ThruDate;

                //            if (objPrintLettersVM.chkReprint == false)
                //            {
                //                MYLETTER.DatePrinted = DateTime.Now;
                //                objDebtAccountLetters = AutoMapper.Mapper.Map<DebtAccountLetters>(MYLETTER);
                //                tblDebtAccountLetters = (ITable)objDebtAccountLetters;
                //                DBO.Provider.Update(ref tblDebtAccountLetters);
                //            }
                //            else if (objPrintLettersVM.chkReprint == true)
                //            {
                //                MYLETTER.IsReprintd = true;
                //                MYLETTER.RunFromDate = objPrintLettersVM.FromDate;
                //                objDebtAccountLetters = AutoMapper.Mapper.Map<DebtAccountLetters>(MYLETTER);
                //                tblDebtAccountLetters = (ITable)objDebtAccountLetters;
                //                DBO.Provider.Update(ref tblDebtAccountLetters);
                //            }

                //        }
                //    }
                //}

                //string Path = ZipLocation;//Location for inside Test Folder  
                //string[] Filenames = Directory.GetFiles(path);
                //string ZipFile = new DirectoryInfo(path).Name;
                //if (Filenames.Length > 0)
                //{
                //    using (ZipFile zip = new ZipFile())
                //    {
                //        zip.AddFiles(Filenames, ZipFile);//Zip file inside filename  
                //        zip.Save(ZipLocation + ".zip");//location and name for creating zip file  
                //    }

                //    pdfPath = ZipLocation + ".zip";
                //}
                //else
                //{
                //    pdfPath = "";
                //}
            }

            catch (Exception ex)
            {
                return "";
            }
            return "1";
        }
       
        #endregion
    }
}
