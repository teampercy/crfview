﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard
{
    public class ADDebtAccountSmallClaims
    {
        #region Properties

        public Int32 PKID { get; set; }
        public Int32 DebtAccountId { get; set; }
        public DateTime TrialDate { get; set; }
        public string TrialTime { get; set; }
        public DateTime JudgmentDate { get; set; }
        public string ClaimDescription { get; set; }
        public string ServAdd1 { get; set; }
        public string ServCity1 { get; set; }
        public string ServSt1 { get; set; }
        public string ServZip1 { get; set; }
        public string ServAdd2 { get; set; }
        public string ServCity2 { get; set; }
        public string ServSt2 { get; set; }
        public string ServZip2 { get; set; }
        public string EmpName { get; set; }
        public string EmpAdd { get; set; }
        public string EmpCity { get; set; }
        public string EmpSt { get; set; }
        public string EmpZip { get; set; }
        public string EntityType2 { get; set; }
        public string EntityName2 { get; set; }
        public string LevyCounty { get; set; }
        public string LevyAdd { get; set; }
        public string LevyCity { get; set; }
        public string LevySt { get; set; }
        public string LevyZip { get; set; }
        public string ServAdd3 { get; set; }
        public string ServCity3 { get; set; }
        public string ServSt3 { get; set; }
        public string ServZip3 { get; set; }
        public string EntityType3 { get; set; }
        public string EntityName3 { get; set; }
        public DateTime SCSuitFiledDate { get; set; }
        public Int32 CourtId { get; set; }
        public Int32 ProcessServerId { get; set; }
        public string CaseNum { get; set; }
        public decimal JudgmentAmt { get; set; }
        public string ServInstructions { get; set; }
        public string EntityType1 { get; set; }
        public string EntityName1 { get; set; }
        public DateTime SCReviewDate { get; set; }
        public string InvNum { get; set; }
        public DateTime AssignDate { get; set; }
        public string SCVenue { get; set; }
        public string SCDeskNum { get; set; }
        public Int32 SCDeskNumId { get; set; }
        public decimal UnusedCC { get; set; }
        public decimal Costs { get; set; }
        public decimal Credits { get; set; }
        public decimal DailyInt { get; set; }
        public decimal TotalDue { get; set; }
        public decimal SubTotal1 { get; set; }
        public decimal SubTotal2 { get; set; }
        public string ProcessServerRefNum { get; set; }
        public decimal SuitAmt { get; set; }
        public string VenueNum { get; set; }
        public string ProcessServerNum { get; set; }
        public decimal FilingFee { get; set; }
        public decimal ServiceFee { get; set; }
        public decimal WritFee { get; set; }
        public decimal LevyFee { get; set; }
        public decimal AbstractFee { get; set; }
        public decimal RecordingFee { get; set; }
        public decimal AgencyFee { get; set; }
        public string AbstractCounty { get; set; }
        public DateTime AbstractDate { get; set; }
        public string AbstractNum { get; set; }
        public DateTime AbstractRecordDate { get; set; }
        public DateTime WritDate { get; set; }
        public string WritCounty { get; set; }
        public DateTime ORAPDate { get; set; }
        public decimal ORAPFee { get; set; }
        public DateTime LevyDate { get; set; }
        public decimal AbstractRecordFee { get; set; }
        public decimal PostCosts { get; set; }
        public decimal RecCosts { get; set; }
        public decimal CostsRec { get; set; }
        public decimal CostsSpent { get; set; }

        #region Additional Properties
        public decimal Interest { get; set; }
        public decimal SCTotalDue { get; set; }
        #endregion

        #endregion

        #region CRUD

        public static ADDebtAccountSmallClaims GetDebtAccountSmallClaims(DataTable dt)
        {
            try
            {
                List<ADDebtAccountSmallClaims> SmallClaimsInfo = (from DataRow dr in dt.Rows
                                                          select new ADDebtAccountSmallClaims()
                                                          {
                                                              PKID = ((dr["PKID"] != DBNull.Value ? Convert.ToInt32(dr["PKID"].ToString()) : 0)),
                                                              DebtAccountId = ((dr["DebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAccountId"].ToString()) : 0)),
                                                              TrialDate = ((dr["TrialDate"] != DBNull.Value ? Convert.ToDateTime(dr["TrialDate"]) : DateTime.MinValue)),
                                                              TrialTime = ((dr["TrialTime"] != DBNull.Value ? dr["TrialTime"].ToString() : "")),
                                                              JudgmentDate = ((dr["JudgmentDate"] != DBNull.Value ? Convert.ToDateTime(dr["JudgmentDate"]) : DateTime.MinValue)),
                                                              ClaimDescription = ((dr["ClaimDescription"] != DBNull.Value ? dr["ClaimDescription"].ToString() : "")),
                                                              ServAdd1 = ((dr["ServAdd1"] != DBNull.Value ? dr["ServAdd1"].ToString() : "")),
                                                              ServCity1 = ((dr["ServCity1"] != DBNull.Value ? dr["ServCity1"].ToString() : "")),
                                                              ServSt1 = ((dr["ServSt1"] != DBNull.Value ? dr["ServSt1"].ToString() : "")),
                                                              ServZip1 = ((dr["ServZip1"] != DBNull.Value ? dr["ServZip1"].ToString() : "")),
                                                              ServAdd2 = ((dr["ServAdd2"] != DBNull.Value ? dr["ServAdd2"].ToString() : "")),
                                                              ServCity2 = ((dr["ServCity2"] != DBNull.Value ? dr["ServCity2"].ToString() : "")),
                                                              ServSt2 = ((dr["ServSt2"] != DBNull.Value ? dr["ServSt2"].ToString() : "")),
                                                              ServZip2 = ((dr["ServZip2"] != DBNull.Value ? dr["ServZip2"].ToString() : "")),
                                                              EmpName = ((dr["EmpName"] != DBNull.Value ? dr["EmpName"].ToString() : "")),
                                                              EmpAdd = ((dr["EmpAdd"] != DBNull.Value ? dr["EmpAdd"].ToString() : "")),
                                                              EmpCity = ((dr["EmpCity"] != DBNull.Value ? dr["EmpCity"].ToString() : "")),
                                                              EmpSt = ((dr["EmpSt"] != DBNull.Value ? dr["EmpSt"].ToString() : "")),
                                                              EmpZip = ((dr["EmpZip"] != DBNull.Value ? dr["EmpZip"].ToString() : "")),
                                                              EntityType2 = ((dr["EntityType2"] != DBNull.Value ? dr["EntityType2"].ToString() : "")),
                                                              EntityName2 = ((dr["EntityName2"] != DBNull.Value ? dr["EntityName2"].ToString() : "")),
                                                              LevyCounty = ((dr["LevyCounty"] != DBNull.Value ? dr["LevyCounty"].ToString() : "")),
                                                              LevyAdd = ((dr["LevyAdd"] != DBNull.Value ? dr["LevyAdd"].ToString() : "")),
                                                              LevyCity = ((dr["LevyCity"] != DBNull.Value ? dr["LevyCity"].ToString() : "")),
                                                              LevySt = ((dr["LevySt"] != DBNull.Value ? dr["LevySt"].ToString() : "")),
                                                              LevyZip = ((dr["LevyZip"] != DBNull.Value ? dr["LevyZip"].ToString() : "")),
                                                              ServAdd3 = ((dr["ServAdd3"] != DBNull.Value ? dr["ServAdd3"].ToString() : "")),
                                                              ServCity3 = ((dr["ServCity3"] != DBNull.Value ? dr["ServCity3"].ToString() : "")),
                                                              ServSt3 = ((dr["ServSt3"] != DBNull.Value ? dr["ServSt3"].ToString() : "")),
                                                              ServZip3 = ((dr["ServZip3"] != DBNull.Value ? dr["ServZip3"].ToString() : "")),
                                                              EntityType3 = ((dr["EntityType3"] != DBNull.Value ? dr["EntityType3"].ToString() : "")),
                                                              EntityName3 = ((dr["EntityName3"] != DBNull.Value ? dr["EntityName3"].ToString() : "")),
                                                              SCSuitFiledDate = ((dr["SCSuitFiledDate"] != DBNull.Value ? Convert.ToDateTime(dr["SCSuitFiledDate"]) : DateTime.MinValue)),
                                                              CourtId = ((dr["CourtId"] != DBNull.Value ? Convert.ToInt32(dr["CourtId"].ToString()) : 0)),
                                                              ProcessServerId = ((dr["ProcessServerId"] != DBNull.Value ? Convert.ToInt32(dr["ProcessServerId"].ToString()) : 0)),
                                                              CaseNum = ((dr["CaseNum"] != DBNull.Value ? dr["CaseNum"].ToString() : "")),
                                                              JudgmentAmt = ((dr["JudgmentAmt"] != DBNull.Value ? Convert.ToDecimal(dr["JudgmentAmt"].ToString()) : 0)),
                                                              ServInstructions = ((dr["ServInstructions"] != DBNull.Value ? dr["ServInstructions"].ToString() : "")),
                                                              EntityType1 = ((dr["EntityType1"] != DBNull.Value ? dr["EntityType1"].ToString() : "")),
                                                              EntityName1 = ((dr["EntityName1"] != DBNull.Value ? dr["EntityName1"].ToString() : "")),
                                                              SCReviewDate = ((dr["SCReviewDate"] != DBNull.Value ? Convert.ToDateTime(dr["SCReviewDate"]) : DateTime.MinValue)),
                                                              InvNum = ((dr["InvNum"] != DBNull.Value ? dr["InvNum"].ToString() : "")),
                                                              AssignDate = ((dr["AssignDate"] != DBNull.Value ? Convert.ToDateTime(dr["AssignDate"]) : DateTime.MinValue)),
                                                              SCVenue = ((dr["SCVenue"] != DBNull.Value ? dr["SCVenue"].ToString() : "")),
                                                              SCDeskNum = ((dr["SCDeskNum"] != DBNull.Value ? dr["SCDeskNum"].ToString() : "")),
                                                              SCDeskNumId = ((dr["SCDeskNumId"] != DBNull.Value ? Convert.ToInt32(dr["SCDeskNumId"].ToString()) : 0)),
                                                              UnusedCC = ((dr["UnusedCC"] != DBNull.Value ? Convert.ToDecimal(dr["UnusedCC"].ToString()) : 0)),
                                                              Costs = ((dr["Costs"] != DBNull.Value ? Convert.ToDecimal(dr["Costs"].ToString()) : 0)),
                                                              Credits = ((dr["Credits"] != DBNull.Value ? Convert.ToDecimal(dr["Credits"].ToString()) : 0)),
                                                              DailyInt = ((dr["DailyInt"] != DBNull.Value ? Convert.ToDecimal(dr["DailyInt"].ToString()) : 0)),
                                                              TotalDue = ((dr["TotalDue"] != DBNull.Value ? Convert.ToDecimal(dr["TotalDue"].ToString()) : 0)),
                                                              SubTotal1 = ((dr["SubTotal1"] != DBNull.Value ? Convert.ToDecimal(dr["SubTotal1"].ToString()) : 0)),
                                                              SubTotal2 = ((dr["SubTotal2"] != DBNull.Value ? Convert.ToDecimal(dr["SubTotal2"].ToString()) : 0)),
                                                              ProcessServerRefNum = ((dr["ProcessServerRefNum"] != DBNull.Value ? dr["ProcessServerRefNum"].ToString() : "")),
                                                              SuitAmt = ((dr["SuitAmt"] != DBNull.Value ? Convert.ToDecimal(dr["SuitAmt"].ToString()) : 0)),
                                                              VenueNum = ((dr["VenueNum"] != DBNull.Value ? dr["VenueNum"].ToString() : "")),
                                                              ProcessServerNum = ((dr["ProcessServerNum"] != DBNull.Value ? dr["ProcessServerNum"].ToString() : "")),
                                                              FilingFee = ((dr["FilingFee"] != DBNull.Value ? Convert.ToDecimal(dr["FilingFee"].ToString()) : 0)),
                                                              ServiceFee = ((dr["ServiceFee"] != DBNull.Value ? Convert.ToDecimal(dr["ServiceFee"].ToString()) : 0)),
                                                              WritFee = ((dr["WritFee"] != DBNull.Value ? Convert.ToDecimal(dr["WritFee"].ToString()) : 0)),
                                                              LevyFee = ((dr["LevyFee"] != DBNull.Value ? Convert.ToDecimal(dr["LevyFee"].ToString()) : 0)),
                                                              AbstractFee = ((dr["AbstractFee"] != DBNull.Value ? Convert.ToDecimal(dr["AbstractFee"].ToString()) : 0)),
                                                              RecordingFee = ((dr["RecordingFee"] != DBNull.Value ? Convert.ToDecimal(dr["RecordingFee"].ToString()) : 0)),
                                                              AgencyFee = ((dr["AgencyFee"] != DBNull.Value ? Convert.ToDecimal(dr["AgencyFee"].ToString()) : 0)),
                                                              AbstractCounty = ((dr["AbstractCounty"] != DBNull.Value ? dr["AbstractCounty"].ToString() : "")),
                                                              AbstractDate = ((dr["AbstractDate"] != DBNull.Value ? Convert.ToDateTime(dr["AbstractDate"]) : DateTime.MinValue)),
                                                              AbstractNum = ((dr["AbstractNum"] != DBNull.Value ? dr["AbstractNum"].ToString() : "")),
                                                              AbstractRecordDate = ((dr["AbstractRecordDate"] != DBNull.Value ? Convert.ToDateTime(dr["AbstractRecordDate"]) : DateTime.MinValue)),
                                                              WritDate = ((dr["WritDate"] != DBNull.Value ? Convert.ToDateTime(dr["WritDate"]) : DateTime.MinValue)),
                                                              WritCounty = ((dr["WritCounty"] != DBNull.Value ? dr["WritCounty"].ToString() : "")),
                                                              ORAPDate = ((dr["ORAPDate"] != DBNull.Value ? Convert.ToDateTime(dr["ORAPDate"]) : DateTime.MinValue)),
                                                              ORAPFee = ((dr["ORAPFee"] != DBNull.Value ? Convert.ToDecimal(dr["ORAPFee"].ToString()) : 0)),
                                                              LevyDate = ((dr["LevyDate"] != DBNull.Value ? Convert.ToDateTime(dr["LevyDate"]) : DateTime.MinValue)),
                                                              AbstractRecordFee = ((dr["AbstractRecordFee"] != DBNull.Value ? Convert.ToDecimal(dr["AbstractRecordFee"].ToString()) : 0)),
                                                              PostCosts = ((dr["PostCosts"] != DBNull.Value ? Convert.ToDecimal(dr["PostCosts"].ToString()) : 0)),
                                                              RecCosts = ((dr["RecCosts"] != DBNull.Value ? Convert.ToDecimal(dr["RecCosts"].ToString()) : 0)),
                                                              CostsRec = ((dr["CostsRec"] != DBNull.Value ? Convert.ToDecimal(dr["CostsRec"].ToString()) : 0)),
                                                              CostsSpent = ((dr["CostsSpent"] != DBNull.Value ? Convert.ToDecimal(dr["CostsSpent"].ToString()) : 0)),
                                                          }).ToList();
                return SmallClaimsInfo.First();
            }
            catch (Exception ex)
            {
                return (new ADDebtAccountSmallClaims());
            }
        }

        public static DataTable GetDebtAccountSmallClaimsList(string AccountId)
        {
            try
            {
                string sql = "Select * from DebtAccountSmallClaims where DebtAccountId  = " + AccountId;

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }


        //Read Details from database
        public static ADDebtAccountSmallClaims ReadDebtAccountSmallClaims(int id)
        {
            DebtAccountSmallClaims myentity = new DebtAccountSmallClaims();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (DebtAccountSmallClaims)myentityItable;
            AutoMapper.Mapper.CreateMap<DebtAccountSmallClaims, ADDebtAccountSmallClaims>();
            return (AutoMapper.Mapper.Map<ADDebtAccountSmallClaims>(myentity));
        }

        //public static int SaveDebtAccountSmallClaims(ADDebtAccountSmallClaims objUpdatedADDebtAccountSmallClaims)
        //{
        //    try
        //    {
        //        AutoMapper.Mapper.CreateMap<ADDebtAccountSmallClaims, DebtAccountSmallClaims>();
        //        ITable tblDebtAccountSmallClaims;
        //        DebtAccountSmallClaims objDebtAccountSmallClaims;

        //        CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

        //        if (objUpdatedADDebtAccountSmallClaims.PKID != 0)
        //        {
        //            ADDebtAccountSmallClaims objADDebtAccountSmallClaimsOriginal = ReadDebtAccountSmallClaims(Convert.ToInt32(objUpdatedADDebtAccountSmallClaims.PKID));

        //            if (objUpdatedADDebtAccountSmallClaims.DebtAccountId == 0)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.DebtAccountId = objUpdatedADDebtAccountSmallClaims.DebtAccountId;
        //            }

        //            #region Account Info

        //            if (objUpdatedADDebtAccountSmallClaims.CourtId == 0)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.CourtId = objUpdatedADDebtAccountSmallClaims.CourtId;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.VenueNum == "" || objUpdatedADDebtAccountSmallClaims.VenueNum == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.VenueNum = objUpdatedADDebtAccountSmallClaims.VenueNum.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.SCVenue == "" || objUpdatedADDebtAccountSmallClaims.SCVenue == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.SCVenue = objUpdatedADDebtAccountSmallClaims.SCVenue.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.ProcessServerId == 0)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.ProcessServerId = objUpdatedADDebtAccountSmallClaims.ProcessServerId;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.ProcessServerNum == "" || objUpdatedADDebtAccountSmallClaims.ProcessServerNum == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.ProcessServerNum = objUpdatedADDebtAccountSmallClaims.ProcessServerNum.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.EntityName1 == "" || objUpdatedADDebtAccountSmallClaims.EntityName1 == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.EntityName1 = objUpdatedADDebtAccountSmallClaims.EntityName1.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.SuitAmt == 0)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.SuitAmt = objUpdatedADDebtAccountSmallClaims.SuitAmt;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.SCReviewDate == DateTime.MinValue)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.SCReviewDate = objUpdatedADDebtAccountSmallClaims.SCReviewDate;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.AssignDate == DateTime.MinValue)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.AssignDate = objUpdatedADDebtAccountSmallClaims.AssignDate;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.TrialDate == DateTime.MinValue)
        //            {
        //                objADDebtAccountSmallClaimsOriginal.TrialDate = objUpdatedADDebtAccountSmallClaims.TrialDate;
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.TrialDate = objUpdatedADDebtAccountSmallClaims.TrialDate;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.TrialTime == "" || objUpdatedADDebtAccountSmallClaims.TrialTime == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.TrialTime = objUpdatedADDebtAccountSmallClaims.TrialTime.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.SCSuitFiledDate == DateTime.MinValue)
        //            {
        //                objADDebtAccountSmallClaimsOriginal.SCSuitFiledDate = objUpdatedADDebtAccountSmallClaims.SCSuitFiledDate;
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.SCSuitFiledDate = objUpdatedADDebtAccountSmallClaims.SCSuitFiledDate;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.CaseNum == "" || objUpdatedADDebtAccountSmallClaims.CaseNum == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.CaseNum = objUpdatedADDebtAccountSmallClaims.CaseNum.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.ProcessServerRefNum == "" || objUpdatedADDebtAccountSmallClaims.ProcessServerRefNum == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.ProcessServerRefNum = objUpdatedADDebtAccountSmallClaims.ProcessServerRefNum.Trim();
        //            }

        //            #endregion

        //            #region Account Info

        //            #region Entity Info

        //            if (objUpdatedADDebtAccountSmallClaims.EntityName1 == "" || objUpdatedADDebtAccountSmallClaims.EntityName1 == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.EntityName1 = objUpdatedADDebtAccountSmallClaims.EntityName1.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.EntityType1 == "" || objUpdatedADDebtAccountSmallClaims.EntityType1 == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.EntityType1 = objUpdatedADDebtAccountSmallClaims.EntityType1.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.EntityName2 == "" || objUpdatedADDebtAccountSmallClaims.EntityName2 == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.EntityName2 = objUpdatedADDebtAccountSmallClaims.EntityName2.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.EntityType2 == "" || objUpdatedADDebtAccountSmallClaims.EntityType2 == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.EntityType2 = objUpdatedADDebtAccountSmallClaims.EntityType2.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.EntityName3 == "" || objUpdatedADDebtAccountSmallClaims.EntityName3 == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.EntityName3 = objUpdatedADDebtAccountSmallClaims.EntityName3.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.EntityType3 == "" || objUpdatedADDebtAccountSmallClaims.EntityType3 == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.EntityType3 = objUpdatedADDebtAccountSmallClaims.EntityType3.Trim();
        //            }

        //            #endregion

        //            #region Service Address Info
        //            if (objUpdatedADDebtAccountSmallClaims.ServAdd1 == "" || objUpdatedADDebtAccountSmallClaims.ServAdd1 == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.ServAdd1 = objUpdatedADDebtAccountSmallClaims.ServAdd1.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.ServCity1 == "" || objUpdatedADDebtAccountSmallClaims.ServCity1 == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.ServCity1 = objUpdatedADDebtAccountSmallClaims.ServCity1.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.ServSt1 == "" || objUpdatedADDebtAccountSmallClaims.ServSt1 == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.ServSt1 = objUpdatedADDebtAccountSmallClaims.ServSt1.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.ServZip1 == "" || objUpdatedADDebtAccountSmallClaims.ServZip1 == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.ServZip1 = objUpdatedADDebtAccountSmallClaims.ServZip1.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.ServAdd2 == "" || objUpdatedADDebtAccountSmallClaims.ServAdd2 == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.ServAdd2 = objUpdatedADDebtAccountSmallClaims.ServAdd2.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.ServCity2 == "" || objUpdatedADDebtAccountSmallClaims.ServCity2 == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.ServCity2 = objUpdatedADDebtAccountSmallClaims.ServCity2.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.ServSt2 == "" || objUpdatedADDebtAccountSmallClaims.ServSt2 == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.ServSt2 = objUpdatedADDebtAccountSmallClaims.ServSt2.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.ServZip2 == "" || objUpdatedADDebtAccountSmallClaims.ServZip2 == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.ServZip2 = objUpdatedADDebtAccountSmallClaims.ServZip2.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.ServAdd3 == "" || objUpdatedADDebtAccountSmallClaims.ServAdd3 == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.ServAdd3 = objUpdatedADDebtAccountSmallClaims.ServAdd3.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.ServCity3 == "" || objUpdatedADDebtAccountSmallClaims.ServCity3 == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.ServCity3 = objUpdatedADDebtAccountSmallClaims.ServCity3.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.ServSt3 == "" || objUpdatedADDebtAccountSmallClaims.ServSt3 == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.ServSt3 = objUpdatedADDebtAccountSmallClaims.ServSt3.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.ServZip3 == "" || objUpdatedADDebtAccountSmallClaims.ServZip3 == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.ServZip3 = objUpdatedADDebtAccountSmallClaims.ServZip3.Trim();
        //            }
        //            #endregion

        //            #region Employer Info
        //            if (objUpdatedADDebtAccountSmallClaims.EmpName == "" || objUpdatedADDebtAccountSmallClaims.EmpName == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.EmpName = objUpdatedADDebtAccountSmallClaims.EmpName.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.EmpAdd == "" || objUpdatedADDebtAccountSmallClaims.EmpAdd == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.EmpAdd = objUpdatedADDebtAccountSmallClaims.EmpAdd.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.EmpCity == "" || objUpdatedADDebtAccountSmallClaims.EmpCity == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.EmpCity = objUpdatedADDebtAccountSmallClaims.EmpCity.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.EmpSt == "" || objUpdatedADDebtAccountSmallClaims.EmpSt == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.EmpSt = objUpdatedADDebtAccountSmallClaims.EmpSt.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.EmpZip == "" || objUpdatedADDebtAccountSmallClaims.EmpZip == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.EmpZip = objUpdatedADDebtAccountSmallClaims.EmpZip.Trim();
        //            }
        //            #endregion


        //            #region Other Info
        //            if (objUpdatedADDebtAccountSmallClaims.ClaimDescription == "" || objUpdatedADDebtAccountSmallClaims.ClaimDescription == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.ClaimDescription = objUpdatedADDebtAccountSmallClaims.ClaimDescription.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.SuitAmt == 0)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.SuitAmt = objUpdatedADDebtAccountSmallClaims.SuitAmt;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.SCVenue == "" || objUpdatedADDebtAccountSmallClaims.SCVenue == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.SCVenue = objUpdatedADDebtAccountSmallClaims.SCVenue.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.LevyCounty == "" || objUpdatedADDebtAccountSmallClaims.LevyCounty == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.LevyCounty = objUpdatedADDebtAccountSmallClaims.LevyCounty.Trim();
        //            }

        //            #endregion

        //            #endregion

        //            #region Court Cost Info

        //            if (objUpdatedADDebtAccountSmallClaims.CostsSpent == 0)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.CostsSpent = objUpdatedADDebtAccountSmallClaims.CostsSpent;
        //            }
        //            #endregion

        //            #region Account Action

        //            if (objUpdatedADDebtAccountSmallClaims.SCReviewDate == DateTime.MinValue)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.SCReviewDate = objUpdatedADDebtAccountSmallClaims.SCReviewDate;
        //            }
        //            #endregion

        //            #region Post Judgement Info

        //            if (objUpdatedADDebtAccountSmallClaims.JudgmentDate == DateTime.MinValue)
        //            {
        //                //objADDebtAccountSmallClaimsOriginal.JudgmentDate = objUpdatedADDebtAccountSmallClaims.JudgmentDate;
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.JudgmentDate = objUpdatedADDebtAccountSmallClaims.JudgmentDate;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.JudgmentAmt == 0)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.JudgmentAmt = objUpdatedADDebtAccountSmallClaims.JudgmentAmt;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.Costs == 0)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.Costs = objUpdatedADDebtAccountSmallClaims.Costs;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.SubTotal1 == 0)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.SubTotal1 = objUpdatedADDebtAccountSmallClaims.SubTotal1;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.Credits == 0)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.Credits = objUpdatedADDebtAccountSmallClaims.Credits;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.PostCosts == 0)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.PostCosts = objUpdatedADDebtAccountSmallClaims.PostCosts;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.RecCosts == 0)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.RecCosts = objUpdatedADDebtAccountSmallClaims.RecCosts;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.SubTotal2 == 0)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.SubTotal2 = objUpdatedADDebtAccountSmallClaims.SubTotal2;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.DailyInt == 0)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.DailyInt = objUpdatedADDebtAccountSmallClaims.DailyInt;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.Interest == 0)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.Interest = objUpdatedADDebtAccountSmallClaims.Interest;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.SCTotalDue == 0)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.SCTotalDue = objUpdatedADDebtAccountSmallClaims.SCTotalDue;
        //            }
        //            #endregion

        //            #region Letter Merge Info

        //            if (objUpdatedADDebtAccountSmallClaims.FilingFee == 0)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.FilingFee = objUpdatedADDebtAccountSmallClaims.FilingFee;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.ServiceFee == 0)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.ServiceFee = objUpdatedADDebtAccountSmallClaims.ServiceFee;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.AgencyFee == 0)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.AgencyFee = objUpdatedADDebtAccountSmallClaims.AgencyFee;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.RecordingFee == 0)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.RecordingFee = objUpdatedADDebtAccountSmallClaims.RecordingFee;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.AbstractFee == 0)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.AbstractFee = objUpdatedADDebtAccountSmallClaims.AbstractFee;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.AbstractDate == DateTime.MinValue)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.AbstractDate = objUpdatedADDebtAccountSmallClaims.AbstractDate;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.AbstractRecordDate == DateTime.MinValue)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.AbstractRecordDate = objUpdatedADDebtAccountSmallClaims.AbstractRecordDate;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.AbstractNum == "" || objUpdatedADDebtAccountSmallClaims.AbstractNum == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.AbstractNum = objUpdatedADDebtAccountSmallClaims.AbstractNum.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.AbstractCounty == "" || objUpdatedADDebtAccountSmallClaims.AbstractCounty == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.AbstractCounty = objUpdatedADDebtAccountSmallClaims.AbstractCounty.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.WritFee == 0)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.WritFee = objUpdatedADDebtAccountSmallClaims.WritFee;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.WritDate == DateTime.MinValue)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.WritDate = objUpdatedADDebtAccountSmallClaims.WritDate;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.WritCounty == "" || objUpdatedADDebtAccountSmallClaims.WritCounty == null)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.WritCounty = objUpdatedADDebtAccountSmallClaims.WritCounty.Trim();
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.LevyFee == 0)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.LevyFee = objUpdatedADDebtAccountSmallClaims.LevyFee;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.LevyDate == DateTime.MinValue)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.LevyDate = objUpdatedADDebtAccountSmallClaims.LevyDate;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.ORAPFee == 0)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.ORAPFee = objUpdatedADDebtAccountSmallClaims.ORAPFee;
        //            }

        //            if (objUpdatedADDebtAccountSmallClaims.ORAPDate == DateTime.MinValue)
        //            {
        //            }
        //            else
        //            {
        //                objADDebtAccountSmallClaimsOriginal.ORAPDate = objUpdatedADDebtAccountSmallClaims.ORAPDate;
        //            }
        //            #endregion

        //            objDebtAccountSmallClaims = AutoMapper.Mapper.Map<DebtAccountSmallClaims>(objADDebtAccountSmallClaimsOriginal);
        //            tblDebtAccountSmallClaims = (ITable)objDebtAccountSmallClaims;
        //            DBO.Provider.Update(ref tblDebtAccountSmallClaims);
        //        }
        //        else
        //        {
        //            objDebtAccountSmallClaims = AutoMapper.Mapper.Map<DebtAccountSmallClaims>(objUpdatedADDebtAccountSmallClaims);
        //            tblDebtAccountSmallClaims = (ITable)objDebtAccountSmallClaims;
        //            DBO.Provider.Create(ref tblDebtAccountSmallClaims);

        //            //Record New Small Claims ActionHistory
        //            var MYSPROC = new cview_Accounts_CreateActionHistory();
        //            MYSPROC.DebtAccountId = objUpdatedADDebtAccountSmallClaims.DebtAccountId;
        //            MYSPROC.ActionId = 5;
        //            MYSPROC.UserId = user.Id;
        //            MYSPROC.UserName = user.UserName;
        //            DBO.Provider.ExecNonQuery(MYSPROC);
        //        }

        //        return 1;
        //    }
        //    catch (Exception ex)
        //    {
        //        ErrorLog.SendErrorToText(ex);
        //        return 0;
        //    }
        //}

        public static int SaveDebtAccountSmallClaims_AccountInfo(ADDebtAccountSmallClaims objUpdatedADDebtAccountSmallClaims)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADDebtAccountSmallClaims, DebtAccountSmallClaims>();
                ITable tblDebtAccountSmallClaims;
                DebtAccountSmallClaims objDebtAccountSmallClaims;

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                if (objUpdatedADDebtAccountSmallClaims.PKID != 0)
                {
                    ADDebtAccountSmallClaims objADDebtAccountSmallClaimsOriginal = ReadDebtAccountSmallClaims(Convert.ToInt32(objUpdatedADDebtAccountSmallClaims.PKID));

                    if (objUpdatedADDebtAccountSmallClaims.DebtAccountId == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.DebtAccountId = objUpdatedADDebtAccountSmallClaims.DebtAccountId;
                    }

                    #region Account Info

                    if (objUpdatedADDebtAccountSmallClaims.CourtId == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.CourtId = objUpdatedADDebtAccountSmallClaims.CourtId;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.VenueNum == "" || objUpdatedADDebtAccountSmallClaims.VenueNum == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.VenueNum = objUpdatedADDebtAccountSmallClaims.VenueNum.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.SCVenue == "" || objUpdatedADDebtAccountSmallClaims.SCVenue == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.SCVenue = objUpdatedADDebtAccountSmallClaims.SCVenue.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.ProcessServerId == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.ProcessServerId = objUpdatedADDebtAccountSmallClaims.ProcessServerId;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.ProcessServerNum == "" || objUpdatedADDebtAccountSmallClaims.ProcessServerNum == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.ProcessServerNum = objUpdatedADDebtAccountSmallClaims.ProcessServerNum.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.EntityName1 == "" || objUpdatedADDebtAccountSmallClaims.EntityName1 == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.EntityName1 = objUpdatedADDebtAccountSmallClaims.EntityName1.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.SuitAmt == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.SuitAmt = objUpdatedADDebtAccountSmallClaims.SuitAmt;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.SCReviewDate == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.SCReviewDate = objUpdatedADDebtAccountSmallClaims.SCReviewDate;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.AssignDate == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.AssignDate = objUpdatedADDebtAccountSmallClaims.AssignDate;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.TrialDate == DateTime.MinValue)
                    {
                        objADDebtAccountSmallClaimsOriginal.TrialDate = objUpdatedADDebtAccountSmallClaims.TrialDate;
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.TrialDate = objUpdatedADDebtAccountSmallClaims.TrialDate;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.TrialTime == "" || objUpdatedADDebtAccountSmallClaims.TrialTime == null)
                    {
                        objADDebtAccountSmallClaimsOriginal.TrialTime = objUpdatedADDebtAccountSmallClaims.TrialTime;
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.TrialTime = objUpdatedADDebtAccountSmallClaims.TrialTime.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.SCSuitFiledDate == DateTime.MinValue)
                    {
                        objADDebtAccountSmallClaimsOriginal.SCSuitFiledDate = objUpdatedADDebtAccountSmallClaims.SCSuitFiledDate;
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.SCSuitFiledDate = objUpdatedADDebtAccountSmallClaims.SCSuitFiledDate;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.CaseNum == "" || objUpdatedADDebtAccountSmallClaims.CaseNum == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.CaseNum = objUpdatedADDebtAccountSmallClaims.CaseNum.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.ProcessServerRefNum == "" || objUpdatedADDebtAccountSmallClaims.ProcessServerRefNum == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.ProcessServerRefNum = objUpdatedADDebtAccountSmallClaims.ProcessServerRefNum.Trim();
                    }

                    #endregion

             

                    objDebtAccountSmallClaims = AutoMapper.Mapper.Map<DebtAccountSmallClaims>(objADDebtAccountSmallClaimsOriginal);
                    tblDebtAccountSmallClaims = (ITable)objDebtAccountSmallClaims;
                    DBO.Provider.Update(ref tblDebtAccountSmallClaims);
                }
                else
                {
                    objDebtAccountSmallClaims = AutoMapper.Mapper.Map<DebtAccountSmallClaims>(objUpdatedADDebtAccountSmallClaims);
                    tblDebtAccountSmallClaims = (ITable)objDebtAccountSmallClaims;
                    DBO.Provider.Create(ref tblDebtAccountSmallClaims);

                    //Record New Small Claims ActionHistory
                    var MYSPROC = new cview_Accounts_CreateActionHistory();
                    MYSPROC.DebtAccountId = objUpdatedADDebtAccountSmallClaims.DebtAccountId;
                    MYSPROC.ActionId = 5;
                    MYSPROC.UserId = user.Id;
                    MYSPROC.UserName = user.UserName;
                    DBO.Provider.ExecNonQuery(MYSPROC);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int SaveDebtAccountSmallClaims_AdditionalInfo(ADDebtAccountSmallClaims objUpdatedADDebtAccountSmallClaims)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADDebtAccountSmallClaims, DebtAccountSmallClaims>();
                ITable tblDebtAccountSmallClaims;
                DebtAccountSmallClaims objDebtAccountSmallClaims;

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                if (objUpdatedADDebtAccountSmallClaims.PKID != 0)
                {
                    ADDebtAccountSmallClaims objADDebtAccountSmallClaimsOriginal = ReadDebtAccountSmallClaims(Convert.ToInt32(objUpdatedADDebtAccountSmallClaims.PKID));

                    if (objUpdatedADDebtAccountSmallClaims.DebtAccountId == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.DebtAccountId = objUpdatedADDebtAccountSmallClaims.DebtAccountId;
                    }

                    

                    #region Additional Info

                    #region Entity Info

                    if (objUpdatedADDebtAccountSmallClaims.EntityName1 == "" || objUpdatedADDebtAccountSmallClaims.EntityName1 == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.EntityName1 = objUpdatedADDebtAccountSmallClaims.EntityName1.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.EntityType1 == "" || objUpdatedADDebtAccountSmallClaims.EntityType1 == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.EntityType1 = objUpdatedADDebtAccountSmallClaims.EntityType1.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.EntityName2 == "" || objUpdatedADDebtAccountSmallClaims.EntityName2 == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.EntityName2 = objUpdatedADDebtAccountSmallClaims.EntityName2.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.EntityType2 == "" || objUpdatedADDebtAccountSmallClaims.EntityType2 == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.EntityType2 = objUpdatedADDebtAccountSmallClaims.EntityType2.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.EntityName3 == "" || objUpdatedADDebtAccountSmallClaims.EntityName3 == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.EntityName3 = objUpdatedADDebtAccountSmallClaims.EntityName3.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.EntityType3 == "" || objUpdatedADDebtAccountSmallClaims.EntityType3 == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.EntityType3 = objUpdatedADDebtAccountSmallClaims.EntityType3.Trim();
                    }

                    #endregion

                    #region Service Address Info
                    if (objUpdatedADDebtAccountSmallClaims.ServAdd1 == "" || objUpdatedADDebtAccountSmallClaims.ServAdd1 == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.ServAdd1 = objUpdatedADDebtAccountSmallClaims.ServAdd1.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.ServCity1 == "" || objUpdatedADDebtAccountSmallClaims.ServCity1 == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.ServCity1 = objUpdatedADDebtAccountSmallClaims.ServCity1.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.ServSt1 == "" || objUpdatedADDebtAccountSmallClaims.ServSt1 == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.ServSt1 = objUpdatedADDebtAccountSmallClaims.ServSt1.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.ServZip1 == "" || objUpdatedADDebtAccountSmallClaims.ServZip1 == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.ServZip1 = objUpdatedADDebtAccountSmallClaims.ServZip1.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.ServAdd2 == "" || objUpdatedADDebtAccountSmallClaims.ServAdd2 == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.ServAdd2 = objUpdatedADDebtAccountSmallClaims.ServAdd2.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.ServCity2 == "" || objUpdatedADDebtAccountSmallClaims.ServCity2 == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.ServCity2 = objUpdatedADDebtAccountSmallClaims.ServCity2.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.ServSt2 == "" || objUpdatedADDebtAccountSmallClaims.ServSt2 == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.ServSt2 = objUpdatedADDebtAccountSmallClaims.ServSt2.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.ServZip2 == "" || objUpdatedADDebtAccountSmallClaims.ServZip2 == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.ServZip2 = objUpdatedADDebtAccountSmallClaims.ServZip2.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.ServAdd3 == "" || objUpdatedADDebtAccountSmallClaims.ServAdd3 == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.ServAdd3 = objUpdatedADDebtAccountSmallClaims.ServAdd3.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.ServCity3 == "" || objUpdatedADDebtAccountSmallClaims.ServCity3 == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.ServCity3 = objUpdatedADDebtAccountSmallClaims.ServCity3.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.ServSt3 == "" || objUpdatedADDebtAccountSmallClaims.ServSt3 == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.ServSt3 = objUpdatedADDebtAccountSmallClaims.ServSt3.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.ServZip3 == "" || objUpdatedADDebtAccountSmallClaims.ServZip3 == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.ServZip3 = objUpdatedADDebtAccountSmallClaims.ServZip3.Trim();
                    }
                    #endregion

                    #region Employer Info
                    if (objUpdatedADDebtAccountSmallClaims.EmpName == "" || objUpdatedADDebtAccountSmallClaims.EmpName == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.EmpName = objUpdatedADDebtAccountSmallClaims.EmpName.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.EmpAdd == "" || objUpdatedADDebtAccountSmallClaims.EmpAdd == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.EmpAdd = objUpdatedADDebtAccountSmallClaims.EmpAdd.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.EmpCity == "" || objUpdatedADDebtAccountSmallClaims.EmpCity == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.EmpCity = objUpdatedADDebtAccountSmallClaims.EmpCity.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.EmpSt == "" || objUpdatedADDebtAccountSmallClaims.EmpSt == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.EmpSt = objUpdatedADDebtAccountSmallClaims.EmpSt.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.EmpZip == "" || objUpdatedADDebtAccountSmallClaims.EmpZip == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.EmpZip = objUpdatedADDebtAccountSmallClaims.EmpZip.Trim();
                    }
                    #endregion


                    #region Other Info
                    if (objUpdatedADDebtAccountSmallClaims.ClaimDescription == "" || objUpdatedADDebtAccountSmallClaims.ClaimDescription == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.ClaimDescription = objUpdatedADDebtAccountSmallClaims.ClaimDescription.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.SuitAmt == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.SuitAmt = objUpdatedADDebtAccountSmallClaims.SuitAmt;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.SCVenue == "" || objUpdatedADDebtAccountSmallClaims.SCVenue == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.SCVenue = objUpdatedADDebtAccountSmallClaims.SCVenue.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.LevyCounty == "" || objUpdatedADDebtAccountSmallClaims.LevyCounty == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.LevyCounty = objUpdatedADDebtAccountSmallClaims.LevyCounty.Trim();
                    }

                    #endregion

                    #endregion

                    objDebtAccountSmallClaims = AutoMapper.Mapper.Map<DebtAccountSmallClaims>(objADDebtAccountSmallClaimsOriginal);
                    tblDebtAccountSmallClaims = (ITable)objDebtAccountSmallClaims;
                    DBO.Provider.Update(ref tblDebtAccountSmallClaims);
                }
                else
                {
                    objDebtAccountSmallClaims = AutoMapper.Mapper.Map<DebtAccountSmallClaims>(objUpdatedADDebtAccountSmallClaims);
                    tblDebtAccountSmallClaims = (ITable)objDebtAccountSmallClaims;
                    DBO.Provider.Create(ref tblDebtAccountSmallClaims);

                    //Record New Small Claims ActionHistory
                    var MYSPROC = new cview_Accounts_CreateActionHistory();
                    MYSPROC.DebtAccountId = objUpdatedADDebtAccountSmallClaims.DebtAccountId;
                    MYSPROC.ActionId = 5;
                    MYSPROC.UserId = user.Id;
                    MYSPROC.UserName = user.UserName;
                    DBO.Provider.ExecNonQuery(MYSPROC);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }


        public static int SaveDebtAccountSmallClaims_CourtCostInfo(ADDebtAccountSmallClaims objUpdatedADDebtAccountSmallClaims)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADDebtAccountSmallClaims, DebtAccountSmallClaims>();
                ITable tblDebtAccountSmallClaims;
                DebtAccountSmallClaims objDebtAccountSmallClaims;

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                if (objUpdatedADDebtAccountSmallClaims.PKID != 0)
                {
                    ADDebtAccountSmallClaims objADDebtAccountSmallClaimsOriginal = ReadDebtAccountSmallClaims(Convert.ToInt32(objUpdatedADDebtAccountSmallClaims.PKID));

                    if (objUpdatedADDebtAccountSmallClaims.DebtAccountId == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.DebtAccountId = objUpdatedADDebtAccountSmallClaims.DebtAccountId;
                    }

                    #region Court Cost Info

                    if (objUpdatedADDebtAccountSmallClaims.CostsSpent == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.CostsSpent = objUpdatedADDebtAccountSmallClaims.CostsSpent;
                    }
                    #endregion

                    objDebtAccountSmallClaims = AutoMapper.Mapper.Map<DebtAccountSmallClaims>(objADDebtAccountSmallClaimsOriginal);
                    tblDebtAccountSmallClaims = (ITable)objDebtAccountSmallClaims;
                    DBO.Provider.Update(ref tblDebtAccountSmallClaims);
                }
                else
                {
                    objDebtAccountSmallClaims = AutoMapper.Mapper.Map<DebtAccountSmallClaims>(objUpdatedADDebtAccountSmallClaims);
                    tblDebtAccountSmallClaims = (ITable)objDebtAccountSmallClaims;
                    DBO.Provider.Create(ref tblDebtAccountSmallClaims);

                    //Record New Small Claims ActionHistory
                    var MYSPROC = new cview_Accounts_CreateActionHistory();
                    MYSPROC.DebtAccountId = objUpdatedADDebtAccountSmallClaims.DebtAccountId;
                    MYSPROC.ActionId = 5;
                    MYSPROC.UserId = user.Id;
                    MYSPROC.UserName = user.UserName;
                    DBO.Provider.ExecNonQuery(MYSPROC);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }


        public static int SaveDebtAccountSmallClaims_AccountAction(ADDebtAccountSmallClaims objUpdatedADDebtAccountSmallClaims)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADDebtAccountSmallClaims, DebtAccountSmallClaims>();
                ITable tblDebtAccountSmallClaims;
                DebtAccountSmallClaims objDebtAccountSmallClaims;

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                if (objUpdatedADDebtAccountSmallClaims.PKID != 0)
                {
                    ADDebtAccountSmallClaims objADDebtAccountSmallClaimsOriginal = ReadDebtAccountSmallClaims(Convert.ToInt32(objUpdatedADDebtAccountSmallClaims.PKID));

                    if (objUpdatedADDebtAccountSmallClaims.DebtAccountId == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.DebtAccountId = objUpdatedADDebtAccountSmallClaims.DebtAccountId;
                    }


                    #region Account Action

                    if (objUpdatedADDebtAccountSmallClaims.SCReviewDate == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.SCReviewDate = objUpdatedADDebtAccountSmallClaims.SCReviewDate;
                    }
                    #endregion

                    objDebtAccountSmallClaims = AutoMapper.Mapper.Map<DebtAccountSmallClaims>(objADDebtAccountSmallClaimsOriginal);
                    tblDebtAccountSmallClaims = (ITable)objDebtAccountSmallClaims;
                    DBO.Provider.Update(ref tblDebtAccountSmallClaims);
                }
                else
                {
                    objDebtAccountSmallClaims = AutoMapper.Mapper.Map<DebtAccountSmallClaims>(objUpdatedADDebtAccountSmallClaims);
                    tblDebtAccountSmallClaims = (ITable)objDebtAccountSmallClaims;
                    DBO.Provider.Create(ref tblDebtAccountSmallClaims);

                    //Record New Small Claims ActionHistory
                    var MYSPROC = new cview_Accounts_CreateActionHistory();
                    MYSPROC.DebtAccountId = objUpdatedADDebtAccountSmallClaims.DebtAccountId;
                    MYSPROC.ActionId = 5;
                    MYSPROC.UserId = user.Id;
                    MYSPROC.UserName = user.UserName;
                    DBO.Provider.ExecNonQuery(MYSPROC);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int SaveDebtAccountSmallClaims_PostJudgementInfo(ADDebtAccountSmallClaims objUpdatedADDebtAccountSmallClaims)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADDebtAccountSmallClaims, DebtAccountSmallClaims>();
                ITable tblDebtAccountSmallClaims;
                DebtAccountSmallClaims objDebtAccountSmallClaims;

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                if (objUpdatedADDebtAccountSmallClaims.PKID != 0)
                {
                    ADDebtAccountSmallClaims objADDebtAccountSmallClaimsOriginal = ReadDebtAccountSmallClaims(Convert.ToInt32(objUpdatedADDebtAccountSmallClaims.PKID));

                    if (objUpdatedADDebtAccountSmallClaims.DebtAccountId == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.DebtAccountId = objUpdatedADDebtAccountSmallClaims.DebtAccountId;
                    }

                    #region Post Judgement Info

                    if (objUpdatedADDebtAccountSmallClaims.JudgmentDate == DateTime.MinValue)
                    {
                        objADDebtAccountSmallClaimsOriginal.JudgmentDate = objUpdatedADDebtAccountSmallClaims.JudgmentDate;
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.JudgmentDate = objUpdatedADDebtAccountSmallClaims.JudgmentDate;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.JudgmentAmt == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.JudgmentAmt = objUpdatedADDebtAccountSmallClaims.JudgmentAmt;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.Costs == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.Costs = objUpdatedADDebtAccountSmallClaims.Costs;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.SubTotal1 == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.SubTotal1 = objUpdatedADDebtAccountSmallClaims.SubTotal1;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.Credits == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.Credits = objUpdatedADDebtAccountSmallClaims.Credits;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.PostCosts == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.PostCosts = objUpdatedADDebtAccountSmallClaims.PostCosts;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.RecCosts == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.RecCosts = objUpdatedADDebtAccountSmallClaims.RecCosts;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.SubTotal2 == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.SubTotal2 = objUpdatedADDebtAccountSmallClaims.SubTotal2;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.DailyInt == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.DailyInt = objUpdatedADDebtAccountSmallClaims.DailyInt;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.Interest == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.Interest = objUpdatedADDebtAccountSmallClaims.Interest;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.SCTotalDue == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.SCTotalDue = objUpdatedADDebtAccountSmallClaims.SCTotalDue;
                    }
                    #endregion

                    objDebtAccountSmallClaims = AutoMapper.Mapper.Map<DebtAccountSmallClaims>(objADDebtAccountSmallClaimsOriginal);
                    tblDebtAccountSmallClaims = (ITable)objDebtAccountSmallClaims;
                    DBO.Provider.Update(ref tblDebtAccountSmallClaims);
                }
                else
                {
                    objDebtAccountSmallClaims = AutoMapper.Mapper.Map<DebtAccountSmallClaims>(objUpdatedADDebtAccountSmallClaims);
                    tblDebtAccountSmallClaims = (ITable)objDebtAccountSmallClaims;
                    DBO.Provider.Create(ref tblDebtAccountSmallClaims);

                    //Record New Small Claims ActionHistory
                    var MYSPROC = new cview_Accounts_CreateActionHistory();
                    MYSPROC.DebtAccountId = objUpdatedADDebtAccountSmallClaims.DebtAccountId;
                    MYSPROC.ActionId = 5;
                    MYSPROC.UserId = user.Id;
                    MYSPROC.UserName = user.UserName;
                    DBO.Provider.ExecNonQuery(MYSPROC);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int SaveDebtAccountSmallClaims_LetterMergeInfo(ADDebtAccountSmallClaims objUpdatedADDebtAccountSmallClaims)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADDebtAccountSmallClaims, DebtAccountSmallClaims>();
                ITable tblDebtAccountSmallClaims;
                DebtAccountSmallClaims objDebtAccountSmallClaims;

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                if (objUpdatedADDebtAccountSmallClaims.PKID != 0)
                {
                    ADDebtAccountSmallClaims objADDebtAccountSmallClaimsOriginal = ReadDebtAccountSmallClaims(Convert.ToInt32(objUpdatedADDebtAccountSmallClaims.PKID));

                    if (objUpdatedADDebtAccountSmallClaims.DebtAccountId == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.DebtAccountId = objUpdatedADDebtAccountSmallClaims.DebtAccountId;
                    }

                    #region Letter Merge Info

                    if (objUpdatedADDebtAccountSmallClaims.FilingFee == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.FilingFee = objUpdatedADDebtAccountSmallClaims.FilingFee;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.ServiceFee == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.ServiceFee = objUpdatedADDebtAccountSmallClaims.ServiceFee;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.AgencyFee == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.AgencyFee = objUpdatedADDebtAccountSmallClaims.AgencyFee;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.RecordingFee == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.RecordingFee = objUpdatedADDebtAccountSmallClaims.RecordingFee;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.AbstractFee == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.AbstractFee = objUpdatedADDebtAccountSmallClaims.AbstractFee;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.AbstractDate == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.AbstractDate = objUpdatedADDebtAccountSmallClaims.AbstractDate;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.AbstractRecordDate == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.AbstractRecordDate = objUpdatedADDebtAccountSmallClaims.AbstractRecordDate;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.AbstractNum == "" || objUpdatedADDebtAccountSmallClaims.AbstractNum == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.AbstractNum = objUpdatedADDebtAccountSmallClaims.AbstractNum.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.AbstractCounty == "" || objUpdatedADDebtAccountSmallClaims.AbstractCounty == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.AbstractCounty = objUpdatedADDebtAccountSmallClaims.AbstractCounty.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.WritFee == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.WritFee = objUpdatedADDebtAccountSmallClaims.WritFee;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.WritDate == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.WritDate = objUpdatedADDebtAccountSmallClaims.WritDate;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.WritCounty == "" || objUpdatedADDebtAccountSmallClaims.WritCounty == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.WritCounty = objUpdatedADDebtAccountSmallClaims.WritCounty.Trim();
                    }

                    if (objUpdatedADDebtAccountSmallClaims.LevyFee == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.LevyFee = objUpdatedADDebtAccountSmallClaims.LevyFee;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.LevyDate == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.LevyDate = objUpdatedADDebtAccountSmallClaims.LevyDate;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.ORAPFee == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.ORAPFee = objUpdatedADDebtAccountSmallClaims.ORAPFee;
                    }

                    if (objUpdatedADDebtAccountSmallClaims.ORAPDate == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADDebtAccountSmallClaimsOriginal.ORAPDate = objUpdatedADDebtAccountSmallClaims.ORAPDate;
                    }
                    #endregion

                    objDebtAccountSmallClaims = AutoMapper.Mapper.Map<DebtAccountSmallClaims>(objADDebtAccountSmallClaimsOriginal);
                    tblDebtAccountSmallClaims = (ITable)objDebtAccountSmallClaims;
                    DBO.Provider.Update(ref tblDebtAccountSmallClaims);
                }
                else
                {
                    objDebtAccountSmallClaims = AutoMapper.Mapper.Map<DebtAccountSmallClaims>(objUpdatedADDebtAccountSmallClaims);
                    tblDebtAccountSmallClaims = (ITable)objDebtAccountSmallClaims;
                    DBO.Provider.Create(ref tblDebtAccountSmallClaims);

                    //Record New Small Claims ActionHistory
                    var MYSPROC = new cview_Accounts_CreateActionHistory();
                    MYSPROC.DebtAccountId = objUpdatedADDebtAccountSmallClaims.DebtAccountId;
                    MYSPROC.ActionId = 5;
                    MYSPROC.UserId = user.Id;
                    MYSPROC.UserName = user.UserName;
                    DBO.Provider.ExecNonQuery(MYSPROC);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }


        public static DataTable GetDebtAccountSCPostPmtsDt(string DebtAccountId , DateTime JudgmentDate)
        {
            try
            {
                string mysql = "Select dbo.fn_Debt_GetSCPostPmts(" + DebtAccountId + ", '" + JudgmentDate + "') as Credit";

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(mysql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        public static DataTable GetDebtAccountSCPostCostsDt(string DebtAccountId, DateTime JudgmentDate)
        {
            try
            {
                string mysql = "Select dbo.fn_Debt_GetSCPostCosts(" + DebtAccountId + ", '" + JudgmentDate + "') as PostCosts";

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(mysql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        public static DataTable GetDebtAccountSCRecCostsDt(string DebtAccountId)
        {
            try
            {
                string mysql = "Select dbo.fn_Debt_GetSCRecCosts(" + DebtAccountId + ") as RecCosts";

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(mysql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

             public static DataTable GetDebtAccountSCCostsSpentDt(string DebtAccountId)
        {
            try
            {
                string mysql = "Select dbo.fn_Debt_GetSCCostsSpent(" + DebtAccountId + ") as CostsSpent";

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(mysql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }
        #endregion
    }
}
