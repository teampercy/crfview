﻿using System;
using System.Collections.Generic;
using System.Linq;
using CRF.BLL.CRFDB.TABLES;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System.Data;
using CRF.BLL.Providers;

namespace CRFView.Adapters.Collections.Collectors.WorkCard
{
    public class ADLawListInfo
    {
        #region Properties
        public int PKID { get; set; }
        public string LawList { get; set; }
        public string LawAdd { get; set; }
        public string LawCity { get; set; }
        public string LawSt { get; set; }
        public string LawZip { get; set; }
        public string LawListCode { get; set; }

        #endregion

        #region CRUD Operations
    
        //Read Details from database
        public static ADLawListInfo ReadLawLists(int id)
        {
            LawListInfo myentity = new LawListInfo();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (LawListInfo)myentityItable;
            AutoMapper.Mapper.CreateMap<LawListInfo, ADLawListInfo>();
            return (AutoMapper.Mapper.Map<ADLawListInfo>(myentity));
        }

     
        #endregion
    }
}
