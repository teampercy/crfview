﻿using CRF.BLL.CRFDB.SPROCS;
using CRFView.Adapters.Collections.DayEnd.Acks.ViewModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRF.BLL;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using Ionic.Zip;
using System.IO;
using CRF.BLL.CRFDB.VIEWS;
using CRFView.Adapters.Collections.Management.DeskAssignment.ViewModels;

namespace CRFView.Adapters
{
    public class ADvwAccountList
    {
        #region Properties
        public string DebtorName { get; set; }
        public string TaxId { get; set; }
        public string DriversLicense { get; set; }
        public bool IsUnwillingToPay { get; set; }
        public bool IsUnableToPay { get; set; }
        public System.DateTime BirthDate { get; set; }
        public bool HasReturnedChecks { get; set; }
        public string ClientCode { get; set; }
        public string ServiceDescr { get; set; }
        public string ClientName { get; set; }
        public bool IsIndAck { get; set; }
        public bool IsRegAck { get; set; }
        public bool IsCloseRpt { get; set; }
        public bool IsMasterRpt { get; set; }
        public bool IsPmtRpt { get; set; }
        public string ClientAddressLine1 { get; set; }
        public string ClientAddressLine2 { get; set; }
        public string ClientCity { get; set; }
        public string ClientState { get; set; }
        public string ClientPostalCode { get; set; }
        public bool IsTrustAccount { get; set; }
        public bool IsRolloverList { get; set; }
        public bool IsRollover { get; set; }
        public string ServiceCode { get; set; }
        public int DebtAccountId { get; set; }
        public int ClientId { get; set; }
        public int ContractId { get; set; }
        public int DebtId { get; set; }
        public string FileNumber { get; set; }
        public int LocationId { get; set; }
        public int CollectionStatusId { get; set; }
        public int TempOwnerId { get; set; }
        public string TempOwner { get; set; }
        public string BranchNum { get; set; }
        public string ClientAgentCode { get; set; }
        public string Priority { get; set; }
        public string AccountNo { get; set; }
        public string CloseCode { get; set; }
        public string ClaimNum { get; set; }
        public string DebtAccountNote { get; set; }
        public int Age { get; set; }
        public System.DateTime ReferalDate { get; set; }
        public System.DateTime LastServiceDate { get; set; }
        public System.DateTime LastPaymentDate { get; set; }
        public System.DateTime LastUpdateDate { get; set; }
        public System.DateTime LastTrustDate { get; set; }
        public System.DateTime NextContactDate { get; set; }
        public int NextContactNo { get; set; }
        public System.DateTime NextLetterDate { get; set; }
        public int NextLetterSeqNo { get; set; }
        public System.DateTime NextStatusDate { get; set; }
        public System.DateTime NextLegalDate { get; set; }
        public System.DateTime InterestFromDate { get; set; }
        public System.DateTime InterestThruDate { get; set; }
        public System.DateTime CloseDate { get; set; }
        public System.DateTime FirstReportDate { get; set; }
        public System.DateTime LastReportDate { get; set; }
        public System.DateTime NextReportDate { get; set; }
        public string CreditReportStatus { get; set; }
        public System.DateTime ClientLastViewDate { get; set; }
        public int ClientLastViewBy { get; set; }
        public bool IsEstAudit { get; set; }
        public bool IsSecondary { get; set; }
        public bool IsSkip { get; set; }
        public bool IsJudgmentAcct { get; set; }
        public bool IsIntlAcct { get; set; }
        public int TempId { get; set; }
        public string CollectionStatus { get; set; }
        public string Location { get; set; }
        public string FeeCode { get; set; }
        public int CommRateId { get; set; }
        public decimal TotAsgAmt { get; set; }
        public decimal TotAsgRcvAmt { get; set; }
        public decimal TotCollFeeAmt { get; set; }
        public decimal TotCollFeeRcvAmt { get; set; }
        public decimal TotAdjAmt { get; set; }
        public decimal TotPmtAmt { get; set; }
        public decimal TotBalAmt { get; set; }
        public decimal InterestRate { get; set; }
        public decimal TotIntAmt { get; set; }
        public decimal TotIntRcvAmt { get; set; }
        public decimal TotCostExpAmt { get; set; }
        public decimal TotCostRcvAmt { get; set; }
        public decimal TotAttyExpAmt { get; set; }
        public decimal TotAttyRcvAmt { get; set; }
        public decimal TotalDue { get; set; }
        public bool IsMainAddress { get; set; }
        public string AddressName { get; set; }
        public string ContactName { get; set; }
        public string ContactTitle { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string PostalCode { get; set; }
        public string County { get; set; }
        public string Country { get; set; }
        public string PhoneNo { get; set; }
        public string Fax { get; set; }
        public string Email { get; set; }
        public string PhoneNo2 { get; set; }
        public string TypeCode { get; set; }
        public string PhoneNoExt { get; set; }
        public string PhoneNo2Ext { get; set; }
        public string CellPhone { get; set; }
        public bool NoMailFlag { get; set; }
        public bool MailReturnFlag { get; set; }
        public bool BadAddressFlag { get; set; }
        public bool BadPhoneFlag { get; set; }
        public string ClientContactName { get; set; }
        public string ClientContactEmail { get; set; }
        public string ClientContactPhone { get; set; }
        public int BatchId { get; set; }
        public int BatchDebtAccountId { get; set; }
        public decimal TotLegalExpAmt { get; set; }
        public decimal TotRcvAmt { get; set; }
        public string CollectionStatusDescr { get; set; }
        public string CollectorEmail { get; set; }
        public string ClientContactTitle { get; set; }
        public string ClientContactSalutation { get; set; }
        public int StatusGroup { get; set; }
        public string CollectorName { get; set; }
        public string SalesNum { get; set; }
        public string SalesName { get; set; }
        public System.DateTime SetupDate { get; set; }
        public bool IsPrecollect { get; set; }
        public int SalesTeamId { get; set; }
        public decimal TotAdjNoCFAmt { get; set; }
        public decimal TotBalNoCFAmt { get; set; }
        public string ClientContact { get; set; }
        public string ClientTitle { get; set; }
        public string ClientAs { get; set; }
        public string ClientLegalName { get; set; }
        public string ClientEntity { get; set; }
        public string ClientDBA { get; set; }
        public byte[] Signature { get; set; }
        public string LegalAddr1 { get; set; }
        public string LegalAddr2 { get; set; }
        public string LegalCity { get; set; }
        public string LegalState { get; set; }
        public string LegalZip { get; set; }
        public string SCContactName { get; set; }
        public string SCContactTitle { get; set; }
        public string SCAddr1 { get; set; }
        public string SCAddr2 { get; set; }
        public string SCCity { get; set; }
        public string SCState { get; set; }
        public string SCZip { get; set; }
        public string SCPhone { get; set; }
        public string SCFax { get; set; }
        public string SCEmail { get; set; }
        public string FBN { get; set; }
        public System.DateTime ExpireDate { get; set; }
        public string FBNCounty { get; set; }
        public string ClientPhone { get; set; }
        public decimal TotAsgAmtEx { get; set; }
        public int CollectionRateId { get; set; }
        public bool MiscVar { get; set; }
        public bool IsRapidRebate { get; set; }
        public bool IsForwarding { get; set; }
        public bool IsSmallClaims { get; set; }
        public string ServiceType { get; set; }
        public string MatchingKey { get; set; }
        public string AddressKey { get; set; }
        public System.DateTime SCReviewDate { get; set; }
        public System.DateTime TrialDate { get; set; }
        public int ProcessServerId { get; set; }
        public int CourtId { get; set; }
        public string CaseNum { get; set; }
        public string EntityName1 { get; set; }
        public bool IsDropContact { get; set; }
        public int AttyId { get; set; }
        public bool IsNoCBR { get; set; }
        public bool IsNoCollectFee { get; set; }
        public bool IsCreditReport { get; set; }
        public System.DateTime SuitFiledDate { get; set; }
        public string BKCaseNo { get; set; }
        public string DebtorBKName { get; set; }
        public decimal TotCollFeeRcvAmtEx { get; set; }
        public System.DateTime LegalSetupDate { get; set; }
        public decimal LegalAssignAmt { get; set; }
        public decimal LegalRate { get; set; }
        public decimal TotFeeAmt { get; set; }
        public decimal TotAttyOwedAmt { get; set; }
        public int ParentDebtAccountId { get; set; }
        public string SCVenue { get; set; }
        public bool IsTenDayContactPlan { get; set; }
        public string CollectorPhone { get; set; }
        public string ClientFax { get; set; }
        public bool IsIntlClient { get; set; }
        public string SCContactAs { get; set; }
        public string SIFAuthority { get; set; }
        public string ClientTypeName { get; set; }
        public string ClientTypeCode { get; set; }
        public int ClientTypeId { get; set; }
        public decimal CanAccountAmt { get; set; }
        public System.DateTime JudgmentDate { get; set; }
        public System.DateTime FwdJudgmentDate { get; set; }
        public string SSN { get; set; }
        public int DebtAddressTypeId { get; set; }
        public decimal DebtAddressId { get; set; }
        public string BatchType { get; set; }
        public string SubmittedBy { get; set; }
        public bool IsStatusGroup { get; set; }
        public bool IsStatusRpt { get; set; }
        public System.DateTime PaymentDueDate { get; set; }
        public decimal UsageFee { get; set; }
        public decimal EarlyTerminationFee { get; set; }
        public string ServiceState { get; set; }
        #endregion
        
        //Print Letters
        public static string PrintAcks(AcksVM objAcksVM)
        {
            string pdfPath = null;
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                string ZipLocation = user.OutputFolder + "\\DebtAccountAckNotice-" + DateTime.Now.ToString("yyyyMMddhhmmss");

                if (System.IO.Directory.Exists(ZipLocation) == false)
                {
                    System.IO.Directory.CreateDirectory(ZipLocation);
                }

                user.OutputFolder = ZipLocation;


                var Sp_OBJ = new cview_Report_GetAcks();
                if(objAcksVM.ClientId != "0" && objAcksVM.ClientId != "" && objAcksVM.ClientId != null)
                {
                    Sp_OBJ.ClientId = objAcksVM.ClientId;
                }
                Sp_OBJ.DateFROM = objAcksVM.FromDate.ToShortDateString();
                Sp_OBJ.DateTO = objAcksVM.ThruDate.ToShortDateString();

                CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;
                if (objAcksVM.RbtPrintOption == "Send To PDF")
                {
                    mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                }

                DataSet myds = DBO.Provider.GetDataSet(Sp_OBJ);

                TableView myview = DBO.Provider.GetTableView(Sp_OBJ);

                int myidx = 0;
                DataTable mytbl = myds.Tables[1];

                if (myds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow mydr in (myds.Tables[0].Rows))
                    {
                        myidx += 1;
                        Globals.RenderC1Report(user,mytbl, "CollectViewReports.xml", "AckNotice", mymode, Convert.ToString(mydr["ClientCode"]), "", "");
                    }
                }

                string path = ZipLocation;//Location for inside Test Folder  
                string[] Filenames = Directory.GetFiles(path);
                string ZipFile = new DirectoryInfo(path).Name;
                if (Filenames.Length > 0)
                {
                    using (ZipFile zip = new ZipFile())
                    {
                        zip.AddFiles(Filenames, ZipFile);//Zip file inside filename  
                        zip.Save(ZipLocation + ".zip");//location and name for creating zip file  
                    }

                    pdfPath = ZipLocation + ".zip";
                }
                else
                {
                    pdfPath = "";
                }

            }

            catch (Exception ex)
            {
                return "";
            }
            return pdfPath;
        }

        
        public static DataTable GetDebtAccountInfoList(string DebtAccountId)
        {
            try
            {
                string sql = "Select * from vwAccountList where DebtAccountId = " + DebtAccountId;
                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        public static ADvwAccountList GetDebtAccontInfo(DataTable dt)
        {
            List<ADvwAccountList> Account = (from DataRow dr in dt.Rows
                                             select new ADvwAccountList()
                                             {
                                                 DebtorName = ((dr["DebtorName"] != DBNull.Value ? dr["DebtorName"].ToString() : "")),
                                                 TaxId = ((dr["TaxId"] != DBNull.Value ? dr["TaxId"].ToString() : "")),
                                                 DriversLicense = ((dr["DriversLicense"] != DBNull.Value ? dr["DriversLicense"].ToString() : "")),
                                                 IsUnwillingToPay = ((dr["IsUnwillingToPay"] != DBNull.Value ? Convert.ToBoolean(dr["IsUnwillingToPay"]) : false)),
                                                 IsUnableToPay = ((dr["IsUnableToPay"] != DBNull.Value ? Convert.ToBoolean(dr["IsUnableToPay"]) : false)),
                                                 BirthDate = ((dr["BirthDate"] != DBNull.Value ? Convert.ToDateTime(dr["BirthDate"]) : DateTime.MinValue)),
                                                 HasReturnedChecks = ((dr["HasReturnedChecks"] != DBNull.Value ? Convert.ToBoolean(dr["HasReturnedChecks"]) : false)),
                                                 ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                                 ServiceDescr = ((dr["ServiceDescr"] != DBNull.Value ? dr["ServiceDescr"].ToString() : "")),
                                                 ClientName = ((dr["ClientName"] != DBNull.Value ? dr["ClientName"].ToString() : "")),
                                                 IsIndAck = ((dr["IsIndAck"] != DBNull.Value ? Convert.ToBoolean(dr["IsIndAck"]) : false)),
                                                 IsRegAck = ((dr["IsRegAck"] != DBNull.Value ? Convert.ToBoolean(dr["IsRegAck"]) : false)),
                                                 IsCloseRpt = ((dr["IsCloseRpt"] != DBNull.Value ? Convert.ToBoolean(dr["IsCloseRpt"]) : false)),
                                                 IsMasterRpt = ((dr["IsMasterRpt"] != DBNull.Value ? Convert.ToBoolean(dr["IsMasterRpt"]) : false)),
                                                 IsPmtRpt = ((dr["IsPmtRpt"] != DBNull.Value ? Convert.ToBoolean(dr["IsPmtRpt"]) : false)),
                                                 ClientAddressLine1 = ((dr["ClientAddressLine1"] != DBNull.Value ? dr["ClientAddressLine1"].ToString() : "")),
                                                 ClientAddressLine2 = ((dr["ClientAddressLine2"] != DBNull.Value ? dr["ClientAddressLine2"].ToString() : "")),
                                                 ClientCity = ((dr["ClientCity"] != DBNull.Value ? dr["ClientCity"].ToString() : "")),
                                                 ClientState = ((dr["ClientState"] != DBNull.Value ? dr["ClientState"].ToString() : "")),
                                                 ClientPostalCode = ((dr["ClientPostalCode"] != DBNull.Value ? dr["ClientPostalCode"].ToString() : "")),
                                                 IsTrustAccount = ((dr["IsTrustAccount"] != DBNull.Value ? Convert.ToBoolean(dr["IsTrustAccount"]) : false)),
                                                 IsRolloverList = ((dr["IsRolloverList"] != DBNull.Value ? Convert.ToBoolean(dr["IsRolloverList"]) : false)),
                                                 IsRollover = ((dr["IsRollover"] != DBNull.Value ? Convert.ToBoolean(dr["IsRollover"]) : false)),
                                                 ServiceCode = ((dr["ServiceCode"] != DBNull.Value ? dr["ServiceCode"].ToString() : "")),
                                                 DebtAccountId = ((dr["DebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAccountId"]) : 0)),
                                                 ClientId = ((dr["ClientId"] != DBNull.Value ? Convert.ToInt32(dr["ClientId"]) : 0)),
                                                 ContractId = ((dr["ContractId"] != DBNull.Value ? Convert.ToInt32(dr["ContractId"]) : 0)),
                                                 DebtId = ((dr["DebtId"] != DBNull.Value ? Convert.ToInt32(dr["DebtId"]) : 0)),
                                                 FileNumber = ((dr["FileNumber"] != DBNull.Value ? dr["FileNumber"].ToString() : "")),
                                                 LocationId = ((dr["LocationId"] != DBNull.Value ? Convert.ToInt32(dr["LocationId"]) : 0)),
                                                 CollectionStatusId = ((dr["CollectionStatusId"] != DBNull.Value ? Convert.ToInt32(dr["CollectionStatusId"]) : 0)),
                                                 TempOwnerId = ((dr["TempOwnerId"] != DBNull.Value ? Convert.ToInt32(dr["TempOwnerId"]) : 0)),
                                                 TempOwner = ((dr["TempOwner"] != DBNull.Value ? dr["TempOwner"].ToString() : "")),
                                                 BranchNum = ((dr["BranchNum"] != DBNull.Value ? dr["BranchNum"].ToString() : "")),
                                                 ClientAgentCode = ((dr["ClientAgentCode"] != DBNull.Value ? dr["ClientAgentCode"].ToString() : "")),
                                                 Priority = ((dr["Priority"] != DBNull.Value ? dr["Priority"].ToString() : "")),
                                                 AccountNo = ((dr["AccountNo"] != DBNull.Value ? dr["AccountNo"].ToString() : "")),
                                                 CloseCode = ((dr["CloseCode"] != DBNull.Value ? dr["CloseCode"].ToString() : "")),
                                                 ClaimNum = ((dr["ClaimNum"] != DBNull.Value ? dr["ClaimNum"].ToString() : "")),
                                                 DebtAccountNote = ((dr["DebtAccountNote"] != DBNull.Value ? dr["DebtAccountNote"].ToString() : "")),
                                                 Age = ((dr["Age"] != DBNull.Value ? Convert.ToInt32(dr["Age"]) : 0)),
                                                 ReferalDate = ((dr["ReferalDate"] != DBNull.Value ? Convert.ToDateTime(dr["ReferalDate"]) : DateTime.MinValue)),
                                                 LastServiceDate = ((dr["LastServiceDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastServiceDate"]) : DateTime.MinValue)),
                                                 LastPaymentDate = ((dr["LastPaymentDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastPaymentDate"]) : DateTime.MinValue)),
                                                 LastUpdateDate = ((dr["LastUpdateDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastUpdateDate"]) : DateTime.MinValue)),
                                                 LastTrustDate = ((dr["LastTrustDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastTrustDate"]) : DateTime.MinValue)),
                                                 NextContactDate = ((dr["NextContactDate"] != DBNull.Value ? Convert.ToDateTime(dr["NextContactDate"]) : DateTime.MinValue)),
                                                 NextContactNo = ((dr["NextContactNo"] != DBNull.Value ? Convert.ToInt32(dr["NextContactNo"]) : 0)),
                                                 NextLetterDate = ((dr["NextLetterDate"] != DBNull.Value ? Convert.ToDateTime(dr["NextLetterDate"]) : DateTime.MinValue)),
                                                 NextLetterSeqNo = ((dr["NextLetterSeqNo"] != DBNull.Value ? Convert.ToInt32(dr["NextLetterSeqNo"]) : 0)),
                                                 NextStatusDate = ((dr["NextStatusDate"] != DBNull.Value ? Convert.ToDateTime(dr["NextStatusDate"]) : DateTime.MinValue)),
                                                 NextLegalDate = ((dr["NextLegalDate"] != DBNull.Value ? Convert.ToDateTime(dr["NextLegalDate"]) : DateTime.MinValue)),
                                                 InterestFromDate = ((dr["InterestFromDate"] != DBNull.Value ? Convert.ToDateTime(dr["InterestFromDate"]) : DateTime.MinValue)),
                                                 InterestThruDate = ((dr["InterestThruDate"] != DBNull.Value ? Convert.ToDateTime(dr["InterestThruDate"]) : DateTime.MinValue)),
                                                 CloseDate = ((dr["CloseDate"] != DBNull.Value ? Convert.ToDateTime(dr["CloseDate"]) : DateTime.MinValue)),
                                                 FirstReportDate = ((dr["FirstReportDate"] != DBNull.Value ? Convert.ToDateTime(dr["FirstReportDate"]) : DateTime.MinValue)),
                                                 LastReportDate = ((dr["LastReportDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastReportDate"]) : DateTime.MinValue)),
                                                 NextReportDate = ((dr["NextReportDate"] != DBNull.Value ? Convert.ToDateTime(dr["NextReportDate"]) : DateTime.MinValue)),
                                                 CreditReportStatus = ((dr["CreditReportStatus"] != DBNull.Value ? dr["CreditReportStatus"].ToString() : "")),
                                                 ClientLastViewDate = ((dr["ClientLastViewDate"] != DBNull.Value ? Convert.ToDateTime(dr["ClientLastViewDate"]) : DateTime.MinValue)),
                                                 ClientLastViewBy = ((dr["ClientLastViewBy"] != DBNull.Value ? Convert.ToInt32(dr["ClientLastViewBy"]) : 0)),
                                                 IsEstAudit = ((dr["IsEstAudit"] != DBNull.Value ? Convert.ToBoolean(dr["IsEstAudit"]) : false)),
                                                 IsSecondary = ((dr["IsSecondary"] != DBNull.Value ? Convert.ToBoolean(dr["IsSecondary"]) : false)),
                                                 IsSkip = ((dr["IsSkip"] != DBNull.Value ? Convert.ToBoolean(dr["IsSkip"]) : false)),
                                                 IsJudgmentAcct = ((dr["IsJudgmentAcct"] != DBNull.Value ? Convert.ToBoolean(dr["IsJudgmentAcct"]) : false)),
                                                 IsIntlAcct = ((dr["IsIntlAcct"] != DBNull.Value ? Convert.ToBoolean(dr["IsIntlAcct"]) : false)),
                                                 TempId = ((dr["TempId"] != DBNull.Value ? Convert.ToInt32(dr["TempId"]) : 0)),
                                                 CollectionStatus = ((dr["CollectionStatus"] != DBNull.Value ? dr["CollectionStatus"].ToString() : "")),
                                                 Location = ((dr["Location"] != DBNull.Value ? dr["Location"].ToString() : "")),
                                                 FeeCode = ((dr["FeeCode"] != DBNull.Value ? dr["FeeCode"].ToString() : "")),
                                                 CommRateId = ((dr["CommRateId"] != DBNull.Value ? Convert.ToInt32(dr["CommRateId"]) : 0)),
                                                 TotAsgAmt = ((dr["TotAsgAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotAsgAmt"]) : 0)),
                                                 TotAsgRcvAmt = ((dr["TotAsgRcvAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotAsgRcvAmt"]) : 0)),
                                                 TotCollFeeAmt = ((dr["TotCollFeeAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotCollFeeAmt"]) : 0)),
                                                 TotCollFeeRcvAmt = ((dr["TotCollFeeRcvAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotCollFeeRcvAmt"]) : 0)),
                                                 TotAdjAmt = ((dr["TotAdjAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotAdjAmt"]) : 0)),
                                                 TotPmtAmt = ((dr["TotPmtAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotPmtAmt"]) : 0)),
                                                 TotBalAmt = ((dr["TotBalAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotBalAmt"]) : 0)),
                                                 InterestRate = ((dr["InterestRate"] != DBNull.Value ? Convert.ToDecimal(dr["InterestRate"]) : 0)),
                                                 TotIntAmt = ((dr["TotIntAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotIntAmt"]) : 0)),
                                                 TotIntRcvAmt = ((dr["TotIntRcvAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotIntRcvAmt"]) : 0)),
                                                 TotCostExpAmt = ((dr["TotCostExpAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotCostExpAmt"]) : 0)),
                                                 TotCostRcvAmt = ((dr["TotCostRcvAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotCostRcvAmt"]) : 0)),
                                                 TotAttyExpAmt = ((dr["TotAttyExpAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotAttyExpAmt"]) : 0)),
                                                 TotAttyRcvAmt = ((dr["TotAttyRcvAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotAttyRcvAmt"]) : 0)),
                                                 TotalDue = ((dr["TotalDue"] != DBNull.Value ? Convert.ToDecimal(dr["TotalDue"]) : 0)),
                                                 IsMainAddress = ((dr["IsMainAddress"] != DBNull.Value ? Convert.ToBoolean(dr["IsMainAddress"]) : false)),
                                                 AddressName = ((dr["AddressName"] != DBNull.Value ? dr["AddressName"].ToString() : "")),
                                                 ContactName = ((dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : "")),
                                                 ContactTitle = ((dr["ContactTitle"] != DBNull.Value ? dr["ContactTitle"].ToString() : "")),
                                                 AddressLine1 = ((dr["AddressLine1"] != DBNull.Value ? dr["AddressLine1"].ToString() : "")),
                                                 AddressLine2 = ((dr["AddressLine2"] != DBNull.Value ? dr["AddressLine2"].ToString() : "")),
                                                 City = ((dr["City"] != DBNull.Value ? dr["City"].ToString() : "")),
                                                 State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),
                                                 PostalCode = ((dr["PostalCode"] != DBNull.Value ? dr["PostalCode"].ToString() : "")),
                                                 County = ((dr["County"] != DBNull.Value ? dr["County"].ToString() : "")),
                                                 Country = ((dr["Country"] != DBNull.Value ? dr["Country"].ToString() : "")),
                                                 PhoneNo = ((dr["PhoneNo"] != DBNull.Value ? dr["PhoneNo"].ToString() : "")),
                                                 Fax = ((dr["Fax"] != DBNull.Value ? dr["Fax"].ToString() : "")),
                                                 Email = ((dr["Email"] != DBNull.Value ? dr["Email"].ToString() : "")),
                                                 PhoneNo2 = ((dr["PhoneNo2"] != DBNull.Value ? dr["PhoneNo2"].ToString() : "")),
                                                 TypeCode = ((dr["TypeCode"] != DBNull.Value ? dr["TypeCode"].ToString() : "")),
                                                 PhoneNoExt = ((dr["PhoneNoExt"] != DBNull.Value ? dr["PhoneNoExt"].ToString() : "")),
                                                 PhoneNo2Ext = ((dr["PhoneNo2Ext"] != DBNull.Value ? dr["PhoneNo2Ext"].ToString() : "")),
                                                 CellPhone = ((dr["CellPhone"] != DBNull.Value ? dr["CellPhone"].ToString() : "")),
                                                 NoMailFlag = ((dr["NoMailFlag"] != DBNull.Value ? Convert.ToBoolean(dr["NoMailFlag"]) : false)),
                                                 MailReturnFlag = ((dr["MailReturnFlag"] != DBNull.Value ? Convert.ToBoolean(dr["MailReturnFlag"]) : false)),
                                                 BadAddressFlag = ((dr["BadAddressFlag"] != DBNull.Value ? Convert.ToBoolean(dr["BadAddressFlag"]) : false)),
                                                 BadPhoneFlag = ((dr["BadPhoneFlag"] != DBNull.Value ? Convert.ToBoolean(dr["BadPhoneFlag"]) : false)),
                                                 ClientContactName = ((dr["ClientContactName"] != DBNull.Value ? dr["ClientContactName"].ToString() : "")),
                                                 ClientContactEmail = ((dr["ClientContactEmail"] != DBNull.Value ? dr["ClientContactEmail"].ToString() : "")),
                                                 ClientContactPhone = ((dr["ClientContactPhone"] != DBNull.Value ? dr["ClientContactPhone"].ToString() : "")),
                                                 BatchId = ((dr["BatchId"] != DBNull.Value ? Convert.ToInt32(dr["BatchId"]) : 0)),
                                                 BatchDebtAccountId = ((dr["BatchDebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["BatchDebtAccountId"]) : 0)),
                                                 TotLegalExpAmt = ((dr["TotLegalExpAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotLegalExpAmt"]) : 0)),
                                                 TotRcvAmt = ((dr["TotRcvAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotRcvAmt"]) : 0)),
                                                 CollectionStatusDescr = ((dr["CollectionStatusDescr"] != DBNull.Value ? dr["CollectionStatusDescr"].ToString() : "")),
                                                 CollectorEmail = ((dr["CollectorEmail"] != DBNull.Value ? dr["CollectorEmail"].ToString() : "")),
                                                 ClientContactTitle = ((dr["ClientContactTitle"] != DBNull.Value ? dr["ClientContactTitle"].ToString() : "")),
                                                 ClientContactSalutation = ((dr["ClientContactSalutation"] != DBNull.Value ? dr["ClientContactSalutation"].ToString() : "")),
                                                 StatusGroup = ((dr["StatusGroup"] != DBNull.Value ? Convert.ToInt32(dr["StatusGroup"]) : 0)),
                                                 CollectorName = ((dr["CollectorName"] != DBNull.Value ? dr["CollectorName"].ToString() : "")),
                                                 SalesNum = ((dr["SalesNum"] != DBNull.Value ? dr["SalesNum"].ToString() : "")),
                                                 SalesName = ((dr["SalesName"] != DBNull.Value ? dr["SalesName"].ToString() : "")),
                                                 SetupDate = ((dr["SetupDate"] != DBNull.Value ? Convert.ToDateTime(dr["SetupDate"]) : DateTime.MinValue)),
                                                 IsPrecollect = ((dr["IsPrecollect"] != DBNull.Value ? Convert.ToBoolean(dr["IsPrecollect"]) : false)),
                                                 SalesTeamId = ((dr["SalesTeamId"] != DBNull.Value ? Convert.ToInt32(dr["SalesTeamId"]) : 0)),
                                                 TotAdjNoCFAmt = ((dr["TotAdjNoCFAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotAdjNoCFAmt"]) : 0)),
                                                 TotBalNoCFAmt = ((dr["TotBalNoCFAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotBalNoCFAmt"]) : 0)),
                                                 ClientContact = ((dr["ClientContact"] != DBNull.Value ? dr["ClientContact"].ToString() : "")),
                                                 ClientTitle = ((dr["ClientTitle"] != DBNull.Value ? dr["ClientTitle"].ToString() : "")),
                                                 ClientAs = ((dr["ClientAs"] != DBNull.Value ? dr["ClientAs"].ToString() : "")),
                                                 ClientLegalName = ((dr["ClientLegalName"] != DBNull.Value ? dr["ClientLegalName"].ToString() : "")),
                                                 ClientEntity = ((dr["ClientEntity"] != DBNull.Value ? dr["ClientEntity"].ToString() : "")),
                                                 ClientDBA = ((dr["ClientDBA"] != DBNull.Value ? dr["ClientDBA"].ToString() : "")),
                                                 // Signature = ((dr["Signature"] != DBNull.Value ? Convert.ToByte( dr["Signature"]) : )),
                                                 LegalAddr1 = ((dr["LegalAddr1"] != DBNull.Value ? dr["LegalAddr1"].ToString() : "")),
                                                 LegalAddr2 = ((dr["LegalAddr2"] != DBNull.Value ? dr["LegalAddr2"].ToString() : "")),
                                                 LegalCity = ((dr["LegalCity"] != DBNull.Value ? dr["LegalCity"].ToString() : "")),
                                                 LegalState = ((dr["LegalState"] != DBNull.Value ? dr["LegalState"].ToString() : "")),
                                                 LegalZip = ((dr["LegalZip"] != DBNull.Value ? dr["LegalZip"].ToString() : "")),
                                                 SCContactName = ((dr["SCContactName"] != DBNull.Value ? dr["SCContactName"].ToString() : "")),
                                                 SCContactTitle = ((dr["SCContactTitle"] != DBNull.Value ? dr["SCContactTitle"].ToString() : "")),
                                                 SCAddr1 = ((dr["SCAddr1"] != DBNull.Value ? dr["SCAddr1"].ToString() : "")),
                                                 SCAddr2 = ((dr["SCAddr2"] != DBNull.Value ? dr["SCAddr2"].ToString() : "")),
                                                 SCCity = ((dr["SCCity"] != DBNull.Value ? dr["SCCity"].ToString() : "")),
                                                 SCState = ((dr["SCState"] != DBNull.Value ? dr["SCState"].ToString() : "")),
                                                 SCZip = ((dr["SCZip"] != DBNull.Value ? dr["SCZip"].ToString() : "")),
                                                 SCPhone = ((dr["SCPhone"] != DBNull.Value ? dr["SCPhone"].ToString() : "")),
                                                 SCFax = ((dr["SCFax"] != DBNull.Value ? dr["SCFax"].ToString() : "")),
                                                 SCEmail = ((dr["SCEmail"] != DBNull.Value ? dr["SCEmail"].ToString() : "")),
                                                 FBN = ((dr["FBN"] != DBNull.Value ? dr["FBN"].ToString() : "")),
                                                 ExpireDate = ((dr["ExpireDate"] != DBNull.Value ? Convert.ToDateTime(dr["ExpireDate"]) : DateTime.MinValue)),
                                                 FBNCounty = ((dr["FBNCounty"] != DBNull.Value ? dr["FBNCounty"].ToString() : "")),
                                                 ClientPhone = ((dr["ClientPhone"] != DBNull.Value ? dr["ClientPhone"].ToString() : "")),
                                                 TotAsgAmtEx = ((dr["TotAsgAmtEx"] != DBNull.Value ? Convert.ToDecimal(dr["TotAsgAmtEx"]) : 0)),
                                                 CollectionRateId = ((dr["CollectionRateId"] != DBNull.Value ? Convert.ToInt32(dr["CollectionRateId"]) : 0)),
                                                 MiscVar = ((dr["MiscVar"] != DBNull.Value ? Convert.ToBoolean(dr["MiscVar"]) : false)),
                                                 IsRapidRebate = ((dr["IsRapidRebate"] != DBNull.Value ? Convert.ToBoolean(dr["IsRapidRebate"]) : false)),
                                                 IsForwarding = ((dr["IsForwarding"] != DBNull.Value ? Convert.ToBoolean(dr["IsForwarding"]) : false)),
                                                 IsSmallClaims = ((dr["IsSmallClaims"] != DBNull.Value ? Convert.ToBoolean(dr["IsSmallClaims"]) : false)),
                                                 ServiceType = ((dr["ServiceType"] != DBNull.Value ? dr["ServiceType"].ToString() : "")),
                                                 MatchingKey = ((dr["MatchingKey"] != DBNull.Value ? dr["MatchingKey"].ToString() : "")),
                                                 AddressKey = ((dr["AddressKey"] != DBNull.Value ? dr["AddressKey"].ToString() : "")),
                                                 SCReviewDate = ((dr["SCReviewDate"] != DBNull.Value ? Convert.ToDateTime(dr["SCReviewDate"]) : DateTime.MinValue)),
                                                 TrialDate = ((dr["TrialDate"] != DBNull.Value ? Convert.ToDateTime(dr["TrialDate"]) : DateTime.MinValue)),
                                                 ProcessServerId = ((dr["ProcessServerId"] != DBNull.Value ? Convert.ToInt32(dr["ProcessServerId"]) : 0)),
                                                 CourtId = ((dr["CourtId"] != DBNull.Value ? Convert.ToInt32(dr["CourtId"]) : 0)),
                                                 CaseNum = ((dr["CaseNum"] != DBNull.Value ? dr["CaseNum"].ToString() : "")),
                                                 EntityName1 = ((dr["EntityName1"] != DBNull.Value ? dr["EntityName1"].ToString() : "")),
                                                 IsDropContact = ((dr["IsDropContact"] != DBNull.Value ? Convert.ToBoolean(dr["IsDropContact"]) : false)),
                                                 AttyId = ((dr["AttyId"] != DBNull.Value ? Convert.ToInt32(dr["AttyId"]) : 0)),
                                                 IsNoCBR = ((dr["IsNoCBR"] != DBNull.Value ? Convert.ToBoolean(dr["IsNoCBR"]) : false)),
                                                 IsNoCollectFee = ((dr["IsNoCollectFee"] != DBNull.Value ? Convert.ToBoolean(dr["IsNoCollectFee"]) : false)),
                                                 IsCreditReport = ((dr["IsCreditReport"] != DBNull.Value ? Convert.ToBoolean(dr["IsCreditReport"]) : false)),
                                                 SuitFiledDate = ((dr["SuitFiledDate"] != DBNull.Value ? Convert.ToDateTime(dr["SuitFiledDate"]) : DateTime.MinValue)),
                                                 BKCaseNo = ((dr["BKCaseNo"] != DBNull.Value ? dr["BKCaseNo"].ToString() : "")),
                                                 DebtorBKName = ((dr["DebtorBKName"] != DBNull.Value ? dr["DebtorBKName"].ToString() : "")),
                                                 TotCollFeeRcvAmtEx = ((dr["TotCollFeeRcvAmtEx"] != DBNull.Value ? Convert.ToDecimal(dr["TotCollFeeRcvAmtEx"]) : 0)),
                                                 LegalSetupDate = ((dr["LegalSetupDate"] != DBNull.Value ? Convert.ToDateTime(dr["LegalSetupDate"]) : DateTime.MinValue)),
                                                 LegalAssignAmt = ((dr["LegalAssignAmt"] != DBNull.Value ? Convert.ToDecimal(dr["LegalRate"]) : 0)),
                                                 LegalRate = ((dr["LegalRate"] != DBNull.Value ? Convert.ToDecimal(dr["TotCollFeeRcvAmtEx"]) : 0)),
                                                 TotFeeAmt = ((dr["TotFeeAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotFeeAmt"]) : 0)),
                                                 TotAttyOwedAmt = ((dr["TotAttyOwedAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotAttyOwedAmt"]) : 0)),
                                                 ParentDebtAccountId = ((dr["ParentDebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["ParentDebtAccountId"]) : 0)),
                                                 SCVenue = ((dr["SCVenue"] != DBNull.Value ? dr["SCVenue"].ToString() : "")),
                                                 IsTenDayContactPlan = ((dr["IsTenDayContactPlan"] != DBNull.Value ? Convert.ToBoolean(dr["IsTenDayContactPlan"]) : false)),
                                                 CollectorPhone = ((dr["CollectorPhone"] != DBNull.Value ? dr["CollectorPhone"].ToString() : "")),
                                                 ClientFax = ((dr["ClientFax"] != DBNull.Value ? dr["ClientFax"].ToString() : "")),
                                                 IsIntlClient = ((dr["IsIntlClient"] != DBNull.Value ? Convert.ToBoolean(dr["IsIntlClient"]) : false)),
                                                 SCContactAs = ((dr["SCContactAs"] != DBNull.Value ? dr["SCContactAs"].ToString() : "")),
                                                 SIFAuthority = ((dr["SIFAuthority"] != DBNull.Value ? dr["SIFAuthority"].ToString() : "")),
                                                 ClientTypeName = ((dr["ClientTypeName"] != DBNull.Value ? dr["ClientTypeName"].ToString() : "")),
                                                 ClientTypeCode = ((dr["ClientTypeCode"] != DBNull.Value ? dr["ClientTypeCode"].ToString() : "")),
                                                 ClientTypeId = ((dr["ClientTypeId"] != DBNull.Value ? Convert.ToInt32(dr["ClientTypeId"]) : 0)),
                                                 CanAccountAmt = ((dr["CanAccountAmt"] != DBNull.Value ? Convert.ToDecimal(dr["CanAccountAmt"]) : 0)),
                                                 JudgmentDate = ((dr["JudgmentDate"] != DBNull.Value ? Convert.ToDateTime(dr["JudgmentDate"]) : DateTime.MinValue)),
                                                 FwdJudgmentDate = ((dr["FwdJudgmentDate"] != DBNull.Value ? Convert.ToDateTime(dr["FwdJudgmentDate"]) : DateTime.MinValue)),
                                                 SSN = ((dr["SSN"] != DBNull.Value ? dr["SSN"].ToString() : "")),
                                                 DebtAddressTypeId = ((dr["DebtAddressTypeId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAddressTypeId"]) : 0)),
                                                 DebtAddressId = ((dr["DebtAddressId"] != DBNull.Value ? Convert.ToDecimal(dr["DebtAddressId"]) : 0)),
                                                 BatchType = ((dr["BatchType"] != DBNull.Value ? dr["BatchType"].ToString() : "")),
                                                 SubmittedBy = ((dr["SubmittedBy"] != DBNull.Value ? dr["SubmittedBy"].ToString() : "")),
                                                 IsStatusGroup = ((dr["IsStatusGroup"] != DBNull.Value ? Convert.ToBoolean(dr["IsStatusGroup"]) : false)),
                                                 IsStatusRpt = ((dr["IsStatusRpt"] != DBNull.Value ? Convert.ToBoolean(dr["IsStatusRpt"]) : false)),
                                                 PaymentDueDate = ((dr["PaymentDueDate"] != DBNull.Value ? Convert.ToDateTime(dr["PaymentDueDate"]) : DateTime.MinValue)),
                                                 UsageFee = ((dr["UsageFee"] != DBNull.Value ? Convert.ToDecimal(dr["UsageFee"]) : 0)),
                                                 EarlyTerminationFee = ((dr["EarlyTerminationFee"] != DBNull.Value ? Convert.ToDecimal(dr["EarlyTerminationFee"]) : 0)),
                                                 ServiceState = ((dr["EarlyTerminationFee"] != DBNull.Value ? dr["EarlyTerminationFee"].ToString() : "")),
                                             }).ToList();
            return Account.First();
        }
        
    

        public static DataTable GetAccountDetailDt(DeskAssignmentVM objDeskAssignmentVM)
        {
            try
            {
                var sp_obj = new cview_Processing_MassActionAndDeskChange();

                if (objDeskAssignmentVM.RbtActionChangeOption == "Desk Change")
                {
                    sp_obj.ActionType = 1;
                }

                if (objDeskAssignmentVM.RbtActionChangeOption == "Status Change")
                {
                    sp_obj.ActionType = 1;
                }

                if(objDeskAssignmentVM.chkAllClients == false)
                {
                    sp_obj.ClientId =Convert.ToInt32(objDeskAssignmentVM.ddlAllClientId);
                }
                else
                {
                    sp_obj.ClientId = 0;
                }

                sp_obj.FromDate =objDeskAssignmentVM.AssignedFrom.ToShortDateString();
                sp_obj.ThruDate = objDeskAssignmentVM.AssignedThru.ToShortDateString();

                if (objDeskAssignmentVM.chkAllDesks == false)
                {
                    sp_obj.DeskNumId = Convert.ToInt32(objDeskAssignmentVM.ddlAllDeskId);
                }
                else
                {
                    sp_obj.DeskNumId = 0;
                }

                if (objDeskAssignmentVM.chkAllStatus == false)
                {
                    sp_obj.StatusCodeId = Convert.ToInt32(objDeskAssignmentVM.ddlAllStatusId);
                }
                else
                {
                    sp_obj.StatusCodeId = 0;
                }

                if (!string.IsNullOrEmpty(objDeskAssignmentVM.DebtAccountId))
                {
                    sp_obj.DebtAccountId = Convert.ToInt32(objDeskAssignmentVM.DebtAccountId);
                }
                else
                {
                    sp_obj.DebtAccountId = 0;
                }
                 
                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sp_obj);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        public static List<ADvwAccountList> GetAccountDetailList(DataTable dt)
        {
            List<ADvwAccountList> Notes = (from DataRow dr in dt.Rows 
                                               select new ADvwAccountList()
                                               {
                                                   DebtorName = ((dr["DebtorName"] != DBNull.Value ? dr["DebtorName"].ToString() : "")),
                                                   ClientCode = ((dr["ClientCode"] != DBNull.Value ? dr["ClientCode"].ToString() : "")),
                                                   ClientName = ((dr["ClientName"] != DBNull.Value ? dr["ClientName"].ToString() : "")),
                                                   DebtAccountId = ((dr["DebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAccountId"]) : 0)),
                                                   BranchNum = ((dr["BranchNum"] != DBNull.Value ? dr["BranchNum"].ToString() : "")),
                                                   ReferalDate = ((dr["ReferalDate"] != DBNull.Value ? Convert.ToDateTime(dr["ReferalDate"]) : DateTime.MinValue)),
                                                   LastServiceDate = ((dr["LastServiceDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastServiceDate"]) : DateTime.MinValue)),
                                                   NextContactDate = ((dr["NextContactDate"] != DBNull.Value ? Convert.ToDateTime(dr["NextContactDate"]) : DateTime.MinValue)),
                                                   CollectionStatus = ((dr["CollectionStatus"] != DBNull.Value ? dr["CollectionStatus"].ToString() : "")),
                                                   Location = ((dr["Location"] != DBNull.Value ? dr["Location"].ToString() : "")),
                                                   TotAsgAmt = ((dr["TotAsgAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotAsgAmt"]) : 0)),
                                                   State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),
                                              }).ToList();
            return Notes;
        }
    }


}
