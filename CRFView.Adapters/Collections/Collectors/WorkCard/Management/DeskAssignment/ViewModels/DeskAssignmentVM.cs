﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Collections.Management.DeskAssignment.ViewModels
{
    public class DeskAssignmentVM
    {
        public DeskAssignmentVM()
        {
            objDebtAccount = new ADDebtAccount();
            AllClientIdList = CommonBindList.GetClientIdList();
            AllDeskIdList = CommonBindList.GetDeskIdList();
            AllStatusIdList = CommonBindList.GetStatusIdList();
            DeskAssignmentList = new List<ADvwAccountList>();

            NewDeskIdList = CommonBindList.GetNewDeskIdList();
            NewActionIdList = CommonBindList.GetNewActionIdList();
        }

        public string RbtActionChangeOption { get; set; }
        public DateTime AssignedFrom { get; set; }
        public DateTime AssignedThru { get; set; }
        public string DebtAccountId { get; set; }

        public bool chkAllClients { get; set; }        
        public string ddlAllClientId { get; set; }
        public IEnumerable<SelectListItem> AllClientIdList { get; set; }

        public bool chkAllDesks { get; set; }
        public string ddlAllDeskId { get; set; }
        public IEnumerable<SelectListItem> AllDeskIdList { get; set; }

        public bool chkAllStatus { get; set; }
        public string ddlAllStatusId { get; set; }
        public IEnumerable<SelectListItem> AllStatusIdList { get; set; }

        public DataTable dtDeskAssignmentList { get; set; }

        public List<ADvwAccountList>  DeskAssignmentList { get; set; }

        public string ddlNewDeskId { get; set; }
        public IEnumerable<SelectListItem> NewDeskIdList { get; set; }

        public string ddlNewActionId { get; set; }
        public IEnumerable<SelectListItem> NewActionIdList { get; set; }
        
        public ADDebtAccount objDebtAccount { get; set; }

        public string RbtNewOption { get; set; }
    }
}
