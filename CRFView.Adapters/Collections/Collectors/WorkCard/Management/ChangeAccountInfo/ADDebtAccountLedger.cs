﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADDebtAccountLedger
    {
        #region Properties
        public int DebtAccountLedgerId { get; set; }
        public int DebtAccountId { get; set; }
        public int ClientFinDocId { get; set; }
        public int BatchId { get; set; }
        public int LedgerTypeId { get; set; }
        public string LedgerType { get; set; }
        public string CollectionStatus { get; set; }
        public string TrxCode { get; set; }
        public DateTime TrxDate { get; set; }
        public DateTime ReceiptDate { get; set; }
        public DateTime RemitDate { get; set; }
        public DateTime InvoicedDate { get; set; }
        public decimal TrxAmt { get; set; }
        public decimal AsgAmt { get; set; }
        public decimal AsgRcvAmt { get; set; }
        public decimal FeeRcvAmt { get; set; }
        public decimal AdjAmt { get; set; }
        public decimal IntRcvAmt { get; set; }
        public decimal CostRcvAmt { get; set; }
        public decimal AttyRcvAmt { get; set; }
        public decimal Rate { get; set; }
        public decimal PmtAmt { get; set; }
        public decimal BillableAmt { get; set; }
        public decimal IntSharedAmt { get; set; }
        public decimal FeeAmt { get; set; }
        public decimal BilledAmt { get; set; }
        public decimal RemitAmt { get; set; }
        public decimal OvrPmtAmt { get; set; }
        public decimal AttyFeeAmt { get; set; }
        public decimal RcvByAgencyAmt { get; set; }
        public decimal RcvByClientAmt { get; set; }
        public decimal FeeByAgencyAmt { get; set; }
        public decimal FeeByClientAmt { get; set; }
        public decimal DueFromClientAmt { get; set; }
        public decimal RemitToClientAmt { get; set; }
        public decimal PrincDueAmt { get; set; }
        public decimal TotalDueAmt { get; set; }
        public bool IsRemitted { get; set; }
        public bool IsRcvdbyClient { get; set; }
        public bool IsOmitFromStmt { get; set; }
        public bool IsOmitFromCashRpt { get; set; }
        public bool IsInterestShared { get; set; }
        public bool IsOnHold { get; set; }
        public bool IsNSF { get; set; }
        public string Reference { get; set; }
        public string LedgerNote { get; set; }
        public int DeskNumId { get; set; }
        public string DeskNum { get; set; }
        public string StmtGrouping { get; set; }
        public int EnteredByUserId { get; set; }
        public string EnteredByUserCode { get; set; }
        public decimal AttyOwedAmt { get; set; }
        public bool IsConverted { get; set; }
        public string InvoiceBreak { get; set; }
        public decimal CollectFeeAmt { get; set; }
        public decimal FeeByAgencyAmtCF { get; set; }
        public decimal FeeByClientAmtCF { get; set; }
        public int AltKeyId { get; set; }
        public string SubLedgerType { get; set; }
        public bool IsAdvanceCC { get; set; }
        public int SubLedgerTypeId { get; set; }
        public decimal ExchangeRate { get; set; }
        public decimal CostRcvAmtOnPIF { get; set; }
        public string BankName { get; set; }
        public string BankAcctNum { get; set; }
        public string CheckMaker { get; set; }
        public decimal IntShareRate { get; set; }
        public decimal RebateAmt { get; set; }

        #endregion

        #region CRUD
        public static ADDebtAccountLedger ReadDebtAccountLedger(int DebtAccountLedgerId)
        { 
            DebtAccountLedger myentity = new DebtAccountLedger();
            ITable myentityItable = myentity;
            DBO.Provider.Read(DebtAccountLedgerId.ToString(), ref myentityItable);
            myentity = (DebtAccountLedger)myentityItable;
            AutoMapper.Mapper.CreateMap<DebtAccountLedger, ADDebtAccountLedger>();
            return (AutoMapper.Mapper.Map<ADDebtAccountLedger>(myentity));
        }

        public static ADDebtAccountLedger ReadDebtAccountLedgerByDebtAccountId(string DebtAccountId)
        {
            try
            {
                string mysql = "select * from DebtAccountLedger where LedgerType = 'ASG' and debtaccountid= '" + DebtAccountId + "'";
                var DebtAccountLedgerTableView = DBO.Provider.GetTableView(mysql);
                DataTable dt = DebtAccountLedgerTableView.ToTable();
                return ConvertDataTableToClass(dt);
            }
            catch (Exception ex)
            {
                return new ADDebtAccountLedger();
            }
        }


        public static int SaveDebtAccountLedger(ADDebtAccountLedger objUpdatedADDebtAccountLedger)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADDebtAccountLedger, DebtAccountLedger>();
                ITable tblDebtAccountLedger;
                DebtAccountLedger objDebtAccountLedger;

                if (objUpdatedADDebtAccountLedger.DebtAccountLedgerId != 0)
                {
                    ADDebtAccountLedger objADDebtAccountLedgerOriginal = ReadDebtAccountLedger(Convert.ToInt32(objUpdatedADDebtAccountLedger.DebtAccountLedgerId));

                    if (objADDebtAccountLedgerOriginal.TrxAmt == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLedgerOriginal.TrxAmt = objUpdatedADDebtAccountLedger.TrxAmt;
                    }

                    if (objADDebtAccountLedgerOriginal.AsgAmt == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLedgerOriginal.AsgAmt = objUpdatedADDebtAccountLedger.AsgAmt;
                    }


                    objDebtAccountLedger = AutoMapper.Mapper.Map<DebtAccountLedger>(objADDebtAccountLedgerOriginal);
                    tblDebtAccountLedger = (ITable)objDebtAccountLedger;
                    DBO.Provider.Update(ref tblDebtAccountLedger);
                }
                else
                {
                    objDebtAccountLedger = AutoMapper.Mapper.Map<DebtAccountLedger>(objUpdatedADDebtAccountLedger);
                    tblDebtAccountLedger = (ITable)objDebtAccountLedger;
                    DBO.Provider.Create(ref tblDebtAccountLedger);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        #endregion

        #region Local Methods
        public static ADDebtAccountLedger ConvertDataTableToClass(DataTable dt)
        {
            List<ADDebtAccountLedger> DebtAccountLedgerList = (from DataRow dr in dt.Rows
                                               select new ADDebtAccountLedger()
                                               {
                                                   DebtAccountLedgerId = ((dr["DebtAccountLedgerId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAccountLedgerId"].ToString()) : 0)),
                                                   DebtAccountId = ((dr["DebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAccountId"].ToString()) : 0)),
                                                   ClientFinDocId = ((dr["ClientFinDocId"] != DBNull.Value ? Convert.ToInt32(dr["ClientFinDocId"].ToString()) : 0)),
                                                   BatchId = ((dr["BatchId"] != DBNull.Value ? Convert.ToInt32(dr["BatchId"].ToString()) : 0)),
                                                   LedgerTypeId = ((dr["LedgerTypeId"] != DBNull.Value ? Convert.ToInt32(dr["LedgerTypeId"].ToString()) : 0)),
                                                   LedgerType = ((dr["LedgerType"] != DBNull.Value ? dr["LedgerType"].ToString() : "")),
                                                   CollectionStatus = ((dr["CollectionStatus"] != DBNull.Value ? dr["CollectionStatus"].ToString() : "")),
                                                   TrxCode = ((dr["TrxCode"] != DBNull.Value ? dr["TrxCode"].ToString() : "")),
                                                   TrxDate = ((dr["TrxDate"] != DBNull.Value ? Convert.ToDateTime(dr["TrxDate"]) : DateTime.MinValue)),
                                                   ReceiptDate = ((dr["ReceiptDate"] != DBNull.Value ? Convert.ToDateTime(dr["ReceiptDate"]) : DateTime.MinValue)),
                                                   RemitDate = ((dr["RemitDate"] != DBNull.Value ? Convert.ToDateTime(dr["RemitDate"]) : DateTime.MinValue)),
                                                   InvoicedDate = ((dr["InvoicedDate"] != DBNull.Value ? Convert.ToDateTime(dr["InvoicedDate"]) : DateTime.MinValue)),
                                                   TrxAmt = ((dr["TrxAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TrxAmt"].ToString()) : 0)),
                                                   AsgAmt = ((dr["AsgAmt"] != DBNull.Value ? Convert.ToDecimal(dr["AsgAmt"].ToString()) : 0)),
                                                   AsgRcvAmt = ((dr["AsgRcvAmt"] != DBNull.Value ? Convert.ToDecimal(dr["AsgRcvAmt"].ToString()) : 0)),
                                                   FeeRcvAmt = ((dr["FeeRcvAmt"] != DBNull.Value ? Convert.ToDecimal(dr["FeeRcvAmt"].ToString()) : 0)),
                                                   AdjAmt = ((dr["AdjAmt"] != DBNull.Value ? Convert.ToDecimal(dr["AdjAmt"].ToString()) : 0)),
                                                   IntRcvAmt = ((dr["IntRcvAmt"] != DBNull.Value ? Convert.ToDecimal(dr["IntRcvAmt"].ToString()) : 0)),
                                                   CostRcvAmt = ((dr["CostRcvAmt"] != DBNull.Value ? Convert.ToDecimal(dr["CostRcvAmt"].ToString()) : 0)),
                                                   AttyRcvAmt = ((dr["AttyRcvAmt"] != DBNull.Value ? Convert.ToDecimal(dr["AttyRcvAmt"].ToString()) : 0)),
                                                   Rate = ((dr["Rate"] != DBNull.Value ? Convert.ToDecimal(dr["Rate"].ToString()) : 0)),
                                                   PmtAmt = ((dr["PmtAmt"] != DBNull.Value ? Convert.ToDecimal(dr["PmtAmt"].ToString()) : 0)),
                                                   BillableAmt = ((dr["BillableAmt"] != DBNull.Value ? Convert.ToDecimal(dr["BillableAmt"].ToString()) : 0)),
                                                   IntSharedAmt = ((dr["IntSharedAmt"] != DBNull.Value ? Convert.ToDecimal(dr["IntSharedAmt"].ToString()) : 0)),
                                                   FeeAmt = ((dr["FeeAmt"] != DBNull.Value ? Convert.ToDecimal(dr["FeeAmt"].ToString()) : 0)),
                                                   BilledAmt = ((dr["BilledAmt"] != DBNull.Value ? Convert.ToDecimal(dr["BilledAmt"].ToString()) : 0)),
                                                   RemitAmt = ((dr["RemitAmt"] != DBNull.Value ? Convert.ToDecimal(dr["RemitAmt"].ToString()) : 0)),
                                                   OvrPmtAmt = ((dr["OvrPmtAmt"] != DBNull.Value ? Convert.ToDecimal(dr["OvrPmtAmt"].ToString()) : 0)),
                                                   AttyFeeAmt = ((dr["AttyFeeAmt"] != DBNull.Value ? Convert.ToDecimal(dr["AttyFeeAmt"].ToString()) : 0)),
                                                   RcvByAgencyAmt = ((dr["RcvByAgencyAmt"] != DBNull.Value ? Convert.ToDecimal(dr["RcvByAgencyAmt"].ToString()) : 0)),
                                                   RcvByClientAmt = ((dr["RcvByClientAmt"] != DBNull.Value ? Convert.ToDecimal(dr["RcvByClientAmt"].ToString()) : 0)),
                                                   FeeByAgencyAmt = ((dr["FeeByAgencyAmt"] != DBNull.Value ? Convert.ToDecimal(dr["FeeByAgencyAmt"].ToString()) : 0)),
                                                   FeeByClientAmt = ((dr["FeeByClientAmt"] != DBNull.Value ? Convert.ToDecimal(dr["FeeByClientAmt"].ToString()) : 0)),
                                                   DueFromClientAmt = ((dr["DueFromClientAmt"] != DBNull.Value ? Convert.ToDecimal(dr["DueFromClientAmt"].ToString()) : 0)),
                                                   RemitToClientAmt = ((dr["RemitToClientAmt"] != DBNull.Value ? Convert.ToDecimal(dr["RemitToClientAmt"].ToString()) : 0)),
                                                   PrincDueAmt = ((dr["PrincDueAmt"] != DBNull.Value ? Convert.ToDecimal(dr["PrincDueAmt"].ToString()) : 0)),
                                                   TotalDueAmt = ((dr["TotalDueAmt"] != DBNull.Value ? Convert.ToDecimal(dr["TotalDueAmt"].ToString()) : 0)),
                                                   IsRemitted = ((dr["IsRemitted"] != DBNull.Value ? Convert.ToBoolean(dr["IsRemitted"]) : false)),
                                                   IsRcvdbyClient = ((dr["IsRcvdbyClient"] != DBNull.Value ? Convert.ToBoolean(dr["IsRcvdbyClient"]) : false)),
                                                   IsOmitFromStmt = ((dr["IsOmitFromStmt"] != DBNull.Value ? Convert.ToBoolean(dr["IsOmitFromStmt"]) : false)),
                                                   IsOmitFromCashRpt = ((dr["IsOmitFromCashRpt"] != DBNull.Value ? Convert.ToBoolean(dr["IsOmitFromCashRpt"]) : false)),
                                                   IsInterestShared = ((dr["IsInterestShared"] != DBNull.Value ? Convert.ToBoolean(dr["IsInterestShared"]) : false)),
                                                   IsOnHold = ((dr["IsOnHold"] != DBNull.Value ? Convert.ToBoolean(dr["IsOnHold"]) : false)),
                                                   IsNSF = ((dr["IsNSF"] != DBNull.Value ? Convert.ToBoolean(dr["IsNSF"]) : false)),
                                                   Reference = ((dr["Reference"] != DBNull.Value ? dr["Reference"].ToString() : "")),
                                                   LedgerNote = ((dr["LedgerNote"] != DBNull.Value ? dr["LedgerNote"].ToString() : "")),
                                                   DeskNumId = ((dr["DeskNumId"] != DBNull.Value ? Convert.ToInt32(dr["DeskNumId"].ToString()) : 0)),
                                                   DeskNum = ((dr["DeskNum"] != DBNull.Value ? dr["DeskNum"].ToString() : "")),
                                                   StmtGrouping = ((dr["StmtGrouping"] != DBNull.Value ? dr["StmtGrouping"].ToString() : "")),
                                                   EnteredByUserId = ((dr["EnteredByUserId"] != DBNull.Value ? Convert.ToInt32(dr["EnteredByUserId"].ToString()) : 0)),
                                                   EnteredByUserCode = ((dr["EnteredByUserCode"] != DBNull.Value ? dr["EnteredByUserCode"].ToString() : "")),
                                                   AttyOwedAmt = ((dr["AttyOwedAmt"] != DBNull.Value ? Convert.ToDecimal(dr["AttyOwedAmt"].ToString()) : 0)),
                                                   IsConverted = ((dr["IsConverted"] != DBNull.Value ? Convert.ToBoolean(dr["IsConverted"]) : false)),
                                                   InvoiceBreak = ((dr["InvoiceBreak"] != DBNull.Value ? dr["InvoiceBreak"].ToString() : "")),
                                                   CollectFeeAmt = ((dr["CollectFeeAmt"] != DBNull.Value ? Convert.ToDecimal(dr["CollectFeeAmt"].ToString()) : 0)),
                                                   FeeByAgencyAmtCF = ((dr["FeeByAgencyAmtCF"] != DBNull.Value ? Convert.ToDecimal(dr["FeeByAgencyAmtCF"].ToString()) : 0)),
                                                   FeeByClientAmtCF = ((dr["FeeByClientAmtCF"] != DBNull.Value ? Convert.ToDecimal(dr["FeeByClientAmtCF"].ToString()) : 0)),
                                                   AltKeyId = ((dr["AltKeyId"] != DBNull.Value ? Convert.ToInt32(dr["AltKeyId"].ToString()) : 0)),
                                                   SubLedgerType = ((dr["SubLedgerType"] != DBNull.Value ? dr["SubLedgerType"].ToString() : "")),
                                                   IsAdvanceCC = ((dr["IsAdvanceCC"] != DBNull.Value ? Convert.ToBoolean(dr["IsAdvanceCC"]) : false)),
                                                   SubLedgerTypeId = ((dr["SubLedgerTypeId"] != DBNull.Value ? Convert.ToInt32(dr["SubLedgerTypeId"].ToString()) : 0)),
                                                   ExchangeRate = ((dr["ExchangeRate"] != DBNull.Value ? Convert.ToDecimal(dr["ExchangeRate"].ToString()) : 0)),
                                                   CostRcvAmtOnPIF = ((dr["CostRcvAmtOnPIF"] != DBNull.Value ? Convert.ToDecimal(dr["CostRcvAmtOnPIF"].ToString()) : 0)),
                                                   BankName = ((dr["BankName"] != DBNull.Value ? dr["BankName"].ToString() : "")),
                                                   BankAcctNum = ((dr["BankAcctNum"] != DBNull.Value ? dr["BankAcctNum"].ToString() : "")),
                                                   CheckMaker = ((dr["CheckMaker"] != DBNull.Value ? dr["CheckMaker"].ToString() : "")),
                                                   IntShareRate = ((dr["IntShareRate"] != DBNull.Value ? Convert.ToDecimal(dr["IntShareRate"].ToString()) : 0)),
                                                   RebateAmt = ((dr["RebateAmt"] != DBNull.Value ? Convert.ToDecimal(dr["RebateAmt"].ToString()) : 0)),
                                               }).ToList();
            return DebtAccountLedgerList.First();
        }
        #endregion
    }
}
