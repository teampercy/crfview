﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Collections.Management.Reports.ViewModels
{
    public class ReportsVM
    {
        public ReportsVM()
        {
            objADvwCollectionReportsList = new ADvwCollectionReportsList();

            string ReportCategory = "Management";
            ReportIdList = CommonBindList.GetReportsList(ReportCategory);
        }

        public string RbtPrintOption { get; set; }

        public string ddlReportId { get; set; }
        public IEnumerable<SelectListItem> ReportIdList { get; set; }

        public bool chkAllClients { get; set; }
        public string ClientNum { get; set; }

        public bool chkAllDesks{ get; set; }
        public string DeskNum { get; set; }

        public bool chkAllStatus { get; set; }
        public string StatusCode { get; set; }

        public bool chkByAssignDate { get; set; }
        public bool chkByTrxDate { get; set; }

        public DateTime AssignDateFrom { get; set; }
        public DateTime AssignDateThru { get; set; }

        public DateTime TrxDateFrom { get; set; }
        public DateTime TrxnDateThru { get; set; }

        public ADvwCollectionReportsList objADvwCollectionReportsList { get; set; }
    }
}
