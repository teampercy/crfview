﻿using CRF.BLL.CRFDB.VIEWS;
using CRF.BLL.Providers;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADvwCollectionReportsList
    {
        #region Properties
        public string ReportName { get; set; }
        public string Description { get; set; }
        public string PrintFrom { get; set; }
        public string ReportCategory { get; set; }
        public string ReportFileName { get; set; }
        public string ReportType { get; set; }
        public int ReportId { get; set; }
        public string CustomSPName { get; set; }
        public int ReportFileId { get; set; }
        public int ReportCategoryId { get; set; }


        #endregion

        #region CRUD

        public static ADvwCollectionReportsList ReadvwCollectionReports(int ReportId)
        {
            vwCollectionReportsList myentity = new vwCollectionReportsList();
            ITable myentityItable = myentity;
            DBO.Provider.Read(ReportId.ToString(), ref myentityItable);
            myentity = (vwCollectionReportsList)myentityItable;
            AutoMapper.Mapper.CreateMap<vwCollectionReportsList, ADvwCollectionReportsList>();
            return (AutoMapper.Mapper.Map<ADvwCollectionReportsList>(myentity));
        }

        #endregion
    }
}
