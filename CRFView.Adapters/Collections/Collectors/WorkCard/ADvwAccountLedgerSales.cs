﻿using CRF.BLL;
using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.VIEWS;
using CRF.BLL.Providers;
using CRFView.Adapters.Accounting.AccountingCollections.AuditList.AuditListVM;
using CRFView.Adapters.Accounting.AccountingCollections.SmanCommissions.ViewModels;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard
{
    public class ADvwAccountLedgerSales
    {
        #region Properties

        public string ClientCode { get; set; }
        public string ServiceDescr  { get; set; }
        public string ClientName  { get; set; }
        public string ClientContactName  { get; set; }
        public string ClientAddressLine1  { get; set; }
        public string ClientAddressLine2  { get; set; }
        public string ClientCity { get; set; }
        public string ClientState  { get; set; }
        public string ClientPostalCode  { get; set; }
        public bool IsTrustAccount  { get; set; }
        public bool IsRolloverList  { get; set; }
        public bool IsRollover  { get; set; }
        public string ServiceCode  { get; set; }
        public Int32 DebtAccountId  { get; set; }
        public Int32 ClientId  { get; set; }
        public Int32 ContractId  { get; set; }
        public Int32 DebtId  { get; set; }
        public string FileNumber { get; set; }
        public Int32 LocationId  { get; set; }
        public Int32 CollectionStatusId  { get; set; }
        public DateTime ReferalDate  { get; set; }
        public DateTime LastServiceDate  { get; set; }
        public DateTime LastPaymentDate { get; set; }
        public DateTime LastUpdateDate { get; set; }
        public DateTime LastTrustDate { get; set; }
        public DateTime InterestFromDate { get; set; }
        public DateTime InterestThruDate { get; set; }
        public DateTime CloseDate { get; set; }
        public DateTime FirstReportDate { get; set; }
        public DateTime LastReportDate { get; set; }
        public DateTime NextReportDate { get; set; }
        public string CreditReportStatus { get; set; }
        public DateTime ClientLastViewDate { get; set; }
        public Int32 ClientLastViewBy  { get; set; }
        public bool IsEstAudit  { get; set; }
        public bool IsSecondary  { get; set; }
        public bool IsSkip  { get; set; }
        public bool IsJudgmentAcct  { get; set; }
        public bool IsIntlAcct  { get; set; }
        public Int32 TempId  { get; set; }
        public Int32 ClientFinDocId  { get; set; }
        public string LedgerType  { get; set; } 
        public string CollectionStatus  { get; set; }
        public string TrxCode  { get; set; }
        public DateTime TrxDate { get; set; }
        public DateTime ReceiptDate { get; set; }
        public DateTime InvoicedDate { get; set; }
        public DateTime RemitDate { get; set; }
        public decimal TrxAmt  { get; set; }
        public decimal AsgAmt  { get; set; }
        public decimal AsgRcvAmt { get; set; }
        public decimal FeeRcvAmt  { get; set; }
        public decimal AdjAmt  { get; set; }
        public decimal IntRcvAmt  { get; set; }
        public decimal CostRcvAmt  { get; set; }
        public decimal AttyRcvAmt  { get; set; }
        public decimal Rate  { get; set; }
        public decimal IntSharedAmt  { get; set; }
        public decimal FeeAmt  { get; set; }
        public decimal BilledAmt  { get; set; }
        public decimal RemitAmt  { get; set; }
        public decimal OvrPmtAmt  { get; set; }
        public decimal AttyFeeAmt  { get; set; }
        public decimal RcvByAgencyAmt  { get; set; }
        public decimal RcvByClientAmt  { get; set; }
        public decimal FeeByAgencyAmt  { get; set; }
        public decimal FeeByClientAmt  { get; set; }
        public decimal DueFromClientAmt  { get; set; }
        public decimal RemitToClientAmt  { get; set; }
        public decimal PrincDueAmt  { get; set; }
        public decimal TotalDueAmt  { get; set; }
        public bool IsRemitted  { get; set; }
        public bool IsRcvdbyClient  { get; set; }
        public bool IsOmitFromStmt  { get; set; }
        public bool IsOmitFromCashRpt  { get; set; }
        public bool IsInterestShared  { get; set; }
        public bool IsOnHold  { get; set; }
        public bool IsNSF  { get; set; }
        public string Reference  { get; set; }
        public string LedgerNote  { get; set; }
        public Int32 DeskNumId  { get; set; }
        public string StmtGrouping  { get; set; }
        public string Location  { get; set; }
        public decimal PmtAmt  { get; set; }
        public decimal BillableAmt  { get; set; }
        public Int32 EnteredByUserId  { get; set; }
        public string EnteredByUserCode  { get; set; }
        public decimal RcvAmt  { get; set; }
        public Int32 DebtAccountLedgerId  { get; set; }
        public bool IsSeparateStmt  { get; set; }
        public bool IsIndPaidAgencyStmt  { get; set; }
        public bool IsIndPaidClientStmt  { get; set; }
        public bool IsIndStmt  { get; set; }
        public bool IsGrossRemit  { get; set; }
        public Int32 FinDocCycleId  { get; set; }
        public Int32 FinDocTypeId  { get; set; }
        public string InvoiceBreak  { get; set; }
        public decimal AttyOwedAmt  { get; set; }
        public bool IsShowAttyFeesOnStmt  { get; set; }
        public bool AdvanceCC  { get; set; }
        public string CollectorName  { get; set; }
        public string SalesNum  { get; set; }
        public string SalesName  { get; set; }
        public DateTime SetupDate { get; set; }
        public DateTime MinPayDate { get; set; }
        public DateTime MaxPayDate { get; set; }
        public DateTime OwnershipDate { get; set; }
        public Int32 SalesCommRate  { get; set; }
        public Int32 SalesTeamId  { get; set; }
        public DateTime LastReferralDate { get; set; }
        public decimal ExchRatePay  { get; set; }
        public decimal ExchRateAssign  { get; set; }
        public Int32 RcvByAgencyAmtEx  { get; set; }
        public Int32 RcvByClientAmtEx  { get; set; }
        public Int32 FeeByAgencyAmtEx  { get; set; }
        public Int32 FeeByClientAmtEx  { get; set; }
        public Int32 IntSharedAmtEx  { get; set; }
        public Int32 AttyRcvAMtEx  { get; set; }
        public Int32 AttyOwedAmtEx  { get; set; }
        public Int32 CostRcvAmtEx  { get; set; }
        public string BankName  { get; set; }
        public string BankAcctNum  { get; set; }
        public string CheckMaker  { get; set; }
        public string DebtorName  { get; set; }
        public decimal RebateAmt  { get; set; }
        public decimal txtPMTA_INT  { get; set; }
        public decimal txtPMTC_INT  { get; set; }
        public Int32 txtPMTA_INTEx  { get; set; }
        public Int32 txtPMTC_INTEx  { get; set; }
        public decimal txtPMTA_INTR  { get; set; }
        public decimal txtPMTC_INTR  { get; set; }
        public Int32 txtPMTA_INTREx  { get; set; }
        public Int32 txtPMTC_INTREx  { get; set; }
        public decimal txtPMTA_CC  { get; set; }
        public decimal txtPMTC_CC  { get; set; }
        public Int32 txtPMTA_CCEx  { get; set; }
        public Int32 txtPMTC_CCEx  { get; set; }
        public decimal txtPMTA_AF  { get; set; }
        public decimal txtPMTC_AF  { get; set; }
        public decimal txtPMTA_REBATE  { get; set; }
        public decimal txtPMTC_REBATE  { get; set; }
        public decimal txtPMTA_AttyOwed  { get; set; }

        #endregion

        #region CRUD

        //Read vwAccountLedgerSales From DebtAccountId
        public static ADvwAccountLedgerSales ReadvwAccountLedgerSales(string DebtAccountId)
        {
            try
            {
                ADvwAccountLedgerSales Obj_vwAccountLedgerSales = new ADvwAccountLedgerSales();
                string MYSQL = "Select Top 1 * from vwAccountLedgerSales where DebtAccountId = " + DebtAccountId;
                System.Data.DataSet ds = DBO.Provider.GetDataSet(MYSQL);
                DataTable dt = ds.Tables[0];

                if (dt.Rows.Count >= 1)
                {
                    Obj_vwAccountLedgerSales.SalesName = dt.Rows[0]["SalesName"].ToString();
                }
                else
                {

                }
                return Obj_vwAccountLedgerSales;
            }
            catch (Exception ex)
            {
                return new ADvwAccountLedgerSales();
            }
        }


        // print Sman Commission Report

        public static string PrintSmanCommissionsReport(AccountingSmanCommissionsVM objAccountingSmanCommissionsVM)
        {
            string pdfPath = null;
            string sFromDate = null;
            string sThruDate = null;

            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var Sp_OBJ = new cview_Reports_SelectByTrxDate();

                //string sql = "Select * from vwCollectionReportsList Where ReportId = " + Convert.ToInt32(objCollectionReportsVM.ddlReportId);
        //        TableView mytableview = DBO.Provider.GetTableView(Sp_OBJ);              

                if (objAccountingSmanCommissionsVM.chkAllSman == false)
                {
                    Sp_OBJ.SmanNum = objAccountingSmanCommissionsVM.TeamId;
                }

                Sp_OBJ.FromDate = objAccountingSmanCommissionsVM.DateTimePicker1;
                Sp_OBJ.ThruDate = objAccountingSmanCommissionsVM.DateTimePicker2;
                sFromDate = objAccountingSmanCommissionsVM.DateTimePicker1;
                sThruDate = objAccountingSmanCommissionsVM.DateTimePicker2;

                string myreportname = "";
                if (objAccountingSmanCommissionsVM.Rbtbtn1 == "optSmanDetailReport")
                {
                    myreportname = "SalesCommissionCollection";
                    Sp_OBJ.ReportName = "SalesCommissionCollection";
                }
                else
                {
                    myreportname = "SalesCommissionCollectionClientSum";
                    Sp_OBJ.ReportName = "SalesCommissionCollectionClientSum";
                }

                //TableView mytableview = DBO.Provider.GetTableView(Sp_OBJ);
                TableView myview = CRF.BLL.Providers.DBO.Provider.GetTableView(Sp_OBJ);

                CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;
                //mymode = mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                if (objAccountingSmanCommissionsVM.RbtPrintOption == "optToPDF")
                {
                  mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                }
              
                //If ProcessDialog.optToPDF.Checked = True Then
                //    mymode = BLL.Common.PrintMode.PrintToPDF
                //End If

                string myreportfilename = Globals.CurrentUser.SettingsFolder + "CollectviewReports.xml";
                {
                    if (objAccountingSmanCommissionsVM.RbtPrintOption == "optToCSVFile")
                    {
                        pdfPath = ADvwAccountLedgerSales.ExportToFile(myview, myreportname);
                    }
                    else if (objAccountingSmanCommissionsVM.RbtPrintOption == "optToCSVAllColumns")
                    {
                        pdfPath = Convert.ToString(ADvwAccountLedgerSales.ExportToFileAll(myview, myreportname));
                    }
                    else
                    {
                        pdfPath = Globals.RenderC1Report(user, myview.Table, myreportfilename, myreportname, mymode, "", sFromDate, sThruDate);
                    }
                }           

               
            }

            catch (Exception ex)
            {
                return "";
            }

            

            return pdfPath;

        }



        private static string ExportToFile(DataView aview, string areportname)
        {
            CRF.BLL.Export.ExportColumns mycols = new CRF.BLL.Export.ExportColumns();
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
            string sfilepath = CRF.BLL.CRFView.CRFView.GetFileName(ref user, "Management", areportname, ".CSV");
            string fileName = CRF.BLL.Export.Export.ExportToCSV(ref aview, ref sfilepath, ref mycols, true);
            if (!string.IsNullOrEmpty(fileName))
            {
                return fileName;
            }
            else
            {
                return "";
            }
        }

        public static bool ExportToFileAll(TableView MyView, string areportname)
        {
            CRF.BLL.Export.ExportColumns mycols = new CRF.BLL.Export.ExportColumns();
            CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

            string sfilepath = CRF.BLL.CRFView.CRFView.GetFileName(ref user, "", areportname, ".CSV", true);

            MyView.ExportColumns.Add(vwAccountLedgerSales.ColumnNames.ClientCode);
            MyView.ExportColumns.Add(vwAccountLedgerSales.ColumnNames.ClientName);
            MyView.ExportColumns.Add(vwAccountLedgerSales.ColumnNames.DebtAccountId);
            MyView.ExportColumns.Add(vwAccountLedgerSales.ColumnNames.ReferalDate);
            MyView.ExportColumns.Add(vwAccountLedgerSales.ColumnNames.TrxDate);
            MyView.ExportColumns.Add(vwAccountLedgerSales.ColumnNames.RcvAmt);
            MyView.ExportColumns.Add(vwAccountLedgerSales.ColumnNames.SalesNum);
            MyView.ExportColumns.Add(vwAccountLedgerSales.ColumnNames.SalesName);
            MyView.ExportColumns.Add(vwAccountLedgerSales.ColumnNames.SetupDate);
            MyView.ExportColumns.Add(vwAccountLedgerSales.ColumnNames.MinPayDate);
            MyView.ExportColumns.Add(vwAccountLedgerSales.ColumnNames.MaxPayDate);
            MyView.ExportColumns.Add(vwAccountLedgerSales.ColumnNames.OwnershipDate);
            MyView.ExportColumns.Add(vwAccountLedgerSales.ColumnNames.SalesCommRate);
            MyView.ExportColumns.Add(vwAccountLedgerSales.ColumnNames.RcvByAgencyAmtEx);
            MyView.ExportColumns.Add(vwAccountLedgerSales.ColumnNames.RcvByClientAmtEx);
            MyView.ExportColumns.Add(vwAccountLedgerSales.ColumnNames.IntSharedAmtEx);
            MyView.ExportColumns.Add(vwAccountLedgerSales.ColumnNames.AttyRcvAMtEx);
            MyView.ExportColumns.Add(vwAccountLedgerSales.ColumnNames.AttyOwedAmtEx);
            MyView.ExportColumns.Add(vwAccountLedgerSales.ColumnNames.DebtorName);
            MyView.ExportColumns.Add(vwAccountLedgerSales.ColumnNames.CommissionDate);

            MyView.ExportToCSV(sfilepath);

            return true;

        }

        public static string PrintAuditListReport(AuditOptionsVM objAuditOptionsVM)
        {
            string pdfPath = null;
            string sFromDate = null;
            string sThruDate = null;

            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var Sp_OBJ = new cview_Accounting_GetAuditList();

                //string sql = "Select * from vwCollectionReportsList Where ReportId = " + Convert.ToInt32(objCollectionReportsVM.ddlReportId);
                //        TableView mytableview = DBO.Provider.GetTableView(Sp_OBJ);              

                if(objAuditOptionsVM.ChkRemitDate == true)
                {
                    Sp_OBJ.RemitFROM = objAuditOptionsVM.RemitFromDate.ToString();
                    Sp_OBJ.RemitTO = objAuditOptionsVM.RemitThruDate.ToString();
                    sFromDate = objAuditOptionsVM.RemitFromDate.ToString();
                    sThruDate = objAuditOptionsVM.RemitThruDate.ToString();
                }

                if (objAuditOptionsVM.ChkTrxDate == true)
                {
                    Sp_OBJ.TrxFROM = objAuditOptionsVM.TrxFromDate.ToString();
                    Sp_OBJ.TrxTO = objAuditOptionsVM.TrxThruDate.ToString();
                    //sFromDate = objAuditOptionsVM.TrxFromDate.ToString();
                    //sThruDate = objAuditOptionsVM.TrxThruDate.ToString();
                }
                if (objAuditOptionsVM.ChkInvoiceCycle == true)
                {
                    Sp_OBJ.InvoiceCycle = objAuditOptionsVM.InvoiceCycle;
                }
                if (objAuditOptionsVM.ChkDebtorState == true)
                {
                    Sp_OBJ.DebtorState = objAuditOptionsVM.DebtorState;
                }

                if(objAuditOptionsVM.ReportType == "BillingReport")
                {
                    Sp_OBJ.AuditList = 2;
                }
                else if(objAuditOptionsVM.ReportType == "CombineDeductInternal")
                    {
                    Sp_OBJ.AuditList = 3;
                }
                else if (objAuditOptionsVM.ReportType == "GrossRemit")
                {
                    Sp_OBJ.AuditList = 4;
                }
                else if (objAuditOptionsVM.ReportType == "NSF")
                {
                    Sp_OBJ.AuditList = 5;
                }
                else if (objAuditOptionsVM.ReportType == "Remit Report")
                {
                    Sp_OBJ.AuditList = 6;
                }
                else if (objAuditOptionsVM.ReportType == "TrustJournal")
                {
                    Sp_OBJ.AuditList = 7;
                }
                else if (objAuditOptionsVM.ReportType == "TrustLiability")
                {
                    Sp_OBJ.AuditList = 8;
                }
                else if (objAuditOptionsVM.ReportType == "TrustLiabilityGross")
                {
                    Sp_OBJ.AuditList = 9;
                }

                else
                {
                    Sp_OBJ.AuditList = 1;
                }

                if(objAuditOptionsVM.AllOrIndvdClient == "ByClients")
                {
                   Sp_OBJ.ClientId = objAuditOptionsVM.ClientId.ToString();
                }

                TableView myview = CRF.BLL.Providers.DBO.Provider.GetTableView(Sp_OBJ);

                CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;
                if(objAuditOptionsVM.PrintOptions == "SendToPDF")
                {
                    mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                }
              
                string s = "";

                if (objAuditOptionsVM.ReportType == "TrustLiability")
                {
                    //s = Globals.RenderC1Report(DBO.GetDataSet(mysproc).Tables(0), "CollectViewReports.xml", "TrustLiability", mymode, "", sFromDate, sThruDate)
                    pdfPath = Globals.RenderC1Report(user, myview.Table, "CollectViewReports.xml", "TrustLiability", mymode, "", sFromDate, sThruDate);
                }
                else if (objAuditOptionsVM.ReportType == "TrustLiabilityGross")
                {
                    pdfPath = Globals.RenderC1Report(user, myview.Table, "CollectViewReports.xml", "TrustLiabilityGross", mymode, "", sFromDate, sThruDate);
                }

                else if (objAuditOptionsVM.ReportType == "Remit Report")
                {
                    pdfPath = Globals.RenderC1Report(user, myview.Table, "CollectViewReports.xml", "RemitReport", mymode, "", sFromDate, sThruDate);
                }
                else if (objAuditOptionsVM.ReportType == "GrossRemit")
                {
                    pdfPath = Globals.RenderC1Report(user, myview.Table, "CollectViewReports.xml", "GrossRemit", mymode, "", sFromDate, sThruDate);
                }
                else if (objAuditOptionsVM.ReportType == "CombineDeductInternal")
                {
                    pdfPath = Globals.RenderC1Report(user, myview.Table, "CollectViewReports.xml", "CombineDeductInternal", mymode, "", sFromDate, sThruDate);
                }
                else
                {
                    pdfPath = Globals.RenderC1Report(user, myview.Table, "CollectViewReports.xml", "TrustJournal", mymode, "", sFromDate, sThruDate);
                }
                
                //Sp_OBJ.FromDate = objAccountingSmanCommissionsVM.DateTimePicker1;
                //Sp_OBJ.ThruDate = objAccountingSmanCommissionsVM.DateTimePicker2;
                //sFromDate = objAccountingSmanCommissionsVM.DateTimePicker1;
                //sThruDate = objAccountingSmanCommissionsVM.DateTimePicker2;

                //string myreportname = "";
                //if (objAccountingSmanCommissionsVM.Rbtbtn1 == "optSmanDetailReport")
                //{
                //    myreportname = "SalesCommissionCollection";
                //    Sp_OBJ.ReportName = "SalesCommissionCollection";
                //}
                //else
                //{
                //    myreportname = "SalesCommissionCollectionClientSum";
                //    Sp_OBJ.ReportName = "SalesCommissionCollectionClientSum";
                //}

                ////TableView mytableview = DBO.Provider.GetTableView(Sp_OBJ);
                //TableView myview = CRF.BLL.Providers.DBO.Provider.GetTableView(Sp_OBJ);

                //CRF.BLL.COMMON.PrintMode mymode = CRF.BLL.COMMON.PrintMode.PrintDirect;
                ////mymode = mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                //if (objAccountingSmanCommissionsVM.RbtPrintOption == "optToPDF")
                //{
                //    mymode = CRF.BLL.COMMON.PrintMode.PrintToPDF;
                //}

                ////If ProcessDialog.optToPDF.Checked = True Then
                ////    mymode = BLL.Common.PrintMode.PrintToPDF
                ////End If

                //string myreportfilename = Globals.CurrentUser.SettingsFolder + "CollectviewReports.xml";
                //{
                //    if (objAccountingSmanCommissionsVM.RbtPrintOption == "optToCSVFile")
                //    {
                //        pdfPath = ADvwAccountLedgerSales.ExportToFile(myview, myreportname);
                //    }
                //    else if (objAccountingSmanCommissionsVM.RbtPrintOption == "optToCSVAllColumns")
                //    {
                //        pdfPath = Convert.ToString(ADvwAccountLedgerSales.ExportToFileAll(myview, myreportname));
                //    }
                //    else
                //    {
                //        pdfPath = Globals.RenderC1Report(user, myview.Table, myreportfilename, myreportname, mymode, "", sFromDate, sThruDate);
                //    }
                //}


            }

            catch (Exception ex)
            {
                return "";
            }



            return pdfPath;

        }


        #endregion

    }
}
