﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using CRFView.Adapters.Collections.Collectors.WorkCard;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADDebtAccountAttachment
    {
        #region Properties
        public int Id { get; set; }
        public int DebtAccountId { get; set; }
        public int UserId { get; set; }
        public bool Processed { get; set; }
        public System.DateTime DateCreated { get; set; }
        public string FileName { get; set; }
        public string DocumentType { get; set; }
        public string DocumentDescription { get; set; }
        public byte[] DocumentImage { get; set; }
        public int BatchDebtAccountId { get; set; }
        public string UserCode { get; set; }
        public bool IsClientView { get; set; }
        #endregion

        #region CRUD
        public static List<ADDebtAccountAttachment> GetDebtAttachments(DataTable dt)
        {
            List<ADDebtAccountAttachment> Documents = (from DataRow dr in dt.Rows
                                             select new ADDebtAccountAttachment()
                                             {
                                                 Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                                 //DebtAccountId = ((dr["DebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAccountId"]) : 0)),
                                                 //UserId = ((dr["UserId"] != DBNull.Value ? Convert.ToInt32(dr["UserId"]) : 0)),
                                                 //Processed = ((dr["Processed"] != DBNull.Value ? Convert.ToBoolean(dr["Processed"]) : false)),
                                                 DateCreated = ((dr["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dr["DateCreated"]) : DateTime.MinValue)),
                                                 //FileName = ((dr["FileName"] != DBNull.Value ? dr["FileName"].ToString() : "")),
                                                 DocumentType = ((dr["DocumentType"] != DBNull.Value ? dr["DocumentType"].ToString() : "")),
                                                 DocumentDescription = ((dr["DocumentDescription"] != DBNull.Value ? dr["DocumentDescription"].ToString() : "")),
                                                 //BatchDebtAccountId = ((dr["BatchDebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["BatchDebtAccountId"]) : 0)),
                                                 UserCode = ((dr["UserCode"] != DBNull.Value ? dr["UserCode"].ToString() : "")),
                                                 //IsClientView = ((dr["IsClientView"] != DBNull.Value ? Convert.ToBoolean(dr["IsClientView"]) : false)),
                                             }).ToList();
            if(Documents.Count==0)
            {
                return (new List<ADDebtAccountAttachment>());
            }
            return Documents;
        }

        public static ADDebtAccountAttachment ReadDebtAccountAttachment(int Id)
        {
            DebtAccountAttachments myentity = new DebtAccountAttachments();
            ITable myentityItable = myentity;
            DBO.Provider.Read(Id.ToString(), ref myentityItable);
            myentity = (DebtAccountAttachments)myentityItable;
            AutoMapper.Mapper.CreateMap<DebtAccountAttachments, ADDebtAccountAttachment>();
            return (AutoMapper.Mapper.Map<ADDebtAccountAttachment>(myentity));
        }

        public static int SaveDebtAccountAttachment(ADDebtAccountAttachment objUpdatedADDebtAccountAttachment)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADDebtAccountAttachment, DebtAccountAttachments>();
                ITable tblADDebtAccountAttachment;
                DebtAccountAttachments objDebtAccountAttachments;

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                if (objUpdatedADDebtAccountAttachment.Id != 0)
                {
                    ADDebtAccountAttachment objADDebtAccountAttachmentOriginal = ReadDebtAccountAttachment(Convert.ToInt32(objUpdatedADDebtAccountAttachment.Id));

                    //if (objUpdatedADDebtAccountAttachment.DateCreated.ToString() == "" || objUpdatedADDebtAccountAttachment.DateCreated == null)
                    //{
                    //}
                    //else
                    //{
                    //    objADDebtAccountAttachmentOriginal.DateCreated = objUpdatedADDebtAccountAttachment.DateCreated;
                    //}

                    if (objUpdatedADDebtAccountAttachment.DocumentType == "" || objUpdatedADDebtAccountAttachment.DocumentType == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountAttachmentOriginal.DocumentType = objUpdatedADDebtAccountAttachment.DocumentType.Trim();
                    }

                    if (objUpdatedADDebtAccountAttachment.FileName == "" || objUpdatedADDebtAccountAttachment.FileName == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountAttachmentOriginal.FileName = objUpdatedADDebtAccountAttachment.FileName.Trim();
                    }

                    if (objUpdatedADDebtAccountAttachment.DocumentDescription == "" || objUpdatedADDebtAccountAttachment.DocumentDescription == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountAttachmentOriginal.DocumentDescription = objUpdatedADDebtAccountAttachment.DocumentDescription.Trim();
                    }

                    if (objUpdatedADDebtAccountAttachment.DocumentImage == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountAttachmentOriginal.DocumentImage = objUpdatedADDebtAccountAttachment.DocumentImage;
                    }

                 
                        objADDebtAccountAttachmentOriginal.DocumentDescription = "[Filename: " + objADDebtAccountAttachmentOriginal.FileName + "]  ";
                  

                    objADDebtAccountAttachmentOriginal.UserId = user.Id;
                    objADDebtAccountAttachmentOriginal.UserCode = user.UserName;

                    objADDebtAccountAttachmentOriginal.DateCreated = DateTime.Now; // show current Datetime

                    objDebtAccountAttachments = AutoMapper.Mapper.Map<DebtAccountAttachments>(objADDebtAccountAttachmentOriginal);
                    tblADDebtAccountAttachment = (ITable)objDebtAccountAttachments;
                    DBO.Provider.Update(ref tblADDebtAccountAttachment);
                }
                else
                {

                    objUpdatedADDebtAccountAttachment.DateCreated = DateTime.Now; // show current time

                    objUpdatedADDebtAccountAttachment.UserId = user.Id;
                    objUpdatedADDebtAccountAttachment.UserCode = user.UserName;
                    objUpdatedADDebtAccountAttachment.DocumentDescription = "[Filename: " + objUpdatedADDebtAccountAttachment.FileName + "]  ";

                    objDebtAccountAttachments = AutoMapper.Mapper.Map<DebtAccountAttachments>(objUpdatedADDebtAccountAttachment);
                    tblADDebtAccountAttachment = (ITable)objDebtAccountAttachments;
                    DBO.Provider.Create(ref tblADDebtAccountAttachment);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static bool DeleteDocument(string Id)
        {
            try
            {
                ITable IDebtAccountAttachments;
                DebtAccountAttachments objIDebtAccountAttachments = new DebtAccountAttachments();
                objIDebtAccountAttachments.Id = Convert.ToInt32(Id);
                IDebtAccountAttachments = objIDebtAccountAttachments;
                DBO.Provider.Delete(ref IDebtAccountAttachments);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static void SaveFileToBeViewed(byte[] ByteArray, string filePath)
        {
            File.WriteAllBytes(filePath, ByteArray);
        }


        public static List<ADDebtAccountAttachment> GetBatchDebtAccountAttachments(string BatchDebtAccountId)
        {
            string mysql = "Select Id, DateCreated,UserCode,DocumentType,DocumentDescription from DebtAccountAttachments ";
            mysql += " Where BatchDebtAccountId > 0 and BatchDebtAccountId = " + BatchDebtAccountId;
            var BatchDebtAccountAttachmentsView = DBO.Provider.GetTableView(mysql);
            DataTable dt = BatchDebtAccountAttachmentsView.ToTable();

            List<ADDebtAccountAttachment> Documents = (from DataRow dr in dt.Rows
                                                       select new ADDebtAccountAttachment()
                                                       {
                                                           Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                                           //DebtAccountId = ((dr["DebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAccountId"]) : 0)),
                                                           //UserId = ((dr["UserId"] != DBNull.Value ? Convert.ToInt32(dr["UserId"]) : 0)),
                                                           //Processed = ((dr["Processed"] != DBNull.Value ? Convert.ToBoolean(dr["Processed"]) : false)),
                                                           DateCreated = ((dr["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dr["DateCreated"]) : DateTime.MinValue)),
                                                           //FileName = ((dr["FileName"] != DBNull.Value ? dr["FileName"].ToString() : "")),
                                                           DocumentType = ((dr["DocumentType"] != DBNull.Value ? dr["DocumentType"].ToString() : "")),
                                                           DocumentDescription = ((dr["DocumentDescription"] != DBNull.Value ? dr["DocumentDescription"].ToString() : "")),
                                                           //BatchDebtAccountId = ((dr["BatchDebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["BatchDebtAccountId"]) : 0)),
                                                           UserCode = ((dr["UserCode"] != DBNull.Value ? dr["UserCode"].ToString() : "")),
                                                           //IsClientView = ((dr["IsClientView"] != DBNull.Value ? Convert.ToBoolean(dr["IsClientView"]) : false)),
                                                       }).ToList();
            if (Documents.Count == 0)
            {
                return (new List<ADDebtAccountAttachment>());
            }
            return Documents;
        }
        public static List<ADDebtAccountAttachment> GetDebtAccountAttachments(string DebtAccountId)
        {
            string mysql = "Select Id, FileName,DateCreated,UserCode,DocumentType,DocumentDescription from DebtAccountAttachments where DebtAccountId=" + DebtAccountId;
          
            var BatchDebtAccountAttachmentsView = DBO.Provider.GetTableView(mysql);
            DataTable dt = BatchDebtAccountAttachmentsView.ToTable();

            List<ADDebtAccountAttachment> Documents = (from DataRow dr in dt.Rows
                                                       select new ADDebtAccountAttachment()
                                                       {
                                                           Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                                           //DebtAccountId = ((dr["DebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAccountId"]) : 0)),
                                                           //UserId = ((dr["UserId"] != DBNull.Value ? Convert.ToInt32(dr["UserId"]) : 0)),
                                                           //Processed = ((dr["Processed"] != DBNull.Value ? Convert.ToBoolean(dr["Processed"]) : false)),
                                                           DateCreated = ((dr["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dr["DateCreated"]) : DateTime.MinValue)),
                                                           FileName = ((dr["FileName"] != DBNull.Value ? dr["FileName"].ToString() : "")),
                                                           DocumentType = ((dr["DocumentType"] != DBNull.Value ? dr["DocumentType"].ToString() : "")),
                                                           DocumentDescription = ((dr["DocumentDescription"] != DBNull.Value ? dr["DocumentDescription"].ToString() : "")),
                                                           //BatchDebtAccountId = ((dr["BatchDebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["BatchDebtAccountId"]) : 0)),
                                                           UserCode = ((dr["UserCode"] != DBNull.Value ? dr["UserCode"].ToString() : "")),
                                                           //IsClientView = ((dr["IsClientView"] != DBNull.Value ? Convert.ToBoolean(dr["IsClientView"]) : false)),
                                                       }).ToList();
            if (Documents.Count == 0)
            {
                return (new List<ADDebtAccountAttachment>());
            }
            return Documents;
        }

        #endregion
    }
}
