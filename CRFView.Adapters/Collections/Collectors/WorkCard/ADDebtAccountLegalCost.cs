﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard
{
    public class ADDebtAccountLegalCost
    {

        #region Properties
        public int Id { get; set; }
        public int DebtAccountId { get; set; }
        public DateTime DateCreated { get; set; }
        public bool IsSmallClaims { get; set; }
        public string LedgerType { get; set; }
        public decimal CourtCostAmt { get; set; }
        public decimal ServiceFeeAmt { get; set; }
        public decimal WritFeeAmt { get; set; }
        public decimal LevyFeeAmt { get; set; }
        public decimal RecordingFeeAmt { get; set; }
        public decimal AbstractFeeAmt { get; set; }
        public decimal SuitFeeAmt { get; set; }
        public decimal CostExpAmt { get; set; }
        public decimal AttyExpAmt { get; set; }
        public string Reference { get; set; }
        public string Note { get; set; }
        public int EnteredByUserId { get; set; }
        public string EnteredByUser { get; set; }
        public bool IsAdvanceCC { get; set; }
        public decimal FilingFee { get; set; }
        #endregion
 
        #region CRUD
        public static List<ADDebtAccountLegalCost> GetDebtAccountLegalCost(DataTable dt)
        {
            List<ADDebtAccountLegalCost> DebtAccountLegalCosts = (from DataRow dr in dt.Rows
                                               select new ADDebtAccountLegalCost()
                                               {
                                                   Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                                   DebtAccountId = ((dr["DebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAccountId"]) : 0)),
                                                   DateCreated = ((dr["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dr["DateCreated"]) : DateTime.MinValue)),
                                                   IsSmallClaims = ((dr["IsSmallClaims"] != DBNull.Value ? Convert.ToBoolean(dr["IsSmallClaims"]) : false)),
                                                   LedgerType = ((dr["LedgerType"] != DBNull.Value ? dr["LedgerType"].ToString() : "")),
                                                   CourtCostAmt = ((dr["CourtCostAmt"] != DBNull.Value ? Convert.ToDecimal(dr["CourtCostAmt"].ToString()) : 0)),
                                                   ServiceFeeAmt = ((dr["ServiceFeeAmt"] != DBNull.Value ? Convert.ToDecimal(dr["ServiceFeeAmt"].ToString()) : 0)),
                                                   WritFeeAmt = ((dr["WritFeeAmt"] != DBNull.Value ? Convert.ToDecimal(dr["WritFeeAmt"].ToString()) : 0)),
                                                   LevyFeeAmt = ((dr["LevyFeeAmt"] != DBNull.Value ? Convert.ToDecimal(dr["LevyFeeAmt"].ToString()) : 0)),
                                                   RecordingFeeAmt = ((dr["RecordingFeeAmt"] != DBNull.Value ? Convert.ToDecimal(dr["RecordingFeeAmt"].ToString()) : 0)),
                                                   AbstractFeeAmt = ((dr["AbstractFeeAmt"] != DBNull.Value ? Convert.ToDecimal(dr["AbstractFeeAmt"].ToString()) : 0)),
                                                   SuitFeeAmt = ((dr["SuitFeeAmt"] != DBNull.Value ? Convert.ToDecimal(dr["SuitFeeAmt"].ToString()) : 0)),
                                                   CostExpAmt = ((dr["CostExpAmt"] != DBNull.Value ? Convert.ToDecimal(dr["CostExpAmt"].ToString()) : 0)),
                                                   AttyExpAmt = ((dr["AttyExpAmt"] != DBNull.Value ? Convert.ToDecimal(dr["AttyExpAmt"].ToString()) : 0)),
                                                   Reference = ((dr["Reference"] != DBNull.Value ? dr["Reference"].ToString() : "")),
                                                   Note = ((dr["Note"] != DBNull.Value ? dr["Note"].ToString() : "")),
                                                   EnteredByUserId = ((dr["EnteredByUserId"] != DBNull.Value ? Convert.ToInt32(dr["EnteredByUserId"]) : 0)),
                                                   EnteredByUser = ((dr["EnteredByUser"] != DBNull.Value ? dr["EnteredByUser"].ToString() : "")),
                                                   IsAdvanceCC = ((dr["IsAdvanceCC"] != DBNull.Value ? Convert.ToBoolean(dr["IsAdvanceCC"]) : false)),
                                                   FilingFee = ((dr["FilingFee"] != DBNull.Value ? Convert.ToDecimal(dr["FilingFee"].ToString()) : 0)),

                                               }).ToList();
            return DebtAccountLegalCosts;
        }

        public static DataTable GetDebtAccountLegalCostATTCDtList(string AccountId)
        {
            try
            {
                string mysql = "select * from debtaccountlegalcost where ledgertype in ('ATTC' ) AND debtaccountid = " + AccountId;

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(mysql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        public static DataTable GetDebtAccountLegalCostATTADtList(string AccountId)
        {
            try
            {
                string mysql = "select * from debtaccountlegalcost where ledgertype in ('ATTA' ) AND debtaccountid = " + AccountId;

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(mysql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        //Read Details from database
        public static ADDebtAccountLegalCost ReadDebtAccountLegalCost(int id)
        {
            DebtAccountLegalCost myentity = new DebtAccountLegalCost();
            ITable myentityItable = myentity;
            DBO.Provider.Read(id.ToString(), ref myentityItable);
            myentity = (DebtAccountLegalCost)myentityItable;
            AutoMapper.Mapper.CreateMap<DebtAccountLegalCost, ADDebtAccountLegalCost>();
            return (AutoMapper.Mapper.Map<ADDebtAccountLegalCost>(myentity));
        }

        public static bool DeleteDebtAccountLegalCost(string Id)
        {
            try
            {
                ITable IDebtAccountLegalCost;
                DebtAccountLegalCost objDebtAccountLegalCost = new DebtAccountLegalCost();
                objDebtAccountLegalCost.Id = Convert.ToInt32(Id);
                IDebtAccountLegalCost = objDebtAccountLegalCost;
                DBO.Provider.Delete(ref IDebtAccountLegalCost);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static int SaveDebtAccountLegalCost(ADDebtAccountLegalCost objUpdatedADDebtAccountLegalCost)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADDebtAccountLegalCost, DebtAccountLegalCost>();
                ITable tblDebtAccountLegalCost;
                DebtAccountLegalCost objDebtAccountLegalCost;

                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                if (objUpdatedADDebtAccountLegalCost.Id != 0)
                {
                    ADDebtAccountLegalCost objADDebtAccountLegalCostOriginal = ReadDebtAccountLegalCost(Convert.ToInt32(objUpdatedADDebtAccountLegalCost.Id));
                    
                    if (objUpdatedADDebtAccountLegalCost.DateCreated == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalCostOriginal.DateCreated = objUpdatedADDebtAccountLegalCost.DateCreated;
                    }

                    if (objUpdatedADDebtAccountLegalCost.DebtAccountId == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalCostOriginal.DebtAccountId = objUpdatedADDebtAccountLegalCost.DebtAccountId;
                    }

                    if (objUpdatedADDebtAccountLegalCost.LedgerType == "" || objUpdatedADDebtAccountLegalCost.LedgerType == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalCostOriginal.LedgerType = objUpdatedADDebtAccountLegalCost.LedgerType.Trim();
                    }

                    if (objUpdatedADDebtAccountLegalCost.CourtCostAmt == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalCostOriginal.CourtCostAmt = objUpdatedADDebtAccountLegalCost.CourtCostAmt;
                    }

                    if (objUpdatedADDebtAccountLegalCost.SuitFeeAmt == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalCostOriginal.SuitFeeAmt = objUpdatedADDebtAccountLegalCost.SuitFeeAmt;
                    }

                    if (objUpdatedADDebtAccountLegalCost.AttyExpAmt == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalCostOriginal.AttyExpAmt = objUpdatedADDebtAccountLegalCost.AttyExpAmt;
                    }

                    if (objUpdatedADDebtAccountLegalCost.Reference == "" || objUpdatedADDebtAccountLegalCost.Reference == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalCostOriginal.Reference = objUpdatedADDebtAccountLegalCost.Reference.Trim();
                    }

                    if (objUpdatedADDebtAccountLegalCost.Note == "" || objUpdatedADDebtAccountLegalCost.Note == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalCostOriginal.Note = objUpdatedADDebtAccountLegalCost.Note.Trim();
                    }

                    if (objUpdatedADDebtAccountLegalCost.FilingFee == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalCostOriginal.FilingFee = objUpdatedADDebtAccountLegalCost.FilingFee;
                    }

                    if (objUpdatedADDebtAccountLegalCost.ServiceFeeAmt == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalCostOriginal.ServiceFeeAmt = objUpdatedADDebtAccountLegalCost.ServiceFeeAmt;
                    }

                    if (objUpdatedADDebtAccountLegalCost.WritFeeAmt == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalCostOriginal.WritFeeAmt = objUpdatedADDebtAccountLegalCost.WritFeeAmt;
                    }

                    if (objUpdatedADDebtAccountLegalCost.LevyFeeAmt == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalCostOriginal.LevyFeeAmt = objUpdatedADDebtAccountLegalCost.LevyFeeAmt;
                    }

                    if (objUpdatedADDebtAccountLegalCost.AbstractFeeAmt == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalCostOriginal.AbstractFeeAmt = objUpdatedADDebtAccountLegalCost.AbstractFeeAmt;
                    }

                    if (objUpdatedADDebtAccountLegalCost.RecordingFeeAmt == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalCostOriginal.RecordingFeeAmt = objUpdatedADDebtAccountLegalCost.RecordingFeeAmt;
                    }

                    if (objUpdatedADDebtAccountLegalCost.CostExpAmt == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalCostOriginal.CostExpAmt = objUpdatedADDebtAccountLegalCost.CostExpAmt;
                    }

                    objUpdatedADDebtAccountLegalCost.EnteredByUserId = user.Id;
                    objUpdatedADDebtAccountLegalCost.EnteredByUser = user.UserName;

                    objDebtAccountLegalCost = AutoMapper.Mapper.Map<DebtAccountLegalCost>(objADDebtAccountLegalCostOriginal);
                    tblDebtAccountLegalCost = (ITable)objDebtAccountLegalCost;
                    DBO.Provider.Update(ref tblDebtAccountLegalCost);
                }
                else
                {
                    objUpdatedADDebtAccountLegalCost.EnteredByUserId = user.Id;
                    objUpdatedADDebtAccountLegalCost.EnteredByUser = user.UserName;

                    objDebtAccountLegalCost = AutoMapper.Mapper.Map<DebtAccountLegalCost>(objUpdatedADDebtAccountLegalCost);
                    tblDebtAccountLegalCost = (ITable)objDebtAccountLegalCost;
                    DBO.Provider.Create(ref tblDebtAccountLegalCost);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }


        public static DataTable GetDebtAccountSCCRTADtList(string AccountId)
        {
            try
            {
                string mysql = "select * from debtaccountlegalcost where ledgertype in ('CRTA' ) AND debtaccountid = " + AccountId;

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(mysql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }


        public static DataTable GetDebtAccountSCCRTCDtList(string AccountId)
        {
            try
            {
                string mysql = "select * from debtaccountlegalcost where ledgertype in ('CRTC' ) AND debtaccountid = " + AccountId;

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(mysql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        #endregion
    }
}
