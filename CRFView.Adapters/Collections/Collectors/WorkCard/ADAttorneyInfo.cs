﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard
{
    public class ADAttorneyInfo
    {
        #region Properties
        public int PKID { get; set; }
        public string AttyCode { get; set; }
        public string FirmName { get; set; }
        public string AttorneyName { get; set; }
        public string AttyAs { get; set; }
        public string AttyAddress { get; set; }
        public string AttyCity { get; set; }
        public string AttyState { get; set; }
        public string AttyZip { get; set; }
        public string AttyPhoneNo { get; set; }
        public string AttyFax { get; set; }
        public string LawList { get; set; }
        public string AttyNote { get; set; }
        public string EMail { get; set; }
        public string TaxID { get; set; }
        public int LawListId { get; set; } 
        #endregion

        #region CRUD Operations
        //List of AttorneyInfo
        public List<ADAttorneyInfo> ListAttorneyInfo()
        {
            AttorneyInfo myentity = new AttorneyInfo();
            TableView EntityList;
            try
            {
                Query query = new Query(myentity);
                string sql = query.BuildQuery();
                EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                List<ADAttorneyInfo> CTList = (from DataRow dr in dt.Rows
                                            select new ADAttorneyInfo()
                                            {
                                                PKID = Convert.ToInt32(dr["PKID"]),
                                                AttyCode = Convert.ToString(dr["AttyCode"]),
                                                LawList = Convert.ToString(dr["LawList"]),
                                                AttorneyName = Convert.ToString(dr["AttorneyName"]),
                                                FirmName = Convert.ToString(dr["FirmName"])
                                            }).ToList();

                return CTList;
            }
            catch (Exception ex)
            {
                return (new List<ADAttorneyInfo>());
            }
        }

        //Read Details from database
        public static ADAttorneyInfo ReadAttorneyInfo(int pkid)
        {
            AttorneyInfo myentity = new AttorneyInfo();
            ITable myentityItable = myentity;
            DBO.Provider.Read(pkid.ToString(), ref myentityItable);
            myentity = (AttorneyInfo)myentityItable;
            AutoMapper.Mapper.CreateMap<AttorneyInfo, ADAttorneyInfo>();
            return (AutoMapper.Mapper.Map<ADAttorneyInfo>(myentity));
        }
        
        public static int SaveDebtAccountAttorneyInfo(ADAttorneyInfo objUpdatedAttorneyInfo)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADAttorneyInfo, AttorneyInfo>();
                ITable tblAttorneyInfo;
                AttorneyInfo objAttorneyInfo;

                if (objUpdatedAttorneyInfo.PKID != 0)
                {
                    ADAttorneyInfo objAttorneyInfoOriginal = ReadAttorneyInfo(Convert.ToInt32(objUpdatedAttorneyInfo.PKID));

                    #region Payment/NSF

                    //if (objUpdatedAttorneyInfo.AttyNO == "" || objUpdatedAttorneyInfo.AttyNO == null)
                    //{
                    //}
                    //else
                    //{
                    //    objAttorneyInfoOriginal.AttyNO = objUpdatedAttorneyInfo.AttyNO.Trim();
                    //}

                    //if (objUpdatedADDebtAccountMisc.CheckAmt == 0)
                    //{
                    //}
                    //else
                    //{
                    //    objADDebtAccountMiscOriginal.CheckAmt = objUpdatedADDebtAccountMisc.CheckAmt;
                    //}

                    #endregion













                    objAttorneyInfo = AutoMapper.Mapper.Map<AttorneyInfo>(objAttorneyInfoOriginal);
                    tblAttorneyInfo = (ITable)objAttorneyInfo;
                    DBO.Provider.Update(ref tblAttorneyInfo);
                }
                else
                {
                    objAttorneyInfo = AutoMapper.Mapper.Map<AttorneyInfo>(objUpdatedAttorneyInfo);
                    tblAttorneyInfo = (ITable)objAttorneyInfo;
                    DBO.Provider.Create(ref tblAttorneyInfo);


                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        #endregion
    }
}
