﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADvwDebtAccountLetters
    {
        #region Properties
        public int DebtAccountId { get; set; }
        public int UserId { get; set; }
        public int LetterId { get; set; }
        public string LetterCode { get; set; }
        public DateTime DateCreated { get; set; }
        public DateTime DatePrinted { get; set; }
        public int AddressId { get; set; }
        public string UserCode { get; set; }
        public int AddressTypeId { get; set; }
        public int PKID { get; set; }
        public string LetterDescr { get; set; }
        public string AddressType { get; set; }
        public string CustomLetterCode { get; set; }
        public string CustomText { get; set; }
        public string LetterType { get; set; }
        public bool IsCustom { get; set; }
        public int LocationId { get; set; }
        #endregion

        #region CRUD
        
        public static List<ADvwDebtAccountLetters> GetDebtAccountLettersList(DataTable dt)
        {
            try
            {
                List<ADvwDebtAccountLetters> LettersInfo = (from DataRow dr in dt.Rows
                                                                   select new ADvwDebtAccountLetters()
                                                                   {
                                                                       DebtAccountId = ((dr["DebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAccountId"].ToString()) : 0)),
                                                                       UserId = ((dr["UserId"] != DBNull.Value ? Convert.ToInt32(dr["UserId"].ToString()) : 0)),
                                                                       LetterId = ((dr["LetterId"] != DBNull.Value ? Convert.ToInt32(dr["LetterId"].ToString()) : 0)),
                                                                       LetterCode = ((dr["LetterCode"] != DBNull.Value ? dr["LetterCode"].ToString() : "")),
                                                                       DateCreated = ((dr["DateCreated"] != DBNull.Value ? Convert.ToDateTime(dr["DateCreated"]) : DateTime.MinValue)),
                                                                       DatePrinted = ((dr["DatePrinted"] != DBNull.Value ? Convert.ToDateTime(dr["DatePrinted"]) : DateTime.MinValue)),
                                                                       AddressId = ((dr["AddressId"] != DBNull.Value ? Convert.ToInt32(dr["AddressId"].ToString()) : 0)),
                                                                       UserCode = ((dr["UserCode"] != DBNull.Value ? dr["UserCode"].ToString() : "")),
                                                                       AddressTypeId = ((dr["AddressTypeId"] != DBNull.Value ? Convert.ToInt32(dr["AddressTypeId"].ToString()) : 0)),
                                                                       PKID = ((dr["PKID"] != DBNull.Value ? Convert.ToInt32(dr["PKID"].ToString()) : 0)),
                                                                       LetterDescr = ((dr["LetterDescr"] != DBNull.Value ? dr["LetterDescr"].ToString() : "")),
                                                                       AddressType = ((dr["AddressType"] != DBNull.Value ? dr["AddressType"].ToString() : "")),
                                                                       CustomLetterCode = ((dr["CustomLetterCode"] != DBNull.Value ? dr["CustomLetterCode"].ToString() : "")),
                                                                       CustomText = ((dr["CustomText"] != DBNull.Value ? dr["CustomText"].ToString() : "")),
                                                                       LetterType = ((dr["LetterType"] != DBNull.Value ? dr["LetterType"].ToString() : "")),
                                                                       IsCustom = ((dr["IsCustom"] != DBNull.Value ? Convert.ToBoolean(dr["IsCustom"]) : false)),
                                                                       LocationId = ((dr["LocationId"] != DBNull.Value ? Convert.ToInt32(dr["LocationId"].ToString()) : 0)),
                                                                   }).ToList();
                return LettersInfo;
            }
            catch (Exception ex)
            {
                return (new List<ADvwDebtAccountLetters>());
            }
        }
        
        #endregion
    }
}
