﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using CRF.BLL.Users;

namespace CRFView.Adapters
{
    public class ADDebtAccountCreditReport
    {
        #region Properties
        public int Id { get; set; }
        public int DebtAccountId { get; set; }
        public string ContactName { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string MiddleName { get; set; }
        public string SuffixName { get; set; }
        public string SSN { get; set; }
        public string AddressLine1 { get; set; }
        public string AddressLine2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string ECOACode { get; set; }
        public System.DateTime DateOpened { get; set; }
        public decimal CurrentBalance { get; set; }
        public string AccountStatus { get; set; }
        public string PaymentRating { get; set; }
        public string ComplianceCode { get; set; }
        public System.DateTime BillingDate { get; set; }
        public decimal PastDueAmt { get; set; }
        public System.DateTime FirstDelinqDate { get; set; }
        public string OriginalCreditor { get; set; }
        public System.DateTime RequestedOn { get; set; }
        public System.DateTime LastReportedOn { get; set; }
        public System.DateTime UpdatedOn { get; set; }
        public int AccountStatusId { get; set; }
        public int AddressId { get; set; }
        public string CreditReportKey { get; set; }
        public int DebtAddressTypeId { get; set; }
        public int SeqNo { get; set; }
        public decimal HighestCreditAmt { get; set; }
        public System.DateTime LastPayDate { get; set; }
        public System.DateTime? CloseDate { get; set; }
        public string ReportType { get; set; }
        public string CosignerLastName { get; set; }
        public string CosignerFirstName { get; set; }
        public string CosignerMiddleName { get; set; }
        public string CosignerAddressLine1 { get; set; }
        public string CosignerAddressLine2 { get; set; }
        public string CosignerCity { get; set; }
        public string CosignerState { get; set; }
        public string CosignerZip { get; set; }
        public string CosignerECOACode { get; set; }
        public string CosignerSSN { get; set; }
        public string CosignerName { get; set; }
        public System.DateTime BirthDate { get; set; }
        #endregion

        #region CRUD

        public static ADDebtAccountCreditReport ReadDebtAccountCreditReport(int Id)
        {
            DebtAccountCreditReport myentity = new DebtAccountCreditReport();
            ITable myentityItable = myentity;
            DBO.Provider.Read(Id.ToString(), ref myentityItable);
            myentity = (DebtAccountCreditReport)myentityItable;
            AutoMapper.Mapper.CreateMap<DebtAccountCreditReport, ADDebtAccountCreditReport>();
            return (AutoMapper.Mapper.Map<ADDebtAccountCreditReport>(myentity));
        }

        public static List<ADDebtAccountCreditReport> GetDebtAccontInfo(DataTable dt)
        {
            List<ADDebtAccountCreditReport> CreditReport = (from DataRow dr in dt.Rows
                                             select new ADDebtAccountCreditReport()
                                             {
                                                 Id = ((dr["Id"] != DBNull.Value ? Convert.ToInt32(dr["Id"]) : 0)),
                                                 DebtAccountId = ((dr["DebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAccountId"]) : 0)),
                                                 ContactName = ((dr["ContactName"] != DBNull.Value ? dr["ContactName"].ToString() : "")),
                                                 LastName = ((dr["LastName"] != DBNull.Value ? dr["LastName"].ToString() : "")),
                                                 FirstName = ((dr["FirstName"] != DBNull.Value ? dr["FirstName"].ToString() : "")),
                                                 MiddleName = ((dr["MiddleName"] != DBNull.Value ? dr["MiddleName"].ToString() : "")),
                                                 SuffixName = ((dr["SuffixName"] != DBNull.Value ? dr["SuffixName"].ToString() : "")),
                                                 SSN = ((dr["SSN"] != DBNull.Value ? dr["SSN"].ToString() : "")),
                                                 AddressLine1 = ((dr["AddressLine1"] != DBNull.Value ? dr["AddressLine1"].ToString() : "")),
                                                 AddressLine2 = ((dr["AddressLine2"] != DBNull.Value ? dr["AddressLine2"].ToString() : "")),
                                                 City = ((dr["City"] != DBNull.Value ? dr["City"].ToString() : "")),
                                                 State = ((dr["State"] != DBNull.Value ? dr["State"].ToString() : "")),
                                                 Zip = ((dr["Zip"] != DBNull.Value ? dr["Zip"].ToString() : "")),
                                                 ECOACode = ((dr["ECOACode"] != DBNull.Value ? dr["ECOACode"].ToString() : "")),
                                                 DateOpened = ((dr["DateOpened"] != DBNull.Value ? Convert.ToDateTime(dr["DateOpened"]) : DateTime.MinValue)),
                                                 CurrentBalance = ((dr["CurrentBalance"] != DBNull.Value ? Convert.ToDecimal(dr["CurrentBalance"]) : 0)),
                                                 AccountStatus = ((dr["AccountStatus"] != DBNull.Value ? dr["AccountStatus"].ToString() : "")),
                                                 PaymentRating = ((dr["PaymentRating"] != DBNull.Value ? dr["PaymentRating"].ToString() : "")),
                                                 ComplianceCode = ((dr["ComplianceCode"] != DBNull.Value ? dr["ComplianceCode"].ToString() : "")),
                                                 BillingDate = ((dr["BillingDate"] != DBNull.Value ? Convert.ToDateTime(dr["BillingDate"]) : DateTime.MinValue)),
                                                 PastDueAmt = ((dr["PastDueAmt"] != DBNull.Value ? Convert.ToDecimal(dr["PastDueAmt"]) : 0)),
                                                 FirstDelinqDate = ((dr["FirstDelinqDate"] != DBNull.Value ? Convert.ToDateTime(dr["FirstDelinqDate"]) : DateTime.MinValue)),
                                                 OriginalCreditor = ((dr["OriginalCreditor"] != DBNull.Value ? dr["OriginalCreditor"].ToString() : "")),
                                                 RequestedOn = ((dr["RequestedOn"] != DBNull.Value ? Convert.ToDateTime(dr["RequestedOn"]) : DateTime.MinValue)),
                                                 LastReportedOn = ((dr["LastReportedOn"] != DBNull.Value ? Convert.ToDateTime(dr["LastReportedOn"]) : DateTime.MinValue)),
                                                 UpdatedOn = ((dr["UpdatedOn"] != DBNull.Value ? Convert.ToDateTime(dr["UpdatedOn"]) : DateTime.MinValue)),
                                                 AccountStatusId = ((dr["AccountStatusId"] != DBNull.Value ? Convert.ToInt32(dr["AccountStatusId"]) : 0)),
                                                 AddressId = ((dr["AddressId"] != DBNull.Value ? Convert.ToInt32(dr["AddressId"]) : 0)),
                                                 CreditReportKey = ((dr["CreditReportKey"] != DBNull.Value ? dr["CreditReportKey"].ToString() : "")),
                                                 DebtAddressTypeId = ((dr["DebtAddressTypeId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAddressTypeId"]) : 0)),
                                                 SeqNo = ((dr["SeqNo"] != DBNull.Value ? Convert.ToInt32(dr["SeqNo"]) : 0)),
                                                 HighestCreditAmt = ((dr["HighestCreditAmt"] != DBNull.Value ? Convert.ToDecimal(dr["HighestCreditAmt"]) : 0)),
                                                 LastPayDate = ((dr["LastPayDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastPayDate"]) : DateTime.MinValue)),
                                                 CloseDate = ((dr["CloseDate"] != DBNull.Value ? Convert.ToDateTime(dr["CloseDate"]) : DateTime.MinValue)),
                                                 ReportType = ((dr["ReportType"] != DBNull.Value ? dr["ReportType"].ToString() : "")),
                                                 CosignerLastName = ((dr["CosignerLastName"] != DBNull.Value ? dr["CosignerLastName"].ToString() : "")),
                                                 CosignerFirstName = ((dr["CosignerFirstName"] != DBNull.Value ? dr["CosignerFirstName"].ToString() : "")),
                                                 CosignerMiddleName = ((dr["CosignerMiddleName"] != DBNull.Value ? dr["CosignerMiddleName"].ToString() : "")),
                                                 CosignerAddressLine1 = ((dr["CosignerAddressLine1"] != DBNull.Value ? dr["CosignerAddressLine1"].ToString() : "")),
                                                 CosignerAddressLine2 = ((dr["CosignerAddressLine2"] != DBNull.Value ? dr["CosignerAddressLine2"].ToString() : "")),
                                                 CosignerCity = ((dr["CosignerCity"] != DBNull.Value ? dr["CosignerCity"].ToString() : "")),
                                                 CosignerState = ((dr["CosignerState"] != DBNull.Value ? dr["CosignerState"].ToString() : "")),
                                                 CosignerZip = ((dr["CosignerZip"] != DBNull.Value ? dr["CosignerZip"].ToString() : "")),
                                                 CosignerECOACode = ((dr["CosignerECOACode"] != DBNull.Value ? dr["CosignerECOACode"].ToString() : "")),
                                                 CosignerSSN = ((dr["CosignerSSN"] != DBNull.Value ? dr["CosignerSSN"].ToString() : "")),
                                                 CosignerName = ((dr["CosignerName"] != DBNull.Value ? dr["CosignerName"].ToString() : "")),
                                                 BirthDate = ((dr["BirthDate"] != DBNull.Value ? Convert.ToDateTime(dr["BirthDate"]) : DateTime.MinValue)),
                                             }).ToList();
            return CreditReport;
        }

        public static int SaveDebtAccountCreditReportIndividual(ADDebtAccountCreditReport objUpdatedADDebtAccountCreditReport)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADDebtAccountCreditReport, DebtAccountCreditReport>();
                ITable tblDebtAccountCreditReport;
                DebtAccountCreditReport objDebtAccountCreditReport;



                if (objUpdatedADDebtAccountCreditReport.Id != 0)
                {

                    ADDebtAccountCreditReport objADDebtAccountCreditReportOriginal = ReadDebtAccountCreditReport(Convert.ToInt32(objUpdatedADDebtAccountCreditReport.Id));

                    if (objUpdatedADDebtAccountCreditReport.FirstName == "" || objUpdatedADDebtAccountCreditReport.FirstName == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.FirstName = objUpdatedADDebtAccountCreditReport.FirstName.Trim();
                    }

                    if (objUpdatedADDebtAccountCreditReport.MiddleName == "" || objUpdatedADDebtAccountCreditReport.MiddleName == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.MiddleName = objUpdatedADDebtAccountCreditReport.MiddleName.Trim();
                    }

                    if (objUpdatedADDebtAccountCreditReport.LastName == "" || objUpdatedADDebtAccountCreditReport.LastName == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.LastName = objUpdatedADDebtAccountCreditReport.LastName.Trim();
                    }

                    if (objUpdatedADDebtAccountCreditReport.SuffixName == "" || objUpdatedADDebtAccountCreditReport.SuffixName == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.SuffixName = objUpdatedADDebtAccountCreditReport.SuffixName.Trim();
                    }


                    if (objUpdatedADDebtAccountCreditReport.AddressLine1 == "" || objUpdatedADDebtAccountCreditReport.AddressLine1 == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.AddressLine1 = objUpdatedADDebtAccountCreditReport.AddressLine1.Trim();
                    }

                    if (objUpdatedADDebtAccountCreditReport.AddressLine2 == "" || objUpdatedADDebtAccountCreditReport.AddressLine2 == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.AddressLine2 = objUpdatedADDebtAccountCreditReport.AddressLine2.Trim();
                    }

                    if (objUpdatedADDebtAccountCreditReport.City == "" || objUpdatedADDebtAccountCreditReport.City == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.City = objUpdatedADDebtAccountCreditReport.City.Trim();
                    }

                    if (objUpdatedADDebtAccountCreditReport.State == "" || objUpdatedADDebtAccountCreditReport.State == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.State = objUpdatedADDebtAccountCreditReport.State.Trim();
                    }

                    if (objUpdatedADDebtAccountCreditReport.Zip == "" || objUpdatedADDebtAccountCreditReport.Zip == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.Zip = objUpdatedADDebtAccountCreditReport.Zip.Trim();
                    }
                 
                    if (objUpdatedADDebtAccountCreditReport.SSN == "" || objUpdatedADDebtAccountCreditReport.SSN == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.SSN = objUpdatedADDebtAccountCreditReport.SSN.Trim();
                    }

                    if (objUpdatedADDebtAccountCreditReport.BirthDate.ToString() == "" || objUpdatedADDebtAccountCreditReport.BirthDate == null)
                    {
                    }
                    else
                    {
                       objADDebtAccountCreditReportOriginal.BirthDate = objUpdatedADDebtAccountCreditReport.BirthDate;
                    }


                    if (objUpdatedADDebtAccountCreditReport.OriginalCreditor == "" || objUpdatedADDebtAccountCreditReport.OriginalCreditor == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.OriginalCreditor = objUpdatedADDebtAccountCreditReport.OriginalCreditor.Trim();
                    }

                    if (objUpdatedADDebtAccountCreditReport.CurrentBalance == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.CurrentBalance = objUpdatedADDebtAccountCreditReport.CurrentBalance;
                    }

                    if (objUpdatedADDebtAccountCreditReport.DateOpened.ToString() == "" || objUpdatedADDebtAccountCreditReport.DateOpened == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.DateOpened = objUpdatedADDebtAccountCreditReport.DateOpened;
                    }

                    if (objUpdatedADDebtAccountCreditReport.BillingDate.ToString() == "" || objUpdatedADDebtAccountCreditReport.BillingDate == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.BillingDate = objUpdatedADDebtAccountCreditReport.BillingDate;
                    }
           
                    if (objUpdatedADDebtAccountCreditReport.ComplianceCode == "" || objUpdatedADDebtAccountCreditReport.ComplianceCode == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.ComplianceCode = objUpdatedADDebtAccountCreditReport.ComplianceCode.Trim();
                    }


                    if (objUpdatedADDebtAccountCreditReport.AccountStatus == "" || objUpdatedADDebtAccountCreditReport.AccountStatus == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.AccountStatus = objUpdatedADDebtAccountCreditReport.AccountStatus.Trim();
                    }

                    if (objUpdatedADDebtAccountCreditReport.AccountStatusId <= 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.AccountStatusId = objUpdatedADDebtAccountCreditReport.AccountStatusId;
                    }

                    if (objUpdatedADDebtAccountCreditReport.PastDueAmt == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.PastDueAmt = objUpdatedADDebtAccountCreditReport.PastDueAmt;
                    }

                    if (objUpdatedADDebtAccountCreditReport.HighestCreditAmt == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.HighestCreditAmt = objUpdatedADDebtAccountCreditReport.HighestCreditAmt;
                    }

                    objADDebtAccountCreditReportOriginal.UpdatedOn = DateTime.Now;
                    objDebtAccountCreditReport = AutoMapper.Mapper.Map<DebtAccountCreditReport>(objADDebtAccountCreditReportOriginal);
                    tblDebtAccountCreditReport = (ITable)objDebtAccountCreditReport;
                    DBO.Provider.Update(ref tblDebtAccountCreditReport);
                }
                else
                {
                    string sql = "Select Max(SeqNo) as MaxSeqNo from DebtAccountCreditReport where DebtAccountId = " + objUpdatedADDebtAccountCreditReport.DebtAccountId;
                    System.Data.DataSet ds = DBO.Provider.GetDataSet(sql);
                    DataTable dt = ds.Tables[0];
                    int mySeqNo = ((dt.Rows[0]["MaxSeqNo"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["MaxSeqNo"]) : 0));
                    if (mySeqNo <= 0)
                    {
                        mySeqNo = 1;
                        objUpdatedADDebtAccountCreditReport.SeqNo = mySeqNo;
                    }
                    else
                    {
                        mySeqNo = mySeqNo + 1;
                        objUpdatedADDebtAccountCreditReport.SeqNo = mySeqNo;
                  }

                    objUpdatedADDebtAccountCreditReport.RequestedOn = DateTime.Now;
                    objUpdatedADDebtAccountCreditReport.LastReportedOn = DateTime.Now;
                    objDebtAccountCreditReport = AutoMapper.Mapper.Map<DebtAccountCreditReport>(objUpdatedADDebtAccountCreditReport);
                    tblDebtAccountCreditReport = (ITable)objDebtAccountCreditReport;
                    DBO.Provider.Create(ref tblDebtAccountCreditReport);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }
        
        public static int SaveDebtAccountCreditReportCommercial(ADDebtAccountCreditReport objUpdatedADDebtAccountCreditReport)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADDebtAccountCreditReport, DebtAccountCreditReport>();
                ITable tblDebtAccountCreditReport;
                DebtAccountCreditReport objDebtAccountCreditReport;

                if (objUpdatedADDebtAccountCreditReport.Id != 0)
                {

                    ADDebtAccountCreditReport objADDebtAccountCreditReportOriginal = ReadDebtAccountCreditReport(Convert.ToInt32(objUpdatedADDebtAccountCreditReport.Id));

                    if (objUpdatedADDebtAccountCreditReport.CosignerName == "" || objUpdatedADDebtAccountCreditReport.CosignerName == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.CosignerName = objUpdatedADDebtAccountCreditReport.CosignerName.Trim();
                    }

                    if (objUpdatedADDebtAccountCreditReport.CosignerAddressLine1 == "" || objUpdatedADDebtAccountCreditReport.CosignerAddressLine1 == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.CosignerAddressLine1 = objUpdatedADDebtAccountCreditReport.CosignerAddressLine1.Trim();
                    }

                    if (objUpdatedADDebtAccountCreditReport.CosignerAddressLine2 == "" || objUpdatedADDebtAccountCreditReport.CosignerAddressLine2 == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.CosignerAddressLine2 = objUpdatedADDebtAccountCreditReport.CosignerAddressLine2.Trim();
                    }

                    if (objUpdatedADDebtAccountCreditReport.CosignerCity == "" || objUpdatedADDebtAccountCreditReport.CosignerCity == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.CosignerCity = objUpdatedADDebtAccountCreditReport.CosignerCity.Trim();
                    }

                    if (objUpdatedADDebtAccountCreditReport.CosignerState == "" || objUpdatedADDebtAccountCreditReport.CosignerState == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.CosignerState = objUpdatedADDebtAccountCreditReport.CosignerState.Trim();
                    }

                    if (objUpdatedADDebtAccountCreditReport.CosignerZip == "" || objUpdatedADDebtAccountCreditReport.CosignerZip == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.CosignerZip = objUpdatedADDebtAccountCreditReport.CosignerZip.Trim();
                    }

                    if (objUpdatedADDebtAccountCreditReport.CosignerSSN == "" || objUpdatedADDebtAccountCreditReport.CosignerSSN == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.CosignerSSN = objUpdatedADDebtAccountCreditReport.CosignerSSN.Trim();
                    }

                    if (objUpdatedADDebtAccountCreditReport.BirthDate.ToString() == "" || objUpdatedADDebtAccountCreditReport.BirthDate == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.BirthDate = objUpdatedADDebtAccountCreditReport.BirthDate;
                    }


                    if (objUpdatedADDebtAccountCreditReport.OriginalCreditor == "" || objUpdatedADDebtAccountCreditReport.OriginalCreditor == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.OriginalCreditor = objUpdatedADDebtAccountCreditReport.OriginalCreditor.Trim();
                    }

                    if (objUpdatedADDebtAccountCreditReport.CurrentBalance == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.CurrentBalance = objUpdatedADDebtAccountCreditReport.CurrentBalance;
                    }

                    if (objUpdatedADDebtAccountCreditReport.DateOpened.ToString() == "" || objUpdatedADDebtAccountCreditReport.DateOpened == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.DateOpened = objUpdatedADDebtAccountCreditReport.DateOpened;
                    }

                    if (objUpdatedADDebtAccountCreditReport.BillingDate.ToString() == "" || objUpdatedADDebtAccountCreditReport.BillingDate == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.BillingDate = objUpdatedADDebtAccountCreditReport.BillingDate;
                    }

                    if (objUpdatedADDebtAccountCreditReport.ComplianceCode == "" || objUpdatedADDebtAccountCreditReport.ComplianceCode == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.ComplianceCode = objUpdatedADDebtAccountCreditReport.ComplianceCode.Trim();
                    }


                    if (objUpdatedADDebtAccountCreditReport.AccountStatus == "" || objUpdatedADDebtAccountCreditReport.AccountStatus == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.AccountStatus = objUpdatedADDebtAccountCreditReport.AccountStatus.Trim();
                    }

                    if (objUpdatedADDebtAccountCreditReport.AccountStatusId <= 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.AccountStatusId = objUpdatedADDebtAccountCreditReport.AccountStatusId;
                    }

                    if (objUpdatedADDebtAccountCreditReport.PastDueAmt == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.PastDueAmt = objUpdatedADDebtAccountCreditReport.PastDueAmt;
                    }

                    if (objUpdatedADDebtAccountCreditReport.HighestCreditAmt == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountCreditReportOriginal.HighestCreditAmt = objUpdatedADDebtAccountCreditReport.HighestCreditAmt;
                    }

                    objADDebtAccountCreditReportOriginal.UpdatedOn = DateTime.Now;
                    objDebtAccountCreditReport = AutoMapper.Mapper.Map<DebtAccountCreditReport>(objADDebtAccountCreditReportOriginal);
                    tblDebtAccountCreditReport = (ITable)objDebtAccountCreditReport;
                    DBO.Provider.Update(ref tblDebtAccountCreditReport);
                }
                else
                {
                    string sql = "Select Max(SeqNo) as MaxSeqNo from DebtAccountCreditReport where DebtAccountId = " + objUpdatedADDebtAccountCreditReport.DebtAccountId;
                    System.Data.DataSet ds = DBO.Provider.GetDataSet(sql);
                    DataTable dt = ds.Tables[0];
                    int mySeqNo = ((dt.Rows[0]["MaxSeqNo"] != DBNull.Value ? Convert.ToInt32(dt.Rows[0]["MaxSeqNo"]) : 0));
                    if (mySeqNo <= 0)
                    {
                        mySeqNo = 1;
                        objUpdatedADDebtAccountCreditReport.SeqNo = mySeqNo;
                    }
                    else
                    {
                        mySeqNo = mySeqNo + 1;
                        objUpdatedADDebtAccountCreditReport.SeqNo = mySeqNo;
                    }

                    objUpdatedADDebtAccountCreditReport.RequestedOn = DateTime.Now;
                    objUpdatedADDebtAccountCreditReport.LastReportedOn = DateTime.Now;
                    objDebtAccountCreditReport = AutoMapper.Mapper.Map<DebtAccountCreditReport>(objUpdatedADDebtAccountCreditReport);
                    tblDebtAccountCreditReport = (ITable)objDebtAccountCreditReport;
                    DBO.Provider.Create(ref tblDebtAccountCreditReport);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }


        public static bool DeleteCreditReportIndividual(string Id)
        {
            try
            {
                ITable IDebtAccountCreditReport;
                DebtAccountCreditReport objDebtAccountCreditReport = new DebtAccountCreditReport();
                objDebtAccountCreditReport.Id = Convert.ToInt32(Id);
                IDebtAccountCreditReport = objDebtAccountCreditReport;
                DBO.Provider.Delete(ref IDebtAccountCreditReport);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public static bool DeleteCreditReportCommercial(string Id)
        {
            try
            {
                ITable IDebtAccountCreditReport;
                DebtAccountCreditReport objDebtAccountCreditReport = new DebtAccountCreditReport();
                objDebtAccountCreditReport.Id = Convert.ToInt32(Id);
                IDebtAccountCreditReport = objDebtAccountCreditReport;
                DBO.Provider.Delete(ref IDebtAccountCreditReport);
                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        //Extract Individual CreditReport (Export .CSV File)
        public static string ExtractIndividualCreditReport()
        {
            string pdfPath = null;
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                string myspreadsheet=  CRF.BLL.CRFView.CRFView.GetFileName(ref user, "CREDITREPORTEXTRACT", "CreditReport", ".CSV", true);
                
                var MYSPROC = new cview_Processing_GetCreditReportExtract();
                DBO.Provider.ExecNonQuery(MYSPROC);

                var myds = DBO.Provider.GetDataSet(MYSPROC);
                var vw = DBO.Provider.GetTableView(MYSPROC);
                vw.ExportToCSV(myspreadsheet);

                pdfPath = myspreadsheet;

               // return vw.Count;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return "";
            }
            return pdfPath;
        }

        //Extract Commercial CreditReport (Export .CSV File)
        public static string ExtractCommercialCreditReport()
        {
            string pdfPath = null;
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                string myspreadsheet = CRF.BLL.CRFView.CRFView.GetFileName(ref user, "CREDITREPORTEXTRACT", "CreditReport_Com", ".CSV", true);

                var MYSPROC = new cview_Processing_GetCreditReportExtractCOM();
                DBO.Provider.ExecNonQuery(MYSPROC);

                var myds = DBO.Provider.GetDataSet(MYSPROC);
                var vw = DBO.Provider.GetTableView(MYSPROC);
                vw.ExportToCSV(myspreadsheet);

                pdfPath = myspreadsheet;

                // return vw.Count;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return "";
            }
            return pdfPath;
        }

        #endregion
    }
}
