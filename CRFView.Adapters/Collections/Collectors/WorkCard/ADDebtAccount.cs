﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using CRFView.Adapters.Collections.Collectors.WorkCard.ViewModels;
using HDS.DAL.COMMON;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using CRF.BLL.Common;
using System.Text;
using System.Threading.Tasks;
using CRFView.Adapters.Accounting.AccountingCollections.PaymentEntry.PaymentEntryVM;
using CRFView.Adapters.Accounting.AccountingCollections.PaymentEntry;
using CRF.BLL;

namespace CRFView.Adapters
{
    public class ADDebtAccount
    {
        #region Properties
        public int DebtAccountId { get; set; }

        public int BatchId { get; set; }
        public int ClientId { get; set; }
        public int ContractId { get; set; }
        public int DebtId { get; set; }
        public string FileNumber { get; set; }
        public int LocationId { get; set; }
        public string Location { get; set; }
        public int CollectionStatusId { get; set; }
        public string CollectionStatus { get; set; }
        public int TempOwnerId { get; set; }
        public string TempOwner { get; set; }
        public string ClientAgentCode { get; set; }
        public string Priority { get; set; }
        public string AccountNo { get; set; }
        public string CloseCode { get; set; }
        public string DebtAccountNote { get; set; }
        public string ServiceType { get; set; }
        public string ClaimNum { get; set; }
        public int Age { get; set; }
        public bool IsNoCBR { get; set; }
        public bool IsCreditReport { get; set; }
        public System.DateTime ReferalDate { get; set; }
        public System.DateTime LastServiceDate { get; set; }
        public System.DateTime LastPaymentDate { get; set; }
        public System.DateTime LastUpdateDate { get; set; }
        public System.DateTime LastTrustDate { get; set; }
        public System.DateTime NextContactDate { get; set; }
        public int NextContactNo { get; set; }
        public System.DateTime NextLetterDate { get; set; }
        public int NextLetterSeqNo { get; set; }
        public System.DateTime NextStatusDate { get; set; }
        public System.DateTime NextLegalDate { get; set; }
        public System.DateTime InterestFromDate { get; set; }
        public System.DateTime InterestThruDate { get; set; }
        public System.DateTime CloseDate { get; set; }
        public System.DateTime FirstReportDate { get; set; }
        public System.DateTime LastReportDate { get; set; }
        public System.DateTime NextReportDate { get; set; }
        public string CreditReportStatus { get; set; }
        public System.DateTime ClientLastViewDate { get; set; }
        public int ClientLastViewBy { get; set; }
        public bool IsEstAudit { get; set; }
        public bool IsSecondary { get; set; }
        public bool IsSkip { get; set; }
        public bool IsJudgmentAcct { get; set; }
        public bool IsIntlAcct { get; set; }
        public int TempId { get; set; }
        public string AltKey { get; set; }
        public decimal FixedRate { get; set; }
        public string FeeCode { get; set; }
        public bool IsRecalcTotals { get; set; }
        public decimal TotAsgAmt { get; set; }
        public decimal TotAsgRcvAmt { get; set; }
        public decimal TotCollFeeAmt { get; set; }
        public decimal TotCollFeeRcvAmt { get; set; }
        public decimal TotAdjAmt { get; set; }
        public decimal TotPmtAmt { get; set; }
        public decimal TotBalAmt { get; set; }
        public decimal InterestRate { get; set; }
        public decimal TotIntAmt { get; set; }
        public decimal TotIntRcvAmt { get; set; }
        public decimal TotCostRcvAmt { get; set; }
        public decimal TotCostExpAmt { get; set; }
        public decimal TotAttyExpAmt { get; set; }
        public decimal TotAttyRcvAmt { get; set; }
        public decimal TotalDue { get; set; }
        public decimal TotBillableAmt { get; set; }
        public decimal TotBilledAmt { get; set; }
        public decimal TotRemitAmt { get; set; }
        public decimal TotFeeAmt { get; set; }
        public decimal TotRcvByAgencyAmt { get; set; }
        public decimal TotRcvByClientAmt { get; set; }
        public decimal TotFeeByAgencyAmt { get; set; }
        public decimal TotFeeByClientAmt { get; set; }
        public decimal TotDueFromClientAmt { get; set; }
        public decimal TotRemitToClientAmt { get; set; }
        public decimal TotOvrPmtAmt { get; set; }
        public decimal TotIntSharedAmt { get; set; }
        public decimal TotFilingFeeAmt { get; set; }
        public decimal TotServiceFeeAmt { get; set; }
        public decimal TotWritFeeAmt { get; set; }
        public decimal TotLevyFeeAmt { get; set; }
        public decimal TotAgencyFeeAmt { get; set; }
        public decimal TotAttySettleAmt { get; set; }
        public decimal TotAttyCourtAmt { get; set; }
        public decimal TotAttyBilledAmt { get; set; }
        public decimal TotAttyFeeOwedAmt { get; set; }
        public decimal TotAttyFeeAmt { get; set; }
        public decimal PromisedPmtAmt { get; set; }
        public System.DateTime PromisedDate { get; set; }
        public System.DateTime LastReminderDate { get; set; }
        public System.DateTime NextPaymentDate { get; set; }
        public int DayInterval { get; set; }
        public int ReminderDays { get; set; }
        public int NumberofPmts { get; set; }
        public int ReminderLetterId { get; set; }
        public bool IsBroken { get; set; }
        public int BatchDebtAccountId { get; set; }
        public string BranchNum { get; set; }
        public int CommRateId { get; set; }
        public int SalesTeamId { get; set; }
        public decimal TotAdjNoCFAmt { get; set; }
        public decimal TotBalNoCFAmt { get; set; }
        public decimal ExchangeRate { get; set; }
        public int PriorityId { get; set; }
        public bool IsRapidRebate { get; set; }
        public bool IsForwarding { get; set; }
        public bool IsSmallClaims { get; set; }
        public bool IsNoCollectFee { get; set; }
        public decimal TotAttyOwedAmt { get; set; }
        public int ParentDebtAccountId { get; set; }
        public bool IsTenDayContactPlan { get; set; }
        public System.DateTime PaymentDueDate { get; set; }
        public decimal UsageFee { get; set; }
        public decimal EarlyTerminationFee { get; set; }
        public string ServiceState { get; set; }

        #endregion

        #region CRUD
        public static ADDebtAccount ReadDebtAccount(int DebtAccountId)
        {
            DebtAccount myentity = new DebtAccount();
            ITable myentityItable = myentity;
            DBO.Provider.Read(DebtAccountId.ToString(), ref myentityItable);
            myentity = (DebtAccount)myentityItable;
            AutoMapper.Mapper.CreateMap<DebtAccount, ADDebtAccount>();
            return (AutoMapper.Mapper.Map<ADDebtAccount>(myentity));
        }

        public static bool UpdateDebtAccount(ADDebtAccount objUpdatedADDebtAccount)
        {
            try
            {
                ADDebtAccount objADDebtAccount = new ADDebtAccount();
                objADDebtAccount = ReadDebtAccount(objUpdatedADDebtAccount.DebtAccountId);

                objADDebtAccount.DebtAccountNote = objUpdatedADDebtAccount.DebtAccountNote;
                AutoMapper.Mapper.CreateMap<ADDebtAccount, DebtAccount>();
                DebtAccount myentity = new DebtAccount();
                myentity = AutoMapper.Mapper.Map<DebtAccount>(objADDebtAccount);


                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static bool UpdateDebtAccountEditView(ADDebtAccount objUpdatedADDebtAccount)
        {
            try
            {
                ADDebtAccount objADDebtAccountOriginal = ReadDebtAccount(objUpdatedADDebtAccount.DebtAccountId);

                objADDebtAccountOriginal.DebtAccountNote = objUpdatedADDebtAccount.DebtAccountNote;
                objADDebtAccountOriginal.BranchNum = objUpdatedADDebtAccount.BranchNum;
                if (objUpdatedADDebtAccount.CloseDate.ToString() == "" || objUpdatedADDebtAccount.CloseDate == null)
                {
                }
                else
                {
                    objADDebtAccountOriginal.CloseDate = objUpdatedADDebtAccount.CloseDate;
                }

                if (objUpdatedADDebtAccount.NextStatusDate.ToString() == "" || objUpdatedADDebtAccount.NextStatusDate == null)
                {
                }
                else
                {
                    objADDebtAccountOriginal.NextStatusDate = objUpdatedADDebtAccount.NextStatusDate;
                }


                if (objUpdatedADDebtAccount.PaymentDueDate.ToString() == "" || objUpdatedADDebtAccount.PaymentDueDate == null)
                {
                }
                else
                {
                    objADDebtAccountOriginal.PaymentDueDate = objUpdatedADDebtAccount.PaymentDueDate;
                }

                objADDebtAccountOriginal.IsIntlAcct = objUpdatedADDebtAccount.IsIntlAcct;
                objADDebtAccountOriginal.IsJudgmentAcct = objUpdatedADDebtAccount.IsJudgmentAcct;
                objADDebtAccountOriginal.IsSkip = objUpdatedADDebtAccount.IsSkip;
                objADDebtAccountOriginal.IsSecondary = objUpdatedADDebtAccount.IsSecondary;
                objADDebtAccountOriginal.LastUpdateDate = objUpdatedADDebtAccount.LastUpdateDate;

                AutoMapper.Mapper.CreateMap<ADDebtAccount, DebtAccount>();
                DebtAccount myentity;
                myentity = AutoMapper.Mapper.Map<DebtAccount>(objADDebtAccountOriginal);
                ITable tblI = myentity;
                DBO.Provider.Update(ref tblI);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }


        public static bool UpdateDebtAccountMoreInfoEditView(ADDebtAccount objUpdatedADDebtAccount)
        {
            try
            {
                ADDebtAccount objADDebtAccountOriginal = ReadDebtAccount(objUpdatedADDebtAccount.DebtAccountId);

                if (objUpdatedADDebtAccount.NextContactDate.ToString() == "" || objUpdatedADDebtAccount.NextContactDate == null)
                {
                }
                else
                {
                    objADDebtAccountOriginal.NextContactDate = objUpdatedADDebtAccount.NextContactDate;
                }

                if (objUpdatedADDebtAccount.LocationId.ToString() == "" || objUpdatedADDebtAccount.LocationId == 0)
                {
                }
                else
                {
                    objADDebtAccountOriginal.LocationId = objUpdatedADDebtAccount.LocationId;
                }

                if (objUpdatedADDebtAccount.TempOwnerId.ToString() == "" || objUpdatedADDebtAccount.TempOwnerId == 0)
                {
                }
                else
                {
                    objADDebtAccountOriginal.TempOwnerId = objUpdatedADDebtAccount.TempOwnerId;
                }

                if (objUpdatedADDebtAccount.Priority.ToString() == "" || objUpdatedADDebtAccount.Priority == null)
                {
                }
                else
                {
                    objADDebtAccountOriginal.Priority = objUpdatedADDebtAccount.Priority;
                }

                AutoMapper.Mapper.CreateMap<ADDebtAccount, DebtAccount>();
                DebtAccount myentity;
                myentity = AutoMapper.Mapper.Map<DebtAccount>(objADDebtAccountOriginal);
                ITable tblI = myentity;
                DBO.Provider.Update(ref tblI);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static PaymentCalculatorVM CalculatePayment(string DebtAccountId, string Frequency, DateTime FirstPayDate, DateTime LastPayDate, decimal TotalDueP, bool IsNoInterest)
        {
            try
            {
                cview_Accounts_GetPromisePaymentAmount Sp_OBJ = new cview_Accounts_GetPromisePaymentAmount();
                Sp_OBJ.DebtAccountId = Convert.ToInt32(DebtAccountId);
                Sp_OBJ.Frequency = Frequency;
                Sp_OBJ.FirstPayDate = FirstPayDate;
                Sp_OBJ.LastPayDate = LastPayDate;
                Sp_OBJ.IsNoInterest = IsNoInterest;
                Sp_OBJ.TotalDue = Convert.ToInt32(TotalDueP);
                TableView myview = DBO.Provider.GetTableView(Sp_OBJ);
                DataTable dt = myview.ToTable();
                List<PaymentCalculatorVM> Vm = (from DataRow dr in dt.Rows
                                                select new PaymentCalculatorVM()
                                                {
                                                    LastPmtDateP = ((dr["LastPaymentDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastPaymentDate"]) : DateTime.MinValue)),
                                                    PmtAmtP = ((dr["PaymentAmt"] != DBNull.Value ? Convert.ToInt32(dr["PaymentAmt"]) : 0)),
                                                    NumOfPmts = ((dr["NumOfPmts"] != DBNull.Value ? Convert.ToInt32(dr["NumOfPmts"]) : 0))

                                                }).ToList();


                return Vm.First();
            }
            catch (Exception ex)
            {
                return new PaymentCalculatorVM();
            }
        }

        //Print Payment Schedule
        public string PrintPaymentSchedule(string DebtAccountId, double PaymentAmt, string Frequency, DateTime FirstPayDate, decimal TotalDue, bool IsNoInterest)
        {
            string pdfPath = null;
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var myds = new DataSet();
                var Sp_OBJ = new cview_Accounts_GetPromiseByTimeFrame();
                Sp_OBJ.DebtAccountId = Convert.ToInt32(DebtAccountId);
                Sp_OBJ.PaymentAmt = Convert.ToInt32(PaymentAmt);
                Sp_OBJ.Frequency = Frequency;
                Sp_OBJ.FirstPayDate = FirstPayDate;
                Sp_OBJ.IsNoInterest = IsNoInterest;
                Sp_OBJ.TotalDue = Convert.ToInt32(TotalDue);

                myds = DBO.Provider.GetDataSet(Sp_OBJ);
                DataTable dt1 = myds.Tables[1];
                TableView myview = new TableView(dt1, "TableItems");

                pdfPath = CRF.BLL.COMMON.Reporting.PrintC1ReportForPaymentSchedule(user, "Collectviewreports.xml", "CollectionPaySchedule", myview, CRF.BLL.COMMON.PrintMode.PrintToPDF);
            }

            catch (Exception ex)
            {
                return "";
            }
            return pdfPath;
        }

        public static PaymentCalculatorVM GetPromiseByTimeFrame(string DebtAccountId, double PaymentAmt, string Frequency, DateTime FirstPayDate, decimal TotalDue, bool IsNoInterest)
        {
            try
            {
                cview_Accounts_GetPromiseByTimeFrame Sp_OBJ = new cview_Accounts_GetPromiseByTimeFrame();
                Sp_OBJ.DebtAccountId = Convert.ToInt32(DebtAccountId);
                Sp_OBJ.PaymentAmt = Convert.ToInt32(PaymentAmt);
                Sp_OBJ.Frequency = Frequency;
                Sp_OBJ.FirstPayDate = FirstPayDate;
                Sp_OBJ.IsNoInterest = IsNoInterest;
                Sp_OBJ.TotalDue = Convert.ToInt32(TotalDue);
                TableView myview = DBO.Provider.GetTableView(Sp_OBJ);
                DataTable dt = myview.ToTable();
                List<PaymentCalculatorVM> Vm = (from DataRow dr in dt.Rows
                                                select new PaymentCalculatorVM()
                                                {
                                                    LastPmtDate = ((dr["LastPayDate"] != DBNull.Value ? Convert.ToDateTime(dr["LastPayDate"]) : DateTime.MinValue)),
                                                    LastPmtAmt = ((dr["LastPaymentAmt"] != DBNull.Value ? Convert.ToInt32(dr["LastPaymentAmt"]) : 0)),
                                                    NumOfPmts = ((dr["NumOfPmts"] != DBNull.Value ? Convert.ToInt32(dr["NumOfPmts"]) : 0))
                                                }).ToList();

                return Vm.First();
            }

            catch (Exception ex)
            {
                return new PaymentCalculatorVM();
            }


            #endregion
        }

        //Print Payment Schedule Amount
        public string PrintPaymentScheduleAmount(string DebtAccountId, string Frequency, DateTime FirstPayDate, DateTime LastPayDate, decimal TotalDue, bool IsNoInterest)
        {
            string pdfPath = null;
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var myds = new DataSet();
                var Sp_OBJ = new cview_Accounts_GetPromisePaymentAmount();
                Sp_OBJ.DebtAccountId = Convert.ToInt32(DebtAccountId);
                Sp_OBJ.Frequency = Frequency;
                Sp_OBJ.FirstPayDate = FirstPayDate;
                Sp_OBJ.LastPayDate = LastPayDate;
                Sp_OBJ.IsNoInterest = IsNoInterest;
                Sp_OBJ.TotalDue = Convert.ToInt32(TotalDue);

                myds = DBO.Provider.GetDataSet(Sp_OBJ);
                DataTable dt1 = myds.Tables[1];
                TableView myview = new TableView(dt1, "TableItems");

                pdfPath = CRF.BLL.COMMON.Reporting.PrintC1ReportForPaymentScheduleAmount(user, "Collectviewreports.xml", "CollectionPaySchedule", myview, CRF.BLL.COMMON.PrintMode.PrintToPDF);
            }

            catch (Exception ex)
            {
                return "";
            }
            return pdfPath;
        }

        public static int ProcessActionCode(string ActionCodesId, string DebtAccountId, string CollectionStatusId)
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
               
                var MYSPROC = new CRF.BLL.CRFDB.SPROCS.cview_Accounts_UpdateActions();
                MYSPROC.DebtAccountId = Convert.ToInt32(DebtAccountId);
                MYSPROC.UserId = user.Id;
                MYSPROC.UserName = user.UserName;

                //string sSql = "Select * from CollectionActions where ActionCode = '" + ActionCodesId + "'";

                string sSql = "Select * from CollectionActions where Id In(" + ActionCodesId + ")";

                TableView myview = DBO.Provider.GetTableView(sSql);
                if (myview.Count == 0)
                {
                    return 0;
                }
                else
                {
                    DataTable dt = myview.ToTable();
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        MYSPROC.ActionIds = dt.Rows[i]["Id"].ToString();
                        if (Convert.ToBoolean(dt.Rows[i]["IsDropReviewDate"]) == true && Convert.ToBoolean(dt.Rows[i]["IsChangeStatus"]) == false)
                        {
                            //  var myCollectionStatus = new CollectionStatus();
                            CollectionStatus myCollectionStatus = new CollectionStatus();
                            ITable myentityItable = myCollectionStatus;
                            DBO.Provider.Read(CollectionStatusId, ref myentityItable);
                            myCollectionStatus = (CollectionStatus)myentityItable;
                            if (myCollectionStatus.IsDropContact == false)
                            {
                                return 1;
                            }
                        }
                        DBO.Provider.ExecNonQuery(MYSPROC);
                    }
                    return 2;
                }
            }
            catch (Exception ex)
            {
                return -1;
            }
        }
        //Print Credit Card Auth Report
        public string PrintCreditCardAuthReport(string DebtAccountId)
        {
            string pdfPath = null;
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();

                var myds = new DataSet();
                var Sp_OBJ = new cview_Accounts_GetAccountInfo();
                Sp_OBJ.AccountId = DebtAccountId;

                myds = DBO.Provider.GetDataSet(Sp_OBJ);
                DataTable dt1 = myds.Tables[1];
                dt1.Columns.Add("DebtorNum", typeof(System.String));
                dt1.Rows[0]["DebtorNum"] = DebtAccountId;
                TableView myview = new TableView(dt1, "vwAccountList");
                pdfPath = CRF.BLL.COMMON.Reporting.PrintC1ReportForCreditCardAuth(user, "CollectviewReports.xml", "CreditCardAuth 1", myview, CRF.BLL.COMMON.PrintMode.PrintToPDF, DebtAccountId);
            }

            catch (Exception ex)
            {
                return "";
            }
            return pdfPath;
        }



        public static int SaveDebtAccount(ADDebtAccount objUpdatedADDebtAccount)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADDebtAccount, DebtAccount>();
                ITable tblDebtAccount;
                DebtAccount objDebtAccount;



                if (objUpdatedADDebtAccount.DebtAccountId != 0)
                {
                    ADDebtAccount objADDebtAccountOriginal = ReadDebtAccount(Convert.ToInt32(objUpdatedADDebtAccount.DebtAccountId));

                    #region Account Action

                    if (objUpdatedADDebtAccount.NextContactDate == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADDebtAccountOriginal.NextContactDate = objUpdatedADDebtAccount.NextContactDate;
                    }
                    #endregion

                    objDebtAccount = AutoMapper.Mapper.Map<DebtAccount>(objADDebtAccountOriginal);
                    tblDebtAccount = (ITable)objDebtAccount;
                    DBO.Provider.Update(ref tblDebtAccount);
                }
                else
                {
                    objDebtAccount = AutoMapper.Mapper.Map<DebtAccount>(objUpdatedADDebtAccount);
                    tblDebtAccount = (ITable)objDebtAccount;
                    DBO.Provider.Create(ref tblDebtAccount);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }


        public static int SaveDebtAccount_Letters(ADDebtAccount objUpdatedADDebtAccount)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADDebtAccount, DebtAccount>();
                ITable tblDebtAccount;
                DebtAccount objDebtAccount;



                if (objUpdatedADDebtAccount.DebtAccountId != 0)
                {
                    ADDebtAccount objADDebtAccountOriginal = ReadDebtAccount(Convert.ToInt32(objUpdatedADDebtAccount.DebtAccountId));


                    if (objUpdatedADDebtAccount.NextContactDate == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADDebtAccountOriginal.NextContactDate = objUpdatedADDebtAccount.NextContactDate;
                    }

                    if (objUpdatedADDebtAccount.NextStatusDate == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADDebtAccountOriginal.NextStatusDate = objUpdatedADDebtAccount.NextStatusDate;
                    }


                    objDebtAccount = AutoMapper.Mapper.Map<DebtAccount>(objADDebtAccountOriginal);
                    tblDebtAccount = (ITable)objDebtAccount;
                    DBO.Provider.Update(ref tblDebtAccount);
                }
                else
                {
                    objDebtAccount = AutoMapper.Mapper.Map<DebtAccount>(objUpdatedADDebtAccount);
                    tblDebtAccount = (ITable)objDebtAccount;
                    DBO.Provider.Create(ref tblDebtAccount);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int SaveDebtAccount_ChangeDebtorAccountInfo(ADDebtAccount objUpdatedADDebtAccount)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADDebtAccount, DebtAccount>();
                ITable tblDebtAccount;
                DebtAccount objDebtAccount;

                if (objUpdatedADDebtAccount.DebtAccountId != 0)
                {
                    ADDebtAccount objADDebtAccountOriginal = ReadDebtAccount(Convert.ToInt32(objUpdatedADDebtAccount.DebtAccountId));

                    #region ChangeDebtorAccountInfo

                    if (objUpdatedADDebtAccount.AccountNo == "" || objUpdatedADDebtAccount.AccountNo == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountOriginal.AccountNo = objUpdatedADDebtAccount.AccountNo;
                    }

                    if (objUpdatedADDebtAccount.ReferalDate == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADDebtAccountOriginal.ReferalDate = objUpdatedADDebtAccount.ReferalDate;
                    }

                    if (objUpdatedADDebtAccount.LastServiceDate == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADDebtAccountOriginal.LastServiceDate = objUpdatedADDebtAccount.LastServiceDate;
                    }

                    if (objUpdatedADDebtAccount.LastPaymentDate == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADDebtAccountOriginal.LastPaymentDate = objUpdatedADDebtAccount.LastPaymentDate;
                    }

                    if (objUpdatedADDebtAccount.CommRateId.ToString() == "" || objUpdatedADDebtAccount.CommRateId == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountOriginal.CommRateId = objUpdatedADDebtAccount.CommRateId;
                    }

                    if (objUpdatedADDebtAccount.IsNoCollectFee.ToString() == "")
                    {
                    }
                    else
                    {
                        objADDebtAccountOriginal.IsNoCollectFee = objUpdatedADDebtAccount.IsNoCollectFee;
                    }

                    if (objUpdatedADDebtAccount.IsRecalcTotals.ToString() == "")
                    {
                    }
                    else
                    {
                        objADDebtAccountOriginal.IsRecalcTotals = objUpdatedADDebtAccount.IsRecalcTotals;
                    }

                    if (objUpdatedADDebtAccount.IsTenDayContactPlan.ToString() == "")
                    {
                    }
                    else
                    {
                        objADDebtAccountOriginal.IsTenDayContactPlan = objUpdatedADDebtAccount.IsTenDayContactPlan;
                    }

                    objADDebtAccountOriginal.LastUpdateDate = DateTime.Now;
                    #endregion

                    objDebtAccount = AutoMapper.Mapper.Map<DebtAccount>(objADDebtAccountOriginal);
                    tblDebtAccount = (ITable)objDebtAccount;
                    DBO.Provider.Update(ref tblDebtAccount);
                }
                else
                {
                    objDebtAccount = AutoMapper.Mapper.Map<DebtAccount>(objUpdatedADDebtAccount);
                    tblDebtAccount = (ITable)objDebtAccount;
                    DBO.Provider.Create(ref tblDebtAccount);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int SaveDebt_ChangeDebtorAccountInfo_ClientInfo(string DebtAccountId, string ClientId)
        {
            try
            {
                var MYSPROC = new cview_Accounts_SwitchClient();
                MYSPROC.DebtaccountId = Convert.ToInt32(DebtAccountId);
                MYSPROC.ClientId = Convert.ToInt32(ClientId);
                DBO.Provider.ExecNonQuery(MYSPROC);

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static int SaveDebtAccount_DeskAssignment_Desk(ADDebtAccount objUpdatedADDebtAccount)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADDebtAccount, DebtAccount>();
                ITable tblDebtAccount;
                DebtAccount objDebtAccount;

                if (objUpdatedADDebtAccount.DebtAccountId != 0)
                {
                    ADDebtAccount objADDebtAccountOriginal = ReadDebtAccount(Convert.ToInt32(objUpdatedADDebtAccount.DebtAccountId));

                    if (objUpdatedADDebtAccount.LocationId.ToString() == "" || objUpdatedADDebtAccount.LocationId == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountOriginal.LocationId = objUpdatedADDebtAccount.LocationId;
                    }

                    objDebtAccount = AutoMapper.Mapper.Map<DebtAccount>(objADDebtAccountOriginal);
                    tblDebtAccount = (ITable)objDebtAccount;
                    DBO.Provider.Update(ref tblDebtAccount);
                }
                else
                {
                    objDebtAccount = AutoMapper.Mapper.Map<DebtAccount>(objUpdatedADDebtAccount);
                    tblDebtAccount = (ITable)objDebtAccount;
                    DBO.Provider.Create(ref tblDebtAccount);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }


        public static int SaveDebtAccount_DeskAssignment_Action(ADDebtAccount objDebtAccount)
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                var Sp_OBJ = new cview_Accounts_CreateActionHistory();
                Sp_OBJ.DebtAccountId = objDebtAccount.DebtAccountId;
                Sp_OBJ.ActionId = objDebtAccount.CollectionStatusId;
                Sp_OBJ.UserId = user.Id;
                Sp_OBJ.UserName = user.UserName;
                DBO.Provider.ExecNonQuery(Sp_OBJ);
                return 1;
            }

            catch (Exception ex)
            {
                return 0;
            }

        }



        public static int SaveNewAdjustment(NewAdjustmentVM ObjNewAdjustmentVM)
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                AutoMapper.Mapper.CreateMap<AdDedtAccountLedger, DebtAccountLedger>();
                ITable tblDebtAccountLedger;
                DebtAccountLedger objDebtAccountLedger;

                AdDedtAccountLedger objAdDedtAccountLedger = AdDedtAccountLedger.ReadDedtAccountLedger(ObjNewAdjustmentVM.DebtAccountLedgerId);

                //AdDedtAccountLedger objAdDedtAccountLedger = new AdDedtAccountLedger();

                objAdDedtAccountLedger.DebtAccountId = ObjNewAdjustmentVM.DebtAccountId;
                objAdDedtAccountLedger.EnteredByUserId = user.Id;
                objAdDedtAccountLedger.SubLedgerTypeId = ObjNewAdjustmentVM.SubLedgerTypeId;
                objAdDedtAccountLedger.LedgerType = "BADJ";

                var mytable = new SubLedgerType();
                ITable IAcc = mytable;
                DBO.Provider.Read(Convert.ToString(ObjNewAdjustmentVM.SubLedgerTypeId), ref IAcc);
                mytable = (SubLedgerType)IAcc;

                objAdDedtAccountLedger.SubLedgerType = mytable.SubLedgerType;
                objAdDedtAccountLedger.TrxDate = ObjNewAdjustmentVM.TrxDate;
                objAdDedtAccountLedger.RemitDate = ObjNewAdjustmentVM.TrxDate;
                objAdDedtAccountLedger.ReceiptDate = ObjNewAdjustmentVM.TrxDate;
                objAdDedtAccountLedger.RemitDate = null;

                objAdDedtAccountLedger.TrxAmt = 0;
                objAdDedtAccountLedger.AdjAmt = Convert.ToDecimal(ObjNewAdjustmentVM.distAdjAmt);
                objAdDedtAccountLedger.PmtAmt = 0;
                objAdDedtAccountLedger.Rate = 0;
                objAdDedtAccountLedger.FeeAmt = 0;

                objAdDedtAccountLedger.ReceiptDate = ObjNewAdjustmentVM.TrxDate;
                objAdDedtAccountLedger.IsRemitted = false;
                objAdDedtAccountLedger.InvoicedDate = null;


                if (ObjNewAdjustmentVM.DebtAccountLedgerId < 1)
                {
                    objDebtAccountLedger = AutoMapper.Mapper.Map<DebtAccountLedger>(objAdDedtAccountLedger);
                    tblDebtAccountLedger = (ITable)objDebtAccountLedger;
                    DBO.Provider.Create(ref tblDebtAccountLedger);
                }

                else
                {

                    objDebtAccountLedger = AutoMapper.Mapper.Map<DebtAccountLedger>(objAdDedtAccountLedger);
                    tblDebtAccountLedger = (ITable)objDebtAccountLedger;
                    DBO.Provider.Update(ref tblDebtAccountLedger);
                }

                ADDebtAccount objAdDedtAccount = ReadDebtAccount(ObjNewAdjustmentVM.DebtAccountId);

                AutoMapper.Mapper.CreateMap<ADDebtAccount, DebtAccount>();
                DebtAccount myentity;
                myentity = AutoMapper.Mapper.Map<DebtAccount>(objAdDedtAccount);
                //myentity = AutoMapper.Mapper.Map<Job>(objUpdatedADJob);


                ITable tblI = myentity;
                DBO.Provider.Update(ref tblI);



                //    Dim myaccount As New TABLES.DebtAccount
                //DBO.Read(myitem.DebtAccountId, myaccount)
                //myaccount.IsRecalcTotals = True
                //DBO.Update(myaccount)


                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }


        public static int SaveNewPaymentDetails(NewPaymentVM ObjNewPaymentVM)
        {
            try
            {
                CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                AutoMapper.Mapper.CreateMap<AdDedtAccountLedger, DebtAccountLedger>();
                ITable tblDebtAccountLedger;
                DebtAccountLedger objDebtAccountLedger;

                AdDedtAccountLedger objAdDedtAccountLedger = new AdDedtAccountLedger();
                objAdDedtAccountLedger = AdDedtAccountLedger.ReadDedtAccountLedger(ObjNewPaymentVM.DebtAccountLedgerId);
                objAdDedtAccountLedger.DebtAccountLedgerId = ObjNewPaymentVM.DebtAccountLedgerId;
                objAdDedtAccountLedger.DebtAccountId = ObjNewPaymentVM.DebtAccountId;
                objAdDedtAccountLedger.EnteredByUserId = user.Id;
                objAdDedtAccountLedger.TrxAmt = ObjNewPaymentVM.objAdDedtAccountLedger.TrxAmt;
                objAdDedtAccountLedger.IsAdvanceCC = ObjNewPaymentVM.objAdDedtAccountLedger.IsAdvanceCC;
                objAdDedtAccountLedger.ExchangeRate = ObjNewPaymentVM.objAdDedtAccountLedger.ExchangeRate;
                objAdDedtAccountLedger.BankName = ObjNewPaymentVM.objAdDedtAccountLedger.BankName;
                objAdDedtAccountLedger.BankAcctNum = ObjNewPaymentVM.objAdDedtAccountLedger.BankAcctNum;
                objAdDedtAccountLedger.CheckMaker = ObjNewPaymentVM.objAdDedtAccountLedger.CheckMaker;

                ClientContract mycontract = new ClientContract();

                if (mycontract.IsIntlClient == false || objAdDedtAccountLedger.ExchangeRate == 0)
                {
                    ObjNewPaymentVM.objAdDedtAccountLedger.ExchangeRate = 1;
                }

                objAdDedtAccountLedger.IsOmitFromStmt = ObjNewPaymentVM.objAdDedtAccountLedger.IsOmitFromStmt;

                if (objAdDedtAccountLedger.IsOmitFromStmt == true)
                {
                    objAdDedtAccountLedger.RemitDate = null;
                }
                else if (ObjNewPaymentVM.objAdDedtAccountLedger.RemitDate == DateTime.MinValue)
                {
                    objAdDedtAccountLedger.RemitDate = null;
                }
                else
                {
                    objAdDedtAccountLedger.RemitDate = ObjNewPaymentVM.objAdDedtAccountLedger.RemitDate;
                }
                string sql = "select Location from Location where LocationId =" + ObjNewPaymentVM.objAdDedtAccountLedger.DeskNumId;
                TableView myview = DBO.Provider.GetTableView(sql);

                objAdDedtAccountLedger.DeskNumId = ObjNewPaymentVM.objAdDedtAccountLedger.DeskNumId;
                if(myview.Count == 0)
                {
                    objAdDedtAccountLedger.DeskNum = "";
                }

                else
                {
                    objAdDedtAccountLedger.DeskNum = myview.get_RowItem("Location").ToString();

                }
                objAdDedtAccountLedger.AsgAmt = 0;
                objAdDedtAccountLedger.AsgRcvAmt = Convert.ToDecimal(ObjNewPaymentVM.distAsgRcvAmt.Trim('$'));
                objAdDedtAccountLedger.FeeRcvAmt = Convert.ToDecimal(ObjNewPaymentVM.distFeeRcv.Trim('$'));
                objAdDedtAccountLedger.IntRcvAmt = Convert.ToDecimal(ObjNewPaymentVM.DistIntAmt.Trim('$'));

                objAdDedtAccountLedger.CostRcvAmt = Convert.ToDecimal(ObjNewPaymentVM.distCostAmt.Trim('$'));
                objAdDedtAccountLedger.AttyRcvAmt = Convert.ToDecimal(ObjNewPaymentVM.distAttyRcv.Trim('$'));
                objAdDedtAccountLedger.AttyOwedAmt = Convert.ToDecimal(ObjNewPaymentVM.AttOwedAmt.Trim('$'));
                objAdDedtAccountLedger.CostRcvAmtOnPIF = Convert.ToDecimal(ObjNewPaymentVM.CostRcvAmtOnPIF.Trim('$'));
                objAdDedtAccountLedger.OvrPmtAmt = Convert.ToDecimal(ObjNewPaymentVM.OvrPmtAmt.Trim('$'));
                objAdDedtAccountLedger.RebateAmt = Convert.ToDecimal(ObjNewPaymentVM.RebateAmt.Trim('$'));

                objAdDedtAccountLedger.PmtAmt = Convert.ToDecimal(ObjNewPaymentVM.distAsgRcvAmt.Trim('$')) + Convert.ToDecimal(ObjNewPaymentVM.distFeeRcv.Trim('$'));
                objAdDedtAccountLedger.Rate = Convert.ToDecimal(ObjNewPaymentVM.feerate);
                objAdDedtAccountLedger.FeeAmt = ((objAdDedtAccountLedger.PmtAmt * objAdDedtAccountLedger.Rate) / 100);

                //Added 5 / 21 / 2012
                objAdDedtAccountLedger.PrincDueAmt = Convert.ToDecimal(ObjNewPaymentVM.newPrincDue.Trim('$'));

                //Interest Shared Calculation
                if (mycontract.IsInterestSharedAsPrinc == true)
                {
                    objAdDedtAccountLedger.IntSharedAmt = ((Convert.ToDecimal(ObjNewPaymentVM.DistIntAmt.Trim('$')) * objAdDedtAccountLedger.Rate) / 100);
                    objAdDedtAccountLedger.IntShareRate = objAdDedtAccountLedger.Rate;
                }
                else if (mycontract.IsInterestShared == true)
                {
                    objAdDedtAccountLedger.IntShareRate = mycontract.InterestShareRate;
                    objAdDedtAccountLedger.IntSharedAmt = ((Convert.ToDecimal(ObjNewPaymentVM.DistIntAmt.Trim('$')) * (objAdDedtAccountLedger.IntShareRate) / 100));
                }
                else
                {
                    objAdDedtAccountLedger.IntShareRate = Convert.ToDecimal(100.0);
                    objAdDedtAccountLedger.IntSharedAmt = Convert.ToDecimal(ObjNewPaymentVM.DistIntAmt.Trim('$'));
                }

                objAdDedtAccountLedger.ReceiptDate = ObjNewPaymentVM.objAdDedtAccountLedger.ReceiptDate;
                objAdDedtAccountLedger.Reference = ObjNewPaymentVM.objAdDedtAccountLedger.Reference;
                objAdDedtAccountLedger.IsRemitted = false;
                objAdDedtAccountLedger.InvoicedDate = null;

                objAdDedtAccountLedger.CollectionStatus = "PAY";

                if (ObjNewPaymentVM.rbtStatementStatus == "rbtLIF")
                {
                    objAdDedtAccountLedger.CollectionStatus = "LIF";
                }
                if (ObjNewPaymentVM.rbtStatementStatus == "rbtLPP")
                {
                    objAdDedtAccountLedger.CollectionStatus = "LPP";
                }
                if (ObjNewPaymentVM.rbtStatementStatus == "rbtPCF")
                {
                    objAdDedtAccountLedger.CollectionStatus = "PCF";
                }
                if (ObjNewPaymentVM.rbtStatementStatus == "rbtPIF")
                {
                    objAdDedtAccountLedger.CollectionStatus = "PIF";
                }
                if (ObjNewPaymentVM.rbtStatementStatus == "rbtPTD")
                {
                    objAdDedtAccountLedger.CollectionStatus = "PTD";
                }
                if (ObjNewPaymentVM.rbtStatementStatus == "rbtSIF")
                {
                    objAdDedtAccountLedger.CollectionStatus = "SIF";
                }

                if (ObjNewPaymentVM.rbtStatementStatus == "rbtPPF")
                {
                    objAdDedtAccountLedger.CollectionStatus = "PPF";
                }
                if (ObjNewPaymentVM.rbtStatementStatus == "rbtPPO")
                {
                    objAdDedtAccountLedger.CollectionStatus = "PPO";
                }
                if (ObjNewPaymentVM.rbtStatementStatus == "rbtINT")
                {
                    objAdDedtAccountLedger.CollectionStatus = "INT";
                }
                if (ObjNewPaymentVM.rbtStatementStatus == "rbtCC")
                {
                    objAdDedtAccountLedger.CollectionStatus = "CC";
                }
                if (ObjNewPaymentVM.rbtStatementStatus == "rbtNSF")
                {
                    objAdDedtAccountLedger.CollectionStatus = "NSF";
                }

                objAdDedtAccountLedger.LedgerType = "PMTC";

                if (ObjNewPaymentVM.rbtVal == "rbtPMTA")
                {
                    objAdDedtAccountLedger.LedgerType = "PMTA";
                }

                objAdDedtAccountLedger.TrxDate = ObjNewPaymentVM.objAdDedtAccountLedger.ReceiptDate;

                if (ObjNewPaymentVM.DebtAccountLedgerId < 1)
                {
                    objDebtAccountLedger = AutoMapper.Mapper.Map<DebtAccountLedger>(objAdDedtAccountLedger);
                    tblDebtAccountLedger = (ITable)objDebtAccountLedger;
                    DBO.Provider.Create(ref tblDebtAccountLedger);
                }

                else
                {

                    objDebtAccountLedger = AutoMapper.Mapper.Map<DebtAccountLedger>(objAdDedtAccountLedger);
                    tblDebtAccountLedger = (ITable)objDebtAccountLedger;
                    DBO.Provider.Update(ref tblDebtAccountLedger);
                }


                ADDebtAccount objAdDedtAccount = ReadDebtAccount(ObjNewPaymentVM.DebtAccountId);

                AutoMapper.Mapper.CreateMap<ADDebtAccount, DebtAccount>();
                objAdDedtAccount.IsRecalcTotals = true;
                DebtAccount myentity;
                myentity = AutoMapper.Mapper.Map<DebtAccount>(objAdDedtAccount);
                //myentity = AutoMapper.Mapper.Map<Job>(objUpdatedADJob);


                ITable tblI = myentity;
                DBO.Provider.Update(ref tblI);

               // ConfigFile myconfig = new ConfigFile();
                //if (ObjNewPaymentVM.rbtPMTA == true)
                //{
                //    myconfig.SetValue("LASTLEDGERTYPE", "PMTA");
                //    myconfig.SaveFile();
                //}
                //else
                //{
                //    myconfig.SetValue("LASTLEDGERTYPE", "PMTC");
                //    myconfig.SaveFile();
                //}




                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }


        public static int TotalDistributions(NewPaymentVM ObjNewPaymentVM)
        {

            ObjNewPaymentVM.newPrincDue = (Convert.ToDouble(ObjNewPaymentVM.currPrincDue.Trim('$')) - Convert.ToDouble(ObjNewPaymentVM.distAsgRcvAmt.Trim('$'))).ToString();
            ObjNewPaymentVM.newFeeDue = (Convert.ToDouble(ObjNewPaymentVM.currFeeDue) - Convert.ToDouble(ObjNewPaymentVM.distFeeRcv)).ToString();
            ObjNewPaymentVM.newCostDue = (Convert.ToDouble(ObjNewPaymentVM.currCostDue) - Convert.ToDouble(ObjNewPaymentVM.distCostAmt)).ToString();
            ObjNewPaymentVM.newIntDue = (Convert.ToDouble(ObjNewPaymentVM.currIntDue) - Convert.ToDouble(ObjNewPaymentVM.DistIntAmt)).ToString();

            
            AdDedtAccountLedger objAdDedtAccountLedger = new AdDedtAccountLedger();

            objAdDedtAccountLedger.m_amt[2, 1] = Convert.ToDecimal(ObjNewPaymentVM.distAsgRcvAmt);
            objAdDedtAccountLedger.m_amt[2, 2] = Convert.ToDecimal(ObjNewPaymentVM.distFeeRcv);
            objAdDedtAccountLedger.m_amt[2, 3] = Convert.ToDecimal(ObjNewPaymentVM.distCostAmt);
            objAdDedtAccountLedger.m_amt[2, 4] = Convert.ToDecimal(ObjNewPaymentVM.DistIntAmt);
            objAdDedtAccountLedger.m_amt[2, 5] = Convert.ToDecimal(ObjNewPaymentVM.distAttyRcv);
            objAdDedtAccountLedger.m_amt[2, 6] = Convert.ToDecimal(ObjNewPaymentVM.AttOwedAmt);
            objAdDedtAccountLedger.m_amt[2, 7] = Convert.ToDecimal(ObjNewPaymentVM.CostRcvAmtOnPIF);
            objAdDedtAccountLedger.m_amt[2, 8] = Convert.ToDecimal(ObjNewPaymentVM.OvrPmtAmt);


            //objAdDedtAccountLedger.m_amt[3, 1] = objAdDedtAccountLedger.m_amt[1, 1] - objAdDedtAccountLedger.m_amt[2, 1];
            //objAdDedtAccountLedger.m_amt[3, 2] = objAdDedtAccountLedger.m_amt[1, 2] - objAdDedtAccountLedger.m_amt[2, 2];
            //objAdDedtAccountLedger.m_amt[3, 3] = objAdDedtAccountLedger.m_amt[1, 3] - objAdDedtAccountLedger.m_amt[2, 3];
            //objAdDedtAccountLedger.m_amt[3, 4] = objAdDedtAccountLedger.m_amt[1, 4] - objAdDedtAccountLedger.m_amt[2, 4];
            //objAdDedtAccountLedger.m_amt[3, 10] = objAdDedtAccountLedger.m_amt[3, 1] + objAdDedtAccountLedger.m_amt[3, 2] + objAdDedtAccountLedger.m_amt[3, 3] + objAdDedtAccountLedger.m_amt[3, 4];

            objAdDedtAccountLedger.m_amt[2, 10] = objAdDedtAccountLedger.m_amt[2, 1] + objAdDedtAccountLedger.m_amt[2, 2] + objAdDedtAccountLedger.m_amt[2, 3] + objAdDedtAccountLedger.m_amt[2, 4] + objAdDedtAccountLedger.m_amt[2, 7] + objAdDedtAccountLedger.m_amt[2, 8];
            //objAdDedtAccountLedger.m_amt[3, 11] = ObjNewPaymentVM.objAdDedtAccountLedger.TrxAmt - objAdDedtAccountLedger.m_amt[2, 10];

            ObjNewPaymentVM.applied = Convert.ToString(objAdDedtAccountLedger.m_amt[2, 10]);
           // ObjNewPaymentVM.NewBalance = Convert.ToString(objAdDedtAccountLedger.m_amt[3, 10]);
            //ObjNewPaymentVM.undistributed = ObjNewPaymentVM.NewBalance = Convert.ToString(objAdDedtAccountLedger.m_amt[3, 11]);
            ObjNewPaymentVM.billable = Convert.ToString(objAdDedtAccountLedger.m_amt[2, 1]) + Convert.ToString(objAdDedtAccountLedger.m_amt[2, 2]);

            //if (Convert.ToInt32(ObjNewPaymentVM.billable) != 0 && Convert.ToInt32(ObjNewPaymentVM.feeearned) != 0)
            //{
                //decimal MFEE = Convert.ToDecimal(ObjNewPaymentVM.feeearned);
                ////decimal MBILL = Convert.ToDecimal(ObjNewPaymentVM.billable);
                //decimal MRATE = (MFEE / MBILL) * 100;
                //ObjNewPaymentVM.feerate = Convert.ToString(MRATE);
            //}

            //11 / 29 / 11

           

            //ObjNewPaymentVM.feeearned = (Convert.ToDouble(ObjNewPaymentVM.feerate) * Convert.ToDouble(ObjNewPaymentVM.billable).ToString() / 100);

            //Me.undistributed.ErrorMessage = ""

            if (Convert.ToInt32(ObjNewPaymentVM.undistributed) != 0)
            {
                //Me.undistributed.IsValid = False
                //Me.undistributed.Validate()
            }

            //return ObjNewPaymentVM;

            return 1;
        }



    }
}
