﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard
{
    public class ADDebtAccountMisc
    {
        #region Properties
        public int PKID { get; set; }
        public int DebtAccountId { get; set; }
        public decimal PymntAmt { get; set; }
        public string EscrowNo { get; set; }
        public decimal CheckAmt { get; set; }
        public string EscrowCompany { get; set; }
        public string CheckNo { get; set; }
        public string EscrowAdd { get; set; }
        public decimal Damages { get; set; }
        public string EscrowCity { get; set; }
        public string EscrowSt { get; set; }
        public string EscrowZip { get; set; }
        public string BankName { get; set; }
        public decimal SettlementAmt { get; set; }
        public string BKAdd { get; set; }
        public string BKCity { get; set; }
        public string BKSt { get; set; }
        public string BKZip { get; set; }
        public string BKCaseNo { get; set; }
        public string Bank { get; set; }
        public string BankAddress { get; set; }
        public string BankCity { get; set; }
        public string BankState { get; set; }
        public string BankZip { get; set; }
        public string AccountNum { get; set; }
        public string PropertyAddress { get; set; }
        public string PropertyCity { get; set; }
        public string PropertyState { get; set; }
        public string PropertyZip { get; set; }
        public string APNNum { get; set; }
        public string DunHist { get; set; }
        public string StHist { get; set; }
        public string DebtorBKName { get; set; }
        public string DebtorWebAdd { get; set; }
        public decimal TotDamages { get; set; }
        public string DebtorFirmName { get; set; }
        public string DebtorAttyName { get; set; }
        public string DebtorAttyAddress { get; set; }
        public string DebtorAttyCity { get; set; }
        public string DebtorAttyState { get; set; }
        public string DebtorAttyZip { get; set; }
        public string DebtorAttyPhone { get; set; }
        public string DebtorAttyEmail { get; set; }
        #endregion

        public static ADDebtAccountMisc GetMiscInfo(DataTable dt)
        {
            try
            {
                List<ADDebtAccountMisc> Misc = (from DataRow dr in dt.Rows
                                                                     select new ADDebtAccountMisc()
                                                                     {
                                                                         PKID = ((dr["PKID"] != DBNull.Value ? Convert.ToInt32(dr["PKID"].ToString()) : 0)),
                                                                         DebtAccountId = ((dr["DebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAccountId"].ToString()) : 0)),
                                                                         PymntAmt = ((dr["PymntAmt"] != DBNull.Value ? Convert.ToDecimal(dr["PymntAmt"].ToString()) : 0)),
                                                                         EscrowNo = ((dr["EscrowNo"] != DBNull.Value ? dr["EscrowNo"].ToString() : "")),
                                                                         CheckAmt = ((dr["CheckAmt"] != DBNull.Value ? Convert.ToDecimal(dr["CheckAmt"].ToString()) : 0)),
                                                                         EscrowCompany = ((dr["EscrowCompany"] != DBNull.Value ? dr["EscrowCompany"].ToString() : "")),
                                                                         CheckNo = ((dr["CheckNo"] != DBNull.Value ? dr["CheckNo"].ToString() : "")),
                                                                         EscrowAdd = ((dr["EscrowAdd"] != DBNull.Value ? dr["EscrowAdd"].ToString() : "")),
                                                                         Damages = ((dr["Damages"] != DBNull.Value ? Convert.ToDecimal(dr["Damages"].ToString()) : 0)),
                                                                         EscrowCity = ((dr["EscrowCity"] != DBNull.Value ? dr["EscrowCity"].ToString() : "")),
                                                                         EscrowSt = ((dr["EscrowSt"] != DBNull.Value ? dr["EscrowSt"].ToString() : "")),
                                                                         EscrowZip = ((dr["EscrowZip"] != DBNull.Value ? dr["EscrowZip"].ToString() : "")),
                                                                         BankName = ((dr["BankName"] != DBNull.Value ? dr["BankName"].ToString() : "")),
                                                                         SettlementAmt = ((dr["SettlementAmt"] != DBNull.Value ? Convert.ToDecimal(dr["SettlementAmt"].ToString()) : 0)),
                                                                         BKAdd = ((dr["BKAdd"] != DBNull.Value ? dr["BKAdd"].ToString() : "")),
                                                                         BKCity = ((dr["BKCity"] != DBNull.Value ? dr["BKCity"].ToString() : "")),
                                                                         BKSt = ((dr["BankName"] != DBNull.Value ? dr["BKSt"].ToString() : "")),
                                                                         BKZip = ((dr["BKZip"] != DBNull.Value ? dr["BKZip"].ToString() : "")),
                                                                         BKCaseNo = ((dr["BKCaseNo"] != DBNull.Value ? dr["BKCaseNo"].ToString() : "")),
                                                                         Bank = ((dr["Bank"] != DBNull.Value ? dr["Bank"].ToString() : "")),
                                                                         BankAddress = ((dr["BankAddress"] != DBNull.Value ? dr["BankAddress"].ToString() : "")),
                                                                         BankCity = ((dr["BankCity"] != DBNull.Value ? dr["BankCity"].ToString() : "")),
                                                                         BankState = ((dr["BankState"] != DBNull.Value ? dr["BankState"].ToString() : "")),
                                                                         BankZip = ((dr["BankZip"] != DBNull.Value ? dr["BankZip"].ToString() : "")),
                                                                         AccountNum = ((dr["AccountNum"] != DBNull.Value ? dr["AccountNum"].ToString() : "")),
                                                                         PropertyAddress = ((dr["PropertyAddress"] != DBNull.Value ? dr["PropertyAddress"].ToString() : "")),
                                                                         PropertyCity = ((dr["PropertyCity"] != DBNull.Value ? dr["PropertyCity"].ToString() : "")),
                                                                         PropertyState = ((dr["PropertyState"] != DBNull.Value ? dr["PropertyState"].ToString() : "")),
                                                                         PropertyZip = ((dr["PropertyZip"] != DBNull.Value ? dr["PropertyZip"].ToString() : "")),
                                                                         APNNum = ((dr["APNNum"] != DBNull.Value ? dr["APNNum"].ToString() : "")),
                                                                         DunHist = ((dr["DunHist"] != DBNull.Value ? dr["DunHist"].ToString() : "")),
                                                                         StHist = ((dr["StHist"] != DBNull.Value ? dr["StHist"].ToString() : "")),
                                                                         DebtorBKName = ((dr["DebtorBKName"] != DBNull.Value ? dr["DebtorBKName"].ToString() : "")),
                                                                         DebtorWebAdd = ((dr["DebtorWebAdd"] != DBNull.Value ? dr["DebtorWebAdd"].ToString() : "")),
                                                                         TotDamages = ((dr["TotDamages"] != DBNull.Value ? Convert.ToDecimal(dr["TotDamages"].ToString()) : 0)),
                                                                         DebtorFirmName = ((dr["DebtorFirmName"] != DBNull.Value ? dr["DebtorFirmName"].ToString() : "")),
                                                                         DebtorAttyName = ((dr["DebtorAttyName"] != DBNull.Value ? dr["DebtorAttyName"].ToString() : "")),
                                                                         DebtorAttyAddress = ((dr["DebtorAttyAddress"] != DBNull.Value ? dr["DebtorAttyAddress"].ToString() : "")),
                                                                         DebtorAttyCity = ((dr["DebtorAttyCity"] != DBNull.Value ? dr["DebtorAttyCity"].ToString() : "")),
                                                                         DebtorAttyState = ((dr["DebtorAttyState"] != DBNull.Value ? dr["DebtorAttyState"].ToString() : "")),
                                                                         DebtorAttyZip = ((dr["DebtorAttyZip"] != DBNull.Value ? dr["DebtorAttyZip"].ToString() : "")),
                                                                         DebtorAttyPhone = ((dr["DebtorAttyPhone"] != DBNull.Value ? dr["DebtorAttyPhone"].ToString() : "")),
                                                                         DebtorAttyEmail = ((dr["DebtorAttyEmail"] != DBNull.Value ? dr["DebtorAttyEmail"].ToString() : "")),
                                                                     }).ToList();
                return Misc.First();
            }
            catch (Exception ex)
            {
                return (new ADDebtAccountMisc());
            }
        }

        public static ADDebtAccountMisc ReadDebtAccountMisc(int PKID)
        {
            DebtAccountMisc myentity = new DebtAccountMisc();
            ITable myentityItable = myentity;
            DBO.Provider.Read(PKID.ToString(), ref myentityItable);
            myentity = (DebtAccountMisc)myentityItable;
            AutoMapper.Mapper.CreateMap<DebtAccountMisc, ADDebtAccountMisc>();
            return (AutoMapper.Mapper.Map<ADDebtAccountMisc>(myentity));
        }

        public static DataTable GetDebtAccountMiscList(string AccountId)
        {
            try
            {
                string sql = "Select * from DebtAccountMisc where DebtAccountId  = " + AccountId;

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }

        public static ADDebtAccountMisc ReadMiscInfoByDebtAccountId(string DebtAccountId)
        {
            try
            {
                string mysql = "Select * from DebtAccountMisc where DebtAccountId  = " + DebtAccountId;
                var AddressTableView = DBO.Provider.GetTableView(mysql);
                DataTable dt = AddressTableView.ToTable();
                return ConvertDataTableToClass(dt);
            }
            catch (Exception ex)
            {
                return new ADDebtAccountMisc();
            }
        }

        public static ADDebtAccountMisc ConvertDataTableToClass(DataTable dt)
        {
            try
            {
                if (dt.Rows.Count > 0)
                {


                    List<ADDebtAccountMisc> Misc = (from DataRow dr in dt.Rows
                                                    select new ADDebtAccountMisc()
                                                    {
                                                        PKID = ((dr["PKID"] != DBNull.Value ? Convert.ToInt32(dr["PKID"].ToString()) : 0)),
                                                        DebtAccountId = ((dr["DebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAccountId"].ToString()) : 0)),
                                                        PymntAmt = ((dr["PymntAmt"] != DBNull.Value ? Convert.ToDecimal(dr["PymntAmt"].ToString()) : 0)),
                                                        EscrowNo = ((dr["EscrowNo"] != DBNull.Value ? dr["EscrowNo"].ToString() : "")),
                                                        CheckAmt = ((dr["CheckAmt"] != DBNull.Value ? Convert.ToDecimal(dr["CheckAmt"].ToString()) : 0)),
                                                        EscrowCompany = ((dr["EscrowCompany"] != DBNull.Value ? dr["EscrowCompany"].ToString() : "")),
                                                        CheckNo = ((dr["CheckNo"] != DBNull.Value ? dr["CheckNo"].ToString() : "")),
                                                        EscrowAdd = ((dr["EscrowAdd"] != DBNull.Value ? dr["EscrowAdd"].ToString() : "")),
                                                        Damages = ((dr["Damages"] != DBNull.Value ? Convert.ToDecimal(dr["Damages"].ToString()) : 0)),
                                                        EscrowCity = ((dr["EscrowCity"] != DBNull.Value ? dr["EscrowCity"].ToString() : "")),
                                                        EscrowSt = ((dr["EscrowSt"] != DBNull.Value ? dr["EscrowSt"].ToString() : "")),
                                                        EscrowZip = ((dr["EscrowZip"] != DBNull.Value ? dr["EscrowZip"].ToString() : "")),
                                                        BankName = ((dr["BankName"] != DBNull.Value ? dr["BankName"].ToString() : "")),
                                                        SettlementAmt = ((dr["SettlementAmt"] != DBNull.Value ? Convert.ToDecimal(dr["SettlementAmt"].ToString()) : 0)),
                                                        BKAdd = ((dr["BKAdd"] != DBNull.Value ? dr["BKAdd"].ToString() : "")),
                                                        BKCity = ((dr["BKCity"] != DBNull.Value ? dr["BKCity"].ToString() : "")),
                                                        BKSt = ((dr["BankName"] != DBNull.Value ? dr["BKSt"].ToString() : "")),
                                                        BKZip = ((dr["BKZip"] != DBNull.Value ? dr["BKZip"].ToString() : "")),
                                                        BKCaseNo = ((dr["BKCaseNo"] != DBNull.Value ? dr["BKCaseNo"].ToString() : "")),
                                                        Bank = ((dr["Bank"] != DBNull.Value ? dr["Bank"].ToString() : "")),
                                                        BankAddress = ((dr["BankAddress"] != DBNull.Value ? dr["BankAddress"].ToString() : "")),
                                                        BankCity = ((dr["BankCity"] != DBNull.Value ? dr["BankCity"].ToString() : "")),
                                                        BankState = ((dr["BankState"] != DBNull.Value ? dr["BankState"].ToString() : "")),
                                                        BankZip = ((dr["BankZip"] != DBNull.Value ? dr["BankZip"].ToString() : "")),
                                                        AccountNum = ((dr["AccountNum"] != DBNull.Value ? dr["AccountNum"].ToString() : "")),
                                                        PropertyAddress = ((dr["PropertyAddress"] != DBNull.Value ? dr["PropertyAddress"].ToString() : "")),
                                                        PropertyCity = ((dr["PropertyCity"] != DBNull.Value ? dr["PropertyCity"].ToString() : "")),
                                                        PropertyState = ((dr["PropertyState"] != DBNull.Value ? dr["PropertyState"].ToString() : "")),
                                                        PropertyZip = ((dr["PropertyZip"] != DBNull.Value ? dr["PropertyZip"].ToString() : "")),
                                                        APNNum = ((dr["APNNum"] != DBNull.Value ? dr["APNNum"].ToString() : "")),
                                                        DunHist = ((dr["DunHist"] != DBNull.Value ? dr["DunHist"].ToString() : "")),
                                                        StHist = ((dr["StHist"] != DBNull.Value ? dr["StHist"].ToString() : "")),
                                                        DebtorBKName = ((dr["DebtorBKName"] != DBNull.Value ? dr["DebtorBKName"].ToString() : "")),
                                                        DebtorWebAdd = ((dr["DebtorWebAdd"] != DBNull.Value ? dr["DebtorWebAdd"].ToString() : "")),
                                                        TotDamages = ((dr["TotDamages"] != DBNull.Value ? Convert.ToDecimal(dr["TotDamages"].ToString()) : 0)),
                                                        DebtorFirmName = ((dr["DebtorFirmName"] != DBNull.Value ? dr["DebtorFirmName"].ToString() : "")),
                                                        DebtorAttyName = ((dr["DebtorAttyName"] != DBNull.Value ? dr["DebtorAttyName"].ToString() : "")),
                                                        DebtorAttyAddress = ((dr["DebtorAttyAddress"] != DBNull.Value ? dr["DebtorAttyAddress"].ToString() : "")),
                                                        DebtorAttyCity = ((dr["DebtorAttyCity"] != DBNull.Value ? dr["DebtorAttyCity"].ToString() : "")),
                                                        DebtorAttyState = ((dr["DebtorAttyState"] != DBNull.Value ? dr["DebtorAttyState"].ToString() : "")),
                                                        DebtorAttyZip = ((dr["DebtorAttyZip"] != DBNull.Value ? dr["DebtorAttyZip"].ToString() : "")),
                                                        DebtorAttyPhone = ((dr["DebtorAttyPhone"] != DBNull.Value ? dr["DebtorAttyPhone"].ToString() : "")),
                                                        DebtorAttyEmail = ((dr["DebtorAttyEmail"] != DBNull.Value ? dr["DebtorAttyEmail"].ToString() : "")),
                                                    }).ToList();
                    return Misc.First();
                }
                else
                {
                    return (new ADDebtAccountMisc());
                }
            }
            catch (Exception ex)
            {
                return (new ADDebtAccountMisc());
            }
        }

          public static int SaveDebtAccountMisc(ADDebtAccountMisc objUpdatedADDebtAccountMisc)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADDebtAccountMisc, DebtAccountMisc>();
                ITable tblDebtAccountMisc;
                DebtAccountMisc objDebtAccountMisc;

                if (objUpdatedADDebtAccountMisc.PKID != 0)
                {
                    ADDebtAccountMisc objADDebtAccountMiscOriginal = ReadDebtAccountMisc(Convert.ToInt32(objUpdatedADDebtAccountMisc.PKID));

                    #region Payment/NSF

                    if (objUpdatedADDebtAccountMisc.BankName == "" || objUpdatedADDebtAccountMisc.BankName == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.BankName = objUpdatedADDebtAccountMisc.BankName.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.CheckAmt == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.CheckAmt = objUpdatedADDebtAccountMisc.CheckAmt;
                    }

                    if (objUpdatedADDebtAccountMisc.CheckNo == "" || objUpdatedADDebtAccountMisc.CheckNo == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.CheckNo = objUpdatedADDebtAccountMisc.CheckNo.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.Damages == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.Damages = objUpdatedADDebtAccountMisc.Damages;
                    }

                    if (objUpdatedADDebtAccountMisc.PymntAmt == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.PymntAmt = objUpdatedADDebtAccountMisc.PymntAmt;
                    }

                    if (objUpdatedADDebtAccountMisc.TotDamages == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.TotDamages = objUpdatedADDebtAccountMisc.TotDamages;
                    }

                    if (objUpdatedADDebtAccountMisc.SettlementAmt == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.SettlementAmt = objUpdatedADDebtAccountMisc.SettlementAmt;
                    }

                    #endregion

                    #region Escrow Info

                    if (objUpdatedADDebtAccountMisc.EscrowNo == "" || objUpdatedADDebtAccountMisc.EscrowNo == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.EscrowNo = objUpdatedADDebtAccountMisc.EscrowNo.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.EscrowCompany == "" || objUpdatedADDebtAccountMisc.EscrowCompany == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.EscrowCompany = objUpdatedADDebtAccountMisc.EscrowCompany.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.EscrowAdd == "" || objUpdatedADDebtAccountMisc.EscrowAdd == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.EscrowAdd = objUpdatedADDebtAccountMisc.EscrowAdd.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.EscrowCity == "" || objUpdatedADDebtAccountMisc.EscrowCity == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.EscrowCity = objUpdatedADDebtAccountMisc.EscrowCity.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.EscrowSt == "" || objUpdatedADDebtAccountMisc.EscrowSt == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.EscrowSt = objUpdatedADDebtAccountMisc.EscrowSt.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.EscrowZip == "" || objUpdatedADDebtAccountMisc.EscrowZip == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.EscrowZip = objUpdatedADDebtAccountMisc.EscrowZip.Trim();
                    }

                    #endregion

                    #region Bank Info

                    if (objUpdatedADDebtAccountMisc.Bank == "" || objUpdatedADDebtAccountMisc.Bank == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.Bank = objUpdatedADDebtAccountMisc.Bank.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.BankAddress == "" || objUpdatedADDebtAccountMisc.BankAddress == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.BankAddress = objUpdatedADDebtAccountMisc.BankAddress.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.BankCity == "" || objUpdatedADDebtAccountMisc.BankCity == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.BankCity = objUpdatedADDebtAccountMisc.BankCity.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.BankState == "" || objUpdatedADDebtAccountMisc.BankState == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.BankState = objUpdatedADDebtAccountMisc.BankState.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.BankZip == "" || objUpdatedADDebtAccountMisc.BankZip == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.BankZip = objUpdatedADDebtAccountMisc.BankZip.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.AccountNum == "" || objUpdatedADDebtAccountMisc.AccountNum == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.AccountNum = objUpdatedADDebtAccountMisc.AccountNum.Trim();
                    }
                    #endregion

                    #region Bankruptcy Info

                    if (objUpdatedADDebtAccountMisc.BKCaseNo == "" || objUpdatedADDebtAccountMisc.BKCaseNo == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.BKCaseNo = objUpdatedADDebtAccountMisc.BKCaseNo.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.DebtorBKName == "" || objUpdatedADDebtAccountMisc.DebtorBKName == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.DebtorBKName = objUpdatedADDebtAccountMisc.DebtorBKName.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.BKAdd == "" || objUpdatedADDebtAccountMisc.BKAdd == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.BKAdd = objUpdatedADDebtAccountMisc.BKAdd.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.BKCity == "" || objUpdatedADDebtAccountMisc.BKCity == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.BKCity = objUpdatedADDebtAccountMisc.BKCity.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.BKSt == "" || objUpdatedADDebtAccountMisc.BKSt == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.BKSt = objUpdatedADDebtAccountMisc.BKSt.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.BKZip == "" || objUpdatedADDebtAccountMisc.BKZip == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.BKZip = objUpdatedADDebtAccountMisc.BKZip.Trim();
                    }
                    #endregion

                    #region Debtor Attorney

                    if (objUpdatedADDebtAccountMisc.DebtorFirmName == "" || objUpdatedADDebtAccountMisc.DebtorFirmName == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.DebtorFirmName = objUpdatedADDebtAccountMisc.DebtorFirmName.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.DebtorAttyName == "" || objUpdatedADDebtAccountMisc.DebtorAttyName == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.DebtorAttyName = objUpdatedADDebtAccountMisc.DebtorAttyName.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.DebtorAttyAddress == "" || objUpdatedADDebtAccountMisc.DebtorAttyAddress == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.DebtorAttyAddress = objUpdatedADDebtAccountMisc.DebtorAttyAddress.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.DebtorAttyCity == "" || objUpdatedADDebtAccountMisc.DebtorAttyCity == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.DebtorAttyCity = objUpdatedADDebtAccountMisc.DebtorAttyCity.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.DebtorAttyState == "" || objUpdatedADDebtAccountMisc.DebtorAttyState == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.DebtorAttyState = objUpdatedADDebtAccountMisc.DebtorAttyState.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.DebtorAttyZip == "" || objUpdatedADDebtAccountMisc.DebtorAttyZip == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.DebtorAttyZip = objUpdatedADDebtAccountMisc.DebtorAttyZip.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.DebtorAttyPhone == "" || objUpdatedADDebtAccountMisc.DebtorAttyPhone == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.DebtorAttyPhone = objUpdatedADDebtAccountMisc.DebtorAttyPhone.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.DebtorAttyEmail == "" || objUpdatedADDebtAccountMisc.DebtorAttyEmail == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.DebtorAttyEmail = objUpdatedADDebtAccountMisc.DebtorAttyEmail.Trim();
                    }

                    #endregion

                    #region Property Info

                    if (objUpdatedADDebtAccountMisc.PropertyAddress == "" || objUpdatedADDebtAccountMisc.PropertyAddress == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.PropertyAddress = objUpdatedADDebtAccountMisc.PropertyAddress.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.PropertyCity == "" || objUpdatedADDebtAccountMisc.PropertyCity == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.PropertyCity = objUpdatedADDebtAccountMisc.PropertyCity.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.PropertyState == "" || objUpdatedADDebtAccountMisc.PropertyState == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.PropertyState = objUpdatedADDebtAccountMisc.PropertyState.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.PropertyZip == "" || objUpdatedADDebtAccountMisc.PropertyZip == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.PropertyZip = objUpdatedADDebtAccountMisc.PropertyZip.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.APNNum == "" || objUpdatedADDebtAccountMisc.APNNum == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.APNNum = objUpdatedADDebtAccountMisc.APNNum.Trim();
                    }

                    #endregion

                    #region Misc

                    if (objUpdatedADDebtAccountMisc.DunHist == "" || objUpdatedADDebtAccountMisc.DunHist == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.DunHist = objUpdatedADDebtAccountMisc.DunHist.Trim();
                    }

                    if (objUpdatedADDebtAccountMisc.DebtorWebAdd == "" || objUpdatedADDebtAccountMisc.DebtorWebAdd == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountMiscOriginal.DebtorWebAdd = objUpdatedADDebtAccountMisc.DebtorWebAdd.Trim();
                    }

                    #endregion

                    objDebtAccountMisc = AutoMapper.Mapper.Map<DebtAccountMisc>(objADDebtAccountMiscOriginal);
                    tblDebtAccountMisc = (ITable)objDebtAccountMisc;
                    DBO.Provider.Update(ref tblDebtAccountMisc);
                }
                else
                {
                    objDebtAccountMisc = AutoMapper.Mapper.Map<DebtAccountMisc>(objUpdatedADDebtAccountMisc);
                    tblDebtAccountMisc = (ITable)objDebtAccountMisc;
                    DBO.Provider.Create(ref tblDebtAccountMisc);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }
    }
}
