﻿using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters
{
    public class ADDebt
    {
        #region Properties
        public int DebtId { get; set; }
        public int BatchId { get; set; }
        public string MatchingKey { get; set; }
        public string DebtorName { get; set; }
        public string TaxId { get; set; }
        public string DriversLicense { get; set; }
        public System.DateTime BirthDate { get; set; }
        public bool IsUnwillingToPay { get; set; }
        public bool IsUnableToPay { get; set; }
        public bool HasReturnedChecks { get; set; }
        public string AlphaSort { get; set; }
        public int DebtScore { get; set; }
        public System.DateTime LastUpdateDate { get; set; }
        public int LastUpdateBy { get; set; }
        public int id { get; set; }
        public int BatchDebtAccountId { get; set; }
        #endregion

        #region CRUD
        public static ADDebt ReadDebt(int DebtId)
        {
            Debt myentity = new Debt();
            ITable myentityItable = myentity;
            DBO.Provider.Read(DebtId.ToString(), ref myentityItable);
            myentity = (Debt)myentityItable;
            AutoMapper.Mapper.CreateMap<Debt, ADDebt>();
            return (AutoMapper.Mapper.Map<ADDebt>(myentity));
        }

        public static bool UpdateDebtEditView(ADDebt ObjADDebtUpdated, CurrentUser CuObj)
        {
            try
            {
                ADDebt objADDebtOriginal = ADDebt.ReadDebt(ObjADDebtUpdated.DebtId);
                objADDebtOriginal.DebtorName = ObjADDebtUpdated.DebtorName;
                string TaxId = ObjADDebtUpdated.TaxId;
                if (!(String.IsNullOrEmpty(TaxId)))
                {
                    if (ObjADDebtUpdated.TaxId.IndexOf('-') > 0)
                    {
                        ObjADDebtUpdated.TaxId.Replace("-", "");
                    }
                }               
                objADDebtOriginal.TaxId = ObjADDebtUpdated.TaxId;
                objADDebtOriginal.LastUpdateDate = DateTime.Now;
                objADDebtOriginal.LastUpdateBy = CuObj.Id;

                Debt myentity = new Debt();
                AutoMapper.Mapper.CreateMap<ADDebt, Debt>();
                myentity = AutoMapper.Mapper.Map<Debt>(objADDebtOriginal);
     
                ITable myentityItable = myentity;
                DBO.Provider.Update(ref myentityItable);
                return true;
            }
            catch(Exception ex)
            {
                return false;
            }
        }

        public static int SaveDebt_ChangeDebtorAccountInfo(ADDebt objUpdatedADDebt)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADDebt, Debt>();
                ITable tblDebt;
                Debt objDebt;



                if (objUpdatedADDebt.DebtId != 0)
                {
                    ADDebt objADDebtAccountOriginal = ReadDebt(Convert.ToInt32(objUpdatedADDebt.DebtId));

                    #region Account Action

                    if (objUpdatedADDebt.DebtorName == "" || objUpdatedADDebt.DebtorName == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountOriginal.DebtorName = objUpdatedADDebt.DebtorName;
                    }
                    #endregion

                    objDebt  = AutoMapper.Mapper.Map<Debt>(objADDebtAccountOriginal);
                    tblDebt  = (ITable)objDebt;
                    DBO.Provider.Update(ref tblDebt);
                }
                else
                {
                    objDebt = AutoMapper.Mapper.Map<Debt>(objUpdatedADDebt);
                    tblDebt = (ITable)objDebt;
                    DBO.Provider.Create(ref tblDebt);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        #endregion
    }
}
