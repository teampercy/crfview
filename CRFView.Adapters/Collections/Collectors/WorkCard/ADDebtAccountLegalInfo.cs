﻿using CRF.BLL.CRFDB.SPROCS;
using CRF.BLL.CRFDB.TABLES;
using CRF.BLL.Providers;
using HDS.DAL.Providers;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.Collectors.WorkCard
{
    public class ADDebtAccountLegalInfo
    {
        #region Properties
        public int PKID { get; set; }
        public int DebtAccountId { get; set; }
        public string AttyNO { get; set; }
        public string LawListCode { get; set; }
        public string AttyRef { get; set; }
        public string AttyMemo { get; set; }
        public decimal AttySuitFee { get; set; }
        public string DebtorLegalName { get; set; }
        public string DebtorDBA { get; set; }
        public string PersonalGuarantor1 { get; set; }
        public string PersonalGuarantor2 { get; set; }
        public string ServiceAddress { get; set; }
        public string ServiceCity { get; set; }
        public string ServiceState { get; set; }
        public string ServiceZip { get; set; }
        public string ServiceAddress2 { get; set; }
        public string ServiceCity2 { get; set; }
        public string ServiceState2 { get; set; }
        public string ServiceZip2 { get; set; }
        public string AccountSummary { get; set; }
        public string DebtDescription { get; set; }
        public string PresAgName { get; set; }
        public bool CorpBox { get; set; }
        public bool PartnershipBox { get; set; }
        public bool SolePropBox { get; set; }
        public bool LLCBox { get; set; }
        public bool OtherBox { get; set; }
        public System.DateTime SuitFiledDate { get; set; }
        public int AttyId { get; set; }
        public int LawListId { get; set; }
        public decimal CourtCostAmt { get; set; }
        public decimal SuitFeeAmt { get; set; }
        public decimal AttyRate { get; set; }
        public decimal LegalRate { get; set; }
        public decimal LegalAssignAmt { get; set; }
        public System.DateTime LegalSetupDate { get; set; }
        public System.DateTime FwdJudgmentDate { get; set; }
        public string CourtMemo { get; set; }
        public System.DateTime FwdCourtDate { get; set; }
        public decimal TotalCosts { get; set; }
        #endregion

        #region CRUD

        public static ADDebtAccountLegalInfo GetDebtAccountLegalInfo(DataTable dt)
        {
            try
            {
                List<ADDebtAccountLegalInfo> LegalInfo = (from DataRow dr in dt.Rows
                                                select new ADDebtAccountLegalInfo()
                                                {
                                                    PKID = ((dr["PKID"] != DBNull.Value ? Convert.ToInt32(dr["PKID"].ToString()) : 0)),
                                                    DebtAccountId = ((dr["DebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAccountId"].ToString()) : 0)),
                                                    AttyNO = ((dr["AttyNO"] != DBNull.Value ? dr["AttyNO"].ToString() : "")),
                                                    LawListCode = ((dr["LawListCode"] != DBNull.Value ? dr["LawListCode"].ToString() : "")),
                                                    AttyRef = ((dr["AttyRef"] != DBNull.Value ? dr["AttyRef"].ToString() : "")),
                                                    AttyMemo = ((dr["AttyMemo"] != DBNull.Value ? dr["AttyMemo"].ToString() : "")),
                                                    AttySuitFee = ((dr["AttySuitFee"] != DBNull.Value ? Convert.ToDecimal(dr["AttySuitFee"].ToString()) : 0)),
                                                    DebtorLegalName = ((dr["DebtorLegalName"] != DBNull.Value ? dr["DebtorLegalName"].ToString() : "")),
                                                    DebtorDBA = ((dr["DebtorDBA"] != DBNull.Value ? dr["DebtorDBA"].ToString() : "")),
                                                    PersonalGuarantor1 = ((dr["PersonalGuarantor1"] != DBNull.Value ? dr["PersonalGuarantor1"].ToString() : "")),
                                                    PersonalGuarantor2 = ((dr["PersonalGuarantor2"] != DBNull.Value ? dr["PersonalGuarantor2"].ToString() : "")),
                                                    ServiceAddress = ((dr["ServiceAddress"] != DBNull.Value ? dr["ServiceAddress"].ToString() : "")),
                                                    ServiceCity = ((dr["ServiceCity"] != DBNull.Value ? dr["ServiceCity"].ToString() : "")),
                                                    ServiceState = ((dr["ServiceState"] != DBNull.Value ? dr["ServiceState"].ToString() : "")),
                                                    ServiceZip = ((dr["ServiceZip"] != DBNull.Value ? dr["ServiceZip"].ToString() : "")),
                                                    ServiceAddress2 = ((dr["ServiceAddress2"] != DBNull.Value ? dr["ServiceAddress2"].ToString() : "")),
                                                    ServiceCity2 = ((dr["ServiceCity2"] != DBNull.Value ? dr["ServiceCity2"].ToString() : "")),
                                                    ServiceState2 = ((dr["ServiceState2"] != DBNull.Value ? dr["ServiceState2"].ToString() : "")),
                                                    ServiceZip2 = ((dr["ServiceZip2"] != DBNull.Value ? dr["ServiceZip2"].ToString() : "")),
                                                    AccountSummary = ((dr["AccountSummary"] != DBNull.Value ? dr["AccountSummary"].ToString() : "")),
                                                    DebtDescription = ((dr["DebtDescription"] != DBNull.Value ? dr["DebtDescription"].ToString() : "")),
                                                    PresAgName = ((dr["PresAgName"] != DBNull.Value ? dr["PresAgName"].ToString() : "")),
                                                    CorpBox = ((dr["CorpBox"] != DBNull.Value ? Convert.ToBoolean(dr["CorpBox"]) : false)),
                                                    PartnershipBox = ((dr["PartnershipBox"] != DBNull.Value ? Convert.ToBoolean(dr["PartnershipBox"]) : false)),
                                                    SolePropBox = ((dr["SolePropBox"] != DBNull.Value ? Convert.ToBoolean(dr["SolePropBox"]) : false)),
                                                    LLCBox = ((dr["LLCBox"] != DBNull.Value ? Convert.ToBoolean(dr["LLCBox"]) : false)),
                                                    OtherBox = ((dr["OtherBox"] != DBNull.Value ? Convert.ToBoolean(dr["OtherBox"]) : false)),
                                                    SuitFiledDate = ((dr["SuitFiledDate"] != DBNull.Value ? Convert.ToDateTime(dr["SuitFiledDate"]) : DateTime.MinValue)),
                                                    AttyId = ((dr["AttyId"] != DBNull.Value ? Convert.ToInt32(dr["AttyId"].ToString()) : 0)),
                                                    LawListId = ((dr["LawListId"] != DBNull.Value ? Convert.ToInt32(dr["LawListId"].ToString()) : 0)),
                                                    CourtCostAmt = ((dr["CourtCostAmt"] != DBNull.Value ? Convert.ToDecimal(dr["CourtCostAmt"].ToString()) : 0)),
                                                    SuitFeeAmt = ((dr["SuitFeeAmt"] != DBNull.Value ? Convert.ToDecimal(dr["SuitFeeAmt"].ToString()) : 0)),
                                                    AttyRate = ((dr["AttyRate"] != DBNull.Value ? Convert.ToDecimal(dr["AttyRate"].ToString()) : 0)),
                                                    LegalRate = ((dr["LegalRate"] != DBNull.Value ? Convert.ToDecimal(dr["LegalRate"].ToString()) : 0)),
                                                    LegalAssignAmt = ((dr["LegalAssignAmt"] != DBNull.Value ? Convert.ToDecimal(dr["LegalAssignAmt"].ToString()) : 0)),
                                                    LegalSetupDate = ((dr["LegalSetupDate"] != DBNull.Value ? Convert.ToDateTime(dr["LegalSetupDate"]) : DateTime.MinValue)),
                                                    FwdJudgmentDate = ((dr["FwdJudgmentDate"] != DBNull.Value ? Convert.ToDateTime(dr["FwdJudgmentDate"]) : DateTime.MinValue)),
                                                    CourtMemo = ((dr["CourtMemo"] != DBNull.Value ? dr["CourtMemo"].ToString() : "")),
                                                    FwdCourtDate = ((dr["FwdCourtDate"] != DBNull.Value ? Convert.ToDateTime(dr["FwdCourtDate"]) : DateTime.MinValue)),
                                                    TotalCosts = ((dr["CourtCostAmt"] != DBNull.Value ? Convert.ToDecimal(dr["CourtCostAmt"].ToString()) : 0)) + ((dr["SuitFeeAmt"] != DBNull.Value ? Convert.ToDecimal(dr["SuitFeeAmt"].ToString()) : 0)),
                                                }).ToList();
                return LegalInfo.First();
            }
            catch (Exception ex)
            {
                return (new ADDebtAccountLegalInfo());
            }
        }

        public static DataTable GetDebtAccountLegalInfoList(string AccountId)
        {
            try
            {
                string sql = "Select * from DebtAccountLegalInfo where DebtAccountId  = " + AccountId;

                //DBO.GetTableView 
                var EntityList = DBO.Provider.GetTableView(sql);
                DataTable dt = EntityList.ToTable();
                return (dt);
            }
            catch (Exception ex)
            {
                return (new DataTable());
            }
        }
        //Read Details from database
        public static ADDebtAccountLegalInfo ReadDebtAccountLegalInfo(int pkid)
        {
            DebtAccountLegalInfo myentity = new DebtAccountLegalInfo();
            ITable myentityItable = myentity;
            DBO.Provider.Read(pkid.ToString(), ref myentityItable);
            myentity = (DebtAccountLegalInfo)myentityItable;
            AutoMapper.Mapper.CreateMap<DebtAccountLegalInfo, ADDebtAccountLegalInfo>();
            return (AutoMapper.Mapper.Map<ADDebtAccountLegalInfo>(myentity));
        }

        public static ADDebtAccountLegalInfo ReadLegalInfoByDebtAccountId(string DebtAccountId)
        {
            try
            { 
                string mysql = "Select * from DebtAccountLegalInfo where DebtAccountId  = " + DebtAccountId;
                var AddressTableView = DBO.Provider.GetTableView(mysql);
                DataTable dt = AddressTableView.ToTable();
                return ConvertDataTableToClass(dt);
            }
            catch (Exception ex)
            {
                return new ADDebtAccountLegalInfo();
            }
        }

        public static int SaveDebtAccountLegalInfo(ADDebtAccountLegalInfo objUpdatedADDebtAccountLegalInfo)
        {
            try
            {
                AutoMapper.Mapper.CreateMap<ADDebtAccountLegalInfo, DebtAccountLegalInfo>();
                ITable tblDebtAccountLegalInfo;
                DebtAccountLegalInfo objDebtAccountLegalInfo;

                if (objUpdatedADDebtAccountLegalInfo.PKID != 0)
                {
                    ADDebtAccountLegalInfo objADDebtAccountLegalInfoOriginal = ReadDebtAccountLegalInfo(Convert.ToInt32(objUpdatedADDebtAccountLegalInfo.PKID));

                    #region Attorney Info
                    if (objUpdatedADDebtAccountLegalInfo.AttyId == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalInfoOriginal.AttyId = objUpdatedADDebtAccountLegalInfo.AttyId;
                    }

                    if (objUpdatedADDebtAccountLegalInfo.AttyNO == "" || objUpdatedADDebtAccountLegalInfo.AttyNO == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalInfoOriginal.AttyNO = objUpdatedADDebtAccountLegalInfo.AttyNO.Trim();
                    }

                    if (objUpdatedADDebtAccountLegalInfo.AttyMemo == "" || objUpdatedADDebtAccountLegalInfo.AttyMemo == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalInfoOriginal.AttyMemo = objUpdatedADDebtAccountLegalInfo.AttyMemo.Trim();
                    }

                    if (objUpdatedADDebtAccountLegalInfo.AttyMemo == "" || objUpdatedADDebtAccountLegalInfo.AttyMemo == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalInfoOriginal.AttyMemo = objUpdatedADDebtAccountLegalInfo.AttyMemo.Trim();
                    }
                    #endregion

                    #region Letter Merge Info

                    if (objUpdatedADDebtAccountLegalInfo.CourtCostAmt == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalInfoOriginal.CourtCostAmt = objUpdatedADDebtAccountLegalInfo.CourtCostAmt;
                    }

                    if (objUpdatedADDebtAccountLegalInfo.SuitFeeAmt == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalInfoOriginal.SuitFeeAmt = objUpdatedADDebtAccountLegalInfo.SuitFeeAmt;
                    }

                    #endregion

                    #region Law List Info

                    if (objUpdatedADDebtAccountLegalInfo.LawListId == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalInfoOriginal.LawListId = objUpdatedADDebtAccountLegalInfo.LawListId;
                    }

                    if (objUpdatedADDebtAccountLegalInfo.LawListCode == "" || objUpdatedADDebtAccountLegalInfo.LawListCode == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalInfoOriginal.LawListCode = objUpdatedADDebtAccountLegalInfo.LawListCode.Trim();
                    }

                    #endregion

                    #region Fowarding Info

                    if (objUpdatedADDebtAccountLegalInfo.LegalSetupDate  == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalInfoOriginal.LegalSetupDate = objUpdatedADDebtAccountLegalInfo.LegalSetupDate;
                    }

                    if (objUpdatedADDebtAccountLegalInfo.LegalRate == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalInfoOriginal.LegalRate = objUpdatedADDebtAccountLegalInfo.LegalRate;
                    }

                    if (objUpdatedADDebtAccountLegalInfo.LegalAssignAmt == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalInfoOriginal.LegalAssignAmt = objUpdatedADDebtAccountLegalInfo.LegalAssignAmt;
                    }

                    if (objUpdatedADDebtAccountLegalInfo.AttyRef == "" || objUpdatedADDebtAccountLegalInfo.AttyRef == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalInfoOriginal.AttyRef = objUpdatedADDebtAccountLegalInfo.AttyRef.Trim();
                    }

                    if (objUpdatedADDebtAccountLegalInfo.SuitFiledDate == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalInfoOriginal.SuitFiledDate = objUpdatedADDebtAccountLegalInfo.SuitFiledDate;
                    }

                    if (objUpdatedADDebtAccountLegalInfo.AttyRate == 0)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalInfoOriginal.AttyRate = objUpdatedADDebtAccountLegalInfo.AttyRate;
                    }

                   if (objUpdatedADDebtAccountLegalInfo.AttyMemo == "" || objUpdatedADDebtAccountLegalInfo.AttyMemo == null)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalInfoOriginal.AttyMemo = objUpdatedADDebtAccountLegalInfo.AttyMemo.Trim();
                    }
                    
                    if (objUpdatedADDebtAccountLegalInfo.FwdJudgmentDate == DateTime.MinValue)
                    {
                    }
                    else
                    {
                        objADDebtAccountLegalInfoOriginal.FwdJudgmentDate = objUpdatedADDebtAccountLegalInfo.FwdJudgmentDate;
                    }
                    #endregion

                    objDebtAccountLegalInfo = AutoMapper.Mapper.Map<DebtAccountLegalInfo>(objADDebtAccountLegalInfoOriginal);
                    tblDebtAccountLegalInfo = (ITable)objDebtAccountLegalInfo;
                    DBO.Provider.Update(ref tblDebtAccountLegalInfo);
                }
                else
                {
                    objDebtAccountLegalInfo = AutoMapper.Mapper.Map<DebtAccountLegalInfo>(objUpdatedADDebtAccountLegalInfo);
                    tblDebtAccountLegalInfo = (ITable)objDebtAccountLegalInfo;
                    DBO.Provider.Create(ref tblDebtAccountLegalInfo);

                    //Record New Fwd ActionHistory
                    CRF.BLL.Users.CurrentUser user = AuthenticationUtility.GetUser();
                 
                    var MYSPROC = new cview_Accounts_CreateActionHistory();
                    MYSPROC.DebtAccountId = objUpdatedADDebtAccountLegalInfo.DebtAccountId;
                    MYSPROC.ActionId = 6;
                    MYSPROC.UserId = user.Id;
                    MYSPROC.UserName = user.UserName;
                    DBO.Provider.ExecNonQuery(MYSPROC);
                }

                return 1;
            }
            catch (Exception ex)
            {
                ErrorLog.SendErrorToText(ex);
                return 0;
            }
        }

        public static ADDebtAccountLegalInfo ConvertDataTableToClass(DataTable dt)
        {
            try
            {
                if (dt.Rows.Count > 0)
                {
                    List<ADDebtAccountLegalInfo> LegalInfo = (from DataRow dr in dt.Rows
                                                              select new ADDebtAccountLegalInfo()
                                                              {
                                                                  PKID = ((dr["PKID"] != DBNull.Value ? Convert.ToInt32(dr["PKID"].ToString()) : 0)),
                                                                  DebtAccountId = ((dr["DebtAccountId"] != DBNull.Value ? Convert.ToInt32(dr["DebtAccountId"].ToString()) : 0)),
                                                                  AttyNO = ((dr["AttyNO"] != DBNull.Value ? dr["AttyNO"].ToString() : "")),
                                                                  LawListCode = ((dr["LawListCode"] != DBNull.Value ? dr["LawListCode"].ToString() : "")),
                                                                  AttyRef = ((dr["AttyRef"] != DBNull.Value ? dr["AttyRef"].ToString() : "")),
                                                                  AttyMemo = ((dr["AttyMemo"] != DBNull.Value ? dr["AttyMemo"].ToString() : "")),
                                                                  AttySuitFee = ((dr["AttySuitFee"] != DBNull.Value ? Convert.ToDecimal(dr["AttySuitFee"].ToString()) : 0)),
                                                                  DebtorLegalName = ((dr["DebtorLegalName"] != DBNull.Value ? dr["DebtorLegalName"].ToString() : "")),
                                                                  DebtorDBA = ((dr["DebtorDBA"] != DBNull.Value ? dr["DebtorDBA"].ToString() : "")),
                                                                  PersonalGuarantor1 = ((dr["PersonalGuarantor1"] != DBNull.Value ? dr["PersonalGuarantor1"].ToString() : "")),
                                                                  PersonalGuarantor2 = ((dr["PersonalGuarantor2"] != DBNull.Value ? dr["PersonalGuarantor2"].ToString() : "")),
                                                                  ServiceAddress = ((dr["ServiceAddress"] != DBNull.Value ? dr["ServiceAddress"].ToString() : "")),
                                                                  ServiceCity = ((dr["ServiceCity"] != DBNull.Value ? dr["ServiceCity"].ToString() : "")),
                                                                  ServiceState = ((dr["ServiceState"] != DBNull.Value ? dr["ServiceState"].ToString() : "")),
                                                                  ServiceZip = ((dr["ServiceZip"] != DBNull.Value ? dr["ServiceZip"].ToString() : "")),
                                                                  ServiceAddress2 = ((dr["ServiceAddress2"] != DBNull.Value ? dr["ServiceAddress2"].ToString() : "")),
                                                                  ServiceCity2 = ((dr["ServiceCity2"] != DBNull.Value ? dr["ServiceCity2"].ToString() : "")),
                                                                  ServiceState2 = ((dr["ServiceState2"] != DBNull.Value ? dr["ServiceState2"].ToString() : "")),
                                                                  ServiceZip2 = ((dr["ServiceZip2"] != DBNull.Value ? dr["ServiceZip2"].ToString() : "")),
                                                                  AccountSummary = ((dr["AccountSummary"] != DBNull.Value ? dr["AccountSummary"].ToString() : "")),
                                                                  DebtDescription = ((dr["DebtDescription"] != DBNull.Value ? dr["DebtDescription"].ToString() : "")),
                                                                  PresAgName = ((dr["PresAgName"] != DBNull.Value ? dr["PresAgName"].ToString() : "")),
                                                                  CorpBox = ((dr["CorpBox"] != DBNull.Value ? Convert.ToBoolean(dr["CorpBox"]) : false)),
                                                                  PartnershipBox = ((dr["PartnershipBox"] != DBNull.Value ? Convert.ToBoolean(dr["PartnershipBox"]) : false)),
                                                                  SolePropBox = ((dr["SolePropBox"] != DBNull.Value ? Convert.ToBoolean(dr["SolePropBox"]) : false)),
                                                                  LLCBox = ((dr["LLCBox"] != DBNull.Value ? Convert.ToBoolean(dr["LLCBox"]) : false)),
                                                                  OtherBox = ((dr["OtherBox"] != DBNull.Value ? Convert.ToBoolean(dr["OtherBox"]) : false)),
                                                                  SuitFiledDate = ((dr["SuitFiledDate"] != DBNull.Value ? Convert.ToDateTime(dr["SuitFiledDate"]) : DateTime.MinValue)),
                                                                  AttyId = ((dr["AttyId"] != DBNull.Value ? Convert.ToInt32(dr["AttyId"].ToString()) : 0)),
                                                                  LawListId = ((dr["LawListId"] != DBNull.Value ? Convert.ToInt32(dr["LawListId"].ToString()) : 0)),
                                                                  CourtCostAmt = ((dr["CourtCostAmt"] != DBNull.Value ? Convert.ToDecimal(dr["CourtCostAmt"].ToString()) : 0)),
                                                                  SuitFeeAmt = ((dr["SuitFeeAmt"] != DBNull.Value ? Convert.ToDecimal(dr["SuitFeeAmt"].ToString()) : 0)),
                                                                  AttyRate = ((dr["AttyRate"] != DBNull.Value ? Convert.ToDecimal(dr["AttyRate"].ToString()) : 0)),
                                                                  LegalRate = ((dr["LegalRate"] != DBNull.Value ? Convert.ToDecimal(dr["LegalRate"].ToString()) : 0)),
                                                                  LegalAssignAmt = ((dr["LegalAssignAmt"] != DBNull.Value ? Convert.ToDecimal(dr["LegalAssignAmt"].ToString()) : 0)),
                                                                  LegalSetupDate = ((dr["LegalSetupDate"] != DBNull.Value ? Convert.ToDateTime(dr["LegalSetupDate"]) : DateTime.MinValue)),
                                                                  FwdJudgmentDate = ((dr["FwdJudgmentDate"] != DBNull.Value ? Convert.ToDateTime(dr["FwdJudgmentDate"]) : DateTime.MinValue)),
                                                                  CourtMemo = ((dr["CourtMemo"] != DBNull.Value ? dr["CourtMemo"].ToString() : "")),
                                                                  FwdCourtDate = ((dr["FwdCourtDate"] != DBNull.Value ? Convert.ToDateTime(dr["FwdCourtDate"]) : DateTime.MinValue)),
                                                              }).ToList();
                    return LegalInfo.First();
                }
                else
                {
                    return (new ADDebtAccountLegalInfo());
                }
            }
            catch (Exception ex)
            {
                return (new ADDebtAccountLegalInfo());
            }
        }
        #endregion
    }
}
