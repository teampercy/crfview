﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace CRFView.Adapters.Collections.DayEnd.Acks.ViewModels
{
    public class AcksVM
    {
        public AcksVM()
        {
            ClientIdList = CommonBindList.GetServiceCodes();
        }

        public string RbtClientFilter { get; set; }
        public string RbtPrintOption { get; set; }

        public DateTime FromDate { get; set; }
        public DateTime ThruDate { get; set; }


        public string ddlClientId { get; set; }
        public IEnumerable<SelectListItem> ClientIdList { get; set; }

        public string ClientId { get; set; }
    }
}
