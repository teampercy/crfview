﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CRFView.Adapters.Collections.DayEnd.MatchReport.ViewModels
{
    public class MatchReportVM
    {
        public MatchReportVM()
        {
            
        }
 
        public string RbtPrintOption { get; set; }

        public DateTime FromDate { get; set; }
        public DateTime ThruDate { get; set; }
  
    }
}
