﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CRFView.Adapters;

namespace CRFView.Controllers
{
    public class ServiceTypeController : Controller
    {
        ADServiceType objADServiceType = new ADServiceType();
        // GET: ADServiceType
        public ActionResult Index()
        {
            List<ADServiceType> lstProduct = objADServiceType.ListProduct();
            Session["ListProduct"] = lstProduct;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstProduct);
        }
        //Show details on Edit
        public ActionResult ProductDetails(int id)
        {
            ADServiceType objProduct = objADServiceType.ReadProduct(id);
            return View(GetVirtualPath("ProductDetails"), objProduct);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult ProductSave(ADServiceType updatedProduct)
        {
            objADServiceType.SaveProduct(updatedProduct);
            List<ADServiceType> lstProduct =  objADServiceType.ListProduct();
            Session["ListProduct"] = lstProduct;
            return PartialView(GetVirtualPath("_ProductList"), lstProduct);
        }
        //Show blank details on Add
        public ActionResult ProductAdd()
        {
            ADServiceType objProduct = new ADServiceType();
            ModelState.Clear();
            objProduct.ID = 0;
            return View(GetVirtualPath("ProductDetails"), objProduct);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value,string Key)
        {
            List<ADServiceType> ListProductFilter = new List<ADServiceType>();
            List<ADServiceType> ListProduct = (List<ADServiceType>)Session["ListProduct"];
            if (value != "" || value != null)
            {
                if(Key=="1")
                {
                    ListProductFilter = (from item in ListProduct
                                         where item.ProdCode.ToLower().Contains(value.ToLower())
                                         select item).ToList();
                }
                else if (Key == "1")
                {
                    ListProductFilter = (from item in ListProduct
                                         where item.ProdName.ToLower().Contains(value.ToLower())
                                         select item).ToList();
                }
            }
            return View(GetVirtualPath("_ProductList"), ListProductFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Product Code" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Product Name" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }


        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADServiceType> lstProduct = objADServiceType.ListProduct();
            Session["ListProduct"] = lstProduct;
            return PartialView(GetVirtualPath("_ProductList"), lstProduct);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Company/ServiceType/" + ViewName + ".cshtml";
            return path;
        }
    }
}