﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CRFView.Adapters;

namespace CRFView.Controllers
{
    public class ClientReportsLogController : Controller
    {
        ADClientReportsLog objClientReportsLog = new ADClientReportsLog();
        // GET: SystemValues
        public ActionResult Index()
        {
            List<ADClientReportsLog> lstClientReportsLog = new List<ADClientReportsLog>();
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstClientReportsLog);
        }
      
        [HttpPost]
        public ActionResult Search(string SearchVal, string SearchKey, string ReadioValue, string date)
        {

            List<ADClientReportsLog> lstClientReportsLog = objClientReportsLog.ListClientReportLog(ReadioValue, date);
            List<ADClientReportsLog> ListClientReportsLogFilter = lstClientReportsLog;

            if (SearchVal != "" || SearchVal != null)
            {
                if(SearchKey=="1")
                {
                    ListClientReportsLogFilter = (from item in lstClientReportsLog
                                                  where item.taskname.ToLower().Contains(SearchVal.ToLower())
                                                  select item).ToList();
                }
                else if (SearchKey == "2")
                {
                    ListClientReportsLogFilter = (from item in lstClientReportsLog
                                                  where  item.batchid.ToLower().Contains(SearchVal.ToLower())
                                                  select item).ToList();
                }
                else if (SearchKey == "3")
                {
                    ListClientReportsLogFilter = (from item in lstClientReportsLog
                                                  where item.clientcode.ToLower().Contains(SearchVal.ToLower())
                                                  select item).ToList();
                }
            }
            return View(GetVirtualPath("_ClientReportsLogList"), ListClientReportsLogFilter);
        }

        //set FillterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Task Name" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Batch Id" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "Client Code" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Company/ClientReportsLog/" + ViewName + ".cshtml";
            return path;
        }
    }
}