﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class SchedulerReportController : Controller
    {
        ADSchedulerReport objSchedulerReport = new ADSchedulerReport();
        // GET: SystemValues
        public ActionResult Index()
        {
            List<ADSchedulerReport> lstSchedulerReport = objSchedulerReport.ListSchedulerReport();
            Session["ListSchedulerReport"] = lstSchedulerReport;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstSchedulerReport);
        }
        //Show details on Edit
        public ActionResult SchedulerReportDetails(int id)
        {
            ADSchedulerReport objReadSchedulerReport = objSchedulerReport.ReadSchedulerReport(id);
            return View(GetVirtualPath("SchedulerReportDetails"), objReadSchedulerReport);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult SchedulerReportSave(ADSchedulerReport updatedSchedulerReport)
        {
            objSchedulerReport.SaveSchedulerReport(updatedSchedulerReport);
            List<ADSchedulerReport> lstSchedulerReport = objSchedulerReport.ListSchedulerReport();
            Session["ListSchedulerReport"] = lstSchedulerReport;
            return PartialView(GetVirtualPath("_SchedulerReportList"), lstSchedulerReport);
        }
        //Show blank details on Add
        public ActionResult SchedulerReportAdd()
        {
            ADSchedulerReport objSchedulerReport = new ADSchedulerReport();
            ModelState.Clear();
            objSchedulerReport.PKID = 0;
            return View(GetVirtualPath("SchedulerReportDetails"), objSchedulerReport);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {
            List<ADSchedulerReport> ListSchedulerReportFilter = new List<ADSchedulerReport>();
            List<ADSchedulerReport> ListSchedulerReport= (List<ADSchedulerReport>)Session["ListSchedulerReport"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {

                    ListSchedulerReportFilter = (from item in ListSchedulerReport
                                                 where item.taskname.ToLower().Contains(value.ToLower())
                                                 select item).ToList();
                }
                else if (Key == "2")
                {
                    ListSchedulerReportFilter = (from item in ListSchedulerReport
                                                 where item.taskalias.ToLower().Contains(value.ToLower())
                                                 select item).ToList();
                }
                else if (Key == "3")
                {

                    ListSchedulerReportFilter = (from item in ListSchedulerReport
                                                 where item.description.ToLower().Contains(value.ToLower())
                                                 select item).ToList();
                }

            }
            return View(GetVirtualPath("_SchedulerReportList"), ListSchedulerReportFilter);
        }
        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Task Name" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Task Alias" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "Description" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }
        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADSchedulerReport> lstSchedulerReport = objSchedulerReport.ListSchedulerReport();
            Session["ListSchedulerReport"] = lstSchedulerReport;
            return PartialView(GetVirtualPath("_SchedulerReportList"), lstSchedulerReport);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Company/SchedulerReport/" + ViewName + ".cshtml";
            return path;
        }

    }
}