﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class ClientEDIInterfaceController : Controller
    {
        ADClientEDIInterface objClientEDIInterface = new ADClientEDIInterface();

        // GET: ClientEDIInterface
        public ActionResult Index()
        {
            List<ADClientEDIInterface> ClientEDIInterfaceList = objClientEDIInterface.ListClientEDIInterface();
            Session["ListClientEDIInterface"] = ClientEDIInterfaceList;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"), ClientEDIInterfaceList);

        }
        //Show details on Edit
        public ActionResult ClientEDIInterfaceDetails(int id)
        {
            objClientEDIInterface = new ADClientEDIInterface();
            objClientEDIInterface = objClientEDIInterface.ReadClientEDIInterface(id);
            objClientEDIInterface.ClientList = CommonBindList.GetClientList();
            objClientEDIInterface.ClientContractList = CommonBindList.GetClientContract(objClientEDIInterface.ClientId);
            return View(GetVirtualPath("ClientEDIInterfaceDetails"), objClientEDIInterface);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult ClientEDIInterfaceSave(ADClientEDIInterface updatedClientEDIInterface)
        {
            if (updatedClientEDIInterface.InterfaceType != "C")
            {
                updatedClientEDIInterface.ContractId = 0;
            }
            objClientEDIInterface.SaveClientEDIInterface(updatedClientEDIInterface);

            List<ADClientEDIInterface> ListClientEDIInterface = objClientEDIInterface.ListClientEDIInterface();
            Session["ListClientEDIInterface"] = ListClientEDIInterface;
            //SetFilterList();
            return PartialView(GetVirtualPath("_ClientEDIInterfaceList"), ListClientEDIInterface);
        }
        //Show blank details on Add
        public ActionResult ClientEDIInterfaceAdd()
        {
            objClientEDIInterface = new ADClientEDIInterface();
            objClientEDIInterface.Id = 0;
            objClientEDIInterface.ClientList = CommonBindList.GetClientList();
            objClientEDIInterface.ClientContractList = CommonBindList.GetClientContract(0);
            return View(GetVirtualPath("ClientEDIInterfaceDetails"), objClientEDIInterface);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {

            List<ADClientEDIInterface> ClientEDIInterfaceList = (List<ADClientEDIInterface>)Session["ListClientEDIInterface"];
            List<ADClientEDIInterface> ClientEDIInterfaceFilterList = null;
            if (value != "" || value != null)
            {
                switch (Key)
                {
                    case "1":
                        ClientEDIInterfaceFilterList = (from item in ClientEDIInterfaceList
                                                        where item.ClientCode.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    case "2":

                        ClientEDIInterfaceFilterList = (from item in ClientEDIInterfaceList
                                                        where item.InterfaceType.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    case "3":

                        ClientEDIInterfaceFilterList = (from item in ClientEDIInterfaceList
                                                        where item.InterfaceName.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    case "4":

                        ClientEDIInterfaceFilterList = (from item in ClientEDIInterfaceList
                                                        where item.InterfaceDescr.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    default:
                        ClientEDIInterfaceFilterList = null;
                        break;

                }

            }
            return View(GetVirtualPath("_ClientEDIInterfaceList"), ClientEDIInterfaceFilterList);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Client Code" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Interface Type" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "Interface Name" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "Interface Description" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult bindContractListByClientId(int ClientId)
        {
            objClientEDIInterface.ClientContractList = CommonBindList.GetClientContract(ClientId);
            return Json(objClientEDIInterface.ClientContractList, JsonRequestBehavior.AllowGet);

        }
        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADClientEDIInterface> ListClientEDIInterface = objClientEDIInterface.ListClientEDIInterface();
            Session["ListClientEDIInterface"] = ListClientEDIInterface;
            return PartialView(GetVirtualPath("_ClientEDIInterfaceList"), ListClientEDIInterface);
        }

        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Company/ClientEDIInterface/" + ViewName + ".cshtml";
            return path;
        }
    }
}