﻿using System.Web.Mvc;
using CRFView.Adapters;

namespace CRFView.Controllers
{
    public class SchedulerSettingsController : Controller
    {
        ADSchedulerSettings objADSchedulerSettings = new ADSchedulerSettings();
        // GET: ADSchedulerSettings
        public ActionResult Index()
        {
            ADSchedulerSettings objReadSchedulerSettings = objADSchedulerSettings.ReadSchedulerSettings(1);
            return PartialView(GetVirtualPath("Index"), objReadSchedulerSettings);
        }
       
        //Save Updated or New Details
        [HttpPost]
        public ActionResult SchedulerSettingsSave(ADSchedulerSettings updatedADSchedulerSettings)
        {
            objADSchedulerSettings.SaveSchedulerSettings(updatedADSchedulerSettings);
            ADSchedulerSettings objReadSchedulerSettings = objADSchedulerSettings.ReadSchedulerSettings(1);
            return PartialView(GetVirtualPath("Index"), objReadSchedulerSettings);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Company/SchedulerSettings/" + ViewName + ".cshtml";
            return path;
        }
    }
}