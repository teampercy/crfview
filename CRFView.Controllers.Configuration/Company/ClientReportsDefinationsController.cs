﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class ClientReportsDefinationsController : Controller
    {
        ADClientReportsDefinations objClientReportsDefinations = new ADClientReportsDefinations();
        // GET: ClientReportsDefinations
        public ActionResult Index()
        {
            List<ADClientReportsDefinations> lstClientReportsDefinations = objClientReportsDefinations.ListClientReportsDefinations();
            Session["ListClientReportsDefinations"] = lstClientReportsDefinations;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstClientReportsDefinations);
        }
        //Show details on Edit
        public ActionResult ClientReportsDefinationsDetails(int id)
        {
            ADClientReportsDefinations objClientReportsDefinationsdetails =  (ADClientReportsDefinations)objClientReportsDefinations.ReadClientReportsDefinations(id);
            return View(GetVirtualPath("ClientReportsDefinationsDetails"), objClientReportsDefinationsdetails);
        }
        //Save Updated or New Details
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ClientReportsDefinationsSave(ADClientReportsDefinations updatedClientReportsDefinations)
        {
            objClientReportsDefinations.SaveClientReportsDefinations(updatedClientReportsDefinations);
            List<ADClientReportsDefinations> lstClientReportsDefinations = objClientReportsDefinations.ListClientReportsDefinations();
            Session["ListClientReportsDefinations"] = lstClientReportsDefinations;
            return PartialView(GetVirtualPath("_ClientReportsDefinationsList"), lstClientReportsDefinations);
        }
        //Show blank details on Add
        public ActionResult ClientReportsDefinationsAdd()
        {
            ADClientReportsDefinations objClientReportsDefinations = new ADClientReportsDefinations();
            ModelState.Clear();
            objClientReportsDefinations.PKID = 0;
            return View(GetVirtualPath("ClientReportsDefinationsDetails"), objClientReportsDefinations);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value ,string Key)
        {
            List<ADClientReportsDefinations> ListClientReportsDefinationsFilter = new List<ADClientReportsDefinations>();
            List<ADClientReportsDefinations> ListClientReportsDefinations= (List<ADClientReportsDefinations>)Session["ListClientReportsDefinations"];
            if (value != "" || value != null)
            {
                if(Key=="1")
                {
                    ListClientReportsDefinationsFilter = (from item in ListClientReportsDefinations
                                                          where item.taskname.ToLower().Contains(value.ToLower())
                                                          select item).ToList();
                }
                else if(Key=="2")
                {
                    ListClientReportsDefinationsFilter = (from item in ListClientReportsDefinations
                                                          where  item.description.ToLower().Contains(value.ToLower())
                                                          select item).ToList();
                }

               
            }
            return View(GetVirtualPath("_ClientReportsDefinationsList"), ListClientReportsDefinationsFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Task Name" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Description" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADClientReportsDefinations> lstClientReportsDefinations = objClientReportsDefinations.ListClientReportsDefinations();
            Session["ListClientReportsDefinations"] = lstClientReportsDefinations;
            return PartialView(GetVirtualPath("_ClientReportsDefinationsList"), lstClientReportsDefinations);
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Company/ClientReportsDefinations/" + ViewName + ".cshtml";
            return path;
        }
    }
}