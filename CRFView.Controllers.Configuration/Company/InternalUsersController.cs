﻿using CRFView.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;


namespace CRFView.Controllers
{
    public class InternalUsersController : Controller
    {
        ADInternalUser objInternalUser = new ADInternalUser();
        public ActionResult Index()
        {
            List<ADInternalUser> ListInternalUser = objInternalUser.InternalUserGetList();
            Session["ListInternalUser"] = ListInternalUser;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),ListInternalUser);
        }
        public ActionResult InternalUserDetails(int id)
        {
            ADInternalUser objPortalUsers = objInternalUser.InternalUserRead(id);

            if (objPortalUsers.ModuleList != null)
            {
                if (objPortalUsers.ModuleList.Contains("Configuration"))
                {
                    ViewBag.Configuration = true;
                }
                else
                {
                    ViewBag.Configuration = false;
                }
                if (objPortalUsers.ModuleList.Contains("Clients"))
                {
                    ViewBag.Clients = true;
                }
                else
                {
                    ViewBag.Clients = false;
                }
                if (objPortalUsers.ModuleList.Contains("Accounting"))
                {
                    ViewBag.Accounting = true;
                }
                else
                {
                    ViewBag.Accounting = false;
                }
                if (objPortalUsers.ModuleList.Contains("Liens"))
                {
                    ViewBag.Liens = true;
                }
                else
                {
                    ViewBag.Liens = false;
                }
                if (objPortalUsers.ModuleList.Contains("Collections"))
                {
                    ViewBag.Collections = true;
                }
                else
                {
                    ViewBag.Collections = false;
                }

            }
            return PartialView(GetVirtualPath("InternalUserDetails"), objPortalUsers);

        }
        [HttpPost]
        public ActionResult InternalUserSave(ADInternalUser updatedUser, FormCollection form)
        {
            updatedUser.InternalUser = 1;
            updatedUser.ClientId = 0;

            if (updatedUser.RoleList == "")
            {
                updatedUser.RoleList = "MANAGER";
            }
            string modulelist = "";
            if (Convert.ToBoolean(form["Configuration"].Split(',')[0]) == true)
            {
                modulelist = "Configuration ,";
            }
            if (Convert.ToBoolean(form["Clients"].Split(',')[0]) == true)
            {
                modulelist += " Clients ,";
            }
            if (Convert.ToBoolean(form["Accounting"].Split(',')[0]) == true)
            {
                modulelist += " Accounting ,";
            }
            if (Convert.ToBoolean(form["Liens"].Split(',')[0]) == true)
            {
                modulelist += " Liens ,";
            }
            if (Convert.ToBoolean(form["Collections"].Split(',')[0]) == true)
            {
                modulelist += " Collections";
            }
            updatedUser.ModuleList = modulelist;

            objInternalUser.InternalUserSave(updatedUser);

            List<ADInternalUser> ListInternalUser = objInternalUser.InternalUserGetList();
            Session["ListInternalUser"] = ListInternalUser;
            return PartialView(GetVirtualPath("_InternalUsersList"), ListInternalUser);

        }
        public ActionResult InternalUserAdd()
        {
            ADInternalUser objPortal_Users = new ADInternalUser();
            ModelState.Clear();
            objPortal_Users.ID = 0;
            objPortal_Users.RoleList = "MANAGER";
            ViewBag.Configuration = false;
            ViewBag.Clients = false;
            ViewBag.Accounting = false;
            ViewBag.Liens = false;
            ViewBag.Collections = false;
            return PartialView(GetVirtualPath("InternalUserDetails"), objPortal_Users);

        }

        [HttpPost]
        public ActionResult Search(string value, string Key)
        {
            List<ADInternalUser> ListInternalUsersFilter = new List<ADInternalUser>();
            List<ADInternalUser> ListInternalUser=(List<ADInternalUser>)Session["ListInternalUser"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    ListInternalUsersFilter = (from item in ListInternalUser
                                               where item.UserName.ToLower().Contains(value.ToLower())
                                                 select item).ToList();
                }
                else if (Key == "2")
                {
                    ListInternalUsersFilter = (from item in ListInternalUser
                                               where item.LoginCode.ToLower().Contains(value.ToLower())
                                                   select item).ToList();
                }


                else if (Key == "3")
                {
                    ListInternalUsersFilter = (from item in ListInternalUser
                                               where item.RoleList.ToLower().Contains(value.ToLower())
                                                    select item).ToList();
                }


                else if (Key == "4")
                {
                    ListInternalUsersFilter = (from item in ListInternalUser
                                               where item.Email.ToLower().Contains(value.ToLower())
                                                 select item).ToList();
                }
            }
            return PartialView(GetVirtualPath("_InternalUsersList"), ListInternalUsersFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "UserName" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Login Code" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "Role List" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "Email" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADInternalUser> ListInternalUser = new List<ADInternalUser>();
            ListInternalUser = objInternalUser.InternalUserGetList();
            Session["ListInternalUser"] = ListInternalUser;
            return PartialView(GetVirtualPath("_InternalUsersList"),ListInternalUser);
        }

        public bool validateLogincode(string code)
        {
            bool flag = false;
            List<ADInternalUser> ListLoginCodeFilter = new List<ADInternalUser>();
            List<ADInternalUser> ListInternalUser = (List<ADInternalUser>)Session["ListInternalUser"];
            ListLoginCodeFilter = (from item in ListInternalUser
                                   where item.LoginCode.ToLower().Equals(code.ToLower())
                                   select item).ToList();

            if (ListLoginCodeFilter.Count > 0)
            {
                flag = true;
            }
            return flag;
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Company/InternalUsers/" + ViewName + ".cshtml";
            return path;
        }
    }
}