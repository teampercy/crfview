﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CRFView.Adapters;

namespace CRFView.Controllers
{
    public class ContractTypeController : Controller
    {
        ADContractType objADContractType = new ADContractType();
        // GET: ADContractType
        public ActionResult Index()
        {
            List<ADContractType> lstContractType =  objADContractType.ListContractType();
            SetFilterList();
            Session["ListContractType"] = lstContractType;
            return PartialView(GetVirtualPath("Index"),lstContractType);
        }
        //Show details on Edit
        public ActionResult ContractTypeDetails(int id)
        {
            ADContractType objContractType = objADContractType.ReadContractType(id);
            return View(GetVirtualPath("ContractTypeDetails"), objContractType);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult ContractTypeSave(ADContractType updatedContractType)
        {
            objADContractType.SaveContractType(updatedContractType);
            List<ADContractType> lstContractType = objADContractType.ListContractType();
            Session["ListContractType"] = lstContractType;
            return PartialView(GetVirtualPath("_ContractTypeList"), lstContractType);
        }
        //Show blank details on Add
        public ActionResult ContractTypeAdd()
        {
            ADContractType objContractType = new ADContractType();
            ModelState.Clear();
            objContractType.Id = 0;
            return View(GetVirtualPath("ContractTypeDetails"), objContractType);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value,string Key)
        {
            List<ADContractType> ListContractTypeFilter = new List<ADContractType>();
            List<ADContractType> ListContractType;
            ListContractType = (List<ADContractType>)Session["ListContractType"];
            if (value != "" || value != null)
            {
                if(Key=="1")
                {

                    ListContractTypeFilter = (from item in ListContractType
                                              where item.ContractTypeCode.ToLower().Contains(value.ToLower())             
                                              select item).ToList();
                }
                else if (Key == "2")
                {

                    ListContractTypeFilter = (from item in ListContractType
                                              where item.ContractTypeName.ToLower().Contains(value.ToLower())
                                              select item).ToList();
                }

            }
            return View(GetVirtualPath("_ContractTypeList"), ListContractTypeFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "ContractType Code" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "ContractType Name" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADContractType> lstContractType =  objADContractType.ListContractType();
            Session["ListContractType"] = lstContractType;
            return PartialView(GetVirtualPath("_ContractTypeList"), lstContractType);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Company/ContractType/" + ViewName + ".cshtml";
            return path;
        }
    }
}