﻿using System.Web.Mvc;
using CRFView.Adapters;

namespace CRFView.Controllers
{
    public class ClientReportSettingsController : Controller
    {
        ADClientReportSettings objADClientReportSettings = new ADClientReportSettings();
        // GET: ADClientReportSettings
        public ActionResult Index()
        {
            objADClientReportSettings = objADClientReportSettings.ReadReportSettings(1);
            return PartialView(GetVirtualPath("Index"), objADClientReportSettings);
        }
       
        //Save Updated or New Details
        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ADClientReportSettingsSave(ADClientReportSettings updatedADClientReportSettings)
        {
            objADClientReportSettings.SaveReportSettings(updatedADClientReportSettings);
            ADClientReportSettings objReadADClientReportSettings = objADClientReportSettings.ReadReportSettings(1);
            return PartialView(GetVirtualPath("Index"), objReadADClientReportSettings);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Company/ClientReportSettings/" + ViewName + ".cshtml";
            return path;
        }
    }
}