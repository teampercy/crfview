﻿using System.Web.Mvc;
using CRFView.Adapters;

namespace CRFView.Controllers
{
    public class CompanyInfoController : Controller
    {
        ADCompanyInfo objCompanyInfo = new ADCompanyInfo();
        // GET: ADCompanyInfo
        public ActionResult Index()
        {
            ADCompanyInfo objReadCompanyInfo= objCompanyInfo.ReadCompanyInfo(1);
            ViewBag.JobActionTypeList = CommonBindList.GetJobActionsList();
            return PartialView(GetVirtualPath("Index"), objReadCompanyInfo);
        }
       
        //Save Updated or New Details
        [HttpPost]
        public ActionResult AgencySave(ADCompanyInfo updatedCompanyInfo)
        { 
            objCompanyInfo.SaveCompanyInfo(updatedCompanyInfo);
            ADCompanyInfo objReadCompanyInfo = objCompanyInfo.ReadCompanyInfo(1);  
            ViewBag.JobActionTypeList = CommonBindList.GetJobActionsList();
            return PartialView(GetVirtualPath("Index"), objReadCompanyInfo);
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Company/CompanyInfo/" + ViewName + ".cshtml";
            return path;
        }
    }
}