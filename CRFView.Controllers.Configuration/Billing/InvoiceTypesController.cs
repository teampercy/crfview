﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;


namespace CRFView.Controllers
{
    public class InvoiceTypesController : Controller
    {
        ADInvoiceTypes objADInvoiceTypes = new ADInvoiceTypes();
        // GET: ADInvoiceTypes
        public ActionResult Index()
        {
            List<ADInvoiceTypes> lstInvoiceTypes = GetList();
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstInvoiceTypes);
        }
        //Show details on Edit
        public ActionResult InvoiceTypesDetails(int id)
        {
            ADInvoiceTypes objInvoiceTypes = objADInvoiceTypes.ReadInvoiceTypes(id);
            return View(GetVirtualPath("InvoiceTypesDetails"), objInvoiceTypes);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult InvoiceTypesSave(ADInvoiceTypes updatedInvoiceTypes)
        {
            objADInvoiceTypes.SaveInvoiceTypes(updatedInvoiceTypes);
            List<ADInvoiceTypes> lstInvoiceTypes = GetList();
            return PartialView(GetVirtualPath("_InvoiceTypesList"), lstInvoiceTypes);
        }
        //Show blank details on Add
        public ActionResult InvoiceTypesAdd()
        {
            ADInvoiceTypes objInvoiceTypes = new ADInvoiceTypes();
            ModelState.Clear();
            objInvoiceTypes.FinDocTypeId = 0;
            return View(GetVirtualPath("InvoiceTypesDetails"), objInvoiceTypes);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value ,string Key)
        {
            List<ADInvoiceTypes> ListInvoiceTypesFilter = new List<ADInvoiceTypes>();
            List<ADInvoiceTypes> ListInvoiceTypes= (List<ADInvoiceTypes>)Session["ListInvoiceTypes"];
            if (value != "" || value != null)
            {
 
            if (Key== "1")
                {
                    ListInvoiceTypesFilter = (from item in ListInvoiceTypes
                                                   where item.FinDocTypeName.ToLower().Contains(value.ToLower())
                                                   select item).ToList();
                   
                }
                else if (Key == "2")
                {
                    ListInvoiceTypesFilter = (from item in ListInvoiceTypes
                                              where item.BranchBreak.ToString().ToLower().Contains(value.ToLower())
                                              select item).ToList();
                }
                else if (Key == "2")
                {
                    ListInvoiceTypesFilter = (from item in ListInvoiceTypes
                                              where item.IsAvailableInColl.ToString().ToLower().Contains(value.ToLower())
                                              select item).ToList();
                }


            }
            return View(GetVirtualPath("_InvoiceTypesList"), ListInvoiceTypesFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Invoice Type" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Branch Grouping" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Available In Collection" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADInvoiceTypes> lstInvoiceTypes = GetList();
            return PartialView(GetVirtualPath("_InvoiceTypesList"), lstInvoiceTypes);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Billing/InvoiceTypes/" + ViewName + ".cshtml";
            return path;
        }

        //getList
        public List<ADInvoiceTypes> GetList()
        {
            List<ADInvoiceTypes> lstInvoiceTypes = objADInvoiceTypes.ListInvoiceTypes();
            Session["ListInvoiceTypes"] = lstInvoiceTypes;
            return  lstInvoiceTypes;
        }
    }
}