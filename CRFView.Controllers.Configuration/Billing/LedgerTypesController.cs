﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class LedgerTypesController : Controller
    {
        ADLedgerTypes objADLedgerTypes = new ADLedgerTypes();
        // GET: ADLedgerTypes
        public ActionResult Index()
        {
            List<ADLedgerTypes> lstLedgerTypes = GetList();
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstLedgerTypes);
        }
        //Show details on Edit
        public ActionResult LedgerTypesDetails(int id)
        {
            ADLedgerTypes objLedgerTypes = objADLedgerTypes.ReadLedgerTypes(id);
            return View(GetVirtualPath("LedgerTypesDetails"), objLedgerTypes);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult LedgerTypesSave(ADLedgerTypes updatedLedgerTypes)
        {
            objADLedgerTypes.SaveLedgerTypes(updatedLedgerTypes);
            List<ADLedgerTypes> lstLedgerTypes = GetList();
            return PartialView(GetVirtualPath("_LedgerTypesList"), lstLedgerTypes);
        }
        //Show blank details on Add
        public ActionResult LedgerTypesAdd()
        {
            ADLedgerTypes objLedgerTypes = new ADLedgerTypes();
            ModelState.Clear();
            objLedgerTypes.LedgerTypeId = 0;
            return View(GetVirtualPath("LedgerTypesDetails"), objLedgerTypes);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value ,string Key)
        {
            List<ADLedgerTypes> ListLedgerTypesFilter = new List<ADLedgerTypes>();
            List<ADLedgerTypes> ListLedgerTypes= (List<ADLedgerTypes>)Session["ListLedgerTypes"];
            if (value != "" || value != null)
            {
 
            if (Key== "1")
                {
                    ListLedgerTypesFilter = (from item in ListLedgerTypes
                                                   where item.LedgerType.ToLower().Contains(value.ToLower())
                                                   select item).ToList();
                   
                }
                else if (Key == "2")
                {
                    ListLedgerTypesFilter = (from item in ListLedgerTypes
                                              where item.Description.ToLower().Contains(value.ToLower())
                                              select item).ToList();
                }
    


            }
            return View(GetVirtualPath("_LedgerTypesList"), ListLedgerTypesFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Ledger Type" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Description" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADLedgerTypes> lstLedgerTypes = GetList();
            return PartialView(GetVirtualPath("_LedgerTypesList"), lstLedgerTypes);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Billing/LedgerTypes/" + ViewName + ".cshtml";
            return path;
        }

        //getList
        public List<ADLedgerTypes> GetList()
        {
            List<ADLedgerTypes> lstLedgerTypes = objADLedgerTypes.ListLedgerTypes();
            Session["ListLedgerTypes"] = lstLedgerTypes;
            return  lstLedgerTypes;
        }
    }
}