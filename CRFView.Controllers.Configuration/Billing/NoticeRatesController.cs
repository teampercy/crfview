﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class NoticeRatesController : Controller
    {
        ADNoticeRates objADNoticeRates = new ADNoticeRates();
        // GET: ADNoticeRates
        public ActionResult Index()
        {
            List<ADNoticeRates> lstNoticeRates = GetList();
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstNoticeRates);
        }
        //Show details on Edit
        public ActionResult NoticeRatesDetails(int id)
        {
            ADNoticeRates objNoticeRates = objADNoticeRates.ReadNoticeRates(id);
            List<CommonBindList> BillableEventTypeList = CommonBindList.GetBillableEventTypeList();
            ViewBag.BillableEventTypeList = BillableEventTypeList;
            return View(GetVirtualPath("NoticeRatesDetails"), objNoticeRates);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult NoticeRatesSave(ADNoticeRates updatedNoticeRates)
        {
            objADNoticeRates.SaveNoticeRates(updatedNoticeRates);
            List<ADNoticeRates> lstNoticeRates = GetList();
            return PartialView(GetVirtualPath("_NoticeRatesList"), lstNoticeRates);
        }
        //Show blank details on Add
        public ActionResult NoticeRatesAdd()
        {
            ADNoticeRates objNoticeRates = new ADNoticeRates();
            ModelState.Clear();
            objNoticeRates.Id = 0;
            List<CommonBindList> BillableEventTypeList = CommonBindList.GetBillableEventTypeList();
            ViewBag.BillableEventTypeList = BillableEventTypeList;
            return View(GetVirtualPath("NoticeRatesDetails"), objNoticeRates);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value ,string Key)
        {
            List<ADNoticeRates> ListNoticeRatesFilter = new List<ADNoticeRates>();
            List<ADNoticeRates> ListNoticeRates= (List<ADNoticeRates>)Session["ListNoticeRates"];
            if (value != "" || value != null)
            {
            if (Key== "1")
                {
                    ListNoticeRatesFilter = (from item in ListNoticeRates
                                                   where item.PriceCodeName.ToLower().Contains(value.ToLower())
                                                   select item).ToList();
                }
                else if (Key == "2")
                {
                    ListNoticeRatesFilter = (from item in ListNoticeRates
                                              where item.UsePrelimVolume.ToString().ToLower().Contains(value.ToLower())
                                              select item).ToList();
                }
                else if (Key == "3")
                {
                    ListNoticeRatesFilter = (from item in ListNoticeRates
                                             where item.IsAllInclusivePrice.ToString().ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
                else if (Key == "4")
                {
                    ListNoticeRatesFilter = (from item in ListNoticeRates
                                             where item.IsFreeFormPricing.ToString().ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
                else if (Key == "5")
                {
                    ListNoticeRatesFilter = (from item in ListNoticeRates
                                             where item.PrelimPercentage.ToString().ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
            }
            return View(GetVirtualPath("_NoticeRatesList"), ListNoticeRatesFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Price Code" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Description" });
            filterlist.Add(new CommonBindList { Id = 1, Name = "Ledger Type" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Description" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADNoticeRates> lstNoticeRates = GetList();
            return PartialView(GetVirtualPath("_NoticeRatesList"), lstNoticeRates);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Billing/NoticeRates/" + ViewName + ".cshtml";
            return path;
        }

        //getList
        public List<ADNoticeRates> GetList()
        {
            List<ADNoticeRates> lstNoticeRates = objADNoticeRates.ListNoticeRates();
            Session["ListNoticeRates"] = lstNoticeRates;
            return  lstNoticeRates;
        }
    }
}