﻿using CRFView.Adapters;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class LettersController : Controller
    {
        ADLetters objADLetters = new ADLetters();
        // GET: ADLetters
        public ActionResult Index()
        {
            List<ADLetters> lstLetters = GetList();
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstLetters);
        }
        //Show details on Edit
        public ActionResult LettersDetails(int id)
        {
            ADLetters objLetters = objADLetters.ReadLetters(id);
          return View(GetVirtualPath("LettersDetails"), objLetters);
        }
        //Save Updated or New Details
        [HttpPost]
        public bool LettersSave(ADLetters updatedLetters)
        {
            return objADLetters.SaveLetters(updatedLetters);
        }
        //Show blank details on Add
        public ActionResult LettersAdd()
        {
            ADLetters objLetters = new ADLetters();
            ModelState.Clear();
            objLetters.PKID = 0;
            return View(GetVirtualPath("LettersDetails"), objLetters);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value ,string Key)
        {
            List<ADLetters> ListLettersFilter = new List<ADLetters>();
            List<ADLetters> ListLetters= (List<ADLetters>)Session["ListLetters"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    ListLettersFilter = (from item in ListLetters
                                             where item.LetterDescr.ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
                else if (Key == "2")
                {
                    ListLettersFilter = (from item in ListLetters
                                             where item.LetterType.ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
                else if (Key == "3")
                {
                    ListLettersFilter = (from item in ListLetters
                                            where item.LetterCode.ToLower().Contains(value.ToLower())
                                            select item).ToList();
                }
                else if (Key == "4")
                {
                    ListLettersFilter = (from item in ListLetters
                                            where item.ReviewDays.ToString().Contains(value.ToLower())
                                            select item).ToList();
                }
                else if (Key == "5")
                {
                    ListLettersFilter = (from item in ListLetters
                                         where item.StatusDays.ToString().Contains(value.ToLower())
                                         select item).ToList();
                }
                else if (Key == "6")
                {
                    ListLettersFilter = (from item in ListLetters
                                         where item.GenerateInvoiceNum.ToString().ToLower().Contains(value.ToLower())
                                         select item).ToList();
                }
                else if (Key == "7")
                {
                    ListLettersFilter = (from item in ListLetters
                                         where item.IsInactive.ToString().ToLower().Contains(value.ToLower())
                                         select item).ToList();
                }

            }
            return View(GetVirtualPath("_LettersList"), ListLettersFilter);
        }

        //Set FilterList for search
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Description" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Letter Type" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "Letter Code" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "Review Days" });
            filterlist.Add(new CommonBindList { Id = 5, Name = "Status Days" });
            filterlist.Add(new CommonBindList { Id = 6, Name = "Generate Invoice Num" });
            filterlist.Add(new CommonBindList { Id = 7, Name = "IsInactive" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADLetters> lstLetters = GetList();
            return PartialView(GetVirtualPath("_LettersList"), lstLetters);
        }
        [HttpPost]
        public string LettersPreview(ADLetters updatedLetters)
        {
            objADLetters.SaveLetters(updatedLetters);
           string PdfUrl= objADLetters.PrintLetter(Convert.ToInt32(updatedLetters.DebtorId), updatedLetters.LetterCode);

            if (PdfUrl != null && PdfUrl != "" && PdfUrl!="Error")
            {
                //string path =Path.Combine(ConfigurationManager.AppSettings.Get("rootpath"), "Output/Reports/", System.IO.Path.GetFileName(PdfUrl));
                string path = Path.Combine(Url.Content("~"), "Output/Reports/", System.IO.Path.GetFileName(PdfUrl));

                return (path);
            }
            return "error";
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Collections/Letters/" + ViewName + ".cshtml";
            return path;
        }

        //getList
        public List<ADLetters> GetList()
        {
            List<ADLetters> lstLetters = objADLetters.ListLetters();
            Session["ListLetters"] = lstLetters;
            return  lstLetters;
        }

       
    }
}