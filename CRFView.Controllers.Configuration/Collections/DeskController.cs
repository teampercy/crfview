﻿using CRFView.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class DeskController : Controller
    {
        ADDesk objADDesk = new ADDesk();
        // GET: ADDesk
        public ActionResult Index()
        {
            List<ADDesk> lstDesk = GetList();
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstDesk);
        }
        //Show details on Edit
        public ActionResult DeskDetails(int id)
        {
            ADDesk objDesk = objADDesk.ReadDesk(id);
            ViewBag.CollectionUsers = CommonBindList.GetCollectionUsers();
            ViewBag.ddlSelectedList = CommonBindList.GetLocationUsers(id);
            ViewBag.ddlAvailableList= CommonBindList.GetCollectionUsers();
            return View(GetVirtualPath("DeskDetails"), objDesk);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult DeskSave(ADDesk updatedDesk)
        {
            objADDesk.SaveDesk(updatedDesk);
            List<ADDesk> lstDesk = GetList();
            return PartialView(GetVirtualPath("_DeskList"), lstDesk);
        }
        //Show blank details on Add
        public ActionResult DeskAdd()
        {
            ADDesk objDesk = new ADDesk();
            ModelState.Clear();
            objDesk.LocationId = 0;
            ViewBag.CollectionUsers = CommonBindList.GetCollectionUsers();
            ViewBag.ddlSelectedList = new List<CommonBindList>();
            ViewBag.ddlAvailableList = CommonBindList.GetCollectionUsers(); 
            return View(GetVirtualPath("DeskDetails"), objDesk);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value ,string Key)
        {
            List<ADDesk> ListDeskFilter = new List<ADDesk>();
            List<ADDesk> ListDesk= (List<ADDesk>)Session["ListDesk"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    ListDeskFilter = (from item in ListDesk
                                             where item.Location.ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
                else if (Key == "2")
                {
                    ListDeskFilter = (from item in ListDesk
                                             where item.LocationDescr.ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
                else if (Key == "3")
                {
                    ListDeskFilter = (from item in ListDesk
                                      where item.IsInactive.ToString().ToLower().Contains(value.ToLower())
                                      select item).ToList();
                }
            }
            return View(GetVirtualPath("_DeskList"), ListDeskFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Location" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Location Description" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "Inactive" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADDesk> lstDesk = GetList();
            return PartialView(GetVirtualPath("_DeskList"), lstDesk);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Collections/Desk/" + ViewName + ".cshtml";
            return path;
        }

        //getList
        public List<ADDesk> GetList()
        {
            List<ADDesk> lstDesk = objADDesk.ListDesk();
            Session["ListDesk"] = lstDesk;
            return  lstDesk;
        }

       
    }
}