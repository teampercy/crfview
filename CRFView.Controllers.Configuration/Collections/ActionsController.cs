﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class ActionsController : Controller
    {
        ADActions objADActions = new ADActions();
        // GET: ADActions
        public ActionResult Index()
        {
            List<ADActions> lstActions = GetList();
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstActions);
        }
        //Show details on Edit
        public ActionResult ActionDetails(int id)
        {
            ADActions objActions = objADActions.ReadActions(id);
            if (objActions.IsCollection==true)
            {
                objActions.ActionType = "1";
            }
            else if(objActions.IsForwarding == true)
            {
                objActions.ActionType = "2";
            }
            else if (objActions.IsSmallClaims == true)
            {
                objActions.ActionType = "3";
            }
            else if (objActions.IsSystem == true)
            {
                objActions.ActionType = "4";
            }
            BindDropdowns();
            return View(GetVirtualPath("ActionsDetails"), objActions);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult ActionSave(ADActions updatedActions)
        {
            if (updatedActions.ActionType == "1")
            {
                updatedActions.IsCollection = true;
            }
            else if (updatedActions.ActionType == "2")
            {
                updatedActions.IsForwarding = true;
            }
            else if (updatedActions.ActionType == "3")
            {
                updatedActions.IsSmallClaims = true;
            }
            else if (updatedActions.ActionType == "4")
            {
                updatedActions.IsSystem = true;
            }
            objADActions.SaveActions(updatedActions);
            List<ADActions> lstActions = GetList();
            return PartialView(GetVirtualPath("_ActionsList"), lstActions);
        }
        //Show blank details on Add
        public ActionResult ActionAdd()
        {
            ADActions objActions = new ADActions();
            ModelState.Clear();
            objActions.Id = 0;
            BindDropdowns();
            return View(GetVirtualPath("ActionsDetails"), objActions);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value ,string Key)
        {
            List<ADActions> ListActionsFilter = new List<ADActions>();
            List<ADActions> ListActions= (List<ADActions>)Session["ListActions"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    ListActionsFilter = (from item in ListActions
                                                   where item.Description.ToLower().Contains(value.ToLower())
                                         select item).ToList();
                }
               else if (Key== "2")
                {
                    ListActionsFilter = (from item in ListActions
                                                   where item.ActionCode.ToLower().Contains(value.ToLower())
                                                   select item).ToList();
                   
                }
                else if (Key == "3")
                {
                    ListActionsFilter = (from item in ListActions
                                                   where item.Location.ToLower().Contains(value.ToLower())
                                         select item).ToList();
                }
                else if (Key == "4")
                {
                    ListActionsFilter = (from item in ListActions
                                                   where item.NewToDesk.ToLower().Contains(value.ToLower())
                                         select item).ToList();
                }
                else if (Key == "5")
                {
                    ListActionsFilter = (from item in ListActions
                                         where item.NextCall.ToString().Contains(value.ToLower())
                                         select item).ToList();
                }
                else if (Key == "6")
                {
                    ListActionsFilter = (from item in ListActions
                                         where item.CollectionStatus.ToLower().Contains(value.ToLower())
                                         select item).ToList();
                }
                else if (Key == "7")
                {
                    ListActionsFilter = (from item in ListActions
                                         where item.PriorityCode.ToLower().Contains(value.ToLower())
                                         select item).ToList();
                }

            }
            return View(GetVirtualPath("_ActionsList"), ListActionsFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Action Description" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Action Code" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "NewDeskId" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "NewToDeskId" });
            filterlist.Add(new CommonBindList { Id = 5, Name = "NextCall" });
            filterlist.Add(new CommonBindList { Id = 6, Name = "NewStatus" });
            filterlist.Add(new CommonBindList { Id = 7, Name = "PriorityCode" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADActions> lstActions = GetList();
            return PartialView(GetVirtualPath("_ActionsList"), lstActions);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Collections/Actions/" + ViewName + ".cshtml";
            return path;
        }

        //getList
        public List<ADActions> GetList()
        {
            List<ADActions> lstActions = objADActions.ListActions();
            Session["ListActions"] = lstActions;
            return  lstActions;
        }

        //Set Dropdowns
        public void BindDropdowns()
        {
            ViewBag.PriorityList = CommonBindList.GetPriorityList();
            ViewBag.NewDeskList = CommonBindList.GetLocationList();
            ViewBag.NewToDeskList = CommonBindList.GetLocationList();
            ViewBag.StatusList = CommonBindList.GetCollectionStatusList();
            ViewBag.CategoryTypeList = CommonBindList.GetCollectionActionTypes();
        }
    }
}