﻿using CRFView.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class ReportManagementController : Controller
    {
        ADReportManagement objADReportManagement = new ADReportManagement();
        // GET: ADReportManagement
        public ActionResult Index()
        {
            List<ADReportManagement> lstReport = GetList();
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstReport);
        }
        //Show details on Edit
        public ActionResult ReportManagementDetails(int id)
        {
            ADReportManagement objReport = objADReportManagement.ReadReport(id);
            BindDropdowns();
          return View(GetVirtualPath("ReportManagementDetails"), objReport);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult ReportManagementSave(ADReportManagement updatedReport)
        {
            objADReportManagement.SaveReport(updatedReport);
            List<ADReportManagement> lstReport = GetList();
            return PartialView(GetVirtualPath("_ReportManagementList"), lstReport);
        }
        //Show blank details on Add
        public ActionResult ReportManagementAdd()
        {
            ADReportManagement objReport = new ADReportManagement();
            ModelState.Clear();
            objReport.ReportId = 0;
            BindDropdowns();
            return View(GetVirtualPath("ReportManagementDetails"), objReport);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value ,string Key)
        {
            List<ADReportManagement> ListReportFilter = new List<ADReportManagement>();
            List<ADReportManagement> ListReport= (List<ADReportManagement>)Session["ListReport"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    ListReportFilter = (from item in ListReport
                                             where item.ReportName.ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
                else if (Key == "2")
                {
                    ListReportFilter = (from item in ListReport
                                             where item.Description.ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
                else if (Key == "3")
                {
                    ListReportFilter = (from item in ListReport
                                        where item.ReportCategory.ToLower().Contains(value.ToLower())
                                        select item).ToList();
                }
                else if (Key == "4")
                {
                    ListReportFilter = (from item in ListReport
                                        where item.PrintFrom.ToLower().Contains(value.ToLower())
                                        select item).ToList();
                }
                else if (Key == "5")
                {
                    ListReportFilter = (from item in ListReport
                                        where item.CustomSPName.ToLower().Contains(value.ToLower())
                                        select item).ToList();

                }
                else if (Key == "5")
                {
                    ListReportFilter = (from item in ListReport
                                        where item.ReportFileName.ToLower().Contains(value.ToLower())
                                        select item).ToList();
                }

            }
            return View(GetVirtualPath("_ReportManagementList"), ListReportFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Report Name" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Description" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "Report Category" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "Print From" });
            filterlist.Add(new CommonBindList { Id = 5, Name = "Custom SProc" });
            filterlist.Add(new CommonBindList { Id = 6, Name = "ReportFile Name" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADReportManagement> lstReport = GetList();
            return PartialView(GetVirtualPath("_ReportManagementList"), lstReport);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Collections/ReportManagement/" + ViewName + ".cshtml";
            return path;
        }

        //getList
        public List<ADReportManagement> GetList()
        {
            List<ADReportManagement> lstReport = objADReportManagement.ListReport();
            Session["ListReport"] = lstReport;
            return  lstReport;
        }
        //Bind Dropdowms
        public void BindDropdowns()
        {
            ViewBag.ReportCategoryList = CommonBindList.GetReportCategoryList();
            ViewBag.ReportFileList = CommonBindList.GetReportFilesList();
        }

    }
}