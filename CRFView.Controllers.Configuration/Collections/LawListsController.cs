﻿using CRFView.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class LawListsController : Controller
    {
        ADLawLists objADLawLists = new ADLawLists();
        // GET: ADLawLists
        public ActionResult Index()
        {
            List<ADLawLists> lstLawLists = GetList();
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstLawLists);
        }
        //Show details on Edit
        public ActionResult LawListsDetails(int id)
        {
            ADLawLists objLawLists = objADLawLists.ReadLawLists(id);
          return View(GetVirtualPath("LawListsDetails"), objLawLists);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult LawListsSave(ADLawLists updatedLawLists)
        {
            objADLawLists.SaveLawLists(updatedLawLists);
            List<ADLawLists> lstLawLists = GetList();
            return PartialView(GetVirtualPath("_LawListsList"), lstLawLists);
        }
        //Show blank details on Add
        public ActionResult LawListsAdd()
        {
            ADLawLists objLawLists = new ADLawLists();
            ModelState.Clear();
            objLawLists.PKID = 0;
            return View(GetVirtualPath("LawListsDetails"), objLawLists);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value ,string Key)
        {
            List<ADLawLists> ListLawListsFilter = new List<ADLawLists>();
            List<ADLawLists> ListLawLists= (List<ADLawLists>)Session["ListLawLists"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    ListLawListsFilter = (from item in ListLawLists
                                             where item.LawListCode.ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
                else if (Key == "2")
                {
                    ListLawListsFilter = (from item in ListLawLists
                                             where item.LawList.ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }
              
            }
            return View(GetVirtualPath("_LawListsList"), ListLawListsFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "LawList Code" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "LawList" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADLawLists> lstLawLists = GetList();
            return PartialView(GetVirtualPath("_LawListsList"), lstLawLists);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Collections/LawLists/" + ViewName + ".cshtml";
            return path;
        }

        //getList
        public List<ADLawLists> GetList()
        {
            List<ADLawLists> lstLawLists = objADLawLists.ListLawLists();
            Session["ListLawLists"] = lstLawLists;
            return  lstLawLists;
        }

       
    }
}