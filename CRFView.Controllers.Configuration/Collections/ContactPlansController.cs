﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class ContactPlansController : Controller
    {
        ADContactPlans objADContactPlans = new ADContactPlans();
        // GET: ADContactPlans
        public ActionResult Index()
        {
            List<ADContactPlans> lstContactPlans = GetList();
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstContactPlans);
        }
        //Show details on Edit
        public ActionResult ContactPlansDetails(int id)
        {
            ADContactPlans objContactPlans = objADContactPlans.ReadContactPlans(id);
            BindDropdowns();
            return View(GetVirtualPath("ContactPlansDetails"), objContactPlans);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult ContactPlansSave(ADContactPlans updatedContactPlans)
        {
            objADContactPlans.SaveContactPlans(updatedContactPlans);
            List<ADContactPlans> lstContactPlans = GetList();
            return PartialView(GetVirtualPath("_ContactPlansList"), lstContactPlans);
        }
        //Show blank details on Add
        public ActionResult ContactPlansAdd()
        {
            ADContactPlans objContactPlans = new ADContactPlans();
            ModelState.Clear();
            BindDropdowns();
            objContactPlans.ContactPlanId = 0;
            return View(GetVirtualPath("ContactPlansDetails"), objContactPlans);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value ,string Key)
        {
            List<ADContactPlans> ListContactPlansFilter = new List<ADContactPlans>();
            List<ADContactPlans> ListContactPlans= (List<ADContactPlans>)Session["ListContactPlans"];
            if (value != "" || value != null)
            {
                switch (Key)
                {
                    case "1":
                        ListContactPlansFilter = (from item in ListContactPlans
                                                  where item.ContactPlanCode.ToLower().Contains(value.ToLower())
                                                  select item).ToList();
                        break;

                    case "2":
                        ListContactPlansFilter = (from item in ListContactPlans
                                                  where item.ContactPlan.ToLower().Contains(value.ToLower())
                                                  select item).ToList();
                        break;

                    case "3":
                        ListContactPlansFilter = (from item in ListContactPlans
                                                  where item.IsTenDayContactPlan.ToString().ToLower().Contains(value.ToLower())
                                                  select item).ToList();
                        break;
                    default:

                        break;
                }

            }
            return View(GetVirtualPath("_ContactPlansList"), ListContactPlansFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Contact Plan Code" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Contact Plan" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "10 Days Contact Plan" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADContactPlans> lstContactPlans = GetList();
            return PartialView(GetVirtualPath("_ContactPlansList"), lstContactPlans);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Collections/ContactPlans/" + ViewName + ".cshtml";
            return path;
        }

        //getList
        public List<ADContactPlans> GetList()
        {
            List<ADContactPlans> lstContactPlans = objADContactPlans.ListContactPlans();
            Session["ListContactPlans"] = lstContactPlans;
            return  lstContactPlans;
        }

        //Bind Dropdowns
        public void BindDropdowns()
        {
            
            ViewBag.StatusList = CommonBindList.GetCollectionStatusList();
            ViewBag.LocationList = CommonBindList.GetLocationList();
        }
    }
}