﻿using CRFView.Adapters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class StatusGroupsController : Controller
    {
        ADStatusGroups objADStatusGroups = new ADStatusGroups();
        // GET: ADStatusGroups
        public ActionResult Index()
        {
            List<ADStatusGroups> lstStatusGroups = GetList();
            SetFilterList();
            return PartialView(GetVirtualPath("Index"),lstStatusGroups);
        }
        //Show details on Edit
        public ActionResult StatusGroupsDetails(int id)
        {
            ADStatusGroups objStatusGroups = objADStatusGroups.ReadStatusGroups(id);
          return View(GetVirtualPath("StatusGroupsDetails"), objStatusGroups);
        }
        //Save Updated or New Details
        [HttpPost]
        public ActionResult StatusGroupsSave(ADStatusGroups updatedStatusGroups)
        {
            objADStatusGroups.SaveStatusGroups(updatedStatusGroups);
            List<ADStatusGroups> lstStatusGroups = GetList();
            return PartialView(GetVirtualPath("_StatusGroupsList"), lstStatusGroups);
        }
        //Show blank details on Add
        public ActionResult StatusGroupsAdd()
        {
            ADStatusGroups objStatusGroups = new ADStatusGroups();
            ModelState.Clear();
            objStatusGroups.Id = 0;
            return View(GetVirtualPath("StatusGroupsDetails"), objStatusGroups);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value ,string Key)
        {
            List<ADStatusGroups> ListStatusGroupsFilter = new List<ADStatusGroups>();
            List<ADStatusGroups> ListStatusGroups= (List<ADStatusGroups>)Session["ListStatusGroups"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    ListStatusGroupsFilter = (from item in ListStatusGroups
                                             where item.StatusGroupDescription.ToLower().Contains(value.ToLower())
                                             select item).ToList();
                }

              
            }
            return View(GetVirtualPath("_StatusGroupsList"), ListStatusGroupsFilter);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "StatusGroup Description" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADStatusGroups> lstStatusGroups = GetList();
            return PartialView(GetVirtualPath("_StatusGroupsList"), lstStatusGroups);
        }
        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Collections/StatusGroups/" + ViewName + ".cshtml";
            return path;
        }

        //getList
        public List<ADStatusGroups> GetList()
        {
            List<ADStatusGroups> lstStatusGroups = objADStatusGroups.ListStatusGroups();
            Session["ListStatusGroups"] = lstStatusGroups;
            return  lstStatusGroups;
        }

       
    }
}