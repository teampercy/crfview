﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CRFView.Adapters;

namespace CRFView.Controllers
{
    public class JobViewDesksController : Controller
    {
        ADJobViewDesks objJobViewDesks = new ADJobViewDesks();
        List<ADJobViewDesks> ResultListJobViewDesks = new List<ADJobViewDesks>();
        // GET: JobViewDesks
        public ActionResult Index()
        {
            ResultListJobViewDesks = GetList();
            return PartialView(GetVirtualPath("Index"), ResultListJobViewDesks);
        }
        //Show details on Edit
        public ActionResult JobViewDesksDetails(int id)
        {
            ADJobViewDesks JobViewDesksinfo = objJobViewDesks.ReadJobViewDesks(id);
            return View(GetVirtualPath("JobViewDesksDetails"), JobViewDesksinfo);
        }
     
        //Save Updated or New Details
        [HttpPost]
        public bool JobViewDesksSave(ADJobViewDesks updatedJobViewDesks)
        {
            return objJobViewDesks.SaveJobViewDesks(updatedJobViewDesks);
        }
        //Show blank details on Add
        public ActionResult JobViewDesksAdd()
        {
            ADJobViewDesks objJobViewDesks = new ADJobViewDesks();
            ModelState.Clear();
            objJobViewDesks.DeskId = 0;
            return View(GetVirtualPath("JobViewDesksDetails"), objJobViewDesks);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value,string Key)
        {
            List<ADJobViewDesks> FilterResult = new List<ADJobViewDesks>();
            List<ADJobViewDesks> ListJobViewDesks = (List<ADJobViewDesks>)Session["ListJobViewDesks"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    FilterResult = (from item in ListJobViewDesks
                                     where item.DeskNum.ToLower().Contains(value.ToLower())
                                     select item).ToList();  
                }
               else if (Key == "2")
                {
                    FilterResult = (from item in ListJobViewDesks
                                    where item.DeskName.ToLower().Contains(value.ToLower())
                                    select item).ToList();
                }
            }

            return View(GetVirtualPath("_JobViewDesksList"), FilterResult);
        }


        [HttpPost]
        public ActionResult Refresh()
        {
            ResultListJobViewDesks = GetList();
            return PartialView(GetVirtualPath("_JobViewDesksList"), ResultListJobViewDesks);
        }
       
        // Get List from database 
        public List<ADJobViewDesks> GetList()
        {
            List<ADJobViewDesks> lstJobViewDesks = objJobViewDesks.ListJobViewDesks();
            Session["ListJobViewDesks"] = lstJobViewDesks;
            SetFilterList();
            return lstJobViewDesks;
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Desk Number" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Desk Name" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/JobView/JobViewDesks/" + ViewName + ".cshtml";
            return path;
        }
    }
}