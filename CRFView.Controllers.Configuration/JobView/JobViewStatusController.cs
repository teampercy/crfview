﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CRFView.Adapters;

namespace CRFView.Controllers
{
    public class JobViewStatusController : Controller
    {
        ADJobViewStatus objJobViewStatus= new ADJobViewStatus();
        List<ADJobViewStatus> ResultListJobViewStatus= new List<ADJobViewStatus>();
        // GET: JobViewDesks
        public ActionResult Index()
        {
            ResultListJobViewStatus= GetList();
            return PartialView(GetVirtualPath("Index"), ResultListJobViewStatus);
        }
        //Show details on Edit
        public ActionResult JobViewStatusDetails(int id)
        {
            ADJobViewStatus JobViewStatusinfo = objJobViewStatus.ReadJobViewStatus(id);
            BindDropdowns();
            return View(GetVirtualPath("JobViewStatusDetails"), JobViewStatusinfo);
        }
     
        //Save Updated or New Details
        [HttpPost]
        public bool JobViewStatusSave(ADJobViewStatus updatedJobViewStatus)
        {
            return objJobViewStatus.SaveJobViewStatus(updatedJobViewStatus);
        }
        //Show blank details on Add
        public ActionResult JobViewStatusAdd()
        {
            ADJobViewStatus objJobViewStatus= new ADJobViewStatus();
            ModelState.Clear();
            objJobViewStatus.Id = 0;
            BindDropdowns();
            return View(GetVirtualPath("JobViewStatusDetails"), objJobViewStatus);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value,string Key)
        {
            List<ADJobViewStatus> FilterResult = new List<ADJobViewStatus>();
            List<ADJobViewStatus> ListJobViewStatus= (List<ADJobViewStatus>)Session["ListJobViewStatus"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    FilterResult = (from item in ListJobViewStatus
                                    where item.JobviewStatus.ToLower().Contains(value.ToLower())
                                     select item).ToList();  
                }
               else if (Key == "2")
                {
                    FilterResult = (from item in ListJobViewStatus
                                    where item.Description.ToLower().Contains(value.ToLower())
                                    select item).ToList();
                }
                else if (Key == "3")
                {
                    FilterResult = (from item in ListJobViewStatus
                                    where item.StatusGroupDescription.ToLower().Contains(value.ToLower())
                                    select item).ToList();
                }
                else if (Key == "4")
                {
                    FilterResult = (from item in ListJobViewStatus
                                    where item.IsManualStatus.ToString().ToLower().Contains(value.ToLower())
                                    select item).ToList();
                }
                else if (Key == "5")
                {
                    FilterResult = (from item in ListJobViewStatus
                                    where item.IsNoticeRequestStatus.ToString().ToLower().Contains(value.ToLower())
                                    select item).ToList();
                }
                else if (Key == "6")
                {
                    FilterResult = (from item in ListJobViewStatus
                                    where item.IsClosed.ToString().ToLower().Contains(value.ToLower())
                                    select item).ToList();
                }
            }

            return View(GetVirtualPath("_JobViewStatusList"), FilterResult);
        }
        
        [HttpPost]
        public ActionResult Refresh()
        {
            ResultListJobViewStatus= GetList();
            return PartialView(GetVirtualPath("_JobViewStatusList"), ResultListJobViewStatus);
        }
       
        // Get List from database 
        public List<ADJobViewStatus> GetList()
        {
            List<ADJobViewStatus> lstJobViewStatus= objJobViewStatus.ListJobViewStatus();
            Session["ListJobViewStatus"] = lstJobViewStatus;
            SetFilterList();
            return lstJobViewStatus;
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Jobview Status" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Description" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "Status Group" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "Manual" });
            filterlist.Add(new CommonBindList { Id = 5, Name = "Notice" });
            filterlist.Add(new CommonBindList { Id = 6, Name = "Close" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }
        //Bind dropdowns
        public void BindDropdowns()
        {
            ViewBag.StatusGroupList =CommonBindList.GetStatusGroupList();
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/JobView/JobViewStatus/" + ViewName + ".cshtml";
            return path;
        }
    }
}