﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CRFView.Adapters;

namespace CRFView.Controllers
{
    public class JobViewDeskStateController : Controller
    {
        ADJobViewDeskState objJobViewDeskState= new ADJobViewDeskState();
        List<ADJobViewDeskState> ResultListJobViewDeskState= new List<ADJobViewDeskState>();
        // GET: JobViewDesks
        public ActionResult Index()
        {
            ResultListJobViewDeskState= GetList();
            return PartialView(GetVirtualPath("Index"), ResultListJobViewDeskState);
        }
        //Show details on Edit
        public ActionResult JobViewDeskStateDetails(int id)
        {
            ADJobViewDeskState JobViewDeskStateinfo = objJobViewDeskState.ReadJobViewDeskState(id);
            BindDropdowns();
            return View(GetVirtualPath("JobViewDeskStateDetails"), JobViewDeskStateinfo);
        }
     
        //Save Updated or New Details
        [HttpPost]
        public bool JobViewDeskStateSave(ADJobViewDeskState updatedJobViewDeskState)
        {
            return objJobViewDeskState.SaveJobViewDeskState(updatedJobViewDeskState);
        }
        //Show blank details on Add
        public ActionResult JobViewDeskStateAdd()
        {
            ADJobViewDeskState objJobViewDeskState= new ADJobViewDeskState();
            ModelState.Clear();
            objJobViewDeskState.Id = 0;
            BindDropdowns();
            return View(GetVirtualPath("JobViewDeskStateDetails"), objJobViewDeskState);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value,string Key)
        {
            List<ADJobViewDeskState> FilterResult = new List<ADJobViewDeskState>();
            List<ADJobViewDeskState> ListJobViewDeskState= (List<ADJobViewDeskState>)Session["ListJobViewDeskState"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    FilterResult = (from item in ListJobViewDeskState
                                    where item.JobViewDeskNum.ToLower().Contains(value.ToLower())
                                     select item).ToList();  
                }
               else if (Key == "2")
                {
                    FilterResult = (from item in ListJobViewDeskState
                                    where item.JobState.ToLower().Contains(value.ToLower())
                                    select item).ToList();
                }
            }

            return View(GetVirtualPath("_JobViewDeskStateList"), FilterResult);
        }
        
        [HttpPost]
        public ActionResult Refresh()
        {
            ResultListJobViewDeskState= GetList();
            return PartialView(GetVirtualPath("_JobViewDeskStateList"), ResultListJobViewDeskState);
        }
       
        // Get List from database 
        public List<ADJobViewDeskState> GetList()
        {
            List<ADJobViewDeskState> lstJobViewDeskState= objJobViewDeskState.ListJobViewDeskState();
            Session["ListJobViewDeskState"] = lstJobViewDeskState;
            SetFilterList();
            return lstJobViewDeskState;
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Desk Number" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Job State" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }
        //Bind dropdowns
        public void BindDropdowns()
        {
            ViewBag.DeskList =CommonBindList.GetJobViewDesksList();
            ViewBag.JobStateList =CommonBindList.GetJobStateList();
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/JobView/JobViewDeskState/" + ViewName + ".cshtml";
            return path;
        }
    }
}