﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class NewsController : Controller
    {
        ADPortal_News objNews = new ADPortal_News();

        // GET: News
        public ActionResult Index()
        {
            List<ADPortal_News> NewsList = objNews.ListNews();
            Session["ListNews"] = NewsList;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"), NewsList);

        }
        //Show details on Edit
        public ActionResult NewsDetails(int id)
        {
            objNews = new ADPortal_News();
            objNews = objNews.ReadNews(id);

            return View(GetVirtualPath("NewsDetails"), objNews);
        }
        //Save Updated or New Details
        [HttpPost]
        [ValidateInput(false)]
        public bool NewsSave(ADPortal_News updatedNews)
        {
            return objNews.SaveNews(updatedNews);
        }
        //Show blank details on Add
        public ActionResult NewsAdd()
        {
            objNews = new ADPortal_News();
            objNews.PKID = 0;
            return View(GetVirtualPath("NewsDetails"), objNews);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {

            List<ADPortal_News> NewsList = (List<ADPortal_News>)Session["ListNews"];
            List<ADPortal_News> NewsFilterList = null;
            if (value != "" || value != null)
            {
                switch (Key)
                {
                    case "1":
                        NewsFilterList = (from item in NewsList
                                          where item.title.ToLower().Contains(value.ToLower())
                                          select item).ToList();
                        break;
                    case "2":
                        NewsFilterList = (from item in NewsList
                                          where item.shortdescription.ToLower().Contains(value.ToLower())
                                          select item).ToList();
                        break;
                    default:
                        NewsFilterList = null;
                        break;
                }
            }
            return View(GetVirtualPath("_NewsList"), NewsFilterList);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Title" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Short description" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADPortal_News> ListNews = objNews.ListNews();
            Session["ListNews"] = ListNews;
            return PartialView(GetVirtualPath("_NewsList"), ListNews);
        }

        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Website/News/" + ViewName + ".cshtml";
            return path;
        }
    }
}