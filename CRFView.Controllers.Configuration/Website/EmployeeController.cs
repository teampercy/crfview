﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class EmployeesController : Controller
    {
        ADPortal_Employee objEmployees = new ADPortal_Employee();

        // GET: Employees
        public ActionResult Index()
        {
            List<ADPortal_Employee> EmployeesList = objEmployees.ListEmployees();
            Session["ListEmployees"] = EmployeesList;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"), EmployeesList);

        }
        //Show details on Edit
        public ActionResult EmployeesDetails(int id)
        {
            objEmployees = new ADPortal_Employee();
            objEmployees = objEmployees.ReadEmployees(id);

            return View(GetVirtualPath("EmployeesDetails"), objEmployees);
        }
        //Save Updated or New Details
        [HttpPost]
        [ValidateInput(false)]
        public bool EmployeesSave(ADPortal_Employee updatedEmployees)
        {
            return objEmployees.SaveEmployees(updatedEmployees);
        }
        //Show blank details on Add
        public ActionResult EmployeesAdd()
        {
            objEmployees = new ADPortal_Employee();
            objEmployees.PKID = 0;
            return View(GetVirtualPath("EmployeesDetails"), objEmployees);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {

            List<ADPortal_Employee> EmployeesList = (List<ADPortal_Employee>)Session["ListEmployees"];
            List<ADPortal_Employee> EmployeesFilterList = null;
            if (value != "" || value != null)
            {
                switch (Key)
                {
                    case "1":
                        EmployeesFilterList = (from item in EmployeesList
                                          where item.fullname.ToLower().Contains(value.ToLower())
                                          select item).ToList();
                        break;
                    case "2":
                        EmployeesFilterList = (from item in EmployeesList
                                          where item.title.ToLower().Contains(value.ToLower())
                                          select item).ToList();
                        break;
                    case "3":
                        EmployeesFilterList = (from item in EmployeesList
                                               where item.isvisible.ToString().ToLower().Contains(value.ToLower())
                                               select item).ToList();
                        break;
                    case "4":
                        EmployeesFilterList = (from item in EmployeesList
                                               where item.IsMarketing.ToString().ToLower().Contains(value.ToLower())
                                               select item).ToList();
                        break;
                    case "5":
                        EmployeesFilterList = (from item in EmployeesList
                                               where item.DisplayOrder.ToString().Contains(value)
                                               select item).ToList();
                        break;
                    default:
                        EmployeesFilterList = null;
                        break;
                }
            }
            return View(GetVirtualPath("_EmployeesList"), EmployeesFilterList);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "fullname" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Title" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "Management" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "Sales" });
            filterlist.Add(new CommonBindList { Id = 5, Name = "DisplayOrder" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADPortal_Employee> ListEmployees = objEmployees.ListEmployees();
            Session["ListEmployees"] = ListEmployees;
            return PartialView(GetVirtualPath("_EmployeesList"), ListEmployees);
        }

        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Website/Employees/" + ViewName + ".cshtml";
            return path;
        }
    }
}