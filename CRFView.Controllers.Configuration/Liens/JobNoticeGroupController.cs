﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class JobNoticeGroupController : Controller
    {
        ADJobNoticeGroup objJobNoticeGroup = new ADJobNoticeGroup();

        // GET: JobNoticeGroup
        public ActionResult Index()
        {
            List<ADJobNoticeGroup> JobNoticeGroupList = objJobNoticeGroup.ListJobNoticeGroup();
            Session["ListJobNoticeGroup"] = JobNoticeGroupList;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"), JobNoticeGroupList);

        }
        //Show details on Edit
        public ActionResult JobNoticeGroupDetails(int id)
        {
            objJobNoticeGroup = new ADJobNoticeGroup();
            ViewBag.MailDeliveredResultList = CommonBindList.GetMailDeliveredResultList();
            objJobNoticeGroup = objJobNoticeGroup.ReadJobNoticeGroup(id);
            return View(GetVirtualPath("JobNoticeGroupDetails"), objJobNoticeGroup);
        }
        //Save Updated or New Details
        [HttpPost]
        public bool JobNoticeGroupSave(ADJobNoticeGroup updatedJobNoticeGroup)
        {
            return objJobNoticeGroup.SaveJobNoticeGroup(updatedJobNoticeGroup);
        }
        //Show blank details on Add
        public ActionResult JobNoticeGroupAdd()
        {
            objJobNoticeGroup = new ADJobNoticeGroup();
            objJobNoticeGroup.Id = 0;
            ViewBag.MailDeliveredResultList = CommonBindList.GetMailDeliveredResultList();
            return View(GetVirtualPath("JobNoticeGroupDetails"), objJobNoticeGroup);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {

            List<ADJobNoticeGroup> JobNoticeGroupList = (List<ADJobNoticeGroup>)Session["ListJobNoticeGroup"];
            List<ADJobNoticeGroup> JobNoticeGroupFilterList = null;
            if (value != "" || value != null)
            {
                switch (Key)
                {
                    case "1":
                        JobNoticeGroupFilterList = (from item in JobNoticeGroupList
                                                    where item.PrintGroup.ToLower().Contains(value.ToLower())
                                                    select item).ToList();
                        break;
                    case "2":
                        JobNoticeGroupFilterList = (from item in JobNoticeGroupList
                                                    where item.IsTXNotice.ToString().ToLower().Contains(value.ToLower())
                                                    select item).ToList();
                        break;
                    case "3":
                        JobNoticeGroupFilterList = (from item in JobNoticeGroupList
                                                    where item.PostageRate.ToLower().Contains(value.ToLower())
                                                    select item).ToList();
                        break;
                    case "4":
                        JobNoticeGroupFilterList = (from item in JobNoticeGroupList
                                                    where item.PrintMailingLog.ToString().ToLower().Contains(value.ToLower())
                                                    select item).ToList();
                        break;
                    case "5":
                        JobNoticeGroupFilterList = (from item in JobNoticeGroupList
                                                    where item.IsCertifiedMail.ToString().ToLower().Contains(value.ToLower())
                                                    select item).ToList();
                        break;
                    case "6":
                        JobNoticeGroupFilterList = (from item in JobNoticeGroupList
                                                    where item.IsGreencard.ToString().ToLower().Contains(value.ToLower())
                                                    select item).ToList();
                        break;
                    case "7":
                        JobNoticeGroupFilterList = (from item in JobNoticeGroupList
                                                    where item.IsMailingLogPostage.ToString().ToLower().Contains(value.ToLower())
                                                    select item).ToList();
                        break;
                 
                }

            }
            return View(GetVirtualPath("_JobNoticeGroupList"), JobNoticeGroupFilterList);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "PrintGroup" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "TX" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "PostageRate" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "PrintMailingLog" });
            filterlist.Add(new CommonBindList { Id = 5, Name = "CertifiedMail" });
            filterlist.Add(new CommonBindList { Id = 6, Name = "Greencard" });
            filterlist.Add(new CommonBindList { Id = 7, Name = "MailingLogPostage" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

    
        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADJobNoticeGroup> ListJobNoticeGroup = objJobNoticeGroup.ListJobNoticeGroup();
            Session["ListJobNoticeGroup"] = ListJobNoticeGroup;
            return PartialView(GetVirtualPath("_JobNoticeGroupList"), ListJobNoticeGroup);
        }

        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Liens/JobNoticeGroup/" + ViewName + ".cshtml";
            return path;
        }
    }
}