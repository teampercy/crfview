﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class StateFormNoticesController : Controller
    {
        ADStateFormNotices objStateFormNotices = new ADStateFormNotices();

        // GET: StateFormNotices
        public ActionResult Index()
        {
            List<ADStateFormNotices> StateFormNoticesList = objStateFormNotices.ListStateFormNotices();
            Session["ListStateFormNotices"] = StateFormNoticesList;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"), StateFormNoticesList);

        }
        //Show details on Edit
        public ActionResult StateFormNoticesDetails(int id)
        {
            objStateFormNotices = new ADStateFormNotices();
            objStateFormNotices = objStateFormNotices.ReadStateFormNotices(id);
            return View(GetVirtualPath("StateFormNoticesDetails"), objStateFormNotices);
        }
        //Save Updated or New Details
        [HttpPost]
        public bool StateFormNoticesSave(ADStateFormNotices updatedStateFormNotices)
        {
            return objStateFormNotices.SaveStateFormNotices(updatedStateFormNotices);
        }
        //Show blank details on Add
        public ActionResult StateFormNoticesAdd()
        {
            objStateFormNotices = new ADStateFormNotices();
            objStateFormNotices.StateFormNoticeId = 0;
            return View(GetVirtualPath("StateFormNoticesDetails"), objStateFormNotices);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {

            List<ADStateFormNotices> StateFormNoticesList = (List<ADStateFormNotices>)Session["ListStateFormNotices"];
            List<ADStateFormNotices> StateFormNoticesFilterList = null;
            if (value != "" || value != null)
            {
                switch (Key)
                {
                    case "1":
                        StateFormNoticesFilterList = (from item in StateFormNoticesList
                                              where item.StateCode.ToLower().Contains(value.ToLower())
                                              select item).ToList();
                        break;
                    case "2":
                        StateFormNoticesFilterList = (from item in StateFormNoticesList
                                              where item.ReportName.ToLower().Contains(value.ToLower())
                                              select item).ToList();
                        break;
                    case "3":
                        StateFormNoticesFilterList = (from item in StateFormNoticesList
                                                     where item.FriendlyName.ToLower().Contains(value.ToLower())
                                                     select item).ToList();
                        break;
                    case "4":
                        StateFormNoticesFilterList = (from item in StateFormNoticesList
                                                      where item.FormType.ToLower().Contains(value.ToLower())
                                                      select item).ToList();
                        break;
                    default:
                        StateFormNoticesFilterList = null;
                        break;
                }
            }
            return View(GetVirtualPath("_StateFormNoticesList"), StateFormNoticesFilterList);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "State Code" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Report Name" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "Friendly Name" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "Form Type" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADStateFormNotices> ListStateFormNotices = objStateFormNotices.ListStateFormNotices();
            Session["ListStateFormNotices"] = ListStateFormNotices;
            return PartialView(GetVirtualPath("_StateFormNoticesList"), ListStateFormNotices);
        }

        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Liens/StateFormNotices/" + ViewName + ".cshtml";
            return path;
        }
    }
}