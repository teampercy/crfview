﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class MailReturnsController : Controller
    {
        ADMailReturns objMailReturns = new ADMailReturns();

        // GET: MailReturns
        public ActionResult Index()
        {
            List<ADMailReturns> MailReturnsList = objMailReturns.ListMailReturns();
            Session["ListMailReturns"] = MailReturnsList;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"), MailReturnsList);

        }
        //Show details on Edit
        public ActionResult MailReturnsDetails(int id)
        {
            objMailReturns = new ADMailReturns();
            BindDropdowns();
            objMailReturns = ADMailReturns.ReadMailReturns(id);
            return View(GetVirtualPath("MailReturnsDetails"), objMailReturns);
        }
        //Save Updated or New Details
        [HttpPost]
        public bool MailReturnsSave(ADMailReturns updatedMailReturns)
        {
            return objMailReturns.SaveMailReturns(updatedMailReturns);
        }
        //Show blank details on Add
        public ActionResult MailReturnsAdd()
        {
            objMailReturns = new ADMailReturns();
            objMailReturns.Id = 0;
            BindDropdowns();
            return View(GetVirtualPath("MailReturnsDetails"), objMailReturns);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {

            List<ADMailReturns> MailReturnsList = (List<ADMailReturns>)Session["ListMailReturns"];
            List<ADMailReturns> MailReturnsFilterList = null;
            if (value != "" || value != null)
            {
                switch (Key)
                {
                    case "1":
                        MailReturnsFilterList = (from item in MailReturnsList
                                                        where item.Description.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    case "2":

                        MailReturnsFilterList = (from item in MailReturnsList
                                                        where item.LegalPartyType.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    case "3":

                        MailReturnsFilterList = (from item in MailReturnsList
                                                        where item.ResearchType.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    case "4":

                        MailReturnsFilterList = (from item in MailReturnsList
                                                        where item.ReasonType.ToLower().Contains(value.ToLower())
                                                        select item).ToList();
                        break;
                    case "5":

                        MailReturnsFilterList = (from item in MailReturnsList
                                                 where item.ActionDescription.ToLower().Contains(value.ToLower())
                                                 select item).ToList();
                        break;
                    default:
                        MailReturnsFilterList = null;
                        break;

                }

            }
            return View(GetVirtualPath("_MailReturnsList"), MailReturnsFilterList);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Description" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "LegalParty Type" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "Research Type" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "Reason Type" });
            filterlist.Add(new CommonBindList { Id = 5, Name = "Action Description" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

    
        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADMailReturns> ListMailReturns = objMailReturns.ListMailReturns();
            Session["ListMailReturns"] = ListMailReturns;
            return PartialView(GetVirtualPath("_MailReturnsList"), ListMailReturns);
        }

        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Liens/MailReturns/" + ViewName + ".cshtml";
            return path;
        }

        public void BindDropdowns()
        {
            ViewBag.JobActionList=CommonBindList.GetMailReturnJobActionList();
            ViewBag.MailReturnReasonList = CommonBindList.GetMailReturnReasonList();
            ViewBag.MailReturnResearchList = CommonBindList.GetMailReturnResearchList();
        }
    }
}