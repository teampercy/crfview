﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class JobStatusController : Controller
    {
        ADJobStatus objJobStatus = new ADJobStatus();

        // GET: JobStatus
        public ActionResult Index()
        {
            List<ADJobStatus> JobStatusList = objJobStatus.ListJobStatus();
            Session["ListJobStatus"] = JobStatusList;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"), JobStatusList);

        }
        //Show details on Edit
        public ActionResult JobStatusDetails(int id)
        {
            objJobStatus = new ADJobStatus();
            ViewBag.StatusGroupList = CommonBindList.GetStatusGroupList();
            objJobStatus = objJobStatus.ReadJobStatus(id);
            return View(GetVirtualPath("JobStatusDetails"), objJobStatus);
        }
        //Save Updated or New Details
        [HttpPost]
        public bool JobStatusSave(ADJobStatus updatedJobStatus)
        {
            return objJobStatus.SaveJobStatus(updatedJobStatus);
        }
        //Show blank details on Add
        public ActionResult JobStatusAdd()
        {
            objJobStatus = new ADJobStatus();
            objJobStatus.Id = 0;
            ViewBag.StatusGroupList = CommonBindList.GetStatusGroupList();
            return View(GetVirtualPath("JobStatusDetails"), objJobStatus);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {

            List<ADJobStatus> JobStatusList = (List<ADJobStatus>)Session["ListJobStatus"];
            List<ADJobStatus> JobStatusFilterList = null;
            if (value != "" || value != null)
            {
                switch (Key)
                {
                    case "1":
                        JobStatusFilterList = (from item in JobStatusList
                                              where item.JobStatus.ToLower().Contains(value.ToLower())
                                              select item).ToList();
                        break;
                    case "2":
                        JobStatusFilterList = (from item in JobStatusList
                                              where item.JobStatusDescr.ToLower().Contains(value.ToLower())
                                              select item).ToList();
                        break;
                    case "3":
                        JobStatusFilterList = (from item in JobStatusList
                                               where item.StatusGroupDescription.ToLower().Contains(value.ToLower())
                                               select item).ToList();
                        break;
                    case "4":
                        JobStatusFilterList = (from item in JobStatusList
                                               where item.IsClosed.ToString().ToLower().Contains(value.ToLower())
                                               select item).ToList();
                        break;
                    case "5":
                        JobStatusFilterList = (from item in JobStatusList
                                               where item.IsLienAlert.ToString().ToLower().Contains(value.ToLower())
                                               select item).ToList();
                        break;
                    case "6":
                        JobStatusFilterList = (from item in JobStatusList
                                               where item.IsNoticeRequestStatus.ToString().ToLower().Contains(value.ToLower())
                                               select item).ToList();
                        break;
                    case "7":
                        JobStatusFilterList = (from item in JobStatusList
                                               where item.LongDescription.ToLower().Contains(value.ToLower())
                                               select item).ToList();
                        break;
                    default:
                        JobStatusFilterList = null;
                        break;
                }
            }
            return View(GetVirtualPath("_JobStatusList"), JobStatusFilterList);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "StatusCode" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Description" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "Status Group" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "Closed" });
            filterlist.Add(new CommonBindList { Id = 5, Name = "LienAlert" });
            filterlist.Add(new CommonBindList { Id = 6, Name = "Notice" });
            filterlist.Add(new CommonBindList { Id = 7, Name = "Long Description" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADJobStatus> ListJobStatus = objJobStatus.ListJobStatus();
            Session["ListJobStatus"] = ListJobStatus;
            return PartialView(GetVirtualPath("_JobStatusList"), ListJobStatus);
        }

        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Liens/JobStatus/" + ViewName + ".cshtml";
            return path;
        }
    }
}