﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class OpenItemController : Controller
    {
        ADOpenItem objOpenItem = new ADOpenItem();

        // GET: OpenItem
        public ActionResult Index()
        {
            List<ADOpenItem> OpenItemList = objOpenItem.ListOpenItem();
            Session["ListOpenItem"] = OpenItemList;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"), OpenItemList);

        }
        //Show details on Edit
        public ActionResult OpenItemDetails(int id)
        {
            objOpenItem = new ADOpenItem();
            objOpenItem = objOpenItem.ReadOpenItem(id);
            return View(GetVirtualPath("OpenItemDetails"), objOpenItem);
        }
        //Save Updated or New Details
        [HttpPost]
        public bool OpenItemSave(ADOpenItem updatedOpenItem)
        {
            return objOpenItem.SaveOpenItem(updatedOpenItem);
        }
        //Show blank details on Add
        public ActionResult OpenItemAdd()
        {
            objOpenItem = new ADOpenItem();
            objOpenItem.Id = 0;
            return View(GetVirtualPath("OpenItemDetails"), objOpenItem);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {

            List<ADOpenItem> OpenItemList = (List<ADOpenItem>)Session["ListOpenItem"];
            List<ADOpenItem> OpenItemFilterList = null;
            if (value != "" || value != null)
            {
                switch (Key)
                {
                    case "1":
                        OpenItemFilterList = (from item in OpenItemList
                                              where item.OpenItemType.ToLower().Contains(value.ToLower())
                                              select item).ToList();
                        break;
                    case "2":
                        OpenItemFilterList = (from item in OpenItemList
                                              where item.OpenItemMsg.ToLower().Contains(value.ToLower())
                                              select item).ToList();
                        break;
                    default:
                        OpenItemFilterList = null;
                        break;
                }
            }
            return View(GetVirtualPath("_OpenItemList"), OpenItemFilterList);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Open Item Type" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Open Item Msg" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADOpenItem> ListOpenItem = objOpenItem.ListOpenItem();
            Session["ListOpenItem"] = ListOpenItem;
            return PartialView(GetVirtualPath("_OpenItemList"), ListOpenItem);
        }

        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Liens/OpenItem/" + ViewName + ".cshtml";
            return path;
        }
    }
}