﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using CRFView.Adapters;

namespace CRFView.Controllers
{
    public class CountyRecorderController : Controller
    {
        ADCountyRecorder objCountyRecorder = new ADCountyRecorder();
        List<ADCountyRecorder> ResultListCountyRecorder = new List<ADCountyRecorder>();
        // GET: CountyRecorder
        public ActionResult Index()
        {
            ResultListCountyRecorder = GetList();
            return PartialView(GetVirtualPath("Index"), ResultListCountyRecorder);
        }
        //Show details on Edit
        public ActionResult CountyRecorderDetails(int id)
        {
            ADCountyRecorder CountyRecorderinfo = objCountyRecorder.ReadCountyRecorder(id);
            return View(GetVirtualPath("CountyRecorderDetails"), CountyRecorderinfo);
        }
     
        //Save Updated or New Details
        [HttpPost]
        public bool CountyRecorderSave(ADCountyRecorder updatedCountyRecorder)
        {
            return objCountyRecorder.SaveCountyRecorder(updatedCountyRecorder);
        }
        //Show blank details on Add
        public ActionResult CountyRecorderAdd()
        {
            ADCountyRecorder objCountyRecorder = new ADCountyRecorder();
            ModelState.Clear();
            objCountyRecorder.CountyRecorderID = 0;
            return View(GetVirtualPath("CountyRecorderDetails"), objCountyRecorder);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value,string Key)
        {
            List<ADCountyRecorder> FilterResult = new List<ADCountyRecorder>();
            List<ADCountyRecorder> ListCountyRecorder = (List<ADCountyRecorder>)Session["ListCountyRecorder"];
            if (value != "" || value != null)
            {
                if (Key == "1")
                {
                    FilterResult = (from item in ListCountyRecorder
                                     where item.CountyRecorderNum.ToLower().Contains(value.ToLower())
                                     select item).ToList();  
                }
               else if (Key == "2")
                {
                    FilterResult = (from item in ListCountyRecorder
                                    where item.RecorderName.ToLower().Contains(value.ToLower())
                                    select item).ToList();
                }
                else if (Key == "3")
                {
                    FilterResult = (from item in ListCountyRecorder
                                    where item.RecorderCounty.ToLower().Contains(value.ToLower())
                                    select item).ToList();
                }
                else if (Key == "4")
                {
                    FilterResult = (from item in ListCountyRecorder
                                    where item.RecorderPhone.ToLower().Contains(value.ToLower())
                                    select item).ToList();
                }
                else if (Key == "5")
                {
                    FilterResult = (from item in ListCountyRecorder
                                    where item.RecorderInfo.ToLower().Contains(value.ToLower())
                                    select item).ToList();
                }

            }

            return View(GetVirtualPath("_CountyRecorderList"), FilterResult);
        }


        [HttpPost]
        public ActionResult Refresh()
        {
            ResultListCountyRecorder = GetList();
            return PartialView(GetVirtualPath("_CountyRecorderList"), ResultListCountyRecorder);
        }
       
        // Get List from database 
        public List<ADCountyRecorder> GetList()
        {
            List<ADCountyRecorder> lstCountyRecorder = objCountyRecorder.ListCountyRecorder();
            Session["ListCountyRecorder"] = lstCountyRecorder;
            SetFilterList();
            return lstCountyRecorder;
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "Recorder Number" });
            filterlist.Add(new CommonBindList { Id = 2, Name = "Recorder Name" });
            filterlist.Add(new CommonBindList { Id = 3, Name = "Recorder County" });
            filterlist.Add(new CommonBindList { Id = 4, Name = "Phone" });
            filterlist.Add(new CommonBindList { Id = 5, Name = "Important Info" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        //Get VirtualPath for view
        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Liens/CountyRecorder/" + ViewName + ".cshtml";
            return path;
        }
    }
}