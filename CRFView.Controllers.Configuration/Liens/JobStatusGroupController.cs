﻿using CRFView.Adapters;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace CRFView.Controllers
{
    public class JobStatusGroupController : Controller
    {
        ADJobStatusGroup objJobStatusGroup = new ADJobStatusGroup();

        // GET: JobStatusGroup
        public ActionResult Index()
        {
            List<ADJobStatusGroup> JobStatusGroupList = objJobStatusGroup.ListJobStatusGroup();
            Session["ListJobStatusGroup"] = JobStatusGroupList;
            SetFilterList();
            return PartialView(GetVirtualPath("Index"), JobStatusGroupList);

        }
        //Show details on Edit
        public ActionResult JobStatusGroupDetails(int id)
        {
            objJobStatusGroup = new ADJobStatusGroup();
            objJobStatusGroup = objJobStatusGroup.ReadJobStatusGroup(id);
            return View(GetVirtualPath("JobStatusGroupDetails"), objJobStatusGroup);
        }
        //Save Updated or New Details
        [HttpPost]
        public bool JobStatusGroupSave(ADJobStatusGroup updatedJobStatusGroup)
        {
            return objJobStatusGroup.SaveJobStatusGroup(updatedJobStatusGroup);
        }
        //Show blank details on Add
        public ActionResult JobStatusGroupAdd()
        {
            objJobStatusGroup = new ADJobStatusGroup();
            objJobStatusGroup.Id = 0;
            return View(GetVirtualPath("JobStatusGroupDetails"), objJobStatusGroup);
        }
        //Search Filter in Grid
        [HttpPost]
        public ActionResult Search(string value, string Key)
        {

            List<ADJobStatusGroup> JobStatusGroupList = (List<ADJobStatusGroup>)Session["ListJobStatusGroup"];
            List<ADJobStatusGroup> JobStatusGroupFilterList = null;
            if (value != "" || value != null)
            {
                switch (Key)
                {
                    case "1":
                        JobStatusGroupFilterList = (from item in JobStatusGroupList
                                              where item.StatusGroupDescription.ToLower().Contains(value.ToLower())
                                              select item).ToList();
                        break;
                 
                    default:
                        JobStatusGroupFilterList = null;
                        break;
                }
            }
            return View(GetVirtualPath("_JobStatusGroupList"), JobStatusGroupFilterList);
        }

        //Set FilterList
        public void SetFilterList()
        {
            List<CommonBindList> filterlist = new List<CommonBindList>();
            filterlist.Add(new CommonBindList { Id = 1, Name = "StatusGroup Description" });
            ViewBag.FilterList = filterlist;
            Session["FilterList"] = ViewBag.FilterList;
        }

        [HttpPost]
        public ActionResult Refresh()
        {
            List<ADJobStatusGroup> ListJobStatusGroup = objJobStatusGroup.ListJobStatusGroup();
            Session["ListJobStatusGroup"] = ListJobStatusGroup;
            return PartialView(GetVirtualPath("_JobStatusGroupList"), ListJobStatusGroup);
        }

        public string GetVirtualPath(string ViewName)
        {
            string path = "~/Views/CRFViewConfiguration/Liens/JobStatusGroup/" + ViewName + ".cshtml";
            return path;
        }
    }
}